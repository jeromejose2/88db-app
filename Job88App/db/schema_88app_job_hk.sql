
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

/*!40000 DROP DATABASE IF EXISTS `88app_job_hk`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `88app_job_hk` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `88app_job_hk`;
DROP TABLE IF EXISTS `app`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app` (
  `Id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `AppGuid` binary(16) NOT NULL COMMENT 'cid passed from 88Shop',
  `OwnerId` varchar(100) DEFAULT NULL COMMENT 'Member Id from 88shop',
  `AppName` varchar(200) DEFAULT NULL,
  `AdQuota` int(11) NOT NULL DEFAULT '-1',
  `SupportSAG` char(1) NOT NULL DEFAULT '0',
  `Published` char(1) NOT NULL DEFAULT '1' COMMENT '0: Not Published, 1: Published',
  `CreateTime` datetime DEFAULT NULL,
  `ModifiedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `AppGuidIndex` (`AppGuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `Id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `AppGuid` binary(16) NOT NULL,
  `Guid` binary(16) NOT NULL,
  `ParentId` bigint(20) unsigned NOT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Published` char(1) NOT NULL DEFAULT '1' COMMENT '0: Not Published, 1: Published',
  `Status` char(1) NOT NULL DEFAULT '1' COMMENT '0: Deleted, 1: Normal',
  `SortOrder` int(10) unsigned DEFAULT NULL,
  `CreateTime` datetime DEFAULT NULL,
  `ModifiedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `GuidIndex` (`Guid`),
  KEY `ParentIdIndex` (`ParentId`),
  KEY `AppGuidIndex` (`AppGuid`),
  KEY `SortOrderIndex` (`SortOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job` (
  `Id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Guid` binary(16) NOT NULL,
  `AppGuid` binary(16) NOT NULL,
  `CategoryId` bigint(20) unsigned NOT NULL,
  `Title` varchar(300) NOT NULL,
  `ReferenceId` varchar(300) NOT NULL,
  `Description` longtext,
  `Salary` varchar(100) DEFAULT NULL,
  `Negotiable` char(1) NOT NULL DEFAULT '0' COMMENT '0: Not negotiable, 1: Negotiable',
  `EmploymentType` varchar(300) NOT NULL,
  `ContactPerson` varchar(100) DEFAULT NULL,
  `Recipient` varchar(300) DEFAULT NULL COMMENT 'one or more Email address, sapareated by comma',
  `Contact` varchar(300) DEFAULT NULL,
  `Published` char(1) NOT NULL DEFAULT '1' COMMENT '0: Not Published, 1: Published',
  `Status` char(1) NOT NULL DEFAULT '1' COMMENT '0: Deleted, 1: Normal',
  `PostDate` date NOT NULL,
  `CreateTime` datetime DEFAULT NULL,
  `ModifiedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `CategoryIdIndex` (`CategoryId`),
  KEY `GuidIndex` (`Guid`),
  KEY `AppGuidIndex` (`AppGuid`),
  KEY `TitleIndex` (`Title`(255)),
  KEY `ReferenceIdIndex` (`ReferenceId`(255)),
  KEY `EmploymentTypeIndex` (`EmploymentType`(255)),
  KEY `PostDateIndex` (`PostDate`),
  KEY `SalaryIndex` (`Salary`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `sag_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sag_log` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Guid` binary(16) NOT NULL,
  `ShopGuid` binary(16) NOT NULL,
  `SAGTaskId` binary(16) NOT NULL,
  `VerificationCode` binary(16) NOT NULL,
  `Status` char(1) NOT NULL,
  `Message` varchar(300) DEFAULT NULL,
  `ProductGuid` binary(16) NOT NULL,
  `TargetId` varchar(100) NOT NULL,
  `ChannelId` varchar(100) NOT NULL,
  `TargetName` varchar(300) NOT NULL,
  `ChannelName` varchar(300) NOT NULL,
  `Quota` varchar(100) DEFAULT NULL,
  `PostUrl` varchar(300) DEFAULT NULL,
  `PostDate` date NOT NULL,
  `CreateTime` datetime NOT NULL,
  `ModifiedTime` datetime NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Guid` (`Guid`),
  UNIQUE KEY `SAGTaskId` (`SAGTaskId`),
  KEY `ShopGuid` (`ShopGuid`),
  KEY `ProductGuidIndex` (`ProductGuid`),
  KEY `CreateTimeIndex` (`CreateTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

