@ECHO OFF

ECHO Export schema
call phing -q -Dbuilddir=./build -Dsrcdir=. -DisWin=T DBExportSchema

ECHO Clean up schema
call phing -q -Dbuilddir=./build -Dsrcdir=. -DisWin=T DBCleanUpSchema


ECHO Export data
call phing -q -Dbuilddir=./build -Dsrcdir=. -DisWin=T DBExportData

ECHO Completed!