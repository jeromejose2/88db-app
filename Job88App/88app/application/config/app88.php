<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* This file is used to store the 88app specific settings which are identical to all countries and regions */

$config['name'] = 'value';

$config['admin_setting_tinycme_buttons_1'] = 'image,mediaLibrary,|,fontselect,fontsizeselect,|,forecolor,justifyleft,justifycenter,justifyright,justifyfull,|,bold,italic,underline,|,strikethrough,backcolor,hr,|,bullist,numlist,|,outdent,indent';
$config['admin_setting_tinycme_buttons_2'] = 'table,row_before,row_after,delete_row,col_before,col_after,delete_col,merge_cells,split_cells,row_props,cell_props,|,cut,copy,paste,pasteword,undo,redo,|,link,unlink,|,removeformat';
$config['admin_setting_tinycme_buttons_3'] = '';
$config['admin_setting_tinycme_buttons_4'] = '';
$config['admin_setting_tinycme_lang'] = array(
		'en'=>'en',
		'sc'=>'cn',
		'bm'=>'ms',
		'id'=>'id',
		'tc'=>'zh-tw',
		'th'=>'en'
);
$config['admin_setting_datepicker_lang'] = array(
		'en'=>'en-GB',
		'sc'=>'zh-CN',
		'bm'=>'ms',
		'id'=>'id',
		'tc'=>'zh-HK',
		'th'=>'th'
);

$config['admin_jobs_list_item_per_page'] = 10;	// added By Vincent
$config['category_sub_level'] = 1;
$config['jobs_list_item_per_page'] = 10;
$config['num_page_show'] = 7;
$config['data_feed_items'] = 50;

$config['job_items_per_page'] = array(
		10,
		20,
		50,
		100
);

$config['job_sorting_criteria'] = array(
		'poDate_asc'=>'front_view_sort_posdate_asc_label',
		'poDate_desc'=>'front_view_sort_posdate_desc_label',
		'title_asc'=>'front_view_sort_jobtitl_asc_label',
		'title_desc'=>'front_view_sort_jobtitl_desc_label',
		'empTyp_asc'=>'front_view_sort_emptype_asc_label',
		'empTyp_desc'=>'front_view_sort_emptype_desc_label'
);

$config['admin_supported_lang'] = array('bm','en','id','sc','tc','th');

$config['admin_sag_list_item_per_page'] = 20;
$config['88db_ws_lang'] = array('tc'=>'ZH-HK', 'en'=>'EN-US', 'bm'=>'MS-MY', 'id'=>'ID-ID', 'sc'=>'ZH-CN', 'th'=>'TH-TH');

$config['system_date_format'] = 'Y-m-d';
$config['db_system_date_format'] = '%Y-%m-%d';
$config['system_datepicker_month_name_short'] = '[ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ]';

$config['social_share_key'] = 'aKSi4sWcolIyo9kGX8Yihqa3Y2xnTNt6';
$config['front_addthis_lang'] = array(
		'bm' => 'ms',
		'en' => 'en',
		'id' => 'id',
		'sc' => 'zh', //Doesn't Support sc.
		'tc' => 'zh',
		'th' => 'th',
		'ja' => 'en'
);