<?php
class SAG_model extends CI_Model {

	private $_available_lang = array(APP88_FRONT_LANGUAGE);
	private $_content_lang = APP88_FRONT_LANGUAGE;
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('app_model');
		$app = $this->app_model->getAppByAppGuid($this->app88param->get88AppGuid());
		
		if ($app)
		{
			if (isset($app['AvailableLang']))
			{
				$app->_available_lang = $app['AvailableLang'];
			}
				
			$app->_content_lang = $app['DefaultLang']? $app['DefaultLang'] : $app->_content_lang;
				
			if (in_array($this->app88param->getLang(), $this->_available_lang))
			{
				$app->_content_lang = $this->app88param->getLang();
			}
		}
	}
	
	public function getShootAdStatusByGuid($guid)
	{
		$sql = 'select * from sag_log where Guid = UNHEX(?)';
		$param = array(
				$guid
		);
		
		$query = $this->db->query($sql, $param);
		
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('error_message'));
		}
		else if ($query->num_rows() > 0)
		{
			$row = $query->row_array();
			$row['Guid'] = $this->guid->createFromBinary($row['Guid'])->toString();
			$row['ShopGuid'] = $this->guid->createFromBinary($row['ShopGuid'])->toString();
			$row['SAGTaskId'] = $this->guid->createFromBinary($row['SAGTaskId'])->toString();
			$row['VerificationCode'] = $this->guid->createFromBinary($row['VerificationCode'])->toString();
			$row['ProductGuid'] = $this->guid->createFromBinary($row['ProductGuid'])->toString();
			return $row;
		}
		else
		{
			$this->app88log->log_message('db','debug', $this->db->_error_message());
			$this->app88log->log_message('db','debug', $this->db->last_query());
			return FALSE;
		}
	}
	
	public function getShootAdStatusById($id)
	{
		$sql = 'select * from sag_log where Id = ?';
		$param = array(
				$id
		);
	
		$query = $this->db->query($sql, $param);
	
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('error_message'));
		}
		else if ($query->num_rows() > 0)
		{
			$row = $query->row_array();
			$row['Guid'] = $this->guid->createFromBinary($row['Guid'])->toString();
			$row['ShopGuid'] = $this->guid->createFromBinary($row['ShopGuid'])->toString();
			$row['SAGTaskId'] = $this->guid->createFromBinary($row['SAGTaskId'])->toString();
			$row['VerificationCode'] = $this->guid->createFromBinary($row['VerificationCode'])->toString();
			$row['ProductGuid'] = $this->guid->createFromBinary($row['ProductGuid'])->toString();
			return $row;
		}
		else
		{
			$this->app88log->log_message('db','debug', $this->db->_error_message());
			$this->app88log->log_message('db','debug', $this->db->last_query());
			return FALSE;
		}
	}
	
	public function listShootAdStatusByShopGuid($shop_guid, $status = '',
												$offset = 0, $limit = 0, 
												$sortField = 'PostDate', $sortAsc=false, 
												$from = -1, $to = -1)
	{
		$sql = "SELECT SQL_CALC_FOUND_ROWS s.*, p.Title ";
		$sql .= "from sag_log s inner join job p ";
		$sql .= "on s.ProductGuid = p.Guid ";
		$sql .= "where s.ShopGuid = UNHEX(?) ";
		$sql .= "and p.AppGuid = UNHEX(?)";
		
		$param = array(
			$shop_guid,
			$shop_guid
		);
		
		if ($status != '')
		{
			$sql .= " and s.status = ?";
			$param []= $status;
		}
		
		$date_format = $this->config->item('db_system_date_format');
		
		if ($from > 0)
		{
			$sql.=" AND DATE(s.PostDate) >= STR_TO_DATE(?, '" . $date_format. "')";
			$param[] = $from;
		}
		
		if ($to > 0)
		{
			$sql.=" AND DATE(s.PostDate) <= STR_TO_DATE(?, '" . $date_format. "')";
			$param[] = $to;
		}
		
		$sql .= " order by ";
		switch ($sortField)
		{
			case 'ProductName':
				$sql .= "Title";
				
				break;
			case 'ChannelName':
			case 'TargetName':
			case 'PostDate':
			case 'Status':
				$sql .= $sortField;
				break;
			default:
				$this->app88log->log_message('db','error', 'Invalid parameter');
				show_error($this->lang->line('error_message'));
				break;
		}
		if ($sortAsc)
		{
			$sql .= " asc";
		}
		else
		{
			$sql .= " desc";
		}
		$sql .= ", s.Id desc";
		
		if ($limit)
		{
			$sql.=" limit ?,?";
			$param[] = $offset;
			$param[] = $limit;
		}
		else if ($offset)
		{
			$sql.=" limit ?";
			$param[] = $offset;
		}
		
		$query = $this->db->query($sql, $param);
	
		$result = array();
		//if no error construct the result array
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('error_message'));
		}
		else if ($query->num_rows() > 0)
		{
			$rs=array();
			foreach ($query->result_array() as $row)
			{
				$row['Guid'] = $this->guid->createFromBinary($row['Guid'])->toString();
				$row['ShopGuid'] = $this->guid->createFromBinary($row['ShopGuid'])->toString();
				$row['SAGTaskId'] = $this->guid->createFromBinary($row['SAGTaskId'])->toString();
				$row['VerificationCode'] = $this->guid->createFromBinary($row['VerificationCode'])->toString();
				$row['ProductGuid'] = $this->guid->createFromBinary($row['ProductGuid'])->toString();
				$rs[] = $row;
			}
			$result['result'] = $rs;
			
			$query = $this->db->query('select FOUND_ROWS()');
			$row = $query->row_array();
			$result['num_rows'] = $row['FOUND_ROWS()'];
		}
		else
		{
			$result['result'] = array();
			$result['num_rows'] = 0;
		}
		
		return $result;
	}
	
	public function addShootAdStatus($data)
	{
		$sql = 'insert into sag_log (Guid, ShopGuid, SAGTaskId, VerificationCode, Status, ProductGuid, TargetId, ChannelId, TargetName, ChannelName, Quota, PostDate, CreateTime, ModifiedTime) values (UNHEX(?),UNHEX(?),UNHEX(?),UNHEX(?),?,UNHEX(?),?,?,?,?,?,?,?,?)';
		$param = array(
				$data['Guid'],
				$this->app88param->get88AppGuid(),
				$data['SAGTaskId'],
				$data['VerificationCode'],
				$data['Status'],
				$data['ProductGuid'],
				$data['TargetId'],
				$data['ChannelId'],
				$data['TargetName'],
				$data['ChannelName'],
				$data['Quota'],
				$data['PostDate'],
				gmdate('Y-m-d H:i:s', time()),
				gmdate('Y-m-d H:i:s', time())
		);
		
		$this->db->trans_start();
		$query = $this->db->query($sql, $param);
		$sag_id = $this->db->insert_id();
		$this->db->trans_complete();
		
		if ($this->db->trans_status() !== FALSE)
		{
			return $this->getShootAdStatusById($sag_id);
		}
		else
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			$this->app88common->force_rollback();
			show_error($this->lang->line('error_message'));
		}
	}
	
	public function updateShootAdStatusByGuid($guid, $statusCode, $status = array())
	{
		$sql = 'update sag_log set Status = ?, Message = ?, PostUrl = ? where Guid = UNHEX(?)';
		$params []= $statusCode;
		$params []= $status['message'];
		$params []= $status['url'];
		$params []= $guid;
		
		$this->db->trans_start();
		
		$query = $this->db->query($sql, $params);
		
		$this->db->trans_complete();
		
		//die;
		if ($this->db->trans_status() !== FALSE)
		{
			return TRUE;
		}
		else
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			$this->app88common->force_rollback();
			show_error($this->lang->line('error_message'));
		}
	}
}