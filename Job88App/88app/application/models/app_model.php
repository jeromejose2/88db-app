<?php
class App_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	public function getAppByAppGuid($cid)
	{
		$sql = 'SELECT * FROM app WHERE AppGuid = UNHEX(?)';
		$param = array(
			$cid
		);
	
		$query = $this->db->query($sql, $param);
	
		if (!$query)
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			show_error($this->lang->line('error_message'));
		}
		else if ($query->num_rows() > 0)
		{
			$row = $query->row_array();
			$row['AppGuid'] = $this->guid->createFromBinary($row['AppGuid'])->toString();
			return $row;
		}
		else
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			return FALSE;
		}
	}
	public function getAppById($id)
	{
		$sql = 'SELECT * FROM app WHERE Id = ?';
		$param = array(
				$id
		);
	
		$query = $this->db->query($sql, $param);
	
		if (!$query)
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			show_error($this->lang->line('error_message'));
		}
		else if ($query->num_rows() > 0)
		{
			$row = $query->row_array();
			$row['AppGuid'] = $this->guid->createFromBinary($row['AppGuid'])->toString();
			return $row;
		}
		else
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			return FALSE;
		}
	}
	
	public function createApp($cid, $owner_id, $data)
	{
	
		$sql = 'INSERT INTO app (AppGuid, OwnerId, AppName, AdQuota, SupportSAG, CreateTime, ModifiedTime) values (UNHEX(?),?,?,?,?,?,?)';
		$param = array(
				$cid,
				$owner_id,
				$data['app_name'],
				APP88_AD_QUOTA,
				APP88_DEFAULT_SAG_SUPPORT,
				gmdate('Y-m-d H:i:s', time()),
				gmdate('Y-m-d H:i:s', time())
		);
		
		$this->db->trans_start();
		
		$query = $this->db->query($sql, $param);
		
		$this->db->trans_complete();
		
		//die;
		if ($this->db->trans_status() !== FALSE)
		{
			return $this->getAppByAppGuid($cid);
		}
		else
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			$this->app88common->force_rollback();
			show_error($this->lang->line('error_message'));
		}
		
	}
}