<?php
class Category_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->model('job_model');
	}
	

	public function getCategoryByGuid($guid, $style = Common::CATEGORY_INCLUDE_NOTHING)
	{

		$sql = 'SELECT * FROM category WHERE Guid = UNHEX(?)';
		$param = array(
			$guid
		);
		
		$query = $this->db->query($sql, $param);
		
		if (!$query)
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			show_error($this->lang->line('error_message'));
		}
		else if ($query->num_rows() > 0)
		{
			$result = $query->row_array();
			$result['Guid'] = $this->guid->createFromBinary($result['Guid'])->toString();
			$result['AppGuid'] = $this->guid->createFromBinary($result['AppGuid'])->toString();
			//when subcategory information is needed
			if($style & Common::CATEGORY_INCLUDE_SUBCATEGORY)
			{
				$category_list = $this->_constructFullCategoryListByAppGuid($result['AppGuid']);
				$subcategory = $this->_constructCategoryList($category_list,$result['Id'],-1);
				$result['Subcategory'] = $subcategory;
			}
			return $result;
		}
		else
		{
			return FALSE;
		}
	}
	
	
	public function getCategoryById($id, $style=Common::CATEGORY_INCLUDE_NOTHING)
	{
		$sql = 'SELECT * FROM category WHERE Id = ?';
		$param = array(
			$id
		);
		
		$query = $this->db->query($sql, $param);
		
		if (!$query)
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			show_error($this->lang->line('error_message'));
		}
		else if ($query->num_rows() > 0)
		{
			$result = $query->row_array();
			$result['Guid'] = $this->guid->createFromBinary($result['Guid'])->toString();
			$result['AppGuid'] = $this->guid->createFromBinary($result['AppGuid'])->toString();
			if($style & Common::CATEGORY_INCLUDE_SUBCATEGORY)
			{
				$category_list = $this->_constructFullCategoryListByAppGuid($result['AppGuid']);
				$subcategory = $this->_constructCategoryList($category_list,$result['Id'],-1);
				$result['Subcategory'] = $subcategory;
			}
			return $result;
		}
		else
		{
			return FALSE;
		}
	}
	

	public function listCategoryByAppGuid($guid,$style=Common::CATEGORY_INCLUDE_NOTHING,$keyword='')
	{
		$full_category_list = $this->_constructFullCategoryListByAppGuid($guid,$keyword);
				
		if ($style) 
		{
			return $this->_constructCategoryList($full_category_list,0,0,$style);
		}
		else
		{
			if($keyword!='')
			{
				$category_list = $this->_constructFullCategoryListByAppGuid($guid);
				return $this->_constructSearchCategoryList($full_category_list,$category_list);
			}
			return $full_category_list;
		}
	}
	
	
	private function _constructSearchCategoryList($category_list,$full_category_list)
	{
		$search_category_list = array();
		if(is_array($category_list))
		{
			foreach($category_list as $category)
			{
				$category_info = $category;
				$category_info['HasJobs'] = $this->job_model->listJobByCategoryGuid($category['Guid'],1);
				$category_info['HasJobs'] = is_array($category_info['HasJobs']);
				$parents = $this->getParentCategoryList($category['ParentId'],0,$full_category_list);
				if (empty($parents)) $category_info['Level'] = 0;
				else $category_info['Level'] = count($parents);
				foreach($full_category_list as $subcategory)
				{
					if($category['Id']==$subcategory['ParentId']) 
					{
						$category_info['Subcategory'] = $subcategory;
						break;
					}
				}
				$search_category_list[] = $category_info;
			}
		}
		return $search_category_list;
	}
	
	
	private function _constructFullCategoryListByAppGuid($guid,$keyword='')
	{
		$sql = "SELECT * FROM category WHERE AppGuid = UNHEX(?) AND Status ='1'";
		$param = array(
			$guid
		);
		if($keyword!='')
		{
			$sql .= " AND Name LIKE '%" . $this->db->escape_like_str($keyword) . "%'";
		}
		$sql .= " ORDER BY SortOrder ASC";
		$query = $this->db->query($sql, $param);
		if (!$query)
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			show_error($this->lang->line('error_message'));
		}
		else if ($query->num_rows() > 0)
		{
			$rs = array();
			foreach ($query->result_array() as $row)
			{
				$row['Guid'] = $this->guid->createFromBinary($row['Guid'])->toString();
				$row['AppGuid'] = $this->guid->createFromBinary($row['AppGuid'])->toString();
				$rs[] = $row;
			}
			return $rs;
		}
		else
		{
			
			return FALSE;
		}
	}
	
	
	private function _constructCategoryList($category_list,$parent_id=0,$level=0,$style=Common::CATEGORY_INCLUDE_SUBCATEGORY)
	{
		if ($level < 0) $level=-2;
		$category_array = array();
		
		if ($level <= $this->config->item('category_sub_level'))
		{
			if (is_array($category_list))
			{
				foreach ($category_list as $category)
				{
					if (($category['ParentId'] == $parent_id) && ($category['Status'] !=0 ))
					{
						$category_info = $category;
						if ($style & Common::CATEGORY_INCLUDE_JOB)
						{
							$category_info['HasJobs'] = $this->job_model->listJobByCategoryGuid($category['Guid'],1);
							$category_info['HasJobs'] = is_array($category_info['HasJobs']);
						}
						$category_info['Subcategory'] = $this->_constructCategoryList($category_list,
													$category_info['Id'],$level+1,$style);
						$category_array[] = $category_info;
					}
				}
			}
		}
		
		return $category_array;
	}

	
	public function getParentCategoryList($parent_id = 0, $app_guid = 0, $category_list = array(), $level = 0)
	{
		$parent = array();
		if ($level == 0 && empty($category_list))
		{
			$category_list = $this->_constructFullCategoryListByAppGuid($app_guid);
		}
		if ($parent_id != 0 && !empty($category_list))
		{
			foreach ($category_list as $category)
			{
				if ($category['Id'] == $parent_id)
				{
					$parent = array_merge($parent,$this->getParentCategoryList($category['ParentId'], $app_guid, $category_list, $level+1));
					$parent[] = $category;
				}
			}
		}
		return $parent;
	}
	
	
	public function getSubcategoryIdList($subcategory_array)
	{
		$id = array();
		if($subcategory_array)
		{
			foreach($subcategory_array as $category)
			{
				if ($category['Published'] ==1)
				{
					$id = array_merge($id,$this->getSubcategoryIdList($category['Subcategory']));
					$id[] = $category['Id'];
				}
			}
		}
		return $id;
	}
	
	
	public function addCategory($input)
	{
		$sql = "INSERT INTO category(AppGuid, Guid, ParentId, Name, Published, Status, SortOrder, CreateTime, ModifiedTime)
		VALUES (UNHEX(?),UNHEX(?),?,?,?,?,?,?,?)";
		$param = array(
				$input['app_guid'],
				Guid::newGuid()->toString(),
				$input['parent_id'],
				$input['category_name'],
				$input['published'],
				1,
				time(),
				gmdate("Y-m-d H:i:s", time()),
				gmdate("Y-m-d H:i:s", time())
		);
		
		$this->db->trans_start();
		
		$query = $this->db->query($sql, $param);
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() !== FALSE)
		{
			return TRUE;

		}
		else
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			$this->app88common->force_rollback();
			show_error($this->lang->line('error_message'));
		}
		
	}
	
	
	public function updateCategoryById($id,$new_category_info)
	{
		
		$sql = "UPDATE category SET ";
		$param=array();
	
		foreach($new_category_info as $key => $value)
		{
			$sql.=($key." = ?, ");
			$param[] = $value;
		}
		$sql=substr($sql, 0, -2).", ModifiedTime = ? WHERE Id = ?";
		$param[] = gmdate("Y-m-d H:i:s", time());
		$param[] = $id;
	
		$this->db->trans_start();
		
		$query = $this->db->query($sql, $param);
	
		$this->db->trans_complete();
		
		if ($this->db->trans_status() !== FALSE)
		{
			return TRUE;
		}
		else
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			$this->app88common->force_rollback();
			show_error($this->lang->line('error_message'));
		}
	
	}
	
	
	public function deleteCategory($category_guid)
	{
		$category_list = $this->getCategoryByGuid($category_guid, Common::CATEGORY_INCLUDE_NOTHING);
		
		$this->db->trans_start();
		
		$this->_deleteCategory($category_list['Id']);
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() !== FALSE)
		{
			return $category_list['Name'];
		}
		else
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			$this->app88common->force_rollback();
			show_error($this->lang->line('error_message'));
		}
		
	}
	
	
	private function _deleteCategory($id, $sub_category_list = array())
	{
		if ($id)
		{

			for ($i=0;$i<sizeof($sub_category_list);$i++)
			{
				$this->_deleteCategory($sub_category_list[$i]['Id'], $sub_category_list[$i]['Subcategory']);
			}
			
			$new_category_info = array(
				'Status' => 0,
			);
			
			/* 
			 * Only empty category can be deleted, so delete news function is remarked
			 */
			$this->updateCategoryById($id,$new_category_info);
		
		}
		return TRUE;
	}
	
	
	public function updateSortOrder($cat)
	{
	
		$sql = "UPDATE category SET SortOrder = ? WHERE Guid = UNHEX(?)";
	
		$this->db->trans_start();
	
		for ($i=0;$i<sizeof($cat);$i++)
		{
			$param = array();
			$param[] = $i;
			$param[] = $cat[$i];
			$query = $this->db->query($sql, $param);
		}
	
		$this->db->trans_complete();
		
		if ($this->db->trans_status() !== FALSE)
		{
			return TRUE;
		}
		else
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			$this->app88common->force_rollback();
			return FALSE;
		}
	
	}

	
	public function isCategoryBelongToShop($guid, $app_guid)
	{
		$sql = 'SELECT Id ';
		$sql .= 'FROM category ';
		$sql .= 'WHERE Guid = UNHEX(?) ';
		$sql .= 'AND AppGuid = UNHEX(?)';
		
		$query = $this->db->query($sql, array($guid, $app_guid));
		
		if (!$query)
		{
			return FALSE;
		}
		else if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}