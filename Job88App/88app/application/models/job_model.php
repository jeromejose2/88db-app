<?php
class Job_model extends CI_Model {

	private $_EMP_TYPES;
	private $_EMP_TYPES_STR_ASC;
	private $_EMP_TYPES_STR_DESC;
	
	function __construct()
	{
		parent::__construct();
		
		$this->_EMP_TYPES = unserialize(APP88_EMPLOYMENT_TYPE);
		
		$numOfType = count($this->_EMP_TYPES);
		
		for ($i=0; $i<$numOfType; $i++)
		{
			$this->_EMP_TYPES_STR_ASC .= ("'" . $this->_EMP_TYPES[$i] . "'");
			
			if ($i != $numOfType - 1)
			{
				$this->_EMP_TYPES_STR_ASC .= ", ";
			}
			
			$this->_EMP_TYPES_STR_DESC = "'" . $this->_EMP_TYPES[$i] . "'" . $this->_EMP_TYPES_STR_DESC;
			
			if ($i != $numOfType - 1)
			{
				$this->_EMP_TYPES_STR_DESC = ", " . $this->_EMP_TYPES_STR_DESC;
			}	
		}
		
	}
	
	
	public function getJobByGuid($guid)
	{
		$sql = "SELECT * FROM job WHERE Guid = UNHEX(?) AND Status = '1'";
		$param = array(
			$guid
		);
		
		$query = $this->db->query($sql, $param);
		
		if (!$query)
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			show_error($this->lang->line('error_message'));
		}
		else if ($query->num_rows() > 0)
		{
			$row = $query->row_array();
			$row['Guid'] = $this->guid->createFromBinary($row['Guid'])->toString();
			$row['AppGuid'] = $this->guid->createFromBinary($row['AppGuid'])->toString();
			$row['PostDateFormatted'] = date(APP88_DATE_FORMAT,strtotime($row['PostDate']));
			return $row;
		}
		else
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			return FALSE;
		}
	}
	
	
	public function getJobById($id)
	{
		$sql = "SELECT * FROM job WHERE Id = ? AND Status = '1'";
		$param = array(
			$id
		);
		
		$query = $this->db->query($sql, $param);
		
		if (!$query)
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			show_error($this->lang->line('error_message'));
		}
		else if ($query->num_rows() > 0)
		{
			$row = $query->row_array();
			$row['Guid'] = $this->guid->createFromBinary($row['Guid'])->toString();
			$row['AppGuid'] = $this->guid->createFromBinary($row['AppGuid'])->toString();
			return $row;
		}
		else
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			return FALSE;
		}
	}
	
	
	public function listJobByAppGuid($app_guid, $limit=0, $offset=0, $keyword='', $startDate='', $endDate='', $published=-1, $withCategory=0, $is_front=-1, $odrType='')
	{
		if ($withCategory)
		{
			$sql = "SELECT SQL_CALC_FOUND_ROWS ".
				"j.*, ".
				"IF(c1.ParentId = 0, c1.Name, c2.Name) AS MainCategory, ".
				"IF(c1.ParentId = 0, NULL, c1.Name) AS SubCategory ".
				"FROM job j INNER JOIN (category c1 LEFT JOIN category c2 ON c1.ParentId = c2.Id) ON j.CategoryId = c1.Id ".
				"WHERE j.AppGuid = UNHEX(?) AND c1.AppGuid = UNHEX(?) AND (c1.ParentId = 0 OR c2.AppGuid = UNHEX(?)) AND j.Status ='1'";
			$params[] = $app_guid;
			$params[] = $app_guid;
			$params[] = $app_guid;
		}
		else
		{
			$sql = "SELECT SQL_CALC_FOUND_ROWS j.* FROM job j WHERE j.AppGuid = UNHEX(?) AND j.Status ='1'";
			$params[] = $app_guid;
		}
		if (!empty($keyword))
		{
			$sql.=" AND (j.Title  LIKE '%".$this->db->escape_like_str($keyword)."%'"." OR j.ReferenceId LIKE '%".$this->db->escape_like_str($keyword)."%')";
		}
		
		$date_format = $this->config->item('db_system_date_format');
		if (!empty($startDate))
		{
			$sql.=" AND DATE(j.PostDate) >= STR_TO_DATE(?, '" . $date_format. "')";
			$params[] = $startDate;
		}
		if (!empty($endDate))
		{
			$sql.=" AND DATE(j.PostDate) <= STR_TO_DATE(?, '" . $date_format. "')";
			$params[] = $endDate;
		}
		if ($published >=0 )
		{
			$sql.=" AND j.Published = ?";
			$params[] = $published;
		}
		if ($is_front == 1)
		{
			$curdate = $this->app88common->local_date();
			$sql.=" and PostDate <= ?";
			$params[] = $curdate;
		}
		
		switch ($odrType)
		{
			case 'poDate_asc':
				$sql .= ' ORDER BY j.PostDate ASC, j.Id DESC';
				break;
			case 'poDate_desc':
				$sql .= ' ORDER BY j.PostDate DESC, j.Id DESC';
				break;
			case 'title_asc':
				$sql .= ' ORDER BY j.Title ASC, j.PostDate DESC';
				break;
			case 'title_desc':
				$sql .= ' ORDER BY j.Title DESC, j.PostDate DESC';
				break;
			case 'empTyp_asc':
				$sql .= ' ORDER BY FIELD(j.EmploymentType,' . $this->_EMP_TYPES_STR_ASC . ' ) , j.PostDate DESC';
				break;
			case 'empTyp_desc':
				$sql .= ' ORDER BY FIELD(j.EmploymentType,' . $this->_EMP_TYPES_STR_DESC . ' ) , j.PostDate DESC';
				break;
			case 'saly_asc':
				$sql .= ' ORDER BY CAST(j.Salary AS UNSIGNED) ASC, j.PostDate DESC';
				break;
			case 'saly_desc':
				$sql .= ' ORDER BY CAST(j.Salary AS UNSIGNED) DESC, j.PostDate DESC';
				break;
			case 'refid_asc':
				$sql .= ' ORDER BY j.ReferenceId ASC, j.PostDate DESC';
				break;
			case 'refid_desc':
				$sql .= ' ORDER BY j.ReferenceId DESC, j.PostDate DESC';
				break;
			default:
				$sql .= ' ORDER BY j.PostDate DESC, j.Id DESC';
				break;
		}
		
		if ($limit > 0)
		{
			$sql.=' limit ?,?';
			$params[] = $offset;
			$params[] = $limit;
		}
	
		$query = $this->db->query($sql, $params);
	
		if (!$query)
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			show_error($this->lang->line('error_message'));
		}
		else if ($query->num_rows() > 0)
		{
			$rs=array('rs'=>array(),'total_records'=>0);
			foreach ($query->result_array() as $row)
			{
				$row['Guid'] = $this->guid->createFromBinary($row['Guid'])->toString();
				$row['AppGuid'] = $this->guid->createFromBinary($row['AppGuid'])->toString();
				$row['PostDateFormatted'] = date(APP88_DATE_FORMAT,strtotime($row['PostDate']));
				$rs['rs'][] = $row;
			}

			$query = $this->db->query('SELECT FOUND_ROWS()');
			$row = $query->row_array();
			$rs['total_records'] = $row['FOUND_ROWS()'];
			return $rs;
		}
		else
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			return FALSE;
		}
	}
	
	
	public function listJobByCategoryGuid($cat, $limit=0, $offset=0, $keyword='', $odrType='')
	{
		$sql = "SELECT SQL_CALC_FOUND_ROWS j.* FROM job j INNER JOIN category c ON j.CategoryId = c.Id AND j.Status ='1'";
		if (!empty($keyword))
		{
			$sql.=" AND (j.Title  LIKE '%".$this->db->escape_like_str($keyword)."%'"." OR j.ReferenceId LIKE '%".$this->db->escape_like_str($keyword)."%')";
		}
		if (is_array($cat) && count($cat) > 0)
		{
			$sql.=" AND (";
			for ($i = 0; $i < count($cat); $i++)
			{
				$sql.= "c.Guid = UNHEX(?)";
				if ($i+1 < count($cat)) $sql.=" OR ";
				$param[] = $cat[$i];
			}
			$sql.=")";
		}
		else
		{
			$sql.=" AND c.Guid = UNHEX(?)";
			$param[] = $cat;
		}
		
		if(!is_array($cat))
		{
			$sql.=" ORDER BY j.PostDate DESC, j.Id DESC";
		}
		
		if ($limit != 0)
		{
			$sql.=" limit ?,?";
			$param[] = $offset;
			$param[] = $limit;
		}
		
	
		$query = $this->db->query($sql, $param);
		
		if (!$query)
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			show_error($this->lang->line('error_message'));
		}
		else if ($query->num_rows() > 0)
		{
			$rs=array('rs'=>array(),'total_records'=>0);
			foreach ($query->result_array() as $row)
			{
				$row['Guid'] = $this->guid->createFromBinary($row['Guid'])->toString();
				$row['AppGuid'] = $this->guid->createFromBinary($row['AppGuid'])->toString();
				$rs['rs'][] = $row;
			}
	
			$query = $this->db->query('select FOUND_ROWS()');
			$row = $query->row_array();
			$rs['total_records'] = $row['FOUND_ROWS()'];
			return $rs;
		}
		else
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			return FALSE;
		}
	}
	

	public function listJobByCategoryId($cat, $limit=0, $offset=0, $published=-1, $is_front=-1, $keyword='', $odrType='')
	{
		$sql = "SELECT SQL_CALC_FOUND_ROWS j.* FROM ";
		if (is_array($cat) && count($cat) > 0)
		{
			$sql.="(";
			for ($i = 0; $i < count($cat); $i++)
			{
				$sql.= "(SELECT * FROM job WHERE CategoryId = ? AND Status = '1'";
				$param[] = $cat[$i];
				if ($published >= 0)
				{
					$sql.= " AND Published = ?";
					$param[] = $published;
				}
				
				if (!empty($keyword))
				{
					$sql.= " AND (Title  LIKE '%" . $this->db->escape_like_str($keyword)."%'" . " OR ReferenceId LIKE '%" . $this->db->escape_like_str($keyword)."%')";
				}
				
				if ($is_front == 1)
				{
					$curdate = $this->app88common->local_date();
					$sql.=" AND PostDate <= ?";
					$param[] = $curdate;
				}
				$sql.=")";
				if ($i+1 < count($cat)) $sql.=" UNION ";
			}
			$sql.=") AS j ";
		}
		else
		{
			$sql.="job AS j WHERE CategoryId = ? AND Status = '1'";
			$param[] = $cat;
			if ($published >= 0)
			{
				$sql.=" AND Published = ?";
				$param[] = $published;
			}
			if ($is_front == 1)
			{
				$curdate = $this->app88common->local_date();
				$sql.= " and PostDate <= ?";
				$param[] = $curdate;
			}
		}
		
		switch ($odrType)
		{
			case 'poDate_asc':
				$sql .= ' ORDER BY j.PostDate ASC, j.Id DESC';
				break;
			case 'poDate_desc':
				$sql .= ' ORDER BY j.PostDate DESC, j.Id DESC';
				break;
			case 'title_asc':
				$sql .= ' ORDER BY j.Title ASC, j.PostDate DESC';
				break;
			case 'title_desc':
				$sql .= ' ORDER BY j.Title DESC, j.PostDate DESC';
				break;
			case 'empTyp_asc':
				$sql .= ' ORDER BY FIELD(j.EmploymentType,' . $this->_EMP_TYPES_STR_ASC . ' ) , j.PostDate DESC';
				break;
			case 'empTyp_desc':
				$sql .= ' ORDER BY FIELD(j.EmploymentType,' . $this->_EMP_TYPES_STR_DESC . ' ) , j.PostDate DESC';
				break;
			case 'saly_asc':
				$sql .= ' ORDER BY CAST(j.Salary AS UNSIGNED) ASC, j.PostDate DESC';
				break;
			case 'saly_desc':
				$sql .= ' ORDER BY CAST(j.Salary AS UNSIGNED) DESC, j.PostDate DESC';
				break;
			case 'refid_asc':
				$sql .= ' ORDER BY j.ReferenceId ASC, j.PostDate DESC';
				break;
			case 'refid_desc':
				$sql .= ' ORDER BY j.ReferenceId DESC, j.PostDate DESC';
				break;				
			default:
				$sql .= ' ORDER BY j.PostDate DESC, j.Id DESC';
				break;
		}
		
		if ($limit > 0)
		{
			$sql.=' limit ?,?';
			$param[] = $offset;
			$param[] = $limit;
		}
	
		$query = $this->db->query($sql, $param);
		
		if (!$query)
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			show_error($this->lang->line('error_message'));
		}
		else if ($query->num_rows() > 0)
		{
			$rs=array('rs'=>array(),'total_records'=>0);
			foreach ($query->result_array() as $row)
			{
				$row['Guid'] = $this->guid->createFromBinary($row['Guid'])->toString();
				$row['AppGuid'] = $this->guid->createFromBinary($row['AppGuid'])->toString();
				$rs['rs'][] = $row;
			}
	
			$query = $this->db->query('SELECT FOUND_ROWS()');
			$row = $query->row_array();
			$rs['total_records'] = $row['FOUND_ROWS()'];
			return $rs;
		}
		else
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			return FALSE;
		}
	}
	
	
	public function deleteJobByGuid($guid)
	{
		$sql = "UPDATE job SET Status = '0', ModifiedTime = ? WHERE Guid = UNHEX(?)";
		$param = array(
				gmdate('Y-m-d H:i:s', time()),
				$guid
		);
	
		$this->db->trans_start();
	
		$query = $this->db->query($sql, $param);
	
		$this->db->trans_complete();
	
		//die;
		if ($this->db->trans_status() !== FALSE)
		{
			return TRUE;
		}
		else
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			$this->app88common->force_rollback();
			show_error($this->lang->line('error_message'));
		}
	}
	
	
	public function deleteJobByCategoryId($cat_id)
	{
		$sql = "UPDATE job SET Status = '0', ModifiedTime = ? WHERE CategoryId = ?";
		$param = array(
			gmdate('Y-m-d H:i:s', time()),
			$cat_id
		);
		
		$this->db->trans_start();
		
		$query = $this->db->query($sql, $param);
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() !== FALSE)
		{
			return TRUE;
		}
		else
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			$this->app88common->force_rollback();
			show_error($this->lang->line('error_message'));
		}
	}
	
	
	public function addJob($data, $resume_data = array())
	{
		$this->db->trans_start();
		
		$sql = 'INSERT INTO job (AppGuid, CategoryId, Guid, Title, ReferenceId, Description, Salary, Negotiable, EmploymentType, ContactPerson, Recipient, Contact, Published, Status, PostDate, CreateTime, ModifiedTime) values (UNHEX(?),?,UNHEX(?),?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
		$param = array(
				$this->app88param->get88AppGuid(),
				$data['category_id'],
				$data['guid'],
				$data['job_title'],
				$data['job_refid'],
				$data['job_desc'],
				$data['job_salary'],
				$data['job_negot'],
				$data['job_emply_type'],
				$data['contact_person'],
				$data['job_recips'],
				$data['job_contact'],
				$data['job_publish'],
				'1',
				$data['post_date'],
				gmdate('Y-m-d H:i:s', time()),
				gmdate('Y-m-d H:i:s', time())
		);
		
		$query = $this->db->query($sql, $param);
		
		$job_id = $this->db->insert_id();
	
		$this->db->trans_complete();
		
		if ($this->db->trans_status() !== FALSE)
		{
			return $this->getJobById($job_id);
		}
		else
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			$this->app88common->force_rollback();
			show_error($this->lang->line('error_message'));
		}
	}
	
	
	public function updateJob($guid, $data)
	{
		$this->db->trans_start();
		$sql = 'UPDATE job SET CategoryId = ?, Title = ?, Description = ?, Salary = ?, Negotiable = ?, EmploymentType = ?, ContactPerson = ?, Recipient = ?, Contact = ?, Published = ?, PostDate=?, ModifiedTime = ? WHERE Guid = UNHEX(?)';
		$param = array(
				$data['category_id'],
				$data['job_title'],
				$data['job_desc'],
				$data['job_salary'],
				$data['job_negot'],
				$data['job_emply_type'],
				$data['contact_person'],
				$data['job_recips'],
				$data['job_contact'],					
				$data['job_publish'],
				$data['post_date'],
				gmdate('Y-m-d H:i:s', time()),
				$guid
		);
		$query = $this->db->query($sql, $param);
		
		$job = $this->getJobByGuid($guid);
		$job_id = $job['Id'];
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() !== FALSE)
		{
			return TRUE;
		}
		else
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			$this->app88common->force_rollback();
			show_error($this->lang->line('error_message'));
		}
	}
	
	
	public function searchJobByTitle($keyword, $app_guid, $limit = 0, $offset = 0, $category_id = '', $is_front = 1, $odrType='')
	{
		if (empty($keyword))
		{
			$sql = "SELECT SQL_CALC_FOUND_ROWS j.* FROM job AS j WHERE j.AppGuid = UNHEX(?) AND j.Status = 1 AND j.Published = 1";
		}
		else
		{
			$sql = "SELECT SQL_CALC_FOUND_ROWS j.* FROM job AS j WHERE (j.Title LIKE '%". $this->db->escape_like_str($keyword) ."%' OR j.ReferenceId LIKE '%" . $this->db->escape_like_str($keyword) . "%') AND j.AppGuid = UNHEX(?) AND j.Status = 1 AND j.Published = 1";
		}

		$params[] = $app_guid;
		if ($category_id)
		{
			$sql.=" AND j.CategoryId = ?";
			$params[] = $category_id;
		}
		
		if ($is_front == 1)
		{
			$curdate = $this->app88common->local_date();
			$sql .= " AND j.PostDate <= ?";
			$params[] = $curdate;
		}
		
		switch ($odrType)
		{
			case 'poDate_asc':
				$sql .= ' ORDER BY j.PostDate ASC, j.Id DESC';
				break;
			case 'poDate_desc':	
				$sql .= ' ORDER BY j.PostDate DESC, j.Id DESC';
				break;
			case 'title_asc':
				$sql .= ' ORDER BY j.Title ASC, j.PostDate DESC';
				break;
			case 'title_desc':
				$sql .= ' ORDER BY j.Title DESC, j.PostDate DESC';
				break;
			case 'empTyp_asc':
				$sql .= ' ORDER BY FIELD(j.EmploymentType,' . $this->_EMP_TYPES_STR_ASC . ' ) , j.PostDate DESC';
				break;
			case 'empTyp_desc':
				$sql .= ' ORDER BY FIELD(j.EmploymentType,' . $this->_EMP_TYPES_STR_DESC . ' ) , j.PostDate DESC';
				break;
			case 'saly_asc':
				$sql .= ' ORDER BY CAST(j.Salary AS UNSIGNED) ASC, j.PostDate DESC';
				break;
			case 'saly_desc':
				$sql .= ' ORDER BY CAST(j.Salary AS UNSIGNED) DESC, j.PostDate DESC';
				break;
			case 'refid_asc':
				$sql .= ' ORDER BY j.ReferenceId ASC, j.PostDate DESC';
				break;
			case 'refid_desc':
				$sql .= ' ORDER BY j.ReferenceId DESC, j.PostDate DESC';
				break;				
			default:
				$sql .= ' ORDER BY j.PostDate DESC, j.Id DESC';
				break;
		}
		
		if ($limit > 0)
		{
			$sql.=" LIMIT ?,?";
			$params[] = $offset;
			$params[] = $limit;
		}
		$query = $this->db->query($sql, $params);
 		if (!$query)
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			show_error($this->lang->line('error_message'));
		}
		else if ($query->num_rows() > 0)
		{
			$rs=array('rs'=>array());
			foreach ($query->result_array() as $row)
			{
				$row['Guid'] = $this->guid->createFromBinary($row['Guid'])->toString();
				$row['AppGuid'] = $this->guid->createFromBinary($row['AppGuid'])->toString();
				$rs['rs'][] = $row;
			}
			$query = $this->db->query('SELECT FOUND_ROWS()');
			$row = $query->row_array();
			$rs['total_records'] = $row['FOUND_ROWS()'];
			return $rs;
		}
		else
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			return FALSE;
		}
	}
	
	
	public function countActiveJobsByAppGuid($guid)
	{
		$sql = "SELECT COUNT(*) AS NumberOfJobs FROM job where AppGuid = UNHEX(?) and Status = '1' and Published = '1'";
		$params[] = $guid;
	
		$query = $this->db->query($sql, $params);
	
		if (!$query)
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			show_error($this->lang->line('error_message'));
		}
		else if ($query->num_rows() > 0)
		{
			$row = $query->row_array();
			return $row['NumberOfJobs'];
		}
		else
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			return 0;
		}
	}
	
	
	public function getNextJob($app_guid, $cat_guid, $postDate, $jobId)
	{
		return $this->getSblingJob($app_guid, $cat_guid, $postDate, $jobId,
			'j.PostDate < ? OR (j.PostDate = ? AND j.Id < ?)',
			'j.PostDate DESC, j.Id DESC', true);
	}
	
	
	public function getPrevJob($app_guid, $cat_guid, $postDate, $jobId)
	{
		return $this->getSblingJob($app_guid, $cat_guid, $postDate, $jobId,
			'j.PostDate > ? OR (j.PostDate = ? AND j.Id > ?)',
			'j.PostDate ASC, j.Id ASC', false);
	}
	
	
	private function getSblingJob($app_guid, $cat_guid, $postDate, $jobId, $timeConditionStmt, $orderStmt, $isFindingNext)
	{
		$sql = "SELECT SQL_CALC_FOUND_ROWS ".
				"j.Title, j.Guid ".
				"FROM job j INNER JOIN (category c1 LEFT JOIN category c2 ON c1.ParentId = c2.Id) ON j.CategoryId = c1.Id ".
				"WHERE j.AppGuid = UNHEX(?) AND c1.AppGuid = UNHEX(?) AND (c1.ParentId = 0 OR c2.AppGuid = UNHEX(?)) AND j.Status ='1' AND j.Published = '1' AND j.PostDate <= ?";
		$params[] = $app_guid;
		$params[] = $app_guid;
		$params[] = $app_guid;
		$params[] = $this->app88common->local_date();
		
		if (!empty($cat_guid) &&
			$cat_guid != 'all')
		{
			$sql .= " AND (c1.Guid = UNHEX(?) OR c2.Guid = UNHEX(?))";
			$params[] = $cat_guid;
			$params[] = $cat_guid;
		}
		
		$sql .= " AND (";
		$sql .= $timeConditionStmt;
		$sql .= ")";
		
		$params[] = $postDate;
		$params[] = $postDate;
		$params[] = $jobId;
		
		$sql .= " ORDER BY ";
		$sql .= $orderStmt;
		$sql .= " LIMIT 0,1";
		
		$query = $this->db->query($sql, $params);
		if (!$query)
		{
			$this->app88log->log_message('db','ERROR', $this->db->_error_message());
			$this->app88log->log_message('db','ERROR', $this->db->last_query());
			show_error($this->lang->line('error_message'));
		}
		else if ($query->num_rows() > 0)
		{
			$result = $query->result_array();
			$result[0]['Guid'] = $this->guid->createFromBinary($result[0]['Guid'])->toString();
			return $result[0];
		}
		else
		{
			return null;
		}
	}
	
	
	public function isReferenceIdNonExist($app_guid, $refId)
	{
		if (!$refId)
		{
			return FALSE;
		}
		else
		{
			$sql = "SELECT Id FROM job WHERE AppGuid = UNHEX(?) AND ReferenceId = ?";
			$params = array($app_guid, $refId);
			$query = $this->db->query($sql, $params);
			if (!$query)
			{
				$this->app88log->log_message('db','ERROR', $this->db->_error_message());
				$this->app88log->log_message('db','ERROR', $this->db->last_query());
				show_error($this->lang->line('error_message'));
			}
			else if ($query->num_rows() > 0)
			{
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
	}
	
	
	public function isJobBelongToShop($guid, $app_guid)
	{
		$sql = 'SELECT Id ';
		$sql .= 'FROM job ';
		$sql .= 'WHERE Guid = UNHEX(?) ';
		$sql .= 'AND AppGuid = UNHEX(?)';
		
		$query = $this->db->query($sql, array($guid, $app_guid));
		
		if (!$query)
		{
			return FALSE;
		}
		else if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}