<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Category extends CI_Controller
{
	private $_EMP_TYPES;
	
	public function __construct($config = array())
	{
		parent::__construct();
		
		$this->lang->load('front', $this->app88param->getLang()?$this->app88param->getLang():APP88_FRONT_LANGUAGE);
		
		if (!$this->app88param->get88AppGuid())
		{
			show_error($this->lang->line('err_session_expired'));
		}

		$this->load->library('app88Authen');
		$this->load->model('job_model');
		$this->load->model('category_model');
		$this->load->model('app_model');
		
		$this->_EMP_TYPES = unserialize(APP88_EMPLOYMENT_TYPE);
		
		$app = $this->app_model->getAppByAppGuid($this->app88param->get88AppGuid());
		if ($app['Published'] == '0')
		{
			show_error($this->lang->line('err_app_closed'));
		}
	}
	
	public function index($category_guid = 'all',$page = 1)
	{
		$current_category = array();
		$parent_category = array();
		
		$limit = $this->config->item('jobs_list_item_per_page');
		
		$items_per_page_options = $this->config->item('job_items_per_page');
	
		$sorting_criteria_options = $this->config->item('job_sorting_criteria');
		
		if ($this->input->get('items_per_page'))
		{
			$limit = intval($this->input->get('items_per_page'));
		}
		
		$offset = ($page-1)*$limit;
		
		$categories = $this->category_model->listCategoryByAppGuid($this->app88param->get88AppGuid(),Common::CATEGORY_INCLUDE_SUBCATEGORY);
		
		if ($categories === FAlSE) $categories = array();
		
		$jobs = array('rs'=>array(),'total_records'=>0);
	
		$sort = $this->input->get("sort")? $this->input->get("sort") : 'poDate_desc';
	
		if (!$this->input->get('keyword'))
		{
			
			$input = $this->input->get();
			
			if ($category_guid !='all' && $category_guid)
			{
				$current_category = $this->category_model->getCategoryByGuid($category_guid,Common::CATEGORY_INCLUDE_SUBCATEGORY);
				if(!$current_category)
				{
					$current_category = array();
					$parent_category = array();
				}
				else
				{
					$parent_category = $this->category_model->getParentCategoryList($current_category['ParentId'],$this->app88param->get88AppGuid());
				}
				$view_params['category_name'] = $current_category['Name'];
				$ids = $this->category_model->getSubcategoryIdList($current_category['Subcategory']);
				$ids[] = $current_category['Id'];
				$jobs = $this->job_model->listJobByCategoryId($ids, $limit, $offset, 1, 1, '', $sort);
			}
			else
			{
				$view_params['category_name'] = $this->lang->line('all_jobs_label');
				
				$jobs = $this->job_model->listJobByAppGuid($this->app88param->get88AppGuid(), $limit, $offset, '', '', '', 1, 0, 1, $sort);
															
			}
		}
		else
		{
			$input = $this->input->get();
			$view_params['category_name'] = $this->lang->line('search_result_label');
			$jobs = $this->job_model->searchJobByTitle($input['keyword'], $this->app88param->get88AppGuid(), $limit, $offset, '', 1, $sort);
			$category_guid = 'all';
		}
		
		
		
		
		if ($jobs === FALSE) $jobs = array('rs'=>array(),'total_records'=>0);
		
		$record_per_page = $this->config->item('admin_jobs_list_item_per_page');
		
		
		$view_params['jobs'] = $jobs['rs'];
		$view_params['employ_types'] = $this->_EMP_TYPES;
		$view_params['parent_category'] = $parent_category;
		$view_params['categories'] = $categories;
		$view_params['category_guid'] = $category_guid;
		$view_params['max_page'] = ceil($jobs['total_records']/$limit);
		$view_params['total_record'] = $jobs['total_records'];
		$view_params['page'] = $page;
		$view_params['keyword'] = $this->input->get("keyword")? $this->input->get('keyword') : "";
		$view_params['sort'] = $sort;
		
		$sortOptStates = array('none', 'desc', 'asc');
		
		$view_params['sort_btn_states_str'] = "'" . implode("', '", $sortOptStates) . "'";
		
		$fliped = array_flip($sortOptStates);
		
		if (!empty($sort))
		{
			$pos = strpos($sort, '_');
			
			$fildStr = substr($sort, 0, $pos);
			$view_params['sort_field'] = $fildStr;
			
			$odrStr = substr($sort, $pos+1);
			$view_params['sort_btn_state'] = "0";
			$view_params['sort_btn_state'] = $fliped[$odrStr];			
		}
		
		$view_params['items_per_page'] = $limit;
		$view_params['items_per_page_opts'] = $items_per_page_options;
		$view_params['sorting_criteria_opts'] = $sorting_criteria_options;
		$view_params['info'] = $this->session->userdata('action_complete_message');
		$view_params['error'] = $this->session->userdata('action_error_message');
		
		$this->load->view("front/category/list",$view_params);
		
		$items_to_clear = array('action_complete_message'=>'', 'action_error_message'=>'');
		$this->session->unset_userdata($items_to_clear);
	}
	
	
}