<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Job extends CI_Controller
{
	public function __construct($config = array())
	{
		parent::__construct();

		$this->lang->load('front', $this->app88param->getLang()?$this->app88param->getLang():APP88_FRONT_LANGUAGE);
	
		if (!$this->app88param->get88AppGuid())
		{
			show_error($this->lang->line('err_session_expired'));
		}
	
		$this->load->library('app88Authen');
		$this->load->model('category_model');
		$this->load->model('app_model');
		$this->load->model('job_model');
	
		$app = $this->app_model->getAppByAppGuid($this->app88param->get88AppGuid());
		if ($app['Published'] == '0')
		{
			show_error($this->lang->line('err_app_closed'));
		}
	}
	
	
	public function view($guid = '')
	{
		if ($guid === '')
		{
			redirect("category/index");
		}
		
		if (strlen($guid) < 32)
		{
			$job = $this->job_model->getJobById($guid);
			$guid = $job['Guid'];
		}
		else 
		{
			$job = $this->job_model->getJobByGuid($guid);
		}		
		
		if ($job === FALSE || $job['Status'] == 0 || $job['Published'] == 0)
		{
			redirect("category/index");
		}		
		
		$query_string = '';
		if ($this->input->get('items_per_page')) $query_string .= "&items_per_page=".urlencode($this->input->get('items_per_page'));
		if ($this->input->get('keyword')) $query_string .= "&keyword=".urlencode($this->input->get('keyword'));
		if ($this->input->get('sort')) $query_string .= "&sort=".urlencode($this->input->get('sort'));
		
		$categories = $this->category_model->listCategoryByAppGuid($this->app88param->get88AppGuid(),Common::CATEGORY_INCLUDE_SUBCATEGORY);
		if ($categories === FAlSE) $categories = array();
		
		$parent_categories = $this->category_model->getParentCategoryList($job['CategoryId'],$this->app88param->get88AppGuid());
		if ($parent_categories === FAlSE) $parent_categories = array();
		
		$view_params['parent_categories'] = $parent_categories;
		$view_params['category_name'] = $parent_categories[count($parent_categories)-1]['Name'];
		$view_params['category_guid'] = $parent_categories[count($parent_categories)-1]['Guid'];
		$view_params['categories'] = $categories;
		$view_params['job'] = $job;
		
		$view_params['back_url'] = site_url("category/index/".$this->input->get('category_guid'). "/" .$this->input->get('page_no')) .$query_string;
		$this->load->view("front/job/detail",$view_params);
	}
}