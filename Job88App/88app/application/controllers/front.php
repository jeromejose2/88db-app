<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front extends CI_Controller {
	
	public function __construct($config = array())
	{
		parent::__construct();
	}
	
	public function index()
	{
		
		session_start();
		if (isset($_SESSION['app_param'][$_GET["cid"]]))
		{
			$this->app88param->setParam($_SESSION['app_param'][$_GET["cid"]]);
			$this->app88param->selectInstance($_GET["cid"]);
			unset($_SESSION['app_param'][$_GET["cid"]]);
		}

		$appdata = json_decode(urldecode($this->app88param->getByKey('appdata')));
		if ($appdata->redirect != '')
		{
			$this->app88param->setByKey('appdata','');
			redirect($appdata->redirect);
		}
		
		$apd = json_decode(urldecode($this->app88param->getByKey('apd')));
		if ($apd->r != '')
		{
			$this->app88param->setByKey('apd','');
			redirect($apd->r);
		}
		else
		{
			redirect("/category/index");
		}
		
	}
	
	public function safari()
	{
		session_start();
		header('Location: '.urldecode($_GET['url']));
	}
	
}