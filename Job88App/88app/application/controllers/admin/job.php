<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Job extends CI_Controller
{
	private $_EMP_TYPES;
	private $_app;
	
	public function __construct($config = array())
	{
		parent::__construct();
		$this->lang->load('admin', $this->app88common->get_shop_lang());
		$this->load->library('app88Authen');
		$this->load->model('job_model');
		$this->load->model('app_model');
		$this->load->model('category_model');

		$this->_EMP_TYPES = unserialize(APP88_EMPLOYMENT_TYPE);
		
		
		if (!$this->app88userinfo->getMemberId() || !$this->app88param->get88AppGuid())
		{
			show_error($this->lang->line('err_session_expired'));
		}
		if (!$this->app88userinfo->isShopOwner())
		{
			redirect('category/index');
		}
		
		$this->_app = $this->app_model->getAppByAppGuid($this->app88param->get88AppGuid());
	}
	
	public function index($page = 1)
	{
		$limit = $this->config->item('admin_jobs_list_item_per_page');
		$offset = ($page - 1) * $limit;
		
		$keyword = '';
		if ($this->input->post()) $keyword = $this->input->post('keyword');
		elseif ($this->input->get('keyword')) $keyword = $this->input->get('keyword');
		
		if (!empty($keyword))
		{
			$keyword = trim($keyword);
		}
		
		
		$startDate = '';
		if ($this->input->post() && $this->_validateDate($this->input->post('startDate')))
		{
			$startDate = $this->input->post('startDate');
		}
		elseif ($this->input->get('startDate') && $this->_validateDate($this->input->get('startDate')))
		{
			$startDate = $this->input->get('startDate');
		}
		
		$date = DateTime::createFromFormat(APP88_DATE_FORMAT, $startDate);
			
		if ($date !== FALSE)
		{
			$startDate_sys = $date->format(str_replace('%', '', $this->config->item('db_system_date_format')));
		}
		
		$endDate = '';
		if ($this->input->post() && $this->_validateDate($this->input->post('endDate')))
		{
			$endDate = $this->input->post('endDate');
		}
		elseif ($this->input->get('endDate') && $this->_validateDate($this->input->get('endDate')))
		{
			$endDate = $this->input->get('endDate');
		}
		
		$date = DateTime::createFromFormat(APP88_DATE_FORMAT, $endDate);
			
		if ($date !== FALSE)
		{
			$endDate_sys = $date->format(str_replace('%', '', $this->config->item('db_system_date_format')));
		}
		
		$rs = $this->job_model->listJobByAppGuid(
													$this->app88param->get88AppGuid(), 
													$limit, 
													$offset, 
													$keyword, 
													$startDate_sys, 
													$endDate_sys, 
													-1, 
													-1,
													-1
												);
		
		if ($keyword)
		{
			$view_param['keyword'] = $keyword;
		}
		
		if ($startDate)
		{
			$view_param['startDate'] = $startDate;
		}
		
		if ($endDate)
		{
			$view_param['endDate'] = $endDate;
		}
		
		if(isset($view_param['keyword']))
		{
			$view_param['title'] = sprintf($this->lang->line('admin_page_title_job_keyword'), form_prep($view_param['keyword']));
		}
		else
		{
			$view_param['title'] = $this->lang->line('admin_page_title_job');
		}
		
		$view_param['support_sag'] = $this->_app['SupportSAG'] && APP88_SAG_SUPPORT;
		$view_param['jobs'] = $rs['rs'];
		$view_param['total_records'] = $rs['total_records'];
		$view_param['page'] = $page;
		$view_param['form_action_url'] = site_url("admin/job/index");
		$view_param['max_page'] = ceil($rs['total_records'] / $this->config->item('admin_jobs_list_item_per_page'));
		
		if ($this->session->userdata('action_complete_message'))
		{
			$view_param['info'] = $this->session->userdata('action_complete_message');
			$this->session->unset_userdata('action_complete_message');
		}
		
		$view_param['active_jobs_num'] = $this->job_model->countActiveJobsByAppGuid($this->app88param->get88AppGuid());
		$view_param['ad_quota'] = $this->_app['AdQuota'];
		
		$this->load->view('admin/job/list',$view_param);
	}
	
	public function add($cat_guid = '')
	{
		if ($this->input->post())
		{
			$fieldErrs = array();
			$error = $this->_validateForm('add', $this->app88param->get88AppGuid(), $fieldErrs);
			
			$data = $this->input->post();
			
			if (sizeof($error) > 0)
			{
				$category_list = $this->category_model->listCategoryByAppGuid(
															$this->app88param->get88AppGuid(),
															Common::CATEGORY_INCLUDE_SUBCATEGORY
														);
				
				if ($category_list === FALSE) $category_list = array();
				
				$view_param['support_sag'] = $this->_app['SupportSAG'] && APP88_SAG_SUPPORT;
				$view_param['category_list'] = $category_list;
				$view_param['title'] = $this->lang->line('add_job_label');
				$view_param['form_action'] = site_url('admin/job/add');
				$view_param['error'] = $error;
				$view_param['field_errs'] = $fieldErrs;
				$data['job_desc'] = html_entity_decode($data['job_desc']);
				$view_param['form_data'] = $data;
				$view_param['employ_types'] = $this->_EMP_TYPES;
				$view_param['mode'] = 'add';
				
				$this->load->view('admin/job/edit',$view_param);
			}
			else
			{
				$data['guid'] = $this->guid->newGuid()->toString();
				$data['job_desc'] = html_entity_decode($this->input->post('job_desc'));
				$date = DateTime::createFromFormat(APP88_DATE_FORMAT, $data['post_date']);
				$data['job_negot'] = $this->input->post('job_negot')? 1 : 0;
				$data['post_date'] = $date->format(str_replace('%', '', $this->config->item('db_system_date_format')));
				
				if (!$this->job_model->addJob($data))
				{
					show_error($this->lang->line('err_unable_add_record'));
				}
				else
				{				
					$this->session->set_userdata('action_complete_message', sprintf($this->lang->line('admin_addjob_complete_msg'),$data['job_title']));
					
					redirect('admin/job/index');
				}
			}
		}
		else
		{
			$category_list = $this->category_model->listCategoryByAppGuid(
														$this->app88param->get88AppGuid(),
														Common::CATEGORY_INCLUDE_SUBCATEGORY
													);
			
			if ($category_list === FALSE) $category_list = array();
			
			$form_data['job_title'] = '';
			$form_data['job_refid'] = '';
			$form_data['job_desc'] = '';
			$form_data['post_date'] = $this->app88common->local_date('',APP88_DATE_FORMAT);
			$form_data['job_publish'] = '0';
			$form_data['job_salary'] = '';
			$form_data['job_negot'] = '0';
			$form_data['job_emply_type'] = '';
			$form_data['contact_person'] = '';
			$form_data['job_recips'] = '';
			$form_data['job_contact'] = '';
			$form_data['category_guid'] = $cat_guid;

			$view_param['support_sag'] = $this->_app['SupportSAG'] && APP88_SAG_SUPPORT;
			$view_param['category_list'] = $category_list;
			$view_param['title'] = $this->lang->line('add_job_label');
			$view_param['form_action'] = site_url('/admin/job/add');
			$view_param['form_data'] = $form_data;
			
			$view_param['employ_types'] = $this->_EMP_TYPES;
			$view_param['mode'] = 'add';
			$this->load->view('admin/job/edit',$view_param);
		}
	}
	
	public function edit($guid = '')
	{
		if (!$this->_validateJob($guid))
		{
			redirect('admin/job/index');
		}
		
		$job = $this->job_model->getJobByGuid($guid);
		
		if (empty($job) || ($job['AppGuid'] != $this->app88param->get88AppGuid()))
		{
			redirect('admin/job/index');
		}
		
		if ($this->input->post())
		{
			$fieldErrs = array();
			$error = $this->_validateForm('edit', $this->app88param->get88AppGuid(), $fieldErrs);
			
			$data = $this->input->post();
			
			if (sizeof($error) > 0)
			{
				
				$category_list = $this->category_model->listCategoryByAppGuid(
															$this->app88param->get88AppGuid(),
															Common::CATEGORY_INCLUDE_SUBCATEGORY
														);
				
				if ($category_list === FALSE) $category_list = array();
				
				$view_param['support_sag'] = $this->_app['SupportSAG'] && APP88_SAG_SUPPORT;
				$view_param['category_list'] = $category_list;
				$view_param['title'] = $this->lang->line('admin_page_title_editjob');
				$view_param['form_action'] = site_url('/admin/job/edit/'.$guid);
				$view_param['error'] = $error;
				$view_param['field_errs'] = $fieldErrs;
				$data['job_desc'] = html_entity_decode($data['job_desc']);
				$view_param['form_data'] = $data;
				$view_param['employ_types'] = $this->_EMP_TYPES;
				$view_param['mode'] = 'edit';
				$this->load->view('admin/job/edit',$view_param);
			}
			else
			{
				$data = $this->input->post();
				$data['job_desc'] = html_entity_decode($this->input->post('job_desc'));
				$date = DateTime::createFromFormat(APP88_DATE_FORMAT, $data['post_date']);
				$data['post_date'] = $date->format(str_replace('%', '', $this->config->item('db_system_date_format')));
				$data['job_negot'] = $this->input->post('job_negot')? 1 : 0;
				
				if (!$this->job_model->updateJob($guid, $data))
				{					
					show_error($this->lang->line('err_unable_update_record'));
				}
				else
				{
					$this->session->set_userdata('action_complete_message', sprintf($this->lang->line('admin_updatejob_complete_msg'),$this->input->post('job_title')));
					
					redirect('admin/job/index');
				}
			}
		}
		else
		{
		
			$category = $this->category_model->getCategoryById($job['CategoryId']);
			
			if (empty($category) || ($category['AppGuid'] != $this->app88param->get88AppGuid()))
			{
				show_error($this->lang->line('err_invalid_param'));
			}
			
			$category_list = $this->category_model->listCategoryByAppGuid(
														$this->app88param->get88AppGuid(),
														Common::CATEGORY_INCLUDE_SUBCATEGORY
													);
			
			if ($category_list === FALSE) $category_list = array();
			
			
			$form_data['job_guid'] = $guid;
			$form_data['category_id'] = $job['CategoryId'];
			$form_data['job_title'] = $job['Title'];
			$form_data['job_contact'] = $job['Contact'];
			$form_data['job_refid'] = $job['ReferenceId'];
			$form_data['job_negot'] = $job['Negotiable'];
			$form_data['job_emply_type'] = $job['EmploymentType'];
			$form_data['contact_person'] = $job['ContactPerson'];
			$form_data['job_contact'] = $job['Contact'];
			$form_data['job_recips'] = $job['Recipient'];
			$form_data['job_salary'] = $this->common->number_format($job['Salary'], FALSE, TRUE);
			$form_data['post_date'] = $job['PostDateFormatted'];
			$form_data['job_desc'] = $job['Description'];
			$form_data['job_publish'] = $job['Published'];
		
			$view_param['support_sag'] = $this->_app['SupportSAG'] && APP88_SAG_SUPPORT;
			$view_param['category_list'] = $category_list;
			$view_param['title'] = $this->lang->line('admin_page_title_editjob');
			$view_param['form_action'] = site_url('/admin/job/edit/'.$guid);
			$view_param['form_data'] = $form_data;
			$view_param['employ_types'] = $this->_EMP_TYPES;
			$view_param['mode'] = 'edit';
			$this->load->view('admin/job/edit',$view_param);
		}
	}
	
	public function hideshow($guid = '', $page = 1)
	{
		if (empty($guid))
		{
			redirect('admin/job/index');
		}
		
		$job = $this->job_model->getJobByGuid($guid);
		
		if (empty($job) || ($job['AppGuid'] != $this->app88param->get88AppGuid()))
		{
			show_error($this->lang->line('err_invalid_param'));
		}
		
		$data['job_publish'] = ($job['Published'] == 1)? 0 : 1;
		$data['category_id'] = $job['CategoryId'];
		$data['job_title'] = $job['Title'];
		$data['job_desc'] = $job['Description'];
		$data['job_salary'] = $job['Salary'];
		$data['job_negot'] = $job['Negotiable'];
		$data['job_emply_type'] = $job['EmploymentType'];
		$data['contact_person'] = $job['ContactPerson'];
		$data['job_recips'] = $job['Recipient'];
		$data['job_contact'] = $job['Contact'];
		$data['post_date'] = $job['PostDate'];
		
		if (!$this->job_model->updateJob($guid, $data))
		{
			show_error($this->lang->line('err_unable_update_record'));
		}
		else
		{
			$this->session->set_userdata('action_complete_message', sprintf($this->lang->line('admin_updatejob_complete_msg'),$data['job_title']));
			
			redirect('admin/job/index/'. $page);
		}
	}
	
	public function delete($guid, $page = 1)
	{
		if (!$this->_validateJob($guid))
		{
			redirect('admin/job/index');
		}
		
		$job = $this->job_model->getJobByGuid($guid);
		
		if (empty($job) || ($job['AppGuid'] != $this->app88param->get88AppGuid()))
		{
			show_error($this->lang->line('err_invalid_param'));
		}
		
		if ($this->job_model->deleteJobByGuid($guid))
		{
			$this->session->set_userdata('action_complete_message', sprintf($this->lang->line('admin_deljob_complete_msg',$job['Title'])));
			
			redirect('admin/job/index/'.$page);
		}
		else
		{
			show_error($this->lang->line('err_no_record'));
		}
	}
	
	private function _validateDate($dateValue)
	{
		return DateTime::createFromFormat(APP88_DATE_FORMAT, $dateValue) !== FALSE;
	}
	
	private function _validateForm($mode = '', $app_guid = '', &$fieldErrs)
	{
		$error = array();
		
		if (APP88_CURRENCY_DECIMALS > 0)
		{
			$price_regex = "/^[0-9]+(\\". APP88_CURRENCY_DECIMAL_POINTS ."[0-9]{". APP88_CURRENCY_DECIMALS ."})?$/";
		}
		else
		{
			$price_regex = "/^[0-9]+$/";
		}
		
		if (trim($this->input->post('job_title')) == '')
		{
			$error[] = $this->lang->line('err_job_title_req');
			$fieldErrs['job_title'][] = $this->lang->line('err_job_title_req');
		}
		if (mb_strlen(trim($this->input->post('job_title')),'UTF-8') > APP88_JOB_TITLE_MAX_LENGTH)
		{
			$error[] = sprintf($this->lang->line('err_job_title_too_long'), APP88_JOB_TITLE_MAX_LENGTH);
			$fieldErrs['job_title'][] = sprintf($this->lang->line('err_job_title_too_long'), APP88_JOB_TITLE_MAX_LENGTH);
		}
		
		if ('add' == $mode)
		{
			$trimed = trim($this->input->post('job_refid'));
			if ($trimed == '')
			{
				$error[] = $this->lang->line('err_job_refid_req');
				$fieldErrs['job_refid'][] = $this->lang->line('err_job_refid_req');
			}
			else if (!$this->_validateString($trimed))
			{
				$error[] = $this->lang->line('err_job_refid_char_invalid');
				$fieldErrs['job_refid'][] = $this->lang->line('err_job_refid_char_invalid');
			}
			else if (!$this->job_model->isReferenceIdNonExist($app_guid, $trimed))
			{
				$error[] = $this->lang->line('err_job_refid_exist');
				$fieldErrs['job_refid'][] = $this->lang->line('err_job_refid_exist');
			}
		}
		
		
		if (trim($this->input->post('job_negot')) != '1' && trim($this->input->post('job_salary')) == '')
		{
			$error[] = $this->lang->line('err_negotiable_req');
			$fieldErrs['job_salary'][] = $this->lang->line('err_negotiable_req');
		}
		if (trim($this->input->post('job_salary')) != '' && !preg_match($price_regex, trim($this->input->post('job_salary'))))
		{
			$error[] = $this->lang->line('err_salary_invalid');
			$fieldErrs['job_salary'][] = $this->lang->line('err_salary_invalid');
		}
		
		if (trim($this->input->post('job_emply_type')) == '')
		{
			$error[] = $this->lang->line('err_employment_type_req');
			$fieldErrs['job_emply_type'][] = $this->lang->line('err_employment_type_req');
		}
		elseif (!in_array(trim($this->input->post('job_emply_type')), $this->_EMP_TYPES))
		{
			$error[] = $this->lang->line('err_employment_type_invalid');
			$fieldErrs['job_emply_type'][] = $this->lang->line('err_employment_type_invalid');
		}
		
		$trimedContactNo = trim($this->input->post('job_contact'));
		if ($trimedContactNo == '')
		{
			$error[] = $this->lang->line('err_contact_req');
			$fieldErrs['job_contact'][] = $this->lang->line('err_contact_req');
		}
		elseif ( !$this->_isDigitOnly($trimedContactNo) )
		{
			$error[] = $this->lang->line('err_contact_non_digit');
			$fieldErrs['job_contact'][] = $this->lang->line('err_contact_non_digit');
		}
		
		if (APPLICATION_EMAIL_ENABLED == 1)
		{
			if (trim($this->input->post('job_recips')) == '')
			{
				$error[] = $this->lang->line('err_recipient_req');
				$fieldErrs['job_recips'][] = $this->lang->line('err_recipient_req');
			}
			elseif (!filter_var(trim($this->input->post('job_recips')), FILTER_VALIDATE_EMAIL))
			{
				$error[] = $this->lang->line('err_recipient_invalid');
				$fieldErrs['job_recips'][] = $this->lang->line('err_recipient_invalid');
			}
		}
		
		if (trim($this->input->post('post_date')) == '')
		{
			$error['post_date'][] = $this->lang->line('err_post_date_req');
		}
		elseif (!$this->_validateDate($this->input->post('post_date')))
		{
			$error['post_date'][] = $this->lang->line('err_display_time_invalid');
		}
		
		if (trim($this->input->post('category_id')) == '')
		{
			$error['category_id'][] = $this->lang->line('err_parentcat_req');
		}
		
		if (trim(str_replace("&nbsp;", "", strip_tags(html_entity_decode($this->input->post('job_desc')),'<img>'))) == '')
		{
			$error[] = $this->lang->line('err_job_des_req');
			$fieldErrs['job_desc'][] = $this->lang->line('err_job_des_req');
		}
		
		if (trim($this->input->post('contact_person')) == '')
		{
			$error[] = $this->lang->line('err_contact_person_req');
			$fieldErrs['contact_person'][] = $this->lang->line('err_contact_person_req');
		}
		
		return $error;
	}
	
	private function _validateString($string)
	{
		if (!$string)
		{
			return FALSE;
		}
		else
		{
			$sanitized = preg_replace("/[a-zA-Z0-9]+/", "", $string);
			if (empty($sanitized))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
	}
	
	private function _isDigitOnly($string)
	{
		return !preg_match ("/[^0-9]/", $string);
	}
	
	private function _validateJob($guid)
	{
		if ($guid == '')
		{
			return FALSE;
		}
		else
		{
			return $this->job_model->isJobBelongToShop($guid, $this->app88param->get88AppGuid());
		}
	}
}
