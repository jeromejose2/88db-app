<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App extends CI_Controller {
	
	public function __construct($config = array())
	{
		parent::__construct();
		$this->lang->load('admin', $this->app88common->get_shop_lang());
		$this->load->library('app88Authen');
		$this->load->model('app_model');
		$this->load->model('category_model');
	}
	
	public function entry()
	{
		session_start();
		if (isset($_SESSION['app_param'][$_GET['cid']]))
		{
			$this->app88param->setParam($_SESSION['app_param'][$_GET['cid']]);
			unset($_SESSION['app_param'][$_GET['cid']]);
		}
		
		$state = json_encode(array('cid'=>$_GET['cid'], 'mode'=>$_GET['mode']));
		
		$authen_handler = 'admin/app/checkOwner_handler';
		
		$app = $this->app_model->getAppByAppGuid($_GET['cid']);
		
		if ((APP88_SAG_SUPPORT == '1') && $app && $app['SupportSAG']) 
		{
			$this->app88authen->authen($authen_handler, $state, 'user_info, email_user, create_post');
		}
		else
		{
			$this->app88authen->authen($authen_handler, $state);
		}
	}
	
	public function checkOwner_handler()
	{
		$error = $this->input->get('error', TRUE);
		$code = $this->input->get('code', TRUE);
		$state = $this->input->get('state', TRUE);
		
		if (!empty($error))
		{
			// User Denied
			show_error($error);
		}
		else if (!empty($code))
		{
			$redirect = 'admin/app/checkOwner_handler';
			$access_token = $this->app88authen->getAccessToken($code, $redirect);
			$user_info = $this->app88authen->getUserInfo($access_token);

			$this->app88userinfo->setUserInfo(json_decode($user_info));
			
			$state_obj = json_decode($state);
			
			$this->app88param->selectInstance($state_obj->cid);
			
			$app = $this->app_model->getAppByAppGuid($this->app88param->get88AppGuid());
				
			if ((APP88_SAG_SUPPORT == '1') && $app && $app['SupportSAG'])
			{
				$db88_access_token = $this->app88authen->getDB88AccessToken($access_token);
				$this->app88param->setAccessToken($db88_access_token);
			}
			
			if ($this->app88param->isEditMode() && $this->app88userinfo->isShopOwner())
			{
				redirect ('admin/app/index?cid='.$this->app88param->get88AppGuid());
			}
			else
			{
				redirect ('front/index?cid='.$this->app88param->get88AppGuid());
			}
		}
	}
	
	public function index()
	{
		
		if (!$this->app88userinfo->getMemberId() || !$this->app88param->get88AppGuid())
		{
			show_error($this->lang->line('err_session_expired'));
		}
		
		if (!$this->app88userinfo->isShopOwner())
		{
			redirect ('front/index');
		}
		
		
		$app = $this->_getApp($this->app88param->get88AppGuid(), $this->app88userinfo->getMemberId());
				
		if (empty($app))
		{
			show_error($this->lang->line('err_unable_load_app'));
		}
		
		if ($this->category_model->listCategoryByAppGuid($app['AppGuid'])==FALSE)
		{
			redirect('admin/category/index');
		}
		else
		{
			redirect('admin/job/index');
		}
	}
	
	private function _getApp($cid, $owner_id) {
		$app = $this->app_model->getAppByAppGuid($cid);
		if ($app)
		{
			return $app;
		}
		else
		{
			$data = array();
			$data['app_name'] = $this->app88userinfo->getShopName();
			return $this->app_model->createApp($cid, $owner_id, $data);
		}
	}
}