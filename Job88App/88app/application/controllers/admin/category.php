<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends CI_Controller
{
	private $_app;
	
	
	public function __construct($config = array())
	{
		parent::__construct();
		$this->lang->load('admin', $this->app88common->get_shop_lang());
		$this->load->library('app88Authen');
		$this->load->model('category_model');
		$this->load->model('job_model');
		$this->load->model('app_model');
		
		if (!$this->app88userinfo->getMemberId() || !$this->app88param->get88AppGuid())
		{
			show_error($this->lang->line('err_session_expired'));
		}
		
		if (!$this->app88userinfo->isShopOwner())
		{
			redirect ('category/index');
		}
		
		$this->_app = $this->app_model->getAppByAppGuid($this->app88param->get88AppGuid());
	}
	
	
	public function index()
	{
		if($this->input->post("search_keyword"))
		{
			$category_array = $this->category_model->listCategoryByAppGuid(
														$this->app88param->get88AppGuid(),
														Common::CATEGORY_INCLUDE_NOTHING,
														$this->input->post("search_keyword")
													);
			
			$view_params['keyword'] = $this->input->post("search_keyword");
		}
		else
		{
			$category_array = $this->category_model->listCategoryByAppGuid(
														$this->app88param->get88AppGuid(),
														Common::CATEGORY_INCLUDE_SUBCATEGORY|Common::CATEGORY_INCLUDE_JOB
													);
		}
		
		if ($category_array === FALSE) $category_array = array();
		
		if($view_params['keyword'])
		{
			$view_params['page_title'] = sprintf(
											$this->lang->line('admin_page_title_category_keyword'), 
											form_prep($view_params['keyword'])
										);
		}
		else
		{
			$view_params['page_title'] = $this->lang->line('admin_page_title_category');
		}
		
		$view_params['support_sag'] = $this->_app['SupportSAG'] && APP88_SAG_SUPPORT;
		$view_params['category_array'] = $category_array;
		$view_params['category_sub_level'] = $this->config->item('category_sub_level');
		$view_params['form_action_url'] = site_url("admin/category/index");
		$view_params['info'] = $this->session->userdata('action_complete_message');
		$view_params['error'] = $this->session->userdata('action_error_message');
		
		$this->load->view('admin/category/list',$view_params);
		
		$array_to_clears = array('action_complete_message' => '', 'action_error_message' => '');
		$this->session->unset_userdata($array_to_clears);
	}
	
	
	public function add($parent_guid = '')
	{
		$parent_category = array();
		
		$parent_category = $this->category_model->getCategoryByGuid($parent_guid);
		
		if ((($parent_guid !== '') && !$parent_category) || (($parent_category != FALSE) && ($parent_category['Status'] == 0)))
		{
			redirect('admin/category/index');
		}
		
		$view_params = array();
		$view_params['support_sag'] = $this->_app['SupportSAG'] && APP88_SAG_SUPPORT;
		$form_data = $this->input->post();
		
		if (!$form_data)
		{
			if ($parent_guid)
			{
				$view_params['page_title'] = $this->lang->line('btn_add_sub_category');
			}
			else
			{
				$view_params['page_title'] = $this->lang->line('admin_page_title_addcategory');
			}
			
			$view_params['form_action_url'] = site_url("admin/category/add/".$parent_guid);
			$this->load->view('admin/category/add',$view_params);
		}
		else
		{
			$error = array();
			$fieldErrs = array();
			$error = $this->_validateForm('add', $form_data, $fieldErrs);
			
			if (sizeof($error) > 0)
			{
				$view_params['form_action_url'] = site_url("admin/category/add/".$parent_guid);
				
				if ($parent_guid)
				{
					$view_params['page_title'] = $this->lang->line('btn_add_sub_category');
				}
				else
				{
					$view_params['page_title'] = $this->lang->line('admin_page_title_addcategory');
				}
				
				$view_params['error'] = $error;
				$view_params['field_errs'] = $fieldErrs;
				$view_params['form_data'] = $form_data;
				
				$this->load->view('admin/category/add', $view_params);
			}
			else
			{
				
				if ($parent_guid)
				{
					$form_data['parent_id'] = $parent_category['Id'];
					$form_data['app_guid'] = $this->app88param->get88AppGuid();
					$this->category_model->addCategory($form_data);
				}
				else
				{
					$form_data['parent_id'] = 0;
					$form_data['app_guid'] = $this->app88param->get88AppGuid();
					$this->category_model->addCategory($form_data);
				}
				
				$this->session->set_userdata("action_complete_message",
						sprintf($this->lang->line('admin_addcat_complete_msg'),$form_data['category_name']));
				
				redirect("admin/category/index");
			}
		}
	}
	

	public function edit($guid) 
	{
		if (!$this->_validateCategory($guid))
		{
			redirect('admin/category/index');
		}
		
		$category_info = $this->category_model->getCategoryByGuid($guid);
		
		if ($category_info === FALSE || $category_info['Status'] == 0) 
		{
			redirect("admin/category/index");
		}
		
		$view_params = array();
		$form_data = $this->input->post();
		$view_params['page_title'] = $this->lang->line('admin_page_title_editcategory');
		$view_params['support_sag'] = $this->_app['SupportSAG'] && APP88_SAG_SUPPORT;
		
		if ($form_data === FALSE)
		{
			$category_list = $this->category_model->listCategoryByAppGuid(
														$this->app88param->get88AppGuid(),
														Common::CATEGORY_INCLUDE_SUBCATEGORY
													);
			
			if ($category_list === FALSE) $category_list = array();
			
			$view_params['category_info'] = $category_info;
			$view_params['category_list'] = $category_list;
			$view_params['form_action_url'] = site_url("admin/category/edit/".$guid);
			
			$this->load->view('admin/category/edit',$view_params);
		}
		else
		{
			$error = array();
			$fieldErrs = array();
			$form_data['parent_category'] = 0;
			$error = $this->_validateForm('edit', $form_data, $fieldErrs);
				
			if (sizeof($error) > 0)
			{
				$view_params['form_action_url'] = site_url("admin/category/edit/".$guid);
				$view_params['error'] = $error;
				$view_params['field_errs'] = $fieldErrs;
				
				$category_list = $this->category_model->listCategoryByAppGuid(
															$this->app88param->get88AppGuid(),
															Common::CATEGORY_INCLUDE_SUBCATEGORY
														);
				
				if ($category_list === FALSE) $category_list = array();
				
				$view_params['form_data'] = $form_data;
				$view_params['category_info'] = $category_info;
				$view_params['category_list'] = $category_list;
			
				$this->load->view('admin/category/edit',$view_params);
			}
			else
			{	
				$parent_id = 0;
				
				if ($form_data['parent_category'] != 0)
				{
					$parent_info = $this->category_model->getCategoryByGuid($form_data['parent_category']);
					$parent_id = $parent_info['Id'];
				}
				
				$parent_id = $category_info['ParentId'];
				
				$new_category_info = array(
						'Name' => $form_data['category_name'],
						'ParentId' => $parent_id,
						'Published' => $form_data['published']
				);
				 
				$this->category_model->updateCategoryById($category_info['Id'],$new_category_info);
				
				$this->session->set_userdata("action_complete_message",
						sprintf($this->lang->line('admin_editcat_complete_msg'),$form_data['category_name']));
				
				redirect("admin/category/index");		
			}
		}
	}
	
	
	public function delete($guid='')
	{
		if (!$this->_validateCategory($guid))
		{
			redirect('admin/category/index');
		}
		
		$has_job = $this->job_model->listJobByCategoryGuid($guid, 1); 
		$has_subcategory = $this->category_model->getCategoryByGuid($guid, Common::CATEGORY_INCLUDE_SUBCATEGORY);
		
		if ($has_job === FALSE && empty($has_subcategory['Subcategory']))
		{
			$category_name = $this->category_model->deleteCategory($guid);
			$this->session->set_userdata("action_complete_message",
					sprintf($this->lang->line('admin_deletecat_complete_msg'),$category_name));
		}
	}
	
	
	public function sort()
	{
		$cat = $this->input->post('cat');
		if (sizeof($cat) > 1)
		{
			
			if (!$this->category_model->updateSortOrder($cat))
			{
				echo "FAIL";
				return;
			}
				
		}
		echo "SUCCESS";
		return;
	}
	
	
	private function _validateForm($mode, $form_data, &$fieldErrs)
	{
		if (trim($form_data['category_name']) == '')
		{
			$error[] = $this->lang->line('err_category_name_req');
			$fieldErrs['category_name'][] = $this->lang->line('err_category_name_req');
		}
		if (mb_strlen(trim($form_data['category_name']),'UTF-8') > APP88_CATEGORY_NAME_MAX_LENGTH)
		{
			$error[] = sprintf($this->lang->line('err_category_name_too_long'),APP88_CATEGORY_NAME_MAX_LENGTH);
			$fieldErrs['category_name'][] = sprintf($this->lang->line('err_category_name_too_long'),APP88_CATEGORY_NAME_MAX_LENGTH);
		}
		if (stripos(trim($form_data['category_name']),"\\") !== FALSE)
		{
			$error[] = $this->lang->line('err_category_name_backslash');
			$fieldErrs['category_name'][] = $this->lang->line('err_category_name_backslash');
		}
		if (trim($form_data['published']) == '')
		{
			$error[] = $this->lang->line('err_published_req');
			$fieldErrs['published'][] = $this->lang->line('err_published_req');
		}
		if ($mode=='edit' && trim($form_data['parent_category']) == '')
		{
			$error[] = $this->lang->line('err_parentcat_req');
			$fieldErrs['parent_category'][] = $this->lang->line('err_parentcat_req');
		}
		return $error;
	}
	
	
	private function _validateCategory($guid)
	{
		if ($guid == '')
		{
			return FALSE;
		}
		else
		{
			return $this->category_model->isCategoryBelongToShop($guid, $this->app88param->get88AppGuid());
		}
	}
}
	