<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shoot extends CI_Controller
{
	private $app;
	
	
	public function __construct($config = array())
	{
		parent::__construct();
		
		$this->lang->load('admin', $this->app88common->get_shop_lang());
		
		$this->load->model('job_model');
		$this->load->model('sag_model');
		$this->load->model('category_model');
		$this->load->model('app_model');

		$this->load->library('app88Authen');
		$this->load->library('sag');
	}
	
	
	private function _validateOwner()
	{
		if (!$this->app88userinfo->getMemberId() || !$this->app88param->get88AppGuid())
		{
			show_error($this->lang->line('err_session_expired'));
		}
		
		if (!$this->app88userinfo->isShopOwner())
		{
			redirect('front/index');
		}
		
		$this->app = $this->app_model->getAppByAppGuid($this->app88param->get88AppGuid());
	}
	
	
	public function notifyStatus($shootStatusGuid, $verificationCode)
	{
		$this->app88log->log_message('sag', 'ERROR', '[LOG] SAG Task Id:'.$shootStatusGuid.', Status returned: '.$this->input->get('status'));
		
		if ($this->input->get('status') === null)
		{
			show_error('status parameter not found');
		}
	
		$shootAdStatus = $this->sag_model->getShootAdStatusByGuid($shootStatusGuid);
		
		if ($shootAdStatus == null)
		{
			$this->app88log->log_message('sag', 'ERROR', '[LOG] SAG Task Id:'.$shootStatusGuid.', Invalid Status Guid');
			show_error('invalid status guid');
		}
		
		if ($shootAdStatus['VerificationCode'] != $verificationCode)
		{
			$this->app88log->log_message('sag', 'ERROR', '[LOG] SAG Task Id:'.$shootStatusGuid.', Invalid Verification Code');
			show_error('invalid verification code');
		}
	
		$statusJson = $this->input->get('status');
		$status = (array)json_decode($statusJson);
		
		switch ($status['status'])
		{
			case -1:
				$statusCode = 'E';
				break;
			case 0:
				$statusCode = 'S';
				break;
			case 1:
				$statusCode = 'P';
				break;
			default:
				$this->app88log->log_message('sag', 'ERROR', '[LOG] SAG Task Id:'.$shootStatusGuid.', Invalid Status Code notified: '.$status['status'].' '.$status['message']);
				show_error('invalid status code notified');
		}
	
		$this->sag_model->updateShootAdStatusByGuid($shootStatusGuid, $statusCode, $status);
	}
	
	
	private function processNewRecord($guid, $shootChannel, $quota = '')
	{
		$sag_shoot_targets = $this->_getSAGChannels(TRUE);
	
		list($channelId, $targetId) = explode('@', $shootChannel);
		
		$channel = array();
		$target = $sag_shoot_targets[$targetId];
		$targetName = $target['name'];
		$channel = $target['channels'][$channelId];
		$channel['targetId'] = $targetId;
		$channel['targetName'] = $targetName;
		$channel['quota'] = $quota;
		$channel['id'] = $channelId;
		//$channels[]= $channel;
		
		$job = $this->job_model->getJobByGuid($guid);
		
		if (empty($job) || ($job['AppGuid'] != $this->app88param->get88AppGuid()))
		{
			show_error($this->lang->line('err_invalid_param'));
		}
		
		$shootStatusGuid = $this->guid->newGuid()->toString();
		$verficationCode = $this->guid->newGuid()->toString();
		
		//TODO: consider combine controller, callback can not be authen
		$callbackUrl = site_url('/admin/shoot/notifyStatus/'.$shootStatusGuid.'/'.$verficationCode);
		
		$data_mapping = array();

		//TODO:: mapping need to revised
		foreach ($channel['mapping'] as $mapping) 
		{
			switch ($mapping)
			{
				case 'jobTitle':
					$data_mapping[$mapping] = $job['Title'];
					break;
				case 'jobDesc':
					$data_mapping[$mapping] = $job['Description'];
					break;
				case 'contactName': 
					$data_mapping[$mapping] = $job['ContactPerson'];
					break;
				case 'phone': 
					$data_mapping[$mapping] = $job['Contact'];
					break;
				case 'email': 
					$data_mapping[$mapping] = $job['Recipient'];
					break;
				case 'salary': 
					$data_mapping[$mapping] = $job['Salary']? $job['Salary'] : '';
					if ($job['Negotiable'] == '1') $data_mapping[$mapping] .= ' '.$this->lang->line('admin_view_job_negot_label');
					break;
				case 'companyAddr':
					$data_mapping[$mapping] = '';
					break;
				case 'jobType':
					if ($job['EmploymentType'] == 'FULLTIME')
					{
						$data_mapping[$mapping] = 'FULLTIME';
					}
					elseif ($job['EmploymentType'] == 'PARTTIME')
					{
						$data_mapping[$mapping] = 'PARTTIME';
					}
					elseif ($job['EmploymentType'] == 'TEMPORARY')
					{
						$data_mapping[$mapping] = 'TEMP';
					}
					elseif ($job['EmploymentType'] == 'CONTRACT')
					{
						$data_mapping[$mapping] = 'CONTRACT';
					}
					elseif ($job['EmploymentType'] == 'FREELANCE')
					{
						$data_mapping[$mapping] = 'FREELANCE';
					}
					elseif ($job['EmploymentType'] == 'SUMMER_JOB')
					{
						$data_mapping[$mapping] = 'SUMMER';
					}
					elseif ($job['EmploymentType'] == 'WORK_AT_HOME')
					{
						$data_mapping[$mapping] = 'HOME';
					}
					elseif ($job['EmploymentType'] == 'NOT_SPECIFIED')
					{
						$data_mapping[$mapping] = 'NOT_SPECIFIED';
					}
					else
					{
						$data_mapping[$mapping] = '';
					}
					break;
				case 'salaryRange':
					if (COUNTRY == 'hk')
					{
						if ($job['Negotiable'] == '1')
						{
							$data_mapping[$mapping] = 'HK999';
						}
						elseif ($job['Salary'] <= 2000)
						{
							$data_mapping[$mapping] = 'HK001';
						}
						elseif ($job['Salary'] <= 4000)
						{
							$data_mapping[$mapping] = 'HK002';
						}
						elseif ($job['Salary'] <= 6000)
						{
							$data_mapping[$mapping] = 'HK003';
						}
						elseif ($job['Salary'] <= 8000)
						{
							$data_mapping[$mapping] = 'HK004';
						}
						elseif ($job['Salary'] <= 10000)
						{
							$data_mapping[$mapping] = 'HK005';
						}
						else
						{
							$data_mapping[$mapping] = 'HK006';
						}
					}
					elseif (COUNTRY == 'cn')
					{
						if ($job['Negotiable'] == '1')
						{
							$data_mapping[$mapping] = 'CN01';
						}
						elseif ($job['Salary'] <= 1000)
						{
							$data_mapping[$mapping] = 'CN02';
						}
						elseif ($job['Salary'] <= 2000)
						{
							$data_mapping[$mapping] = 'CN03';
						}
						elseif ($job['Salary'] <= 3000)
						{
							$data_mapping[$mapping] = 'CN04';
						}
						elseif ($job['Salary'] <= 5000)
						{
							$data_mapping[$mapping] = 'CN05';
						}
						elseif ($job['Salary'] <= 8000)
						{
							$data_mapping[$mapping] = 'CN06';
						}
						elseif ($job['Salary'] <= 12000)
						{
							$data_mapping[$mapping] = 'CN07';
						}
						elseif ($job['Salary'] <= 20000)
						{
							$data_mapping[$mapping] = 'CN08';
						}
						else
						{
							$data_mapping[$mapping] = 'CN09';
						}
					}
					elseif (COUNTRY == 'ph')
					{
						if ($job['Negotiable'] == '1')
						{
							$data_mapping[$mapping] = 'PH05';
						}
						elseif ($job['Salary'] < 10000)
						{
							$data_mapping[$mapping] = 'PH01';
						}
						elseif ($job['Salary'] < 15000)
						{
							$data_mapping[$mapping] = 'PH02';
						}
						elseif ($job['Salary'] < 20000)
						{
							$data_mapping[$mapping] = 'PH03';
						}
						else
						{
							$data_mapping[$mapping] = 'PH04';
						}
					}
					else 
					{
						$data_mapping[$mapping] = '';
					}
					break;
			}
		}
		
		$shootStatusSagTaskId = $this->sag->shoot(
												$channel['channelId'], 
												$channel['topic'], 
												$callbackUrl, 
												$channel['ws_path'], 
												$data_mapping, 
												$channel['quota']);
		
		if ($shootStatusSagTaskId != false)
		{
			$data = array();
			$data['Guid'] = $shootStatusGuid;
			$data['SAGTaskId'] = $shootStatusSagTaskId;
			$data['VerificationCode'] = $verficationCode;
			$data['Status'] = 'P';
			$data['Quota'] = $channel['quota'];
			$data['ProductGuid'] = $guid;
			$data['TargetId'] = $channel['targetId'];
			$data['ChannelId'] = $channel['id'];
			$data['TargetName'] = $channel['targetName'];
			$data['ChannelName'] = $channel['name'];
			$data['PostDate'] = gmdate('Y-m-d', time());
			$this->sag_model->addShootAdStatus($data);
			return true;
		}
		else
		{
			return false;
		}
	
	}
	
	
	public function add($guid)
	{
		$this->_validateOwner();
				
		if (!$this->app['SupportSAG'] || empty($guid))
		{
			redirect('admin/app/index');
		}

		$job = $this->job_model->getJobByGuid($guid);
		if (empty($job) || ($job['AppGuid'] != $this->app88param->get88AppGuid()))
		{
			show_error($this->lang->line('err_invalid_param'));
		}
		
		$category = $this->category_model->getCategoryById($job['CategoryId']);
		if (empty($category) || ($category['AppGuid'] != $this->app88param->get88AppGuid()))
		{
			show_error($this->lang->line('err_invalid_param'));
		}
		
		if ($this->input->post())
		{
			$shootChannel = $this->input->post('shoot_channel');

			if (!isset($shootChannel) || $shootChannel == '')
			{
				$error[]= $this->lang->line('shoot_channel_required_msg');
				
				$view_param['support_sag'] = $this->app['SupportSAG'] && APP88_SAG_SUPPORT;
				$view_param['error'] = $error;
				$view_param['category_guid'] = $category['Guid'];
				$view_param['form_action'] = site_url('/admin/shoot/add/'.$guid);
				$view_param['job_title'] = $job['Title'];
				$view_param['shoot_targets'] = $this->_getSAGChannels();
				
				$this->load->view('admin/shoot/add',$view_param);
			}
			else
			{
				$quota = $this->input->post('quota');
				$sag_result = $this->processNewRecord($guid, $shootChannel, $quota);
				
				if ($sag_result)
				{
					list($channel, $target) = explode('@', $shootChannel);
					
					$shoot_targets = $this->_getSAGChannels();
	
					$view_param['category_guid'] = $category['Guid'];
					$view_param['support_cart'] = $this->app['SupportCart'];
					$view_param['support_sag'] = $this->app['SupportSAG'] && APP88_SAG_SUPPORT;
					$view_param['target'] = $shoot_targets[$target]['name'];
					
					$this->load->view('admin/shoot/success',$view_param);
				}
				else
				{
					$error[]= $this->lang->line('admin_view_sag_error_message');
					
					$view_param['support_sag'] = $this->app['SupportSAG'] && APP88_SAG_SUPPORT;
					$view_param['error'] = $error;
					$view_param['category_guid'] = $category['Guid'];
					$view_param['form_action'] = site_url('/admin/shoot/add/'.$guid);
					$view_param['job_title'] = $job['Title'];
					$view_param['shoot_targets'] = $this->_getSAGChannels();
					
					$this->load->view('admin/shoot/add',$view_param);
				}
			}
		}
		else
		{
			$view_param['support_sag'] = $this->app['SupportSAG'] && APP88_SAG_SUPPORT;
			$view_param['category_guid'] = $category['Guid'];
			$view_param['form_action'] = site_url('/admin/shoot/add/'.$guid);
			$view_param['job_title'] = $job['Title'];
			$view_param['shoot_targets'] = $this->_getSAGChannels();
			
			$this->load->view('admin/shoot/add',$view_param);
		}
	}
	
	
	public function repost($guid)
	{
		$this->_validateOwner();
		
		if (!$this->app['SupportSAG'])
		{
			redirect('admin/app/index');
		}
		
		$status = $this->sag_model->getShootAdStatusByGuid($guid);
		
		if ($status == null)
		{
			show_error($this->lang->line('err_invalid_param'));
		}
		
		if ($status['AppGuid'] != $this->app88param->get88AppGuid())
		{
			show_error($this->lang->line('err_invalid_param'));
		}
		
		$job = $this->job_model->getJobByGuid($status['ProductGuid']);
		if (empty($job) || ($job['AppGuid'] != $this->app88param->get88AppGuid()))
		{
			show_error($this->lang->line('err_invalid_param'));
		}
		
		$shootChannels = array($status['ChannelId'] . '@' . $status['TargetId']);
		$this->processNewRecord($status['ProductGuid'], $shootChannels);

	
		$this->session->set_userdata(
				'action_complete_message', 
				sprintf($this->lang->line('admin_add_sag_complete_msg'),  $job['Title']));
		redirect('admin/shoot/index');
	}
	
	
	public function index($page = 1)
	{
		$this->_validateOwner();
		
		if (!$this->app['SupportSAG'])
		{
			redirect('admin/app/index');
		}
		
		$form_data = array();
		$view_params = array();
		$view_params['page'] = $page;
		
		if ($this->input->get('status_type'))
		{
			$status_type = $this->input->get('status_type');
		}
		else
		{
			$status_type = '';
		}
		
		$form_data['status_type'] = $status_type;
		
		if ($this->input->get('sortfield') != null)
		{
			$sortfield = $this->input->get('sortfield');
			switch($sortfield)
			{
				case 'productname':
					$sortfieldName = 'ProductName';
					break;
				case 'channelname':
					$sortfieldName = 'ChannelName';
					break;
				case 'target':
					$sortfieldName = 'TargetName';
					break;
				case 'postdate':
					$sortfieldName = 'PostDate';
					break;
				case 'status':
					$sortfieldName = 'Status';
					break;
				default:
					$sortfield = 'postdate';
					$sortfieldName = 'PostDate';
					break;
			}
		}
		else
		{
			$sortfield = 'postdate';
			$sortfieldName = 'PostDate';
		}
		
		$form_data['sortfield'] = $sortfield;
		
		if ($this->input->get('sortorder') != null)
		{
			$sortorder = $this->input->get('sortorder');
			$sortorderBool = ($sortorder == 'asc');
		}
		else
		{
			$sortorder = 'desc';
			$sortorderBool = false;
		}
		
		$form_data['sortorder'] = $sortorder;
		
		if ($this->input->get('from'))
		{
			if ( trim($this->input->get('from')) == '' || !$this->_validateDate($this->input->get('from'))  )	
			{
				$error[] = $this->lang->line('err_from_date_invalid');
				$from = null;
			}
			else
			{
				$from = trim($this->input->get('from'));
				
				$date = DateTime::createFromFormat(APP88_DATE_FORMAT, $from);
					
				if ($date !== FALSE)
				{
					$from = $date->format(str_replace('%', '', $this->config->item('db_system_date_format')));
				}
			}
		}
		else
		{
			$from = null;
		}
		
		if ($this->input->get('to'))
		{
			if ( trim($this->input->get('to')) == '' || !$this->_validateDate($this->input->get('to'))  )
			{
				$error[] = $this->lang->line('err_to_date_invalid');
				$to = null;
			}
			else
			{
				$to = trim($this->input->get('to'));
				
				$date = DateTime::createFromFormat(APP88_DATE_FORMAT, $to);
					
				if ($date !== FALSE)
				{
					$to = $date->format(str_replace('%', '', $this->config->item('db_system_date_format')));
				}
			}
		}
		else
		{
			$to = null;
		}
		
		$form_data['from'] = $from;
		$form_data['to'] = $to;
		
		//maximum number of records of a page
		$record_per_page = $this->config->item('admin_sag_list_item_per_page');
		//calculate the record offset according to the current page
		$offset = ($view_params['page']-1)*$record_per_page;
		
		$statuses = $this->sag_model->listShootAdStatusByShopGuid(
				$this->app88param->get88AppGuid(),
				$status_type,
				$offset,
				$record_per_page,
				$sortfieldName,
				$sortorderBool,
				$from != null ? $from : -1,
				$to != null ? $to : -1);
		
		$url = site_url("admin/shoot/index");
		
		$this->_formatDate($form_data);
		$view_param['support_sag'] = $this->app['SupportSAG'] && APP88_SAG_SUPPORT;
		$view_param['form_action_url'] = $url;
		$view_param['page_title'] = $this->lang->line('admin_view_advertisment_history_label');
		$view_param['form_data'] = $form_data;
		$view_param['statuses'] = $statuses['result'];
		$view_param['total_rows'] = $statuses['num_rows'];
		$view_param['max_page'] = ceil($statuses['num_rows']/$record_per_page);
		$view_param['error'] = $error;
		
		if ($view_param['max_page'] == 0)
		{
			$view_param['max_page'] = 1;
		}
		
		if ($this->session->userdata('action_complete_message'))
		{
			$view_param['info'] = $this->session->userdata('action_complete_message');
			$this->session->unset_userdata('action_complete_message');
		}
		
		$this->load->view('admin/shoot/list',$view_param);
	}
	
	
	public function getQuota()
	{
		if (($this->input->post('channel') != '') && ($this->input->post('target') != ''))
		{
			list($channel, $target) = explode('@', $this->input->post('channel'));
			list($t,$c,$category_id) = explode('_', $channel);
			
			$this->load->add_package_path(APPPATH.'third_party/sag_get_option/');
			$this->load->library('sag_get_option');
			
			echo json_encode($this->sag_get_option->getOption($target, $category_id, 'getQuota', APP88_ADMIN_LANGUAGE));
		}		
	}
	
	
	private function _getSAGChannels($flat_child_list = FALSE)
	{
		$sag_shoot_targets = unserialize(SAG_TARGET);
		
		if (isset($sag_shoot_targets['88db']['channels']) && is_array($sag_shoot_targets['88db']['channels']))
		{
			$sag_shoot_targets['88db']['channels'] = array_merge($this->sag->getGenericCategories(), $sag_shoot_targets['88db']['channels']);
		}
		else
		{
			$sag_shoot_targets['88db']['name'] = '88DB';
			$sag_shoot_targets['88db']['channels'] = $this->sag->getGenericCategories();
		}
		
		if ($flat_child_list)
		{
			$formatted = $sag_shoot_targets;
			$sag_shoot_targets = array();
			$sag_shoot_targets['88db']['name'] = '88DB';
			$sag_shoot_targets['88db']['channels'] = array();
			foreach ($formatted['88db']['channels'] as $k => $v)
			{
				$sag_shoot_targets['88db']['channels'] = array_merge($sag_shoot_targets['88db']['channels'], $v['child']);
			}
		}
		
		return $sag_shoot_targets;
	}
	
	
	private function _validateDate($dateValue)
	{
		return DateTime::createFromFormat(APP88_DATE_FORMAT, $dateValue) !== FALSE;
	}
	
	
	private function _formatDate(&$form_data)
	{
		if (trim($form_data['from']) != '')
		{
			$date = DateTime::createFromFormat(str_replace('%', '', $this->config->item('db_system_date_format')), $form_data['from']);
	
			if ($date !== FALSE)
			{
				$form_data['from'] = $date->format(APP88_DATE_FORMAT);
			}
		}
		if (trim($form_data['to']) != '')
		{
			$date = DateTime::createFromFormat(str_replace('%', '', $this->config->item('db_system_date_format')), $form_data['to']);
	
			if ($date !== FALSE)
			{
				$form_data['to'] = $date->format(APP88_DATE_FORMAT);
			}
		}
	}
}