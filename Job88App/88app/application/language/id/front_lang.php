<?php

include_once('country_lang.php');

$lang['label'] = 'teks';
$lang['add_job_label'] = 'Tambah Pekerjaan';
$lang['back_to_job_listing_label'] = 'Kembali ke daftar Pekerjaan';
$lang['refid_label'] = 'Ref. ID';
$lang['post_date_label'] = 'Tanggal Dikirim';
$lang['description_label'] = 'Deksripsi';
$lang['responsibilities_label'] = 'Kewajiban';
$lang['salary_label'] = 'Gaji';
$lang['employment_type_label'] = 'Jenis Pelamar';
$lang['contact_label'] = 'Kontak';
$lang['negotiable_label'] = 'Dapat ditawar';
$lang['front_view_home_label'] = 'Pekerjaan';
$lang['front_view_no_news_category'] = 'Maaf, tidak terdapat pekerjaan dikategori ini. ';
$lang['view_FULLTIME_label'] = 'Rutin';     
$lang['view_PARTTIME_label'] = 'Paruh Waktu';     
$lang['view_TEMPORARY_label'] = 'Sementara';    
$lang['view_CONTRACT_label'] = 'kontrak';      
$lang['view_FREELANCE_label'] = 'Tenaga Lepas';
$lang['view_SUMMER_JOB_label'] = 'Kerja Musiman';
$lang['view_WORK_AT_HOME_label'] = 'Bekerja di rumah';
$lang['view_NOT_SPECIFIED_label'] = 'Tidak Disebutkan';
$lang['front_view_items_per_page_label'] = 'Items Per Halaman';
$lang['front_view_job_title_label'] = 'Judul Pekerjaan';
$lang['front_view_posted_label'] = 'Tanggal Dikirim';
$lang['front_view_sort_by_label'] = 'Urutkan Berdasarkan';
$lang['front_view_sort_posdate_asc_label'] = 'ASC Tanggal Kirim';
$lang['front_view_sort_posdate_desc_label'] = 'DESC tanggal kirim';
$lang['front_view_sort_jobtitl_asc_label'] = 'ASC Judul Pekerjaan';
$lang['front_view_sort_jobtitl_desc_label'] = 'DESC Judul Pekerjaan';
$lang['front_view_sort_emptype_asc_label'] = 'ASC tipe pelamar';
$lang['front_view_sort_emptype_desc_label'] = 'DESC tipe pelamar';
$lang['keyword_text_field_placeholder'] = 'Mencari judul untuk Ref. ID';
$lang['search_result_label'] = 'Hasil Pencarian';
$lang['all_jobs_label'] = 'Semua Pekerjaan';
$lang['err_database_err'] = '<P>Maaf, untuk sementara waktu server tidak dapat memenuhi permintaan Anda. Anda dapat kembali ke<a href="javascript:void(0);" onclick="history.go(-1);">halaman sebelumnya</a> atau dapat ke <a href="http://hk.88db.com/en/" target="_top">88DB.com</a>.<br /><br />Untuk bantuan lainnya, harap klik disini untuk<a href="mailto:cs@88db.com">menghubungi kami</a>.</P>';;
$lang['err_app_closed'] = 'Halaman ini tidak dipublikasikan.';

$lang['error_message'] = '<P>Maaf,  untuk sementara permintaan Anda tidak dapat kami proses. Anda dapat kembali ke<a href="#" onclick="javascript: history.go(-1)"halaman sebelumnya</a> atau ke <a href="http://id.88db.com/" target="_top">88DB.com</a>.<br /><br />Untuk bantuan, silahkan <a href=mailto:cs@88db.com>klik disini</a>untuk hubungi kami.</P>';
$lang['err_session_expired'] = $lang['error_message'];

$lang['btn_get_link'] = 'Mendapatkan Link';
$lang['product_link_dialog_title'] = 'Link ke halaman ini';
$lang['close_label'] = 'Tutup';

