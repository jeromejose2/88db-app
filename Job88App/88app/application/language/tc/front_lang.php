<?php

include_once('country_lang.php');

$lang['label'] = '文字';
$lang['add_job_label'] = '添加工作';
$lang['back_to_job_listing_label'] = '返回工作列表';
$lang['refid_label'] = '參考編號';
$lang['post_date_label'] = '刊登日期';
$lang['description_label'] = '描述';
$lang['responsibilities_label'] = '職責';
$lang['salary_label'] = '薪酬';
$lang['employment_type_label'] = '工作性質';
$lang['contact_label'] = '聯絡人';
$lang['negotiable_label'] = '面議';
$lang['front_view_home_label'] = '工作';
$lang['front_view_no_news_category'] = '對不起，在這個分類中沒有工作信息. ';
$lang['view_FULLTIME_label'] = '全職';     
$lang['view_PARTTIME_label'] = '兼職';     
$lang['view_TEMPORARY_label'] = '臨時';    
$lang['view_CONTRACT_label'] = '合同工作';      
$lang['view_FREELANCE_label'] = '自由工作';
$lang['view_SUMMER_JOB_label'] = '暑期工作';
$lang['view_WORK_AT_HOME_label'] = '在家工作';
$lang['view_NOT_SPECIFIED_label'] = '未指定';
$lang['front_view_items_per_page_label'] = '每頁顯示';
$lang['front_view_job_title_label'] = '工作標題';
$lang['front_view_posted_label'] = '刊登日期';
$lang['front_view_sort_by_label'] = '排序';
$lang['front_view_sort_posdate_asc_label'] = '刊登日期從舊到新';
$lang['front_view_sort_posdate_desc_label'] = '刊登日期從新到舊';
$lang['front_view_sort_jobtitl_asc_label'] = '工作標題順序';
$lang['front_view_sort_jobtitl_desc_label'] = '工作標題逆序';
$lang['front_view_sort_emptype_asc_label'] = '工作性質順序';
$lang['front_view_sort_emptype_desc_label'] = '工作性質逆序';
$lang['keyword_text_field_placeholder'] = '搜尋標題或參考編號';
$lang['search_result_label'] = '搜索結果';
$lang['all_jobs_label'] = '所有工作';
$lang['err_database_err'] = '<P>抱歉，伺服器暫時未能按閣下的要求下載頁面。您可以返回到<a href="javascript:void(0);" onclick="history.go(-1);">上一頁</a> 或前往 <a href="http://hk.88db.com/en/" target="_top">88DB.com</a>。<br /><br />如需更多幫助, 請<a href="mailto:cs@88db.com">點擊這裡</a>來聯系我們。</P>';;
$lang['err_app_closed'] = '此頁未刊登';

$lang['error_message'] = '<P>抱歉，伺服器暫時未能按閣下的要求下載頁面。您可以按<a href="#" onclick="javascript: history.go(-1)">回到上頁</a>或前往<a href="http://hk.88db.com/" target="_top">88DB.com</a>。<br /><br />如需協助，請<a href=mailto:cs@88db.com>按此</a>與我們聯絡。</P>';
$lang['err_session_expired'] = $lang['error_message'];

$lang['btn_get_link'] = '獲取連結';
$lang['product_link_dialog_title'] = '連結到這頁';
$lang['close_label'] = '關閉';

