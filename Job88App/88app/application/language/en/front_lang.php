<?php
include_once('country_lang.php');

$lang['label'] = 'text';
$lang['add_job_label'] = 'Add Job';
$lang['back_to_job_listing_label'] = 'Back to Job Listing';
$lang['refid_label'] = 'Ref. ID';
$lang['post_date_label'] = 'Posting Date';
$lang['description_label'] = 'Description';
$lang['responsibilities_label'] = 'Responsibilities';
$lang['salary_label'] = 'Salary';
$lang['employment_type_label'] = 'Employment Type';
$lang['contact_label'] = 'Contact';
$lang['negotiable_label'] = 'Negotiable';
$lang['front_view_home_label'] = 'Job';
$lang['front_view_no_news_category'] = 'Sorry, there is no jobs in this category. ';
$lang['view_FULLTIME_label'] = 'Full Time';     
$lang['view_PARTTIME_label'] = 'Part Time';     
$lang['view_TEMPORARY_label'] = 'Temporary';    
$lang['view_CONTRACT_label'] = 'Contract';      
$lang['view_FREELANCE_label'] = 'Freelance';
$lang['view_SUMMER_JOB_label'] = 'Summer Job';
$lang['view_WORK_AT_HOME_label'] = 'Work From Home';
$lang['view_NOT_SPECIFIED_label'] = 'Not Specified';
$lang['front_view_items_per_page_label'] = 'Items Per Page';
$lang['front_view_job_title_label'] = 'Job Title';
$lang['front_view_posted_label'] = 'Post Date';
$lang['front_view_sort_by_label'] = 'Sort by';
$lang['front_view_sort_posdate_asc_label'] = 'Posted Date ASC';
$lang['front_view_sort_posdate_desc_label'] = 'Posted Date DESC';
$lang['front_view_sort_jobtitl_asc_label'] = 'Job Title ASC';
$lang['front_view_sort_jobtitl_desc_label'] = 'Job Title DESC';
$lang['front_view_sort_emptype_asc_label'] = 'Employment Type ASC';
$lang['front_view_sort_emptype_desc_label'] = 'Employment Type DESC';
$lang['keyword_text_field_placeholder'] = 'Search Title or Ref. ID';
$lang['search_result_label'] = 'Search Result';
$lang['all_jobs_label'] = 'All Jobs';
$lang['err_database_err'] = '<P>Sorry, the server is temporarily unable to service your request.  You can return to the <a href="javascript:void(0);" onclick="history.go(-1);">previous page</a> or go to <a href="http://hk.88db.com/en/" target="_top">88DB.com</a>.<br /><br />For more help, please click here to <a href="mailto:cs@88db.com">contact us</a>.</P>';;
$lang['err_app_closed'] = 'This page is not published.';

$lang['error_message'] = '<P>Sorry, the server is temporarily unable to service your request.  You can return to the <a href="javascript:void(0);" onclick="history.go(-1);">previous page</a> or go to <a href="http://hk.88db.com/en/" target="_top">88DB.com</a>.<br /><br />For more help, please click here to <a href="mailto:cs@88db.com">contact us</a>.</P>';
$lang['err_session_expired'] = $lang['error_message'];

$lang['btn_get_link'] = 'Get Link';
$lang['product_link_dialog_title'] = 'Link to this page';
$lang['close_label'] = 'Close';

