<?php

include_once('country_lang.php');

$lang['label'] = 'teks';
$lang['add_job_label'] = 'Tambah Pekerjaan';
$lang['back_to_job_listing_label'] = 'Kembali ke Penyenaraian Kerja';
$lang['refid_label'] = 'ID Rujukan';
$lang['post_date_label'] = 'Tarikh Paparan';
$lang['description_label'] = 'Deskripsi';
$lang['responsibilities_label'] = 'Tanggungjawab';
$lang['salary_label'] = 'Gaji';
$lang['employment_type_label'] = 'Jenis Pekerjaan';
$lang['contact_label'] = 'No. Telefon';
$lang['negotiable_label'] = 'Boleh Dirunding';
$lang['front_view_home_label'] = 'Pekerjaan';
$lang['front_view_no_news_category'] = 'Maaf, tiada pekerjaan di kategori ini. ';
$lang['view_FULLTIME_label'] = 'Sepenuh Masa';
$lang['view_PARTTIME_label'] = 'Sambilan';
$lang['view_TEMPORARY_label'] = 'Sementara';
$lang['view_CONTRACT_label'] = 'Kontrak';
$lang['view_FREELANCE_label'] = 'Bebas';
$lang['view_SUMMER_JOB_label'] = 'Kerja Musim Panas';
$lang['view_WORK_AT_HOME_label'] = 'Kerja dari rumah';
$lang['view_NOT_SPECIFIED_label'] = 'Tidak Dinyatakan';
$lang['front_view_items_per_page_label'] = 'Items Setiap Halaman';
$lang['front_view_job_title_label'] = 'Nama Jawatan';
$lang['front_view_posted_label'] = 'Tarikh Paparan';
$lang['front_view_sort_by_label'] = 'Susun ikut';
$lang['front_view_sort_posdate_asc_label'] = 'Tarikh Paparan Urutan Naik';
$lang['front_view_sort_posdate_desc_label'] = 'Tarikh Paparan Urutan Turun';
$lang['front_view_sort_jobtitl_asc_label'] = 'Nama Jawatan Urutan Naik';
$lang['front_view_sort_jobtitl_desc_label'] = 'Nama Jawatan Urutan Turun';
$lang['front_view_sort_emptype_asc_label'] = 'Jenis Pekerjaan Urutan Naik';
$lang['front_view_sort_emptype_desc_label'] = 'Jenis Pekerjaan Urutan Turun';
$lang['keyword_text_field_placeholder'] = 'Cari Jawatan atau ID Rujukan';
$lang['search_result_label'] = 'Hasil Carian';
$lang['all_jobs_label'] = 'Semua Pekerjaan';
$lang['err_database_err'] = '<P>Maaf, permintaan anda gagal di proses untuk sementara waktu.  Anda boleh kembali ke <a href="javascript:void(0);" onclick="history.go(-1);">previous page</a> atau ke <a href="http://hk.88db.com/en/" target="_top">88DB.com</a>.<br /><br />Untuk bantuan, sila klik <a href="mailto:cs@88db.com">hubungi kami</a>.</P>';;
$lang['err_app_closed'] = 'Laman ini tidak dipaparkan.';

$lang['error_message'] = '<P>Maaf, untuk sementara permintaan anda tidak dapat kami proses. Anda boleh kembali ke<a href="#" onclick="javascript: history.go(-1)"halaman sebelumnya</a> atau ke <a href="http://my.88db.com" target="_top">88DB.com</a>.<br /><br />Untuk bantuan, sila <a href=mailto:cs@88db.com>klik disini</a>untuk hubungi kami.</P>';
$lang['err_session_expired'] = $lang['error_message'];

$lang['btn_get_link'] = 'Dapatkan Pautan';
$lang['product_link_dialog_title'] = 'Pautan ke laman ini';
$lang['close_label'] = 'Tutup';

