<?php

include_once('country_lang.php');

$lang['label'] = '文本';
$lang['add_job_label'] = '添加职位';
$lang['back_to_job_listing_label'] = '返回职位列表';
$lang['refid_label'] = '岗位编号';
$lang['post_date_label'] = '发布日期';
$lang['description_label'] = '职位描述';
$lang['responsibilities_label'] = '职责';
$lang['salary_label'] = '薪水';
$lang['employment_type_label'] = '职位类型';
$lang['contact_label'] = '联系';
$lang['negotiable_label'] = '面议';
$lang['front_view_home_label'] = '职位';
$lang['front_view_no_news_category'] = '对不起，在这个分类中没有职位信息. ';
$lang['view_FULLTIME_label'] = '全职';     
$lang['view_PARTTIME_label'] = '兼职';     
$lang['view_TEMPORARY_label'] = '临时';    
$lang['view_CONTRACT_label'] = '合同';      
$lang['view_FREELANCE_label'] = '自由工作';
$lang['view_SUMMER_JOB_label'] = '暑期工作';
$lang['view_WORK_AT_HOME_label'] = '在家工作';
$lang['view_NOT_SPECIFIED_label'] = '未指定';
$lang['front_view_items_per_page_label'] = '每页显示';
$lang['front_view_job_title_label'] = '职位标题';
$lang['front_view_posted_label'] = '发布日期';
$lang['front_view_sort_by_label'] = '排序按照';
$lang['front_view_sort_posdate_asc_label'] = '发布日期升序';
$lang['front_view_sort_posdate_desc_label'] = '发布日期降序';
$lang['front_view_sort_jobtitl_asc_label'] = '职位标题升序';
$lang['front_view_sort_jobtitl_desc_label'] = '职位标题降序';
$lang['front_view_sort_emptype_asc_label'] = '职位类型升序';
$lang['front_view_sort_emptype_desc_label'] = '职位类型降序';
$lang['keyword_text_field_placeholder'] = '搜索标题或岗位编号';
$lang['search_result_label'] = '搜索结果';
$lang['all_jobs_label'] = '所有职位';
$lang['err_database_err'] = '<P>对不起, 服务器暂时无法响应您的请求.  您可以返回到<a href="javascript:void(0);" onclick="history.go(-1);">上一页</a> 或前往 <a href="http://hk.88db.com/en/" target="_top">88DB.com</a>.<br /><br />如需更多帮助, 请点击这里来<a href="mailto:cs@88db.com">联系我们</a>.</P>';;
$lang['err_app_closed'] = '此页面未发布.';

$lang['error_message'] = '<P>对不起，服务器暂时未能按您的要求下载页面。您可以按<a href="#" onclick="javascript: history.go(-1)">回到上页</a>或前往<a href="http://cn.88db.com/" target="_top">88DB</a>。<br /><br />如需帮助，请<a href=mailto:cs@88db.com>按此</a>联系我们。</P>';
$lang['err_session_expired'] = $lang['error_message'];

$lang['btn_get_link'] = '获取链接';
$lang['product_link_dialog_title'] = '链接此页';
$lang['close_label'] = '关闭';

