<?php

include_once('country_lang.php');

$lang['label'] = 'ข้อความ';
$lang['add_job_label'] = 'เพิ่มงาน';
$lang['back_to_job_listing_label'] = 'กลับสู่หน้ารายการงาน';
$lang['refid_label'] = 'ID อ้างอิง';
$lang['post_date_label'] = 'วันที่ประกาศ';
$lang['description_label'] = 'รายละเอียด';
$lang['responsibilities_label'] = 'ความรับผิดชอบ';
$lang['salary_label'] = 'เงินเดือน';
$lang['employment_type_label'] = 'ประเภทการจ้างงาน';
$lang['contact_label'] = 'ติดต่อ';
$lang['negotiable_label'] = 'สามารถต่อรองได้';
$lang['front_view_home_label'] = 'งาน';
$lang['front_view_no_news_category'] = 'ขออภัย ไม่พบตำแหน่งงานในหมวดหมู่นี้';
$lang['view_FULLTIME_label'] = 'Full Time';     
$lang['view_PARTTIME_label'] = 'Part Time';     
$lang['view_TEMPORARY_label'] = 'Temporary';    
$lang['view_CONTRACT_label'] = 'Contract';      
$lang['view_FREELANCE_label'] = 'Freelance';
$lang['view_SUMMER_JOB_label'] = 'Summer Job';
$lang['view_WORK_AT_HOME_label'] = 'Work at home';
$lang['view_NOT_SPECIFIED_label'] = 'ไม่ระบุ';
$lang['front_view_items_per_page_label'] = 'จำนวนต่อหน้า';
$lang['front_view_job_title_label'] = 'ชื่อตำแหน่งงาน';
$lang['front_view_posted_label'] = 'วันที่ประกาศ';
$lang['front_view_sort_by_label'] = 'เรียงลำดับโดย';
$lang['front_view_sort_posdate_asc_label'] = 'เรียงวันที่ประกาศจากน้อยไปมาก';
$lang['front_view_sort_posdate_desc_label'] = 'เรียงวันที่ประกาศจากมากไปน้อย';
$lang['front_view_sort_jobtitl_asc_label'] = 'เรียงชื่อตำแหน่งงาน';
$lang['front_view_sort_jobtitl_desc_label'] = 'เรียงชื่อตำแหน่งงาน';
$lang['front_view_sort_emptype_asc_label'] = 'เรียงลำดับประเภทการจ้างงานจากน้อยไปมาก';
$lang['front_view_sort_emptype_desc_label'] = 'เรียงลำดับประเภทการจ้างงานจากมากไปน้อย';
$lang['keyword_text_field_placeholder'] = 'ค้นหาชื่อตำแหน่ง หรือ ID อ้างอิง';
$lang['search_result_label'] = 'ผลการค้นหา';
$lang['all_jobs_label'] = 'งานทั้งหมด';
$lang['err_database_err'] = '<P>ขออภัย เซิร์ฟเวอร์ไม่สามารถใช้งานได้ชั่วคราว คุณสามารถกลับไปหน้า <a href="javascript:void(0);" onclick="history.go(-1);">ก่อนหน้า</a> หรือไปยัง <a href="http://th.88db.com/ " target="_top">88DB.com</a>.<br /><br />หากท่านต้องการความช่วยเหลือเพิ่มเติม กรุณาคลิกที่นี่เพื่อ <a href="mailto:cs@88db.co.th">ติดต่อเรา</a>.</P>';;
$lang['err_app_closed'] = 'หน้านี้ยังไม่ได้เผยแพร่';

$lang['error_message'] = '<P>ขออภัย ขณะเซิร์ฟเวอร์ไม่สามารถดำเนินการตามคำขอของคุณได้เป็นการชั่วคราว กรุณากลับไปยัง<a href="#" onclick="javascript: history.go(-1)">หน้าที่ผ่านมา</a> หรือไปยัง <a href="http://th.88db.com" target="_top">88DB.com</a>.<br /><br />หากท่านต้องการความช่วยเหลือเพิ่มเติม กรุณา<a href=mailto:cs@88db.com>click here</a> ติดต่อเร</P>';
$lang['err_session_expired'] = $lang['error_message'];

$lang['btn_get_link'] = 'รับลิงก์';
$lang['product_link_dialog_title'] = 'ลิงก์ไปหน้านี้';
$lang['close_label'] = 'ปิด';

