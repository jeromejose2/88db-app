<?php 
	$this->load->view("front/common/header");
	
	function printBreadCrumb($parents) 
	{
		if(is_array($parents))
		{
			foreach ($parents as $parent)
			{
				echo '<li><a href="'.site_url("category/index/".$parent['Guid']).'">'.form_prep($parent['Name']).'</a></li><span class="dbShop-ui-appBCrumbTxt">&gt; </span>';
			}
		}
	}
	
	function reformatDate($date,$style=1)
	{
		if($style!=1) return date('d M Y', strtotime($date));
		else return date(APP88_DATE_FORMAT, strtotime($date));
	}
?>

<div id="breadcrumb" style="height:24px;">
	<ul id="breadcrumbs">
		<li><a href="<?php echo site_url('front/index');?>"><?php echo $this->lang->line('front_view_home_label'); ?></a></li>
		<span class="dbShop-ui-appBCrumbTxt">&gt; </span>
		<?php printBreadCrumb($parent_category); ?>
		<li><?php echo form_prep($category_name); ?></li>
	</ul>
</div>

<?php $this->load->view('front/common/left-side-col');?>

<div id="main">

<?php 
	if ($error) $this->common->showError($error);
	if ($info) $this->common->showInfo($info);
?>

	<div id="product_filter">
		<?php if ($total_record > $this->config->item('jobs_list_item_per_page')) { ?>
		<label class="dbShop-ui-left"><?php echo $this->lang->line('front_view_items_per_page_label'); ?>: </label>
		<div class="dbShop-ui-left">
		<?php 
		$cnt = count($items_per_page_opts);
		for ($i=0; $i<$cnt; $i++)
		{
			if ($items_per_page_opts[$i] == $items_per_page)
			{
				echo '<span id="current_items">' . $items_per_page_opts[$i] . '</span>';
			}
			else
			{
				echo '<a class="items_per_page_button" href="#">' . $items_per_page_opts[$i] . '</a>';
			}
			
			if ($i<$cnt-1)
			{
				echo ' | ';
			}
		}
			
		?>
			
		</div>
		<?php } ?>
		<script type="text/javascript">
		
		var app88job = app88job || {};


		app88job.sortOpt = '<?php
			if (!empty($sort))
			{
				echo $sort;
			}
			else
			{
				echo "poDate_desc";	
			}
		?>';
		
		app88job.siteURL = '<?php
		 	$url = "category/index";
		 	if ($category_guid)
		 	{
		 		$url .= ("/".$category_guid);
		 	}
		 	if ($page)
		 	{
		 		$url .= ("/".$page);
		 	}
		 	echo site_url($url); 
		  
		?>';

		<?php 
			$url = "job/view";
			$url = site_url($url);
			
			$paramStr;
			$urlBaseStr;
			$paramStrPos = strpos($url, '?');
			
			if (FALSE === $paramStrPos)
			{
				// not found param, error should occur
				$urlBaseStr = $url;
				$paramStr = '';
			}
			else 
			{
				$paramStr = substr($url, $paramStrPos);
				$urlBaseStr = substr($url, 0, $paramStrPos);
			}
			
			echo 'app88job.jobDetailURLBase = "' . $urlBaseStr . '";';
			echo "\n";
			echo "\t\t";
			echo 'app88job.jobDetailURLParam = "' . $paramStr . '";';
			echo "\n";
		?>

		$(document).ready(function(){
				  
			$('#search').click(
				function(){
					var keywd = $('#keyword').val();
					keywd = $.trim(keywd);
					if($('#keyword').attr('search') == 0)
					{
						keywd = '';
					}
					var itemsPerPage = $('span#current_items').text();
					window.location = app88job.siteURL + '&keyword=' + encodeURIComponent(keywd) + '&sort=' + app88job.sortOpt + '&items_per_page=' + itemsPerPage;
				}
			);

			$('a.items_per_page_button').click(
				function(){
					var keywd = $('#keyword').val();
					keywd = $.trim(keywd);
					if($('#keyword').attr('search') == 0)
					{
						keywd = '';
					}
					window.location = app88job.siteURL + '&keyword=' + encodeURIComponent(keywd) + '&sort=' + app88job.sortOpt + '&items_per_page=' + $(this).text();
				
				}
			);

			$("#keyword").keypress(
				function(event) {
					if (13 == event.keyCode)
					{
						var keywd = $('#keyword').val();
						keywd = $.trim(keywd);
						if($('#keyword').attr('search') == 0)
						{
							keywd = '';
						}	
						var itemsPerPage = $('span#current_items').text();
						window.location = app88job.siteURL + '&keyword=' + encodeURIComponent(keywd) + '&sort=' + app88job.sortOpt + '&items_per_page=' + itemsPerPage;
					}
				}
			);
			

			$('div.listcontent').click(
				function(event)
				{
					var guid = $(this).attr('id');
					var url = app88job.jobDetailURLBase + "/" + guid + app88job.jobDetailURLParam;
					<?php 
							echo 'url += "&category_guid=' . $category_guid . '";';
							if ($page)
							{
								echo 'url += "&page_no=' . $page . '";';
							}
							if($keyword) 
							{
								echo 'url += "&keyword=' . urlencode($keyword) . '";';
								echo "\n";
							}
							if($sort)
							{
								echo 'url += "&sort=' . $sort . '";';	
								echo "\n";
							}
							if($items_per_page )
							{
								echo 'url += "&items_per_page=' . $items_per_page . '";';
								echo "\n";
							}
							
					?>
					window.location = url;
				}
			);

			app88job.SORT_BTN_STATES = new Array(<?php echo $sort_btn_states_str;?>);
			
			$('div.jobheader>span').click(
				function(event)
				{
					var staInd = parseInt($(this).attr('data-state'));
					staInd++;
					if (staInd == app88job.SORT_BTN_STATES.length)
					{
						staInd = 1;	// note!
					}
					app88job.sortOpt = $(this).attr('data-field') + '_' + app88job.SORT_BTN_STATES[staInd];
					//console.log(app88job.sortOpt);
					var keywd = $('#keyword').val();
					keywd = $.trim(keywd);
					if($('#keyword').attr('search') == 0)
					{
						keywd = '';
					}
					var itemsPerPage = $('span#current_items').text();
					window.location = app88job.siteURL + '&keyword=' + encodeURIComponent(keywd) + '&sort=' + app88job.sortOpt + '&items_per_page=' + itemsPerPage;
				}
			).css('cursor', 'pointer');


			

		});
		</script>
		<div class="dbShop-ui-price">
			<input id="keyword" name="keyword" type="text" value="<?php echo $keyword; ?>" style="color: #999999" />
			<a id="search" href="#" class="search_btn" id="search_button"></a>
		</div>
		

		<div class="dbShop-ui-clearboth"></div>
	</div>

	<div class="dbShop-ui-clearboth"></div>
	<div class="jobheader">
		
		<span class="ch01" id="title_filter_btn" data-field="title" data-state="<?php 
			$state = "0";
			if (isset($sort_field) && 'title' == $sort_field)
			{
				$state = $sort_btn_state;
			}
			echo $state;
		?>">
		<?php echo $this->lang->line('front_view_job_title_label'); ?>
	
		<a class="arrow_btn">	
		<img src="/images/front_end/<?php 
				if ("0"==$state)
				{
					echo "fair-arrow.gif";
				}
				elseif ("1"==$state)
				{
					echo "down-arrow.gif";
				}
				elseif ("2"==$state)
				{
					echo "up-arrow.gif";
				}			
			?>">
		</a>
		
		</span> 
		
		
		
		<span class="ch02" id="empTyp_filter_btn" data-field="empTyp" data-state="<?php 
			$state = "0";
			if (isset($sort_field) && 'empTyp' == $sort_field)
			{
				$state = $sort_btn_state;
			}
			echo $state;
		?>">
		<?php echo $this->lang->line('employment_type_label'); ?>
		
		<a class="arrow_btn">	
		<img src="/images/front_end/<?php 
				if ("0"==$state)
				{
					echo "fair-arrow.gif";
				}
				elseif ("1"==$state)
				{
					echo "down-arrow.gif";
				}
				elseif ("2"==$state)
				{
					echo "up-arrow.gif";
				}			
			?>">
		</a>
			
		</span> 
		
		<span class="ch03" id="saly_filter_btn" data-field="saly" data-state="<?php 
			$state = "0";
			if (isset($sort_field) && 'saly' == $sort_field)
			{
				$state = $sort_btn_state;
			}
			echo $state;
		?>">
		<?php echo $this->lang->line('salary_label'); ?>
		
		<a class="arrow_btn">	
		<img src="/images/front_end/<?php 
				if ("0"==$state)
				{
					echo "fair-arrow.gif";
				}
				elseif ("1"==$state)
				{
					echo "down-arrow.gif";
				}
				elseif ("2"==$state)
				{
					echo "up-arrow.gif";
				}			
			?>">
		</a>
		
		</span> 
		
		<span class="ch04" id="refid_filter_btn" data-field="refid" data-state="<?php 
			$state = "0";
			if (isset($sort_field) && 'refid' == $sort_field)
			{
				$state = $sort_btn_state;
			}
			echo $state;
		?>">
		<?php echo $this->lang->line('refid_label'); ?>
		
		<a class="arrow_btn">	
		<img src="/images/front_end/<?php 
				if ("0"==$state)
				{
					echo "fair-arrow.gif";
				}
				elseif ("1"==$state)
				{
					echo "down-arrow.gif";
				}
				elseif ("2"==$state)
				{
					echo "up-arrow.gif";
				}			
			?>">
		</a>
		
		</span> 
		
		<span class="ch05" style="width: 74px" id="poDate_filter_btn" data-field="poDate" data-state="<?php
			$state = "0";
			if (isset($sort_field) && 'poDate' == $sort_field)
			{
				$state = $sort_btn_state;
			}
			echo $state;
		?>">
		<?php echo $this->lang->line('front_view_posted_label'); ?>
		
		<a class="arrow_btn">	
		<img src="/images/front_end/<?php 
				if ("0"==$state)
				{
					echo "fair-arrow.gif";
				}
				elseif ("1"==$state)
				{
					echo "down-arrow.gif";
				}
				elseif ("2"==$state)
				{
					echo "up-arrow.gif";
				}			
			?>">
		</a>
		
		</span>
	</div>

	
	<?php
	if (empty($jobs))
	{
	?>
		<div class="dbShop-ui-warning">
			<?php echo $this->lang->line('front_view_no_news_category'); ?>
			<?php if (false && $this->app88userinfo->isShopOwner()) { /* hide the button, we dont want front end authen user every time yet */?>
				<a class="itemBtn-red" href="<?php echo site_url("admin/job/add/".$category_guid); ?>"><?php echo $this->lang->line('front_view_add_news'); ?></a>
			<?php } ?>
		</div>		
	<?php
	}
	else
	{
		$counter = 0;
			
		if (!empty($jobs) &&
			is_array($jobs))
		{
			foreach ($jobs as $job) {
	?>
				<div class="listcontent row<?php echo $counter%2+1; ?>" id="<?php echo form_prep($job['Guid']);?>">
					<span class="cc01"><?php echo form_prep($job['Title']);?></span>
					<span class="cc02" ><?php echo $this->lang->line('view_'.$job['EmploymentType'].'_label');?></span>
					<span class="cc03"><?php if ($job['Salary']) echo $this->common->money_format(form_prep($job['Salary'])) . '<br>';?><?php if ('1'==$job['Negotiable']) echo $this->lang->line('negotiable_label'); ?></span>
					<span class="cc04"><?php echo form_prep($job['ReferenceId']); ?></span>
					<span class="cc05"><?php echo reformatDate($job['PostDate']); ?></span>
				</div>  
		<?php 
				$counter++;
			}
		}
	}
	?>

	
	<?php if ($max_page > 1) {?>
	<div class="paging" id="paging" style="display: block;">
	<?php
	
		$leftpage = $page - ($this->config->item('num_page_show')-1)/2;
		$leftpage = $leftpage >= 1? $leftpage : 1;
		
		$rightpage = $leftpage + $this->config->item('num_page_show')-1;
		$rightpage = $rightpage >= $max_page? $max_page: $rightpage;
		
		$leftpage = $rightpage >= $max_page? ($rightpage - $this->config->item('num_page_show')+1): $leftpage;
		
		for($i=1;$i<=$max_page;$i++)
		{
			if ($i == 1)
			{
				if ($page > 1)
				{
					$site_url =  site_url('category/index/'.$category_guid.'/'.($page-1));
					if($keyword) $site_url.="&keyword=". urlencode($keyword) ;
					if($sort) $site_url.="&sort=". $sort;
					if($items_per_page) $site_url.="&items_per_page=". $items_per_page;
					echo '<a class="previous" href="'.$site_url.'">&lt;&lt;</a>';
				}
				else
				{
					echo '<span style="padding:0px 17px"></span>';
				}
			}
			
			if ($i==$page)
			{
		?>
				<strong <?php if ($page == $max_page) echo 'style="border:none"'; ?>><?php echo $i; ?></strong>
		<?php 
			}
			else
			{
				if($i == $max_page || $i == 1 || ($i <= $rightpage && $i >= $leftpage))
				{
		?>
				<a href="
		<?php 
			$site_url =  site_url('category/index/'.$category_guid.'/'.$i);
			if($keyword) $site_url.="&keyword=". urlencode($keyword);
			if($sort) $site_url.="&sort=". $sort;
			if($items_per_page) $site_url.="&items_per_page=". $items_per_page;
			echo $site_url;
		?>" <?php if ($i == $max_page) echo 'class="last"'; ?>><?php echo $i; ?></a>
		<?php
				}
				else
				{
					if($i == $rightpage+1) echo'<span>&nbsp;...&nbsp;</span>';
					elseif($i == $leftpage-1) echo '<span>&nbsp;...&nbsp;</span>';
				}
			}
			
			if ($i == $max_page)
			{
				if($page < $max_page)
				{
					$site_url =  site_url('category/index/'.$category_guid.'/'.($page+1));
					if($keyword) $site_url.="&keyword=". urlencode($keyword);
					if($sort) $site_url.="&sort=". $sort;
					if($items_per_page) $site_url.="&items_per_page=". $items_per_page;
					echo '<a class="next" href="'.$site_url.'">&gt;&gt;</a>';
				}
				else
				{
					echo '<span style="padding:0px 17px"></span>';
				}
			}
		}
	?>
	</div>
	<?php } ?>

</div>
<?php $this->load->view("front/common/footer");?>
