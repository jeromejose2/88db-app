
<?php 
	function printCategoryNav($categories, $current_guid, $level = 0)
	{
		$counter = 0;
		if (is_array($categories))
		{
			foreach ($categories as $category)
			{
				if ($category['Published'] == 1)
				{
					if ($level == 0)
					{
						echo '<li id="sh-menu">';
					}
					else
					{
						if ($counter == 0) 
						{
							echo '<ul class="sub-level">';
							$counter++;
						}
						echo '<li';
						if ($category['Guid'] == $current_guid) echo ' class="current-category"';
						echo '>';
					}
					echo '<a href="'.site_url("category/index/".$category['Guid']).'" class="katalog';
					if ($category['Guid'] == $current_guid)
					{
						if ($level > 0) echo ' current-category';
						else echo ' current-category';
					}
					echo '">';
					echo form_prep($category['Name']);
					echo '</a>';
					if($category['Subcategory'])
					{
						printCategoryNav($category['Subcategory'],$current_guid,$level+1);
					}
					if ($level == 0)
					{
						echo '</li>';
					}
					else
					{
						echo '</li>';
					}
				}
			}
			if ($counter != 0) echo '</ul>';
		}
	}

?>
<noscript>
	<style type="text/css">
		#mcs_container .customScrollBox{overflow:auto;}
		#mcs_container .dragger_container{display:none;}
	</style>
</noscript>
<div id="dbShop-ui-mainLeft">	
	<span class="dbShop-ui-cattitle" onclick="location.href='<?php echo site_url('category/index'); ?>'" style="cursor:pointer">
	<?php echo $this->lang->line('front_view_home_label'); ?></span>
	
	<div id="mcs_container" class="mousescroll">
		<div class="customScrollBox">
			<div style="top: 0px;" id="nav-kategori" class="container">
				<div class="content">
					<ul class="navigation">
						<?php printCategoryNav($categories,$category_guid,0); ?>
					</ul>
				</div>
			</div>
			<div style="display: none;" class="dragger_container">
				<div style="top: 0px; display: none;" class="dragger"></div>
			</div>
		</div>
		<a style="display: none;" href="#" class="scrollUpBtn"></a>
		<a style="display: none;" href="#" class="scrollDownBtn"></a>
	</div>
	
	<span class="spliter" style="display:none;"></span>
	<?php 
	if (1==2)
	{
	
	?>
	<span class="cattitle-payment" style="display:none;">Payment</span>
	<div id="bank" style="display:none;">
		<div class="pay-method-wrapper">
			<div>
					<span class="pay-method-img"><img src="/images/bank_central_asia_bca.jpg"></span>
			</div>
			<div>
					<span class="pay-method-nama-ttl">A/N</span>
					<span class="pay-method-nama">Deasy Valentine Tj</span>
			</div>
			<div>
					<span class="pay-method-norek-ttl">No.</span>
					<span class="pay-method-norek">5485014656</span>
			</div>
		</div>
												
		<div class="pay-method-wrapper">
			<div>
					<span class="pay-method-img"><img src="/images/bank_mandiri.jpg"></span>
			</div>
			<div>
					<span class="pay-method-nama-ttl">A/N</span>
					<span class="pay-method-nama">Deasy Valentine Tj</span>
			</div>
			<div>
					<span class="pay-method-norek-ttl">No.</span>
					<span class="pay-method-norek">1150004351229</span>
			</div>
		</div>
	</div>
	
	<?php 
	}
	?>
	
</div>
<script>
	$(document).ready(function() {
	$('li.current-category').parents('#sh-menu').children('a:eq(0)').addClass('current-category');
	});
</script>
<?php //ob_flush(); ?>