<!-- CSS Link -->
<link rel="stylesheet" type="text/css" href="/s/v<?php echo VERSION; ?>/css/front_end/job.css">

<link rel="stylesheet" type="text/css" href="/s/v<?php echo VERSION; ?>/js/jquery_ui/themes/smoothness/jquery-ui-1.8.20.custom.min.css" media="screen">

<!-- Javascript Link -->

<?php 
$app_param = $this->session->userdata('app_param');
if ( !empty($app_param[$_GET['cid']]['common_js_path']) )
{
	echo "	<script type=\"text/javascript\" src=\"". $app_param[$_GET['cid']]['common_js_path'] ."\"></script>\n";
}
?>
<script type="text/javascript" src="/s/v<?php echo VERSION; ?>/js/jquery_ui/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="/s/v<?php echo VERSION; ?>/js/jquery.easing.1.3.min.js"></script>
<script type="text/javascript" src="/s/v<?php echo VERSION; ?>/js/jquery_ui/ui/minified/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="/s/v<?php echo VERSION; ?>/js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript">

	var app_instance = "<?php echo $_GET['cid']; ?>";

	var placeholder_keyword = '<?php echo $this->app88common->js_quote($this->lang->line('keyword_text_field_placeholder'));?>';

	$(document).ready(function(){

		$("#paging").bind("contextmenu",function(){

			return false;

		});

		<?php
		if(!isset($keyword) || $keyword == "")
		{
		?>
			$("input#keyword").css('color','#999');
			$("input#keyword").val(placeholder_keyword);
			$("input#keyword").attr('search','0');
		<?php
		}
		else
		{
		?>
		    $("input#keyword").css('color','black');
		    $("input#keyword").attr('search','1');
		<?php 
		}
		?>
		
		$("input#keyword").focus(function() {

			$(this).attr('search','1');
			$(this).css('color','black');
			
			var value = $.trim($(this).val());
			if(value == placeholder_keyword)
			{
				$(this).val("");
				
			}
			
		}).blur(function() {
			if($.trim($(this).val()) == "")
			{
				$(this).attr('search','0');
				$(this).val(placeholder_keyword);
				$(this).css('color','#999');
			}
			else
			{
				$(this).attr('search','1');
				$(this).css('color','black');
			}
		});
	});

</script>





