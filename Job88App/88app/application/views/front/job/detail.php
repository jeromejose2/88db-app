<?php 
	$this->load->view("front/common/header");
	
	function printBreadCrumb($parents)
	{
		if(is_array($parents))
		{
			$i = 0;
			$length = count($parents);
			foreach ($parents as $parent)
			{
				echo '<li><a href="'.site_url("category/index/".$parent['Guid']).'" ';
				echo ($i+1 >= $length)? 'style="color:rgb(204, 0, 0)"' : "";
				echo '>'.form_prep($parent['Name']).'</a></li>';
				if (++$i < $length) echo '<span class="dbShop-ui-appBCrumbTxt">&gt;  </span>';
			}
		}
	}
	
	function createShareUrlParam($param)
	{
		$str = '';
		foreach ($param as $key => $value)
		{
			if($value != null)
			{
				if($str != '')
				{
					$str .= '&';
				}
				$str .= $key.'='.$value;
			}
		}
		return $str;
	}
?>

<script type="text/javascript">
	$(document).ready(function(){
		$('div.itemContent img').load(function() {
			app88resize();
		});

		$('.dbShop-ui-getlink').click(function(event) {

			var simpleWindow;
			if ($("#windownbg").length == 0) {
				simpleWindow = $("<div id=\"windownbg\" style=\"height:" + $(document).height() + "px;filter:alpha(opacity=0);opacity:0;z-index: 100\"></div>");
				$("body").append(simpleWindow);
			} else {
				simpleWindow = $("#windownbg");
			}
			simpleWindow.show();

			var dialog = $( "#product-link-layer" );
			dialog.find('textarea').val('<?php echo $this->app88common->get_direct_link('j/' . $job['Guid']); ?>');
			dialog.show();
			dialog.css("left", (($('body').width() - dialog.width()) / 2) + "px");

			if (event != 0)
			{
				var top = event.clientY - dialog.height()/2 - 20;
				top = Math.min(Math.max(top, 10), $('body').height() - dialog.height() - 30);	
				dialog.css("top", top + "px");
			}

			selectAllText();
			
		});

		$('#product-link-layer .msBtn').click(function() {
			$( "#product-link-layer" ).hide();
			$('#windownbg').hide('fast');
		});
	});

	function selectAllText()
	{
	  var textComponent = document.getElementById('product_link_text');
	  textComponent.focus();
	  textComponent.selectionStart = 0;
	  textComponent.selectionEnd = textComponent.value.length;
	}
</script>

<div id="breadcrumb" style="height:24px;">
	<ul id="breadcrumbs">
		<li><a href="<?php echo site_url('front/index');?>"><?php echo $this->lang->line('front_view_home_label'); ?></a></li>
		<?php if ($category_guid != 'all') { ?><span class="dbShop-ui-appBCrumbTxt">&gt; </span><?php } ?>
		<?php printBreadCrumb($parent_categories); ?>
	</ul>
</div>

<?php $this->load->view('front/common/left-side-col');?>

<div id="main">
	
	<?php
		if (!empty($error) || !empty($info))
		{
			$this->common->showError($error);
			$this->common->showInfo($info);
		}
	?>
	

	<span class="tabs-Btn">
		<?php if (false) { ?>
		<a class="itemBtn-red" href="#"><?php echo $this->lang->line('add_job_label'); ?></a>
		<?php } ?>
		<a class="dbShop-ui-backlink" href="<?php echo $back_url; ?>">
			&lt;&lt; <?php echo $this->lang->line('back_to_job_listing_label'); ?>
		</a>
	</span>
	
	<div id="tabs-produk">	
		<div class="detail-title">
			<h2><?php echo form_prep($job['Title']); ?></h2>
		</div>
	
		<div class="ref-id">
			<label class="title-tabs"><?php echo $this->lang->line('refid_label'); ?>: </label>
			<?php echo form_prep($job['ReferenceId']);?>
		</div>
		
		<div class="news-date">
			<label class="title-tabs"><?php echo $this->lang->line('post_date_label'); ?>: </label>
			<?php echo $job['PostDateFormatted']; ?>
		</div>
			
		<div style="clear:both"></div>
		
		<div class="itemContent">
			<label class="title-content"><?php echo $this->lang->line('description_label'); ?>:</label>
			<div class="itemContent-des">
				<?php echo $job['Description']; ?>
			</div>
		</div>
	
		<div class="bottom-info">
			<div class="detail-row1"> 
				<label class="title-info" style="width:125px;"><?php echo $this->lang->line('salary_label'); ?>:</label>
				<div class="detail-content">
					<?php
						if ($job['Salary'])
						{
							echo $this->common->money_format($job['Salary']); 
						}
					?>
					<?php 
						if ($job['Negotiable'] == '1')
						{
							echo " ".$this->lang->line('negotiable_label');
						}
					?>
				&nbsp;</div>
			</div>
			
			<div class="detail-row2"> 
				<label class="title-info" style="width:125px;"><?php echo $this->lang->line('employment_type_label'); ?>:</label>
				<div class="detail-content"><?php echo $this->lang->line('view_' . $job['EmploymentType'] .'_label'); ?>&nbsp;</div>
			</div>
	
			<div class="detail-row1"> 
				<label class="title-info" style="width:125px;"><?php echo $this->lang->line('contact_label'); ?>:</label>
				<div class="detail-content" style="margin-left:135px">
				<?php 
					echo form_prep($job['ContactPerson']) . "<br/>" . form_prep($job['Contact']) . '<br/>';
					if ($job['Recipient'])
					{
						echo '<a href="mailto:' . $job['Recipient'] . '">' 
							. form_prep($job['Recipient']) . '</a>';
					} 
				?>
				&nbsp;
				</div>
			</div>
			
			
			
			<div class="shareBlk">
			<?php 
				if(APP88_JIATHIS_ENABLE || APP88_ADDTHIS_ENABLE)
				{
					$link = $this->app88common->get_direct_link('j/'.$job['Guid']);
					
					$param['og_title'] = urlencode($job['Title']);
					$param['og_desc'] = urlencode($this->common->trimToChars(preg_replace("/[\n\r]+/"," ",strip_tags($job['Description'])), APP88_SHARE_DESCRIPTION_CHAR_LIMIT));
					
					$secret = $this->config->item('social_share_key');	
					$checksum = md5(createShareUrlParam($param).'&secret='.$secret);
					
					$param['checksum'] = $checksum;
				}
			?>
			<?php if (APP88_JIATHIS_ENABLE && $this->app88param->getLang() != 'en') { ?>
				<div class="share_ui">
					<div class="jiathis_style">
						<a href="http://www.jiathis.com/share" class="jiathis jiathis_txt" target="_blank">
							<img src="http://v2.jiathis.com/code_mini/images/btn/v1/jiathis1.gif" border="0" />
						</a>
						<a class="jiathis_counter_style_margin:3px 0 0 2px"></a>
					</div>
				</div>
				<script type="text/javascript"> 
					var jiathis_config = { 
						url: '<?php echo $this->app88common->js_quote($link.'&'.createShareUrlParam($param)); ?>', 
						title: '<?php echo $this->app88common->js_quote($this->app88param->getShopName() . ' - ' . $job['Title']); ?>',
						sm: '<?php echo APP88_JIATHIS_SERVICES; ?>',
					    siteNum: <?php echo APP88_JIATHIS_SITENUM; ?>,
						hideMore: true	  
					};
				</script> 
				<script type="text/javascript" src="http://v3.jiathis.com/code/jia.js" charset="utf-8"></script>		
			<?php } ?>
			<?php if (APP88_ADDTHIS_ENABLE || (APP88_JIATHIS_ENABLE && $this->app88param->getLang() == 'en')) { ?>
				<?php $addThisLangCode = $this->config->item('front_addthis_lang'); ?>
				<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4eb0b8c21c603178"></script>			
				<script type="text/javascript">
					var addthis_config = {
						ui_language: '<?php echo $addThisLangCode[$this->app88param->getLang()];?>',
						services_compact: '<?php echo APP88_ADDTHIS_SERVICES; ?>',
						data_track_addressbar: false,
						data_track_clickback: false,
						ui_delay: 500
					};
					var addthis_share = {
						url: '<?php echo $this->app88common->js_quote($link.'&'.createShareUrlParam($param)); ?>',
						title: '<?php echo $this->app88common->js_quote($this->app88param->getShopName() . ' - ' . $job['Title']); ?>'
					};
				</script>
				<div class="share_ui">
					<a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;pubid=xa-4eb0b8c21c603178&amp">
						<img src="/images/front_end/share.png" width="125" height="19.5" alt="Bookmark and Share" style="border:0"/>
					</a>
				</div>
			<?php } ?>
				<a class="dbShop-ui-getlink" href="javascript:void(0);">
					<span class="dbShop-ui-inner_text_small"><?php echo $this->lang->line('btn_get_link'); ?></span>
					<span class="dbShop-ui-getlinkIco"></span>
				</a>
			</div>
			
			<div style="clear:both"></div>
		</div>
		
		<div class="tabs-Btn">	
			<?php if (false) { ?>
			<a class="itemBtn-red" href="#"><?php echo $this->lang->line('add_job_label'); ?></a>
			<?php } ?>
			<a class="dbShop-ui-backlink" href="<?php echo $back_url; ?>">
				&lt;&lt; <?php echo $this->lang->line('back_to_job_listing_label'); ?>
			</a>
		</div>
	</div>

</div>
<?php $this->load->view("front/common/footer");?>