<?php
	$this->load->view('admin/common/header');
	$datepicker_lang = $this->config->item('admin_setting_datepicker_lang');
	function reformatDate($date)
	{
		return date(APP88_DATE_FORMAT, strtotime($date));
	}
?>
<style>
.catalogue-general-center{text-align:left}
</style>
<script type="text/javascript" src="/s/v<?php echo VERSION; ?>/js/jquery_ui/ui/i18n/jquery.ui.datepicker-<?php echo $datepicker_lang[$this->app88common->get_shop_lang()];?>.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$.datepicker.setDefaults({
		yearSuffix: ''
	});
	$( "#from" ).datepicker({
		minDate: new Date(2012,1-1,1),
		maxDate: "+0",
		dateFormat: "<?php echo APP88_DATEPICKER_DATE_FORMAT; ?>",
		defaultDate: "+0",
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: "both",
		buttonImage: "/images/back_end/calendar.gif",
		buttonImageOnly: true,
		buttonText: "",
		onClose: function( selectedDate ) {
			$( "#to" ).datepicker( "option", "minDate", selectedDate );
		},
		monthNamesShort: <?php echo $this->config->item('system_datepicker_month_name_short'); ?>
	});
	$( "#to" ).datepicker({
		minDate: new Date(2012,1-1,1),
		maxDate: "+0",
		defaultDate: "+0",
		dateFormat: "<?php echo APP88_DATEPICKER_DATE_FORMAT; ?>",
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		showOn: "both",
		buttonImage: "/images/back_end/calendar.gif",
		buttonImageOnly: true,
		buttonText: "",
		onClose: function( selectedDate ) {
			$( "#from" ).datepicker( "option", "maxDate", selectedDate );
		},
		monthNamesShort: <?php echo $this->config->item('system_datepicker_month_name_short'); ?>
	});

	$( "#status_type, #to, #from" ).keypress(function(e) {
		if(e.which == 13) {
			updateList();
		}
	});
});
function updateList()
{
	var url = '<?php echo site_url("/admin/shoot/index/1"); ?>'+
	'&from='+encodeURIComponent($("#from").val())+
	'&to='+encodeURIComponent($("#to").val())+
	'&status_type='+encodeURIComponent($("#status_type option:selected").val())+
	'&sortfield='+encodeURIComponent($("input#sortfield").val())+
	'&sortorder='+encodeURIComponent($("input#sortorder").val());
	window.location.href = url;
}

function clearList()
{
	$('#status_type').val('');
	$('#to').val('');
	$('#from').val('');
}
function sort(field,order)
{
	$('input#sortfield').val(field);
	$('input#sortorder').val(order);
	updateList();
}
</script>
<style>
img.ui-datepicker-trigger {margin-left:5px;margin-right:5px;}
.catalogue-listing-ttl{float:none;text-align:center}
</style>
<div id="main-content">
	<div class="pageTitle">
		<h2 class="title-order"><?php echo $page_title;?></h2>
		<?php 
			if($error) $this->common->showError($error);
			if($info) $this->common->showInfo($info);
		?>
	</div>
	
	<div class="Action">
		<div class="pageAdd fl"></div>
		<div class="pageBlank"></div>
		<div class="pageBlank-mid">
			<div style="float:right">
				<?php if (FALSE) { ?><div style="margin-left:20px;float:left;color:#777;"><img src="/images/reshoot_ad_Btn.png" class="dbShop-ui-keyIcon"/><span style="padding-left:5px"><?php echo $this->lang->line('btn_reshoot_ad');?></span></div><?php } ?>
				<div style="margin-left:20px;float:left;color:#777;"><img src="/images/add_view_Btn.png" class="dbShop-ui-keyIcon"/><span style="padding-left:5px"><?php echo $this->lang->line('btn_view');?></span></div>
			</div>
			<?php //echo form_open($form_action_url,array("method"=>"get"));?>
			<div style="float:left">
			<label for="status_type"><?php echo $this->lang->line('admin_view_status_label');?></label>
			<select name="status_type" id="status_type">
			<option value=""
			<?php 
				if ($form_data['status_type']==null || $form_data['status_type']=='') echo 'selected="selected"';
			?>
			><?php echo $this->lang->line('all_label');?></option>
			<option value="P"
			<?php
				if ($form_data['status_type']=='P') echo 'selected="selected"';
			?>
			><?php echo $this->lang->line('admin_view_sag_status_pending_label') ?></option>
			<option value="S"
			<?php 
				if ($form_data['status_type']=='S') echo 'selected="selected"';
			?>
			><?php echo $this->lang->line('admin_view_sag_status_success_label') ?></option>
			<option value="E"
			<?php 
				if ($form_data['status_type']=='E') echo 'selected="selected"';
			?>
			><?php echo $this->lang->line('admin_view_sag_status_error_label') ?></option>
			</select>
			<label for="from"><?php echo $this->lang->line('admin_view_from_label');?></label>
			<input type="text" id="from" readonly="readonly" name="from" value="<?php echo $form_data['from'];?>" style="width:100px"/>
			<label for="to"><?php echo $this->lang->line('admin_view_to_label');?></label>
			<input type="text" id="to" readonly="readonly" name="to" value="<?php echo $form_data['to'];?>" style="width:100px"/>
			<input type="hidden" id="sortfield" name="sortfield" value="<?php echo $form_data['sortfield'];?>"/>
			<input type="hidden" id="sortorder" name="sortorder" value="<?php echo $form_data['sortorder'];?>"/>
			<a href="#" style="display:inline;float:none;" class="dbShop-ui-appBtn dbShop-ui-apply" onclick="updateList();"><?php echo $this->lang->line('admin_view_submit_label'); ?></a>
			<a href="#" style="display:inline;float:none;" class="dbShop-ui-appBtn dbShop-ui-apply" onclick="clearList();"><?php echo $this->lang->line('btn_clear'); ?></a>
			</div>
			<?php //echo form_close();?>
		</div>
		<div class="pageBlank"></div>
	</div>
	
	<div class="pageData">
		<table id="pemesanan-table-grid" class="datagrid2" width="100%">
			<thead>
				<tr><th class="catalogue-listing-ttl-d1" width="20"><?php echo $this->lang->line('admin_page_title_job'); ?>
				<?php 
					if ($form_data['sortfield']!='productname') {
				?>
				<div style="background:white;width:16px;height:16px;display:inline-block;vertical-align:middle;"><a href="javascript:sort('productname','asc')"><span class="ui-icon ui-icon-triangle-2-n-s"></span></a></div>
				<?php
					} else if ($form_data['sortorder']=='asc') {
				?>
				<div style="background:white;width:16px;height:16px;display:inline-block;vertical-align:middle;"><a href="javascript:sort('productname','desc')"><span class="ui-icon ui-icon-triangle-1-n"></span></a></div>
				<?php 
					} else {
				?>
				<div style="background:white;width:16px;height:16px;display:inline-block;vertical-align:middle;"><a href="javascript:sort('productname','asc')"><span class="ui-icon ui-icon-triangle-1-s"></span></a></div>
				<?php 
					}
				?>
				</th>
				<th class="catalogue-listing-ttl-d2"><?php echo $this->lang->line('admin_view_sag_channel_label'); ?>
				<?php 
					if ($form_data['sortfield']!='channelname') {
				?>
				<div style="background:white;width:16px;height:16px;display:inline-block;vertical-align:middle;"><a href="javascript:sort('channelname','asc')"><span class="ui-icon ui-icon-triangle-2-n-s"></span></a></div>
				<?php
					} else if ($form_data['sortorder']=='asc') {
				?>
				<div style="background:white;width:16px;height:16px;display:inline-block;vertical-align:middle;"><a href="javascript:sort('channelname','desc')"><span class="ui-icon ui-icon-triangle-1-n"></span></a></div>
				<?php 
					} else {
				?>
				<div style="background:white;width:16px;height:16px;display:inline-block;vertical-align:middle;"><a href="javascript:sort('channelname','asc')"><span class="ui-icon ui-icon-triangle-1-s"></span></a></div>
				<?php 
					}
				?>
				</th>
				<th class="catalogue-listing-ttl-d3"><?php echo $this->lang->line('admin_view_sag_target_label'); ?>
				<?php 
					if ($form_data['sortfield']!='target') {
				?>
				<div style="background:white;width:16px;height:16px;display:inline-block;vertical-align:middle;"><a href="javascript:sort('target','asc')"><span class="ui-icon ui-icon-triangle-2-n-s"></span></a></div>
				<?php
					} else if ($form_data['sortorder']=='asc') {
				?>
				<div style="background:white;width:16px;height:16px;display:inline-block;vertical-align:middle;"><a href="javascript:sort('target','desc')"><span class="ui-icon ui-icon-triangle-1-n"></span></a></div>
				<?php 
					} else {
				?>
				<div style="background:white;width:16px;height:16px;display:inline-block;vertical-align:middle;"><a href="javascript:sort('target','asc')"><span class="ui-icon ui-icon-triangle-1-s"></span></a></div>
				<?php 
					}
				?>
				</th>
				<th class="catalogue-listing-ttl-d4"><?php echo $this->lang->line('admin_view_sag_post_date_label'); ?>
				<?php 
					if ($form_data['sortfield']!='postdate') {
				?>
				<div style="background:white;width:16px;height:16px;display:inline-block;vertical-align:middle;"><a href="javascript:sort('postdate','asc')"><span class="ui-icon ui-icon-triangle-2-n-s"></span></a></div>
				<?php
					} else if ($form_data['sortorder']=='asc') {
				?>
				<div style="background:white;width:16px;height:16px;display:inline-block;vertical-align:middle;"><a href="javascript:sort('postdate','desc')"><span class="ui-icon ui-icon-triangle-1-n"></span></a></div>
				<?php 
					} else {
				?>
				<div style="background:white;width:16px;height:16px;display:inline-block;vertical-align:middle;"><a href="javascript:sort('postdate','asc')"><span class="ui-icon ui-icon-triangle-1-s"></span></a></div>
				<?php 
					}
				?>
				</th>
				<th class="catalogue-listing-ttl-d5"><?php echo $this->lang->line('admin_view_sag_status_label'); ?>
				<?php 
					if ($form_data['sortfield']!='status') {
				?>
				<div style="background:white;width:16px;height:16px;display:inline-block;vertical-align:middle;"><a href="javascript:sort('status','asc')"><span class="ui-icon ui-icon-triangle-2-n-s"></span></a></div>
				<?php
					} else if ($form_data['sortorder']=='asc') {
				?>
				<div style="background:white;width:16px;height:16px;display:inline-block;vertical-align:middle;"><a href="javascript:sort('status','desc')"><span class="ui-icon ui-icon-triangle-1-n"></span></a></div>
				<?php 
					} else {
				?>
				<div style="background:white;width:16px;height:16px;display:inline-block;vertical-align:middle;"><a href="javascript:sort('status','asc')"><span class="ui-icon ui-icon-triangle-1-s"></span></a></div>
				<?php 
					}
				?>
				</th>
				<th class="catalogue-listing-ttl-d5"><?php echo $this->lang->line('admin_view_action_label'); ?></th>
			</tr></thead>
			
			<tbody>
				<?php 
				if (empty($statuses))
				{
					echo '<td class="menu-error" colspan="6">'.$this->lang->line('no_data_label').'</td>';
				}
				elseif (is_array($statuses)) {

					foreach($statuses as $status)
					{
				?>

						<tr>
							<td class="catalogue-general-center"><?php echo form_prep($status['Title']); ?></td>
							<td class="catalogue-general-center"><?php echo form_prep($status['ChannelName']); ?></td>
							<td class="catalogue-general-center"><?php echo form_prep($status['TargetName']); ?></td>
							<td class="catalogue-general-center"><?php echo reformatDate(form_prep($status['PostDate'])); ?></td>
							<td class="catalogue-general-center">
							<?php
								switch ($status['Status']) {
									case 'P':
										echo $this->lang->line('admin_view_sag_status_pending_label');
										break;
									case 'S':
										echo $this->lang->line('admin_view_sag_status_success_label');
										break;
									case 'E':
										echo $this->lang->line('admin_view_sag_status_error_label') .'<br />('. $status['Message'] .')';
										break;
									default:
										echo $this->lang->line('admin_view_sag_status_error_label');
								}
							?></td>
							
							<td class="catalogue-opt" style="text-align:center">
								<div class="menu-link" style="display:inline-block">
									<?php if (FALSE) { ?>
									<a title="<?php echo $this->lang->line('btn_reshoot_ad');?>" class="dbShop-ui-appBtn dbShop-ui-reshootad_action" href="#" onclick="deleteDialogOpen(event,'<?php echo form_prep($this->app88common->js_quote($this->lang->line('admin_view_sag_repost_message'))); ?>','<?php echo site_url('admin/shoot/repost/'.$status['Guid']); ?>');"></a>
									<?php } ?>
									<?php if ($status['PostUrl'] != '') { ?>
									<a title="<?php echo $this->lang->line('btn_view');?>" class="dbShop-ui-appBtn dbShop-ui-view_action" href="<?php echo $status['PostUrl']; ?>" target="_blank"></a>
									<?php } ?>
									<div style="clear:both"></div>
								</div>
							</td>
						</tr>
				<?php 
					}
				}
				?>
			</tbody>
			
			<tfoot>
				<tr>
					<td class="category-pagination" colspan="6"><?php echo $this->lang->line('page_label'); ?>:
						<?php
							for($i=1;$i<=$max_page;$i++)
							{
								if ($i==$page)
								{
						?>
						&nbsp;&nbsp;<span class="product-paging"><strong style="color:red"><?php echo $i; ?></strong></span>
						<?php 
								}
								else
								{
						?>
						&nbsp;&nbsp;<span class="product-paging"><a href="<?php echo site_url('admin/shoot/index/'.$i.'?status_type='.rawurlencode($form_data['status_type']).'&from='.rawurlencode($form_data['from']).'&to='.rawurlencode($form_data['to']).'&sortfield='.rawurlencode($form_data['sortfield']).'&sortorder='.rawurlencode($form_data['sortorder'])); ?>"><?php echo $i; ?></a></span>
						<?php 
								}
							}
						?>			
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
<?php $this->load->view('admin/common/footer');?>