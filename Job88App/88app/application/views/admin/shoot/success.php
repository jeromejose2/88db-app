<?php
$this->load->view('admin/common/header');
?>

<div id="main-content">
	<div class="pageTitle">
		<h2 class="">
			<?php echo $this->lang->line('admin_page_title_add_advertisement') ?>
		</h2>
	</div>
	<div class="pageAction">

		<div class="formLoader">
			<div class="dbShop-ui-rel">
				<p class="ganti-nama" style="dispaly:block">
					<label style="width:700px;" ><?php echo sprintf($this->lang->line('admin_add_sag_complete_msg'), form_prep($target)); ?></label>
				</p>				
			</div>
			
			<p class="submit-btn" style="margin: 0px">
				<a href="<?php echo site_url('admin/job/index'); ?>" class="dbShop-ui-appBtn dbShop-ui-plain-core"><?php echo $this->lang->line('sag_post_order_label'); ?></a>
				<a href="<?php echo site_url('admin/shoot/index'); ?>" style="margin-left:20px;" class="dbShop-ui-appBtn dbShop-ui-plain-core"><?php echo $this->lang->line('shoot_ad_history_label'); ?></a>	
			</p>
		</div>
	</div>

</div>

<?php $this->load->view('admin/common/footer');?>