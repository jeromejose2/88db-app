<?php

$this->load->view('admin/common/header');
?>
<?php //echo '<pre>'; print_r($shoot_targets); echo '</pre>';?>
<script type="text/javascript">
	var sag = {
		data: <?php echo json_encode($shoot_targets); ?>,
		init: function() {
			if ('88db' in sag.data) {
				for (var t in sag.data['88db']['channels']) {
					$('#88db_category').append('<option value="'+t+'">'+sag.data['88db']['channels'][t]['name']+'</option>');
			    }
				$('#88db_category').change(function(){
					$('#88db_shoot_channel').html('');
					if ($(this).val() != '')
					{
						for (var c in sag.data['88db']['channels'][$(this).val()]['child']) {
							if (sag.data['88db']['channels'][$(this).val()]['child'][c]['paid_only'] == 1)
							{
								$('#88db_shoot_channel').append('<option value="'+c+'@88db">'+sag.data['88db']['channels'][$(this).val()]['child'][c]['name']+' <?php echo $this->app88common->js_quote($this->lang->line('admin_view_sag_paid_only_label')); ?></option>');
							}
							else
							{
								$('#88db_shoot_channel').append('<option value="'+c+'@88db">'+sag.data['88db']['channels'][$(this).val()]['child'][c]['name']+'</option>');
							}
					    }
					}					
				});
			}

			$('#selectQuota .cancelBtn, #confirmQuota .cancelBtn, #purchaseQuota .cancelBtn').click(function(){
				sag.closeDialog();
			});

			$('#selectQuota .confirmBtn').click(function(){
				sag.closeDialog();
				sag.selectQuotaConfirm();
			});
			$('#confirmQuota .confirmBtn').click(function(){
				sag.closeDialog();
				sag.confirmQuotaConfirm();
			});

			$('.dbShop-ui-save').click(sag.channelSelected);		
			
		},
		channelSelected: function() {
			if ($('#sagform select[name=shoot_channel]').val())
			{
				$.blockUI({overlayCSS:{backgroundColor:'#ffffff',opacity:'.0'}, message:null});
				$.ajax({
					type: 'POST',
					data: 'channel='+$('#sagform select[name=shoot_channel]').val()+'&target='+$('#sagform input[name=shoot_target]').val(),
					url: '<?php echo site_url("admin/shoot/getQuota"); ?>'
				}).done(function(rtn) {
					var response = JSON.parse(rtn);
					if (response == 'error')
					{
						alertDialogOpen({'clientY':200},'<?php echo $this->app88common->js_quote($this->lang->line('error_message')); ?>');
					}
					else
					{
						if ((response.Success == 'true') && (response.ErrorCode == '0'))
						{
							if (response.OptionList == '')
							{
								shop88api.open88DBQuotaDialog();
							}
							else if ($.isPlainObject(response.OptionList.Option))
							{
								var simpleWindow;
								if ($("#windownbg").length == 0) {
									simpleWindow = $("<div id=\"windownbg\" style=\"height:" + $(document).height() + "px;filter:alpha(opacity=50);opacity:0.5;z-index: 100\"></div>");
									$("body").append(simpleWindow);
								} else {
									simpleWindow = $("#windownbg");
								}
								
								var dialog = $( "#confirmQuota" );
								dialog.find('th.cat-Ar span').html(response.Header.Fields.Field[0]);
								dialog.find('th.sub-Ar span').html(response.Header.Fields.Field[1]);
								dialog.find('th.duration-A span').html(response.Header.Fields.Field[2]);
								dialog.find('th.expiry-A span').html(response.Header.Fields.Field[3]);
	
								dialog.find('tbody').html('');
								var html = '';
								html += '<tr class="d-container">';
								html += '	<td class="r-cat-A"><span>'+response.OptionList.Option.Values.Value[0]+'</span></td>';
								html += '	<td class="r-sub-A"><span>'+response.OptionList.Option.Values.Value[1]+'</span></td>';
								html += '	<td class="r-duration-A"><span>'+response.OptionList.Option.Values.Value[2]+'</span></td>';
								var expiry = new Date(response.OptionList.Option.Values.Value[3]);
								html += '	<td class="r-expiry-A"><span>'+$.datepicker.formatDate('<?php echo APP88_DATEPICKER_DATE_FORMAT; ?>', expiry)+'</span></td>';
								html += '</tr>';
							
								dialog.find('tbody').append(html);
							
								
								dialog.css("left", ((dialog.parent().width() - dialog.width()) / 2) + "px");
								dialog.css("top", "80px");
	
								$('#sagform input[name=quota]').val(response.OptionList.Option.Id);
								
								simpleWindow.show();
								dialog.show();
							}
							else
							{
								var simpleWindow;
								if ($("#windownbg").length == 0) {
									simpleWindow = $("<div id=\"windownbg\" style=\"height:" + $(document).height() + "px;filter:alpha(opacity=50);opacity:0.5;z-index: 100\"></div>");
									$("body").append(simpleWindow);
								} else {
									simpleWindow = $("#windownbg");
								}
								
								var dialog = $( "#selectQuota" );
								dialog.find('th.cat-A span').html(response.Header.Fields.Field[0]);
								dialog.find('th.sub-A span').html(response.Header.Fields.Field[1]);
								dialog.find('th.duration-A span').html(response.Header.Fields.Field[2]);
								dialog.find('th.expiry-A span').html(response.Header.Fields.Field[3]);
	
								dialog.find('tbody').html('');
								for (i=0;i<response.OptionList.Option.length;i++)
								{
									var html = '';
									html += '<tr class="d-container">';
									if (i==0)
										html += '	<td class="r-check-A"><input type="radio" name="qouta_id" value="'+response.OptionList.Option[i].Id+'" checked="checked"></td>';
									else
										html += '	<td class="r-check-A"><input type="radio" name="qouta_id" value="'+response.OptionList.Option[i].Id+'"></td>';
									html += '	<td class="r-cat-A"><span>'+response.OptionList.Option[i].Values.Value[0]+'</span></td>';
									html += '	<td class="r-sub-A"><span>'+response.OptionList.Option[i].Values.Value[1]+'</span></td>';
									html += '	<td class="r-duration-A"><span>'+response.OptionList.Option[i].Values.Value[2]+'</span></td>';
									var expiry = new Date(response.OptionList.Option[i].Values.Value[3]);
									html += '	<td class="r-expiry-A"><span>'+$.datepicker.formatDate('<?php echo APP88_DATEPICKER_DATE_FORMAT; ?>', expiry)+'</span></td>';
									html += '</tr>';
								
									dialog.find('tbody').append(html);
								}
								
								dialog.css("left", ((dialog.parent().width() - dialog.width()) / 2) + "px");
								dialog.css("top", "80px");
	
								simpleWindow.show();
								dialog.show();
								
							}
						}
					}
					$.unblockUI();
				});
				
			}
			else
			{
				alertDialogOpen({clientY: '200'}, '<?php echo $this->app88common->js_quote($this->lang->line('admin_view_sag_select_category_message'))?>');
			}
		},
		closeDialog: function() {
			$("#selectQuota, #purchaseQuota, #confirmQuota").hide();
			$("#windownbg").hide('fast');
		},
		selectQuotaConfirm: function () {
			$('#sagform input[name=quota]').val($('#selectQuota input[name=qouta_id]:checked').val());
			sag.submitForm();
		},
		confirmQuotaConfirm: function () {
			sag.submitForm();
		},
		submitForm: function () {
			$('#sagform').submit();
		}
	};

	$(document).ready(function() {
		sag.init();
	});
	
</script>

<div id="main-content">
	<div class="pageTitle">
		<h2 class="">
			<?php echo $this->lang->line('admin_page_title_add_advertisement') ?>
		</h2>
	</div>
	<?php echo form_open($form_action,array('id'=>'sagform')); ?>
	<input type="hidden" name="quota" value="" />	
	<div class="pageAction">
		<?php 
		$this->common->showError($error);
		$this->common->showInfo($info);
		?>

		<div class="button-panel" style="padding-bottom: 10px;">
			<a href="#" class="dbShop-ui-appBtn dbShop-ui-save"><?php echo $this->lang->line('post_label');?></a>
			<a class="dbShop-ui-appBtn dbShop-ui-back dbShop-ui-Btn-right" href="<?php echo site_url('admin/job/index') ?>">
				<?php echo $this->lang->line('btn_back');?>
			</a>
		</div>
		
		<div class="formLoader">
			<p class="ganti-nama" style="dispaly:block">
				<label style="width:100px;" ><?php echo $this->lang->line('admin_view_sag_item_name_label'); ?>:</label>
				<label><?php echo form_prep($job_title); ?></label>
			</p>
			
			<p class="ganti-nama" style="dispaly:block">
				<label style="width:100px;"><?php echo $this->lang->line('admin_view_sag_target_label'); ?>:</label>
				<label><?php echo form_prep($shoot_targets['88db']['name']); ?><input type="hidden" name="shoot_target" value="88db" /></label>
			</p>	 
				 
			<div class="dbShop-ui-selector" style="margin-top:0px;">
				<p style="font-size:10pt;font-weight:normal;"><?php echo $this->lang->line('admin_view_sag_category_label'); ?>:</p>
				<select size="10" name="88db_category" id="88db_category" class="FormSelectBoxM" style="height:200px;width:100%;margin-top:0px;">
					
				</select>
			</div>

			<div class="dbShop-ui-selector_btn" style="margin-top:120px;"><img src="/images/back_end/arrow_right.gif" width="37" height="29"></div>

			<div class="dbShop-ui-selector" style="margin-top:0px;">
				<p style="font-size:10pt;font-weight:normal;"><?php echo $this->lang->line('admin_view_sag_subcategory_label'); ?>:</p>
				<select size="10" name="shoot_channel" id="88db_shoot_channel" class="FormSelectBoxM" style="height:200px;width:100%;margin-top:0px;">
					
				</select>
			</div>	 
	
			<div style="clear:both;"></div>
		</div>			
	</div>

	<div class="button-panel" style="margin-top:10px;margin-bottom:10px;">
		<a href="#" class="dbShop-ui-appBtn dbShop-ui-save"><?php echo $this->lang->line('post_label');?></a>
		<a class="dbShop-ui-appBtn dbShop-ui-back dbShop-ui-Btn-right" href="<?php echo site_url('admin/job/index'); ?>">
			<?php echo $this->lang->line('btn_back');?>
		</a>
	</div>
	<?php echo form_close(); ?>
</div>

<div id="selectQuota" class="dbShop-ui-nlayer" style="display:none;width:590px;z-index: 999;position:absolute;left:0;top:0;">
	<h2><?php echo $this->lang->line('admin_view_sag_select_quota_title'); ?></h2>
	<div class="dbShop-ui-wrapper20" style="padding:10px;">
		<table class="datagrid-pop" >
			<thead>
				<tr>
					<th class="check-A"></th>
					<th class="cat-A"><span></span></th>
					<th class="sub-A"><span></span></th>
					<th class="duration-A"><span></span></th>
					<th class="expiry-A"><span></span></th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
	<div class="dbShop-ui-btnArea"><a class="msBtn dbShop-ui-red confirmBtn" href="javascript:void(0);"><?php echo $this->lang->line('confirm_label'); ?></a><a class="msBtn dbShop-ui-grey cancelBtn" href="javascript:void(0);"><?php echo $this->lang->line('cancel_label'); ?></a></div>
</div>



<div id="confirmQuota" class="dbShop-ui-nlayer" style="display:none;width:590px;z-index: 999;position:absolute;left:0;top:0;">
	<h2><?php echo $this->lang->line('admin_view_sag_confirm_quota_title'); ?></h2>
	<div class="dbShop-ui-wrapper20" style="padding:10px;">
		<table class="datagrid-pop" >
			<thead>
				<tr>
					<th class="cat-Ar"><span></span></th>
					<th class="sub-Ar"><span></span></th>
					<th class="duration-A"><span></span></th>
					<th class="expiry-A"><span></span></th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	<div class="dbShop-ui-btnArea"><a class="msBtn dbShop-ui-red confirmBtn" href="javascript:void(0);"><?php echo $this->lang->line('confirm_label'); ?></a><a class="msBtn dbShop-ui-grey cancelBtn" href="javascript:void(0);"><?php echo $this->lang->line('cancel_label'); ?></a></div>
</div>


<?php $this->load->view('admin/common/footer');?>