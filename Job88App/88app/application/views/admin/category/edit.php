<?php 
	$this->load->view('admin/common/header');
	function printCategorySelect($category_array,$level=0,$current_id,$parent_id)
	{
		if (is_array($category_array))
		{
			foreach($category_array as $category)
			{
						if (($category['Id'] != $current_id)) {?>
						<option value="<?php echo $category['Guid'];?>"
						<?php
							if ($category['Guid'] == $form_data['parent_category'])
							{
								echo ' selected="true"';
							} 
							elseif ($category['Id'] == $parent_id)
							{
								echo ' selected="true"';
							}
							echo ' style="font-weight:bold;'.'padding-left:'.($level*10).'px;"';
						?>>
						<?php 
							echo form_prep($category['Name']);
						?>
						</option>
						<?php
						if($category['Subcategory']) printCategorySelect($category['Subcategory'],$level+1,$current_id,$parent_id);
						}
			}
		}
	}
?>
<script type="text/javascript" src="/s/v<?php echo VERSION; ?>/js/jquery.remainCount.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('.txtMessageContent').each(function() {
		var maxText = 0;
		maxText = '<?php echo APP88_CATEGORY_NAME_MAX_LENGTH; ?>';
		var showInOj = $(this).closest('.dbShop-ui-rel').find('.dbShop-ui-tipsCnt');
		var remainStr = "<?php echo $this->app88common->js_quote($this->lang->line('label_content_length_limit')); ?>";
		$(this).remainCount(showInOj, maxText, remainStr);
	});
	
	$('.txtMessageContent').bind({
		'focus':function()
		{
			$(this).closest('.dbShop-ui-rel').find(".dbShop-ui-mToolTips").show();
		},
		'focusout':function()
		{
			$(this).closest('.dbShop-ui-rel').find(".dbShop-ui-mToolTips").hide();
		}
	});
});
</script>
<div id="main-content">
	<div class="pageTitle">
		<h2 class="title-edit-category"><?php echo $page_title;?></h2>
	</div>

	<div class="pageAction">
		<?php 
			$this->common->showError($error);
		?>
		<div class="blank"></div>
		<div class="formLoader">
			<?php echo form_open($form_action_url,array('id'=>'editform')); ?>
				
				<div class="dbShop-ui-rel">
					<p class="ganti-nama">
						<label for="category_name"><span class="compulsory">*</span><?php echo $this->lang->line('admin_view_category_name_label'); ?>:</label>
						<input id="category_name" class="txtMessageContent" name="category_name" size="35" value="<?php 
						if($form_data['category_name']) echo form_prep($form_data['category_name']);
						else echo form_prep($category_info['Name']);
						?>" type="text">
						<span class="contoh-isi"><em><?php echo sprintf($this->lang->line('err_category_name_too_long'),APP88_CATEGORY_NAME_MAX_LENGTH); ?></em></span>
					</p>
					
					<?php if (isset($field_errs) && isset($field_errs['category_name'])) { ?>
                    <div id="category_name_err_bubble" class="dbShop-ui-eMsg dbShop-ui-eMsgTRt" style="left:200px; top:20px; display:block;">
						<div class="dbShop-ui-eMsgPtr"></div>
						<div class="dbShop-ui-eMsgCnt">
						<?php 
							foreach ($field_errs['category_name'] as $e)
							{
								echo $e . "<br>"; 
							}
						?>
						</div>
					</div>
					<?php } ?>
					<div class="dbShop-ui-mToolTips dbShop-ui-mTipsTRt" style="left:220px; top:30px; display:none;">
						<div class="dbShop-ui-tipsPtr"></div>
						<div class="dbShop-ui-tipsCnt"></div>
					</div>
				</div>
				
				<p></p>

				<p class="submit-btn">
					<a href="#" class="dbShop-ui-appBtn dbShop-ui-save" onclick="document.getElementById('editform').submit();"><?php echo $this->lang->line('save_label');?></a>
					<a href="<?php echo site_url('admin/category/index') ?>" class="dbShop-ui-appBtn dbShop-ui-back" style="margin-left:15px"><?php echo $this->lang->line('back_label');?></a>
				</p>
				<input type="hidden" value="1" name="published">
			<?php echo form_close(); ?>
		</div>
	</div>
</div>

<?php $this->load->view('admin/common/footer');?>