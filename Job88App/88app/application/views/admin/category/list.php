<?php $this->load->view('admin/common/header');?>
<?php
$btn_text[] = $this->lang->line('btn_edit');
$btn_text[] = $this->lang->line('add_job_label');
$btn_text[] = $this->lang->line('btn_delete');
$btn_text[] = $this->lang->line('admin_view_subcategory_label');
/*
 * -this function is used to print the html of the category table
 * -input the category array, the $level is used to indentent the subcategory
 */
function printCategoryTable($category_array,$msg,$cannot_del_msg,$level=0,$category_sub_level=0,$keyword,$btn_text)
{
	if(is_array($category_array))
	{
		$counter = 0;
		$length = count($category_array)-1;
		foreach($category_array as $category)
		{?>
				<li id="cat_<?php echo $category['Guid'] ?>">
					<div class="catalogue-ttl" style="
					<?php 
					//if ($level>0) echo "padding-left:".($level*20)."px";
					?>">
					<span  class="category-name" style="cursor:default;">
					<?php 
					if ($category['Published'] == 0) echo "<span style='font-style:italic;color:#999;' title='Not Published'>";
					echo form_prep($category['Name']);
					if ($category['Published'] == 0) echo "</span>"?>
					</span>
					<?php if($length==0) { ?>
						<a href="#" class="dbShop-ui-sorMoveUpBtnDis"></a><a href="#" class="dbShop-ui-sorMoveDnBtnDis"></a>
					<?php } elseif($counter==0) { ?>
						<a href="#" class="dbShop-ui-sorMoveUpBtnDis"></a><a href="#" class="dbShop-ui-sorMoveDnBtn"></a>
					<?php } elseif($counter < $length) { ?>
						<a href="#" class="dbShop-ui-sorMoveUpBtn"></a><a href="#" class="dbShop-ui-sorMoveDnBtn"></a>
					<?php } else {?>
						<a href="#" class="dbShop-ui-sorMoveUpBtn"></a><a href="#" class="dbShop-ui-sorMoveDnBtnDis"></a>
					<?php } ?>
					</div>
					<div class="catalogue-opt">
						<div class="menu-link" style="display:inline-block">
							<?php 
							if ($level<$category_sub_level) 
							{
								if (isset($keyword) && $category['Level']<$category_sub_level)
								{
							?>
									<a title="<?php echo $btn_text[1].' '.$btn_text[3];?>" class="dbShop-ui-appBtn dbShop-ui-addcategory_action" href="<?php echo site_url("admin/category/add/".$category['Guid']); ?>"></a>
							<?php 
								}
								elseif (!isset($keyword))
								{	
							?>
									<a title="<?php echo $btn_text[1].' '.$btn_text[3];?>" class="dbShop-ui-appBtn dbShop-ui-addcategory_action" href="<?php echo site_url("admin/category/add/".$category['Guid']); ?>"></a>
							<?php
								}
								else
								{
							?>
									<div style="width:36px;float:left;">&nbsp;</div>
							<?php
								}
							}
							else
							{?>
							<div style="width:36px;float:left;">&nbsp;</div>
							<?php }?>
							<a title="<?php echo $btn_text[1];?>" class="dbShop-ui-appBtn dbShop-ui-addjob_action" href="<?php echo site_url("admin/job/add/".$category['Guid']); ?>"></a>
							<a title="<?php echo $btn_text[0];?>" style="margin-left:5px" class="dbShop-ui-appBtn dbShop-ui-addname_action" href="<?php echo site_url("admin/category/edit/".$category['Guid']); ?>"></a>
							<?php 
							if(empty($category['Subcategory']) && !$category['HasJobs']) {
							?>
							<a title="<?php echo $btn_text[2];?>" style="margin-left:5px" href="#" class="dbShop-ui-appBtn dbShop-ui-adddelete_action" onclick="deleteDialogOpen(event,'<?php echo $msg; ?>','<?php echo site_url("admin/category/delete/".$category['Guid']);?>');"></a>
							<?php 
							}
							else
							{
							?>
							<a title="<?php echo $btn_text[2];?>" style="margin-left:5px" href="#" class="dbShop-ui-appBtn dbShop-ui-adddelete_action" onclick="alertDialogOpen(event,'<?php echo $cannot_del_msg; ?>');"></a>
							<?php 
							}
							?>
							<div style="clear:both"></div>
						</div>
					</div>
					<div style="clear:both"></div>
					<?php if(!empty($category['Subcategory'])) { ?>
						<ul class="subSortable sortable">
						<?php if(!isset($keyword)) printCategoryTable($category['Subcategory'],$msg,$cannot_del_msg,$level+1,$category_sub_level,$keyword,$btn_text);?>
						</ul>
					<?php } ?>
				</li>
				<?php 
				$counter++;
		}
	}
}
?>
<script>
	
	$(function() {
		$('#category-table-grid').data('original_html',$('#category-table-grid').html());
		initSortable();
		$("ul.sortable").disableSelection();
		$("a.dbShop-ui-sorMoveUpBtn").click(function() {
			moveUp(this);
		});
		$("a.dbShop-ui-sorMoveDnBtn").click(function() {
			moveDown(this);
		});
		$('ul.sortable a.dbShop-ui-sorMoveUpBtn, ul.sortable a.dbShop-ui-sorMoveDnBtn').hover(function() {
			$(this).toggleClass('hover');
		}, function() {
			$(this).toggleClass('hover');
		});
	});

	var initSortable = function() {
		$("ul.sortable").sortable({
			placeholder: "ui-state-highlight",
			cursor: "n-resize",
			opacity: 0.8,
			distance: 5,
			axis: "y", 
			start: function(e, ui ){
				
				ui.placeholder.height(ui.helper.outerHeight());
			}
		});
		
		$("ul.sortable").bind("sortupdate",function(e, ui) {
			e.stopPropagation();
			$('#category-table-grid').block({ overlayCSS: { backgroundColor: '#fff',opacity:0 }, message: null }); 
			$.ajax({
				type: 'POST',
				data: 'csrf_token_name='+$.cookie('csrf_cookie_name')+'&'+$(this).sortable('serialize'),
				url: '<?php echo site_url("admin/category/sort"); ?>'
			}).done(function(rtn) {
				if (rtn == 'SUCCESS')
				{
					$('#category-table-grid').data('original_html',$('#category-table-grid').html());
				}
				else
				{
					alertDialogOpen('<?php echo $this->lang->line('error_category_sort_fail'); ?>');
					$("ul.sortable").sortable('destory');
					$('#category-table-grid').html($('#category-table-grid').data('original_html'));
					initSortable();
					
				}
				$("ul.sortable:eq(0)").children('li').each(function() {
					updateUpDnBtn(this,$('ul.sortable:eq(0) > li').size());
				});
				$('ul.sortable a.dbShop-ui-sorMoveUpBtn, ul.sortable a.dbShop-ui-sorMoveDnBtn').hover(function() {
					$(this).toggleClass('hover');
				}, function() {
					$(this).toggleClass('hover');
				});
				$('#category-table-grid').unblock();
			});
		});
	}

	function updateUpDnBtn(element,size)
	{
		var index = $(element).index();
		$(element).find('a:eq(0)').unbind().removeClass().end().find('a:eq(1)').unbind().removeClass();
		if(size == 1)
		{
			$(element).find('a:eq(0)').attr('class','dbShop-ui-sorMoveUpBtnDis').unbind().end().find('a:eq(1)').attr('class','dbShop-ui-sorMoveDnBtnDis').unbind();
		}
		else if(index == 0)
		{
			$(element).find('a:eq(0)').attr('class','dbShop-ui-sorMoveUpBtnDis').unbind().end().find('a:eq(1)').attr('class','dbShop-ui-sorMoveDnBtn').unbind();
			$(element).find('a:eq(1)').click(function() {
				moveDown(this);
			});
		}
		else if(index < size-1)
		{
			$(element).find('a:eq(0)').attr('class','dbShop-ui-sorMoveUpBtn').unbind().end().find('a:eq(1)').attr('class','dbShop-ui-sorMoveDnBtn').unbind();
			$(element).find('a:eq(0)').click(function() {
				moveUp(this);
			}).end().find('a:eq(1)').click(function() {
				moveDown(this);
			});
		}
		else
		{
			$(element).find('a:eq(0)').attr('class','dbShop-ui-sorMoveUpBtn').unbind().end().find('a:eq(1)').attr('class','dbShop-ui-sorMoveDnBtnDis').unbind();
			$(element).find('a:eq(0)').click(function() {
				moveUp(this);
			});
		}
		var childsize = $(element).children('ul').length;
		if(childsize > 0)
		{
			childsize = $(element).children('ul:eq(0)').children('li').length;
			$(element).children('ul').children('li').each(function() {
				updateUpDnBtn(this,childsize);
			});
		}
	}

	function moveUp(item)
	{
		$("a.dbShop-ui-sorMoveUpBtn, a.dbShop-ui-sorMoveUpBtn:hover").attr('class','dbShop-ui-sorMoveUpBtnDis');
		$("a.dbShop-ui-sorMoveDnBtn, a.dbShop-ui-sorMoveDnBtn:hover").attr('class','dbShop-ui-sorMoveDnBtnDis');
		var parent = $(item).parent().parent();
		var before = $(parent).prev();
		$(parent).insertBefore(before);
		$(parent).trigger("sortupdate");
	}

	function moveDown(item)
	{
		$("a.dbShop-ui-sorMoveUpBtn, a.dbShop-ui-sorMoveUpBtn:hover").attr('class','dbShop-ui-sorMoveUpBtnDis');
		$("a.dbShop-ui-sorMoveDnBtn, a.dbShop-ui-sorMoveDnBtn:hover").attr('class','dbShop-ui-sorMoveDnBtnDis');
		var parent = $(item).parent().parent();
		var after = $(parent).next();
		$(parent).insertAfter(after);
		$(parent).trigger("sortupdate");
	}


</script>
<style>
	.ui-state-highlight { height: 24px; line-height: 24px; background:#FFFFFF; border:2px dotted #CCCCCC; margin-bottom:3px;}
</style>
<div id="main-content">
	<div class="pageTitle">
		<h2><?php echo $page_title; ?></h2>
		<p class="dbShop-ui-content"><?php echo $this->lang->line('reorder_help_text');?></p>
		<?php 
			if ($error) $this->common->showError($error);
			if ($info) $this->common->showInfo($info);
		?>
	</div>
	
	<div class="Action">
			<div class="pageAdd fl">
				<a href="<?php echo site_url("admin/category/add");?>" class="dbShop-ui-appBtn dbShop-ui-addcategory"><?php echo $this->lang->line('btn_add');?></a>
			</div>
			<div style="float:right" class="pageBlank-mid">			
			<div style="float:left;color:#777;"><img class="dbShop-ui-keyIcon" src="/images/add_folder_Btn.png"><span style="padding-left:5px"><?php echo $this->lang->line('admin_page_title_addcategory'); ?></span></div>
			<div style="margin-left:20px;float:left;color:#777;"><img class="dbShop-ui-keyIcon" src="/images/addjob_Btn.png"><span style="padding-left:5px"><?php echo $this->lang->line('add_job_label'); ?></span></div>
			<div style="float:left;margin-left:20px;color:#777;"><img class="dbShop-ui-keyIcon" src="/images/add_name_Btn.png"><span style="padding-left:5px"><?php echo $this->lang->line('admin_page_title_editcategory'); ?></span></div>
			<div style="float:left;margin-left:20px;color:#777;"><img class="dbShop-ui-keyIcon" src="/images/add_delete_Btn.png"><span style="padding-left:5px"><?php echo $this->lang->line('btn_delete'); ?></span></div>
			<div style="clear:both"></div>
			</div>
		<div class="pageBlank"></div>
	</div>

	<div class="pageData">
		<table id="category-table-grid" class="datagrid2" width="100%">
			<thead>
				<tr>
					<th class="catalogue-listing-ttl" width="680"><?php echo $this->lang->line('admin_view_category_name_label'); ?></th>
					<th class="catalogue-listing-ttl" width="73"><?php echo $this->lang->line('reorder_label'); ?></th>
					<th class="catalogue-listing-ttl" width="155"><?php echo $this->lang->line('admin_view_action_label'); ?></th>
				</tr>
			</thead>
			<tbody>
			<?php 
			if (empty($category_array))
			{
				echo '<td class="menu-error" colspan="3">'.$this->lang->line('no_data_label').'</td>';
			}
			else
			{
				echo '<tr><td colspan="3">';
				echo '<ul class="sortable">';
				printCategoryTable($category_array,$this->lang->line('delete_category_msg'),$this->lang->line('cannot_delete_category_msg'),0,$category_sub_level,$keyword,$btn_text);
				echo '</ul>';
				echo '</td></tr>';
			}
			?>
			</tbody>
			<tfoot>
				<!-- <tr>
					<td class="category-pagination" colspan="3">Page:&nbsp;&nbsp;<span id="category-paging"><b>1</b></span></td>
				</tr>-->
			</tfoot>
		</table>
	</div>
	<?php
	if(!isset($keyword))
	{
	?>
	<div class="Action">
		<div class="pageAdd fl">
			<a href="<?php echo site_url("admin/category/add");?>" class="dbShop-ui-appBtn dbShop-ui-addcategory"><?php echo $this->lang->line('btn_add');?></a>
		</div>
		<div class="pageBlank">

		</div>
	</div>
	<?php 
	}
	?>
</div>


<?php $this->load->view('admin/common/footer');?>