<?php
$this->load->view('admin/common/header');
$datepicker_lang = $this->config->item('admin_setting_datepicker_lang');
function printCategorySelect($category_array,$level=0,$parent_id,$parent_guid)
{
	if (is_array($category_array))
	{
		foreach($category_array as $category)
		{
			?>
<option value="<?php echo $category['Id'];?>"
<?php
if ($category['Id'] == $parent_id || $category['Guid'] == $parent_guid)
{
	echo ' selected="selected"';
}
echo ' style="font-weight:bold;'.'padding-left:'.($level*10).'px;"';
?>>
	<?php 
	echo form_prep($category['Name']);
	?>
</option>
<?php
if($category['Subcategory']) printCategorySelect($category['Subcategory'],$level+1,$parent_id,$parent_guid);
		}
	}
}
?>
<script type="text/javascript" src="/s/v<?php echo VERSION; ?>/js/tinymce/jquery.tinymce.js"></script>
<script type="text/javascript" src="/s/v<?php echo VERSION; ?>/js/jquery_ui/ui/i18n/jquery.ui.datepicker-<?php echo $datepicker_lang[$this->app88common->get_shop_lang()];?>.js"></script>
<script type="text/javascript" src="/s/v<?php echo VERSION; ?>/js/jquery.remainCount.js"></script>
<script type="text/javascript">
	var jcrop_api, boundx, boundy;
	var frameid;
	var image_path;
	$(document).ready(function(){
		$.datepicker.setDefaults({
			yearSuffix: ''
		});
		
		$('.txtMessageContent').each(function() {
			var maxText = 0;
			maxText = '<?php echo APP88_JOB_TITLE_MAX_LENGTH; ?>';
			var showInOj = $(this).closest('.dbShop-ui-rel').find('.dbShop-ui-tipsCnt');
			var remainStr = "<?php echo $this->app88common->js_quote($this->lang->line('label_content_length_limit')); ?>";
			$(this).remainCount(showInOj, maxText, remainStr);
		});
		
		$('.txtMessageContent').bind({
			'focus':function()
			{
				$(this).closest('.dbShop-ui-rel').find(".dbShop-ui-mToolTips").show();
			},
			'focusout':function()
			{
				$(this).closest('.dbShop-ui-rel').find(".dbShop-ui-mToolTips").hide();
			}
		});
		
		$('textarea.tinymce').tinymce({
			script_url : '/s/v<?php echo VERSION; ?>/js/tinymce/tiny_mce.js',
			theme : "advanced",
			theme_advanced_buttons1 : "<?php echo $this->config->item('admin_setting_tinycme_buttons_1'); ?>",
			theme_advanced_buttons2 : "<?php echo $this->config->item('admin_setting_tinycme_buttons_2'); ?>",
			theme_advanced_buttons3 : "<?php echo $this->config->item('admin_setting_tinycme_buttons_3'); ?>",
			theme_advanced_buttons4 : "<?php echo $this->config->item('admin_setting_tinycme_buttons_4'); ?>",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,
			theme_advanced_path : false,
			skin : "o2k7",
			skin_variant : "silver",
			content_css: "/s/v<?php echo VERSION; ?>/css/front_end/job.css",
			body_class: "itemContent-des",
			<?php
				$lang_mapping = $this->config->item('admin_setting_tinycme_lang');
				echo 'language:"'.$lang_mapping[$this->app88common->get_shop_lang()].'",';
			?>
			theme_advanced_font_sizes : "10px,12px,14px,16px,24px",
			theme_advanced_fonts : "Arial=arial,helvetica,sans-serif;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier,monospace;Tahoma=tahoma,arial,helvetica,sans-serif;Times New Roman=times new roman,times;",
			encoding : "xml",
			plugins: "paste,table,mediaLibrary",
			init_instance_callback : "app88resize",
			handle_event_callback : "hideErrorBubble",
			relative_urls : false,
			remove_script_host : false,
			setup: function(ed) {
				ed.onInit.add(function(ed){
					if (tinymce.isGecko) {
						tinymce.dom.Event.add(ed.getDoc(), 'DOMNodeInserted', function(e) {
	                    	var v;
	                        e = e.target;
							
	                        if (e.nodeName === "IMG"){ 
	                        	var img_src = e.src.substring(0, 30);
	                        	if (img_src.match(/.*?data:image.*?;base64/g)) {
	                                imgParent = e.parentNode;
	                                imgParent.removeChild(e);
	                            }
	                        }
							
	                        if (e.nodeType === 1 && e.nodeName === 'IMG' && (v = e.getAttribute('mce_src')))
	                            e.src = t.documentBaseURI.toAbsolute(v);
	                    }); 
					}
                });
            }
		});

		$("input#post_date").datepicker({
			dateFormat: '<?php echo APP88_DATEPICKER_DATE_FORMAT; ?>',
			defaultDate: "+0",
			changeMonth: true,
			changeYear: true,
			numberOfMonths: 1,
			showOn: "both",
			buttonImage: "/images/back_end/calendar.gif",
			buttonImageOnly: true,
			<?php
				if(APP88_PAST_DATE_ALLOW === 0)
				{
					echo 'minDate: "0",';
				}
			?>
			buttonText: "",
			monthNamesShort: <?php echo $this->config->item('system_datepicker_month_name_short'); ?>
		});

		$('.formLoader').on('keyup dragend focus cut paste', 'input[name=job_salary]', function(event) {
			var obj = this;
			setTimeout(function() {
				var text = $(obj).val();
				if(text!="")
				{
					<?php if (APP88_CURRENCY_DECIMALS > 0) { ?>
					var valid = text.match(/^\d+([\.]?\d{0,<?php echo APP88_CURRENCY_DECIMALS; ?>})$/g);
					<?php } else { ?>
					var valid = text.match(/^\d+$/g);
					<?php } ?> 
					if(valid != null)
					{
						//$(obj).val(valid);
					}
					else
					{
						if (text.indexOf("<?php echo APP88_CURRENCY_DECIMAL_POINTS; ?>") != text.lastIndexOf("<?php echo APP88_CURRENCY_DECIMAL_POINTS; ?>"))
						{
							text = text.replace('<?php echo APP88_CURRENCY_DECIMAL_POINTS; ?>','');
						}
						<?php if (APP88_CURRENCY_DECIMALS > 0) { ?>
						$(obj).val(text.replace(/[^\d\<?php echo APP88_CURRENCY_DECIMAL_POINTS;?>]/g, ''));
						$(obj).val($(obj).val().match(/^\d+([\.]?\d{0,<?php echo APP88_CURRENCY_DECIMALS; ?>})/g));
						<?php } else { ?>
						$(obj).val(text.replace(/[^\d]/g, ''));
						<?php } ?> 
					}
				}
			}, 0);
		});
	});


	function getCookie(c_name)
	{
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++)
		{
			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
			x=x.replace(/^\s+|\s+$/g,"");
			if (x==c_name)
			{
				return unescape(y);
			}
		}
	}

	function submitJob()
	{
		$('#jobForm').attr('action','<?php echo $form_action; ?>');
		$('#jobForm').attr('target','');
		$('#jobForm').submit();
	}

	function hideErrorBubble(e)
	{
		if ('mousedown'==e.type)
		{
			$('#job_desc_err_bubble').fadeOut();
		}
	}

</script>
<div id="main-content">
	<div class="pageTitle">
		<h2 class="">
			<?php echo $title ?>
		</h2>
	</div>
	<?php echo form_open_multipart($form_action, array('id'=>'jobForm')); ?>
	<div class="pageAction">
		<?php 
		$this->common->showError($error);
		$this->common->showInfo($info);
		?>
		<div class="button-panel" style="margin-bottom: 15px">
			<a href="#" class="dbShop-ui-appBtn dbShop-ui-save"
				onclick="submitJob()"><?php echo $this->lang->line('save_label');?>
			</a> <a href="<?php echo site_url('admin/job/index') ?>"
				class="dbShop-ui-appBtn dbShop-ui-back" style="margin-left: 15px"><?php echo $this->lang->line('back_label');?>
			</a>
		</div>
		<div class="formLoader">

			<div class="dbShop-ui-rel">
				<p class="ganti-nama">
					<label for="job_title"><span class="compulsory">*</span><?php echo $this->lang->line('admin_view_job_title_label'); ?>:</label>
					<input id="job_title" class="txtMessageContent" name="job_title" size="35" type="text"
						value="<?php echo form_prep($form_data['job_title']); ?>">
					<span class="contoh-isi"><em><?php echo sprintf($this->lang->line('err_job_title_too_long'),APP88_CATEGORY_NAME_MAX_LENGTH);?></em></span>	
				</p>
				<?php if (isset($field_errs) && isset($field_errs['job_title'])) { ?>
                    <div id="job_title_err_bubble" class="dbShop-ui-eMsg dbShop-ui-eMsgTRt" style="left:200px; top:20px; display:block;">
					<div class="dbShop-ui-eMsgPtr"></div>
					<div class="dbShop-ui-eMsgCnt">
					<?php 
						foreach ($field_errs['job_title'] as $e)
						{
							echo $e . "<br>"; 
						}
					?>
					</div>
				</div>
				<?php } ?>
				<div class="dbShop-ui-mToolTips dbShop-ui-mTipsTRt" style="left:220px; top:30px; display:none;">
					<div class="dbShop-ui-tipsPtr"></div>
					<div class="dbShop-ui-tipsCnt"></div>
				</div>
			</div>			
			
			<div class="dbShop-ui-rel">	
				<p class="ganti-nama">
					<label for="job_refid"><span class="compulsory">*</span><?php echo $this->lang->line('admin_view_job_refid_label'); ?>:</label>
					<input id="job_refid" name="job_refid" size="35" type="text"
						value="<?php echo form_prep($form_data['job_refid']); ?>" <?php if ('edit'==$mode) echo 'readonly="readonly"'; ?>>
				</p>
				<?php if (isset($field_errs) && isset($field_errs['job_refid'])) { ?>
                    <div id="job_refid_err_bubble" class="dbShop-ui-eMsg dbShop-ui-eMsgTRt" style="left:200px; top:20px; display:block;">
					<div class="dbShop-ui-eMsgPtr"></div>
					<div class="dbShop-ui-eMsgCnt">
					<?php 
						foreach ($field_errs['job_refid'] as $e)
						{
							echo $e . "<br>"; 
						}
					?>
					</div>
				</div>
				<?php } ?>
			</div>
			

			<p class="ganti-nama">
				<label for="category_id"><span class="compulsory">*</span><?php echo $this->lang->line('admin_view_parent_category_label')?>:</label>
				<select name="category_id">
					<?php printCategorySelect($category_list,0,$form_data['category_id'],$form_data['category_guid']);?>
				</select>
			</p>

			<div class="dbShop-ui-rel">	
				<p class="ganti-nama">
					<label for="job_desc"><span class="compulsory">*</span><?php echo $this->lang->line('admin_view_job_des_label')?>:</label>
					<textarea id="job_desc" name="job_desc" cols="60" rows="20"
						class="tinymce">
						<?php echo form_prep($form_data['job_desc']); ?>
					</textarea>
				</p>
				
				<?php if (isset($field_errs) && isset($field_errs['job_desc'])) { ?>
                    <div id="job_desc_err_bubble" class="dbShop-ui-eMsg dbShop-ui-eMsgTRt" style="left:650px; top:80px; display:block;">
					<div class="dbShop-ui-eMsgPtr"></div>
					<div class="dbShop-ui-eMsgCnt">
					<?php 
						foreach ($field_errs['job_desc'] as $e)
						{
							echo $e . "<br>"; 
						}
					?>
					</div>
				</div>
				<?php } ?>
			</div>

			
			<div class="dbShop-ui-rel">
				<p class="ganti-nama">
					<label for="job_salary"><span class="compulsory">*</span><?php echo $this->lang->line('admin_view_job_salary_label')?>:</label> 
					<?php echo sprintf(APP88_MONEY_FORMAT, '<input id="job_salary" type="text" name="job_salary" value="'. form_prep($form_data['job_salary']) .'" size="10" />'); ?>
					<input name="job_negot" value="1"
					<?php if ($form_data['job_negot']) echo "checked='checked'"; ?>
						type="checkbox">
					<?php echo $this->lang->line('admin_view_job_negot_label'); ?>
				</p>
				
				<?php if (isset($field_errs) && isset($field_errs['job_salary'])) { ?>
                    <div id="job_salary_err_bubble" class="dbShop-ui-eMsg dbShop-ui-eMsgTRt" style="left:200px; top:20px; display:block;">
					<div class="dbShop-ui-eMsgPtr"></div>
					<div class="dbShop-ui-eMsgCnt">
					<?php 
						foreach ($field_errs['job_salary'] as $e)
						{
							echo $e . "<br>"; 
						}
					?>
					</div>
				</div>
				<?php } ?>
			</div>
			
			
			<div class="dbShop-ui-rel">
				<p class="ganti-nama">
					<label for="job_emply_type"><span class="compulsory">*</span><?php echo $this->lang->line('admin_view_job_emplytype_label')?>:</label> 
					
					<select id="job_emply_type" name="job_emply_type" id="job_emply_type">
						<?php 
							foreach ($employ_types as $type)
							{
								echo '<option value="' . $type . '"';
								if ($type == $form_data['job_emply_type'])
								{
									echo ' selected="selected"';
								}
								echo '>';
								echo $this->lang->line('view_'.$type.'_label');
								echo '</option>';
							}
						?>
						
					</select>
				</p>
				<?php if (isset($field_errs) && isset($field_errs['job_emply_type'])) { ?>
                    <div id="job_emply_type_err_bubble" class="dbShop-ui-eMsg dbShop-ui-eMsgTRt" style="left:200px; top:20px; display:block;">
					<div class="dbShop-ui-eMsgPtr"></div>
					<div class="dbShop-ui-eMsgCnt">
					<?php 
						foreach ($field_errs['job_emply_type'] as $e)
						{
							echo $e . "<br>"; 
						}
					?>
					</div>
				</div>
				<?php } ?>
			</div>
			
			
			<div class="dbShop-ui-rel">	
				<p class="ganti-nama">
					<label for="contact_person"><span class="compulsory">*</span><?php echo $this->lang->line('admin_view_job_contact_person_label'); ?>:</label>
					<input id="contact_person" name="contact_person" size="35" type="text"
						value="<?php echo form_prep($form_data['contact_person']); ?>">
				</p>
				<?php if (isset($field_errs) && isset($field_errs['contact_person'])) { ?>
                    <div id="contact_person_err_bubble" class="dbShop-ui-eMsg dbShop-ui-eMsgTRt" style="left:200px; top:20px; display:block;">
					<div class="dbShop-ui-eMsgPtr"></div>
					<div class="dbShop-ui-eMsgCnt">
					<?php 
						foreach ($field_errs['contact_person'] as $e)
						{
							echo $e . "<br>"; 
						}
					?>
					</div>
				</div>
				<?php } ?>
			</div>
			
			
			<div class="dbShop-ui-rel">
				<p class="ganti-nama">
					<label for="job_contact"><span class="compulsory">*</span><?php echo $this->lang->line('admin_view_job_contact_label')?>:</label> 
					<input id="job_contact" name="job_contact" size="35" type="text"
						value="<?php echo form_prep($form_data['job_contact']); ?>">
				</p>
				<?php if (isset($field_errs) && isset($field_errs['job_contact'])) { ?>
                    <div id="job_contact_err_bubble" class="dbShop-ui-eMsg dbShop-ui-eMsgTRt" style="left:200px; top:20px; display:block;">
					<div class="dbShop-ui-eMsgPtr"></div>
					<div class="dbShop-ui-eMsgCnt">
					<?php 
						foreach ($field_errs['job_contact'] as $e)
						{
							echo $e . "<br>"; 
						}
					?>
					</div>
				</div>
				<?php } ?>
			</div>
			
			<?php if (APPLICATION_EMAIL_ENABLED == 1) { ?>
			<div class="dbShop-ui-rel">
				<p class="ganti-nama">
					<label for="job_recips"><span class="compulsory">*</span><?php echo $this->lang->line('admin_view_job_recipient_label')?>:</label> 
					<input id="job_recips" name="job_recips" size="35" type="text"
						value="<?php echo form_prep($form_data['job_recips']); ?>">
				</p>
			
				<?php if (isset($field_errs) && isset($field_errs['job_recips'])) { ?>
                    <div id="job_recips_err_bubble" class="dbShop-ui-eMsg dbShop-ui-eMsgTRt" style="left:200px; top:20px; display:block;">
					<div class="dbShop-ui-eMsgPtr"></div>
					<div class="dbShop-ui-eMsgCnt">
					<?php 
						foreach ($field_errs['job_recips'] as $e)
						{
							echo $e . "<br>"; 
						}
					?>
					</div>
				</div>
				<?php } ?>
			</div>
			<?php } else { ?>
			<input type="hidden" value="<?php echo form_prep($form_data['job_recips']); ?>" name="job_recips"/>
			<?php } ?>

			<p class="ganti-nama">
				<label for="job_publish"><span class="compulsory">*</span><?php echo $this->lang->line('published_label')?>:</label>
				<input name="job_publish" value="1"
				<?php if ($form_data['job_publish']) echo "checked='checked'"; ?>
					type="radio"> <span class="notice-opt"><?php echo $this->lang->line('show_label')?>
				</span> <input name="job_publish" value="0"
				<?php if (!$form_data['job_publish']) echo "checked='checked'"; ?>
					type="radio"> <span><?php echo $this->lang->line('hide_label')?> </span>
			</p>

			<p class="ganti-nama">
				<label for="post_date"><span class="compulsory">*</span><?php echo $this->lang->line('admin_view_job_postdate_label')?>:</label>
				<input id="post_date" name="post_date" size="20" type="text"
					value="<?php echo form_prep($form_data['post_date']); ?>"
					readonly="readonly"> <span class="contoh-isi"><em><?php echo $this->lang->line('admin_view_date_example_text'); ?>
				</em> </span>
			</p>

		</div>

		<div class="button-panel">
			
			<a href="#" class="dbShop-ui-appBtn dbShop-ui-save"
				onclick="submitJob()"><?php echo $this->lang->line('save_label');?>
			</a> <a href="<?php echo site_url('admin/job/index') ?>"
				class="dbShop-ui-appBtn dbShop-ui-back" style="margin-left: 15px"><?php echo $this->lang->line('back_label');?>
			</a>
		</div>
		<?php echo form_close(); ?>
	</div>

	
</div>
<?php $this->load->view('admin/common/footer');?>