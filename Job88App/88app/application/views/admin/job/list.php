<?php
	$this->load->view('admin/common/header');
	$datepicker_lang = $this->config->item('admin_setting_datepicker_lang');
?>

<style>
.catalogue-listing-ttl{float:none;text-align:center} 
</style>
<script
	type="text/javascript"
	src="/s/v<?php echo VERSION; ?>/js/jquery_ui/ui/i18n/jquery.ui.datepicker-<?php echo $datepicker_lang[$this->app88common->get_shop_lang()];?>.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$.datepicker.setDefaults({
			yearSuffix: ''
		});
		$("input#startDate").datepicker({
			dateFormat: "<?php echo APP88_DATEPICKER_DATE_FORMAT; ?>",
			defaultDate: "+0",
			changeMonth: true,
			changeYear: true,
			numberOfMonths: 1,
			showOn: "both",
			buttonImage: "/images/back_end/calendar.gif",
			buttonImageOnly: true,
			buttonText: "",
			onClose: function(selectedDate) {
				$( "input#endDate" ).datepicker( "option", "minDate", selectedDate );
			},
			monthNamesShort: <?php echo $this->config->item('system_datepicker_month_name_short'); ?>
		});
		$("input#endDate").datepicker({
			defaultDate: "+0",
			dateFormat: "<?php echo APP88_DATEPICKER_DATE_FORMAT; ?>",
			changeMonth: true,
			changeYear: true,
			numberOfMonths: 1,
			showOn: "both",
			buttonImage: "/images/back_end/calendar.gif",
			buttonImageOnly: true,
			buttonText: "",
			onClose: function(selectedDate) {
				$( "input#startDate" ).datepicker( "option", "maxDate", selectedDate );
			},
			monthNamesShort: <?php echo $this->config->item('system_datepicker_month_name_short'); ?>
		});
		$( "#keyword, #startDate, #endDate" ).keypress(function(e) {
			if(e.which == 13) {
				document.getElementById('filterform').submit();
			}
		});
	});
	function clearList()
	{
		$('#keyword').val('');
		$('#startDate').val('');
		$('#endDate').val('');
	}
</script>

<div id="main-content">
	<div class="pageTitle">
		<h2 class=""><?php echo $title; ?>
		<?php if ($ad_quota != -1) { ?>
			<span>
				<?php echo sprintf($this->lang->line('quota_label'), $active_jobs_num, $ad_quota); ?>
			</span>
		<?php }
				else
				{ 
		 ?>
			<span>
				<?php echo sprintf($this->lang->line('quota_label_non_ltd'), $active_jobs_num); ?>
			</span>
		 <?php 
				}
		 ?>
		</h2>
	</div>
	
	<?php 
		$this->common->showError($error);
		$this->common->showInfo($info);
	?>
		
	<div class="Action">
		<div class="pageAdd-fl" style="float:left">
				<a href="<?php echo site_url("admin/job/add"); ?>" class="dbShop-ui-appBtn dbShop-ui-addproduct"><?php echo $this->lang->line('btn_add');?></a>
		</div>
		
		<div class="pageBlank-mid" style="float:right">
			<?php echo form_open($form_action_url,array('id'=>'filterform')); ?>
			
			<div style="float:right;">
				<a href="#" style="margin-left:10px;" class="dbShop-ui-appBtn dbShop-ui-apply" onClick="document.getElementById('filterform').submit();">
					<?php echo $this->lang->line('admin_view_submit_label');?>
				</a>
				<a href="#" class="dbShop-ui-appBtn dbShop-ui-apply" style="margin-left:5px;" onClick="clearList();">
					<?php echo $this->lang->line('btn_clear');?>
				</a>
			</div>
			
			<div style="float:right">
				<label for="startDate"><?php echo $this->lang->line('admin_view_job_startdate_label'); ?></label>
				<input id="startDate" readonly="readonly" name="startDate" style="width:100px" type="text" value="<?php echo $startDate;?>">
				
				<label for="endDate"><?php echo $this->lang->line('admin_view_job_enddate_label'); ?></label>
				<input id="endDate" readonly="readonly" name="endDate" style="width:100px" type="text" value="<?php echo $endDate; ?>">
				
				<label for="keyword"><?php echo $this->lang->line('admin_view_job_keywords_label'); ?></label>
				<input name="keyword" id="keyword" type="text" value="<?php echo $keyword; ?>">
			</div>
			
			<div style="clear:both"></div>
			
			<?php echo form_close(); ?>
		</div>
		
		<div class="pageBlank"></div>
	</div>

	<div class="pageData">
		<table id="product-table-grid" class="datagrid2" width="100%">
			<thead>
				<tr>
					<th class="catalogue-listing-ttl"><?php echo $this->lang->line('admin_view_job_title_label'); ?></th>
					<th class="catalogue-listing-ttl" width="20"><?php echo $this->lang->line('admin_view_category_label'); ?></th>
					<th class="catalogue-listing-ttl" width="50"><?php echo $this->lang->line('published_label'); ?></th>
					<th class="catalogue-listing-ttl" width="120"><?php echo $this->lang->line('admin_view_job_postdate_label'); ?></th>
					<th class="catalogue-listing-ttl" width="150"><?php echo $this->lang->line('admin_view_action_label'); ?></th>
				</tr>
			</thead>
			
			<tbody>
				<?php 
					if(is_array($jobs) && !empty($jobs))
					{
						foreach ($jobs as $job)
						{
				?>
				<tr>
					<td class="catalogue-product">
						<a href="<?php echo site_url("admin/job/edit/".$job['Guid']); ?>"><?php echo form_prep($job['Title']); ?></a>
					</td>
					<td class="catalogue-num">
						<p><?php echo ($job['SubCategory'] != NULL)? form_prep($job['SubCategory']) : form_prep($job['MainCategory']); ?></p>
					</td>
					<td class="catalogue-published">
						<?php echo $job['Published']? $this->lang->line('show_label') : $this->lang->line('hide_label'); ?>
					</td>
					<td class="catalogue-date">
						<p><?php echo form_prep($job['PostDateFormatted']); ?></p>
					</td>
					<td class="catalogue-opt" style="text-align:center;float:none;">
						<div class="menu-link" style="display:inline-block;padding:0px;">
							<a title="<?php echo $this->lang->line("btn_edit"); ?>" class="dbShop-ui-appBtn dbShop-ui-addeditproduct_action" href="<?php echo site_url("admin/job/edit/".$job['Guid']); ?>"></a>
							<a title="<?php echo $this->lang->line("hide_label")." / ".$this->lang->line("show_label"); ?>" class="dbShop-ui-appBtn <?php if ('1' === $job['Published']) { echo 'dbShop-ui-hide_action'; } else { echo 'dbShop-ui-show_action'; } ?>" href="<?php echo site_url("admin/job/hideshow/".$job['Guid']."/".$page); ?>"></a>
							<?php if ($support_sag) { ?>
							<a href="<?php echo site_url('admin/shoot/add/'.$job['Guid']); ?>" class="dbShop-ui-appBtn dbShop-ui-shootad_action dbShop-ui-Btn-right" title="<?php echo $this->lang->line('btn_shoot_ad'); ?>"></a>
							<?php } ?>
							<a title="<?php echo $this->lang->line("btn_delete"); ?>" href="#" style="margin-left:5px" class="dbShop-ui-appBtn dbShop-ui-adddelete_action" 
								onClick="deleteDialogOpen(event,'<?php echo $this->lang->line('delete_job_msg'); ?>','<?php echo site_url("admin/job/delete/".$job['Guid']."/".$page)?>');"></a>
							<div style="clear:both"></div>
						</div>
					</td>
				</tr>
				<?php 
						}
					}
					else
					{
						echo '<tr><td colspan="5" class="menu-error">'.$this->lang->line('no_data_label').'</td></tr>';
					}
				?>
			</tbody>
			
			<tfoot>
				<?php if ($max_page > 1 ) {?>
				<tr>
					<td class="category-pagination" colspan="5">
						<div class="paging" id="paging" style="display: block;">
						<?php
							$leftpage = $page - ($this->config->item('num_page_show')-1)/2;
							$leftpage = $leftpage >= 1? $leftpage : 1;
							
							$rightpage = $leftpage + $this->config->item('num_page_show')-1;
							$rightpage = $rightpage >= $max_page? $max_page: $rightpage;
							
							$leftpage = $rightpage >= $max_page? ($rightpage - $this->config->item('num_page_show')+1): $leftpage;
							
							for($i=1;$i<=$max_page;$i++)
							{
								if ($i == 1)
								{
									if ($page > 1)
									{
										$site_url =  site_url('admin/job/index/'.($page-1));
										if($keyword) $site_url.="&keyword=".urlencode($keyword);
										echo '<a class="previous" href="'.$site_url.'">&lt;&lt;</a>';
									}
									else
									{
										echo '<span style="padding:0px 17px"></span>';
									}
								}
								
								
								if ($i==$page)
								{
							?>
									<strong <?php if ($page == $max_page) echo 'style="border:none"'; ?>><?php echo $i; ?></strong>
							<?php 
								}
								else
								{
									if($i == $max_page || $i == 1 || ($i <= $rightpage && $i >= $leftpage))
									{
							?>
									<a href="
							<?php 
								$site_url =  site_url('admin/job/index/'.$i);
								if($keyword) $site_url.="&keyword=".urlencode($keyword);
								echo $site_url;
							?>" <?php if ($i == $max_page) echo 'class="last"'; ?>><?php echo $i; ?></a>
							<?php
									}
									else
									{
										if($i == $rightpage+1) echo'<span>&nbsp;...&nbsp;</span>';
										elseif($i == $leftpage-1) echo '<span>&nbsp;...&nbsp;</span>';
									}
								}
								
								if ($i == $max_page)
								{
									if($page < $max_page)
									{
										$site_url =  site_url('admin/job/index/'.($page+1));
										if($keyword) $site_url.="&keyword=".urlencode($keyword);
										echo '<a class="next" href="'.$site_url.'">&gt;&gt;</a>';
									}
									else
									{
										echo '<span style="padding:0px 17px"></span>';
									}
								}
							}
						?>
						</div>
					</td>
				</tr>
				<?php } ?>
			</tfoot>
		
		</table>
		
	</div>
	
	<div class="Action">
		<div class="pageAdd fl">
			<a class="dbShop-ui-appBtn dbShop-ui-addproduct" href="<?php echo site_url("admin/job/add"); ?>"><?php echo $this->lang->line('btn_add');?></a>
		</div>
		<div class="pageBlank"></div>
	</div>
	
</div>
<?php $this->load->view('admin/common/footer');?>