<div id="head-navigation" class="clearfix">
	<ul id="navigation" class="clearfix">
		
		<?php if (!$hide_nav) { ?>
		<li class="catalogue-menu">
			<a class="<?php echo $this->router->fetch_class() == 'category' ? 'catOn' : 'cat';?>" href="<?php echo site_url('admin/category/index'); ?>"><?php echo $this->lang->line('admin_view_category_label');?></a>
		</li>
		
		<li class="catalogue-menu">
			<a class="<?php echo $this->router->fetch_class() == 'job' ? 'jobsOn' : 'jobs';?>" href="<?php echo site_url('admin/job/index'); ?>"><?php echo $this->lang->line('admin_page_title_job');?></a>
		</li>
		
		<?php if ($support_sag) { ?>
		<li class="catalogue-menu">
			<a class="<?php echo $this->router->fetch_class() == 'shoot' ? 'postAdOn' : 'postAd';?>" href="<?php echo site_url('admin/shoot/index'); ?>"><?php echo $this->lang->line('admin_view_navgiat_postad_label');?></a>
		</li>
		<?php } ?>
		
		<?php if (APP88_FEED_SUPPORT == '1') { ?>
		<li class="catalogue-menu">
			<a class="<?php echo $this->router->fetch_class() == 'data_feed' ? 'feedsOn' : 'feeds';?>"  href="<?php echo site_url('admin/data_feed/index'); ?>"><?php echo $this->lang->line('admin_view_feed_label');?></a>
		</li>
		<?php } ?>
		
		<?php if (APP88_LAYOUT_SUPPPORT === '1') { ?>
		<li class="catalogue-menu">
			<a class="<?php echo $this->router->fetch_class() == 'layout' ? 'layoutOn' : 'layout';?>" href="<?php echo site_url('admin/job/index'); ?>"><?php echo $this->lang->line('admin_view_navgiat_layout_label');?></a>
		</li>
		<?php } ?>
		<?php } ?>
	</ul>
</div>