<script type="text/javascript" src="/s/v<?php echo VERSION; ?>/js/jquery_ui/jquery-1.7.2.min.js"></script>
<!-- CSS Link -->
<link rel="stylesheet" type="text/css" href="/s/v<?php echo VERSION; ?>/css/back_end/backend.css" >
<link rel="stylesheet" type="text/css" href="/s/v<?php echo VERSION; ?>/js/jquery_ui/themes/smoothness/jquery.ui.all.css" media="screen">
<link rel="stylesheet" type="text/css" href="/s/v<?php echo VERSION; ?>/css/buttons.css" > 
<!-- Javascript Link -->
<?php 
$app_param = $this->session->userdata('app_param');
if ( !empty($app_param[$_GET['cid']]['common_js_path']) )
{
	echo "	<script type=\"text/javascript\" src=\"". $app_param[$_GET['cid']]['common_js_path'] ."\"></script>\n";
}
?>
<style>
.category-pagination a {color:black !important};
.category-pagination span strong {color:red !important};
</style>
<script language="javascript" type="text/javascript" src="/s/v<?php echo VERSION; ?>/js/jquery-ui-1.8.16.custom.min.js"></script>
<script language="javascript" type="text/javascript" src="/s/v<?php echo VERSION; ?>/js/jquery.cookie.min.js"></script>
<script language="javascript" type="text/javascript" src="/s/v<?php echo VERSION; ?>/js/jquery.blockUI.min.js"></script>
<script language="javascript" type="text/javascript">
	var app_instance = "<?php echo $_GET['cid']; ?>";    

	function deleteDialogOpen(event, msg, url, callback)
	{
		var simpleWindow;
		if ($("#windownbg").length == 0) {
			simpleWindow = $("<div id=\"windownbg\" style=\"height:" + $(document).height() + "px;filter:alpha(opacity=0);opacity:0.5;z-index: 100\"></div>");
			$("body").append(simpleWindow);
		} else {
			simpleWindow = $("#windownbg");
		}
		simpleWindow.show();
	
		var delDialog = $( ".deleteMessageLayer" );
		delDialog.find('.dbShop-ui-wrapper20').html(msg);
		delDialog.show();
		$(".deleteMessageLayer a.btnConfirmDeleteMessage").data('url', url);
		$(".deleteMessageLayer a.btnConfirmDeleteMessage").data('callback', callback);
		delDialog.css("left", ((delDialog.parent().width() - delDialog.width()) / 2) + "px");
		if (event != 0)
		{
			var top = event.clientY - delDialog.height()/2;
			top = Math.min(Math.max(top, 10), $('body').height() - delDialog.height() - 30);
			delDialog.css("top", top + "px");
		}
	}

	function doDeleteMessage(url, callback)
	{
		$("#windownbg").hide('fast');
		$(".deleteMessageLayer").hide();
		if ((url != null) && (url != ''))
		{
			location.href=url;
		}
		else if ((callback != null) && (callback != ''))
		{
			callback();
		}
	}

	function closeDeleteDialog()
	{
		$( ".deleteMessageLayer" ).hide();
		$('#windownbg').hide('fast');
	}

	function alertDialogOpen(event,msg)
	{
		var simpleWindow;
		if ($("#windownbg").length == 0) {
			simpleWindow = $("<div id=\"windownbg\" style=\"height:" + $(document).height() + "px;filter:alpha(opacity=0);opacity:0.5;z-index: 100\"></div>");
			$("body").append(simpleWindow);
		} else {
			simpleWindow = $("#windownbg");
		}
		simpleWindow.show();
	
		var altlDialog = $( ".alertLayer" );
		altlDialog.find('.dbShop-ui-wrapper20').html(msg);
		altlDialog.show();
		altlDialog.css("left", ((altlDialog.parent().width() - altlDialog.width()) / 2) + "px");
		if (event != 0)
		{
			var top = event.clientY - altlDialog.height()/2;
			top = Math.min(Math.max(top, 10), $('body').height() - altlDialog.height() - 30);
			altlDialog.css("top", top + "px");
		}
	}


	function closeAlertDialog()
	{
		$( ".alertLayer" ).hide();
		$('#windownbg').hide('fast');
	}


	$(document).ready(function(){
		$('#category_name, #job_title, #job_refid, #job_salary, #job_emply_type, #contact_person, #job_contact, #job_recips').focus(
			function(){
				$(this).parent().next('.dbShop-ui-eMsg.dbShop-ui-eMsgTRt').fadeOut();
			}
		);
	});

	
</script>
