<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('site_url'))
{
	function site_url($uri = '')
	{
		$CI =& get_instance();
		
		if (isset($_GET['cid']))
		{
			if (strstr($uri, '?'))
			{
				$uri .= '&cid='.$_GET['cid'];
			}
			else
			{
				$uri .= '?cid='.$_GET['cid'];
			}
		}
		return $CI->config->site_url($uri);
	}
}

