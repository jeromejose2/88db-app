<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('SAG_GET_OPTION_DRIVER_PATH', realpath(dirname(__FILE__).'/sag_get_option_drivers'));

class SAG_Get_Option
{
	private $_CI;
	private $_config;
	private $_driver;
	
	public function __construct($config = array())
	{
		$this->_CI = &get_instance();
		
		if (file_exists(APPPATH.'third_party/sag_get_option/config/'.ENV.'/'.COUNTRY.'.php'))
		{
			include APPPATH.'third_party/sag_get_option/config/'.ENV.'/'.COUNTRY.'.php';
			$this->_config = unserialize(SAG_GET_OPTION_CONFIG);
		}
		
	
	}

	public function getOption($target, $channel, $action, $lang)
	{
		if (isset($this->_config[$target][$action]))
		{
			// 88DB
			$driver_class = 'SAG_Get_Option_'.strtoupper($target);
			$driver_path = SAG_GET_OPTION_DRIVER_PATH.'/'.strtolower($target).'.php';
			if ( ! file_exists($driver_path)) return FALSE;
			require_once($driver_path);
			
			if ( ! class_exists($driver_class)) return FALSE;
			
			$this->_driver = new $driver_class();
			
			$param = array();
			$param['action'] = $action;
			$param['lang'] = $lang;
			$param['category_id'] = $channel; 
			return $this->_driver->getOption($param, $this->_config[$target][$action]);

		}
		else
		{
			// Error
			return FALSE;
		}
	}
	
}