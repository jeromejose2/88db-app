<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SAG_Get_Option_88DB
{
	private $CI;
	private $_langMapping;
	
	public function __construct($config = array())
	{
		$this->CI = &get_instance();
		$this->CI->load->library('app88param');
		$this->_langMapping = $this->CI->config->item('88db_ws_lang');
	}

	public function getOption($param, $config)
	{
		if (!empty($param['action']) && isset($config) && is_array($config))
		{
			$db88_access_token = $this->CI->app88param->getAccessToken();
			$db88AccessToken = $db88_access_token['token'];
			
			$xml = '';
			$xml .= '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:db="http://88db.com/"><soapenv:Header/><soapenv:Body>';
			$xml .= '<db:GetOptionList>';
			$xml .= '<db:AppAuthToken>'.$db88AccessToken.'</db:AppAuthToken>';
			$xml .= '<db:request>';
			$xml .= '<db:OptionListName>'.$config['option_list'].'</db:OptionListName>';
			$xml .= '<db:Params>';
			$xml .= '<db:ParamNameValuePair>';
			$xml .= '<db:Name>CategoryID</db:Name>';
			$xml .= '<db:Value>'.$param['category_id'].'</db:Value>';
			$xml .= '</db:ParamNameValuePair>';
			$xml .= '<db:ParamNameValuePair>';
			$xml .= '<db:Name>Lang</db:Name>';
			$xml .= '<db:Value>'.strtolower($this->_langMapping[$param['lang']]).'</db:Value>';
			$xml .= '</db:ParamNameValuePair>';
			$xml .= '</db:Params>';
			$xml .= '</db:request>';
			$xml .= '</db:GetOptionList>';
			$xml .= '</soapenv:Body></soapenv:Envelope>';
			
			$this->CI->app88log->log_message('sag', 'DEBUG', __FILE__.'('.__METHOD__.'@'. __LINE__ .') '.$xml);
			
			require_once(APPPATH.'libraries/nusoap/lib/nusoap.php');
			$client = new nusoap_client($config['ws_url'], 'wsdl');
			if ($client->getError())
			{
				$this->CI->app88log->log_message('sag', 'ERROR', 'Fail to connect '.$config['ws_url'].' with error: '.$client->getError());
				return 'error';
			}
			
			$client->soap_defencoding ='utf-8';
			$client->decode_utf8 =false;
			$client->xml_encoding = 'utf-8';
			$result = $client->send($xml, 'http://88db.com/GetOptionList');

			if ($client->getError())
			{
				$this->CI->app88log->log_message('sag', 'ERROR', __FILE__.'('.__METHOD__.'@'. __LINE__ .') '.$xml);
				$this->CI->app88log->log_message('sag', 'ERROR', __FILE__.'('.__METHOD__.'@'. __LINE__ .') '.htmlspecialchars($client->response, ENT_QUOTES));
				$this->CI->app88log->log_message('sag', 'ERROR', 'Error in web service '.$config['ws_url'].' with error: '.$client->getError());
				return 'error';
			}
			else if ($result['GetOptionListResult']['Success'] == 'false')
			{
				$this->CI->app88log->log_message('sag', 'ERROR', __FILE__.'('.__METHOD__.'@'. __LINE__ .') '.$xml);
				$this->CI->app88log->log_message('sag', 'ERROR', __FILE__.'('.__METHOD__.'@'. __LINE__ .') '.htmlspecialchars($client->response, ENT_QUOTES));
				$this->CI->app88log->log_message('sag', 'ERROR', 'Error in web service '.$config['ws_url'].' with error ('. $result['GetOptionListResult']['ErrorCode'] .'): '.$result['GetOptionListResult']['ErrorMessage']);
				return 'error';
			}
			return $result['GetOptionListResult'];
		}
		else
		{
			$this->CI->app88log->log_message('sag', 'ERROR', __FILE__.'('.__METHOD__.'@'. __LINE__ .') '.'No Parameter');
			return 'error';
		}
	}
}