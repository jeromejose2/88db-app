<?php

define('APP88_ID','88app_job_'.strtoupper(COUNTRY).'_'.strtoupper(ENV).'_v'.VERSION);
define('APP88_VERSION','v'.VERSION);

define('APP88_DB_USER', 'root');
define('APP88_DB_PWD', '');
define('APP88_DB_NAME', '88app_job_hk');

/*
|--------------------------------------------------------------------------
| Error Logging Threshold
|--------------------------------------------------------------------------
|
| If you have enabled error logging, you can set an error threshold to
| determine what gets logged. Threshold options are:
| You can enable error logging by setting a threshold over zero. The
| threshold determines what gets logged. Threshold options are:
|
|	0 = Disables logging, Error logging TURNED OFF
|	1 = Error Messages (including PHP errors)
|	2 = Debug Messages
|	3 = Informational Messages
|	4 = All Messages
|
| For a live site you'll usually only enable Errors (1) to be logged otherwise
| your log files will fill up very fast.
|
*/
define('LOG_THRESHOLD', 2); // Suggested, DEV: 2, UAT and PROD: 1

define('AUTH_APP_ID', '29a91e2d530d43978f709e40f2ede12e'); // Generate new one for a new app
define('AUTH_APP_SECRET', '123456');
define('AUTH_JQUERY_PATH', 'http://hk.dev.shop.88static.com/s/v'.VERSION.'/js/jquery.min.js');
define('AUTH_JSON_PATH', 'http://hk.dev.shop.88static.com/s/v'.VERSION.'/js/json2.min.js');
define('AUTH_ACCESS_TOKEN_PATH', 'http://secure01.dev.88db.com/addon/accesstoken');
define('AUTH_USER_INFO_PATH', 'http://secure01.dev.88db.com/addon/user');
define('APP88_EMAIL_GATEWAY_URL','http://secure01.dev.88db.com/addon/email');
define('APP88_EMAIL_SHOP_OWNER_GATEWAY_URL','http://secure01.dev.88db.com/addon/emailShopOwner');
define('APP88_EMAIL_88DB_MEMBER_GATEWAY_URL','http://secure01.dev.88db.com/addon/email88DbMember');

define('AUTH_DB88_ACCESS_TOKEN_PATH', 'http://secure01.dev.88db.com/en/addon/db88accesstoken');
define('APP88_SAG_SUPPORT', 0);
define('APP88_DEFAULT_SAG_SUPPORT', 1);
define('SAG_88DB_CATEGORY_WS','http://preview.th.88db.com/th/WebServices/Shop/AppWS.asmx?wsdl');
define('SAG_88DB_CATEGORY_CACHE_DURATION',12*60); // minutes
define('SAG_JOB_TARGET', serialize(array(
	'ws_path' => 'http://th.dev.sag.88apps.net/gateway/index',
	'channelId' => 'db88_TH_',
	'topic' => 'job',
	'mapping' => array(
		'companyAddr',
		'jobDesc',
		'jobTitle',
		'email',
		'phone',
		'contactName',
		'salary'
	)
)));
define('ALLOW_CHANNEL_LIST', serialize(array(
	'4360000',
	'4360100',
	'4360200',
	'4360300',
	'4360400',
	'4360500',
	'4360600',
	'4360700',
	'4360800',
	'4360900',
	'4361000',
	'4361100',
	'4361200',
	'4361300',
	'4361400',
	'4361500',
	'4361600',
	'4361700',
	'4361800',
	'4361900'
)));
define('SAG_TARGET', serialize(array()));
define('APPLICATION_EMAIL_ENABLED', 1);