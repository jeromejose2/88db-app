<?php

define('APP88_TIMEZONE_OFFSET', '+5.5'); // Hour offset from GMT
define('APP88_FRONT_LANGUAGE', 'en');
define('APP88_ADMIN_LANGUAGE', 'en');
define('APP88_DATE_FORMAT','Y-m-d');
define('APP88_DATEPICKER_DATE_FORMAT','yy-mm-dd');

define('APP88_PAST_DATE_ALLOW',0);

define('APP88_CATEGORY_NAME_MAX_LENGTH', 60);
define('APP88_JOB_TITLE_MAX_LENGTH', 60);
define('APP88_FEED_SUPPORT', 0);
define('APP88_LAYOUT_SUPPPORT',0);

define ("APP88_EMPLOYMENT_TYPE", serialize (array (
												'FULLTIME', 
												'PARTTIME', 
												'TEMPORARY',
												'CONTRACT',
												'FREELANCE',
												'SUMMER_JOB',
												'WORK_AT_HOME',
												'NOT_SPECIFIED'
												)
		));
define('APP88_CURRENCY_DECIMALS', '0');
define('APP88_CURRENCY_DECIMAL_POINTS', '.'); // '.' or ',', other character may cause regex fail
define('APP88_CURRENCY_THOUSANDS_SEPARATOR', ',');
define('APP88_MONEY_FORMAT', '%s INR');
define('APP88_AD_QUOTA', 50);
define('APP88_ADDTHIS_ENABLE',1);
define('APP88_JIATHIS_ENABLE',0);
define('APP88_ADDTHIS_SERVICES','facebook,twitter,gmail,google_plusone_share,email,linkedin');
define('APP88_JIATHIS_SERVICES','fb,twitter,gmail,googleplus,email,linkedin');
define('APP88_JIATHIS_SITENUM','6');
define('APP88_SHARE_DESCRIPTION_CHAR_LIMIT',100);