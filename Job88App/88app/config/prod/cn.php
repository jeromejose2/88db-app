<?php

define('APP88_ID','88App_Job_'.strtoupper(COUNTRY).'_'.strtoupper(ENV).'_v'.VERSION);
define('APP88_VERSION','v'.VERSION);

define('APP88_DB_USER', '88appsweb');
define('APP88_DB_PWD', 'DCDvvTgITuEw');
define('APP88_DB_NAME', '88app_job_cn');

/*
|--------------------------------------------------------------------------
| Error Logging Threshold
|--------------------------------------------------------------------------
|
| If you have enabled error logging, you can set an error threshold to
| determine what gets logged. Threshold options are:
| You can enable error logging by setting a threshold over zero. The
| threshold determines what gets logged. Threshold options are:
|
|	0 = Disables logging, Error logging TURNED OFF
|	1 = Error Messages (including PHP errors)
|	2 = Debug Messages
|	3 = Informational Messages
|	4 = All Messages
|
| For a live site you'll usually only enable Errors (1) to be logged otherwise
| your log files will fill up very fast.
|
*/
define('LOG_THRESHOLD', 1); // Suggested, DEV: 2, UAT and PROD: 1

define('AUTH_APP_ID', '29a91e2d530d43978f709e40f2ede12e'); // Generate new one for a new app
define('AUTH_APP_SECRET', '123456');
define('AUTH_JQUERY_PATH', 'http://shop.88static.88db.cn/s/v'.VERSION.'/js/jquery.min.js');
define('AUTH_JSON_PATH', 'http://shop.88static.88db.cn/s/v'.VERSION.'/js/json2.min.js');
define('AUTH_ACCESS_TOKEN_PATH', 'https://secure01.app.88db.cn/addon/accesstoken');
define('AUTH_DB88_ACCESS_TOKEN_PATH', 'https://secure01.app.88db.cn/addon/db88accesstoken');
define('AUTH_USER_INFO_PATH', 'https://secure01.app.88db.cn/addon/user');
define('APP88_EMAIL_GATEWAY_URL','https://secure01.app.88db.cn/addon/email');
define('APP88_EMAIL_SHOP_OWNER_GATEWAY_URL','https://secure01.app.88db.cn/addon/emailShopOwner');
define('APP88_EMAIL_88DB_MEMBER_GATEWAY_URL','https://secure01.app.88db.cn/addon/email88DbMember');

define('APP88_SAG_SUPPORT', 0);
define('APP88_DEFAULT_SAG_SUPPORT', 1);
define('SAG_88DB_CATEGORY_WS','http://88db.cn/cn/WebServices/Shop/AppWS.asmx?wsdl');
define('SAG_88DB_CATEGORY_CACHE_DURATION',12*60); // minutes
define('SAG_JOB_TARGET', serialize(array(
	'ws_path' => 'http://sag.88apps.88db.cn/gateway/index',
	'channelId' => 'db88_CN_',
	'topic' => 'job',
	'mapping' => array(
		'companyAddr',
		'jobDesc',
		'jobTitle',
		'email',
		'phone',
		'contactName',
		'salaryRange'
	)
)));
define('ALLOW_CHANNEL_LIST', serialize(array(
	'7260000',
	'7260100',
	'7260200',
	'7260300',
	'7260400',
	'7260500',
	'7260600',
	'7260700',
	'7260800',
	'7260900',
	'7261000',
	'7261100',
	'7261300',
	'7261400',
	'7261500',
	'7261600',
	'7261700',
	'7261800',
	'7261900',
	'7262000',
	'7262100',
	'7262200',
	'7262300',
	'7262400',
	'7262500',
	'7262600',
	'7262700',
	'7262800',
	'7262900',
	'7263000',
	'7263100',
	'7263200',
	'7263300',
	'7263400',
	'7263500',
	'7263600',
	'7263700',
	'7263800',
	'7263900',
	'7264000'
)));
define('SAG_TARGET', serialize(array()));
define('APPLICATION_EMAIL_ENABLED', 1);