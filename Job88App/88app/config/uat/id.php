<?php

define('APP88_ID','88App_Job_'.strtoupper(COUNTRY).'_'.strtoupper(ENV).'_v'.VERSION);
define('APP88_VERSION','v'.VERSION);

define('APP88_DB_USER', '88appsweb');
define('APP88_DB_PWD', 'DCDvvTgITuEw');
define('APP88_DB_NAME', '88app_job_id');

/*
|--------------------------------------------------------------------------
| Error Logging Threshold
|--------------------------------------------------------------------------
|
| If you have enabled error logging, you can set an error threshold to
| determine what gets logged. Threshold options are:
| You can enable error logging by setting a threshold over zero. The
| threshold determines what gets logged. Threshold options are:
|
|	0 = Disables logging, Error logging TURNED OFF
|	1 = Error Messages (including PHP errors)
|	2 = Debug Messages
|	3 = Informational Messages
|	4 = All Messages
|
| For a live site you'll usually only enable Errors (1) to be logged otherwise
| your log files will fill up very fast.
|
*/
define('LOG_THRESHOLD', 1); // Suggested, DEV: 2, UAT and PROD: 1

define('AUTH_APP_ID', '29a91e2d530d43978f709e40f2ede12e'); // Generate new one for a new app
define('AUTH_APP_SECRET', '123456');
define('AUTH_JQUERY_PATH', 'http://sg.uat.shop.88static.com/s/v'.VERSION.'/js/jquery.min.js');
define('AUTH_JSON_PATH', 'http://sg.uat.shop.88static.com/s/v'.VERSION.'/js/json2.min.js');
define('AUTH_ACCESS_TOKEN_PATH', 'http://secure01.uat.88db.com/addon/accesstoken');
define('AUTH_USER_INFO_PATH', 'http://secure01.uat.88db.com/addon/user');
define('APP88_EMAIL_GATEWAY_URL','http://secure01.uat.88db.com/addon/email');
define('APP88_EMAIL_SHOP_OWNER_GATEWAY_URL','http://secure01.uat.88db.com/addon/emailShopOwner');
define('APP88_EMAIL_88DB_MEMBER_GATEWAY_URL','http://secure01.uat.88db.com/addon/email88DbMember');

define('AUTH_DB88_ACCESS_TOKEN_PATH', 'http://secure01.uat.88db.com/addon/db88accesstoken');
define('APP88_SAG_SUPPORT', 0);
define('APP88_DEFAULT_SAG_SUPPORT', 1);
define('SAG_88DB_CATEGORY_WS','http://preview.id.88db.com/id/WebServices/Shop/AppWS.asmx?wsdl');
define('SAG_88DB_CATEGORY_CACHE_DURATION',12*60); // minutes
define('SAG_JOB_TARGET', serialize(array(
	'ws_path' => 'http://id.uat.sag.88apps.net/gateway/index',
	'channelId' => 'db88_ID_',
	'topic' => 'job',
	'mapping' => array(
		'companyAddr',
		'jobDesc',
		'jobTitle',
		'email',
		'phone',
		'contactName',
		'jobType',
		'salary'
	)
)));
define('ALLOW_CHANNEL_LIST', serialize(array(
	'1019',
	'3001',
	'3002',
	'3003',
	'3004',
	'3005',
	'3006',
	'3007',
	'3008',
	'3009',
	'3010',
	'3011',
	'3012',
	'3013',
	'3014',
	'3015',
	'3016',
	'3017',
	'3018',
	'3019',
	'3020'
)));
define('SAG_TARGET', serialize(array()));
define('APPLICATION_EMAIL_ENABLED', 1);