/**
 * @author MingHo
 */

var ieSelectionBookmark;
          
window.App88EventHandler = {
	onMediaLibraryCallback: function(result){
		for (var i in result) {
			if (result[i]['errCode'] == "success") {
				var imgSrc = result[i]['imgLink'];
				var imgTag = '<img src="' + imgSrc + '" />';
				if (tinymce.isIE) {
					tinyMCE.activeEditor.selection.moveToBookmark(ieSelectionBookmark);
	            }
				tinyMCE.execCommand('mceInsertContent', false, imgTag);				
			} else {
				alert('Upload image failed. Error code: ' + result[i]['errCode']);
			}
		}
	}
};

(function() {
	// Load plugin specific language pack
	tinymce.PluginManager.requireLangPack('mediaLibrary');

	tinymce.create('tinymce.plugins.MediaLibrary', {
		/**
		 * Initializes the plugin, this will be executed after the plugin has been created.
		 * This call is done before the editor instance has finished it's initialization so use the onInit event
		 * of the editor instance to intercept that event.
		 *
		 * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
		 * @param {string} url Absolute URL to where the plugin is located.
		 */
		init : function(ed, url) {
			// Register the command so that it can be invoked by using tinyMCE.activeEditor.execCommand('mceExample');
			ed.addCommand('mediaLibrary', function() {
				mediaLibrary.open();
			});

			// Register example button
			ed.addButton('mediaLibrary', {
				title : 'mediaLibrary.desc',
				cmd : 'mediaLibrary',
				image : url + '/img/icon_imageLibrary.gif',
				onclick : function() {
					ed.focus();
					if (tinymce.isIE) {
						ieSelectionBookmark = ed.selection.getBookmark();
					}
					var data = {callBackFunction: 'onMediaLibraryCallback'};
					shop88api.openMediaLib(data);
				}
			});

			// Add a node change handler, selects the button in the UI when a image is selected
			/*ed.onNodeChange.add(function(ed, cm, n) {
				cm.setActive('example', n.nodeName == 'IMG');
			});*/
		},

		/**
		 * Creates control instances based in the incomming name. This method is normally not
		 * needed since the addButton method of the tinymce.Editor class is a more easy way of adding buttons
		 * but you sometimes need to create more complex controls like listboxes, split buttons etc then this
		 * method can be used to create those.
		 *
		 * @param {String} n Name of the control to create.
		 * @param {tinymce.ControlManager} cm Control manager to use inorder to create new control.
		 * @return {tinymce.ui.Control} New control instance or null if no control was created.
		 */
		createControl : function(n, cm) {
			return null;
		},

		/**
		 * Returns information about the plugin as a name/value array.
		 * The current keys are longname, author, authorurl, infourl and version.
		 *
		 * @return {Object} Name/value array containing information about the plugin.
		 */
		getInfo : function() {
			return {
				longname : 'Media Library',
				author : '88Shop',
				authorurl : 'http://hk.shop.88db.com',
				infourl : 'http://hk.shop.88db.com',
				version : "1.0"
			};
		}
	});

	// Register plugin
	tinymce.PluginManager.add('mediaLibrary', tinymce.plugins.MediaLibrary);
})();