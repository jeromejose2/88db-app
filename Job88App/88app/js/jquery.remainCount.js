
/**
 * @author MingHo
 */
if (window.jQuery) {
	(function(){
		function charCount(chr, str){
			var count = 0;
			for (var i = str.length - 1; i >= 0; i--){
				if(str.charAt(i) == chr)
					count++;
			}
			return count;
		}
		/**
		 * To count and limit text field
		 * @param {jQuery} showInOj
		 * @param {int} maxText
		 */
		if (!jQuery.lang) 
			jQuery.lang = {};
		jQuery.lang.charactersRemaining = "%remain% character(s) remaining";
		
		jQuery.fn.remainCount = function(showInOj, maxText, remainStr){
			if (this[0]) {
				if (!this[0].showInOj) {
					var rcKeyUp = function(eventObject){
						rcUpdateRemain.call(this);
					};
					
					var getValue = function(){
						if(jQuery.browser.msie && parseFloat(jQuery.browser.version) <= 8 ){
							var val = this.value;
							while(val.indexOf("\r") != -1){
								val = val.replace("\r", "");
							}
							return val;
						}
						return this.value;
					}
					
					var rcStopKeyEvt = function(eventObject){
						if (eventObject) {
							if (!eventObject.ctrlKey && !eventObject.altKey) {
								var keyWhich = eventObject.which;
								//console.log("Key Code:"+eventObject.keyCode);
								//console.log(keyWhich);
								if ((keyWhich > 31 && keyWhich < 33) ||
								(keyWhich > 46 && keyWhich < 112) ||
								(keyWhich > 123 && keyWhich != 144 && keyWhich != 145 && keyWhich != 229 && keyWhich != 197)) {
									//console.log("Remain:"+this.maxText);
									if (this.maxText - getValue.call(this).length <= 0) {
										//console.log("Stop");
										eventObject.preventDefault();
									}
								}
							}
						}
					};
					
					var rcPasteEvt = function(eventObject){
						var val = getValue.call(this);
						if (val.length > this.maxText) {
							rcLengthCorr.call(this);
						} else {
							var that = this;
							setTimeout(function(){
								rcLengthCorr.call(that)
							}, 50);
						}
					};
					
					var rcLengthCorr = function(){
						var val = getValue.call(this);
						if (val.length > this.maxText) {
							this.value = val.substr(0, this.maxText);
						}
						rcUpdateRemain.call(this);
					};
					
					var rcUpdateRemain = function(){
						var val = getValue.call(this);
						var textLen = val.length;
						if($(this).hasClass("dbShop-ui-watermark")){
							textLen=0;
						}
						var reMainText = this.maxText - textLen;
						if (reMainText < 0) 
							reMainText = 0;
						this.showInOj.html((remainStr || jQuery.lang.charactersRemaining).replace("%remain%", reMainText));
						if(window.chrome){
							var lineCount = charCount("\n", val);
							$(this).attr("maxlength", this.maxText + reMainText + lineCount);
						}
					};
					
					
					
					if (typeof(showInOj) === "string") {
						showInOj = $(showInOj);
					}
					
					if (this.length != 1 || showInOj.length != 1) 
						throw new Error("remainCount only accept on Object.");
					this[0].showInOj = showInOj;
					this[0].maxText = maxText;
					this.keyup(rcKeyUp);
					
					this.keydown(rcStopKeyEvt);
					this.keypress(rcStopKeyEvt);
					this.change(function(){
						rcLengthCorr.call(this);
					});
					
					if(window.chrome){
						this.attr("maxlength", maxText*2);
					} else {
						this.attr("maxlength", maxText);
					}
					//rcKeyUp.call(this[0]);
					this.bind("paste", rcPasteEvt);
					var that = this[0];
					this[0].updateRemainChar = function(){
						rcUpdateRemain.call(that);
					}
				}
				this[0].updateRemainChar();
			}
		};
	}())
}
