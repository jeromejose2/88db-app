<?php 
	include_once 'includePHPHeaders.php';
?>
<?php 
	if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') !== FALSE) && (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') === FALSE) && empty($_COOKIE))
	{
		echo "<script>\n";
		if (isset($_GET['appdata']) && !empty($_GET['appdata']))
		{
			echo "	top.location.href='front/safari?url=". urlencode('http://'.urldecode($_GET["shopdomain"]).'/'.urldecode($_GET["pagepath"]).'?apd='.urldecode($_GET["appdata"])) ."';\n";
		}
		else if (isset($_GET['apd']) && !empty($_GET['apd']))
		{
			echo "	top.location.href='front/safari?url=". urlencode('http://'.urldecode($_GET["shopdomain"]).'/'.urldecode($_GET["pagepath"]).'?apd='.urldecode($_GET["apd"])) ."';\n";
		}
		else
		{
			echo "	top.location.href='front/safari?url=". urlencode('http://'.urldecode($_GET["shopdomain"]).'/'.urldecode($_GET["pagepath"])) ."';\n";
		}
		echo "</script>\n";
		die;
	}
?>
<?php
	header('p3p: CP="NOI ADM DEV PSAi COM NAV OUR OTR STP IND DEM"');
	session_start();
	
	$_SESSION['app_param'][$_GET["cid"]] = $_GET;
	
	if (isset($_GET["device"]) && ($_GET["device"] == "M"))
	{
		header('Location: front/index?cid='.$_GET["cid"]);
	}
	elseif (isset($_GET["mode"]) && ($_GET["mode"] == "E"))
	{
		header('Location: admin/app/entry?cid='.$_GET["cid"].'&mode='.$_GET["mode"]);
		// Enter edit mode after confirming the current user is owner
	}
	else
	{
		// Visiter mode
		header('Location: front/index?cid='.$_GET["cid"]);
	}
