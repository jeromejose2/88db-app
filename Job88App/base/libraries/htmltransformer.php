<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class HtmlTransformer {

	private static $_blackList = array('script', 'iframe', 'form', 'object', 'link', 'head', 'meta', 'style');
	private $_encoding = 'UTF-8';
	private $_htmlMarkups;
	private $_customBlackList = array();
	private $_dom;
	private $_xpath;
	
	public function __construct()
	{
		
	}
	
	
	public function getMobilizedHTML($htmlMarkups, $customBlackList = NULL, $encoding = NULL)
	{
		$this->setHtmlMarkups($htmlMarkups, $encoding);
		$this->setCustomBlacklist($customBlackList);
		$this->mobilizeHTML();
		return $this->getHtmlMarkups();
	}
	
	
	protected function setHtmlMarkups($htmlMarkups, $encoding)
	{
		$this->_htmlMarkups = $htmlMarkups;
		
		if ( !empty($encoding) )
		{
			$this->_encoding = $encoding;
		}
	}
	
	
	protected function getHtmlMarkups()
	{
		return mb_convert_encoding($this->_htmlMarkups, $this->_encoding, 'HTML-ENTITIES');
	}
	
	
	protected function setCustomBlacklist($customBlackList)
	{
		if ( !empty($customBlackList) && is_array($customBlackList) )
		{
			$this->_customBlackList = $customBlackList;
		}
	}
	
	
	protected function mobilizeHTML()
	{
		if (empty($this->_htmlMarkups))
		{
			return '';
		}
		
		$this->_htmlMarkups = $this->_htmlStrip($this->_htmlMarkups);
		$this->_htmlMarkups = $this->_stripEmptyTags($this->_htmlMarkups);

		$this->_htmlMarkups = str_replace("&", "&amp;", $this->_htmlMarkups);
		$this->_htmlMarkups = mb_convert_encoding($this->_htmlMarkups, 'HTML-ENTITIES', $this->_encoding);
		
		// convert to dom before continuing with processing 
		$this->_dom = new DOMDocument();
		$this->_dom->preserveWhiteSpace = false;
		@$this->_dom->loadHTML($this->_htmlMarkups);
		$this->_xpath = new DOMXpath($this->_dom);
	
		$this->_sanitizeHtmlNode($this->_dom->documentElement);
		$this->_mobilizeDom($this->_dom->documentElement);
		
		// convert back to HTML markups
		$this->_htmlMarkups = preg_replace(array('/^\<\!DOCTYPE.*?<html><body>/si',
													'!</body></html>!si'),
											'',
											$this->_dom->saveHTML());
	}


	private function _htmlStrip($html)
	{
		$html = preg_replace('/(sizcache|sizset|width|height)=(\"|\')[^\"\']*\2/i', '', $html);
		
		if (NULL===$html)
		{
			log_message('error', 'preg replacing fails at line: ' . __LINE__);
			return '';
		}
		
		$html = preg_replace_callback('/style=(\"|\')[^\"\']*\1/i', array($this, "_filterStyleAttribute"), $html);
		
		if (NULL===$html)
		{
			log_message('error', 'preg replacing fails at line: ' . __LINE__);
			return '';
		}
		
		return $html;
	}


	private function _stripEmptyTags($html)
	{
		$html = preg_replace('/((&nbsp;|\s)*<br[\s]*[\/]?>(&nbsp;|\s)*){3,}/i', '<br/><br/>', $html);
		
		if (NULL===$html)
		{
			log_message('error', 'preg replacing fails at line: ' . __LINE__);
			return '';
		}
		
		
		$html = preg_replace('/<([\w]*)[\s]?>(&nbsp;|\s)*<\/\1[\s]*>/i', '', $html);
		
		if (NULL===$html)
		{
			log_message('error', 'preg replacing fails at line: ' . __LINE__);
			return '';
		}
		
		return $html;
	}

	
	private function _sanitizeHtmlNode($rootNode)
	{
		if (XML_ELEMENT_NODE == $rootNode->nodeType)
		{
			$tagName = strtolower($rootNode->localName);
			if ( in_array($tagName, $this->_customBlackList) || in_array($tagName, self::$_blackList) )
			{
				$rootNode->parentNode->removeChild($rootNode);
				return;
			}

			if ($rootNode->hasAttributes())
			{
				$attrs = $rootNode->attributes;
				$cnt = $attrs->length;
				
				for ($i = $cnt-1; $i >= 0; $i--)
				{
					$attrName = strtolower($attrs->item($i)->nodeName);
					$attrValue = strtolower($attrs->item($i)->nodeValue);
						
					if ( $this->_startsWith($attrName, 'on') )
					{
						$rootNode->removeAttribute($attrName);
					}
					elseif ( ( $attrName == 'href' || $attrName == 'src' || $attrName == 'dynsrc' || $attrName == 'lowsrc' ) &&
							( !empty($attrValue) ) &&
							( $this->_containsString($attrValue, 'javascript:') )
					)
					{
						$rootNode->removeAttribute($attrName);
					}	
					elseif ( $attrName == 'style' && !empty($attrValue) &&
							( $this->_containsString($attrValue, 'expression') ) ||
							( $this->_containsString($attrValue, 'javascript:') ) ||
							( $this->_containsString($attrValue, 'vbscript:') )
					)
					{
						$rootNode->removeAttribute($attrName);
					}
				}
			}
					
		}

		if ($rootNode->hasChildNodes())
		{
			$cnt = $rootNode->childNodes->length;
			for ($i = $cnt-1; $i >=0 ; $i--)
			{
				$this->_sanitizeHtmlNode($rootNode->childNodes->item($i));
			}
		}

	}


	private function _mobilizeDom($rootNodes)
	{
		$this->_processExtLink($rootNodes);
		$this->_mobilizeTables($rootNodes);
		$this->_processImg($rootNodes);
		$this->_processEmbed($rootNodes);
		$this->_processFont($rootNodes);
		$this->_removeEmptyTags($rootNodes);
	}
	
	
	private function _mobilizeTables($node)
	{
		$tableNodes = $this->_xpath->query('.//table', $node);

		if ( 0 === $tableNodes->length )
		{
			return;
		}
		
		foreach ($tableNodes as $tableNode)
		{
			if ( $this->_shouldTranformTableNodeToDIV($tableNode) )
			{
				$this->_tranformTableNodeToDIV($tableNode);
			}
			else
			{
				$tableNode->setAttribute('style', 'max-width: 100%; width:100%;');
			}
		}		
	}
	
	
	private function _processExtLink($node)
	{
		$childAnchorNodes = $this->_xpath->query('.//a', $node);
	
		if ( $childAnchorNodes && $childAnchorNodes->length > 0 )
		{
			foreach ($childAnchorNodes as $ahchor)
			{
				if ( !$this->_isAnchorTag_LinkTo_88DB($ahchor) )
				{
					$ahchor->setAttribute('rel', 'nofollow');
					$ahchor->setAttribute('target', '_blank');
				}
			}
		}
	}
	
	
	private function _processImg($node)
	{
		$childImgNodes = $this->_xpath->query('.//img', $node);
		
		if ($childImgNodes && $childImgNodes->length > 0)
		{
			foreach ($childImgNodes as $imgNode)
			{
				$imgNode->setAttribute('style', 'max-width: 100%;');
				
				// TODO: Lazy load image script support for 88DB?
				// add largy load  <pre class="lazyimage"><!--<img/>--></pre>				
				/*
				$imageHtml = $this->_dom->saveHTML($imgNode);
				
				$imageHtml = $this->_filterStringForComment($imageHtml);
				
				$commentNode = $this->_dom->createComment($imageHtml);
				
				$preNode = $this->_dom->createElement('pre');
				$preNode->setAttribute('class', 'lazyimage');
				$preNode->appendChild($commentNode);

				$imgNode->parentNode->replaceChild($preNode, $imgNode);	
				*/	
			}
		}
		
	}
	
	
	private function _processEmbed($node)
	{
		$childEmbedNodes = $this->_xpath->query('.//embed', $node);
		
		if ($childEmbedNodes && $childEmbedNodes->length > 0)
		{
			foreach ($childEmbedNodes as $embedNode)
			{
				if ('' === $embedNode->getAttribute('type'))
				{
					$embedNode->setAttribute('type', 'application/x-shockwave-flash');
				}
			}
		}                
	}
	
	
	private function _processFont($node)
	{
		$childFontNodes = $this->_xpath->query('.//font', $node);
		
		if ($childFontNodes && $childFontNodes->length > 0)
		{
			foreach ($childFontNodes as $fontNode)
			{
				$fontNode->removeAttribute('size');
			}
		}
	}
	
	
	private function _removeEmptyTags($node)
	{
		$this->_removeNodeIfEmpty($node, 'span');
		$this->_removeNodeIfEmpty($node, 'p');
		$this->_removeNodeIfEmpty($node, 'tr');
		$this->_removeNodeIfEmpty($node, 'div');
	}
	
	
	private function _removeNodeIfEmpty($startNode, $tagName)
	{
		$childTagNodes = $this->_xpath->query('.//'.$tagName, $startNode);
		
		if ($childTagNodes && $childTagNodes->length > 0)
		{
			foreach ($childTagNodes as $tagNode)
			{
				$imgChdNodes = $this->_xpath->query('.//img', $tagNode);
				
				if ($imgChdNodes && $imgChdNodes->length > 0)
				{
					continue;
				}
				
				$embChdNodes = $this->_xpath->query('.//embed', $tagNode);
				
				if ($embChdNodes && $embChdNodes->length > 0)
				{
					continue;
				}
				
				$breakChdNodes = $this->_xpath->query('.//br', $tagNode);
				
				if ($breakChdNodes && $breakChdNodes->length > 0)
				{
					continue;
				}
				
				$innerText = $tagNode->nodeValue;
				$innerText = str_replace('\n', '', $innerText);
				$innerText = str_replace(' ', '', $innerText);
				$innerText = str_replace('&nbsp;', '', $innerText);
				$innerText = trim($innerText);
				
				if ('' === $innerText)
				{
					$tagNode->parentNode->removeChild($tagNode);
				}
				else
				{
					$decodedInnerTxt = html_entity_decode($innerText, ENT_QUOTES, "UTF-8");
					$ptn = '/'. html_entity_decode('&nbsp;', ENT_QUOTES, "UTF-8") . '/u';
					$stripedDedInTxt = preg_replace($ptn, '', $decodedInnerTxt);
					if ('' === $stripedDedInTxt)
					{
						$tagNode->parentNode->removeChild($tagNode);
					}
				}
				
			}
		}
	}
	
	
	private function _tranformTableNodeToDIV($tableNode)
	{
		// a temp array to hold the div tags
		$divNodeArray = array();

		$trNodes = $this->_xpath->query('.//tr', $tableNode);
		
		// if there is no direct <tr> inside <table>, then they might be inside <tbody>
		if ( !($trNodes && $trNodes->length > 0) )
		{
			$trNodes = $this->_xpath->query('.//tbody/tr', $tableNode);
		}
		
		// if this is just an empty table, then just remove it
		if ( !($trNodes && $trNodes->length > 0) )
		{
			$tableNode->parentNode->removeChild($tableNode);
			return;
		}
		
		foreach ($trNodes as $trNode)
		{
			$tdNodes = $this->_xpath->query('.//td', $trNode);
			
			if ( !($tdNodes && $tdNodes->length > 0) )
			{
				continue;
			}
			
			$div = $this->_dom->createElement('div');
			
			foreach ($tdNodes as $tdNode)
			{
				if ($tdNode === null) 
				{
					continue;
				}
			
				// transfer all td's children to div
				$childNodes = $tdNode->childNodes;
				
				while ($childNodes->length > 0)
				{
					$div->appendChild($childNodes->item(0));
				}
				
				// push it to the temp array
				$divNodeArray[] = $div;				
			}
		}
		
		foreach ($divNodeArray as $divNode)
		{
			$tableNode->parentNode->insertBefore($divNode, $tableNode);
		}
		
		$tableNode->parentNode->removeChild($tableNode);

	}
	
	
	private function _filterStyleAttribute($matches)
	{
		$rules = array();
	
		$num_of_rules = preg_match_all('/(font-weight|font-family|font-style|font-variant|([\s|;|\"]color)|background-color|font):[^;]*;/i', html_entity_decode($matches[0], ENT_QUOTES), $rules);
	
		if ($num_of_rules > 0)
		{
			$styleAttr = 'style="';
	
			foreach ($rules[0] as $r)
			{
				$r = trim($r);
					
				if ( $this->_startsWith($r, 'font:') || $this->_startsWith($r, ';font:') )
				{
					$styleAttr .= htmlentities($this->_filterFontStyleRules($r), ENT_QUOTES, $this->_encoding);
				}
				else if ( $this->_startsWith($r, '"color') || $this->_startsWith($r, ';color') )
				{
					$styleAttr .= htmlentities(substr($r, 1), ENT_QUOTES, $this->_encoding) . ' ';
				}
				else
				{
					$styleAttr .= htmlentities($r, ENT_QUOTES, $this->_encoding) . ' ';
				}
			}
	
			$styleAttr .= '"';
			return $styleAttr;
	
		}
		else if ($num_of_rules === 0)
		{
			return '';
		}
		else
		{
			log_message('error', 'preg matching fails at line: ' . __LINE__);
			return '';
		}
	
	}
	
	
	private function _filterFontStyleRules($fontAttrStr)
	{
		if ( $this->_startsWith($fontAttrStr, ';') )
		{
			$fontAttrStr = substr($fontAttrStr, 1);
		}
	
		$res = preg_replace('/\d+(px|em)\/\d+(px|em)|\d+(px|em)/i', '', $fontAttrStr);
	
		if (NULL !== $res)
		{
			return $res;
		}
		else
		{
			log_message('error', 'preg replacing fails at line: ' . __LINE__);
			return '';
		}
	
	}	
	
	/****************
	 * Helpers
	 ****************/
	
	
	private function _isAnchorTag_LinkTo_88DB($aNode)
	{
		if ( $aNode->hasAttribute('href') )
		{
			return $this->_isURL_88DB_Host($aNode->getAttribute('href'));
		}
		else
		{
			return false;
		}
	}
	
	
	private function _shouldTranformTableNodeToDIV($tableNode)
	{
		$embedNodes = $this->_xpath->query('.//embed', $tableNode);
	
		if ($embedNodes && $embedNodes->length > 0)
		{
			return true;
		}	
	
		$imgNodes = $this->_xpath->query('.//img', $tableNode);
	
		if ($imgNodes && $imgNodes->length > 0)
		{
			// check for PNG or JPG, if there is transform, exclude gif for now
			foreach ($imgNodes as $img)
			{
				$src = 	strtolower($img->getAttribute('src'));
	
				if ( $this->_endsWith($src, 'jpg') || $this->_endsWith($src, 'png') )
				{
					return true;
				}
			}
		}
	
		return false;
	}
	
	
	private function _isURL_88DB_Host($url)
	{
		if ( !$this->_startsWith($url, 'http://') && !$this->_startsWith($url, 'https://') )
		{
			return FALSE;
		}
		$components = parse_url($url);
		// TODO: Change to array of allowed domains (88db.com, 88apps.net, 88shop.com)?
		return $this->_containsString($components['host'], '88db.com');
	}
	
	
	private function _filterStringForComment($input_str)
	{
		return str_replace('--', '-', $input_str);
	}
	
	
	private function _containsString($haystack, $needle)
	{
		return stripos($haystack, $needle) !== FALSE;
	}


	private function _startsWith($haystack, $needle)
	{
		return !strncasecmp($haystack, $needle, strlen($needle));
	}
	
	
	private function _endsWith($haystack, $needle)
	{
		$length = strlen($needle);
		if ($length == 0) 
		{
			return true;
		}
		return strtolower(substr($haystack, -$length)) === strtolower($needle);
	}


	private function _getInnerHTML($element)
	{
		$innerHTML = '';
		$children = $element->childNodes;
		foreach ($children as $child)
		{
			$tmp_dom = new DOMDocument();
			$deep = true;
			$tmp_dom->appendChild($tmp_dom->importNode($child, $deep));
			$innerHTML .= trim($tmp_dom->saveHTML());
		}
		return $innerHTML;
	}

}