<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class App88Param {

	private $CI;
	private $ap;
	private $cid;
	
	public function __construct($config = array())
	{
		$this->CI =& get_instance();
		if (isset($_GET['cid']) && ($_GET["cid"] != ''))
		{
			$this->cid = $_GET['cid'];
			$cs = $this->CI->session->userdata('app_param');
			$this->ap = $cs[$this->cid];
		}
		else
		{
			$this->ap = $this->CI->session->userdata('app_param');
		}
	}
	
	public function selectInstance($cid)
	{
		$this->cid = $cid;
		$cs = $this->CI->session->userdata('app_param');
		$this->ap = $cs[$this->cid];
	}
	
	public function setParam($ap)
	{
		$cs = $this->CI->session->userdata('app_param');
		$cs[$this->cid] = $ap;
		$this->CI->session->set_userdata('app_param', $cs);
		$this->ap = $ap;
		return;
	}
	
	public function getByKey($key)
	{
		if (empty($key))
		{
			return NULL;
		}
		else if (empty($this->ap))
		{
			return NULL;
		}
		else if (isset($this->ap[$key]))
		{
			return $this->ap[$key];
		}
		else
		{
			return FALSE;
		}
	}
	
	public function setByKey($key, $value)
	{
		if (empty($key))
		{
			return NULL;
		}
		else if (empty($this->ap))
		{
			return NULL;
		}
		else
		{
			$this->ap[$key] = $value;
			$this->setParam($this->ap);
			return TRUE;
		}
	}
	
	public function isEditMode() {
		if (empty($this->ap))
		{
			return FALSE;
		}
		else if ($this->getByKey('mode') == 'E')
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function getCommonJs() {
		return $this->getByKey('common_js_path');
	}
	
	public function getApplicationGuid() {
		return $this->getByKey('appid');
	}
	
	public function get88AppGuid() {
		return $this->getByKey('cid');
	}
	
	public function get88ShopGuid() {
		return $this->getByKey('sid');
	}
	
	public function getCountry() {
		return $this->getByKey('country');
	}
	public function getRegion() {
		return $this->getByKey('region');
	}
	public function getLang() {
		return $this->getByKey('lang');
	}
	public function getShopName() {
		return $this->getByKey('shopName');
	}
	public function getSection() {
		return $this->getByKey('section');
	}
	public function getPagePath() {
		return $this->getByKey('pagepath');
	}
	public function getSlib() {
		return $this->getByKey('slib');
	}
	public function getShopDomain() {
		return $this->getByKey('shopdomain');
	}
	public function getAppPath() {
		return $this->getByKey('apppath');
	}
	public function setAppPath($value) {
		return $this->setByKey('apppath', $value);
	}
	public function setAccessToken($value) {
		return $this->setByKey('88db_access_token', $value);
	}
	public function getAccessToken() {
		return $this->getByKey('88db_access_token');
	}
	public function isMobile() {
		if (empty($this->ap))
		{
			return FALSE;
		}
		elseif ($this->getByKey('device') == 'M')
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
		
	public function getDefaultLang()
	{
		return $this->getByKey('defaultLang');
	}
	
	public function getAvailableLang()
	{
		$result = $this->getByKey('availableLang');
		if ($result)
		{
			return explode(',',$result);
		}
		else 
		{
			return FALSE;
		}
	}

	public function getSBLang()
	{
		return $this->getByKey('sbLang');
	}
}