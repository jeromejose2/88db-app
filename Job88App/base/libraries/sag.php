<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Last Modified: 29 August 2013 Chris Lam
class SAG {

	private $CI;
	
	private $_langMapping;

	private $_allow_channel_id_list;
	 
	public function __construct($config = array())
	{
		$this->CI =& get_instance();
		$this->CI->load->library('app88common');
		$this->CI->load->library('app88param');
		$this->CI->load->library('app88log');
		$this->CI->load->library('guid');
		$this->CI->load->helper('form');

		$this->_langMapping = $this->CI->config->item('88db_ws_lang');
		$this->_allow_channel_id_list = !defined('ALLOW_CHANNEL_LIST')? TRUE : unserialize(ALLOW_CHANNEL_LIST);
	}
	
	private function _generateRequestXml($encryptedToken, $sessionKey, $request)
	{
		$encryptedRequestBin = $requestXmlEncryptedBin = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, base64_decode($sessionKey), $request, MCRYPT_MODE_ECB);
		$encryptedRequest = base64_encode($encryptedRequestBin);
	
		//no need to do xml encode as the encrypted strings are base64 encode already
		$xmlText =
		'<envelope>'.
		'<token>'.$encryptedToken.'</token>'.
		'<request>'.$encryptedRequest.'</request>'.
		'</envelope>';
		return $xmlText;
	}
	
	private function _generateRequest($channelId, $topic, $callbackUrl, $data_mapping, $quota='', $images = array())
	{
		$xml = '<?xml version="1.0" encoding="UTF-8"?>';
		$xml .= '<request>';
		$xml .= '<channelId>'.form_prep($channelId).'</channelId>';
		$xml .= '<quota>'.form_prep($quota).'</quota>';
		$xml .= '<topic>'.form_prep($topic).'</topic>';
		$xml .= '<urlCallback>'.form_prep($callbackUrl).'</urlCallback>';
	
		foreach ($data_mapping as $k => $v)
		{
			$xml .= '<field name="'.form_prep($k).'">'.form_prep($v).'</field>';
		}
	
		if (isset($images) && is_array($images) && count($images) > 0)
		{
			for ($i = 0; $i < count($images); ++$i)
			{
				if ($i == 0)
				{
					$xml .= '<field name="mainphoto">'.$images[$i].'</field>';
				}
				else
				{
					$xml .= '<field name="photo'.($i - 1).'">'.$images[$i].'</field>';
				}
			}
		}
	
		$xml .= '</request>';
		
		return $xml;
	}
	
	public function shoot($channelId, $topic, $callbackUrl, $ws_url, $data_mapping, $quota='', $images = array())
	{
		
		$db88_access_token = $this->CI->app88param->getAccessToken();
		
		$db88AccessToken = $db88_access_token['token'];
		$db88SessionKey = $db88_access_token['session_key'];
		
		if (empty($db88AccessToken) || empty($db88SessionKey))
		{
			$this->CI->app88log->log_message('sag', 'ERROR', __FILE__.'('.__METHOD__.'@'. __LINE__ .') '.'Unable to obtain access token and session key for SAG');
			show_error($this->CI->lang->line('err_invalid_param'));
		}
		
		$request = $this->_generateRequest( $channelId, $topic, $callbackUrl, $data_mapping, $quota, $images);
		$requestXml = $this->_generateRequestXml($db88AccessToken, $db88SessionKey, $request);
		$this->CI->app88log->log_message('sag', 'ERROR', '[LOG] SAG Request XML: '.$request);
		$this->CI->app88log->log_message('sag', 'ERROR', '[LOG] SAG Request XML (Encrypted): '.$requestXml);
		$this->CI->app88log->log_message('sag', 'ERROR', 'after client init');
		try
		{
			$soapClient = new SoapClient($ws_url);
			$soapClientReturn = $soapClient->submitRequest($requestXml);
			$this->CI->app88log->log_message('sag', 'ERROR', '[LOG] SAG Task Id:'.$soapClientReturn.', SAG Request XML: '.$request);
			$this->CI->app88log->log_message('sag', 'ERROR', '[LOG] SAG Task Id:'.$soapClientReturn.', SAG Request XML (Encrypted): '.$requestXml);
			return $soapClientReturn;
		}
		catch (SoapFault $exception)
		{
			$this->CI->app88log->log_message('sag', 'ERROR', '[LOG] SAG Request XML: '.$request);
			$this->CI->app88log->log_message('sag', 'ERROR', '[LOG] SAG Request XML (Encrypted): '.$requestXml);
			$this->CI->app88log->log_message('sag', 'ERROR', __FILE__.'('.__METHOD__.'@'. __LINE__ .') '.'Unable to submit request to SAG '."\n".$exception);
			return false;
		}
	}
	
	public function getGenericCategories()
	{
		$this->CI->load->driver('cache');
		$result = $this->CI->cache->file->get('88DB_GetAllCategoriesResult');
		if (!$result)
		{
			$soapClient = new SoapClient(SAG_88DB_CATEGORY_WS);
			$soapResult = $soapClient->GetAllCategories();
			
			$result = array();
			
			$supported_lang = explode(';', $soapResult->GetAllCategoriesResult->SupportedLanguage);
			
			if (in_array($this->_langMapping[APP88_ADMIN_LANGUAGE], $supported_lang))
			{
				$lang = $this->_langMapping[APP88_ADMIN_LANGUAGE];
			}
			elseif (in_array($this->_langMapping[$this->CI->config->item['language']], $supported_lang))
			{
				$lang = $this->_langMapping[$this->CI->config->item['language']];
			}
			else
			{
				$lang = $supported_lang[0];
			}
			
			$this->_constructGenericCategoriesResult($soapResult->GetAllCategoriesResult->Categories, $result, unserialize(SAG_JOB_TARGET), $lang);
			$this->CI->cache->file->save('88DB_GetAllCategoriesResult', $result, SAG_88DB_CATEGORY_CACHE_DURATION*60);
		}

		return $result;
	}
	
	private function _constructGenericCategoriesResult($cat, &$result, $target_template, $lang, $level = 1)
	{
		for ($i=0;$i<sizeof($cat->SAGCategoryProxy);$i++)
		{
			if ($level > 1)
			{
				$tmp = array();
				$tmp['name'] = '';
				for ($j=0;$j<sizeof($cat->SAGCategoryProxy[$i]->CategoryName->Values->MultiLangValue );$j++)
				{
					if (sizeof($cat->SAGCategoryProxy[$i]->CategoryName->Values->MultiLangValue ) > 1)
					{
						if ($cat->SAGCategoryProxy[$i]->CategoryName->Values->MultiLangValue[$j]->LangName == $lang)
						{
							$tmp['name'] = $cat->SAGCategoryProxy[$i]->CategoryName->Values->MultiLangValue[$j]->Value;
							break;
						}
					}
					else
					{
						$tmp['name'] = $cat->SAGCategoryProxy[$i]->CategoryName->Values->MultiLangValue->Value;
						break;					
					}
				}
	
				$tmp['ws_path'] = $target_template['ws_path'];
				$tmp['channelId'] = $target_template['channelId'].$cat->SAGCategoryProxy[$i]->CategoryId;
				$tmp['paid_only'] = $cat->SAGCategoryProxy[$i]->PaidOnly?'1':'0';
				$tmp['topic'] = $target_template['topic'];
				$tmp['mapping'] = $target_template['mapping'];
				
				if (is_array($this->_allow_channel_id_list))
				{
					if (in_array($cat->SAGCategoryProxy[$i]->CategoryId, $this->_allow_channel_id_list))
					{
						$result[$tmp['channelId']] = $tmp;
						$this->_constructGenericCategoriesResult($cat->SAGCategoryProxy[$i]->ChildCategories, $result, $target_template, $lang, $level+1);
					}
				}
				else 
				{
					$result[$tmp['channelId']] = $tmp;
					$this->_constructGenericCategoriesResult($cat->SAGCategoryProxy[$i]->ChildCategories, $result, $target_template, $lang, $level+1);
				}
			}
			else
			{
				$tmp = array();
				$tmp['name'] = '';
				for ($j=0;$j<sizeof($cat->SAGCategoryProxy[$i]->CategoryName->Values->MultiLangValue );$j++)
				{
					if (sizeof($cat->SAGCategoryProxy[$i]->CategoryName->Values->MultiLangValue ) > 1)
					{
						if ($cat->SAGCategoryProxy[$i]->CategoryName->Values->MultiLangValue[$j]->LangName == $lang)
						{
							$tmp['name'] = $cat->SAGCategoryProxy[$i]->CategoryName->Values->MultiLangValue[$j]->Value;
							break;
						}
					}
					else
					{
						$tmp['name'] = $cat->SAGCategoryProxy[$i]->CategoryName->Values->MultiLangValue->Value;
						break;
					}
				}
				$tmp['channelId'] = $target_template['channelId'].$cat->SAGCategoryProxy[$i]->CategoryId;
				
				if (is_array($this->_allow_channel_id_list))
				{
					if (in_array($cat->SAGCategoryProxy[$i]->CategoryId, $this->_allow_channel_id_list))
					{
						$result[$tmp['channelId']] = $tmp;
						$result[$tmp['channelId']]['child'] = array();
						$this->_constructGenericCategoriesResult($cat->SAGCategoryProxy[$i]->ChildCategories, $result[$tmp['channelId']]['child'], $target_template, $lang, $level+1);
					}
				}
				else
				{
					$result[$tmp['channelId']] = $tmp;
					$result[$tmp['channelId']]['child'] = array();
					$this->_constructGenericCategoriesResult($cat->SAGCategoryProxy[$i]->ChildCategories, $result[$tmp['channelId']]['child'], $target_template, $lang, $level+1);
				}
			}
		}
	}
}