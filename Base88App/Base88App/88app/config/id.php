<?php
define('APP88_ID','88app_base_'.strtoupper(COUNTRY).'_'.strtoupper(ENV).'_v'.VERSION);
define('APP88_VERSION','v'.VERSION);

define('APP88_FRONT_LANGUAGE', 'id');
define('APP88_ADMIN_LANGUAGE', 'id');

define('APP88_TIMEZONE_OFFSET','+8');

$config['app88_dummy'] = '';
$config['app88_support_feed'] = 1;

