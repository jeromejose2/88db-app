<?php
define('APP88_DB_USER', 'root');
define('APP88_DB_PWD', '');
define('APP88_DB_NAME', '88app_base_hk');

/*
|--------------------------------------------------------------------------
| Error Logging Threshold
|--------------------------------------------------------------------------
|
| If you have enabled error logging, you can set an error threshold to
| determine what gets logged. Threshold options are:
| You can enable error logging by setting a threshold over zero. The
| threshold determines what gets logged. Threshold options are:
|
|	0 = Disables logging, Error logging TURNED OFF
|	1 = Error Messages (including PHP errors)
|	2 = Debug Messages
|	3 = Informational Messages
|	4 = All Messages
|
| For a live site you'll usually only enable Errors (1) to be logged otherwise
| your log files will fill up very fast.
|
*/
define('LOG_THRESHOLD', 2); // Suggested, DEV: 2, UAT and PROD: 1

define('AUTH_APP_ID', '866b40679cb248c18155044f901302a3'); // Generate new one for a new app
define('AUTH_APP_SECRET', '123456');
define('AUTH_JQUERY_PATH', 'http://hk.dev.shop.88static.com/s/v'.VERSION.'/js/jquery.min.js');
define('AUTH_JSON_PATH', 'http://hk.dev.shop.88static.com/s/v'.VERSION.'/js/json2.min.js');
define('AUTH_ACCESS_TOKEN_PATH', 'http://secure01.dev.88db.com/addon/accesstoken');
define('AUTH_DB88_ACCESS_TOKEN_PATH', 'http://secure01.dev.88db.com/addon/db88accesstoken');
define('AUTH_USER_INFO_PATH', 'http://secure01.dev.88db.com/addon/user');
define('APP88_EMAIL_GATEWAY_URL','http://secure01.dev.88db.com/addon/email');
define('APP88_EMAIL_SHOP_OWNER_GATEWAY_URL','http://secure01.dev.88db.com/addon/emailShopOwner');
define('APP88_EMAIL_88DB_MEMBER_GATEWAY_URL','http://secure01.dev.88db.com/addon/email88DbMember');

$config['app88_dummy'] = '';