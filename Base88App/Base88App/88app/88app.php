<?php 
	include_once 'includePHPHeaders.php';
?>
<?php 
	if ((strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') !== FALSE) && empty($_COOKIE))
	{
		echo "<script>\n";
		if (isset($_GET['appdata']) && !empty($_GET['appdata']))
		{
			echo "	top.location.href='fo/safari?url=". urlencode('http://'.urldecode($_GET["shopdomain"]).'/'.urldecode($_GET["pagepath"]).'?apd='.urldecode($_GET["appdata"])) ."';\n";
		}
		else if (isset($_GET['apd']) && !empty($_GET['apd']))
		{
			echo "	top.location.href='fo/safari?url=". urlencode('http://'.urldecode($_GET["shopdomain"]).'/'.urldecode($_GET["pagepath"]).'?apd='.urldecode($_GET["apd"])) ."';\n";
		}
		else
		{
			echo "	top.location.href='fo/safari?url=". urlencode('http://'.urldecode($_GET["shopdomain"]).'/'.urldecode($_GET["pagepath"])) ."';\n";
		}
		echo "</script>\n";
		die;
	}
	else
	{
		error_reporting(0);
		header('p3p: CP="NOI ADM DEV PSAi COM NAV OUR OTR STP IND DEM"');
		session_start();
		
		$_SESSION['app_param'][$_GET["cid"]] = $_GET;
		
		if (isset($_GET["device"]) && ($_GET["device"] == "M"))
		{
			header('Location: front/index?cid='.$_GET["cid"]);
		}
		elseif (isset($_GET["mode"]) && ($_GET["mode"] == "E"))
		{
			header('Location: admin/bo/init88App?cid='.$_GET["cid"].'&mode='.$_GET["mode"]);
			// Enter edit mode after confirming the current user is owner
		}
		else
		{
			// Visiter mode
			header('Location: fo/index?cid='.$_GET["cid"]);
		}
	}
