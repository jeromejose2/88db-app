<?php

include_once('country_lang.php');

$lang['label'] = 'text';
$lang['err_unable_load_app'] = 'Unable to load app data';
$lang['err_uable_load_shopowner'] = 'Unable to load shop owner data';
$lang['err_session_expired'] = 'Session has been expired, please reload the shop';
$lang['err_invalid_response'] = 'Invalid Response';