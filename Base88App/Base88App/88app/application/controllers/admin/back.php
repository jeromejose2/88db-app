<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Back extends CI_Controller {
	
	public function __construct($config = array())
	{
		parent::__construct();
		$this->lang->load('admin', $this->app88common->get_shop_lang());
		$this->load->library('app88Authen');
		
		if (!$this->app88userinfo->getMemberId() || !$this->app88param->get88AppGuid())
		{
			show_error($this->lang->line('err_session_expired'));
		}
		if (!$this->app88userinfo->isShopOwner())
		{
			redirect('fo/index');
		}
		
	}

	public function welcome()
	{
		echo "<H1>Back Office</H1>";
		echo $this->config->item('name');
	}
	
}