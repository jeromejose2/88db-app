<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front extends CI_Controller {
	
	public function __construct($config = array())
	{
		parent::__construct();
		$this->lang->load('front', $this->app88param->getLang()?$this->app88param->getLang():APP88_FRONT_LANGUAGE);
	
		if (!$this->app88param->get88AppGuid())
		{
			show_error($this->lang->line('err_session_expired'));
		}
	
	}

	public function welcome()
	{
		$this->load->view('front/welcome');
	}
}