<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FO extends CI_Controller {

	public function index()
	{
		
		session_start();
		if (isset($_SESSION['app_param'][$_GET["cid"]]))
		{
			$this->app88param->setParam($_SESSION['app_param'][$_GET["cid"]]);
			$this->app88param->selectInstance($_GET["cid"]);
			unset($_SESSION['app_param'][$_GET["cid"]]);
		}
		
		$appdata = json_decode(urldecode($this->app88param->getByKey('appdata')));
		if ($appdata->redirect != '')
		{
			$this->app88param->setByKey('appdata','');
			redirect($appdata->redirect);
		}
		else
		{
			redirect('front/welcome');
		}
		
	}
	
	
	public function safari()
	{
		session_start();
		header('Location: '.urldecode($_GET['url']));
	}
	
}