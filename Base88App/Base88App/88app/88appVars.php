﻿<?php

define('ENV', 'dev');
define('COUNTRY','hk');
define('VERSION','1301');
if(file_exists('config/'.COUNTRY.'.php'))
{
	include 'config/'.COUNTRY.'.php';
}
if(file_exists('config/'.ENV.'/'.COUNTRY.'.php'))
{
	include 'config/'.ENV.'/'.COUNTRY.'.php';
}