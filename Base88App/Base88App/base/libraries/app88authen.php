<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class App88Authen {

	private $CI;

	public function __construct($config = array())
	{
		$this->CI =& get_instance();
	}
	
	public function authen($handler, $state = '', $scope = 'user_info, email_user')
	{
		
		$state_obj = json_decode($state);
		echo "<html>\n";
		echo "<head>\n";
		
		//TODO: the following js url need to be adjusted
		echo "	<script type=\"text/javascript\" src=\"".AUTH_JQUERY_PATH."\"></script>\n";
		echo "	<script type=\"text/javascript\" src=\"".AUTH_JSON_PATH."\"></script>\n";

		$app_param = $this->CI->session->userdata('app_param');
		if ( !empty($app_param[$state_obj->cid]['common_js_path']) )
		{
			echo "	<script type=\"text/javascript\" src=\"". $app_param[$state_obj->cid]['common_js_path'] ."\"></script>\n";
		}

		echo "	<script type=\"text/javascript\">\n";
		echo "		$(document).ready(function() {\n";
		echo "			var state = '". $state ."';\n";
		echo "			var data = {\n";
		echo "				appid : '". AUTH_APP_ID ."',\n";
		echo "				redirecturi : '". urlencode($handler) ."',\n";
		echo "				scope : '". $scope ."',\n";
		//echo "				state : JSON.stringify(state)\n";
		echo "				state : state\n";
		echo "			};\n";
		echo "			shop88api.authen(data);\n";
		echo "		});\n";
		echo "	</script>\n";
		echo "</head>\n";
		echo "<body>\n";
		echo "</body>\n";
		echo "</html>\n";
		
	}
	
	public function getAccessToken($code, $redirect)
	{

		if (!empty($code))
		{
			$token_url = AUTH_ACCESS_TOKEN_PATH . '?appid=' . AUTH_APP_ID . '&appsecret=' . AUTH_APP_SECRET . '&code=' . $code . '&redirecturi='. $redirect;
			// TODO: Check file_get_contents error
			$token_return = file_get_contents($token_url);
			if ($token_return == '')
			{
				show_error($this->lang->line('err_invalid_response'));
			}
			$response = json_decode($token_return);
			$token = isset($response->token) ? $response->token : '';
	
			if ($response != NULL)
			{
				if (!empty($token))
				{
					return $token;
				}
				else
				{
					// No token returned
					show_error($response->error->message);
				}
			}
			else
			{
				show_error($this->lang->line('err_invalid_response'));
			}
			
		}
		else
		{
			show_error($this->lang->line('err_invalid_response'));
		}
	}
	
	public function getDB88AccessToken($token)
	{
		if (!empty($token))
		{
			$token_url = AUTH_DB88_ACCESS_TOKEN_PATH . '?access_token=' . $token;
			// TODO: Check file_get_contents error
			$token_return = file_get_contents($token_url);
			if ($token_return == '')
			{
				show_error($this->lang->line('err_invalid_response'));
			}
			$response = json_decode($token_return);
			$token = isset($response->db88AccessToken) ? $response->db88AccessToken : '';
			$sessionKey = isset($response->sessionKey) ? $response->sessionKey : '';
	
			if ($response != NULL)
			{
				if (!empty($token))
				{
					return array('token'=>$token, 'session_key'=>$sessionKey);
				}
				else
				{
					// No token returned
					show_error($response->error->message);
				}
			}
			else
			{
				show_error($this->lang->line('err_invalid_response'));
			}
				
		}
		else
		{
			show_error($this->lang->line('err_invalid_response'));
		}
	}
	
	public function getUserInfo($token)
	{
		if (!empty($token))
		{
			
			$user_url = AUTH_USER_INFO_PATH . '?access_token=' . $token;
			$user_return = file_get_contents($user_url);
			if ($user_return == '')
			{
				show_error($this->lang->line('err_invalid_response'));
			}
			$response = json_decode($user_return);
			
			if ($response != NULL)
			{
				if (isset($response->error))
				{
					die($response->error->message);
				}
				else
				{
					return $user_return;
				
				}
			}
			else
			{
				show_error($this->lang->line('err_invalid_response'));
			}
			
		}
		else
		{
			// No token returned
			show_error($this->lang->line('err_invalid_response'));
		}
	}
}