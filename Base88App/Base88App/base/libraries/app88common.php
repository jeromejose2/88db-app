<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class App88Common {

	private $CI;

	public function __construct($config = array())
	{
		$this->CI =& get_instance();
		$this->CI->load->library('app88param');
	}
	
	
	public function js_quote($js,$forUrl=false)
	{
		
		if($forUrl)
			return strtr($js,array('%'=>'%25',"\t"=>'\t',"\n"=>'\n',"\r"=>'\r','"'=>'\"','\''=>'\\\'','\\'=>'\\\\','</'=>'<\/'));
		else
			return strtr($js,array("\t"=>'\t',"\n"=>'\n',"\r"=>'\r','"'=>'\"','\''=>'\\\'','\\'=>'\\\\','</'=>'<\/'));
	}
	
	
	public function force_rollback()
	{
		$this->CI->db->simple_query('ROLLBACK');
		$this->CI->db->simple_query('SET AUTOCOMMIT=1');
	}
	
	public function benchmark()
	{
		$hostName = gethostname();
			
		if($hostName){
			$pos = strpos($hostName, '.');
	
			if($pos)
			{
				$hostName = substr($hostName, 0, $pos);
			}
		}
	
		return $hostName.' '.APP88_ID.' '.date('m/d/Y h:i:s A').' | exec time:'. $this->CI->benchmark->elapsed_time() .' version:'.APP88_VERSION;
	}
	
	public function local_date($db_time = '', $date_format = 'Y-m-d')
	{
		if ($db_time == '') $db_time = gmdate('Y-m-d H:i:s');
		return date($date_format, strtotime($db_time)+60*60*APP88_TIMEZONE_OFFSET);
	}
	
	public function local_datetime($db_time = '', $date_format = 'Y-m-d H:i:s')
	{
		return $this->local_date($db_time,$date_format);
	}
	
	public function get_shop_url()
	{
		// Remarked due to combine site, region is no longer in use
		/*if (($this->CI->app88param->getShopDomain() != '') && ($this->CI->app88param->getRegion() != '') && ($this->CI->app88param->getPagePath() != ''))
		{
			return 'http://' . $this->CI->app88param->getShopDomain() . "/" . $this->CI->app88param->getRegion() . "/" . $this->CI->app88param->getPagePath();
		}
		else*/if (($this->CI->app88param->getShopDomain() != '') && ($this->CI->app88param->getPagePath() != ''))
		{
			return 'http://' . $this->CI->app88param->getShopDomain() . "/" . $this->CI->app88param->getPagePath();
		}
		else
		{
			return FALSE;
		}
	}
	
	public function get_direct_link($route)
	{
		$shop_url = $this->get_shop_url();
		if ($shop_url !== FALSE)
		{
			if (isset($route) && $route!='')
			{
				$appDataArr = array('redirect'=>$route);
				$url = $shop_url . '?appdata=' . urlencode(json_encode($appDataArr));
				return $url;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	
	public function get_shop_lang()
	{
		return in_array($this->CI->app88param->getSBLang(), $this->CI->config->item('admin_supported_lang')) ? $this->CI->app88param->getSBLang() : APP88_ADMIN_LANGUAGE;
	}

	public function show_readonly()
	{
		header('Location: http://'.$this->CI->app88param->getShopDomain().'/_maintenance/index.php');
	}
}