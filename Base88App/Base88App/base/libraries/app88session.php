<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class App88Session {

	private $_CI;
	private $_appSession;
	private $_cid;
	
	public function __construct($config = array())
	{
		$this->_CI = & get_instance();
		if (isset($_GET['cid']) && ($_GET["cid"] != ''))
		{
			$this->_cid = $_GET['cid'];
			$cs = $this->_CI->session->userdata('app_session');

			if (isset($cs[$this->_cid])) 
			{
				$this->_appSession = $cs[$this->_cid];
			}
			else
			{
				$this->_appSession = array();
			}
		}
		else
		{
			$cs = $this->_CI->session->userdata('app_session');
			if (FALSE !== $cs && is_array($cs))
			{
				$this->_appSession = $cs;
			}
			else
			{
				$this->_appSession = array();
			}
		}
	}
	
	public function selectInstance($cid)
	{
		$this->_cid = $cid;
		$cs = $this->_CI->session->userdata('app_session');
		if (isset($cs[$this->_cid])) 
		{
			$this->_appSession = $cs[$this->_cid];
		}
		else
		{
			$this->_appSession = array();
		}
	}
	
	public function setSession($appSession)
	{
		$cs = $this->_CI->session->userdata('app_session');
		$cs[$this->_cid] = $appSession;	//$cs might be FALSE, but PHP still can handle the convert
		$this->_CI->session->set_userdata('app_session', $cs);
		$this->_appSession = $appSession;
		return;
	}
	
	public function getByKey($key)
	{
		if (empty($key))
		{
			return NULL;
		}
		else if (empty($this->_appSession))
		{
			return NULL;
		}
		else if (isset($this->_appSession[$key]))
		{
			return $this->_appSession[$key];
		}
		else
		{
			return FALSE;
		}
	}
	
	public function setByKey($key, $value)
	{
		if (empty($key))
		{
			return NULL;
		}
		else if (!isset($this->_appSession) || !is_array($this->_appSession))
		{
			return NULL;
		}
		else
		{
			$this->_appSession[$key] = $value;
			$this->setSession($this->_appSession);
			return TRUE;
		}
	}
	
}