<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class App88UserInfo {

	private $CI;
	private $ui;

	public function __construct($config = array())
	{
		$this->CI =& get_instance();
		$this->ui = $this->CI->session->userdata('user_info');
	}
		
	public function setUserInfo($ui)
	{
		$this->CI->session->set_userdata('user_info', $ui);
		$this->ui = $ui;
		return;
	}
	
	public function isShopOwner()
	{
		if (empty($this->ui))
		{
			return NULL;
		}
		else if ($this->ui->isShopOwner == 'T')
		{
			return TRUE;
		}
		else
		{
			return FALSE;
			//return TRUE;  // temperately set to return true since the development environment has no true owner records.
		}
	}
	
	public function getMemberId()
	{
		if (empty($this->ui))
		{
			return NULL;
		}
		else if (isset($this->ui->uid))
		{
			return $this->ui->uid;
		}
		else
		{
			return NULL;
		}
	}
	
	public function getBoUserId()
	{
		if (empty($this->ui))
		{
			return NULL;
		}
		else if (isset($this->ui->boUserId))
		{
			return $this->ui->boUserId;
		}
		else
		{
			return NULL;
		}
	}
	
	public function getReviewerName()
	{
		if (empty($this->ui))
		{
			return NULL;
		}
		else if (isset($this->ui->reviewerName))
		{
			return $this->ui->reviewerName;
		}
		else
		{
			return NULL;
		}
	}

	public function isBoUser()
	{
		if (empty($this->ui))
		{
			return NULL;
		}
		else if (isset($this->ui->boUserId))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function isReviewer()
	{
		if (empty($this->ui))
		{
			return NULL;
		}
		else if (isset($this->ui->reviewerName))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function getDisplayName()
	{
		if (empty($this->ui))
		{
			return NULL;
		}
		else if (isset($this->ui->displayname))
		{
			return $this->ui->displayname;
		}
		else
		{
			return NULL;
		}
	}
	
	public function getShopName()
	{
		return $this->getDisplayName();
	}
	
	public function isAcceptEmail()
	{
		if (empty($this->ui))
		{
			return NULL;
		}
		else if ($this->ui->acceptEmail == 'T')
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function getEmail()
	{
		if (empty($this->ui))
		{
			return NULL;
		}
		else if (isset($this->ui->userEmail))
		{
			return $this->ui->userEmail;
		}
		else
		{
			return NULL;
		}
	}
	
	public function isInternalAccess()
	{
		if (empty($this->ui))
		{
			return NULL;
		}
		else if ($this->ui->isInternalAccess == 'T')
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function isInternalTraffic()
	{
		if (empty($this->ui))
		{
			return NULL;
		}
		else if ($this->ui->isInternalTraffic == 'T')
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}