<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Atom {

	private $CI;

	private $title;
	private $app_domain;
	private $app_type;
	private $cid;
	private $entry = array(); 
	
	public function __construct($config = array())
	{
		
		$this->CI =& get_instance();
		$this->CI->load->library('app88common');
		$this->CI->load->library('guid');
		
		if (isset($config['title']) && !empty($config['title']))
		{
			$this->set_title($config['title']);
		}
		if (isset($config['domain']) && !empty($config['domain']))
		{
			$this->set_app_domain($config['domain']);
		}
		if (isset($config['app_type']) && !empty($config['app_type']))
		{
			$this->set_app_type($config['app_type']);
		}
		if (isset($config['cid']) && !empty($config['cid']))
		{
			$this->set_cid($config['cid']);
		}
	}
	
	public function set_title($title)
	{
		$this->title = $title;
		return TRUE;
	}
	public function set_app_domain($domain)
	{
		$this->app_domain = $domain;
		return TRUE;
	}
	public function set_app_type($type)
	{
		$this->app_type = $type;
		return TRUE;
	}
	public function set_cid($cid)
	{
		$this->cid = $cid;
		return TRUE;
	}
	public function add_entry($entry)
	{
		if (!empty($entry['title']) && !empty($entry['app_path']))
		{
			$new_entry = array();
			foreach(array_keys($entry) as $key) {
				$new_entry[$key] = $entry[$key];
			}
			
			if (empty($entry['id']))
			{
				$new_entry['id'] = $this->CI->guid->newGuid()->toString();
			}
			else
			{
				$new_entry['id'] = $entry['id'];
			}
			if (empty($entry['updated']))
			{
				$new_entry['updated'] = $this->_format_date();
			}
			else
			{
				$new_entry['updated'] = $this->_format_date($entry['updated']);
			}
			array_push($this->entry, $new_entry);
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function xml()
	{
		$xml = '<?xml version="1.0" encoding="utf-8"?>'."\n";
		$xml .= '<feed xmlns="http://www.w3.org/2005/Atom" xmlns:db88="http://www.88db.com/" >'."\n";
		$xml .= '	<title><![CDATA['. $this->title .']]></title>'."\n";
		$xml .= '	<db88:app>'. $this->app_type .'</db88:app>'."\n";
		$xml .= '	<link href="'. $this->app_domain .'" />'."\n";
		$xml .= '	<updated>'.$this->_format_date().'</updated>'."\n";
		$xml .= '	<id>'.$this->cid.'</id>'."\n";
	
		foreach($this->entry as $entry)
		{
			$xml .= "	<entry>\n";
			foreach(array_keys($entry) as $key)
			{
				switch($key)
				{
					case "db88":
						foreach(array_keys($entry[$key]) as $db88_key)
						{
							$xml .= "		<db88:". $db88_key ."><![CDATA[". $entry['db88'][$db88_key] ."]]></db88:". $db88_key .">\n";
						}
						break;
					case "app_path":
						$xml .= "		<link href=\"appdata=". urlencode(json_encode(array('redirect' => $entry['app_path']))) ."\" />\n";
						break;
					case "link":
						// Ignore link element to avoid conflit
						break;
					default:
						$xml .= "		<". $key ."><![CDATA[". $entry[$key] ."]]></". $key .">\n";
						break;
				}

			}
			$xml .= "	</entry>\n";
		}
		
		$xml .= '</feed>'."\n";
		return $xml;
	}
	
	private function _format_date($date = '', $offset = APP88_TIMEZONE_OFFSET)
	{
		if (empty($date))
		{
			return $this->_format_date($this->CI->app88common->local_datetime());
		}
		else
		{
			list($offset_hour, $offset_min) = explode('.', $offset);
			$offset_hour = str_replace(array('+','-'),'',$offset_hour);
			$offset_sign = ($offset > 0)?'+':'-';
			
			return date('Y-m-d',strtotime($date))."T".date('H:i:s',strtotime($date)).$offset_sign. substr('00'.$offset_hour, -2).":".substr('00'.(60*$offset_min/10), -2);
		}
	}
	
}