<?php
class App88Email
{
	const Success = 0;
	const Invalid_request = 100;
	const Unauthorized_user = 101;
	const Access_denied = 102;
	const Unsupported_response_type = 103;
	const Invalid_scope = 104;
	const Server_error = 105;
	const Temporarily_unavailable = 106;
	const Database_error = 107;
	
	const Invalid_cookie = 108;
	const Permission_denied = 109;
	
	const Invalid_credential = 110;
	const Invalid_code = 111;
	const Used_code = 112;
	const Expired_code = 113;
	const Unmatched_redirecturi = 114;
	const Invalid_token = 115;
	const Expired_token = 116;
	const Shopguid_notfound = 117;
	
	const Email_fail = 118;
	const Invalid_Email = 119;
	
	private $CI;
	
	public function __construct($config = array())
	{
		$this->CI =& get_instance();
		$this->CI->load->library('app88log');
	}
	
	public function sendEmail($params)
	{
		if (ENV == 'dev')
		{
			require_once(realpath('./application/libraries/PHPMailer/class.phpmailer.php'));
			$result = $this->_validateParams($params);
			if(count($result)==0)
			{
				$mail = new PHPMailer();
				$mail->IsSMTP();
				$mail->Host = "localhost";
				$mail->CharSet = 'utf-8';
				$mail->AddAddress($params['recemail'],$params['recname']);
				$mail->SetFrom($params['senderemailstr'],$params['sendernamestr']);
				$mail->Subject = $params['subject'];
				$mail->Body = $params['body'];
				$mail->IsHTML(true);
				if(!$mail->Send()) {
					echo 'Message was not sent.';
					echo 'Mailer error: ' . $mail->ErrorInfo;
				}
			}
			$result = json_encode($result);
			return $result;
		}
		else
		{
			$result = $this->_validateParams($params);
			if(count($result)==0)
			{
				$addr_str=APP88_EMAIL_GATEWAY_URL."?method=POST";
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $addr_str);
				curl_setopt($ch, CURLOPT_VERBOSE, 1);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_POST, 1);
				
				$params_str = '';
				foreach($params as $key=>$value)
				{
					if($params_str) $params_str.='&';
					$params_str = $params_str.$key."=".urlencode(trim($value));
				}
				curl_setopt($ch, CURLOPT_POSTFIELDS, $params_str);
				
				$result = curl_exec($ch);
				
				if(!$result)
				{
					$this->CI->app88log->log_message('email','error', __METHOD__." ".__LINE__." ".__FILE__.": ".curl_error($ch)."(".curl_errno($ch).")");
						
					return FALSE;
				}
				
				$result = json_decode($result,true);
				$result = $this->_genErrorResponse($result['error']['code']);
				curl_close($ch);
			}
			$result=json_encode($result);
			return $result;
		}
	}
	
	
	public function sendEmailShopOwner($params)
	{
		$result = $this->_validateParams($params);
		if(count($result)==0)
		{
			$addr_str=APP88_EMAIL_SHOP_OWNER_GATEWAY_URL;
			$params_str = '';
			foreach($params as $key=>$value)
			{
				if($params_str) $params_str.='&';
				$params_str = $params_str.$key."=".urlencode(trim($value));
			}
			$addr_str.= '?'.$params_str;
			$result = file_get_contents($addr_str);
			$result = json_decode($result,true);
			$result = $this->_genErrorResponse($result['error']['code']);
		}
		$result=json_encode($result);
		return $result;
	}

	
	public function sendEmail88DbMember($params)
	{
		$result = $this->_validateParams($params);
		if(count($result)==0)
		{
			$addr_str=APP88_EMAIL_88DB_MEMBER_GATEWAY_URL;
			$params_str = '';
			foreach($params as $key=>$value)
			{
				if($params_str) $params_str.='&';
				$params_str = $params_str.$key."=".urlencode(trim($value));
			}
			$addr_str.= '?'.$params_str;
			$result = file_get_contents($addr_str);
			$result = json_decode($result,true);
			$result = $this->_genErrorResponse($result['error']['code']);
		}
		$result=json_encode($result);
		return $result;
	}
	
	private function _validateParams($params)
	{
		foreach($params as $key=>$value)
		{
			if(empty($value))
			{
				return $this->_genErrorResponse(App88Email::Invalid_request);
			}
			elseif(($key=='senderemailstr'||$key=='recemail') && !filter_var($value, FILTER_VALIDATE_EMAIL))
			{
				return $this->_genErrorResponse(App88Email::Invalid_Email);
			}
		}
		return array();
	}
	
	private function _genErrorResponse($errorCode)
	{
		return array('error'=>array('code'=>$errorCode, 'message'=>$this->_getErrorMessage($errorCode)));
	}
	
	private function _getErrorMessage($errorCode)
	{
		switch($errorCode)
		{
			case App88Email::Success:
				return "Success";
			case App88Email::Invalid_request:
					return "Invalid Request";
			case App88Email::Unauthorized_user:
				return "User Unauthorized";
			case App88Email::Access_denied:
					return "No Access Token";
			case App88Email::Unsupported_response_type:
				return "Response Type Unsupported";
			case App88Email::Invalid_scope:
				return "Invalid Scope";
			case App88Email::Server_error:
				return "Server Error";
			case App88Email::Temporarily_unavailable:
				return "Service Temporarilly Unavailable";
			case App88Email::Database_error:
				return "Database Error";
			case App88Email::Invalid_cookie:
				return "User Not Login";
			case App88Email::Permission_denied:
				return "User Refused Permission";
			case App88Email::Invalid_credential:
				return "Invalid Application Credential";
			case App88Email::Invalid_code:
				return "Invalid Code";
			case App88Email::Used_code:
				return "Used Code";
			case App88Email::Expired_code:
				return "Expired Code";
			case App88Email::Unmatched_redirecturi:
				return "Invalid RedirectURI";
			case App88Email::Invalid_token:
				return "Invalid Token";
			case App88Email::Expired_token:
				return "Expired Token";
			case App88Email::Shopguid_notfound:
					return "Shop Guid Not Found";
			case App88Email::Email_fail:
				return "Email Sending Failure";
			case App88Email::Invalid_Email:
				return "Invalid Email";
			default:
				return '';
		}
	}
}