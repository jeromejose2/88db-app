<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class App88UserInfo {

	private $CI;
	private $ui;

	public function __construct($config = array())
	{
		$this->CI =& get_instance();
		$this->ui = $this->CI->session->userdata('user_info');
	}
		
	public function setUserInfo($ui)
	{
		$this->CI->session->set_userdata('user_info', $ui);
		$this->ui = $ui;
		return;
	}
	public function isShopOwner() {
		if (empty($this->ui))
		{
			return NULL;
		}
		else if ($this->ui->isShopOwner == 'T')
		{
			return TRUE;
		}
		else
		{
			return FALSE;
			//return TRUE;  // temperately set to return true since the development environment has no true owner records.
		}
	}
	public function getMemberId() {
		if (empty($this->ui))
		{
			return NULL;
		}
		else if (isset($this->ui->uid))
		{
			return $this->ui->uid;
		}
		else
		{
			return NULL;
		}
	}
	public function getMemberEmail()
	{
		if (empty($this->ui))
		{
			return NULL;
		}
		else if (isset($this->ui->userEmail))
		{
			return $this->ui->userEmail;
		}
		else
		{
			return NULL;
		}
	}
	public function getShopName() {
		if (empty($this->ui))
		{
			return NULL;
		}
		else if (isset($this->ui->displayname))
		{
			return $this->ui->displayname;
		}
		else
		{
			return NULL;
		}
	}
	public function isAcceptEmail() {
		if (empty($this->ui))
		{
			return NULL;
		}
		else if ($this->ui->acceptEmail == 'T')
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}