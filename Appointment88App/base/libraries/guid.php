<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Guid{
	
	protected $bin;
	protected $txt;

	public function __construct()
	{
	}
	
	
	public static function createFromString($value)
	{
		$guid = new Guid();
		
		if(!empty($value))
		{
			$guid->bin = pack("H*", $value);
			$guid->txt = $value;
		}

		return $guid;
	}
	
	public static function createFromBinary($value)
	{
		$guid = new Guid();
		
		if(!empty($value))
		{
			$guid->bin = $value;
		}
		
		return $guid;
	}
	
	public static function newGuid()
	{
		$uuidTxt = md5(uniqid(mt_rand(), true));
		return Guid::createFromString($uuidTxt);
	}
	
	public function toBin()
	{
		return $this->bin;
	}
	
	public function toString(){
		return $this->__toString();
	}

	public function __toString()
	{
		if($this->txt === null)
		{
			list(,$this->txt) = unpack("H*", $this->bin); 
		}
		
		return $this->txt;
	}
	
	public function equals($guid){
		return $this->bin == $guid->bin;		
	}
}