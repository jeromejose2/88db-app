
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `88app_appointment_hk` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `88app_appointment_hk`;

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `app` WRITE;
/*!40000 ALTER TABLE `app` DISABLE KEYS */;
/*!40000 ALTER TABLE `app` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;



INSERT INTO `resource` (`Id`, `Guid`, `AppInstanceGuid`, `Name`, `PhoneNumber`, `Email`, `Description`, `Status`, `Version`, `IsSnapShot`, `CreatedTime`, `ModifiedTime`) VALUES
(1, UNHEX('2162dac36fe1d6b77a92309b8a33f3d7'), UNHEX('59d63e7b761c6427cb3ae0556f97b80a'), 'Threapist Johnny', '1310000000', 'aaa@a.com', 'aaaaaaa', 'A', 0, 'F', '2013-05-09 11:37:06', NULL),
(2, UNHEX('069480bd530c6bdd543ccc3b5c5b70e4'), UNHEX('59d63e7b761c6427cb3ae0556f97b80a'), '劉三姐2', '131002300', 'aaffefawfa@a.com', 'bbbbbbbbb', 'A', 1, 'F', '2013-05-09 11:37:06', NULL),
(3, UNHEX('7f849762b29b697df6f55de56082f81c'), UNHEX('59d63e7b761c6427cb3ae0556f97b80a'), 'Threapist Taka', '1313213 300', 'afffffa@a.com', 'cccccccccc', 'A', 0, 'F', '2013-05-09 11:37:06', NULL),
(4, UNHEX('069480bd530c6bdd543ccc3b5c5b70e4'), UNHEX('59d63e7b761c6427cb3ae0556f97b80a'), '劉三姐1', '131002300', 'aaffefawfa@a.com', 'bbbbbbbbb', 'A', 0, 'T', '2013-05-21 03:51:12', NULL),
(5, UNHEX('069480bd530c6bdd543ccc3b5c5b70e4'), UNHEX('59d63e7b761c6427cb3ae0556f97b80a'), '劉三姐2', '131002300', 'aaffefawfa@a.com', 'bbbbbbbbb', 'A', 1, 'T', '2013-05-21 04:01:48', NULL);

INSERT INTO `resource_dayoff` (`Status`,`Id`, `ResourceId`, `StartDate`, `EndDate`, `CreatedTime`, `ModifiedTime`) VALUES
('A', 1, 2, '2013-05-20', '2013-05-20', '2013-05-09 15:19:13', NULL),
('A', 4, 2, '2013-05-26', '2013-05-28', '2013-05-08 14:38:40', NULL),
('A', 3, 2, '2013-06-11', NULL, '2013-05-08 14:38:40', NULL);

INSERT INTO `resource_normal_hour` (`Id`, `ResourceId`, `Day`, `StartTime`, `EndTime`, `Status`, `CreatedTime`, `ModifiedTime`) VALUES
(1, 2, 1, '08:00:00', '11:00:00', 'A', '2013-05-09 15:15:18', NULL),
(2, 2, 1, '14:00:00', '18:00:00', 'A', '2013-05-09 15:15:18', NULL),
(3, 2, 2, '09:00:00', '18:00:00', 'A', '2013-05-09 15:15:18', NULL),
(4, 2, 4, '10:00:00', '14:00:00', 'A', '2013-05-10 10:02:34', NULL),
(5, 2, 5, '10:00:00', '15:00:00', 'A', '2013-05-10 11:43:11', NULL),
(6, 2, 3, '09:00:00', '18:00:00', 'A', '2013-05-15 09:06:33', NULL);

INSERT INTO `resource_provided_service` (`Id`, `ServiceId`, `ResourceId`, `Status`, `CreatedTime`, `ModifiedTime`) VALUES
(1, 4, 1, 'A', '2013-05-09 11:40:24', NULL),
(2, 4, 2, 'A', '2013-05-09 11:40:24', NULL),
(3, 4, 3, 'A', '2013-05-09 11:40:24', NULL),
(4, 3, 3, 'A', '2013-05-09 11:40:24', NULL),
(5, 3, 1, 'A', '2013-05-09 11:40:24', NULL),
(6, 2, 2, 'A', '2013-05-09 11:40:24', NULL);

INSERT INTO `resource_special_hour` (`Status`, `Id`, `ResourceId`, `Date`, `StartTime`, `EndTime`, `CreatedTime`, `ModifiedTime`) VALUES
('A', 2, 2, '2013-05-21', '14:00:00', '17:50:00', '2013-01-01 00:00:00', NULL);

INSERT INTO `service` (`Id`, `Guid`, `AppInstanceGuid`, `Name`, `Duration`, `Cost`, `Description`, `TimeSlotDivision`, `AvailablePerTimeSlot`, `MinTime`, `AdvanceTime`, `Status`, `Version`, `IsSnapShot`, `CreatedTime`, `ModifiedTime`) VALUES
(2, UNHEX('2bc3d9cb26271a5dc84caa07a186ffad'), UNHEX('59d63e7b761c6427cb3ae0556f97b80a'), 'Spa - Invigorating Cleanse ', 181, 171.0000, 'bbbb', 45, 1, 1, 336, 'A', 1, 'F', '2013-05-09 09:43:10', NULL),
(3, UNHEX('a58de0b0667de9e4f1c61b479b13c2b0'), UNHEX('59d63e7b761c6427cb3ae0556f97b80a'), 'Spa - Indulgence', 182, 172.0000, 'ccccc', 1, 1, 1, 1, 'A', 1, 'F', '2013-05-09 09:43:10', NULL),
(4, UNHEX('07831f08bed82ae1e433633cf06cbb2f'), UNHEX('59d63e7b761c6427cb3ae0556f97b80a'), '口腔科', 45, 170.0000, 'aaaaaaaaaaaa', 15, 2, 1, 336, 'A', 3, 'F', '2013-05-17 06:04:29', NULL),
(21, UNHEX('07831f08bed82ae1e433633cf06cbb2f'), UNHEX('59d63e7b761c6427cb3ae0556f97b80a'), '口腔科', 45, 170.0000, 'aaaaaaaaaaaa', 15, 2, 1, 336, 'A', 3, 'T', '2013-05-21 03:50:47', NULL);
