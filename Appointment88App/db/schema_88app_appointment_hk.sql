
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

/*!40000 DROP DATABASE IF EXISTS `88app_appointment_hk`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `88app_appointment_hk` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `88app_appointment_hk`;
DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `app`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app` (
  `Id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `AppInstanceGuid` binary(16) NOT NULL COMMENT 'cid passed from 88Shop',
  `ShopGuid` BINARY( 16 ) NOT NULL,
  `OwnerId` varchar(100) DEFAULT NULL COMMENT 'Member Id from 88shop',
  `ShopName` varchar(200) DEFAULT NULL,
  `CreateTime` datetime DEFAULT NULL,
  `ModifiedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `OwnerIdIndex` (`OwnerId`),
  KEY `ShopGuidIndex` (`ShopGuid`),
  KEY `AppInstanceGuidIndex` (`AppInstanceGuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;


--
--  `booking`
--
DROP TABLE IF EXISTS `booking`;
CREATE TABLE `booking` (
  `Id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `CustomerId` bigint(20) unsigned NOT NULL,
  `Guid` binary(16) NOT NULL,
  `ServiceId` bigint(20) unsigned NOT NULL,
  `ResourceId` bigint(20) unsigned NOT NULL,
  `AppInstanceGuid` binary(16) NOT NULL,
  `StartDateTime` datetime NOT NULL,
  `EndDateTime` datetime NOT NULL,
  `IsReminder` char(1) NOT NULL,
  `Status` char(1) NOT NULL,  
  `Note` varchar(2500) NOT NULL,
  `Source` VARCHAR( 50 ) NOT NULL,
  `TimeSlotStatusId` bigint(20) unsigned NOT NULL DEFAULT '0',
  `CreatedTime` datetime NOT NULL,
  `ModifiedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
--  `customer`
--
DROP TABLE IF EXISTS `customer`;
CREATE TABLE IF NOT EXISTS `customer` (
  `Id` bigint(20) unsigned NOT NULL auto_increment,
  `Guid` binary(16) NOT NULL,
  `FirstName` varchar(250) NOT NULL,
  `LastName` varchar(250) NOT NULL,
  `Contact` varchar(25) NOT NULL,
  `Email` varchar(250) NOT NULL,
  `MemberId` bigint(20) unsigned NOT NULL,
  `IsOffline` VARCHAR( 1 ) NOT NULL DEFAULT 'F',
  `AppInstanceGuid` binary(16) NOT NULL,
  `CreatedTime` datetime NOT NULL,
  `ModifiedTime` datetime default NULL,
  PRIMARY KEY  (`Id`),
  KEY `Guid` (`Guid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
--  `customernote`
--
DROP TABLE IF EXISTS `customernote`;
CREATE TABLE `customernote` (
  `Id` bigint(20) unsigned NOT NULL auto_increment,
  `Guid` binary(16) NOT NULL,
  `CustomerId` bigint(20) unsigned NOT NULL,
  `AppInstanceGuid` binary(16) NOT NULL,
  `Message` text NOT NULL,
  `Status` char(1) NOT NULL,
  `CreatedTime` datetime NOT NULL,
  `ModifiedTime` datetime default NULL,
  PRIMARY KEY  (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
--  `deletebookinglog`
--

DROP TABLE IF EXISTS `deletebookinglog`;
CREATE TABLE `deletebookinglog` (
  `Id` bigint(20) unsigned NOT NULL auto_increment,
  `BookingId` bigint(20) unsigned NOT NULL,
  `Reason` text NOT NULL,
  `CreatedTime` datetime NOT NULL,
  `ModifiedTime` datetime default NULL,
  PRIMARY KEY  (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
--  `resource`
--

DROP TABLE IF EXISTS `resource`;
CREATE TABLE `resource` (
  `Id` bigint(20) unsigned NOT NULL auto_increment,
  `Guid` binary(16) NOT NULL,
  `AppInstanceGuid` binary(16) NOT NULL,
  `Name` varchar(300) NOT NULL,
  `PhoneNumber` varchar(20) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Description` longtext NOT NULL,
  `AssignColor` varchar(7) NOT NULL,
  `Status` char(1) NOT NULL default 'A',
  `Version` bigint(20) unsigned NOT NULL,
  `IsSnapShot` char(1) NOT NULL default 'F',
  `CreatedTime` datetime NOT NULL,
  `ModifiedTime` datetime default NULL,
  PRIMARY KEY  (`Id`),
  KEY `Guid` (`Guid`),
  KEY `AppInstanceGuid` (`AppInstanceGuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
--  `resource_dayoff`
--
DROP TABLE IF EXISTS `resource_dayoff`;
CREATE TABLE `resource_dayoff` (
  `Id` bigint(20) unsigned NOT NULL auto_increment,
  `ResourceId` bigint(20) unsigned NOT NULL,
  `StartDate` date NOT NULL,
  `EndDate` date default NULL,
  `Status` char(1) NOT NULL,
  `CreatedTime` datetime NOT NULL,
  `ModifiedTime` datetime default NULL,
  PRIMARY KEY  (`Id`),
  KEY `ResourceId` (`ResourceId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
--  `resource_normal_hour`
--

DROP TABLE IF EXISTS `resource_normal_hour`;
CREATE TABLE `resource_normal_hour` (
  `Id` bigint(20) unsigned NOT NULL auto_increment,
  `ResourceId` bigint(20) unsigned NOT NULL,
  `Day` varchar(10) NOT NULL,
  `StartTime` time NOT NULL,
  `EndTime` time NOT NULL,
  `Status` char(1) NOT NULL,
  `CreatedTime` datetime NOT NULL,
  `ModifiedTime` datetime default NULL,
  PRIMARY KEY  (`Id`),
  KEY `ResourceId` (`ResourceId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
--  `resource_provided_service`
--

DROP TABLE IF EXISTS `resource_provided_service`;
CREATE TABLE `resource_provided_service` (
  `Id` bigint(20) unsigned NOT NULL auto_increment,
  `ServiceId` bigint(20) unsigned NOT NULL,
  `ResourceId` bigint(20) unsigned NOT NULL,
  `Status` char(1) NOT NULL default 'A',
  `CreatedTime` datetime NOT NULL,
  `ModifiedTime` datetime default NULL,
  PRIMARY KEY  (`Id`),
  KEY `ServiceId` (`ServiceId`),
  KEY `ResourceId` (`ResourceId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
--  `resource_special_hour`
--

DROP TABLE IF EXISTS `resource_special_hour`;
CREATE TABLE `resource_special_hour` (
  `Id` bigint(20) unsigned NOT NULL auto_increment,
  `ResourceId` bigint(20) unsigned NOT NULL,
  `Date` date NOT NULL,
  `StartTime` time NOT NULL,
  `EndTime` time NOT NULL,
  `Status` char(1) NOT NULL,
  `CreatedTime` datetime NOT NULL,
  `ModifiedTime` datetime default NULL,
  PRIMARY KEY  (`Id`),
  KEY `ResourceId` (`ResourceId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
--  `sendemail`
--
DROP TABLE IF EXISTS `sendemail`;
CREATE TABLE `sendemail` (
  `Id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `SendTo` varchar(250) NOT NULL,
  `Subject` text NOT NULL,
  `Body` longtext NOT NULL,
  `AppInstanceGuid` binary(16) NOT NULL,
  `RefId` bigint(20) unsigned NOT NULL,
  `EmailType` varchar(50) NOT NULL,
  `CreatedTime` datetime NOT NULL,
  `ModifiedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
--  `service`
--

DROP TABLE IF EXISTS `service`;
CREATE TABLE IF NOT EXISTS `service` (
  `Id` bigint(16) unsigned NOT NULL AUTO_INCREMENT,
  `Guid` binary(16) NOT NULL,
  `AppInstanceGuid` binary(16) NOT NULL,
  `Name` varchar(300) NOT NULL,
  `AssignColor` varchar(7) NOT NULL,
  `Duration` int(11) unsigned NOT NULL,
  `Cost` decimal(19,2) unsigned NOT NULL,
  `Description` longtext NOT NULL,
  `TimeSlotDivision` int(11) unsigned NOT NULL,
  `AvailablePerTimeSlot` int(11) unsigned NOT NULL,
  `MinTime` int(11) unsigned NOT NULL,
  `AdvanceTime` int(11) unsigned DEFAULT NULL,
  `Status` char(1) NOT NULL DEFAULT 'A' COMMENT 'A:action, I:inaction, D:deleted',
  `Version` bigint(20) unsigned NOT NULL,
  `IsSnapShot` char(1) NOT NULL DEFAULT 'F',
  `CreatedTime` datetime NOT NULL,
  `ModifiedTime` datetime DEFAULT NULL,
  `Weight` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY (`Id`),
  KEY `Guid` (`Guid`),
  KEY `AppInstanceGuid` (`AppInstanceGuid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
--  `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting` (
  `Id` bigint(20) unsigned NOT NULL auto_increment,
  `AppInstanceGuid` binary(16) NOT NULL,
  `EnableNotificationEmail` char(1) NOT NULL,
  `EnableReminderEmail` char(1) NOT NULL,
  `ReminderMinute` int(11) NOT NULL,
  `Greeting` longtext NOT NULL,
  `CustomizeTitles` VARCHAR( 2000 ) NOT NULL,
  `CreatedTime` datetime NOT NULL,
  `ModifiedTime` datetime default NULL,
  PRIMARY KEY  (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
--  `timeslotstatus`
--
DROP TABLE IF EXISTS `timeslotstatus`;
CREATE TABLE `timeslotstatus` (
  `Id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `MemberId` bigint(20) NOT NULL,
  `SessionId` binary(16) DEFAULT NULL,
  `ServiceId` bigint(20) unsigned NOT NULL,
  `ResourceId` bigint(20) unsigned NOT NULL,
  `AppInstanceGuid` binary(16) NOT NULL,
  `BookingTime` datetime NOT NULL,
  `Status` char(10) NOT NULL,
  `CreatedTime` datetime NOT NULL,
  `ModifiedTime` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

