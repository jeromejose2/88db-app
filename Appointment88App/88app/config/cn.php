<?php
define('APP88_ID','88App_Appointment_'.strtoupper(COUNTRY).'_'.strtoupper(ENV).'_v'.VERSION);
define('APP88_VERSION','v'.VERSION);

define('APP88_FRONT_LANGUAGE', 'sc');
define('APP88_ADMIN_LANGUAGE', 'sc');

define('APP88_TIMEZONE_OFFSET','+8');

$config['app88_dummy'] = '';
$config['app88_support_feed'] = 1;

