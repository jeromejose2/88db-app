<?php
define('APP88_DB_USER', '88appsweb');
define('APP88_DB_PWD', 'DCDvvTgITuEw');
define('APP88_DB_NAME', '88app_appointment_ph');

/*
|--------------------------------------------------------------------------
| Error Logging Threshold
|--------------------------------------------------------------------------
|
| If you have enabled error logging, you can set an error threshold to
| determine what gets logged. Threshold options are:
| You can enable error logging by setting a threshold over zero. The
| threshold determines what gets logged. Threshold options are:
|
|	0 = Disables logging, Error logging TURNED OFF
|	1 = Error Messages (including PHP errors)
|	2 = Debug Messages
|	3 = Informational Messages
|	4 = All Messages
|
| For a live site you'll usually only enable Errors (1) to be logged otherwise
| your log files will fill up very fast.
|
*/
define('LOG_THRESHOLD', 1); // Suggested, DEV: 2, UAT and PROD: 1

define('AUTH_APP_ID', 'ACA356B889FD42C882697EB02B1467E4'); // Generate new one for a new app
define('AUTH_APP_SECRET', '123456');
define('AUTH_JQUERY_PATH', 'http://sg.uat.shop.88static.com/js/jquery.min.js');
define('AUTH_JSON_PATH', 'http://sg.uat.shop.88static.com/js/json2.min.js');
define('AUTH_ACCESS_TOKEN_PATH', 'http://secure01.uat.88db.com/addon/accesstoken');
define('AUTH_USER_INFO_PATH', 'http://secure01.uat.88db.com/addon/user');
define('APP88_EMAIL_GATEWAY_URL','http://secure01.uat.88db.com/addon/email');
define('APP88_EMAIL_SHOP_OWNER_GATEWAY_URL','http://secure01.uat.88db.com/addon/emailShopOwner');
define('APP88_EMAIL_88DB_MEMBER_GATEWAY_URL','http://secure01.uat.88db.com/addon/email88DbMember');
define('APP88_REGISTER_88DB_MEMBER_URL','http://secure01.uat.88db.com/addon/register88DBMember');

define('APP88_FRONT_LANGUAGE', 'en');
define('APP88_ADMIN_LANGUAGE', 'en');
define('APP88_CURRENCY_DECIMALS', '2');
define('APP88_CURRENCY_DECIMAL_POINTS', '.');
define('APP88_CURRENCY_THOUSANDS_SEPARATOR', ',');
define('APP88_MONEY_FORMAT', 'PHP %s');
define('APP88_CURRENCY', 'PHP');

define('APP88_DURATION_FORMAT', '%dh %dm ');
define('APP88_DATE_FORMAT_JS', 'yyyy-MM-dd');
define('APP88_MONTH_FORMAT_JS', 'yyyy-MM');
define('APP88_DAY_FORMAT_JS', 'MM-dd');
define('APP88_DATE_FORMAT_PHP', 'Y-m-d');
define('APP88_TIME_FORMAT_PHP', 'H:i');
define('APP88_RESERVEDAUTOEXPIREDDURATION', '30');
define('APP88_PAGESIZE', '12');
define('APP88_RESOURCE_CREATION_LIMIT', '-1');
define('APP88_APPOINTMENT_SETTING', '{"enableNotificationEmail":"T", "enableReminderEmail":"F", "reminderMinute":0, "greeting":"", "customizeTitles":"{}"}');
define('APP88_APPOINTMENT_CUSTOMIZE_TITLE', '{"tc":{"AppointmentTitle":"預約","ServiceTitle":"服務","ResourceTitle":"服務員","ScheduleTitle":"時間"}, "en":{"AppointmentTitle":"Appointment","ServiceTitle":"Service","ResourceTitle":"Resource","ScheduleTitle":"Time"},"th":{"AppointmentTitle":"รนัดหมาย","ServiceTitle":"บริการ","ResourceTitle":"เจ้าหน้าที่","ScheduleTitle":"ตารางเวลา"},"bm":{"AppointmentTitle":"Temujanji","ServiceTitle":"Perkhidmatan","ResourceTitle":"Sumber","ScheduleTitle":"Jadual"},"sc":{"AppointmentTitle":"预约","ServiceTitle":"服务","ResourceTitle":"资源","ScheduleTitle":"时间"},"id":{"AppointmentTitle":"Janji","ServiceTitle":"Layanan","ResourceTitle":"Sumber daya","ScheduleTitle":"Jadwal"}}');
define('APP88_APPOINTMENT_EMAIL_CLIENT_MAPPING', '{"@gmail.com":"http://www.gmail.com", "@yahoo.com":"http://mail.yahoo.com", "@88db.com": "http://www.88db.com"}');
define('APP88_SHOPPASSPORTSERVICE_ENDPOINT', 'http://sso.uat.88db.com/ShopPassportService.asmx');
define('APP88_ACTIVE_APPOINTMENT_API', 'http://uat.appointment.ph.88apps.net/notification/activateAppointment');

define('APP88_88DB_PRIVACY_LINK', 'http://ph.88db.com/Privacy/');
define('APP88_88DB_TERMS_LINK', 'http://ph.88db.com/Terms/');
define('APP88_SINGLE_NAME_FIELD', 'disable');