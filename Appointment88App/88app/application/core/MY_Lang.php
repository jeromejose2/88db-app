<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Lang extends CI_Lang {
	
	function __construct()
	{
		log_message('debug', 'MY_Lang Class Initialized');
		parent::__construct();
	}
	
	function load($langfile = '', $idiom = '', $return = FALSE, $add_suffix = TRUE, $alt_path = '')
	{
		$config =& get_config();
		$deft_lang = ( ! isset($config['language'])) ? 'en' : $config['language'];
	
		$langfile = str_replace('.php', '', $langfile);
		
		if ($add_suffix == TRUE)
		{
			$langfile = str_replace('_lang.', '', $langfile).'_lang';
			$add_suffix = FALSE;
		}
		
		$langfile .= '.php';
		
		// Determine where the language file is
		$found = FALSE;
		if ($alt_path != '' && file_exists($alt_path.'language/'.$idiom.'/'.$langfile))
		{
			$found = TRUE;
		}
		else
		{
			$found = FALSE;

			foreach (get_instance()->load->get_package_paths(TRUE) as $package_path)
			{
				if (file_exists($package_path.'language/'.$idiom.'/'.$langfile))
				{
					$found = TRUE;
					break;
				}
			}
		}
		
		if (($idiom != $deft_lang) && $found)
		{
			if (parent::load($langfile, $deft_lang, $return, $add_suffix, $alt_path))
			{
				$langfile_full = str_replace('.php', '', $langfile);
	
				if ($add_suffix == TRUE)
				{
					$langfile_full = str_replace('_lang.', '', $langfile_full).'_lang';
				}
	
				$langfile_full .= '.php';
				$this->is_loaded = array_diff($this->is_loaded, array($langfile_full));
			}
		}
		else
		{
			$idiom = $deft_lang;
		}
		return parent::load($langfile, $idiom, $return, $add_suffix, $alt_path);
	}
	
	function unload($langfile = '', $add_suffix = TRUE)
	{
		$langfile_full = str_replace('.php', '', $langfile);
		
		if ($add_suffix == TRUE)
		{
			$langfile_full = str_replace('_lang.', '', $langfile_full).'_lang';
		}
		
		$langfile_full .= '.php';
		$this->is_loaded = array_diff($this->is_loaded, array($langfile_full));
		
		return;
	}
	
	function reload($langfile = '', $idiom = '', $return = FALSE, $add_suffix = TRUE, $alt_path = '')
	{
		$this->unload($langfile, $add_suffix);

		return $this->load($langfile, $idiom, $return, $add_suffix, $alt_path);
	}
	
	function appointmentLine($name)
	{	
		$value = $this->line($name);
		$this->CI =& get_instance();
		$this->CI->load->model("setting_model");
		$shopId = $this->CI->app88param->get88AppGuid();
		$lang = $this->CI->app88param->getLang()?$this->CI->app88param->getLang():APP88_ADMIN_LANGUAGE;
		$setting = $this->CI->setting_model->getSetting($shopId);
		$customizeTitle = json_decode($setting["customizeTitles"]);
		$customizeDefaultTitle = json_decode(APP88_APPOINTMENT_CUSTOMIZE_TITLE);
		
		$patterns = array();		
		foreach($customizeDefaultTitle->$lang as $titleName => $titleValue){
			$patterns[] = '/{'.$titleName.'}/i';		
		}
		
		$replacements = array();		
		foreach($customizeTitle as $titleName => $titleValue){
			$replacements[] = $titleValue;		
		}	
		
		return preg_replace($patterns, $replacements, $value);		
	}
}