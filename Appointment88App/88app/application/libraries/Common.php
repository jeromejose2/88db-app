<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common {

	private $CI;
	private $decimals = APP88_CURRENCY_DECIMALS;
	
	public function __construct($config = array())
	{
		$this->CI =& get_instance();
	}
	
	public function setDecimals($d)
	{
		$this->decimals = $d;
	}
	
	public function showError($error)
	{
		if (isset($error))
		{
			echo "<div class='cms_error'>";
			if (is_array($error))
			{
				echo "<ul>";
				for ($i=0;$i<sizeof($error);$i++)
				{
				echo "<li>". htmlspecialchars($error[$i]);
				}
				echo "</ul>";
			}
			else
			{
				echo "<P>".htmlspecialchars($error)."</P>";
			}
			echo "</div>";
		}
	}
	
	public function showInfo($info)
	{
		if (isset($info))
		{
			echo <<< EOD
<script>
$(document).ready(function(){
	var width = $('div.cms_info').width()+'px';
	var height = $('div.cms_info').height()+'px';
	$('div.cms_info').css('z-index','10');
	$('div.cms_info').css('position','absolute');
	$('div.cms_info').css('width',width);
	//$('div.cms_info').css('height',height);
	$('div.cms_info').delay(3000).fadeOut(1000);
});
</script>
EOD;
			echo "<div class='cms_info'>";
			if (is_array($info))
			{
				echo "<ul>";
				for ($i=0;$i<sizeof($info);$i++)
				{
					echo "<li>". htmlspecialchars($info[$i])."</li>";
				}
				echo "</ul>";
			}
			else
			{
				echo "<P>".htmlspecialchars($info)."</P>";
			}
			echo "</div>";
		}
	}
	
	public function number_format($num)
	{ 
		return number_format($num, $this->decimals, APP88_CURRENCY_DECIMAL_POINTS, APP88_CURRENCY_THOUSANDS_SEPARATOR); 
	}
	
	public function money_format($num)
	{ 
		return sprintf(APP88_MONEY_FORMAT ,$this->number_format($num)); 
	} 
	
	public function local_date($db_time, $date_format = 'Y-m-d')
	{
		if ($db_time == '') $db_time = gmdate('Y-m-d H:i:s');
		return date($date_format, strtotime($db_time)+60*60*APP88_TIMEZONE_OFFSET);
	}
	public function local_datetime($db_time, $date_format = 'Y-m-d H:i:s')
	{
		return $this->local_date($db_time,$date_format);
	}
	public function addTime($date, $h, $i=0,$m=0, $d=0, $y=0)
    {
		$cd = strtotime($date);
		$resultDate=date('Y-m-d H:i:s',  mktime(
				date('H',$cd) + $h,
				date('i',$cd) + $i, 
				date('s',$cd), 
				date('m',$cd) + $m,
				date('d',$cd) + $d, 
				date('Y',$cd) + $y
			));		
        return $resultDate;
    }
	public function force_rollback()
	{
		$this->CI->db->simple_query('ROLLBACK');
		$this->CI->db->simple_query('SET AUTOCOMMIT=1');
	}
	
	public function showBenchmark()
	{
		$hostName = gethostname();
			
		if($hostName){
			$pos = strpos($hostName, '.');
		
			if($pos)
			{
				$hostName = substr($hostName, 0, $pos);
			}
		}
		
		return $hostName.' '.APP88_ID.' '.date('m/d/Y h:i:s A').' | exec time:'. $this->CI->benchmark->elapsed_time() .' version:'.APP88_VERSION;
	}
	
	public function genPaging($total, $pageNumber = 1, $pageSize = APP88_PAGESIZE)
	{		
		$totalPage = ceil($total / $pageSize);		
		$url = preg_replace('/&page=[^&]*/','',$_SERVER["REQUEST_URI"]);
		$html = '<div style="display: block;" id="paging" class="paging"> ';
		if($pageNumber > 1)
		{
			$html .= '<a href="'.$url.'&page='.($pageNumber-1).'" class="previous">&lt;&lt;</a>';
		}		
		for ($i=1; $i<=$totalPage; $i++) 
		{
			$cls = ($i == $totalPage ? "last":"");
			if($pageNumber == $i)
			{
				$html .='<strong class="'.$cls.'">'.$i.'</strong>';
			}
			else
			{
				$html .='<a href="'.$url.'&page='.$i.'" class="'.$cls.'">'.$i.'</a>';
			}
			
		}
		if($pageNumber < $totalPage)
		{
			$html .='<a href="'.$url.'&page='.($pageNumber+1).'" class="next">&gt;&gt;</a></div>';
		}
		return $html;
	}	
	
	public function getVersionFilePath($fileName)
	{
		return base_url("s/v".VERSION."/".$fileName);
	}
}