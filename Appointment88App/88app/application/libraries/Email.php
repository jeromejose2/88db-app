<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email {

	private $CI;

	public function __construct($config = array())
	{
		$this->CI =& get_instance();
		$this->CI->load->model("app_model");
	}

	public function sendCustomerMessage($model)
	{
		$email = $model["email"];
		$message = form_prep($model["message"]);
		$customerName = form_prep($model["customerName"]);
		$customerId = $model["customerId"];
		$memberId = $model["memberId"];
		$subject = 'Customer Message';
		$body = 'Hi '.$customerName.'<br/>Sent you a message:'.$message;
		if($this->sendEmail88DbMember($memberId, $subject, $body) === TRUE)
		{
			$this->CI->sendemail_model->createEmailRecord(array(
				'to' => $email,
				'subject' => $subject,
				'body' => $body,
				'refId' => $customerId,
				'emailType' => 'Customer Message'
			));
		}
	}
	
	public function sendCancelAppointmentEmail($data)
	{
		$from = $data["From"];
		
		$id = $data["Id"];
		$email = $data["Email"];
		$memberId = $data["MemberId"];
		$setting = $this->CI->setting_model->getSetting();
		
		$sendSuccess = TRUE;
		
		/** to customer */
		$this->CI->lang->reload('template', APP88_FRONT_LANGUAGE);
		
		$subject = '';
		$template = $this->CI->lang->appointmentLine('cancel_appointment_to_customer_email_template');
		
		if($from == 'owner')
		{
			$subject = sprintf($this->CI->lang->appointmentLine('cancel_appointment_to_customer_by_owner_email_subject'), $data["Service"], $data["ShopName"]);
			$template = str_replace('{Reason}', $this->CI->lang->appointmentLine('cancel_appointment_reason'), $template);
		}
		else
		{
			$subject = sprintf($this->CI->lang->appointmentLine('cancel_appointment_to_customer_by_customer_email_subject'), $data["Service"], $data["ShopName"]);
			$template = str_replace('{Reason}', '', $template);
		}
		
		$email_message = $this->_genEmailMessage($template, $data);
		
		if($memberId != '0')
		{
			if($this->sendEmail88DbMember($memberId, $subject, $email_message) === TRUE)
			{
				$this->CI->sendemail_model->createEmailRecord(array(
					'to' => $email,
					'subject' => $subject,
					'body' => $email_message,
					'refId' => $id,
					'emailType' => 'Cancel Appointment'
				));
			}
		}
		else 
		{
			$sendSuccess = FALSE;
		}
		
		/** to owner */
		if($setting['enableNotificationEmail'] == 'T')
		{
			$this->CI->lang->reload('template', APP88_ADMIN_LANGUAGE);
			
			$subject = '';
			$template = $this->CI->lang->appointmentLine('cancel_appointment_to_owner_email_template');
				
			if($from == 'owner')
			{
				$subject = sprintf($this->CI->lang->appointmentLine('cancel_appointment_to_owner_by_owner_email_subject'), $data["Service"]);
			}
			else
			{
				$subject = sprintf($this->CI->lang->appointmentLine('cancel_appointment_to_owner_by_customer_email_subject'), $data["Service"]);
			}
			
			$email_message = $this->_genEmailMessage($template, $data);
			
			if($this->sendEmailShopOwner($subject, $email_message) === TRUE)
			{
				$this->CI->sendemail_model->createEmailRecord(array(
						'to' => 'Shop Owner',
						'subject' => $subject,
						'body' => $email_message,
						'refId' => $id,
						'emailType' => 'Cancel Appointment'
				));
			}
			else
			{
				$sendSuccess = FALSE;
			}
		}
		
		return $sendSuccess;
	}
	
	public function sendUpdateNotificationEmail($data)
	{
		$id = $data["Id"];
		$email = $data["Email"];
		$memberId = $data["MemberId"];
		$appGuid = isset($data["AppGuid"]) ? $data["AppGuid"] : '';
	
		$sendSuccess = TRUE;
	
		/** to customer */
		$this->CI->lang->reload('template', APP88_FRONT_LANGUAGE);
	
		$subject = sprintf($this->CI->lang->appointmentLine('update_appointment_to_customer_email_subject'), $data["Service"], $data["ShopName"]);
		$template = $this->CI->lang->appointmentLine('update_appointment_to_customer_email_template');
	
		$email_message = $this->_genEmailMessage($template, $data);
	
		if($this->sendEmail88DbMember($memberId, $subject, $email_message, $appGuid) === TRUE)
		{
			$this->CI->sendemail_model->createEmailRecord(array(
					'to' => $email,
					'subject' => $subject,
					'body' => $email_message,
					'refId' => $id,
					'emailType' => 'Notification'
			), $appGuid);
		}
		else
		{
			$sendSuccess = FALSE;
		}
	
		if(!isset($data["OnlyToCustomer"]) || $data["OnlyToCustomer"] !== TRUE)
		{
			/** to owner */
			$this->CI->lang->reload('template', APP88_ADMIN_LANGUAGE);
				
			$subject = sprintf($this->CI->lang->appointmentLine('update_appointment_to_owner_email_subject'), $data["Service"]);
			$template = $this->CI->lang->appointmentLine('update_appointment_to_owner_email_template');
				
			$email_message = $this->_genEmailMessage($template, $data);
				
			if($this->sendEmailShopOwner($subject, $email_message, $appGuid) === TRUE)
			{
				$this->CI->sendemail_model->createEmailRecord(array(
						'to' => 'Shop Owner',
						'subject' => $subject,
						'body' => $email_message,
						'refId' => $id,
						'emailType' => 'Notification'
				), $appGuid);
			}
			else
			{
				$sendSuccess = FALSE;
			}
		}
	
		return $sendSuccess;
	}
	
	public function sendNotificationEmail($data)
	{
		$id = $data["Id"];
		$email = $data["Email"];
		$memberId = $data["MemberId"];
		$appGuid = isset($data["AppGuid"]) ? $data["AppGuid"] : '';
		
		$sendSuccess = TRUE;
		
		/** to customer */
		$this->CI->lang->reload('template', APP88_FRONT_LANGUAGE);
		
		$subject = sprintf($this->CI->lang->appointmentLine('new_appointment_to_customer_email_subject'), $data["Service"], $data["ShopName"]);
		$template = $this->CI->lang->appointmentLine('new_appointment_to_customer_email_template');
		
		$email_message = $this->_genEmailMessage($template, $data);
		
		if($this->sendEmail88DbMember($memberId, $subject, $email_message, $appGuid) === TRUE)
		{
			$this->CI->sendemail_model->createEmailRecord(array(
					'to' => $email,
					'subject' => $subject,
					'body' => $email_message,
					'refId' => $id,
					'emailType' => 'Notification'
			), $appGuid);
		}
		else
		{
			$sendSuccess = FALSE;
		}
		
		if(!isset($data["OnlyToCustomer"]) || $data["OnlyToCustomer"] !== TRUE)
		{
			/** to owner */
			$this->CI->lang->reload('template', APP88_ADMIN_LANGUAGE);
			
			$subject = sprintf($this->CI->lang->appointmentLine('new_appointment_to_owner_email_subject'), $data["Service"]);
			$template = $this->CI->lang->appointmentLine('new_appointment_to_owner_email_template');
			
			$email_message = $this->_genEmailMessage($template, $data);
			
			if($this->sendEmailShopOwner($subject, $email_message, $appGuid) === TRUE)
			{
				$this->CI->sendemail_model->createEmailRecord(array(
					'to' => 'Shop Owner',
					'subject' => $subject,
					'body' => $email_message,
					'refId' => $id,
					'emailType' => 'Notification'
				), $appGuid);
			}
			else
			{
				$sendSuccess = FALSE;
			}
		}
		
		return $sendSuccess;
	}
	
	public function sendReminderEmail($data)
	{
		$id = $data["Id"];
		$appGuid = $data["AppGuid"];
		
		$sendSuccess = FALSE;	
		
		$email = '';
		$subject = '';
		$email_message = '';
		
		if($data["EmailType"] === 'CustomerReminder')
		{
			$email = $data["Email"];
			
			$memberId = $data["MemberId"];
			
			$this->CI->lang->reload('template', APP88_FRONT_LANGUAGE);
			
			$subject = sprintf($this->CI->lang->appointmentLine('reminder_to_customer_email_subject'), $data["Service"], $data["ShopName"]);
			$template = $this->CI->lang->appointmentLine('reminder_to_customer_email_template');
			
			$email_message = $this->_genEmailMessage($template, $data);
			
			if($memberId != '0')
			{
				$sendSuccess = $this->sendEmail88DbMember($memberId, $subject, $email_message, $appGuid);
			}
		}
		else if($data["EmailType"] === 'OwnerReminder')
		{
			$email = 'Shop Owner';
			
			$this->CI->lang->reload('template', APP88_ADMIN_LANGUAGE);
			
			$subject = sprintf($this->CI->lang->appointmentLine('reminder_to_owner_email_subject'), $data["Service"]);
			$template = $this->CI->lang->appointmentLine('reminder_to_owner_email_template');
			
			$email_message = $this->_genEmailMessage($template, $data);
			
			$sendSuccess = $this->sendEmailShopOwner($subject, $email_message, $appGuid);
		}
		
		if($sendSuccess)
		{
			$this->CI->sendemail_model->createEmailRecord(array(
				'to' => $email,
				'subject' => $subject,
				'body' => $email_message,
				'refId' => $id,
				'emailType' => $data["EmailType"]
			), $appGuid);
		}
		
		return $sendSuccess;
	}
	
	private function _genEmailMessage($template, $data)
	{
		$search = array(
			'{SeperateLine}',
			'{ShopName}',
			'{FirstName}',
			'{LastName}',
			'{Service}',
			'{Resource}',
			'{Schedule}',
			'{Price}',
			'{Note}',
			'{Reason}',
			'{Email}',
			'{Phone}',
		);
		
		$replace = array();
		foreach($search as $s)
		{
			$key = str_replace(array('{','}'), array('',''), $s);
			$element = isset($data[$key]) ? $data[$key] : '';
			
			if($s === '{SeperateLine}')
			{
				$element = '<tr><td colspan="2" style="padding:0"><img src="'.preg_replace('/\?.*/', '', site_url('images/seperateLine.jpg')).'" border="0"></td>';
			}
			elseif($s === '{Schedule}')
			{
				$element = date(APP88_DATE_FORMAT_PHP.' (D), h:iA', strtotime($element));
			}
			elseif($s === '{Price}')
			{
				$element = $this->CI->common->money_format($element);
			}
			
			$replace[] = $element;
		}
		
		return str_replace($search, $replace, $template);
	}
	
	public function sendEmailShopOwner($subject, $body, $appGuid = '')
	{
		if($appGuid === '')
		{
			$shopguid = $this->CI->app88param->get88ShopGuid();
		}
		else
		{
			$app = $this->CI->app_model->getAppByAppGuid($appGuid);
			$shopguid = $app['ShopGuid'];
		}
		$param = array(
			"appid" => AUTH_APP_ID, 
			"appsecret" => AUTH_APP_SECRET, 
			"shopguid" => $shopguid,
			"subject" => $subject, 
			"body" => $body
		);
		$result = json_decode($this->CI->app88email->sendEmailShopOwner($param));
		if($result->error->code !== 0)
		{
			$this->CI->app88log->log_message('email','error', json_encode($result->error));
			$this->CI->app88log->log_message('email','error', json_encode($param));
		}
		return $result->error->code == 0;		
	}
	
	public function sendEmail88DbMember($memberId, $subject, $body, $appGuid = '')
	{
		if($memberId == "0")
		{
			return FALSE;
		}
		if($appGuid === '')
		{
			$shopguid = $this->CI->app88param->get88ShopGuid();
		}
		else
		{
			$app = $this->CI->app_model->getAppByAppGuid($appGuid);
			$shopguid = $app['ShopGuid'];
		}
		$param = array(
			"appid" => AUTH_APP_ID, 
			"appsecret" => AUTH_APP_SECRET, 
			"uidstr" => $memberId,
			"shopguid" => $shopguid,
			"subject" => $subject, 
			"body" => $body,
			"ishtml" => "true"
		);		
		$result = json_decode($this->CI->app88email->sendEmail88DbMember($param));
		if($result->error->code !== 0)
		{
			$this->CI->app88log->log_message('email','error', json_encode($result));
			$this->CI->app88log->log_message('email','error', json_encode($param));
		}
		return $result->error->code == 0;
	}
}