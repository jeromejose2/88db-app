<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* This file is used to store the 88app specific settings which are identical to all countries and regions */

$config['config_variable_in_app88.php'] = "Hello, nice to meet you";
$config['name'] = 'value';
$config['admin_supported_lang'] = array('bm','en','id','sc','tc','th','ja');

$config['admin_setting_tinycme_lang'] = array(
		'en'=>'en',
		'sc'=>'cn',
		'bm'=>'ms',
		'id'=>'id',
		'tc'=>'zh-tw',
		'th'=>'en',
		'ja'=>'en'
);