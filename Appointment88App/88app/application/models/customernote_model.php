<?php
class CustomerNote_model extends CI_Model {
	private $shopId;
	function __construct()
	{
		parent::__construct();
		$this->shopId = $this->app88param->get88AppGuid();
	}
	
	function createCustomerNote($note)
	{
		$sql = "INSERT INTO customernote (Guid, CustomerId, AppInstanceGuid, Message, Status, CreatedTime) VALUES (UNHEX('?'), ?, UNHEX(?), ?, 'A', ?)";
		$guid = $this->guid->newGuid();
		$createdTime = gmdate('Y-m-d H:i:s', time());
		$param = array(
			$guid,
			$note["customerId"],
			$this->shopId,
			$note["message"],
			$createdTime
		);
		$this->db->trans_start();
		$query = $this->db->query($sql, $param);	
		$this->db->trans_complete();		
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
		else
		{
			return array(
				'guid'=>$guid,
				'createdTime'=>$createdTime
			);
		}
	}
	
	function getCustomerNotes($customerId, $pageNumber = 1, $pageSize = 12)
	{
		$sql = "SELECT Guid, Message,CreatedTime FROM customernote WHERE Status = 'A' AND AppInstanceGuid = UNHEX(?) AND CustomerId =? ORDER BY CreatedTime DESC LIMIT ".(($pageNumber-1)*$pageSize).",".$pageSize;
		$sql_count = "SELECT COUNT(*) AS Count FROM customernote WHERE Status = 'A' AND AppInstanceGuid = UNHEX(?) AND CustomerId =?";
		$param = array(
			$this->shopId,
			$customerId
		);
		$query = $this->db->query($sql, $param);		
		$query_count = $this->db->query($sql_count, $param);	
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
		else
		{
			$count = $query_count->result();	
			$notes = $query->result();
			return array(
				'rows'=>$notes,
				'total'=>$count[0]->Count
			);
		}
	}
	
	function deleteCustomerNote($noteGuid)
	{
		$sql = "UPDATE customernote SET Status = 'D', ModifiedTime =? WHERE Guid = UNHEX(?) AND Status <> 'D' AND AppInstanceGuid = UNHEX(?)";
		$param = array(
			gmdate('Y-m-d H:i:s', time()),
			$noteGuid,
			$this->shopId
		);	
		$query = $this->db->query($sql, $param);		
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
}