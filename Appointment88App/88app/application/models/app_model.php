<?php
class App_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	
	public function getAppByAppGuid($cid)
	{
		$sql = 'select * from app where AppInstanceGuid = UNHEX(?)';
		$param = array(
			$cid
		);
	
		$query = $this->db->query($sql, $param);
	
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else if ($query->num_rows() > 0)
		{
			$row = $query->row_array();
			$row['AppInstanceGuid'] = $this->guid->createFromBinary($row['AppInstanceGuid'])->toString();
			$row['ShopGuid'] = $this->guid->createFromBinary($row['ShopGuid'])->toString();
			return $row;
		}
		else
		{
			$this->app88log->log_message('db','debug', $this->db->_error_message());
			$this->app88log->log_message('db','debug', $this->db->last_query());
			return FALSE;
		}
	}
	
	public function getAppById($id)
	{
		$sql = 'select * from app where Id = ?';
		$param = array(
				$id
		);
	
		$query = $this->db->query($sql, $param);
	
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else if ($query->num_rows() > 0)
		{
			$row = $query->row_array();
			$row['AppInstanceGuid'] = $this->guid->createFromBinary($row['AppInstanceGuid'])->toString();
			return $row;
		}
		else
		{
			$this->app88log->log_message('db','debug', $this->db->_error_message());
			$this->app88log->log_message('db','debug', $this->db->last_query());
			return FALSE;
		}
	}
	
	public function createApp($sid, $cid, $owner_id, $data)
	{
		$sql = 'insert into app (AppInstanceGuid, ShopGuid, OwnerId, ShopName, CreateTime, ModifiedTime) values (UNHEX(?),UNHEX(?),?,?,?,?)';
		$param = array(
				$cid,
				$sid,
				$owner_id,
				$data['shop_name'],
				gmdate('Y-m-d H:i:s', time()),
				gmdate('Y-m-d H:i:s', time())
		);
		
		$this->db->trans_start();
		
		$query = $this->db->query($sql, $param);
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() !== FALSE)
		{
			return $this->getAppByAppGuid($cid);
		}
		else
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			$this->app88common->force_rollback();
			show_error($this->lang->line('err_database_err'));
		}
	}
	
	public function updateAppShopName($cid, $shop_name)
	{
		$sql = 'UPDATE app SET ShopName = ? WHERE AppInstanceGuid = UNHEX(?)';
		
		$this->db->trans_start();
		
		$query = $this->db->query($sql, array($shop_name, $cid));
		
		$this->db->trans_complete();
		
		if ($this->db->trans_status() !== FALSE)
		{
			return TRUE;
		}
		else
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			$this->app88common->force_rollback();
			show_error($this->lang->line('err_database_err'));
		}
	}
}