<?php
class DeleteBookingLog_model extends CI_Model {
	private $shopId;
	function __construct()
	{
		parent::__construct();
		$this->shopId = $this->app88param->get88AppGuid();
	}
	
	public function createLog($model)
	{
		$sql = "INSERT INTO deletebookinglog (BookingId, Reason, CreatedTime) VALUES (?, ?, ?)";
		$param = array(
			$model["bookingId"],
			$model["reason"],
			gmdate('Y-m-d H:i:s', time())			
		);
		$this->db->trans_start();
		$query = $this->db->query($sql, $param);	
		$customerId = $this->db->insert_id();
		$this->db->trans_complete();
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
}