<?php
class Service_model extends CI_Model {
	private $shopId;
	function __construct()
	{
		parent::__construct();
		$this->shopId = $this->app88param->get88AppGuid();
		$this->load->model("resource_model");
	}
	
	public function getServicesNameList($ignoreStatus = FALSE)
	{
		$sql = "SELECT Guid as ServiceGuid, Name FROM service WHERE Status IN (".($ignoreStatus === TRUE ?"'I', 'A'":"'A'").") AND IsSnapShot = 'F' AND AppInstanceGuid = UNHEX(?) ORDER BY Weight ASC";
		$param = array($this->shopId);		
		$query = $this->db->query($sql, $param);
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else
		{
			return $query->result();	
		}	
	}
	
	public function getAvailableServicesNameList()
	{
		//staff must > 0
		$sql = "SELECT s.Guid as ServiceGuid, s.Status, s.Name, IFNULL(rs.Staff,'0') AS Staff  FROM service s LEFT JOIN ".
			"(SELECT ServiceId, COUNT(*) AS Staff FROM `resource_provided_service` WHERE Status = 'A' group by ServiceId) rs ".
			" ON s.Id = rs.ServiceId ".
			" WHERE s.Status IN ('A', 'I') AND s.IsSnapShot = 'F' AND s.AppInstanceGuid = UNHEX(?) AND Staff > 0  ORDER BY s.Weight ASC";
		$param = array($this->shopId);		
		$query = $this->db->query($sql, $param);
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else if ($query->num_rows() > 0)
		{
			$result = array();
			//check staff have schema
			foreach($query->result() as $r)
			{				
				if($this->resource_model->getAvailableResourceNameListByServiceGuid( $this->guid->createFromBinary($r->ServiceGuid)->toString() ) !== FALSE)
				{
					array_push($result, $r);
				}
			}		
			if(count($result) === 0)
			{
				return FALSE;
			}
			return $result;
		}
		else
		{
			$this->app88log->log_message('db','debug', $this->db->_error_message());
			$this->app88log->log_message('db','debug', $this->db->last_query());
			return FALSE;
		}		
	}
	
	public function getServiceDetailByGuid($serviceGuid, $ignoreStatus = FALSE, $serviceId = 0)
	{
		
		$sql = "SELECT Name, AssignColor, Duration, Description, Cost, AvailablePerTimeSlot, TimeSlotDivision, MinTime, AdvanceTime FROM service WHERE Status IN (".($ignoreStatus === TRUE ?"'I', 'A'":"'A'").") ".($serviceId == 0 ? "AND IsSnapShot = 'F' ":"")."AND ".($serviceId == 0 ? "Guid = UNHEX(?) ":"Id = ?");
		if($serviceId == 0)
		{
			$param = array(
				$serviceGuid
			);
		}
		else
		{
			$param = array(
				$serviceId
			);
		}
		$query = $this->db->query($sql, $param);		
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else if ($query->num_rows() > 0)
		{
			$result = $query->result();
			$service = $result[0];
			$service->FormatCost = $this->common->number_format($service->Cost);
			return $service;	
		}
		else
		{			
			$this->app88log->log_message('db','debug', $this->db->_error_message());
			$this->app88log->log_message('db','debug', $this->db->last_query());			
			return FALSE;
		}		
	}
	
	public function getServiceIdByGuid($serviceGuid, $ignoreStatus = FALSE)
	{
		$sql = "SELECT Id FROM service WHERE Status IN (".($ignoreStatus ? "'A','I'": "'A'").") AND Guid = UNHEX(?) AND IsSnapShot = 'F'";
		$param = array(
			$serviceGuid
		);		
		$query = $this->db->query($sql, $param);
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else if ($query->num_rows() > 0)
		{
			$result = $query->result();
			return $result[0]->Id;
		}
		else
		{
			$this->app88log->log_message('db','debug', $this->db->_error_message());
			$this->app88log->log_message('db','debug', $this->db->last_query());
			return FALSE;
		}		
	}
	
	public function createSnapShot($serviceGuid)
	{
		$sql = "INSERT INTO service (AssignColor, Guid, AppInstanceGuid, Name, Duration, Cost, Description, TimeSlotDivision, AvailablePerTimeSlot, MinTime, AdvanceTime, Status, Version, IsSnapShot, CreatedTime, Weight) SELECT AssignColor, Guid, AppInstanceGuid, Name, Duration, Cost, Description, TimeSlotDivision, AvailablePerTimeSlot, MinTime, AdvanceTime, Status, Version, 'T', ?, Weight FROM service WHERE Status IN ('I', 'A') AND IsSnapShot = 'F' AND Guid = UNHEX(?);";	
		$param = array(
			gmdate('Y-m-d H:i:s', time()),
			$serviceGuid
		);
		$query = $this->db->query($sql, $param);
		$serviceId = $this->db->insert_id();
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else
		{
			return $serviceId;
		}	
	}
	
	public function getServiceSnapShot($serviceGuid, $ignoreStatus = FALSE)
	{
		$result;
		$sql = "SELECT Id, Version, Name, Duration, Description, Cost, TimeSlotDivision, MinTime, AdvanceTime FROM service WHERE Status IN (".($ignoreStatus ? "'A','I'": "'A'").") AND Guid = UNHEX(?) AND IsSnapShot = 'T' AND Version = (SELECT Version FROM service WHERE Status  IN (".($ignoreStatus ? "'A','I'": "'A'").") AND IsSnapShot = 'F' AND Guid = UNHEX(?) );";
		
		$param = array(
			$serviceGuid,
			$serviceGuid
		);					
		
		
		$query = $this->db->query($sql, $param);
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else if ($query->num_rows() > 0)
		{
			return $query->result();	
		}
		else
		{
			$this->app88log->log_message('db','debug', $this->db->_error_message());
			$this->app88log->log_message('db','debug', $this->db->last_query());
			return FALSE;
		}	
	}	
	
	public function createService($service)
	{
		$sql = "INSERT INTO service (Guid, AppInstanceGuid, Name, AssignColor, Duration, Cost, Description, TimeSlotDivision, AvailablePerTimeSlot, MinTime, AdvanceTime, Status, Version, IsSnapShot, CreatedTime) VALUES ".
		"(UNHEX('?'), UNHEX(?), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
		$param = array(
			$this->guid->newGuid(),
			$this->shopId,
			$service["name"],
			$service["assignColor"],
			$service["duration"],
			$service["cost"],
			$service["description"],
			$service["timeSlotDivision"],
			$service["availablePerTimeSlot"],
			$service["minTime"],
			$service["advanceTime"],
			'A',
			'1',
			'F',
			gmdate('Y-m-d H:i:s', time())
		);
		$update_sql = "UPDATE service SET Weight = ? WHERE Id = ?;";
		$this->db->trans_start();
		$query = $this->db->query($sql, $param);
		$serviceId = $this->db->insert_id();
		$query = $this->db->query($update_sql, array($serviceId, $serviceId));
		$this->db->trans_complete();
		if ($this->db->trans_status() !== FALSE) 
		{            
			return TRUE;
		} 
		else 
		{
		    log_message('error', $this->db->_error_message());
			log_message('error', $this->db->last_query());	    			
			$this->common->force_rollback();
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
	}
	
	public function updateService($service)
	{
		
		$sql = "UPDATE service SET Name = ?, AssignColor = ?, Duration = ?, Cost = ?, Description = ?, TimeSlotDivision = ?, AvailablePerTimeSlot = ?, MinTime = ?, AdvanceTime = ?, Version = Version + 1, ModifiedTime = ? ".
			"WHERE Guid = UNHEX(?) AND AppInstanceGuid = UNHEX(?) AND IsSnapShot = 'F' AND Status IN ('A', 'I')";
		$param = array(			
			$service["name"],
			$service["assignColor"],
			$service["duration"],
			$service["cost"],
			$service["description"],
			$service["timeSlotDivision"],
			$service["availablePerTimeSlot"],
			$service["minTime"],
			$service["advanceTime"],
			gmdate('Y-m-d H:i:s', time()),
			$service["guid"],			
			$this->shopId
		);
		
		$sql_assignColor = "UPDATE service SET AssignColor = ? WHERE Guid = UNHEX(?) AND AppInstanceGuid = UNHEX(?) AND IsSnapShot = 'T' AND Status IN ('A', 'I')";
		$param_assignColor = array(			
			$service["assignColor"],
			$service["guid"],			
			$this->shopId
		);		
		$query = $this->db->query($sql_assignColor, $param_assignColor);		
		$query = $this->db->query($sql, $param);				
		if (!$query) 
		{     
			
			log_message('error', $this->db->_error_message());
			log_message('error', $this->db->last_query());	    			
			$this->common->force_rollback();
			show_error($this->db->last_query());
			return FALSE;			
		} 
		else 
		{		
		    if($this->db->affected_rows() === 0)
			{			
				return FALSE;
			}
			return TRUE;
		}
	}
	
	
	public function getServicesList($shopId, $condition = array(), $pageSize = 12, $pageNumber = 1, $sortName = "Weight", $sortOrder = "asc")
	{
		$result = array(
			'rows' => array(), 
			'total' => 0,
			'activeCount' => 0
		);
		
		$sql = "SELECT s.*, IFNULL(rs.Staff,'0') AS Staff FROM service s LEFT JOIN ".
			" (SELECT ServiceId, COUNT(*) AS Staff FROM `resource_provided_service` WHERE Status = 'A' group by ServiceId) rs".
			" ON s.Id = rs.ServiceId ".
			" WHERE s.Status IN ('A','I') AND s.IsSnapShot = 'F' AND s.AppInstanceGuid = UNHEX(?) ".
			 (ISSET($condition["q"])?"AND s.Name LIKE '%".$this->db->escape_like_str($condition["q"])."%'" : "").
			" ORDER BY ".$sortName." ".$sortOrder.", Id asc LIMIT ".($pageNumber - 1)*$pageSize.", ".$pageSize;
		$param = array($this->shopId);		
		$query = $this->db->query($sql, $param);	
		
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}

		$result["rows"] = $query->result();
		$result["total"] = $this->getServicesCount($shopId, $condition);
		$result["activeCount"] = $this->getServicesCount($shopId, $condition, "'A'");
		
		return $result;
	}
	
	public function getServicesCount($shopId, $condition = array(), $Status = "'A','I'")
	{
		$sql = "SELECT COUNT(*) AS Count FROM service WHERE Status IN (".$Status.") AND IsSnapShot = 'F' AND AppInstanceGuid = UNHEX(?)".
		(ISSET($condition["q"])?"AND Name LIKE '%".$this->db->escape_like_str($condition["q"])."%'" : "");
		$param = array($this->shopId);		
		$query = $this->db->query($sql, $param);
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else if ($query->num_rows() > 0)
		{
			$result = $query->result();
			return $result[0]->Count;
		}
		else
		{
			$this->app88log->log_message('db','debug', $this->db->_error_message());
			$this->app88log->log_message('db','debug', $this->db->last_query());
			return FALSE;
		}
	}
	
	public function updateServiceWeight($serviceGuid, $weight)
	{
		$sql = "UPDATE service SET Weight = ?, ModifiedTime = ? WHERE Guid = UNHEX(?) AND IsSnapShot = 'F' AND AppInstanceGuid = UNHEX(?)";
		$param = array(			
			$weight,
			gmdate('Y-m-d H:i:s', time()),
			$serviceGuid,
			$this->shopId
		);		
		$this->db->trans_start();		
		$query = $this->db->query($sql, $param);
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			return FALSE;
		}
		return TRUE;
	}
	
	public function updateServiceStatus($serviceGuid, $status)
	{
		$sql = "UPDATE service SET Status = ?, ModifiedTime = ? WHERE Guid = UNHEX(?) AND Status <> ? AND IsSnapShot = 'F' AND AppInstanceGuid = UNHEX(?)";
		$param = array(			
			$status,
			gmdate('Y-m-d H:i:s', time()),
			$serviceGuid,
			$status,
			$this->shopId
		);		
		
		$this->db->trans_start();		
		$query = $this->db->query($sql, $param);
		if($status === 'D')
		{
			$sql = "UPDATE resource_provided_service SET Status = 'D', ModifiedTime = ? WHERE ServiceId = (SELECT Id FROM service WHERE Guid = UNHEX(?) AND IsSnapShot = 'F' AND AppInstanceGuid = UNHEX(?))";
			$param = array(			
				gmdate('Y-m-d H:i:s', time()),
				$serviceGuid,
				$this->shopId
			);
			$query = $this->db->query($sql, $param);
		}
		$this->db->trans_complete();
		
		if ($this->db->trans_status() === FALSE)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		return TRUE;
	}
	
	public function getNeighbourService($curServiceGuid, $direction = 'next')
	{
		//$direction = next or prev
		$sql = "SELECT Guid, Weight FROM service WHERE Weight ".($direction == 'next' ? '>':'<')." (SELECT Weight FROM service WHERE Guid = UNHEX(?) AND IsSnapShot = 'F' AND AppInstanceGuid = UNHEX(?) )  AND AppInstanceGuid = UNHEX(?) AND IsSnapShot = 'F' ORDER BY Weight ".($direction == 'next' ? 'ASC':'DESC')." LIMIT 0,1";
		$param = array(
			$curServiceGuid,
			$this->shopId,
			$this->shopId
		);		
		$query = $this->db->query($sql, $param);		
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else if ($query->num_rows() > 0)
		{
			$result = $query->result();			
			return $result[0];
		}
		else
		{
			return FALSE;
		}	
	}
}