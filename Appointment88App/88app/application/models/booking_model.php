<?php
class Booking_model extends CI_Model {
	private $shopId;
	function __construct()
	{
		parent::__construct();
		$this->shopId = $this->app88param->get88AppGuid();
	}
	
	public function createBooking($model)
	{
		$sql = "INSERT INTO booking (Guid, CustomerId, ServiceId, ResourceId, StartDateTime, EndDateTime, IsReminder, CreatedTime, TimeSlotStatusId, AppInstanceGuid, Status, Note, Source) VALUES ".
			"(UNHEX('?'), ?, ?, ?, ?, ?, ?, ?, ?, UNHEX(?), ?, ?, ?)";
		$param = array(
			$this->guid->newGuid(),
			$model["customerId"],
			$model["serviceId"],
			$model["resourceId"],
			date("Y-m-d H:i:s", strtotime($model["startDateTime"])),
			date("Y-m-d H:i:s", strtotime($model["endDateTime"])),
			$model["isReminder"],
			gmdate('Y-m-d H:i:s', time()),
			$model["timeSlotStatusId"],
			$this->shopId,
			$model["status"],
			$model["note"],
			ISSET($model["source"])?$model["source"]:''
		);
		$this->db->trans_start();
		$query = $this->db->query($sql, $param);		
		$this->db->trans_complete();
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	public function getBookingList($memberId = 0, $pageSize = 0, $type = "all", $pageNumber = 1, $serivceGuid = '', $customerGuid = '', $startDateRang = '', $endDateRang = '', $sort = 'DESC', $customerId = 0)
	{
		//$type: all, upcoming, previous
		$sql = "SELECT s.Guid as ServiceGuid, r.Guid as ResourceGuid, b.Guid AS BookingGuid, b.Note, c.Guid AS CustomerGuid, b.StartDateTime, b.EndDateTime, b.Source, s.Name AS ServiceName, r.AssignColor, r.Name as ResourceName, s.Cost, c.FirstName, c.LastName, c.Email, c.Contact FROM booking b ";
		$sql_count = "SELECT COUNT(*) AS Count FROM booking b ";
		$sql_condition = " LEFT JOIN customer c ON c.Id = b.CustomerId ".
		" LEFT JOIN service s ON s.Id = b.ServiceId ".
		" LEFT JOIN resource r on r.Id = b.ResourceId ".
		" WHERE b.Status = 'A' AND c.AppInstanceGuid = UNHEX(?)";
		$param = array(
			$this->shopId
		);
		if($memberId > 0)
		{
			$sql_condition .= " AND c.MemberId = ?";
			array_push($param, $memberId);
		}
		if($customerId > 0)
		{
			$sql_condition .= " AND c.Id = ?";
			array_push($param, $customerId);
		}
		if($type == "upcoming")
		{
			$sql_condition .= " AND b.StartDateTime > ?";
			array_push($param, $this->common->local_datetime(''));
		}
		else if($type == "previous")
		{
			$sql_condition .= " AND b.StartDateTime <= ?";
			array_push($param, $this->common->local_datetime(''));
		}
		
		if( $serivceGuid !== '')
		{
			$sql_condition .= " AND s.Guid = UNHEX(?)";
			array_push($param, $serivceGuid);
		}
		if($customerGuid !== '')
		{
			$sql_condition .= " AND r.Guid = UNHEX(?)";
			array_push($param, $customerGuid);
		}
		if($startDateRang !=='' && $endDateRang !== '')
		{
			$sql_condition .= " AND b.StartDateTime >= ? AND b.StartDateTime <= ?";
			array_push($param, $startDateRang, $endDateRang);			
		}
		$sql .= $sql_condition;
		$sql_count .= $sql_condition;
		
		$sql .= " ORDER BY b.StartDateTime ".$sort;
		if($pageSize > 0){
			$sql .= " LIMIT ".($pageNumber-1)*$pageSize.", ".$pageSize;
		}
		
		$query = $this->db->query($sql, $param);
		$query_count = $this->db->query($sql_count, $param);	
		
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
		else
		{				
			$count = $query_count->result();	
			$bookings = $query->result();

			return array(
				'rows'=>$bookings,
				'total'=>$count[0]->Count
			);	
		}
	}
	
	public function getNeedReminderBookings($startDate = '', $emailType = 'CustomerReminder')
	{
		$sql = "SELECT b.Id AS BookingId, b.Guid AS BookingGuid, c.Guid AS CustomerGuid, c.Email AS CustomerEmail, c.MemberId, b.StartDateTime, b.EndDateTime, b.Note , s.Name AS ServiceName, r.Name as ResourceName, s.Cost, c.FirstName, c.LastName, c.Contact FROM booking b ";
		$sql_condition = " LEFT JOIN customer c ON c.Id = b.CustomerId ".
		" LEFT JOIN service s ON s.Id = b.ServiceId ".
		" LEFT JOIN resource r on r.Id = b.ResourceId ".
		" WHERE b.Status = 'A' AND IsReminder = 'T' AND b.Id NOT IN (SELECT RefId FROM sendemail WHERE EmailType = ?)";
		$param = array(
			$emailType
		);		
		if($startDate !=='')
		{
			$sql_condition .= " AND b.StartDateTime <= ?";
			array_push($param, $startDate);			
		}
		
		$sql .= $sql_condition;		
		$query = $this->db->query($sql, $param);
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			return FALSE;
		}
		else
		{				
			$bookings = $query->result();
			return $bookings;
		}
	}
	
	function deleteBooking($guid, $customerId, $memberId, $timeSlotStatusId)
	{
		$sql = "UPDATE booking SET Status = 'D', ModifiedTime = ? WHERE Guid = UNHEX(?) AND CustomerId = ? AND Status <> 'D' AND AppInstanceGuid = UNHEX(?)";
		$param = array(
			gmdate('Y-m-d H:i:s', time()),
			$guid, 
			$customerId,
			$this->shopId
		);	
		
		//clear time slot status
		$sql_ts = "UPDATE timeslotstatus SET Status = ?, ModifiedTime = ? WHERE MemberId = ? AND Id = ? AND Status = 'Confirmed'";
		$param_ts = array(
			'Cancel',
			gmdate('Y-m-d H:i:s', time()),
			$memberId,
			$timeSlotStatusId		
		);
		$this->db->trans_start();		
		$query = $this->db->query($sql_ts, $param_ts);	
		$query = $this->db->query($sql, $param);	
		$this->db->trans_complete();
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
		else
		{
			if ($this->db->affected_rows() === 0)
			{
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
	}
	
	function activateBooking($bookingId, $ignoreAppGuid = FALSE)
	{	
		$sql = "UPDATE booking SET Status = 'A', ModifiedTime = ? WHERE Id = ?".($ignoreAppGuid === TRUE ? "":" AND AppInstanceGuid = UNHEX(?) ");
		$param = array(
			gmdate('Y-m-d H:i:s', time()),		
			$bookingId
		);
		
		if($ignoreAppGuid === FALSE){
			array_push($param, $this->shopId);
		}
		
		$query = $this->db->query($sql, $param);	
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			return FALSE;
		}
		else
		{
			if ($this->db->affected_rows() === 0)
			{
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
	}
	
	function getBookingDetailByGuid($guid, $customerId)
	{
		$sql = "SELECT * FROM booking WHERE Status = 'A' AND Guid = UNHEX(?) AND CustomerId = ? AND AppInstanceGuid = UNHEX(?)";
		$param = array(
			$guid,
			$customerId,
			$this->shopId
		);		
		$query = $this->db->query($sql, $param);		
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else if ($query->num_rows() > 0)
		{
			$result = $query->result();
			return $result[0];
		}
		else
		{
			$this->app88log->log_message('db','debug', $this->db->_error_message());
			$this->app88log->log_message('db','debug', $this->db->last_query());
			return FALSE;
		}		
	}
	
	function getPendingBookingByMemberBy($memberId, $ignoreAppGuid = FALSE)
	{
		$sql = "SELECT b.AppInstanceGuid, ts.SessionId, b.ServiceId, b.ResourceId, b.Id AS BookingId, b.TimeSlotStatusId, c.Email, s.Guid AS ServiceGuid, r.Guid AS ResourceGuid, b.StartDateTime AS DealDate, c.FirstName, c.LastName, c.Contact,b.CustomerId FROM `booking` b ".
			"INNER JOIN `service` s ON b.ServiceId = s.Id ".
			"INNER JOIN `resource` r ON b.ResourceId = r.Id ".
			"INNER JOIN `customer` c ON b.CustomerId = c.Id ".
			"INNER JOIN `timeslotstatus` ts ON b.TimeSlotStatusId = ts.Id ".
			"WHERE b.Status = 'P' AND s.Status = 'A' AND s.IsSnapShot = 'T' AND r.Status = 'A' AND r.IsSnapShot = 'T' AND ts.Status = 'Reserved' ".
			"AND c.MemberId = ? AND b.CreatedTime >= ? ".($ignoreAppGuid === TRUE ? "":"AND b.AppInstanceGuid = UNHEX(?) ").
			"ORDER BY b.Id DESC";
		$param = array(
			$memberId,
			$date = $this->common->addTime(gmdate('Y-m-d H:i:s'), 0, -30)
		);		
		if($ignoreAppGuid === FALSE){
			array_push($param, $this->shopId);
		}
		$query = $this->db->query($sql, $param);		
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			return FALSE;
		}
		else
		{
			return  $query->result();
		}
	}
	
	function updateBookingResource($bookingId, $resouceId)
	{
		$sql = "UPDATE booking SET ModifiedTime = ?, ResourceId = ? WHERE Id = ? AND AppInstanceGuid = UNHEX(?)";
		$param = array(
			gmdate('Y-m-d H:i:s', time()),
			$resouceId, 
			$bookingId,
			$this->shopId
		);	
		
		$this->db->trans_start();		
		$query = $this->db->query($sql, $param);	
		$this->db->trans_complete();
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
		else
		{
			if ($this->db->affected_rows() === 0)
			{
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
	}
	
	function updateBooking($bookingId, $serviceId, $resouceId, $startDateTime, $endDateTime, $source)
	{
		$sql = "UPDATE booking SET ModifiedTime = ?, ServiceId = ?, ResourceId = ?, StartDateTime = ?, EndDateTime = ?, Source = ? WHERE Id = ? AND AppInstanceGuid = UNHEX(?)";
		$param = array(
			gmdate('Y-m-d H:i:s', time()),
			$serviceId,
			$resouceId, 
			$startDateTime,
			$endDateTime,
			$source,
			$bookingId,
			$this->shopId
		);	
		
		$this->db->trans_start();		
		$query = $this->db->query($sql, $param);	
		$this->db->trans_complete();
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
		else
		{
			if ($this->db->affected_rows() === 0)
			{
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
	}
}