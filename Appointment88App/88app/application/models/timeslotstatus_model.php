<?php
class TimeSlotStatus_model extends CI_Model {
	private $shopId;
	function __construct()
	{
		parent::__construct();
		$this->shopId = $this->app88param->get88AppGuid();
	}
	
	public function createTimeSlotStatus($model)
	{
		$sql = "INSERT INTO `timeslotstatus` (`MemberId`, `SessionId`, `ServiceId`, `ResourceId`, `BookingTime`, `Status`, `CreatedTime`, `AppInstanceGuid`) VALUES " .
			"(?, UNHEX(?), ?, ?, ?, ?, ?, UNHEX(?));";
		$param = array(
			ISSET($model["memberId"])?$model["memberId"]:0,
			$model["sessionId"],
			$model["serviceId"],
			$model["resourceId"],
			date("Y-m-d H:i:s", strtotime($model["bookingTime"])),
			$model["status"],
			gmdate('Y-m-d H:i:s', time()),
			$this->shopId
		);
		$query = $this->db->query($sql, $param);	
		$timeslotId = $this->db->insert_id();
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
		else
		{
			return $timeslotId;
		}
	}
	
	public function releaseTimeSlot($sessionId)
	{
		$sql = "UPDATE timeslotstatus SET Status = 'Expired', ModifiedTime = ? WHERE SessionId = UNHEX(?) AND Status = 'Reserved' AND AppInstanceGuid = UNHEX(?)";
		$param = array(			
			gmdate('Y-m-d H:i:s', time()),
			$sessionId,
			$this->shopId
		);		
		$query = $this->db->query($sql, $param);		
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	public function getTimeSlotStatus($model)
	{
		$sql = "SELECT * FROM timeslotstatus WHERE SessionId = UNHEX(?) AND ServiceId = ? AND ResourceId = ? AND BookingTime = ? AND Status = ? AND AppInstanceGuid = UNHEX(?)";
		$param = array(
			$model["sessionId"],
			$model["serviceId"],
			$model["resourceId"],
			date("Y-m-d H:i:s", strtotime($model["bookingTime"])),
			$model["status"],
			$this->shopId
		);	
		$query = $this->db->query($sql, $param);		  		
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return NULL;
		}
		else
		{
			return $query->result();	
		}
	}
	
	public function releaseExpritedTimeSlot($duration, $ignoreAppGuid = FALSE)
	{
		$sql = "UPDATE timeslotstatus SET Status = 'Expired', ModifiedTime = ? WHERE Status = 'Reserved' AND IFNULL(ModifiedTime, CreatedTime) <= ?".($ignoreAppGuid === TRUE ? "":"AND AppInstanceGuid = UNHEX(?) ");
		$param = array(			
			gmdate('Y-m-d H:i:s', time()),
			$this->common->addTime(gmdate('Y-m-d H:i:s', time()), 0, -$duration)
		);		
		if($ignoreAppGuid === FALSE){
			array_push($param, $this->shopId);
		}
		$query = $this->db->query($sql, $param);	
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	public function getAvailableTimeSlotByMonth($serviceId, $resourceId, $date)
	{
		//忽略$serviceId, 獲取全部$resourceId的預定情況
		//$sql = "SELECT BookingTime FROM timeslotstatus WHERE ServiceId = ? AND ResourceId = ? AND BookingTime >= ? AND BookingTime<= ? AND Status IN ('Confirmed', 'Reserved') AND AppInstanceGuid = UNHEX(?);";
		$sql = "SELECT BookingTime, s.Duration, b.StartDateTime, b.EndDateTime FROM timeslotstatus ts ".
				"LEFT JOIN service s ON s.Id = ts.ServiceId ".
				"LEFT JOIN resource r ON r.Id = ts.ResourceId ".
				"LEFT JOIN booking b ON b.TimeSlotStatusId = ts.Id ".
				"WHERE  ts.ResourceId = ? AND ts.BookingTime >= ? AND ts.BookingTime<= ? AND ts.Status IN ('Confirmed', 'Reserved') AND ts.AppInstanceGuid = UNHEX(?) ".
				"AND s.Status IN ('I', 'A') AND r.Status='A'";
		$cd = strtotime($date);
		$param = array(
			//$serviceId,
			$resourceId,
			date('Y-m-d H:i:s',  mktime(
				0,
				0,
				0,
				date('m',$cd),
				1,
				date('Y',$cd)
			)),
			date('Y-m-d H:i:s',  mktime(
				0,
				0,
				0,
				date('m',$cd) + 1,
				1,
				date('Y',$cd)
			)),
			$this->shopId
		);	
		
		$query = $this->db->query($sql, $param);		
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return NULL;
		}
		else
		{	
			return $query->result();	
		}
	}
	
	public function getAvailableTimeSlotByDate($serviceId, $resourceId, $date)
	{		
		
		$sql = "SELECT ts.BookingTime, s.Duration, b.StartDateTime, b.EndDateTime ".
			" FROM timeslotstatus ts ".
			"LEFT JOIN service s ON s.Id = ts.ServiceId ".
			"LEFT JOIN resource r ON r.Id = ts.ResourceId ".
			"LEFT JOIN booking b ON b.TimeSlotStatusId = ts.Id ".
			" WHERE ts.ServiceId = ? AND ts.ResourceId = ? AND ts.BookingTime >= ? AND ts.BookingTime<= ? AND ts.Status IN ('Confirmed', 'Reserved') AND ts.AppInstanceGuid = UNHEX(?) ".
			"AND s.Status IN ('I', 'A') AND r.Status='A'";
		$param = array(
			$serviceId,
			$resourceId,
			date("Y-m-d 00:00:00", strtotime($date)),		
			date("Y-m-d 23:59:59", strtotime($date)),
			$this->shopId
		);	
             
		$query = $this->db->query($sql, $param);	
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return NULL;
		}
		else
		{
			return $query->result();	
		}
	}
	
	public function getAvailableTimeSlotByDateTime($serviceId, $resourceId, $datetime, $sessionId = "", $status = "'Confirmed', 'Reserved'")
	{	
		$sql = "SELECT BookingTime, s.Duration ".
				"FROM timeslotstatus ts ".
				"LEFT JOIN service s ON ts.ServiceId = s.Id ".
				"LEFT JOIN booking b ON b.TimeSlotStatusId = ts.Id ".
				"WHERE ts.ResourceId = ? AND ? >= ts.BookingTime  AND ? < IFNULL(b.EndDateTime,date_add(BookingTime,interval s.Duration minute)) AND ts.Status IN (".$status.")".($sessionId !== "" ? " AND ts.SessionId <> UNHEX(?) " : "")."  AND ts.AppInstanceGuid = UNHEX(?);";
		$bookDateTime = date("Y-m-d H:i:00", strtotime($datetime));
		if( $sessionId === "" )
		{
			
			$param = array(
				//$serviceId,
				$resourceId,
				$bookDateTime,
				$bookDateTime,
				$this->shopId
			);	
		}
		else
		{
			$param = array(
				//$serviceId,
				$resourceId,
				$bookDateTime,
				$bookDateTime,
				$sessionId,
				$this->shopId
			);	
		}
		$query = $this->db->query($sql, $param);
		//echo $this->db->last_query();		
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return NULL;
		}
		else
		{
			return $query->result();	
		}
	}
	
	public function getConfirmTimeSlotCount($serviceId, $resourceId, $schema)
	{
		$sql = "SELECT COUNT(*) AS Count FROM `timeslotstatus` WHERE Status = 'Confirmed' AND ServiceId = ? AND ResourceId = ? AND BookingTime = ? AND AppInstanceGuid = UNHEX(?)";
		$param = array(
			$serviceId,
			$resourceId,
			$schema,
			$this->shopId
		);	
        
		$query = $this->db->query($sql, $param);
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return NULL;
		}
		else
		{
			$result = $query->result();
			return $result[0]->Count;
		}
	}
	
	public function updateTimeSlotStatus($model)
	{
		$sql = "UPDATE timeslotstatus SET Status = ?, ModifiedTime = ?, MemberId = ? WHERE SessionId = UNHEX(?) AND ServiceId = ? AND ResourceId = ? AND BookingTime = ? AND Status <> 'Confirmed'  AND AppInstanceGuid = UNHEX(?)";
		$param = array(
			$model["status"],
			gmdate('Y-m-d H:i:s', time()),
			$model["memberId"],
			$model["sessionId"],
			$model["serviceId"],
			$model["resourceId"],
			date("Y-m-d H:i:s", strtotime($model["bookingTime"])),
			$this->shopId
		);		
		$query = $this->db->query($sql, $param);				
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	public function updateTimeSlotStatusById($model, $ignoreAppGuid = FALSE)
	{
		$sql = "UPDATE timeslotstatus SET Status = ?, ModifiedTime = ? WHERE Id = ?".($ignoreAppGuid === TRUE ? "":" AND AppInstanceGuid = UNHEX(?) ");
		$param = array(
			$model["status"],
			gmdate('Y-m-d H:i:s', time()),
			$model["timeSlotId"]			
		);		
		if($ignoreAppGuid === FALSE){
			array_push($param, $this->shopId);
		}
		$query = $this->db->query($sql, $param);			
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	//拋棄的，不再使用
	public function updateTimeSlotRerouceId($model)
	{
		$sql = "UPDATE timeslotstatus SET ResourceId = ?, ModifiedTime = ? WHERE AppInstanceGuid = UNHEX(?) AND Id = ?";
		$param = array(
			$model["resourceId"],
			gmdate('Y-m-d H:i:s', time()),
			$this->shopId,
			$model["tileSlotId"]			
		);		
		$query = $this->db->query($sql, $param);				
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	public function updateTimeSlot($model)
	{
		$sql = "UPDATE timeslotstatus SET ServiceId = ?, ResourceId = ?, BookingTime = ?, ModifiedTime = ? WHERE AppInstanceGuid = UNHEX(?) AND Id = ?";
		$param = array(
			$model["serviceId"],
			$model["resourceId"],
			$model["bookingTime"],
			gmdate('Y-m-d H:i:s', time()),
			$this->shopId,
			$model["tileSlotId"]			
		);		
		$query = $this->db->query($sql, $param);				
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	public function cancelTimeSlot($model)
	{
		$sql = "UPDATE timeslotstatus SET Status = ?, ModifiedTime = ? WHERE MemberId = ? AND Id = ? AND Status = 'Confirmed'  AND AppInstanceGuid = UNHEX(?)";
		$param = array(
			'Cancel',
			gmdate('Y-m-d H:i:s', time()),
			$model["MemberId"],
			$model["TimeSlotStatusId"],
			$this->shopId
		);		
		$query = $this->db->query($sql, $param);						
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
}