<?php
class Customer_model extends CI_Model {
	private $shopId;
	function __construct()
	{
		parent::__construct();
		$this->shopId = $this->app88param->get88AppGuid();
	}
	
	public function createCustomer($model)
	{
		$sql = "INSERT INTO customer (Guid, FirstName, LastName, Contact, Email, MemberId, CreatedTime, AppInstanceGuid, IsOffline) VALUES (UNHEX('?'), ?, ?, ?, ?, ?, ?, UNHEX(?), ?)";
		$param = array(
			$this->guid->newGuid(),
			$model["firstName"],
			$model["lastName"],
			$model["contact"],
			$model["email"],
			$model["memberId"],
			gmdate('Y-m-d H:i:s', time()),
			$this->shopId,
			$model["isOffline"]
		);
		$this->db->trans_start();
		$query = $this->db->query($sql, $param);	
		$customerId = $this->db->insert_id();
		$this->db->trans_complete();
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
		else
		{
			return $customerId;
		}
	}	
	
	public function updateCustomerInfo($model)
	{
		$sql = "UPDATE customer SET FirstName = ?, LastName = ?, Contact = ?, Email = ?, ModifiedTime = ? WHERE MemberId = ? AND AppInstanceGuid = UNHEX(?)";
		$param = array(
			$model["firstName"],
			$model["lastName"],
			$model["contact"],
			$model["email"],
			gmdate('Y-m-d H:i:s', time()),	
			$model["memberId"],
			$this->shopId
		);
		$this->db->trans_start();
		$query = $this->db->query($sql, $param);
		$this->db->trans_complete();
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	public function getCustomerInfoByMemberId($memberId)
	{
		$sql = "SELECT * FROM customer WHERE MemberId = ? AND AppInstanceGuid = UNHEX(?)";
		$param = array(
			$memberId,
			$this->shopId
		);	
		$query = $this->db->query($sql, $param);		
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
		else
		{
			if ($this->db->affected_rows() === 0)
			{
				return FALSE;
			}
			else
			{
				return $query->result();	
			}
		}
	}
	
	public function getCustomerInfoByGuid($customerGuid)
	{
		$sql = "SELECT Id, MemberId, Contact,Email,FirstName,LastName, IsOffline FROM customer WHERE Guid  = UNHEX(?) AND AppInstanceGuid = UNHEX(?)";
		$param = array(
			$customerGuid,
			$this->shopId
		);	
		$query = $this->db->query($sql, $param);	
		
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
		else
		{
			if ($this->db->affected_rows() === 0)
			{
				return FALSE;
			}
			else
			{	
				$result = $query->result();
				return $result[0];	
			}
		}
	}
	
	public function getCustomersNameList()
	{
		$sql = "SELECT Guid as CustomerGuid, FirstName, LastName, Contact, Email FROM customer WHERE AppInstanceGuid = UNHEX(?) ORDER BY FirstName";
		$param = array($this->shopId);		
		$query = $this->db->query($sql, $param);
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else
		{
			return $query->result();
		}
	}
}