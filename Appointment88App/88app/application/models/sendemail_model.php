<?php
class SendEmail_model extends CI_Model {
	private $shopId;
	function __construct()
	{
		parent::__construct();
		$this->load->library("app88email");
		$this->shopId = $this->app88param->get88AppGuid();
	}
	
	public function createEmailRecord($model, $appGuid = '')
	{
		$sql = "INSERT INTO sendemail (SendTo, Subject, Body, AppInstanceGuid, RefId, EmailType, CreatedTime) VALUES (?, ?, ?, UNHEX(?), ?, ?, ?)";
		$param = array(
			$model["to"],
			$model["subject"],
			$model["body"],
			$appGuid === '' ? $this->shopId : $appGuid,
			$model["refId"],
			$model["emailType"],
			gmdate('Y-m-d H:i:s', time())			
		);
		$this->db->trans_start();
		$query = $this->db->query($sql, $param);
		$this->db->trans_complete();
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

}