<?php
class Setting_model extends CI_Model {
	private $shopId;
	function __construct()
	{
		parent::__construct();
		$this->shopId = $this->app88param->get88AppGuid();
	}
	
	function getSetting($appInstanceGuid = '')
	{
		$sql = "SELECT * FROM setting WHERE AppInstanceGuid = UNHEX(?)";
		$param = array(
			$appInstanceGuid === '' ? $this->shopId : $appInstanceGuid
		);		
		$query = $this->db->query($sql, $param);		
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else if ( $query->num_rows() > 0)
		{
			$result = $query->result();		
			$customizeTitle = json_decode($result[0]->CustomizeTitles);
			$lang = $this->app88param->getLang()?$this->app88param->getLang():APP88_ADMIN_LANGUAGE;
			return array(
				'enableNotificationEmail'=>$result[0]->EnableNotificationEmail,
				'enableReminderEmail'=>$result[0]->EnableReminderEmail,
				'reminderMinute'=>$result[0]->ReminderMinute,
				'greeting'=>$result[0]->Greeting,
				'customizeTitles'=> $customizeTitle->$lang === NULL ? "{}" : json_encode($customizeTitle->$lang),
				'wholeCustomizeTitles' => $customizeTitle
			);
		}
		else
		{	
			$json = json_decode(APP88_APPOINTMENT_SETTING);
			$setting = array(
				'enableNotificationEmail' => $json->enableNotificationEmail,
				'enableReminderEmail' => $json->enableReminderEmail,
				'reminderMinute' => $json->reminderMinute,
				'greeting' => $json->greeting,
				'customizeTitles'=>APP88_APPOINTMENT_CUSTOMIZE_TITLE,
				'wholeCustomizeTitles' => APP88_APPOINTMENT_CUSTOMIZE_TITLE
			);
			$this->createSetting($setting);			
			return $setting;
		}		
	}
	
	function getAllShopSettings()
	{
		$sql = "SELECT * FROM setting";
		$param = array();		
		$query = $this->db->query($sql, $param);		
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			return FALSE;
		}
		else if ($query->num_rows() > 0)
		{
			return $query->result();	
		}
		else
		{	
			return FALSE;
		}		
	}
	
	function updateGreeting($greeting)
	{
		$sql = "UPDATE setting SET Greeting = ?, ModifiedTime = ? ".
				"WHERE AppInstanceGuid = UNHEX(?)";
		$param = array(			
			$greeting,		
			gmdate('Y-m-d H:i:s', time()),
			$this->shopId
		);
		
		$query = $this->db->query($sql, $param);	
		if (!$query) 
		{      
			log_message('error', $this->db->_error_message());
			log_message('error', $this->db->last_query());	    			
			$this->common->force_rollback();
			show_error($this->db->last_query());
			return FALSE;			
		} 
		else 
		{		
		    if($this->db->affected_rows() === 0)
			{			
				return FALSE;
			}
			return TRUE;
		}
	}
	
	function updateSetting($setting)
	{
		$sql = "UPDATE setting SET CustomizeTitles = ?, EnableNotificationEmail = ?, EnableReminderEmail = ?, ReminderMinute = ?, ModifiedTime = ? ".
			"WHERE AppInstanceGuid = UNHEX(?)";
		
		//save customize title
		$lang = $this->app88param->getLang()?$this->app88param->getLang():APP88_ADMIN_LANGUAGE;
		$shopSetting = $this->getSetting($this->shopId);
		$wholeCustomizeTitles = $shopSetting["wholeCustomizeTitles"];
		$wholeCustomizeTitles->$lang = $setting["customizeTitles"];

		$param = array(			
			json_encode($wholeCustomizeTitles),
			$setting["enableNotificationEmail"],
			$setting["enableReminderEmail"],
			$setting["reminderMinute"],			
			gmdate('Y-m-d H:i:s', time()),
			$this->shopId
		);
		
		$query = $this->db->query($sql, $param);	
		if (!$query) 
		{      
			log_message('error', $this->db->_error_message());
			log_message('error', $this->db->last_query());	    			
			$this->common->force_rollback();
			show_error($this->db->last_query());
			return FALSE;			
		} 
		else 
		{		
		    if($this->db->affected_rows() === 0)
			{			
				return FALSE;
			}
			return TRUE;
		}
	}
	
	function createSetting($setting)
	{
		$sql = "INSERT INTO setting (AppInstanceGuid, CustomizeTitles, EnableNotificationEmail, EnableReminderEmail, ReminderMinute, CreatedTime) VALUES ".
		"(UNHEX(?), ?, ?, ?, ?, ?);";
		$param = array(
			$this->shopId,
			$setting["customizeTitles"],
			$setting["enableNotificationEmail"],
			$setting["enableReminderEmail"],
			$setting["reminderMinute"],
			gmdate('Y-m-d H:i:s', time())
		);
		
		$this->db->trans_start();
		$query = $this->db->query($sql, $param);
		$this->db->trans_complete();
		
		if ($this->db->trans_status() !== FALSE) 
		{            
			return TRUE;
		} 
		else 
		{
		    log_message('error', $this->db->_error_message());
			log_message('error', $this->db->last_query());	    			
			$this->common->force_rollback();
			show_error($this->lang->line('err_database_err'));
			return FALSE;
		}
	}	
	
}