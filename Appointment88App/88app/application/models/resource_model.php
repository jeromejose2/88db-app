<?php
class Resource_model extends CI_Model {
	private $shopId;
	function __construct()
	{
		parent::__construct();
		$this->shopId = $this->app88param->get88AppGuid();
	}
	
	public function getResourceNameList($ignoreStatus = FALSE)
	{
		$sql = "SELECT Id,Guid,Name FROM `resource` r WHERE r.Status IN (".($ignoreStatus === TRUE ?"'I', 'A'":"'A'").") AND r.IsSnapShot = 'F' AND r.AppInstanceGuid = UNHEX(?) ORDER By Name";
		$param = array(
			$this->shopId
		);		
		$query = $this->db->query($sql, $param);
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));			
		}
		else
		{
			return $query->result();
		}	
	}
	
	public function getActiveResourceCount()
	{
		$sql = "SELECT COUNT(*) AS Count FROM resource WHERE AppInstanceGuid = UNHEX(?) AND IsSnapShot = 'F' AND Status = 'A'";
		$param = array(
			$this->shopId
		);	
		$query = $this->db->query($sql, $param);		
		if (!$query)
		{			
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else if ($query->num_rows() > 0)
		{
			$result = $query->result();
			return $result[0]->Count;
		}
		else
		{
			$this->app88log->log_message('db','debug', $this->db->_error_message());
			$this->app88log->log_message('db','debug', $this->db->last_query());
			return FALSE;
		}
	}
	
	public function getAvailableResourceNameListByServiceGuid($serviceGuid, $onlyHasSchema = FALSE)
	{
		$sql = "SELECT r.Id, r.Guid as ResourceGuid, r.Name, s.AvailablePerTimeSlot, r.Description  FROM `resource_provided_service` rs LEFT JOIN service s ON rs.ServiceId = s.Id LEFT JOIN resource r ON rs.ResourceId = r.Id WHERE s.Status IN ('A', 'I') AND r.Status = 'A' AND rs.Status = 'A' and s.Guid = UNHEX(?) AND s.IsSnapShot = 'F' AND r.AppInstanceGuid = UNHEX(?) AND s.AppInstanceGuid = UNHEX(?)";
		$param = array(
			$serviceGuid,
			$this->shopId,
			$this->shopId
		);		
		$query = $this->db->query($sql, $param);
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));			
		}
		else if ($query->num_rows() > 0)
		{		
			$result = array();
			foreach($query->result() as $r)
			{
				if($this->checkResourceHasSchema( $r->Id ))
				{
					$r->HasSchema = TRUE;
				}
				else
				{
					$r->HasSchema = FALSE;
				}
				if($r->HasSchema || !$onlyHasSchema){
					array_push($result, $r);
				}
			}			
			if(count($result) === 0)
			{
				return FALSE;
			}
			return $result;	
		}
		else
		{					
			$this->app88log->log_message('db','debug', $this->db->_error_message());
			$this->app88log->log_message('db','debug', $this->db->last_query());
			return FALSE;
		}		
	}
	
	public function getResources($resourceGuids)
	{		
		function wrapUNHEX($rs) 
		{	
			return "UNHEX('".$rs."')";
		}		
		$sql = "SELECT r.Guid AS ResourceGuid, r.Name, r.PhoneNumber, r.Email, r.Description FROM `resource` r WHERE r.Status = 'A' AND r.IsSnapShot = 'F' AND AppInstanceGuid = UNHEX(?) AND  r.Guid IN (".(join(',', array_map("wrapUNHEX",$resourceGuids))).")";
		$param = array(			
			$this->shopId,
			$resourceGuid
		);				
		$query = $this->db->query($sql, $param);		
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else
		{
			return $query->result();
		}	
	}
	
	public function getResourceDetailByGuid($resourceGuid, $outId = FALSE, $resourceId = 0, $ignoreServiceStatus = FALSE)
	{
		$sql = "SELECT ".($outId === TRUE?"r.Id,":"")." r.Guid AS ResourceGuid, r.Name, r.AssignColor, r.PhoneNumber, r.Email, r.Description FROM `resource` r WHERE r.Status = 'A' ".($resourceId == 0 ? "AND r.IsSnapShot = 'F' ":"")."AND AppInstanceGuid = UNHEX(?) AND ".($resourceId == 0?"r.Guid = UNHEX(?)":"r.Id = ?");
		if($resourceId == 0)
		{
			$param = array(			
				$this->shopId,
				$resourceGuid
			);		
		}
		else
		{
			$param = array(			
				$this->shopId,
				$resourceId
			);	
		}
		$query = $this->db->query($sql, $param);
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else if ($query->num_rows() > 0)
		{
			$resources = $query->result();
			$resources[0]->Services = $this->getResourceService($resourceGuid, $ignoreServiceStatus);
			return $resources[0];	
		}
		else
		{
			$this->app88log->log_message('db','debug', $this->db->_error_message());
			$this->app88log->log_message('db','debug', $this->db->last_query());
			return FALSE;
		}		
	}
	
	public function getResourceService($resourceGuid, $ignoreServiceStatus = FALSE)
	{
		$sql = "SELECT s.Guid AS ServiceGuid FROM `resource_provided_service` rps LEFT JOIN resource r ON rps.ResourceId = r.Id LEFT JOIN service s ON rps.ServiceId = s.Id WHERE rps.Status = 'A' AND s.Status IN ( ".($ignoreServiceStatus ? "'A','I'": "'A'" )." ) AND r.Status = 'A' AND r.IsSnapShot = 'F' AND r.Guid = UNHEX(?) AND r.AppInstanceGuid = UNHEX(?)";
		$param = array(
			$resourceGuid,
			$this->shopId
		);		
		$query = $this->db->query($sql, $param);
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else if ($query->num_rows() > 0)
		{
			$this->app88log->log_message('db','error', $this->db->last_query());
			return $query->result();
		}
		else
		{
			$this->app88log->log_message('db','debug', $this->db->_error_message());
			$this->app88log->log_message('db','debug', $this->db->last_query());
			return FALSE;
		}		
	}
	
	public function getResourceIdByGuid($resourceGuid)
	{
		
		$sql = "SELECT r.Id FROM `resource` r WHERE r.Status = 'A' AND r.IsSnapShot = 'F' AND r.Guid = UNHEX(?) AND AppInstanceGuid = UNHEX(?)";
		$param = array(
			$resourceGuid,
			$this->shopId
		);		
		$query = $this->db->query($sql, $param);
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else if ($query->num_rows() > 0)
		{
			$result = $query->result();
			return $result[0]->Id;
		}
		else
		{
			$this->app88log->log_message('db','debug', $this->db->_error_message());
			$this->app88log->log_message('db','debug', $this->db->last_query());
			return FALSE;
		}		
	}
	
	public function getResourceServiceDateTime($resourceGuid)
	{
		$result = array();
		
		//get resource.Id from guid
		$resource = $this->getResourceDetailByGuid($resourceGuid, TRUE);
		$resourceId = $resource->Id;		
		//get resourcenormalhours
		$sql = "SELECT rnh.Day, rnh.StartTime, rnh.EndTime FROM `resource_normal_hour` rnh LEFT JOIN resource r ON rnh.ResourceId = r.Id WHERE r.Status = 'A' AND rnh.Status = 'A' AND r.IsSnapShot = 'F' AND r.Id = ?";
		$param = array(
			$resourceId
		);		
		$query = $this->db->query($sql, $param);		
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());			
			show_error($this->lang->line('err_database_err'));
		}
		else if ($query->num_rows() > 0)
		{
			$result["NormalHours"] = $query->result();	
		}
		else
		{
			$result["NormalHours"] = array();
		}
		//get resourcedayoff
		$sql = "SELECT rdo.StartDate, rdo.EndDate FROM `resource_dayoff` rdo LEFT JOIN resource r ON rdo.ResourceId = r.Id WHERE r.Status = 'A' AND rdo.Status = 'A' AND r.Id = ?";			
		$query = $this->db->query($sql, $param);		
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());			
			show_error($this->lang->line('err_database_err'));
		}
		else if ($query->num_rows() > 0)
		{
			$result["DayOff"] = $query->result();	
		}
		
		//get ResourceSpecialHours
		$sql = "SELECT rsh.Date, rsh.StartTime, rsh.EndTime FROM `resource_special_hour` rsh LEFT JOIN resource r ON rsh.ResourceId = r.Id WHERE r.Status = 'A' AND rsh.Status = 'A' AND r.Id = ?";
		$query = $this->db->query($sql, $param);		
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());			
			show_error($this->lang->line('err_database_err'));
		}
		else if ($query->num_rows() > 0)
		{
			$result["SpecialHours"] = $query->result();	
		}
		else
		{
			$result["SpecialHours"] = array();
		}
		return $result;
	}
	
	public function updateResource($resource)
	{
		$resourceId = $this->getResourceIdByGuid($resource["ResourceGuid"]);
		if($resourceId === FALSE)
		{
			show_error("resource not found.");
		}		
		$this->db->trans_start();
		//update resource
		$sql = "UPDATE resource SET Version = Version + 1, Name = ?, PhoneNumber = ?, AssignColor = ?, Email = ?, Description = ?, ModifiedTime = ? WHERE Guid = UNHEX(?) AND IsSnapShot = 'F' AND AppInstanceGuid = UNHEX(?)";
		$param = array(
			$resource["Name"],
			$resource["PhoneNumber"],
			$resource["AssignColor"],
			$resource["Email"],
			$resource["Description"],
			gmdate('Y-m-d H:i:s', time()),
			$resource["ResourceGuid"],
			$this->shopId
		);
		$query = $this->db->query($sql, $param);		
		//$this->app88log->log_message('db','error', $this->db->last_query());
		
		//update resource_dayoff
		//delete all
		$sql = "UPDATE resource_dayoff SET Status = 'D', ModifiedTime = ?  WHERE  ResourceId = ?";
		$query = $this->db->query($sql, array(
			gmdate('Y-m-d H:i:s', time()),
			$resourceId
		));								
		
		if(ISSET($resource["DayOff"]) === TRUE)
		{
			foreach($resource["DayOff"] as $dayoff)
			{
				$startDate = $dayoff["StartDate"];
				$endDate = $dayoff["EndDate"];
				
				//check is exist
				$sql = "SELECT * FROM resource_dayoff WHERE ResourceId = ? AND StartDate = ? AND EndDate ".($endDate == "null" ? "IS":"=")." ?";
				$query = $this->db->query($sql, array(
					$resourceId,
					$startDate,
					$endDate == "null" ? null : $endDate
				));				
				
				if($this->db->affected_rows() > 0)
				{
					//update
					$sql = "UPDATE resource_dayoff SET Status = 'A', ModifiedTime = ? WHERE ResourceId = ? AND StartDate = ? AND EndDate ".($endDate == "null" ? "IS":"=")." ?";
					$query = $this->db->query($sql, array(
						gmdate('Y-m-d H:i:s', time()),
						$resourceId,
						$startDate,
						$endDate == "null" ? null : $endDate
					));					
				}
				else
				{
					//create
					$sql = "INSERT INTO resource_dayoff (ResourceId, StartDate, EndDate, Status, CreatedTime) VALUES (? ,?, ?, 'A', ?)";
					$query = $this->db->query($sql, array(
						$resourceId,
						$startDate,
						$endDate == "null" ? null : $endDate,
						gmdate('Y-m-d H:i:s', time())
					));
				}
			}			
		}		
		
		//update resource_normal_hour
		//delete all
		$sql = "UPDATE resource_normal_hour SET Status = 'D', ModifiedTime = ?  WHERE  ResourceId = ?";
		$query = $this->db->query($sql, array(
			gmdate('Y-m-d H:i:s', time()),
			$resourceId
		));								
		
		if(ISSET($resource["NormalHours"]) === TRUE)
		{
			foreach($resource["NormalHours"] as $normalhour)
			{
				$day = $normalhour["Day"];
				$startTime = $normalhour["StartTime"];
				$endTime = $normalhour["EndTime"];
				if($startTime === $endTime)
				{
					continue;
				}
				//check is exist
				$sql = "SELECT * FROM resource_normal_hour WHERE ResourceId = ? AND Day = ? AND StartTime = ? AND EndTime = ?";
				$query = $this->db->query($sql, array(
					$resourceId,
					$day,
					$startTime,
					$endTime
				));				
				
				if($this->db->affected_rows() > 0)
				{
					//update
					$sql = "UPDATE resource_normal_hour SET Status = 'A', ModifiedTime = ? WHERE ResourceId = ? AND Day = ? AND StartTime = ? AND EndTime = ?";
					$query = $this->db->query($sql, array(
						gmdate('Y-m-d H:i:s', time()),
						$resourceId,
						$day,
						$startTime,
						$endTime
					));					
				}
				else
				{
					//create
					$sql = "INSERT INTO resource_normal_hour (ResourceId, Day, StartTime, EndTime, Status, CreatedTime) VALUES (? ,?, ?, ?, 'A', ?)";
					$query = $this->db->query($sql, array(
						$resourceId,
						$day,
						$startTime,
						$endTime,
						gmdate('Y-m-d H:i:s', time())
					));
				}
			}			
		}		
		
		//update resource_provided_service
		//delete all
		$sql = "UPDATE resource_provided_service SET Status = 'D', ModifiedTime = ?  WHERE  ResourceId = ?";
		$query = $this->db->query($sql, array(
			gmdate('Y-m-d H:i:s', time()),
			$resourceId
		));								
		
		if(ISSET($resource["Services"]) === TRUE)
		{
			foreach($resource["Services"] as $service)
			{
				$serviceGuid = $service["ServiceGuid"];
				$serviceId = $this->service_model->getServiceIdByGuid($serviceGuid, TRUE);
				if($serviceId === FALSE)
				{
					continue;
				}
				//check is exist
				$sql = "SELECT * FROM resource_provided_service WHERE ResourceId = ? AND ServiceId = ?";
				$query = $this->db->query($sql, array(
					$resourceId,
					$serviceId
				));				
				
				if($this->db->affected_rows() > 0)
				{
					//update
					$sql = "UPDATE resource_provided_service SET Status = 'A', ModifiedTime = ? WHERE ResourceId = ? AND ServiceId = ?";
					$query = $this->db->query($sql, array(
						gmdate('Y-m-d H:i:s', time()),
						$resourceId,
						$serviceId
					));					
				}
				else
				{
					//create
					$sql = "INSERT INTO resource_provided_service (ResourceId, ServiceId, Status, CreatedTime) VALUES (? ,?, 'A', ?)";
					$query = $this->db->query($sql, array(
						$resourceId,
						$serviceId,
						gmdate('Y-m-d H:i:s', time())
					));
				}
			}			
		}	
		
		//update resource_special_hour
		//delete all
		$sql = "UPDATE resource_special_hour SET Status = 'D', ModifiedTime = ?  WHERE  ResourceId = ?";
		$query = $this->db->query($sql, array(
			gmdate('Y-m-d H:i:s', time()),
			$resourceId
		));								
		
		if(ISSET($resource["SpecialHours"]) === TRUE)
		{
			foreach($resource["SpecialHours"] as $specialHour)
			{
				$date = $specialHour["Date"];
				$startTime = $specialHour["StartTime"];
				$endTime = $specialHour["EndTime"];
				if($startTime === $endTime)
				{
					continue;
				}
				//check is exist
				$sql = "SELECT * FROM resource_special_hour WHERE ResourceId = ? AND Date = ? AND StartTime = ? AND EndTime = ?";
				$query = $this->db->query($sql, array(
					$resourceId,
					$date,
					$startTime,
					$endTime
				));				
				
				if($this->db->affected_rows() > 0)
				{
					//update
					$sql = "UPDATE resource_special_hour SET Status = 'A', ModifiedTime = ? WHERE ResourceId = ? AND Date = ? AND StartTime = ? AND EndTime = ?";
					$query = $this->db->query($sql, array(
						gmdate('Y-m-d H:i:s', time()),
						$resourceId,
						$date,
						$startTime,
						$endTime
					));					
				}
				else
				{
					//create
					$sql = "INSERT INTO resource_special_hour (ResourceId, Date, StartTime, EndTime, Status, CreatedTime) VALUES (? ,?, ?, ?, 'A', ?)";
					$query = $this->db->query($sql, array(
						$resourceId,
						$date,
						$startTime,
						$endTime,
						gmdate('Y-m-d H:i:s', time())
					));
				}
			}			
		}	
		
		//update snapshot assign color
		$sql_assignColor = "UPDATE resource SET AssignColor = ? WHERE Guid = UNHEX(?) AND AppInstanceGuid = UNHEX(?) AND IsSnapShot = 'T' AND Status IN ('A', 'I')";
		$param_assignColor = array(			
			$resource["AssignColor"],
			$resource["ResourceGuid"],			
			$this->shopId
		);		
		$query = $this->db->query($sql_assignColor, $param_assignColor);		
		
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			$this->db->trans_rollback();
			show_error($this->lang->line('err_database_err'));			
		}
		else
		{
			$this->db->trans_commit();			
			return TRUE;
		}	
	}
	
	public function createResource($resource)	
	{
		
		$this->db->trans_start();
		//create resource
		$sql = "INSERT INTO resource (Guid, AppInstanceGuid, Name, PhoneNumber, AssignColor, Email, Description, Status, Version, IsSnapShot, CreatedTime) VALUES ".
			"(UNHEX('?'), UNHEX(?), ?, ?, ?, ?, ?, 'A', 1, 'F', ?)";
		$param = array(
			$this->guid->newGuid(),
			$this->shopId,
			$resource["Name"],
			$resource["PhoneNumber"],
			$resource["AssignColor"],
			$resource["Email"],
			$resource["Description"],
			gmdate('Y-m-d H:i:s', time())
		);
		$query = $this->db->query($sql, $param);		
		$resourceId = $this->db->insert_id();
		
		//create resource_dayoff
		if(ISSET($resource["DayOff"]) === TRUE)
		{
			foreach($resource["DayOff"] as $dayoff)
			{
				$startDate = $dayoff["StartDate"];
				$endDate = $dayoff["EndDate"];				
				
				//create
				$sql = "INSERT INTO resource_dayoff (ResourceId, StartDate, EndDate, Status, CreatedTime) VALUES (? ,?, ?, 'A', ?)";
				$query = $this->db->query($sql, array(
					$resourceId,
					$startDate,
					$endDate == "null" ? null : $endDate,
					gmdate('Y-m-d H:i:s', time())
				));				
			}			
		}		
		
		//update resource_normal_hour
		if(ISSET($resource["NormalHours"]) === TRUE)
		{
			foreach($resource["NormalHours"] as $normalhour)
			{
				$day = $normalhour["Day"];
				$startTime = $normalhour["StartTime"];
				$endTime = $normalhour["EndTime"];				
				if($startTime === $endTime)
				{
					continue;
				}
				//create
				$sql = "INSERT INTO resource_normal_hour (ResourceId, Day, StartTime, EndTime, Status, CreatedTime) VALUES (? ,?, ?, ?, 'A', ?)";
				$query = $this->db->query($sql, array(
					$resourceId,
					$day,
					$startTime,
					$endTime,
					gmdate('Y-m-d H:i:s', time())
				));				
			}			
		}		
		
		//update resource_provided_service
		if(ISSET($resource["Services"]) === TRUE)
		{
			foreach($resource["Services"] as $service)
			{
				$serviceGuid = $service["ServiceGuid"];
				$serviceId = $this->service_model->getServiceIdByGuid($serviceGuid, TRUE);
				if($serviceId === FALSE)
				{
					continue;
				}				
				//create
				$sql = "INSERT INTO resource_provided_service (ResourceId, ServiceId, Status, CreatedTime) VALUES (? ,?, 'A', ?)";
				$query = $this->db->query($sql, array(
					$resourceId,
					$serviceId,
					gmdate('Y-m-d H:i:s', time())
				));				
			}			
		}	
		
		//update resource_special_hour
		if(ISSET($resource["SpecialHours"]) === TRUE)
		{
			foreach($resource["SpecialHours"] as $specialHour)
			{
				$date = $specialHour["Date"];
				$startTime = $specialHour["StartTime"];
				$endTime = $specialHour["EndTime"];				
				if($startTime === $endTime)
				{
					continue;
				}
				//create
				$sql = "INSERT INTO resource_special_hour (ResourceId, Date, StartTime, EndTime, Status, CreatedTime) VALUES (? ,?, ?, ?, 'A', ?)";
				$query = $this->db->query($sql, array(
					$resourceId,
					$date,
					$startTime,
					$endTime,
					gmdate('Y-m-d H:i:s', time())
				));				
			}			
		}	
		
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			$this->db->trans_rollback();
			show_error($this->lang->line('err_database_err'));			
		}
		else
		{
			$this->db->trans_commit();			
			return TRUE;
		}	
	}
	
	public function createResourceSnapShot($resourceGuid)
	{
		$sql = "INSERT INTO resource (AssignColor, Guid, AppInstanceGuid, Name, PhoneNumber, Email, Description, Status, Version, IsSnapShot, CreatedTime) ".
			"SELECT AssignColor, Guid, AppInstanceGuid, Name, PhoneNumber, Email, Description, Status, Version, 'T', ? FROM resource WHERE Status = 'A' AND IsSnapShot = 'F' AND Guid = UNHEX(?);";	
		$param = array(
			gmdate('Y-m-d H:i:s', time()),
			$resourceGuid
		);
		$query = $this->db->query($sql, $param);
		$resourceId = $this->db->insert_id();
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else
		{
			return $resourceId;
		}	
	}
	
	public function getResourceSnapShot($resourceGuid)
	{
		$result;
		$sql = "SELECT Id, Version, Name, PhoneNumber, AssignColor, Email, Description, Status, Version FROM resource WHERE Status = 'A' AND Guid = UNHEX(?) AND IsSnapShot = 'T' AND Version = (SELECT Version FROM resource WHERE Status = 'A' AND IsSnapShot = 'F' AND Guid = UNHEX(?) );";
		
		$param = array(
			$resourceGuid,
			$resourceGuid
		);
		
		$query = $this->db->query($sql, $param);
		if (!$query)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else if ($query->num_rows() > 0)
		{
			return $query->result();	
		}
		else
		{
			$this->app88log->log_message('db','debug', $this->db->_error_message());
			$this->app88log->log_message('db','debug', $this->db->last_query());
			return FALSE;
		}	
	}	
	
	//判斷Resource是否和Service關聯
	public function isReourceProvidedService($serviceId, $resourceId)
	{
		$sql = "SELECT COUNT(*) AS Count FROM resource_provided_service WHERE ServiceId = ? AND ResourceId = ? AND Status = 'A'";
		$param = array(
			$serviceId,
			$resourceId
		);	
		$query = $this->db->query($sql, $param);		
		if (!$query)
		{			
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else if ($query->num_rows() > 0)
		{
			$result = $query->result();
			return $result[0]->Count > 0;
		}
		else
		{
			$this->app88log->log_message('db','debug', $this->db->_error_message());
			$this->app88log->log_message('db','debug', $this->db->last_query());
			return FALSE;
		}
	}
	
	//檢查Resource當前時間開始時候有schema
	public function checkResourceHasSchema($resourceId)
	{
		//是否有normal day
		$sql = "SELECT COUNT(*) AS Count FROM resource_normal_hour WHERE ResourceId = ? AND Status = 'A'";
		$param = array(
			$resourceId
		);
		$query = $this->db->query($sql, $param);		
		if (!$query)
		{			
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else if ($query->num_rows() > 0)
		{
			$result = $query->result();
			if($result[0]->Count == 0)
			{	
				//是否有special hour
				$sql = "SELECT COUNT(*) AS Count FROM resource_special_hour WHERE ResourceId = ? AND Status = 'A'";
				$param = array(
					$resourceId
				);
				$query = $this->db->query($sql, $param);
				if (!$query)
				{			
					$this->app88log->log_message('db','error', $this->db->_error_message());
					$this->app88log->log_message('db','error', $this->db->last_query());
					show_error($this->lang->line('err_database_err'));
				}
				else if ($query->num_rows() > 0)
				{
					$result = $query->result();
					if($result[0]->Count == 0)
					{
						return FALSE; // 沒有normal day
					}
				}
				else
				{
					return FALSE; // 沒有resource
				}				
			}
		}
		else
		{
			return FALSE; // 沒有resource
		}
		
		//no end day的day off startdate是否小於當前時間
		$sql = "SELECT StartDate, EndDate FROM resource_dayoff WHERE ResourceId = ? AND Status = 'A' AND EndDate IS NULL";
		$query = $this->db->query($sql, $param);		
		if (!$query)
		{			
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			show_error($this->lang->line('err_database_err'));
		}
		else if ($query->num_rows() > 0)
		{
			$result = $query->result();
			$now = gmdate('Y-m-d H:i:s', time());
			foreach($result as $model)
			{
				if(strtotime($model->StartDate) <= time())
				{
					return FALSE;
				}
			}			
		}
		return TRUE;
	}
	
	public function deleteResource($resourceGuid)	
	{
		$resourceId = $this->getResourceIdByGuid($resourceGuid);
		if($resourceId === FALSE)
		{
			show_error("resource not found.");
		}		
		$this->db->trans_start();
		//delete resource
		$sql = "UPDATE resource SET Status = 'D', ModifiedTime = ? WHERE Id = ? AND IsSnapShot = 'F' AND AppInstanceGuid = UNHEX(?)";
		$param = array(
			gmdate('Y-m-d H:i:s', time()),
			$resourceId,
			$this->shopId
		);
		$query = $this->db->query($sql, $param);		
		
		//update resource_dayoff
		//delete all
		$sql = "UPDATE resource_dayoff SET Status = 'D', ModifiedTime = ?  WHERE  ResourceId = ?";
		$query = $this->db->query($sql, array(
			gmdate('Y-m-d H:i:s', time()),
			$resourceId
		));								
		
		
		//update resource_normal_hour
		//delete all
		$sql = "UPDATE resource_normal_hour SET Status = 'D', ModifiedTime = ?  WHERE  ResourceId = ?";
		$query = $this->db->query($sql, array(
			gmdate('Y-m-d H:i:s', time()),
			$resourceId
		));	
		
		//update resource_provided_service
		//delete all
		$sql = "UPDATE resource_provided_service SET Status = 'D', ModifiedTime = ?  WHERE  ResourceId = ?";
		$query = $this->db->query($sql, array(
			gmdate('Y-m-d H:i:s', time()),
			$resourceId
		));								
		
		//update resource_special_hour
		//delete all
		$sql = "UPDATE resource_special_hour SET Status = 'D', ModifiedTime = ?  WHERE  ResourceId = ?";
		$query = $this->db->query($sql, array(
			gmdate('Y-m-d H:i:s', time()),
			$resourceId
		));								
		
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			$this->app88log->log_message('db','error', $this->db->_error_message());
			$this->app88log->log_message('db','error', $this->db->last_query());
			$this->db->trans_rollback();
			show_error($this->lang->line('err_database_err'));			
		}
		else
		{
			$this->db->trans_commit();			
			return TRUE;
		}	
	}
	
	
}