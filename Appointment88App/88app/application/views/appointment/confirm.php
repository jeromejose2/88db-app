<?php 
	include_once 'includePHPHeaders.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head> 
  <?php $this->load->view("section/header.php"); ?>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/date.js") ?>"></script>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/front_end/confirm.js") ?>"></script>
  <script type="text/javascript">
		global["SELECTERESOURCE_URL"] = "<?php echo site_url("appointment/selectResource") ?>";
		global["SELECTESCHEDULE_URL"] = "<?php echo site_url("appointment/selectSchedule") ?>";		
		global["verifyCodeError_dialog_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('verifyCodeError_dialog_title')) ?>";
		global["verifyCodeError_dialog_content"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('verifyCodeError_dialog_content')) ?>";
		global["close"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('close')) ?>";
		global["COMPLETE_URL"] = "<?php echo site_url("appointment/complete") ?>";		
	</script>
 </head> 
 <body> 
  <div id="wrapper"> 
   <div id="header"> 
    <ul id="headers">
      <?php echo $this->lang->appointmentLine('title_make_appointment') ?>
    </ul> 
	<?php $this->load->view("section/header_memberinfo.php"); ?>
   </div> 
   <?php $this->load->view("section/customer_header.php"); ?>
<div class="ProgressBar">

<span class="step_one">
<a class="dbShop-ui-Progress_visited"  href="<?php echo site_url("appointment/welcome") ?>"> 
<div class="dbShop-ui-Progress_step">1.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_select_service') ?></span>
<span class="dbShop-ui-Progress_detail appServiceName">--</span>
</div>
</a>
</span>

<span class="step_two">
<a class="dbShop-ui-Progress_visited appBtnGotoStep3"> 
<div class="dbShop-ui-Progress_step">2.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_select_resource_schedule') ?></span>
<span class="dbShop-ui-Progress_detail appDealDate">--</span>
</div>
</a>
</span>

<span class="step_three">
<a class="dbShop-ui-Progress_visited" href="javascript:history.back();"> 
<div class="dbShop-ui-Progress_step">3.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_contace_info') ?></span>
<span class="dbShop-ui-Progress_detail"></span>
</div>
</a>
</span>

<span class="step_four">
<div class="dbShop-ui-Progress_active"> 
<div class="dbShop-ui-Progress_step">4.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_confirmation') ?></span>
<span class="dbShop-ui-Progress_detail"></span>
</div>
</div>
</span>

</div>

   
   <noscript> 
    <style type="text/css">
		#mcs_container .customScrollBox{overflow:auto;}
		#mcs_container .dragger_container{display:none;}
	</style> 
   </noscript> 
   <div id="main_full"> 
   <div style="width:500px; padding-left:10px" class="detail-title">
			<h2><?php echo $this->lang->appointmentLine('tab_confirmation') ?></h2>
		</div>
    <div class="tabs-info"> 
     <div class="form_container_a"> 
      <div class="form_title">
       <?php echo $this->lang->appointmentLine('service') ?> :
      </div> 
      <div class="form_info">
       <span id="serviceName">--</span>
      </div> 
     </div> 
     <div class="dbShop-ui-clearboth"></div> 
     <div class="form_container_b"> 
      <div class="form_title">
       <?php echo $this->lang->appointmentLine('resource') ?> :
      </div> 
      <div class="form_info">
       <span id="resourceName">--</span>
      </div> 
     </div> 
     <div class="dbShop-ui-clearboth"></div> 
     <div class="form_container_b"> 
      <div class="form_title">
       <?php echo $this->lang->appointmentLine('schedule') ?> :
      </div> 
      <div class="form_info">
       <span id="scheduleTime">--</span>
      </div> 
     </div> 
     <div class="dbShop-ui-clearboth"></div> 
     <div class="form_container_b"> 
      <div class="form_title">
      <?php echo $this->lang->appointmentLine('contact_info') ?> :
      </div> 
      <div class="form_info">
        <?php echo $this->lang->appointmentLine('name') ?> : <span id="firstName"><?php echo $contact["firstName"] ?></span> <span id="lastName"><?php echo $contact["lastName"] ?></span>
       <p></p> <?php echo $this->lang->appointmentLine('contact') ?> : <span id="phoneNumber"><?php echo $contact["phoneNumber"] ?></span>
       <p></p>  <?php echo $this->lang->appointmentLine('email') ?> : <span id="email"><?php echo $contact["email"] ?></span>
	   <span id="note" style="display:none"> <?php echo $contact["note"] ?></span>
      </div> 
     </div> 
     <div class="dbShop-ui-clearboth"></div> 
     <div class="form_container_b"> 
      <div class="form_title">
        <?php echo $this->lang->appointmentLine('reminder') ?> :
      </div> 
      <div class="form_info">
       <div class="checkbox">
        <input type="checkbox" id = "IsReminder" checked="" />
       </div> <?php echo $this->lang->appointmentLine('send_me_email_reminder') ?>
      </div> 
     </div> 
     <div class="dbShop-ui-clearboth"></div> 
	 <div class="dbShop-ui-FormArea_b_Verify">
<input type="text" style="color:#777777;" class="txtField common-validatebox txtVerifyCode" maxlength  ="4" data-options="required:true, missingMessage:'<?php echo $this->app88common->js_quote($this->lang->appointmentLine('verifycode')) ?>'" placeholder="<?php echo $this->lang->appointmentLine('verifycode') ?>">
<a href="javascript:void(0);" onclick="return false;" class="dbShop-ui-refreshCode btnRereshVerifyCode"></a>
<div class="dbShop-ui-vCode"><img src="#" id="imgConfirmVerifyCode"></div>
</div>
     <div class="dbShop-ui-clearboth"></div> 


	 <div class="UI-Btn-Btm_narrow">	
<a class="dbShop-ui-SecondaryBtn dbShop-ui-FormBack" href="javascript:history.back();"> 
<span class="dbShop-ui-inner_text"><?php echo $this->lang->appointmentLine('back') ?></span>
<span class="dbShop-ui-CutomIcon dbShop-ui-BackIco_White"></span>
</a>
 <a style="margin-left:10px; float:left;" class="dbShop-ui-PrimaryBtn dbShop-ui-FormNext btnNext" href="javascript:;" onclick="return false"> 
<span class="dbShop-ui-inner_text"><?php echo $this->lang->appointmentLine('confirm_appointment') ?></span>
<span class="dbShop-ui-CutomIcon dbShop-ui-NextIco_White"></span>
</a>         </div>
<div><label class="note_agreement"><?php echo sprintf($this->lang->appointmentLine('tc'), APP88_88DB_PRIVACY_LINK, APP88_88DB_TERMS_LINK); ?></label></div>


     <div class="dbShop-ui-clearboth"></div> 
    </div> 
    <div style="clear:both"></div> 
   </div>  
   <?php $this->load->view("section/footer.php"); ?>
  </div>
 </body>
</html>
<?php 
	echo '<!-- '.$this->common->showBenchmark().' -->';
?>