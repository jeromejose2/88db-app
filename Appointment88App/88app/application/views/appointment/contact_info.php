<?php 
	include_once 'includePHPHeaders.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head> 
  <?php $this->load->view("section/header.php"); ?>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/date.js") ?>"></script>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/front_end/contactinfo.js") ?>"></script>
  <script type="text/javascript">
		global["SELECTERESOURCE_URL"] = "<?php echo site_url("appointment/selectResource") ?>";
		global["CONFIRM_URL"] = "<?php echo site_url("appointment/confirm") ?>";		
		global["resx_invalid_emailcompare"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('invalid_emailcompare')) ?>";
	</script>
 </head> 
 <body> 
  <div id="wrapper"> 
   <div id="header"> 
    <ul id="headers">
      <?php echo $this->lang->appointmentLine('title_make_appointment') ?>
    </ul> 
	<?php $this->load->view("section/header_memberinfo.php"); ?>
   </div> 
   <?php $this->load->view("section/customer_header.php"); ?>
<div class="ProgressBar">

<span class="step_one">
<a class="dbShop-ui-Progress_visited" href="<?php echo site_url("appointment/welcome") ?>"> 
<div class="dbShop-ui-Progress_step">1.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_select_service') ?></span>
<span class="dbShop-ui-Progress_detail appServiceName"> -- </span>
</div>
</a>
</span>

<span class="step_two">
<a class="dbShop-ui-Progress_visited " href="<?php echo site_url("appointment/selectResource?serviceid=".$_GET["serviceid"]."&resourceid=".$_GET["resourceid"]) ?>"> 
<div class="dbShop-ui-Progress_step">2.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_select_resource_schedule') ?></span>
<span class="dbShop-ui-Progress_detail appDealDate">--</span>
</div>
</a>
</span>

<span class="step_three">
<div class="dbShop-ui-Progress_active" href="#"> 
<div class="dbShop-ui-Progress_step">3.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_contace_info') ?></span>
<span class="dbShop-ui-Progress_detail"></span>
</div>
</div>
</span>

<span class="step_four">
<div class="dbShop-ui-Progress_normal"> 
<div class="dbShop-ui-Progress_step">4.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_confirmation') ?></span>
<span class="dbShop-ui-Progress_detail"></span>
</div>
</div>
</span>

</div>
   
   <noscript> 
    <style type="text/css">
		#mcs_container .customScrollBox{overflow:auto;}
		#mcs_container .dragger_container{display:none;}
	</style> 
   </noscript> 
   
   <div id="main_full"> 
   <div style="width:500px; padding-left:10px" class="detail-title">
			<h2><?php echo $this->lang->appointmentLine('contact_info') ?></h2>
		</div>
    <div class="tabs-info_contact_a"> 
     <div class="form_container_a"> 
      <div class="form_title">
      <span class="compulsory">*</span> <?php echo $this->lang->appointmentLine('name') ?> :
      </div> 
      <div class="dbShop-ui-FormArea_<?php echo APP88_SINGLE_NAME_FIELD === 'disable'?'a':'b' ?>"> 
       <input type="text" id="firstName" class="txtField common-validatebox" value="<?php echo $displayName ?>" maxlength="100" data-options="required:true, missingMessage:'<?php echo $this->app88common->js_quote($this->lang->appointmentLine('verify_firstname')) ?>'" placeholder="<?php echo $this->lang->appointmentLine('first_name') ?>"  />
      </div> 
	  <?php if(APP88_SINGLE_NAME_FIELD === 'disable'): ?>
      <div class="dbShop-ui-FormArea_a"> 
       <input type="text" id="lastName" class="txtField common-validatebox" value="<?php echo $customer[0]->LastName ?>" maxlength="100" data-options="required:true, missingMessage:'<?php echo $this->app88common->js_quote($this->lang->appointmentLine('verify_lastname')) ?>'" placeholder="<?php echo $this->lang->appointmentLine('last_name') ?>"  />
      </div> 
	  <?php endif;?>
     </div> 
     <div class="dbShop-ui-clearboth"></div> 
     <div class="form_container_b"> 
      <div class="form_title">
       <span class="compulsory">*</span><?php echo $this->lang->appointmentLine('contact') ?> :
      </div> 
      <div class="dbShop-ui-FormArea_b"> 
       <input type="text" id="phoneNumber" class="txtField common-validatebox" value="<?php echo $customer[0]->Contact ?>" maxlength="25" data-options="required:true,validType: 'phone',invalidMessage:'<?php echo $this->app88common->js_quote($this->lang->appointmentLine('invalid_phone')) ?>',  missingMessage:'<?php echo $this->app88common->js_quote($this->lang->appointmentLine('verify_phone')) ?>'" placeholder="<?php echo $this->lang->appointmentLine('phone_number') ?>"  />
      </div> 
     </div> 
     <div class="dbShop-ui-clearboth"></div> 
	 <?php if($isLogin === FALSE){?>
     <div class="form_container_b"> 
      <div class="form_title">
      <span class="compulsory">*</span> <?php echo $this->lang->appointmentLine('email') ?> :
      </div> 
      <div class="dbShop-ui-FormArea_b"> 
       <input type="text" id="email" class="txtField common-validatebox" value="<?php echo $memberEmail ?>" maxlength="100" data-options="required:true,validType: 'email',invalidMessage:'<?php echo $this->app88common->js_quote($this->lang->appointmentLine('invalid_email')) ?>', missingMessage:'<?php echo $this->app88common->js_quote($this->lang->appointmentLine('verify_email')) ?>'" placeholder="<?php echo $this->lang->appointmentLine('email') ?>"  />
      </div> 
     </div> 
     <div class="dbShop-ui-clearboth"></div> 
     <div class="form_container_b"> 
      <div class="form_title">
       <span class="compulsory">*</span> <?php echo $this->lang->appointmentLine('email_verify') ?> :
      </div> 
      <div class="dbShop-ui-FormArea_b"> 
       <input type="text" id="emailVerify" class="txtField common-validatebox" value="<?php echo $memberEmail ?>" maxlength="100" data-options="required:true,validType: 'email',invalidMessage:'<?php echo $this->app88common->js_quote($this->lang->appointmentLine('invalid_email')) ?>', missingMessage:'<?php echo $this->app88common->js_quote($this->lang->appointmentLine('verify_verifyemail')) ?>'" placeholder="<?php echo $this->lang->appointmentLine('email') ?>"  />
      </div> 
     </div> 	
	 <?php }else{ ?>
	 <div class="form_container_b"> 
      <div class="form_title">
       <?php echo $this->lang->appointmentLine('email') ?> :
      </div> 
      <div class="dbShop-ui-FormArea_b"> 
       <label><?php echo $memberEmail ?></label>
      </div> 
     </div> 
     <div class="dbShop-ui-clearboth"></div> 
	 <?php }?>
	 
	  <div class="dbShop-ui-clearboth"></div>
<div class="form_container_b">		
		<div class="form_title"><?php echo $this->lang->appointmentLine('note_optional') ?> :</div>
		<div class="dbShop-ui-FormArea_b">
			<textarea id="note" style="height:100px;" class="txtArea"  placeholder="<?php echo $this->lang->appointmentLine('please_leave_message_here') ?>"><?php echo $customer[0]->Note ?></textarea>
		</div>
	</div>

    </div> 
    <?php if (FALSE) { ?>
	<?php if($isLogin === FALSE) {?>
    <div class="tabs-info_contact_b">
      <?php echo $this->lang->appointmentLine('login_with_facebook') ?>
     <div class="dbShop-ui-clearboth"></div> 
     <a class="itemBtn-fb" href="#"></a> 
    </div> 
	<?php } ?>
	<?php } ?>
    <div class="dbShop-ui-clearboth"></div> 

	
		 
	 <div style="padding-left:130px;margin-top:10px; width:auto;" class="UI-Btn-Btm">	
<a class="dbShop-ui-SecondaryBtn dbShop-ui-FormBack" href="<?php echo site_url("appointment/selectResource?serviceid=".$_GET["serviceid"]."&resourceid=".$_GET["resourceid"]) ?>"> 
<span class="dbShop-ui-inner_text"><?php echo $this->lang->appointmentLine('back') ?></span>
<span class="dbShop-ui-CutomIcon dbShop-ui-BackIco_White"></span>
</a>
 <a style="margin-left:10px; float:left;" class="dbShop-ui-PrimaryBtn dbShop-ui-FormNext btnNext" href="javascript:;" onclick="return false"> 
<span class="dbShop-ui-inner_text"><?php echo $this->lang->appointmentLine('next') ?></span>
<span class="dbShop-ui-CutomIcon dbShop-ui-NextIco_White"></span>
</a>         </div>

    <div style="clear:both"></div> 
   </div>  
   <?php $this->load->view("section/footer.php"); ?>
  </div>
 </body>
</html>
<?php 
	echo '<!-- '.$this->common->showBenchmark().' -->';
?>