<?php 
	include_once 'includePHPHeaders.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head>   
  <?php $this->load->view("section/header.php"); ?>
 </head> 
 <body> 
  <div id="wrapper"> 

	
	<div class="dbShop-ui-warning"><?php echo $this->lang->appointmentLine('service_is_unavailable') ?>
    </div>
	
   </noscript>  
   <?php $this->load->view("section/footer.php"); ?>
   <div class="dbShop-ui-clearboth"></div>
  </div>
 </body>
</html>
<?php 
	echo '<!-- '.$this->common->showBenchmark().' -->';
?>