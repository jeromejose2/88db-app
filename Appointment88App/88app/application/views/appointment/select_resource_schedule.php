<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head> 
	<?php $this->load->view("section/header.php"); ?>
	<link rel="stylesheet" type="text/css" href="<?php echo  $this->common->getVersionFilePath("css/front_end/jquery-ui-1.8.20.custom.css") ?>">
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery.ui.core.js") ?>"></script>
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery-ui-1.8.16.custom.min.js") ?>"></script>
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/date.js") ?>"></script>
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/front_end/resource_schedule.js") ?>"></script>
	<?php if ($this->lang->appointmentLine('datepicker_language') !== '') {?>
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery.datepicker.localize/jquery.ui.datepicker-".$this->lang->appointmentLine('datepicker_language').".min.js") ?>"></script>
	<?php } ?>
	<script type="text/javascript">
		global["ServiceId"]="<?php echo $this->input->get("serviceid"); ?>";
		global["ResourceId"]="<?php echo $this->input->get("resourceid"); ?>";
		global["SELECTERESOURCE_URL"] = "<?php echo site_url("appointment/selectResource") ?>";
		global["datepicker_dateformat"] =  "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('datepicker_dateformat')) ?>";
		global["CONTACTINFO_URL"] = "<?php echo site_url("appointment/contactInfo") ?>";
		global["please_select_schedule"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('please_select_schedule')) ?>";
		global["timelostfull_dialog_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('timelostfull_dialog_title')) ?>";
		global["timelostfull_dialog_content"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('timelostfull_dialog_content')) ?>";
		global["close"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('close')) ?>";
		global["nowDateTime"] = "<?php echo $this->common->local_date('', 'Y-m-d H:i:s') ?>";
		global["please_select_resouce"] = "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('please_select_resouce')) ?>";
		global["no_available_resource"] = "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('no_available_resource')) ?>";
	</script>
 </head> 
 <body> 
  <div id="wrapper"> 
   <div id="header"> 
    <ul id="headers">
      <?php echo $this->lang->appointmentLine('title_make_appointment') ?>
    </ul> 
	<?php $this->load->view("section/header_memberinfo.php"); ?>
   </div> 
   <?php $this->load->view("section/customer_header.php"); ?>
  <div class="ProgressBar">

<span class="step_one">
<a class="dbShop-ui-Progress_visited" href="<?php echo site_url("appointment/welcome") ?>"> 
<div class="dbShop-ui-Progress_step">1.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_select_service') ?></span>
<span class="dbShop-ui-Progress_detail"><?php print_r($service->Name) ?></span>
</div>
</a>
</span>

<span class="step_two">
<div class="dbShop-ui-Progress_active" href="#"> 
<div class="dbShop-ui-Progress_step">2.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_select_resource_schedule') ?></span>
<span class="dbShop-ui-Progress_detail"></span>
</div>
</div>
</span>

<span class="step_three">
<div class="dbShop-ui-Progress_normal" href="#"> 
<div class="dbShop-ui-Progress_step">3.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_contace_info') ?></span>
<span class="dbShop-ui-Progress_detail"></span>
</div>
</div>
</span>

<span class="step_four">
<div class="dbShop-ui-Progress_normal"> 
<div class="dbShop-ui-Progress_step">4.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_confirmation') ?></span>
<span class="dbShop-ui-Progress_detail"></span>
</div>
</div>
</span>

</div>

   <noscript> 
    <style type="text/css">
		#mcs_container .customScrollBox{overflow:auto;}
		#mcs_container .dragger_container{display:none;}
	</style> 
   </noscript> 
   
   <div id="dbShop-ui-mainRight"> 
   <div class="mousescroll" id="mcs_container"> 
    <div class="navTitle">
         <?php echo $this->lang->appointmentLine('tab_select_resource') ?>
        </div> 
    <div class="customScrollBox"> 
     <div class="container" id="nav-kategori" style="top: 0px;"> 
      <div class="content"> 
       <ul class="SelectorList"> 
       <li resourceid="" hasschema="true"> 
         <div class="SelectorList_container"> 
          <div class="SelectorList_radio"> 
           <input type="radio" class="btnSelectReource"> 
          </div> 
          <div class="SelectorList_text"> 
           <a class="btnSelectReource" href="javascript:void(0);">
			<?php echo $this->lang->appointmentLine('no_preference') ?>
		   </a> 
          </div> 
        </li>
		<?php for ( $i = 0; $i< count($resources); $i++ ): ?>
        <li  hasschema="<?php echo $resources[$i]->HasSchema === TRUE?"true":"false" ?>" resourceid="<?php echo  $this->guid->createFromBinary($resources[$i]->ResourceGuid) ?>"> 
         <div class="SelectorList_container"> 
          <div class="SelectorList_radio"> 
           <input type="radio" class="btnSelectReource" <?php echo $resources[$i]->HasSchema === TRUE?"":"disabled=\"disabled\"" ?> /> 
          </div> 
          <div class="SelectorList_text"> 
           <a href="javascript:void(0);" class="btnSelectReource <?php echo $resources[$i]->HasSchema === TRUE?"":"Off" ?>">
			<?php echo form_prep($resources[$i]->Name) ?> 
		   </a> 
          </div> 
          <span class="SelectorList_viewdetail viewdetail_gray btnViewRerouceDetail" style="display:<?php echo strlen($resources[$i]->Description) == 0 ? "none":"block"?>"> </span>
         </div> </li> 
		  <?php endfor; ?>		  
       </ul> 
      </div> 
     </div> 
     <div class="dragger_container" style="display: none;"> 
      <div class="dragger" style="top: 0px; display: none;"></div> 
     </div> 
    </div> 
    <a class="scrollUpBtn" href="#" style="display: none;"></a> 
    <a class="scrollDownBtn" href="#" style="display: none;"></a> 
   </div> 
  </div> 
  <div id="main2"> 
   <div class="calendar_block"> 
    <div style="padding:0 0 15px;" class="navTitle">
     <?php echo $this->lang->appointmentLine('tab_select_schedule') ?>
    </div> 
    <div id="calendar_container" style=""> 
     <div id="datepicker"></div> 
    </div> 
   </div> 
   <div class="time_table timeSlotLoading"> 
		<img src="../images/loading.gif" style="margin: 126px 0px 0px 145px;"/>
   </div>
   <div class="time_table noTimeSlot" style="display:none"> 
		<?php echo $this->lang->appointmentLine('current_day_no_timeslot') ?>
   </div>
   <div class="time_table timeSlotContainer" style="display:none">
    <div class="datetimeSum">--</div>
    <div class="resettime" style="visibility: hidden;">
     <span><?php echo $this->lang->appointmentLine('clear_selection'); ?></span>
    </div>
    <div class="item_selectdate"> 
     <h2><?php echo $this->lang->appointmentLine('am') ?></h2> 
     <div class="item_datetable AMTimeSlots"> 
      </div> 
     <div class="dbShop-ui-clearboth"></div> 
     <br /> 
     <h2><?php echo $this->lang->appointmentLine('pm') ?></h2> 
    <div class="item_datetable PMTimeSlots"> 
      </div> 
     <div class="dbShop-ui-clearboth"></div> 
    </div> 
   </div> 
   
   
   <div style="clear:both"></div> 
  </div> 
  <div style="width:970px; float:left; padding:0px 0px 60px 10px; " class="UI-Btn-Btm"> 
   <a class="dbShop-ui-SecondaryBtn dbShop-ui-FormBack" href="javascript:history.back();"> <span class="dbShop-ui-inner_text"><?php echo $this->lang->appointmentLine('back') ?></span> <span class="dbShop-ui-CutomIcon dbShop-ui-BackIco_White"></span> </a> 
   <a class="dbShop-ui-PrimaryBtn dbShop-ui-FormNext btnGotoContactInfo" href="javascript:;"> <span class="dbShop-ui-inner_text"><?php echo $this->lang->appointmentLine('next') ?></span> <span class="dbShop-ui-CutomIcon dbShop-ui-NextIco_White"></span> </a> 
  </div>
  
   <?php $this->load->view("section/footer.php"); ?>
  </div>
 </body>
</html>
<?php 
	echo '<!-- '.$this->common->showBenchmark().' -->';
?>