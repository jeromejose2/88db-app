<?php 
	include_once 'includePHPHeaders.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head> 
  <title></title> 
  <?php $this->load->view("section/header.php"); ?>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/date.js") ?>"></script>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/front_end/member.js") ?>"></script>
  <script type="text/javascript">
		global["yes"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('yes')) ?>";
		global["no"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('no')) ?>";
		global["schedule"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('schedule')) ?>";
		global["service"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('service')) ?>";
		global["resource"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('resource')) ?>";
		global["cancel_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('cancel_appointment')) ?>";
		global["cancel_appointment_to_sure"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('cancel_appointment_to_sure')) ?>";
		
	</script>
 </head> 
 <body> 
  <div id="wrapper"> 
   <div id="header"> 
    <ul id="headers">
      <?php echo $this->lang->appointmentLine('scheduled_appointment') ?>
    </ul> 
   </div> 
   <noscript> 
    <style type="text/css">
		#mcs_container .customScrollBox{overflow:auto;}
		#mcs_container .dragger_container{display:none;}
	</style> 
   </noscript> 
   <div id="main_full"> 
    <div class="tabs-info"> 
     <div class="scheduled-container"> 
      <div class="scheduled-header"> 
		<span class="ch01"><?php echo $this->lang->appointmentLine('service') ?></span>
		<span class="ch02"><?php echo $this->lang->appointmentLine('start') ?></span>
		<span class="ch03"><?php echo $this->lang->appointmentLine('end') ?></span>
		<span class="ch04"><?php echo $this->lang->appointmentLine('resource') ?></span>
		<span class="ch05"><?php echo $this->lang->appointmentLine('service_cost') . '('.APP88_CURRENCY.')' ?></span>
      </div> 
	  <?php if($bookings !== FALSE){
	  $row = 1;
	  foreach($bookings as $b):
	  ?>
      <div class="listcontent row<?php echo $row?>" guid="<?php echo $this->guid->createFromBinary($b->BookingGuid) ?>"> 
       <span class="cc01"><?php echo $b->ServiceName?></span> 
       <span class="cc02"><?php echo $b->StartDateTime?></span> 
	   <span class="cc03"><?php echo $b->EndDateTime?></span> 
       <span class="cc04"><?php echo $b->ResourceName?></span> 
	   <span class="cc05"><?php echo $this->common->number_format($b->Cost)?></span> 
	   <?php if( (strtotime(gmdate('Y-m-d H:i:s'))+60*60*APP88_TIMEZONE_OFFSET) < strtotime($b->StartDateTime)): ?>
	   <a  href="javascript:;" class="dbShop-ui-SecondaryBtn dbShop-ui-BtnText btnCancel" > 
		<div class="dbShop-ui-inner_text"><?php echo $this->lang->appointmentLine('cancel') ?></div></a>
		<?php endif;?>
      </div>       
	  <?php 
		$row++;
		if($row > 2)
		{
			$row = 1;
		}
		endforeach;
	  }?>
      <div class="dbShop-ui-clearboth"></div> 
     </div> 
     <div class="dbShop-ui-clearboth"></div> 
    
	 <div class="UI-Btn-Btm" style="padding-left:10px;margin-top:10px; width:auto;">	
<a href="<?php echo site_url("appointment/welcome") ?>" class="dbShop-ui-PrimaryBtn dbShop-ui-BtnText"> 
<span class="dbShop-ui-inner_text"><?php echo $this->lang->appointmentLine('btn_make_new_appointment') ?></span>
</a>
        </div>
		
     <div style="clear:both"></div> 
    </div> 
   </div>  
   <?php $this->load->view("section/footer.php"); ?>
  </div>
 </body>
</html>
<?php 
	echo '<!-- '.$this->common->showBenchmark().' -->';
?>