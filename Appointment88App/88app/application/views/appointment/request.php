<?php 
	include_once 'includePHPHeaders.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head> 
  <?php $this->load->view("section/header.php"); ?>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/date.js") ?>"></script>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/front_end/request.js") ?>"></script>
  <script type="text/javascript">
	global["dealdate"] = "<?php echo $dealDate ?>";
	global["serverduration"] = <?php echo $service[0]->Duration ?>;	
  </script>
 </head> 
 <body> 
  <div id="wrapper"> 
   <div id="header"> 
    <ul id="headers">
      <?php echo $this->lang->appointmentLine('almost_done_with_your_booking') ?>
    </ul> 
   </div> 
   <noscript> 
    <style type="text/css">
		#mcs_container .customScrollBox{overflow:auto;}
		#mcs_container .dragger_container{display:none;}
	</style> 
   </noscript> 
   <div id="main_full"> 
    <div class="tabs-info"> 
     <div class="form_warn">
      <div class="itemContent-des"> 
       <?php echo str_replace("{email}", $email, $this->lang->appointmentLine('almost_done_with_your_booking_description')); ?>
      </div>
     </div> 
     <div class="form_container_a"> 
      <div class="form_title">
       <?php echo $this->lang->appointmentLine('service') ?> : 
      </div> 
      <div class="form_info">
        <?php echo form_prep($service[0]->Name) ?>
      </div> 
     </div> 
     <div class="dbShop-ui-clearboth"></div> 
     <div class="form_container_b"> 
      <div class="form_title">
        <?php echo $this->lang->appointmentLine('resource') ?> : 
      </div> 
      <div class="form_info">
       <?php echo form_prep($resource[0]->Name) ?>
      </div> 
     </div> 
     <div class="dbShop-ui-clearboth"></div> 
     <div class="form_container_b"> 
      <div class="form_title">
       <?php echo $this->lang->appointmentLine('schedule') ?> : 
      </div> 
      <div class="form_info schedule">
       {0} <?php echo $this->lang->appointmentLine('to') ?> {1}
      </div> 
     </div> 
     <div class="dbShop-ui-clearboth"></div> 
     <div class="form_container_b"> 
      <div class="dbShop-ui-clearboth"></div> 
      <div class="dbShop-ui-clearboth"></div> 
	  
   <div style="padding-left:130px;margin-top:10px; width:auto;" class="UI-Btn-Btm">	
   <?php 
   preg_match("/@.*/i",$email,$match);
   if(count($match) == 1 ){
	   foreach($emailClientMapping as $key => $value) {
		   if ($key === $match[0])
		   {
			   echo '<a class="dbShop-ui-PrimaryBtn dbShop-ui-BtnText" href="'.$value.'" target="_blank" ><span class="dbShop-ui-inner_text">'.$this->lang->appointmentLine('open_your_email_inbox').'</span></a> ';
			   break;
		   }
	   }
   }
		?>   

 <a style="margin-left:10px;" class="dbShop-ui-SecondaryBtn dbShop-ui-BtnText" href="<?php echo site_url("appointment/welcome") ?>"> 
<span class="dbShop-ui-inner_text"><?php echo $this->lang->appointmentLine('btn_make_new_appointment') ?></span>
</a>         </div>


      <div style="clear:both"></div> 
     </div> 
    </div>  
    <?php $this->load->view("section/footer.php"); ?>
   </div>
   <div class="dbShop-ui-clearboth"></div>
  </div>
 </body>
</html>
<?php 
	echo '<!-- '.$this->common->showBenchmark().' -->';
?>