<?php 
	include_once 'includePHPHeaders.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head> 
<?php $this->load->view("section/header.php"); ?>
<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/date.js") ?>"></script>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/front_end/resource.js") ?>"></script>
  <script type="text/javascript">
	global["SELECTESCHEDULE_URL"] = "<?php echo site_url("appointment/selectSchedule") ?>";
	global["text_not_available"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('not_available')) ?>";
	global["text_timeslot_is_full"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('timeslot_is_full')) ?>";
	global["serviceInfo"] = {
		Duration: <?php echo $service->Duration ?>,
		TimeSlotDivision: <?php echo $service->TimeSlotDivision ?>,
		MinTime: <?php echo $service->MinTime ?>,
		AdvanceTime: <?php echo $service->AdvanceTime == NULL ? 0:$service->AdvanceTime ?>,
		AvailablePerTimeSlot:<?php echo $service->AvailablePerTimeSlot ?>
	};
	global["nowDateTime"] = "<?php echo $this->common->local_date('', 'Y-m-d H:i:s') ?>";
	</script>
 </head> 
 <body> 
  <div id="wrapper">   
   <div id="header"> 
    <ul id="headers">
      <?php echo $this->lang->appointmentLine('title_make_appointment') ?>
    </ul> 
	<?php $this->load->view("section/header_memberinfo.php"); ?>
   </div> 
<?php $this->load->view("section/customer_header.php"); ?>
<div class="ProgressBar">

<span class="step_one">
<a class="dbShop-ui-Progress_visited" href="<?php echo site_url("appointment/welcome") ?>"> 
<div class="dbShop-ui-Progress_step">1.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_select_service') ?></span>
<span class="dbShop-ui-Progress_detail"><?php print_r($service->Name) ?></span>
</div>
</a>
</span>

<span class="step_two">
<div class="dbShop-ui-Progress_active" href="#"> 
<div class="dbShop-ui-Progress_step">2.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_select_resource') ?></span>
<span class="dbShop-ui-Progress_detail"></span>
</div>
</div>
</span>

<span class="step_three">
<div class="dbShop-ui-Progress_normal" href="#"> 
<div class="dbShop-ui-Progress_step">3.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_select_schedule') ?></span>
<span class="dbShop-ui-Progress_detail"></span>
</div>
</div>
</span>

<span class="step_four">
<div class="dbShop-ui-Progress_normal" href="#"> 
<div class="dbShop-ui-Progress_step">4.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_contace_info') ?></span>
<span class="dbShop-ui-Progress_detail"></span>
</div>
</div>
</span>

<span class="step_five">
<div class="dbShop-ui-Progress_normal"> 
<div class="dbShop-ui-Progress_step">5.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_confirmation') ?></span>
<span class="dbShop-ui-Progress_detail"></span>
</div>
</div>
</span>

</div>   
   
   <noscript> 
    <style type="text/css">
		#mcs_container .customScrollBox{overflow:auto;}
		#mcs_container .dragger_container{display:none;}
	</style> 
   </noscript> 
   <div id="dbShop-ui-mainLeft"> 
    <div id="mcs_container" class="mousescroll"> 
     <div class="customScrollBox"> 
      <div style="top: 0px;" id="nav-kategori" class="container"> 
       <div class="content"> 
        <ul class="SelectorList"> 
		<div class="navTitle"><?php echo $this->lang->appointmentLine('tab_select_resource') ?></div>
		<?php for ( $i = 0; $i< count($resources); $i++ ): ?>
		<li>
			<div class="SelectorList_container">
				<div class="SelectorList_radio">
					<input type="radio" name="radiogroup">
				</div>
				<div class="SelectorList_text">
					<a href="javascript:;" hasschema="<?php echo $resources[$i]->HasSchema === TRUE?"true":"false" ?>" resourceid="<?php echo  $this->guid->createFromBinary($resources[$i]->ResourceGuid) ?>" ><?php echo form_prep($resources[$i]->Name) ?> <?php echo $resources[$i]->HasSchema === TRUE?"":"(".$this->lang->appointmentLine('not_available').")" ?></a>
				</div>
			</div>
        </li>         
		 <?php endfor; ?>
        </ul> 
       </div> 
      </div> 
      <div style="display: none;" class="dragger_container"> 
       <div style="top: 0px; display: none;" class="dragger"></div> 
      </div> 
     </div> 
     <a style="display: none;" href="#" class="scrollUpBtn"></a> 
     <a style="display: none;" href="#" class="scrollDownBtn"></a> 
    </div> 
   </div> 
   <div id="main"> 
	<div class="UI-Btn-Top">	
		<a  href="javascript:;" onclick="return false;" class="dbShop-ui-PrimaryBtn dbShop-ui-FormNext btnGotoSelectSchedule"> 
			<span class="dbShop-ui-inner_text"><?php echo $this->lang->appointmentLine('next') ?></span>
			<span class="dbShop-ui-CutomIcon dbShop-ui-NextIco_White"></span>
		</a>         
	</div>
    <div id="tabs-produk"> 
     <div class="detail-title"> 
      <h2>--</h2> 
     </div> 
     <div class="itemContent"> 
      <div class="itemContent-des"> 
       --
      </div> 
     </div> 
	 
	<div class="UI-Btn-Btm">	
		<a href="javascript:history.back();" class="dbShop-ui-SecondaryBtn dbShop-ui-FormBack"> 
			<span class="dbShop-ui-inner_text"><?php echo $this->lang->appointmentLine('back') ?></span>
			<span class="dbShop-ui-CutomIcon dbShop-ui-BackIco_White"></span>
		</a>
		<a class="dbShop-ui-PrimaryBtn dbShop-ui-FormNext btnGotoSelectSchedule" href="javascript:;" onclick="return false;"> 
			<span class="dbShop-ui-inner_text"><?php echo $this->lang->appointmentLine('next') ?></span>
			<span class="dbShop-ui-CutomIcon dbShop-ui-NextIco_White"></span>
		</a>         
	</div>
	
    </div>  
    <div style="clear:both"></div> 
   </div>  
  <?php $this->load->view("section/footer.php"); ?>
  </div>
 </body>
</html>
<?php 
	echo '<!-- '.$this->common->showBenchmark().' -->';
?>