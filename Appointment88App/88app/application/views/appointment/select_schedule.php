<?php 
	include_once 'includePHPHeaders.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head> 
	<?php $this->load->view("section/header.php"); ?>
	<link rel="stylesheet" type="text/css" href="<?php echo  $this->common->getVersionFilePath("css/front_end/jquery-ui-1.8.20.custom.css") ?>">
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery.ui.core.js") ?>"></script>
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery-ui-1.8.16.custom.min.js") ?>"></script>
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/date.js") ?>"></script>
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/front_end/schedule.js") ?>"></script>
	<?php if ($this->lang->appointmentLine('datepicker_language') !== '') {?>
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery.datepicker.localize/jquery.ui.datepicker-".$this->lang->appointmentLine('datepicker_language').".min.js") ?>"></script>
	<?php } ?>
	<script type="text/javascript">
		global["ServiceId"]="<?php echo $this->input->get("serviceid"); ?>";
		global["ResourceId"]="<?php echo $this->input->get("resourceid"); ?>";
		global["SELECTERESOURCE_URL"] = "<?php echo site_url("appointment/selectResource") ?>";
		global["datepicker_dateformat"] =  "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('datepicker_dateformat')) ?>";
		global["CONTACTINFO_URL"] = "<?php echo site_url("appointment/contactInfo") ?>";
		global["please_select_schedule"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('please_select_schedule')) ?>";
		global["timelostfull_dialog_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('timelostfull_dialog_title')) ?>";
		global["timelostfull_dialog_content"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('timelostfull_dialog_content')) ?>";
		global["close"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('close')) ?>";
		global["nowDateTime"] = "<?php echo $this->common->local_date('', 'Y-m-d H:i:s') ?>";
	</script>
 </head> 
 <body> 
  <div id="wrapper"> 
   <div id="header"> 
    <ul id="headers">
      <?php echo $this->lang->appointmentLine('title_make_appointment') ?>
    </ul> 
	<?php $this->load->view("section/header_memberinfo.php"); ?>
   </div> 
   <?php $this->load->view("section/customer_header.php"); ?>
  <div class="ProgressBar">

<span class="step_one">
<a class="dbShop-ui-Progress_visited" href="<?php echo site_url("appointment/welcome") ?>"> 
<div class="dbShop-ui-Progress_step">1.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_select_service') ?></span>
<span class="dbShop-ui-Progress_detail appServiceName">--</span>
</div>
</a>
</span>

<span class="step_two">
<a class="dbShop-ui-Progress_visited gotoStep2"> 
<div class="dbShop-ui-Progress_step">2.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_select_resource') ?></span>
<span class="dbShop-ui-Progress_detail appResourceName">--</span>
</div>
</a>
</span>

<span class="step_three">
<div class="dbShop-ui-Progress_active" href="#"> 
<div class="dbShop-ui-Progress_step">3.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_select_schedule') ?></span>
<span class="dbShop-ui-Progress_detail"></span>
</div>
</div>
</span>

<span class="step_four">
<div class="dbShop-ui-Progress_normal" href="#"> 
<div class="dbShop-ui-Progress_step">4.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_contace_info') ?></span>
<span class="dbShop-ui-Progress_detail"></span>
</div>
</div>
</span>

<span class="step_five">
<div class="dbShop-ui-Progress_normal"> 
<div class="dbShop-ui-Progress_step">5.</div>
<div class="dbShop-ui-title_container">
<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_confirmation') ?></span>
<span class="dbShop-ui-Progress_detail"></span>
</div>
</div>
</span>

</div>

   <noscript> 
    <style type="text/css">
		#mcs_container .customScrollBox{overflow:auto;}
		#mcs_container .dragger_container{display:none;}
	</style> 
   </noscript> 
   <div id="dbShop-ui-mainLeft2"> 
   <div class="navTitle"><?php echo $this->lang->appointmentLine('tab_select_schedule') ?></div>
    <div id="calendar_container" style="padding-left:10px"> 
     <div id="datepicker"></div> 
    </div> 
   </div> 
   <div id="main2"> 
    <div id="tabs-produk"> 
     <div class="detail-title"> 
      <h2>--</h2> 
     </div> 
     <div class="item_selectdate"> 
      <h2><?php echo $this->lang->appointmentLine('am') ?></h2> 
      <div class="item_datetable AMTimeSlots"> 
      </div> 
      <div class="dbShop-ui-clearboth"></div> 
      <br /> 
      <h2><?php echo $this->lang->appointmentLine('pm') ?></h2> 
      <div class="item_datetable PMTimeSlots"> 
      </div> 
      <div class="dbShop-ui-clearboth"></div> 
     </div> 
     <div class="dbShop-ui-clearboth"></div> 

	 
	 <div class="UI-Btn-Btm">	
<a href="javascript:history.back();" class="dbShop-ui-SecondaryBtn dbShop-ui-FormBack"> 
<span class="dbShop-ui-inner_text"><?php echo $this->lang->appointmentLine('back') ?></span>
<span class="dbShop-ui-CutomIcon dbShop-ui-BackIco_White"></span>
</a>
 <a href="javascript:;" onclick="return false;" class="dbShop-ui-PrimaryBtn dbShop-ui-FormNext btnGotoContactInfo"> 
<span class="dbShop-ui-inner_text"><?php echo $this->lang->appointmentLine('next') ?></span>
<span class="dbShop-ui-CutomIcon dbShop-ui-NextIco_White"></span>
</a>         </div>

    </div> 
    <div style="clear:both"></div> 
   </div>  
   <?php $this->load->view("section/footer.php"); ?>
  </div>
 </body>
</html>
<?php 
	echo '<!-- '.$this->common->showBenchmark().' -->';
?>