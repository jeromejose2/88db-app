<?php 
	include_once 'includePHPHeaders.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head> 
  <?php $this->load->view("section/header.php"); ?>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/date.js") ?>"></script>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/front_end/complete.js") ?>"></script>
  <script type="text/javascript">
	global["dealdate"] = "<?php echo $dealDate ?>";
	global["serverduration"] = <?php echo $service[0]->Duration ?>;		
  </script>
 </head> 
 <body> 
  <div id="wrapper"> 
   <div id="header"> 
    <ul id="headers">
      <?php echo $this->lang->appointmentLine('made_successfully') ?>
    </ul> 
	<?php $this->load->view("section/header_memberinfo.php"); ?>	
   </div> <!--end hreader-->
   <noscript> 
    <style type="text/css">
		#mcs_container .customScrollBox{overflow:auto;}
		#mcs_container .dragger_container{display:none;}
	</style> 
   </noscript> 
   <div class="dbShop-ui-clearboth"></div> 
   <div id="main_full"> 
    <div class="tabs-info"> 
     <div class="form_container_a"> 
      <div class="form_title">
       <?php echo $this->lang->appointmentLine('service') ?> :
      </div> 
      <div class="form_info">
       <?php echo form_prep($service[0]->Name) ?>
      </div> 
     </div> 
     <div class="dbShop-ui-clearboth"></div> 
     <div class="form_container_b"> 
      <div class="form_title">
       <?php echo $this->lang->appointmentLine('resource') ?> :
      </div> 
      <div class="form_info">
       <?php echo form_prep($resource[0]->Name) ?>
      </div> 
     </div> 
     <div class="dbShop-ui-clearboth"></div> 
     <div class="form_container_b"> 
      <div class="form_title">
       <?php echo $this->lang->appointmentLine('schedule') ?> :
      </div> 
      <div class="form_info schedule">
        {0} <?php echo $this->lang->appointmentLine('to') ?> {1}
      </div> 
     </div> 
     <div class="dbShop-ui-clearboth"></div> 
	 
   <div class="UI-Btn-Btm" style="padding-left:130px;margin-top:10px; width:auto;">	
   <?php if($isLogin) { ?>
	<a href="<?php echo site_url("appointment/member") ?>" class="dbShop-ui-PrimaryBtn dbShop-ui-BtnText"> 
	<span class="dbShop-ui-inner_text"><?php echo $this->lang->appointmentLine('btn_view_booking_detail') ?></span>
	</a>
	<?php } ?>
	 <a href="<?php echo site_url("appointment/welcome") ?>" class="dbShop-ui-SecondaryBtn dbShop-ui-BtnText" style="margin-left:10px;"> 
	<span class="dbShop-ui-inner_text"><?php echo $this->lang->appointmentLine('btn_make_new_appointment') ?></span>
	</a>         </div>

    
   </div>  
   
   <?php $this->load->view("section/footer.php"); ?>
   <div class="dbShop-ui-clearboth"></div> 
  </div><div class="dbShop-ui-clearboth"></div> 
 </body>
</html>
<?php 
	echo '<!-- '.$this->common->showBenchmark().' -->';
?>