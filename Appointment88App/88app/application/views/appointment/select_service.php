<?php 
	include_once 'includePHPHeaders.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head> 
  <?php $this->load->view("section/header.php"); ?>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery.ui.core.js") ?>"></script>
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery-ui-1.8.16.custom.min.js") ?>"></script>
  <script type="text/javascript" src="<?php echo $this->common->getVersionFilePath("js/front_end/service.js"); ?>"></script>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/date.js") ?>"></script>
  <script type="text/javascript">
	global["APP88_DURATION_FORMAT"]="<?php echo APP88_DURATION_FORMAT ?>";
	global["DURATION_FORMAT"]="<?php echo $this->lang->appointmentLine('duration_format'); ?>";
	global["SELECTERESOURCE_URL"] = "<?php echo site_url("appointment/selectResource") ?>";
	global["APP88_MONEY_FORMAT"] = "<?php echo APP88_MONEY_FORMAT; ?>";
	global["nowDateTime"] = "<?php echo $this->common->local_date('', 'Y-m-d H:i:s') ?>";	
	global["not_available"] = "<?php echo $this->lang->appointmentLine('not_available') ?>";
	</script>
 </head> 
 <body style="display:none">  
 <div id="datepicker"></div> 
  <div id="wrapper"> 
   <div id="header"> 
    <ul id="headers">
      <?php echo $this->lang->appointmentLine('title_make_appointment') ?>
    </ul> 
	<?php $this->load->view("section/header_memberinfo.php"); ?>
   </div> 
   <?php $this->load->view("section/customer_header.php"); ?>

<div class="ProgressBar">

	<span class="step_one">
	<div  class="dbShop-ui-Progress_active"> 
		<div class="dbShop-ui-Progress_step">1.</div>
		<div class="dbShop-ui-title_container">
			<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_select_service') ?></span>
			<span class="dbShop-ui-Progress_detail"></span>
		</div>
	</div>
	</span>

	<span class="step_two">
	<div href="#" class="dbShop-ui-Progress_normal"> 
		<div class="dbShop-ui-Progress_step">2.</div>
		<div class="dbShop-ui-title_container">
			<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_select_resource_schedule') ?></span>
			<span class="dbShop-ui-Progress_detail"></span>
		</div>
	</div>
	</span>

	<span class="step_three">
	<div  href="#" class="dbShop-ui-Progress_normal"> 
		<div class="dbShop-ui-Progress_step">3.</div>
		<div class="dbShop-ui-title_container">
			<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_contace_info') ?></span>
			<span class="dbShop-ui-Progress_detail"></span>
		</div>
	</div>
	</span>

	<span class="step_four">
	<div class="dbShop-ui-Progress_normal"> 
		<div class="dbShop-ui-Progress_step">4.</div>
		<div class="dbShop-ui-title_container">
			<span class="dbShop-ui-Progress_title"><?php echo $this->lang->appointmentLine('tab_confirmation') ?></span>
			<span class="dbShop-ui-Progress_detail"></span>
		</div>
	</div>
	</span>

</div>

   
   <noscript> 
    <style type="text/css">
		#mcs_container .customScrollBox{overflow:auto;}
		#mcs_container .dragger_container{display:none;}
	</style> 
   </noscript> 
   <div id="dbShop-ui-mainLeft"> 
    <div id="mcs_container" class="mousescroll"> 
     <div class="customScrollBox"> 
      <div style="top: 0px;" id="nav-kategori" class="container"> 
       <div class="content"> 
        <ul class="SelectorList"> 
		<div class="navTitle"><?php echo $this->lang->appointmentLine('tab_select_service') ?></div>
		<?php for ( $i = 0; $i< count($services); $i++ ): ?>
		<li>
			<div class="SelectorList_container" >
				<div class="SelectorList_radio" >
					<input type="radio" name="radiogroup">
				</div>
				<div class="SelectorList_text">
					<a href="javascript:;" resouceids="<?php echo $services[$i]->Status === 'A' ? join(",", $services[$i]->resources) : ""; ?>" status="<?php echo form_prep($services[$i]->Status); ?>" serviceid="<?php echo $this->guid->createFromBinary($services[$i]->ServiceGuid); ?>"><?php echo form_prep($services[$i]->Name); ?></a>					
				</div>
			</div>    
		</li>
		 <?php endfor; ?>
        </ul> 
       </div> 
      </div> 
      <div style="display: none;" class="dragger_container"> 
       <div style="top: 0px; display: none;" class="dragger"></div> 
      </div> 
     </div> 
     <a style="display: none;" href="#" class="scrollUpBtn"></a> 
     <a style="display: none;" href="#" class="scrollDownBtn"></a> 
    </div> 
   </div> 
   <div id="main">
    <div id="tabs-produk"> 
     <div class="detail-title"> 
      <h2> -- <label class="service_notavailable" style="display: none;">(<?php echo $this->lang->appointmentLine('not_available'); ?>)</label></h2> 
     </div> 
	 <div class="addonbox01">
		 <div class="duration">
		  <label class="title-tabs"><?php echo $this->lang->appointmentLine('service_duration') ?>: </label><span>--</span>
		 </div> 
		 <div class="cost">
		  <label class="title-tabs"><?php echo $this->lang->appointmentLine('service_cost') ?>: </label><span>--</span>
		 </div> 
		<div class="addonbox02">
			<span class="loadingNextButton" style="display:none;background-image: url('../images/loading2.gif'); width: 16px; height: 16px; position: absolute; z-index: 2; top: 7px; right: 33px;"></span>
			<a class="dbShop-ui-PrimaryBtn dbShop-ui-FormNext btnGotoSelectResource" href="javascript:;" onclick="return false" style="display: none;"> 
				<span class="dbShop-ui-inner_text"><?php echo $this->lang->appointmentLine('select') ?></span>
				<span class="dbShop-ui-CutomIcon dbShop-ui-NextIco_White"></span>
			</a>         
		</div>
	</div>
     <div style="clear:both"></div> 
     <div class="itemContent"> 
      <div class="itemContent-des"> 
       --
      </div> 
     </div> 
     <div class="dbShop-ui-clearboth"></div> 
	 <div class="UI-Btn-Btm">	
		<span class="loadingNextButton" style="display:none;background-image: url('../images/loading2.gif'); width: 16px; height: 16px; position: absolute; z-index: 2; top: 16px; right: 33px;"></span>
		<a class="dbShop-ui-PrimaryBtn dbShop-ui-FormNext btnGotoSelectResource" href="javascript:;" onclick="return false" style="display: none;"> 
			<span class="dbShop-ui-inner_text"><?php echo $this->lang->appointmentLine('select') ?></span>
			<span class="dbShop-ui-CutomIcon dbShop-ui-NextIco_White"></span>
		</a>         
	</div>
    
    </div> 
    <div style="clear:both"></div> 
   </div>  
	<?php $this->load->view("section/footer.php"); ?>
  </div>
 </body>
</html>
<?php 
	echo '<!-- '.$this->common->showBenchmark().' -->';
?>