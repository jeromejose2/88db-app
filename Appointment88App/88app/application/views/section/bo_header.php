    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
    <title></title> 
    <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery-1.7.2.min.js") ?>"></script>	
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery.url.min.js") ?>"></script>	
	
    <!-- CSS Link --> 
    <link rel="stylesheet" type="text/css" href="<?php echo $this->common->getVersionFilePath("css/back_end/backend.css") ?>" /> 
    <link rel="stylesheet" type="text/css" href="<?php echo $this->common->getVersionFilePath("css/buttons.css") ?>" /> 
	
    <!-- Javascript Link --> 
    <script type="text/javascript" src="<?php echo  $this->app88param->getCommonJs() ?>"></script>
    <script type="text/javascript">
	var global={};
	global["cid"]="<?php echo $this->app88param->get88AppGuid()?>";
	global["appid"]="<?php echo $this->app88param->getApplicationGuid()?>";
	global["dateFormat"]="<?php echo APP88_DATE_FORMAT_JS ?>";
	global["monthDateFormat"]="<?php echo APP88_MONTH_FORMAT_JS ?>";
	global["dayDateFormat"]="<?php echo APP88_DAY_FORMAT_JS ?>";
	global["pageSize"]=<?php echo APP88_PAGESIZE ?>;
	global["singleName"] = "<?php echo APP88_SINGLE_NAME_FIELD ?>";
	global["timeFormat"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('time_format')) ?>";
	global["day_collection"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('day_collection')) ?>";
	global["ok"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('ok')) ?>";
	global["confirm"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('confirm')) ?>";
	global["save"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('btn_save')) ?>";
	global["cancel"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('cancel')) ?>";
	global["send"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('send')) ?>";
	global["close"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('close')) ?>";
	global["delete"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('delete')) ?>";
	global["dialog_system_error_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('dialog_system_error_title')) ?>";
	global["dialog_system_error_content"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('dialog_system_error_content')) ?>";
	global["text_please_enter_valid_value"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('please_enter_valid_value')) ?>";
	global["text_loading"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('loading')) ?>";
	global["text_not_available"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('not_available')) ?>";
	global["lang"] = "<?php echo  $this->app88param->getLang()?$this->app88param->getLang():APP88_ADMIN_LANGUAGE; ?>";
	$(function(){
		$("#head-navigation #navigation a."+global.pageName).removeClass(global.pageName).addClass(global.pageName+"On");
		//$(".HelpIco").helper();
	});
	</script>
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/appointment.js") ?>"></script>    
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/back_end/bo.js") ?>"></script>