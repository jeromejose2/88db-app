   <div id="header"> 
    <div id="head-navigation" class="clearfix"> 
     <ul id="navigation" class="clearfix">
	  <li class="catalogue-menu"> <a class="calender" href="<?php echo site_url("admin/calendar/month") ?>"><?php echo $this->lang->appointmentLine('calender');?></a> </li> 
      <li class="catalogue-menu"> <a class="customer" href="<?php echo site_url("admin/customer/index") ?>"><?php echo $this->lang->appointmentLine('customer');?></a> </li> 
      <li class="catalogue-menu"> <a class="service" href="<?php echo site_url("admin/service/servicelist") ?>"><?php echo $this->lang->appointmentLine('service');?></a> </li> 
      <li class="catalogue-menu"> <a class="resource" href="<?php echo site_url("admin/resources/resourcelist") ?>"><?php echo $this->lang->appointmentLine('resource');?></a> </li> 
	  <li class="catalogue-menu"> <a href="<?php echo site_url("admin/greeting/index") ?>" class="greeting"><?php echo $this->lang->appointmentLine('greeting');?></a> </li>
		<li class="catalogue-menu"> <a class="settings" href="<?php echo site_url("admin/setting/index") ?>"><?php echo $this->lang->appointmentLine('settings');?></a> </li> 
     </ul> 
    </div> 
   </div>