	<title></title> 
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
	<!-- CSS Link --> 
	<link rel="stylesheet" type="text/css" href="<?php echo $this->common->getVersionFilePath("css/front_end/appointment.css") ?>" /> 
	<link rel="stylesheet" type="text/css" href="<?php echo $this->common->getVersionFilePath("css/buttons.css") ?>" /> 
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery-1.7.2.min.js") ?>"></script>	
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery.url.min.js") ?>"></script>	
	<script type="text/javascript" src="<?php echo  $this->app88param->getCommonJs() ?>"></script>
	<script type="text/javascript">
	var global={};
	global["cid"]="<?php echo $this->app88param->get88AppGuid()?>";
	global["appid"]="<?php echo $this->app88param->getApplicationGuid()?>";
	global["dateFormat"]="<?php echo APP88_DATE_FORMAT_JS ?>";
	global["singleName"] = "<?php echo APP88_SINGLE_NAME_FIELD ?>";
	global["timeFormat"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('time_format')) ?>";
	global["day_collection"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('day_collection')) ?>";
	global["ok"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('ok')) ?>";
	global["confirm"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('confirm')) ?>";
	global["cancel"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('cancel')) ?>";
	global["islogin"] = <?php echo $isLogin ? "true":"false" ?>;
	$(function(){		
		$(".btnLogin").click(function(){
			appointment.popupLoginDialog();
		});
		$(".btnLogout").click(function(){
			appointment.logout(function(){
				location.href = "<?php echo site_url("appointment/welcome") ?>";
			});
		});	
		shop88api.getCurrentMemberStatus(function (result) {
            if (result && !global["islogin"]) {            
                location.href = "/appointment/authen?cid=" + global.cid;
            } else if (!result && global["islogin"]) {
                appointment.logout(function(){
					location.href = "<?php echo site_url("appointment/welcome") ?>";
				});
            }else {
                $("body").show();
            }
        });
	});
	window.App88EventHandler = {
		onLoginStatusChange: function (loggedIn) {
			if (loggedIn) {
				location.href = "/appointment/authen?cid=" + global.cid;
			} else {
				appointment.logout(function(){
					location.href = "<?php echo site_url("appointment/welcome") ?>";
				});
			}
		},
		onLikeStatusChange: function (likeData) { }
	};
	</script>
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/appointment.js") ?>"></script>