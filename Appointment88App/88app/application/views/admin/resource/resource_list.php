<?php 
	include_once 'includePHPHeaders.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head> 
  <?php $this->load->view("section/bo_header.php"); ?>
  <link rel="stylesheet" type="text/css" href="<?php echo  $this->common->getVersionFilePath("css/front_end/jquery-ui-1.8.20.custom.css") ?>">
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery.ui.core.js") ?>"></script>
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery-ui-1.8.16.custom.min.js") ?>"></script>
	<script src="<?php echo  $this->common->getVersionFilePath("js/jquery.minicolors.js") ?>"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->common->getVersionFilePath("css/jquery.minicolors.css") ?>" /> 
  <script type="text/javascript">
		global["dialog_delete_service_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('dialog_delete_service_title')) ?>";
		global["dialog_delete_service_content"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('dialog_delete_service_content')) ?>";	
		global["day_collection"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('day_collection')) ?>";
		global["text_am"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('am')) ?>";
		global["text_pm"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('pm')) ?>";
		global["text_no_end_date"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('no_end_date')) ?>";
		global["save_changes_dialog_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('save_changes_dialog_title')) ?>";
		global["save_changes_dialog_content"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('save_changes_dialog_content')) ?>";
		global["discard_changes_dialog_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('discard_changes_dialog_title')) ?>";
		global["discard_changes_dialog_content"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('discard_changes_dialog_content')) ?>";
		
		global["delete_normal_hour_dialog_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('delete_normal_hour_dialog_title')) ?>";
		global["delete_normal_hour_dialog_content"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('delete_normal_hour_dialog_content')) ?>";
		global["delete_special_hour_dialog_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('delete_special_hour_dialog_title')) ?>";
		global["delete_special_hour_dialog_content"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('delete_special_hour_dialog_content')) ?>";
		global["delete_day_off_dialog_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('delete_day_off_dialog_title')) ?>";
		global["delete_day_off_dialog_content"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('delete_day_off_dialog_content')) ?>";
		global["delete_resource_dialog_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('delete_resource_dialog_title')) ?>";
		global["delete_resource_dialog_content"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('delete_resource_dialog_content')) ?>";
		global["save_resource_dialog_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('save_resource_dialog_title')) ?>";
		global["save_resource_dialog_content"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('save_resource_dialog_content')) ?>";
		global["text_resource_working_hours_settings_tips"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('resource_working_hours_settings_tips')) ?>";
		global["text_resource_working_hours_settings_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('resource_working_hours_settings_title')) ?>";
		
		global["text_discard"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('discard')) ?>";
		global.pageName ="resource";
  </script>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/date.js") ?>"></script>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/back_end/resourcelist.js") ?>"></script>  
  <script src="<?php echo  $this->common->getVersionFilePath("js/tinymce/jscripts/tiny_mce/tiny_mce.js") ?>"></script>
	<script>
        tinymce.init({
            selector:'textarea',
            <?php
    			$lang_mapping = $this->config->item('admin_setting_tinycme_lang');
    			echo 'language:"'.$lang_mapping[$this->app88common->get_shop_lang()].'",';
    		?>
			plugins : "table,spellchecker",
			width:550,
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough, link, unlink,bullist,numlist,separator, justifyleft,justifycenter,justifyright, justifyfull,fontselect,fontsizeselect,forecolor,cleanup,hr",
			theme_advanced_buttons2 : "cut,copy,paste,separator,indent,outdent,blockquote,redo,tablecontrols,spellchecker,sup,sub,image",
			theme_advanced_buttons3 : ""});
	</script>
 </head> 
 </head>  
 <body>  
  <div id="wrapper"> 
   <?php $this->load->view("section/bo_header_bar.php"); ?>
   <div id="main-content"> 
    <div class="pageTitle"> 
     <h2 class="calender"><?php echo $this->lang->appointmentLine('resource');?></h2> 
    </div> 
    <div class="dbShop-ui-mainLeft"> 
     <div class="list-container"> 
      <div class="list-search">
       <?php echo $this->lang->appointmentLine('resource');?>       
	   <span style="margin-left:5px;" class="arrowSort_des" id="btnOrder"></span>
      </div> 
      <div class="listScrollBox_B"> 
	  <?php foreach($resourceNames as $name): ?>
       <li><input type="radio" value="1" name="advance"><a onclick="return false;" href="javascript:;" resourceid="<?php echo $this->guid->createFromBinary($name->Guid)->toString() ?>" class="btnViewDetail scrollList"><?php echo form_prep($name->Name);?></a> </li> 
	    <?php endforeach; ?>
      </div> 
     </div> 
	 
	 <?php if((count($resourceNames) < APP88_RESOURCE_CREATION_LIMIT) || (APP88_RESOURCE_CREATION_LIMIT == -1)): ?>
     <div class="Action"> 
      <div class="pageAdd fl"> 
       <a onclick="return false;" class="btnAddResource dbShop-ui-appBtn dbShop-ui-addproduct" style="margin-top:10px" href="javascript:;"><?php echo $this->lang->appointmentLine('add_new_resource');?></a> 
      </div> 
      <div class="pageBlank"></div> 
     </div> 
	 <?php endif;?>
    </div> 
    <div class="dbShop-ui-mainRight"> 
     <div class="user-info-container"> 
      <div class="infoTitle">
       <?php echo $this->lang->appointmentLine('detail');?>
      </div> 
      <div class="button-panel" style="margin:15px 0px 15px 0px"> 
       <a onclick="return false;" href="javascript:void(0);"  onclick="return false"  class="btnSave dbShop-ui-appBtn dbShop-ui-save"><?php echo $this->lang->appointmentLine('save_all');?></a> 
       <a onclick="return false;" href="javascript:;" class="btnCancel dbShop-ui-appBtn dbShop-ui-cancel" style="margin-left:15px"><?php echo $this->lang->appointmentLine('cancel');?></a> 
	   <a onclick="return false;" href="javascript:;" class="btnDeleteResource dbShop-ui-appBtn dbShop-ui-delete_bo" style="margin-left:15px"><?php echo $this->lang->appointmentLine('delete');?></a>
      </div> 
      <div class="dbShop-ui-rel"> 
       <p class="ganti-nama" style="padding-bottom:20px;"> <label for="name"><span class="compulsory-s">*</span><?php echo $this->lang->appointmentLine('resource_name');?>:</label> <input name="name" class="common-validatebox" maxlength="60" data-options="required:true, missingMessage:'<?php echo $this->app88common->js_quote($this->lang->appointmentLine('please_enter_valid_value')) ?>'" id="name" placeholder="<?php echo $this->lang->appointmentLine('resource_name');?>" type="text" /> </p>
       <p style="margin-left:165px;color:#777"><?php echo $this->lang->appointmentLine('resource_name_must_not_60_chars');?></p> 
       <p></p> 
	   <p class="ganti-nama" style="position:relative; padding-bottom:20px;" >
				<label for="assignColor"><span class="compulsory-s">*</span><?php echo $this->lang->appointmentLine('assign_color');?>:</label>
                <input type="text" value="" id="assignColor" name="assignColor" size="35" style="padding:6.5px; width:80px;" class="minicolors common-validatebox" data-options="required:true,missingMessage:'<?php echo $this->app88common->js_quote($this->lang->appointmentLine('please_enter_valid_value')) ?>'">
                </p>
            
       <p class="ganti-nama" style="padding-bottom:20px;"> <label for="phoneNumber"><?php echo $this->lang->appointmentLine('phone_number');?>:</label> <input name="phoneNumber" size="35" class="common-validatebox" data-options="validType: 'phone', missingMessage:'<?php echo $this->app88common->js_quote($this->lang->appointmentLine('please_enter_valid_value')) ?>'" id="phoneNumber" placeholder="<?php echo $this->lang->appointmentLine('phone_number');?>" type="text" /> </p>
       <p></p> 
       <p class="ganti-nama" style="padding-bottom:20px;"> <label for="email"><?php echo $this->lang->appointmentLine('email_address');?>:</label> <input name="email" size="35" id="email"  type="text" class="common-validatebox" data-options="required:false, validType: 'email', missingMessage:'<?php echo $this->app88common->js_quote($this->lang->appointmentLine('please_enter_valid_value')) ?>'" /> </p>
       <p></p>
       <p></p> 
       <br /> 
       <p class="ganti-nama" style="padding-bottom:20px;"> <label for="description"><?php echo $this->lang->appointmentLine('description');?>:</label> <textarea id="description" name="description" rows="30" cols="50" style="height:300px;width: 600px"></textarea></p>
       <p></p> 
      </div> 
      <div class="dbShop-ui-clearboth"></div> 
     </div> 
     <div class="user-info-container providedService"> 
      <div class="infoTitle btnPrividedSericeTitle">
       <?php echo $this->lang->appointmentLine('provided_service');?>
      </div> 
	  <?php foreach($serviceNames as $name): ?>
       <div class="infoCheckbox"> 
       <div class="checkbox">
        <input type="checkbox" name="<?php echo $this->guid->createFromBinary($name->ServiceGuid)->toString() ?>" id="<?php echo $this->guid->createFromBinary($name->ServiceGuid)->toString() ?>" value="Yes" checked="" />
       </div><label for="<?php echo $this->guid->createFromBinary($name->ServiceGuid)->toString() ?>"><?php echo form_prep($name->Name) ?></label>
      </div> 
	   <?php endforeach; ?>
		
      
      
      <div class="dbShop-ui-clearboth"></div> 
     </div> 
     <div class="user-info-container"> 
      <div class="infoTitle">
       <?php echo $this->lang->appointmentLine('normal_hours');?>
	   <span style="" class="HelpIco"> <a class="help_img"><img width="16" height="16" alt="Help" src="../../images/back_end/info-icon.png"></a></span>
      </div> 
      <div class="scheduled-container normalHoursContainer"> 
       <div class="scheduled-header"> 
        <span class="ch_day"> <?php echo $this->lang->appointmentLine('week');?></span> 
        <span class="ch_start"> <?php echo $this->lang->appointmentLine('start_time');?></span> 
        <span class="ch_end"> <?php echo $this->lang->appointmentLine('end_time');?></span> 
       </div> 
      </div> 
      <div class="dbShop-ui-clearboth"></div> 
      <div class="Action"> 
       <div class="pageAdd fl"> 
        <a onclick="return false;" class="btnNormalHour dbShop-ui-appBtn dbShop-ui-addproduct" style="margin-top:10px" href="javascript:;"> <?php echo $this->lang->appointmentLine('add_available_hour');?></a> 
       </div> 
       <div class="pageBlank"> 
       </div> 
      </div> 
     </div> 
     <div class="user-info-container"> 
      <div class="infoTitle">
       <?php echo $this->lang->appointmentLine('special_hours');?>
	   <span style="" class="HelpIco"> <a class="help_img"><img width="16" height="16" alt="Help" src="../../images/back_end/info-icon.png"></a></span>
      </div> 
      <div class="scheduled-container specialHoursContainer"> 
       <div class="scheduled-header"> 
        <span class="ch_day"><?php echo $this->lang->appointmentLine('date');?></span> 
        <span class="ch_start"><?php echo $this->lang->appointmentLine('start_time');?></span> 
        <span class="ch_end"><?php echo $this->lang->appointmentLine('end_time');?></span> 
       </div>        
      </div> 
      <div class="dbShop-ui-clearboth"></div> 
      <div class="Action"> 
       <div class="pageAdd fl"> 
        <a onclick="return false;" class="btnAddSpecialHour dbShop-ui-appBtn dbShop-ui-addproduct" style="margin-top:10px" href="javascript:;"><?php echo $this->lang->appointmentLine('add_special_hour');?></a> 
       </div> 
       <div class="pageBlank"> 
       </div> 
      </div> 
     </div> 
     <div class="user-info-container"> 
      <div class="infoTitle">
       <?php echo $this->lang->appointmentLine('day_off');?>
	   <span style="" class="HelpIco"> <a class="help_img"><img width="16" height="16" alt="Help" src="../../images/back_end/info-icon.png"></a></span>
      </div> 
      <div class="scheduled-container dayoffContainer"> 
       <div class="scheduled-header"> 
        <span class="ch_day"><?php echo $this->lang->appointmentLine('start_date');?></span> 
        <span class="ch_start" style="width:500px"><?php echo $this->lang->appointmentLine('end_date');?></span> 
       </div>             
      </div> 
      <div class="dbShop-ui-clearboth"></div> 
      <div class="Action"> 
       <div class="pageAdd fl"> 
        <a onclick="return false;" class="btnAddDayoffDay dbShop-ui-appBtn dbShop-ui-addproduct" style="margin-top:10px" href="javascript:;"><?php echo $this->lang->appointmentLine('add_day_off');?></a> 
       </div> 
       <div class="pageBlank"> 
       </div> 
      </div> 
     </div> 
     <div class="button-panel" style="margin:15px 0px 15px 0px"> 
      <a onclick="return false;" href="javascript:void(0);" onclick="return false"  class="btnSave dbShop-ui-appBtn dbShop-ui-save"><?php echo $this->lang->appointmentLine('save_all');?></a> 
      <a onclick="return false;" href="javascript:;" class="btnCancel dbShop-ui-appBtn dbShop-ui-cancel" style="margin-left:15px"><?php echo $this->lang->appointmentLine('cancel');?></a> 
     </div> 
    </div> 
    <div class="dbShop-ui-clearboth"></div> 
   </div>    
   <div id="delete-confirm"> 
    <p></p> 
   </div> 
   <div id="alert-box"> 
    <p></p> 
   </div> 
  </div> 
  <?php $this->load->view("section/footer.php"); ?>
 </body>
</html>
<?php 
	echo '<!-- '.$this->common->showBenchmark().' -->';
?>