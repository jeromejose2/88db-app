<?php 
	include_once 'includePHPHeaders.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head> 
  <?php $this->load->view("section/bo_header.php"); ?>  
  <script type="text/javascript">
		global["text_day"] = "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('day')) ?>";
		global["text_hour"] = "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('hour')) ?>";
		global["text_mins"] = "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('mins')) ?>";
		global["save_changes_dialog_content"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('save_changes_dialog_content')) ?>";
		global["save_changes_dialog_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('save_changes_dialog_title')) ?>";
		global["save_changes_dialog_content"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('save_changes_dialog_content')) ?>";
		global["save_setting_dialog_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('save_setting_dialog_title')) ?>";
		global["save_setting_dialog_content"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('save_setting_dialog_content')) ?>";
		global.pageName ="greeting";
		defaultValue = { 
			EnableNotificationEmail: <?php echo($setting["enableNotificationEmail"] === 'T' ?"true":"false") ?>, 
			EnableReminderEmail: <?php echo($setting["enableReminderEmail"]  === 'T' ?"true":"false") ?>, 
			ReminderMinute: <?php echo $setting["reminderMinute"] == FALSE ? '0': $setting["reminderMinute"] ?> 
		};		
  </script>
  <script src="<?php echo  $this->common->getVersionFilePath("js/tinymce/jscripts/tiny_mce/tiny_mce.js") ?>"></script>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/back_end/greeting.js") ?>"></script>
  <script>
        tinymce.init({
			selector:'textarea',
			<?php
				$lang_mapping = $this->config->item('admin_setting_tinycme_lang');
				echo 'language:"'.$lang_mapping[$this->app88common->get_shop_lang()].'",';
			?>
			plugins : "table,spellchecker",
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough, link, unlink,bullist,numlist,separator, justifyleft,justifycenter,justifyright, justifyfull,fontselect,fontsizeselect,forecolor,cleanup,hr,cut,copy,paste,separator,indent,outdent,blockquote,redo,spellchecker,sup,sub,image",
			theme_advanced_buttons2 : "tablecontrols",
			theme_advanced_buttons3 : ""
		});
	</script>
 </head> 
 <body>    
  <div id="wrapper"> 
   <?php $this->load->view("section/bo_header_bar.php"); ?>
   <div id="main-content"> 
    <div class="pageTitle"> 
     <h2 class=""> <?php echo $this->lang->appointmentLine('greeting') ?> </h2> 
    </div> 
	<div style="clear:both"></div>

	<div class="pageAction" style="padding:0px;">
				<div class="button-panel" style="margin:0px 0px 15px 0px;">
			<a href="javascript:void(0);" onclick="return false;" class="dbShop-ui-appBtn dbShop-ui-save btnSave" onclick="submitNews()"><?php echo $this->lang->appointmentLine('btn_save') ?></a>
			<?php if (FALSE) { ?>
			<a href="javascript:history.back();" class="dbShop-ui-appBtn dbShop-ui-back" style="margin-left:15px"><?php echo $this->lang->appointmentLine('btn_back') ?></a>
			<?php } ?>
                 
		</div>
        <div style="clear:both"></div>
     <p style="padding-bottom:10px;"><?php echo $this->lang->appointmentLine('greeting_tips') ?></p>
	 <textarea id="greeting" name="greeting" rows="30" cols="120" style="height:400px;width: 965px"><?php echo form_prep($setting["greeting"]); ?></textarea>
         <div style="clear:both"></div>
	</div>
	<div style="clear:both"></div>
	<div style="margin:20px 0px 0px 0px" class="button-panel">
		<a class="dbShop-ui-appBtn dbShop-ui-save btnSave" href="javascript:void(0);" onclick="return false;"><?php echo $this->lang->appointmentLine('btn_save') ?></a>
		<?php if (FALSE) { ?>
		<a style="margin-left:15px" class="dbShop-ui-appBtn dbShop-ui-back" href="javascript:history.back();"><?php echo $this->lang->appointmentLine('btn_back') ?></a>
		<?php } ?>
	</div>

   </div> 
  </div>  
  <?php $this->load->view("section/footer.php"); ?>
 </body>
</html>
<?php 
	echo '<!-- '.$this->common->showBenchmark().' -->';
?>