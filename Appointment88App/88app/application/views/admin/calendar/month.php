<?php 
	include_once 'includePHPHeaders.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head> 
  <?php $this->load->view("section/bo_header.php"); ?>
  <link type="text/css" href="<?php echo $this->common->getVersionFilePath("css/back_end/fullcalendar.css") ?>" rel="stylesheet" /> 
  <link type="text/css" href="<?php echo $this->common->getVersionFilePath("css/back_end/fullcalendar.print.css") ?>" rel="stylesheet" media="print" /> 
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery.ui.core.js") ?>"></script> 
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery-ui-1.8.16.custom.min.js") ?>"></script> 
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/fullcalendar.min.js") ?>"></script>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/date.js") ?>"></script>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/back_end/calender.js") ?>"></script> 
  
  <link rel="stylesheet" type="text/css" href="<?php echo  $this->common->getVersionFilePath("css/front_end/jquery-ui-1.8.20.custom.css") ?>">
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery.ui.core.js") ?>"></script>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery-ui-1.8.16.custom.min.js") ?>"></script>
  <?php if ($this->lang->appointmentLine('datepicker_language') != '') {?>
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery.datepicker.localize/jquery.ui.datepicker-".$this->lang->appointmentLine('datepicker_language').".min.js") ?>"></script>
  <?php } ?>
	<script type="text/javascript">
		global["pageName"] = "calender";	
		
		global["text_schedule"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('schedule')) ?>";
		global["text_resource"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('resource')) ?>";
		global["text_service"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('service')) ?>";
		global["text_preview_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('preview_appointment')) ?>";
		global["text_update"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('update')) ?>";
		global["text_delete_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('delete_appointment')) ?>";
		global["text_edit_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('edit_appointment')) ?>";
		global["text_date"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('date')) ?>";
		global["text_avaliable_time"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('avaliable_time')) ?>";
		global["text_new_schedule"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('new_schedule')) ?>";
		global["text_no_schema"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('no_schema')) ?>";
		global["text_create_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('create_appointment')) ?>";
		
		global["text_phone"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('phone')) ?>";
		global["text_email"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('email')) ?>";
		global["text_time"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('time')) ?>";
		global["text_update_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('update_appointment')) ?>";
		global["text_source_of_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('source_of_appointment')) ?>";
		global["text_cancel_this_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('cancel_this_appointment')) ?>";
		global["text_appointment_cancelled"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('appointment_cancelled')) ?>";
		global["text_the_appointment_has_been_canceled_successfully"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('the_appointment_has_been_canceled_successfully')) ?>";
		global["text_reason_for_cancel_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('reason_for_cancel_title')) ?>";
		global["text_reason_for_cancel_message"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('reason_for_cancel_message')) ?>";
		global["text_first_name"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('first_name')) ?>";
		global["text_last_name"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('last_name')) ?>";
		global["text_invalid_phone"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('invalid_phone')) ?>";
		global["text_invalid_email"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('invalid_email')) ?>";
		global["text_source_phone"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('appointment_source_phone')) ?>";
		global["text_source_email"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('appointment_source_email')) ?>";
		global["text_source_walkin"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('appointment_source_walkin')) ?>";
		global["text_source_others"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('appointment_source_others')) ?>";
		global["text_typeLabel"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('customer_type')) ?>";
		global["text_existingCustomerLabel"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('existing_customer')) ?>";
		global["text_newCustomerLabel"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('new_customer')) ?>";
		global["text_appointment_created_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('appointment_created_title')) ?>";
		global["text_appointment_created_message"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('appointment_created_message')) ?>";
		global["text_confirm_update_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('confirm_update_appointment')) ?>";
		global["text_want_to_update_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('want_to_update_appointment')) ?>";
		global["text_confirm_create_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('confirm_create_appointment')) ?>";
		global["text_want_to_create_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('want_to_create_appointment')) ?>";		
		
		global["text_are_you_to_cancel_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('are_you_to_cancel_appointment')) ?>";
		global["text_leave_message_here"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('leave_message_here')) ?>";
		global["text_customer"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('customer')) ?>";
		global["text_cost"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('cost')) ?>";
		global["text_message"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('message')) ?>";
		global["text_select_resource"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('select_resource')) ?>";
		global["text_day_collection"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('day_collection')) ?>";
		global["text_month_collection"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('month_collection')) ?>";
		global["text_select_service"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('select_service')) ?>";
		global["nowDateTime"] = "<?php echo $this->common->local_date('', 'Y-m-d H:i:s') ?>";
	</script> 
	<style>
		.loading{
			height: 16px; 
			background-image: url("../../images/back_end/loading.gif"); 
			display: inline-block; 
			width: 16px; 
			background-position: center center;
		}
	</style>
 </head> 
 <body>
 <div id="wrapper"> 
   <?php $this->load->view("section/bo_header_bar.php"); ?>
   <div id="main-content"> 
    <div class="pageTitle"> 
     <h2 class="calender"><?php echo $this->lang->appointmentLine('calender') ?></h2> 
    </div> 
    <div class="page-tool"> 
      <div style="display:none"> 
       <input name="csrf_token_name" value="2ba5ea06ccd79eb1d7f78b1eb49ac99f" type="hidden" /> 
      </div>
	  
      <a href="#" onclick="return false" class="btnViewPrev dbShop-ui-appBtn dbShop-ui-apply-back"></a> 
      <a href="#" onclick="return false" class="btnViewNext dbShop-ui-appBtn dbShop-ui-apply-next" style="margin-left:5px;"></a> 
      <a href="#" onclick="return false" class="btnViewToday dbShop-ui-appBtn dbShop-ui-apply" style="margin-left:5px;"><?php echo $this->lang->appointmentLine('today') ?></a> 
      <div class="dbShop-ui-feedLinkSel_a">
       <select class="serviceFilter selectbox">
		<option value=""><?php echo $this->lang->appointmentLine('all') ?></option>
		<?php foreach($servicesNameList as $name):?>
		<option value="<?php echo  $this->guid->createFromBinary($name->ServiceGuid) ?>"><?php echo  form_prep($name->Name) ?></option>
		<?php endforeach;?>
	  </select>
      </div> 
      <div class="dbShop-ui-feedLinkSel_a">
       <select class="resourceFilter selectbox">
		<option value=""><?php echo $this->lang->appointmentLine('all') ?></option>
		<?php foreach($resourceNameList as $name):?>
		<option value="<?php echo  $this->guid->createFromBinary($name->Guid) ?>"><?php echo  form_prep($name->Name) ?></option>
		<?php endforeach;?>
	  </select>
		</select>
      </div> 
      <a href="#"  onclick="return false" style="margin-left:10px;" view="month" class="btnChangeView dbShop-ui-appBtn dbShop-ui-applyOn"><?php echo $this->lang->appointmentLine('month') ?></a> 
      <a href="#"  onclick="return false" view="agendaWeek" class="btnChangeView dbShop-ui-appBtn dbShop-ui-apply" style="margin-left:5px;"><?php echo $this->lang->appointmentLine('canlender_week') ?></a> 
      <a href="#"  onclick="return false" view="agendaDay" class="btnChangeView dbShop-ui-appBtn dbShop-ui-apply" style="margin-left:5px;"><?php echo $this->lang->appointmentLine('day') ?></a> 
      <div style="clear:both"></div> 
    </div> 
	<div style="float:right;">
           <a href="javascript:void(0)" class="btnCreateAppointment dbShop-ui-appBtn dbShop-ui-addapointment_bo" style="margin-left:15px; margin-top:2px;"><?php echo $this->lang->appointmentLine('create_new_appointment') ?></a>
           </div>
    <div class="dbShop-ui-clearboth"></div> 
    <div id="calendar"></div> 
   </div>    
  </div> 
  <?php $this->load->view("section/footer.php"); ?>
 </body>
</html>
<?php 
	echo '<!-- '.$this->common->showBenchmark().' -->';
?>