<?php 
	include_once 'includePHPHeaders.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head> 
  <?php $this->load->view("section/bo_header.php"); ?>  
  <script type="text/javascript">
		global["text_day"] = "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('day')) ?>";
		global["text_hour"] = "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('hour')) ?>";
		global["text_mins"] = "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('mins')) ?>";
		global["save_changes_dialog_content"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('save_changes_dialog_content')) ?>";
		global["save_changes_dialog_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('save_changes_dialog_title')) ?>";
		global["save_changes_dialog_content"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('save_changes_dialog_content')) ?>";
		global["save_setting_dialog_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('save_setting_dialog_title')) ?>";
		global["save_setting_dialog_content"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('save_setting_dialog_content')) ?>";
		
		global["defaultCustomizeTitles"]=<?php echo APP88_APPOINTMENT_CUSTOMIZE_TITLE ?>;
		global["defaultCustomizeTitles_fordefaultValue"]=<?php echo APP88_APPOINTMENT_CUSTOMIZE_TITLE ?>;
		
		global.pageName ="settings";
		defaultValue = { 
			EnableNotificationEmail: <?php echo($setting["enableNotificationEmail"] === 'T' ?"true":"false") ?>, 
			EnableReminderEmail: <?php echo($setting["enableReminderEmail"]  === 'T' ?"true":"false") ?>, 
			ReminderMinute: <?php echo $setting["reminderMinute"] == FALSE ? '0': $setting["reminderMinute"] ?> ,
			CustomizeTitles: $.extend(global["defaultCustomizeTitles_fordefaultValue"][global["lang"]], <?php echo $setting["customizeTitles"] ?> )
		};		
  </script>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/back_end/setting.js") ?>"></script>
 </head> 
 <body>    
  <div id="wrapper"> 
   <?php $this->load->view("section/bo_header_bar.php"); ?>
   <div id="main-content"> 
    <div class="pageTitle"> 
     <h2 class=""> <?php echo $this->lang->appointmentLine('settings') ?> </h2> 
    </div> 
    <form action="#" method="post" accept-charset="utf-8" id="newsForm" enctype="multipart/form-data">
     <div style="display:none"> 
      <input name="csrf_token_name" value="#" type="hidden" /> 
     </div> 
     <div class="pageAction" style="padding:10px 20px;*padding:0px 20px;"> 
      <div class="formLoader"> 
       <div class="dbShop-ui-rel"> 
        <div class="infoCheckbox" style="padding:0px 0px 20px 0px; width:100%"> 
         <div class="checkbox">
          <input type="checkbox" name="EnableNotificationEmail" id="EnableNotificationEmail" value="Yes" />
         </div><label for="EnableNotificationEmail"><?php echo $this->lang->appointmentLine('settings_send_notification_email_to_owner') ?></label>
        </div> 
        <div class="checkbox">
         <input type="checkbox" name="EnableReminderEmail" id="EnableReminderEmail" value="Yes" />
        </div><label for="EnableReminderEmail"><?php echo $this->lang->appointmentLine('settings_send_reminder_email_to_owner') ?></label>
       </div> 
       <select id="ReminderMinute_day" disabled="disabled" style="margin:10px 10px 5px 0px; width:100px;"></select> 
	   <?php echo $this->lang->appointmentLine('days') ?>
       <!--<select id="ReminderMinute_hour" disabled="disabled"  style="margin:10px 20px 5px 0px; width:100px;"></select> 
       <select id="ReminderMinute_min" disabled="disabled"  style="margin:10px 20px 5px 0px; width:100px;"></select> -->
	   <span style="padding-left:20px"><?php echo $this->lang->appointmentLine('settings_before_each_appointment') ?></span>
	   <div class="dbShop-ui-clearboth"></div>	
	   <p></p>
	   <p></p>
	   <div style="margin-left:-20px; margin-top:30px" class="pageTitle">
			<h2 class=""><?php echo $this->app88common->js_quote($this->lang->appointmentLine('customize_titles')) ?></h2>
			<p style="margin-left:-10px"><?php echo $this->app88common->js_quote($this->lang->appointmentLine('customize_titles_desc')) ?></p>
			<p></p>
		</div>
		<p></p>
		<div class="dbShop-ui-clearboth"></div>
		
		<div style="padding-left:10px" class="ganti-nama">
			<label for="title_appointment"><span class="compulsory-s">*</span><span class="labelAppointmentTitle">--</span> :</label>
            <span class="arrow_fbo"><img width="24" height="12" src="../../images/back_end/arrow_b.gif"></span>
			<input type="text" class="common-validatebox" data-options="required:true, missingMessage:'<?php echo $this->app88common->js_quote($this->lang->appointmentLine('please_enter_valid_value')) ?>'" maxlength="30" style="margin-left:50px" value="" id="title_appointment" size="35" name="title_appointment">
		</div>
		<p></p>
		<div class="dbShop-ui-clearboth"></div>
		
		<div style="padding-left:10px" class="ganti-nama">
			<label for="title_service"><span class="compulsory-s">*</span><span class="labelServiceTitle">--</span> :</label>
            <span class="arrow_fbo"><img width="24" height="12" src="../../images/back_end/arrow_b.gif"></span>
			<input type="text" class="common-validatebox" data-options="required:true, missingMessage:'<?php echo $this->app88common->js_quote($this->lang->appointmentLine('please_enter_valid_value')) ?>'" maxlength="30" style="margin-left:50px" value="" id="title_service" size="35" name="title_service">
		</div>
		<p></p>
		<div class="dbShop-ui-clearboth"></div>
		
		<div style="padding-left:10px" class="ganti-nama">
			<label for="title_resource"><span class="compulsory-s">*</span><span class="labelResourceTitle">--</span> :</label>
            <span class="arrow_fbo"><img width="24" height="12" src="../../images/back_end/arrow_b.gif"></span>
			<input type="text" class="common-validatebox" data-options="required:true, missingMessage:'<?php echo $this->app88common->js_quote($this->lang->appointmentLine('please_enter_valid_value')) ?>'" maxlength="30" style="margin-left:50px" value="" id="title_resource" size="35" name="title_resource">
		</div>
		<p></p>
		<div class="dbShop-ui-clearboth"></div>
		
		<div style="padding-left:10px" class="ganti-nama">
			<label for="title_schedule"><span class="compulsory-s">*</span><span class="labelScheduleTitle">--</span> :</label>
            <span class="arrow_fbo"><img width="24" height="12" src="../../images/back_end/arrow_b.gif"></span>
			<input type="text" class="common-validatebox" data-options="required:true, missingMessage:'<?php echo $this->app88common->js_quote($this->lang->appointmentLine('please_enter_valid_value')) ?>'" maxlength="30" style="margin-left:50px" value="" id="title_schedule" size="35" name="title_schedule">
		</div>
		<p></p>
		<div class="dbShop-ui-clearboth"></div>
		
      </div> 
	  
	  <div style="padding: 0px 0px 30px 0px" class="Action">
		<div class="pageAdd fl">
			<a href="javascript:void(0);" class="btnRetoreCustomizeTitle dbShop-ui-appBtn dbShop-ui-restore_bo" style="margin-left:225px"><?php echo $this->lang->appointmentLine('restore_wording') ?></a>
		</div>
		<div class="pageBlank"></div>
		</div>
	
      <div class="button-panel" style="margin:20px 0px 15px 0px;"> 
       <a onclick="return false;" href="#" class="btnSave dbShop-ui-appBtn dbShop-ui-save"><?php echo $this->lang->appointmentLine('btn_save') ?></a>
       <?php if (FALSE) { ?> 
       <a onclick="margin-bottom;" href="javascript:history.back();" class="dbShop-ui-appBtn dbShop-ui-back" style="margin-left:15px"><?php echo $this->lang->appointmentLine('btn_back') ?></a>
       <?php } ?> 
      </div> 
     </div>
    </form> 
    <div style="display: none;"> 
    </div> 
   </div> 
  
   <div id="delete-confirm"> 
    <p></p> 
   </div> 
   <div id="alert-box"> 
    <p></p> 
   </div> 
  </div>  
  <?php $this->load->view("section/footer.php"); ?>
 </body>
</html>
<?php 
	echo '<!-- '.$this->common->showBenchmark().' -->';
?>