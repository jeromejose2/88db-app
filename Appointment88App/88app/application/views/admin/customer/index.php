<?php 
	include_once 'includePHPHeaders.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head> 
  <?php $this->load->view("section/bo_header.php"); ?>  
  <link rel="stylesheet" type="text/css" href="<?php echo  $this->common->getVersionFilePath("css/front_end/jquery-ui-1.8.20.custom.css") ?>">
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery.ui.core.js") ?>"></script>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery-ui-1.8.16.custom.min.js") ?>"></script>
  <?php if ($this->lang->appointmentLine('datepicker_language') != '') {?>
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/jquery.datepicker.localize/jquery.ui.datepicker-".$this->lang->appointmentLine('datepicker_language').".min.js") ?>"></script>
  <?php } ?>
  <script type="text/javascript">
		global["text_delete"] = "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('delete')) ?>";
		global["text_hour"] = "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('hour')) ?>";
		global["text_mins"] = "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('mins')) ?>";
		global["text_delete_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('delete_appointment')) ?>";
		global["text_are_you_to_cancel_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('are_you_to_cancel_appointment')) ?>";
		global["text_reason_for_delete"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('reason_for_delete')) ?>";
		global["text_leave_message_here"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('leave_message_here')) ?>";
		global["text_customer"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('customer')) ?>";
		global["text_cancel"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('cancel')) ?>";
		global["text_edit"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('edit')) ?>";
		
		global["text_schedule"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('schedule')) ?>";
		global["text_resource"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('resource')) ?>";
		global["text_service"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('service')) ?>";
		
		global["text_update"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('update')) ?>";		
		global["text_edit_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('edit_appointment')) ?>";
		global["text_create_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('create_appointment')) ?>";
		global["text_date"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('date')) ?>";
		global["text_avaliable_time"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('avaliable_time')) ?>";
		global["text_new_schedule"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('new_schedule')) ?>";
		global["text_no_schema"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('no_schema')) ?>";
		global["text_select_service"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('select_service')) ?>";
		global["nowDateTime"] = "<?php echo $this->common->local_date('', 'Y-m-d H:i:s') ?>";
		global["text_select_resource"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('select_resource')) ?>";
		
		global["text_phone"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('phone')) ?>";
		global["text_email"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('email')) ?>";
		global["text_time"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('time')) ?>";
		global["text_update_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('update_appointment')) ?>";
		global["text_source_of_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('source_of_appointment')) ?>";
		global["text_cancel_this_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('cancel_this_appointment')) ?>";
		global["text_appointment_cancelled"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('appointment_cancelled')) ?>";
		global["text_the_appointment_has_been_canceled_successfully"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('the_appointment_has_been_canceled_successfully')) ?>";
		global["text_reason_for_cancel_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('reason_for_cancel_title')) ?>";
		global["text_reason_for_cancel_message"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('reason_for_cancel_message')) ?>";
		global["text_first_name"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('first_name')) ?>";
		global["text_last_name"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('last_name')) ?>";
		global["text_invalid_phone"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('invalid_phone')) ?>";
		global["text_invalid_email"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('invalid_email')) ?>";
		global["text_source_phone"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('appointment_source_phone')) ?>";
		global["text_source_email"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('appointment_source_email')) ?>";
		global["text_source_walkin"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('appointment_source_walkin')) ?>";
		global["text_source_others"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('appointment_source_others')) ?>";
		global["text_typeLabel"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('customer_type')) ?>";
		global["text_existingCustomerLabel"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('existing_customer')) ?>";
		global["text_newCustomerLabel"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('new_customer')) ?>";
		global["text_appointment_created_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('appointment_created_title')) ?>";
		global["text_appointment_created_message"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('appointment_created_message')) ?>";
		global["text_confirm_update_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('confirm_update_appointment')) ?>";
		global["text_want_to_update_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('want_to_update_appointment')) ?>";
		global["text_confirm_create_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('confirm_create_appointment')) ?>";
		global["text_want_to_create_appointment"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('want_to_create_appointment')) ?>";
		
		global["text_cost"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('cost')). '('.APP88_CURRENCY.')' ?>";
		global["text_message"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('message')) ?>";
		global["text_send_message_to_customer"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('send_message_to_customer')) ?>";
		global["text_add_notes"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('add_notes')) ?>";
		global["text_delete_note"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('delete_note')) ?>";
		global["text_are_you_to_del_note"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('are_you_to_del_note')) ?>";
		global["text_view_note"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('view_note')) ?>";
		global["text_add"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('add')) ?>";
		global["text_send_message_confimation"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('send_message_confimation')) ?>";
		global["text_are_you_sure_send_this_message"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('are_you_sure_send_this_message')) ?>";
		global["text_view"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('view')) ?>";
		global.pageName ="customer";
  </script>
  <style>
		.loading{
			height: 16px; 
			background-image: url("../../images/back_end/loading.gif"); 
			display: inline-block; 
			width: 16px; 
			background-position: center center;
		}
	</style>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/date.js") ?>"></script>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/back_end/customer.js") ?>"></script> 
 </head> 
 <body> 
  <div id="wrapper"> 
   <?php $this->load->view("section/bo_header_bar.php"); ?>
   <div id="main-content" class="customerListContainer"> 
    <div class="pageTitle"> 
     <h2 class="calender"><?php echo $this->lang->appointmentLine('customer') ?></h2> 
    </div> 
	<?php 
	if(count($customersName) > 0 )
	{?>
    <div class="dbShop-ui-mainLeft"> 
     <div class="list-container"> 
      <div class="list-search">
       <?php echo $this->lang->appointmentLine('name') ?>
       <input type="text" maxlength="50" size="20" name="keyword" class="search_Keyword" />
       <a href="javascript:;" class="search_btn" id="search_button" style="cursor:default"></a>
      </div> 
      <div class="listScrollBox_B"> 
		<?php foreach($customersName as $name):?>
       <li><input type="radio" name="advance" value="1"><a href="javascript:;" guid="<?php echo $this->guid->createFromBinary($name->CustomerGuid)->toString() ?>" class="scrollList"><?php echo form_prep($name->FirstName) ?></a></li>
	   <?php endforeach;?>
      </div> 
     </div> 
    </div> 
    <div class="dbShop-ui-mainRight"> 
     <div class="user-info-container"> 
      <div class="infoTitle">
       <?php echo $this->lang->appointmentLine('customer_info') ?>
	   <span style="float:right">
		<a href="javascript:void(0)" class="btnCreateAppointment dbShop-ui-appBtn dbShop-ui-addapointment_bo" style="margin-left:15px; margin-top:2px; font-weight:normal"><?php echo $this->lang->appointmentLine('create_new_appointment') ?></a>
		</span>
      </div> 
      <div class="userInfo"> 
       <label for="category_name"><?php echo $this->lang->appointmentLine('name') ?>:</label> 
       <div class="nameInfo app_name">--</div> 
       <label for="category_name"><?php echo $this->lang->appointmentLine('phone') ?>:</label> 
       <div class="nameInfo app_phone">--</div> 
	   <div class="app_email" style="display:none">--</div> 
       
       <div class="nameInfo">
        <a class="sendMessage btnSendMessage" herf="javascript:;" style="margin-left:-10px;"><?php echo $this->lang->appointmentLine('send_message') ?></a>
       </div> 
      </div> 
      <div class="dbShop-ui-clearboth"></div> 
     </div> 
     <div class="user-info-container"> 
      <div class="infoTitle">
       <?php echo $this->lang->appointmentLine('upcoming_appointment') ?>
      </div> 
	  <div class="upcomingAppointmentsNoDataContainer" style="display:none">
	  <p> </p>
	  <div class="dbShop-ui-warning"><?php echo $this->lang->appointmentLine('no_appointment') ?></div>
	  <p> </p>
	  </div>
      <div class="scheduled-container upcomingAppointmentsContainer"> 
       <div class="scheduled-header" style="padding: 10px 10px 10px 15px;"> 
        <span class="ch01_Upcoming_Appointment"><?php echo $this->lang->appointmentLine('schedule') ?></span> 
        <span class="ch02_Upcoming_Appointment"><?php echo $this->lang->appointmentLine('service') ?></span> 
        <span class="ch03_Upcoming_Appointment"><?php echo $this->lang->appointmentLine('resource') ?></span> 
        <span class="ch04_Upcoming_Appointment"><?php echo $this->lang->appointmentLine('cost'). '('.APP88_CURRENCY.')' ?></span> 
		<span class="ch05_Upcoming_Appointment"><?php echo $this->lang->appointmentLine('note') ?></span>
		<span class="ch06_Upcoming_Appointment"><?php echo $this->lang->appointmentLine('action') ?></span>
       </div>        
      </div> 
      <div class="dbShop-ui-clearboth"></div> 
     </div> 
     <div class="user-info-container"> 
      <div class="infoTitle">
       <?php echo $this->lang->appointmentLine('previous_appointment') ?>
       <a class="viewall btnViewAllPrevAppointments" href="#"> <?php echo $this->lang->appointmentLine('view_all_previous_appointment') ?></a>
      </div> 
	  <div class="previousAppointmentsNoDataContainer" style="display:none">
	  <p> </p>
	  <div class="dbShop-ui-warning"><?php echo $this->lang->appointmentLine('no_appointment') ?></div>
	  <p> </p>
	  </div>
      <div class="scheduled-container previousAppointmentsContainer"> 
       <div class="scheduled-header" style="padding: 10px 10px 10px 15px;"> 
        <span class="ch01_Previous_Appointment"><?php echo $this->lang->appointmentLine('schedule') ?></span> 
        <span class="ch02_Previous_Appointment"><?php echo $this->lang->appointmentLine('service') ?></span> 
        <span class="ch03_Previous_Appointment"><?php echo $this->lang->appointmentLine('resource') ?></span> 
        <span class="ch04_Previous_Appointment"><?php echo $this->lang->appointmentLine('cost'). '('.APP88_CURRENCY.')' ?></span> 
		<span class="ch05_Previous_Appointment"><?php echo $this->lang->appointmentLine('note') ?></span>
       </div> 
      </div> 
      <div class="dbShop-ui-clearboth"></div> 
     </div> 
     <div class="user-info-container"> 
      <div class="infoTitle">
       <?php echo $this->lang->appointmentLine('note') ?>
       <a class="btnViewAllNotes viewall" herf="javascript:;"> <?php echo $this->lang->appointmentLine('view_all_notes') ?></a>
      </div> 
	  <div class="latestCustomerNotesNoData" style="display:none">
	  <p> </p>
	  <div class="dbShop-ui-warning"><?php echo $this->lang->appointmentLine('no_note') ?></div>
	  <p> </p>
	  </div>
      <div class="scheduled-container latestCustomerNotes"> 
       <div class="scheduled-header" style="padding: 10px 10px 10px 15px;"> 
        <span class="ch01_Upcoming_Appointment_Note"><?php echo $this->lang->appointmentLine('time') ?></span> 
        <span class="ch02_Upcoming_Appointment_Note"><?php echo $this->lang->appointmentLine('note') ?></span> 
       </div> 
      </div> 
      <div class="dbShop-ui-clearboth"></div> 
      <div class="Action"> 
       <div class="pageAdd fl"> 
        <a class="dbShop-ui-appBtn dbShop-ui-addproduct btnAddCustomerNote" style="margin-top:10px" href="javascript:;"><?php echo $this->lang->appointmentLine('add') ?></a> 
       </div> 
       <div class="pageBlank"> 
       </div> 
      </div> 
     </div> 
    </div> 
    <div class="dbShop-ui-clearboth"></div> 
	<?php 
	}
	else
	{
	?><?php
		echo $this->lang->appointmentLine('no_customer');
	}?>
   </div>   
   
   <!-- start view prev appointments -->
  <div id="main-content" class="allPrevAppointmentsContainer" style="display:none"> 
    <div class="pageTitle"> 
     <h2 class=""><?php echo $this->lang->appointmentLine('view_all_previous_appointment') ?></h2> 
    </div> 
    <div class="pageData"> 
     <div class="scheduled-container allPrevAppointmentsList"> 
      <div class="scheduled-header"> 
       <span class="ch01_ViewallAppointment"><?php echo $this->lang->appointmentLine('service') ?></span> 
       <span class="ch02_ViewallAppointment"><?php echo $this->lang->appointmentLine('start_time') ?></span> 
       <span class="ch03_ViewallAppointment"><?php echo $this->lang->appointmentLine('end_time') ?></span> 
       <span class="ch04_ViewallAppointment"><?php echo $this->lang->appointmentLine('resource') ?></span> 
	   <span class="ch05_ViewallAppointment"><?php echo $this->lang->appointmentLine('cost'). '('.APP88_CURRENCY.')' ?></span>
	   <span class="ch06_ViewallAppointment"><?php echo $this->lang->appointmentLine('note') ?></span>
      </div> 
     </div>
     <table id="product-table-grid" class="datagrid2" width="100%">  
      <tfoot> 
       <tr> 
        <td class="category-pagination" colspan="7">
         <div class="prevAppointmentsPaging" id="paging" style="display: block;"></div>
		 </td> 
       </tr> 
      </tfoot> 
     </table> 
     <div class="button-panel" style="margin:20px 0px 15px -10px;"> 
      <a href="javascript:;" class="btnBackToCustomerList dbShop-ui-appBtn dbShop-ui-back" style="margin-left:15px"><?php echo $this->lang->appointmentLine('btn_back') ?></a> 
     </div> 
    </div> 
   </div> 
  <!-- end view prev appointments -->
  
  <!-- start all customer notes -->
  <div id="main-content" class="allCustomerNotesContainer" style="display:none"> 
   <div class="pageTitle"> 
    <h2 class=""><?php echo $this->lang->appointmentLine('view_all_notes') ?></h2> 
   </div> 
   <div class="pageData"> 
    <div class="scheduled-container"> 
     <div class="scheduled-header"> 
      <span class="ch01_an"><?php echo $this->lang->appointmentLine('time') ?></span> 
      <span class="ch02_bn"><?php echo $this->lang->appointmentLine('note') ?></span> 
      <span class="ch04_dn"></span> 
     </div>     
    </div>
    <table width="100%" class="datagrid2" id="product-table-grid"> 
     <tfoot> 
      <tr> 
       <td colspan="7" class="category-pagination">
        <div style="display: block;" id="paging" class="paging allCustomerNotesPaging"> </div> </td> 
      </tr> 
     </tfoot> 
    </table> 
    <div style="margin:20px 0px 15px -10px;" class="button-panel"> 
     <a style="margin-left:15px" class="btnBackToCustomerList dbShop-ui-appBtn dbShop-ui-back" href="javascript:;"><?php echo $this->lang->appointmentLine('btn_back') ?></a> 
    </div> 
   </div> 
  </div>
  <!-- end all customer notes -->
  </div> 
  
  
  <?php $this->load->view("section/footer.php"); ?>
 </body>
</html>
<?php 
	echo '<!-- '.$this->common->showBenchmark().' -->';
?>