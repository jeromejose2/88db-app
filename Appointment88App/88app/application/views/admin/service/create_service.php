<?php 
	include_once 'includePHPHeaders.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head> 
	<?php $this->load->view("section/bo_header.php"); ?>
	<script type="text/javascript">
		global["hour"] = "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('hour')) ?>";
		global["mins"] = "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('mins')) ?>";
		global["day"] = "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('day')) ?>";
		global["please_select_valid_value"] = "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('please_select_valid_value')) ?>";
		
		global["text_service_advance_booking_settings_tips"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('service_advance_booking_settings_tips')) ?>";
		global["text_service_advance_booking_settings_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('service_advance_booking_settings_title')) ?>";
		
		global.isEdit = <?php echo empty($service)?"false":"true"; ?>;
		global.timeSlotDivision =  "<?php echo $service->TimeSlotDivision ?>";
		global.minTime =  "<?php echo $service->MinTime ?>";
		global.advanceTime =  "<?php echo $service->AdvanceTime ?>";
		global.duration =  "<?php echo $service->Duration ?>";
		global.pageName ="service";
	</script>
	<script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/back_end/createservice.js") ?>"></script>
	<script src="<?php echo  $this->common->getVersionFilePath("js/tinymce/jscripts/tiny_mce/tiny_mce.js") ?>"></script>
	<script src="<?php echo  $this->common->getVersionFilePath("js/jquery.minicolors.js") ?>"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $this->common->getVersionFilePath("css/jquery.minicolors.css") ?>" /> 
	<script>
        tinymce.init({
			selector:'textarea',
			<?php
				$lang_mapping = $this->config->item('admin_setting_tinycme_lang');
				echo 'language:"'.$lang_mapping[$this->app88common->get_shop_lang()].'",';
			?>
			plugins : "table,spellchecker",
			width:550,
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough, link, unlink,bullist,numlist,separator, justifyleft,justifycenter,justifyright, justifyfull,fontselect,fontsizeselect,forecolor,cleanup,hr",
			theme_advanced_buttons2 : "cut,copy,paste,separator,indent,outdent,blockquote,redo,tablecontrols,spellchecker,sup,sub,image",
			theme_advanced_buttons3 : ""
		});

		$(document).ready(function() {
			//for diable full day checkbox
			$("#duration_hour").removeAttr("disabled");
			$("#duration_min").removeAttr("disabled");
		});
	</script>
 </head> 
 <body> 
  <div id="wrapper"> 
    <?php $this->load->view("section/bo_header_bar.php"); ?>
   <div id="main-content"> 
    <div class="pageTitle"> 
     <h2 class=""><?php echo empty($service)?$this->lang->appointmentLine('create_service'):$this->lang->appointmentLine('edit_service') ?></h2> 
    </div> 
    <form action="<?php echo site_url("admin/service/createservice") ?>" method="post" accept-charset="utf-8">	
     <div class="pageAction"> 
      <div class="button-panel" style="margin-bottom:15px"> 
       <a href="javascript:void(0);"  onclick="return false"  class="dbShop-ui-appBtn dbShop-ui-save btnSave"><?php echo $this->lang->appointmentLine('btn_save') ?></a> 
       <a href="<?php echo site_url("admin/service/servicelist") ?>" class="dbShop-ui-appBtn dbShop-ui-back" style="margin-left:15px"><?php echo $this->lang->appointmentLine('btn_back') ?></a> 
      </div> 
      <div class="formLoader"> 
       <div class="dbShop-ui-rel">
        <p class="ganti-nama"> <label for="news_title"><span class="compulsory-s">*</span><?php echo $this->lang->appointmentLine('service_name') ?>:</label> <input name="name" id="name" value="<?php echo $service->Name ?>" class="common-validatebox" maxlength="60" data-options="required:true, missingMessage:'<?php echo $this->app88common->js_quote($this->lang->appointmentLine('please_enter_valid_value')) ?>'" size="35" id="news_title" placeholder="<?php echo $this->lang->appointmentLine('enter_service_name') ?>" type="text" /> </p>		
        <p style="margin-left:165px;color:#777"><?php echo $this->lang->appointmentLine('service_title_must_60_chars') ?></p> 
		<!--<p style="position:relative;" class="ganti-nama"><label for="assignColor"><span class="compulsory-s">*</span><?php echo $this->lang->appointmentLine('assign_color') ?>:</label><input type="text" value="<?php echo $service->AssignColor ?>" id="assignColor" size="35" class="minicolors common-validatebox" data-options="required:true, missingMessage:'<?php echo $this->app88common->js_quote($this->lang->appointmentLine('please_enter_valid_value')) ?>'" style="padding:6.5px; width:80px;" name="assign_color"></p>-->
        <p class="ganti-nama"> <label for="news_title" id="duration"><span class="compulsory-s">*</span><?php echo $this->lang->appointmentLine('duration') ?>:</label> 
			<!--<input name="duration" value="<?php echo $service->Duration ?>" id="duration" style="margin-right:20px" class="common-validatebox" data-options="required:true, validType: 'number', min:0, missingMessage:'<?php echo $this->app88common->js_quote($this->lang->appointmentLine('please_enter_valid_value')) ?>'" size="35" id="news_title" value="0" type="text" />
			<?php echo $this->lang->appointmentLine('mins') ?> 			-->
			<select class="ddl_hafHours" id="duration_hour" style="margin-right:50px"></select> 
			<span style="display:block;position:absolute;padding-top:10px; left:335px;"> <?php echo $this->lang->appointmentLine('hours') ?></span>
			<select name="category_id" class="ddl_hafMins" style="margin-left:20px;" id="duration_min" style="margin-left:20px;"></select> 
			 <span style="display:block;position:absolute;padding-top:10px; left:555px;"> <?php echo $this->lang->appointmentLine('mins') ?></span>
			 <?php if (FALSE) { ?>
			 <span style="display:block;position:absolute;padding-top:8px; left:610px;">
            <input type="margin-bottomtyle="margin-left:20px;" checked="checked" value="1" id="duration_allday" name="duration_allday" ><span style="padding-right:30px">Full day</span></span>
            <?php } ?>
		</p>
        <p class="ganti-nama">
        	<label for="news_title">
        		<span class="compulsory-s">*</span>
        		<?php echo $this->lang->appointmentLine('cost') ?>:
        	</label>
        	<?php 
        		$input = '<input name="cost" id="cost" value="'.$service->Cost.'" style="margin-right:20px;float:none" size="35" id="news_title" value="0" type="text" class="common-validatebox" data-options="required:true, validType: \'price\', min:1, missingMessage:\''.$this->app88common->js_quote($this->lang->appointmentLine('please_enter_valid_value')).'\'" />';
        		$input .= $this->lang->appointmentLine('price_unit');
        		echo sprintf(APP88_MONEY_FORMAT, $input);
        	?>
        </p>
        <p class="ganti-nama"><label for="news_desc" id="label_description"><?php echo $this->lang->appointmentLine('description') ?>:</label> <textarea id="description" name="description"rows="30" cols="80" style="height:300px;width: 600px"><?php echo form_prep($service->Description) ?></textarea></p>        
        <div class="ganti-nama" style="padding-left:10px">
        	<label for="category_id" id="timeSlotDivision" style="height:32px"><span class="compulsory-s">*</span><?php echo $this->lang->appointmentLine('service_booking_size') ?>:</label> 
			<select class="ddl_hours" id="timeSlotDivision_hour" style="margin-right:50px"></select> 
			<span style="display:block;position:absolute;padding-top:10px; left:335px;"><?php echo $this->lang->appointmentLine('hours') ?></span>
			<select name="category_id" class="ddl_hafMins" id="timeSlotDivision_min" style="margin-left:20px;"></select> 
			<span style="display:block;position:absolute;padding-top:10px; left:555px;"><?php echo $this->lang->appointmentLine('mins') ?></span>
		</div> 
		<p></p>
		<p class="ganti-nama" style="padding-left:175px; margin-top:-10px; color:#666;"><?php echo $this->lang->appointmentLine('service_booking_size_tips') ?></p>
		<p></p>
		<div class="dbShop-ui-clearboth"></div>
		<div class="ganti-nama" style="padding-left:10px"> 
			<label for="news_title"><span class="compulsory-s">*</span><?php echo $this->lang->appointmentLine('availablepertimeslot') ?>:</label> 
			<input name="availablePerTimeSlot" id="availablePerTimeSlot" value="<?php echo $service->AvailablePerTimeSlot ?>" class="common-validatebox" data-options="required:true, validType: 'number',min:1,  missingMessage:'<?php echo $this->app88common->js_quote($this->lang->appointmentLine('please_enter_valid_value')) ?>'" size="21" id="news_title" value="0" type="text" />
		</div>
		<p></p>
		<div class="dbShop-ui-clearboth"></div>
        <p class="ganti-nama"><label for="category_id" id="minTime" style="width:150px"><span class="compulsory-s">*</span><?php echo $this->lang->appointmentLine('min_time_before_can_made') ?>:</label> <select name="category_id" class="ddl_days_0" id="minTime_day" style="margin:5px 50px 0px 10px"></select> 
			<span style="display:block;position:absolute;padding-top:10px; left:335px;"> <?php echo $this->lang->appointmentLine('days') ?></span>
			<!--<select name="category_id" class="ddl_hours" id="minTime_hour" style="margin-left:20px;"></select>-->
			<span class="HelpIco"><a class="help_img"><img width="16" height="16" alt="Help" src="../../images/back_end/info-icon.png"></a></span>
		</p>
		<p></p>
		<div class="dbShop-ui-clearboth"></div>
        <p class="ganti-nama"><label for="category_id" id="advanceTime" style="width:150px"><span class="compulsory-s">*</span><?php echo $this->lang->appointmentLine('how_far_advance_can_made') ?>:</label> 
			<span style="display:block;float:left; padding-top:10px;">
			<input name="RadadvanceTime" value="nolimit" checked="checked" type="radio" style="margin-left:9px;"/> 
			<span style="padding-right:30px"><?php echo $this->lang->appointmentLine('no_limit') ?></span> 
			<input name="RadadvanceTime" value="limit" type="radio"/> 
			</span>
			<select name="advanceTime_day" class="ddl_days" id="advanceTime_day" style="margin:5px 50px 0px 10px"></select> 
			<span style="display:block;position:absolute;padding-top:10px; left:465px;"> <?php echo $this->lang->appointmentLine('days') ?></span>
			<!--<select name="advanceTime_hour" id="advanceTime_hour"class="ddl_hours" style="margin-left:20px;"></select>-->	
			<span class="HelpIco"> <a class="help_img"><img width="16" height="16" alt="Help" src="../../images/back_end/info-icon.png"></a></span>
			
		</p> 
        <p></p> 
        
       </div> 
       <div class="button-panel" style="margin-top:20px"> 
        <!-- <p class="save-btn">
			<input name="preview" value="Preview" type="submit" onclick="previewNews()"/>
		</p> --> 
        <a href="javascript:void(0);" onclick="return false"  class="dbShop-ui-appBtn dbShop-ui-save btnSave"><?php echo $this->lang->appointmentLine('btn_save') ?></a> 
        <a href="<?php echo site_url("admin/service/servicelist") ?>" class="dbShop-ui-appBtn dbShop-ui-back" style="margin-left:15px"><?php echo $this->lang->appointmentLine('btn_back') ?></a> 
       </div> 
      </div> 
      <div style="display: none;"> 
      </div> 
     </div>
    </form> 
    <div id="delete-confirm"> 
     <p></p> 
    </div> 
    <div id="alert-box"> 
     <p></p> 
    </div> 
   </div>    
   <?php $this->load->view("section/footer.php"); ?>
 </body>
</html>
<?php 
	echo '<!-- '.$this->common->showBenchmark().' -->';
?>