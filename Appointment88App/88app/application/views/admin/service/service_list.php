<?php 
	include_once 'includePHPHeaders.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
 <head> 
  <?php $this->load->view("section/bo_header.php"); ?>
  <script type="text/javascript" src="<?php echo  $this->common->getVersionFilePath("js/back_end/servicelist.js") ?>"></script>
  <script type="text/javascript">
		global["dialog_delete_service_title"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('dialog_delete_service_title')) ?>";
		global["dialog_delete_service_content"]="<?php echo $this->app88common->js_quote($this->lang->appointmentLine('dialog_delete_service_content')) ?>";	
		global["text_enabled"] = "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('enabled')) ?>";
		global["text_disabled"] = "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('disabled')) ?>";
		global["text_show"] = "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('show')) ?>";
		global["text_hide"] = "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('hide')) ?>";
		global["dialog_copylink_title"] = "<?php echo $this->app88common->js_quote($this->lang->appointmentLine('dialog_copylink_title')) ?>";
		global["select_service_url"] = "<?php echo site_url("appointment/selectResource") ?>";
		global.pageName ="service";
  </script>
 </head> 
 <body> 
  <div id="wrapper"> 
  <?php $this->load->view("section/bo_header_bar.php"); ?>
   <style>
.catalogue-listing-ttl{float:none;} 
</style> 
   <div id="main-content"> 
    <div class="pageTitle"> 
     <h2 class="">
     	<?php echo $this->lang->appointmentLine('service') ?>
     	<?php if (FALSE) { ?>
     	<span>
     		<?php echo $this->lang->appointmentLine('posted_services') ?>: ( <span class="activeCount" style="margin-left:0"><?php echo $services["activeCount"]; ?></span>/<?php echo $services["total"]; ?>)
     	</span>
     	<?php } ?>
     </h2> 
    </div> 
    <div class="Action"> 
     <div class="pageAdd-fl" style="float:left"> 
      <a href="<?php echo site_url("admin/service/createservice") ?>" class="dbShop-ui-appBtn dbShop-ui-addproduct"><?php echo $this->lang->appointmentLine('add') ?></a> 
     </div> 
     <div class="pageBlank-mid" style="float:right"> 
       <div style="float:right;"> 
        <a href="javascript:return false;" onclick="return false" style="margin-left:10px;" class="btnSearch dbShop-ui-appBtn dbShop-ui-apply"><?php echo $this->lang->appointmentLine('submit') ?></a> 
        <a href="javascript:return false;" onclick="return false" class="btnClear dbShop-ui-appBtn dbShop-ui-apply" style="margin-left:5px;"><?php echo $this->lang->appointmentLine('clear') ?></a> 
       </div> 
       <div style="float:right"> 
        <label for="txtKeyword"><?php echo $this->lang->appointmentLine('keywords') ?></label> 
        <input name="txtKeyword" id="txtKeyword" type="text" /> 
       </div> 
       <div style="clear:both"></div> 
     </div> 
     <div class="pageBlank"></div> 
    </div> 
    <div class="pageData"> 
     <table id="product-table-grid" class="datagrid2" width="100%"> 
      <thead> 
       <tr> 
        <th class="catalogue-listing-ttl"><span href="javascript:;" sortname="Name"><?php echo $this->lang->appointmentLine('service') ?></span><span sortname="Name" class="arrowSort_des" style="display:none"></span></th> 
        <th class="catalogue-listing-ttl"><span href="javascript:;" sortname="Duration"><?php echo $this->lang->appointmentLine('duration') ?></span><span sortname="Duration"class="arrowSort_asc" style="display:none"></span></th> 
        <th class="catalogue-listing-ttl"><span href="javascript:;" sortname="Cost"><?php echo $this->lang->appointmentLine('cost') . ' ('.APP88_CURRENCY.')' ?></span><span sortname="Cost" class="arrowSort_asc" style="display:none"></span></th> 
        <th class="catalogue-listing-ttl"><span href="javascript:;" sortname="Staff"><?php echo $this->lang->appointmentLine('staff') ?></span><span sortname="Staff" class="arrowSort_asc" style="display:none"></span></th> 
        <th class="catalogue-listing-ttl"><span href="javascript:;" sortname="Status"><?php echo $this->lang->appointmentLine('status') ?></span><span sortname="Status" class="arrowSort_asc" style="display:none"></span></th> 
		<th class="catalogue-listing-ttl" ><?php echo $this->lang->appointmentLine('re-order') ?></th>
        <th class="catalogue-listing-ttl"><?php echo $this->lang->appointmentLine('action') ?></th> 
       </tr> 
      </thead> 
      <tbody> 
		<?php 
		$row = 1;
		foreach ($services["rows"] as $service):?>
       <tr guid="<?php echo $this->guid->createFromBinary($service->Guid)->toString() ?>" weight="<?php echo $service->Weight ?>"> 
        <td class="catalogue-product"><a href="<?php echo site_url("admin/service/createservice?serviceid=".$this->guid->createFromBinary($service->Guid)->toString()) ?>"><span class="published_service"><?php echo form_prep($service->Name) ?></span></a></td> 
        <td class="catalogue-published"><span class="published_duration"><?php echo $service->Duration ?> <?php echo $this->lang->appointmentLine('mins') ?></span></td> 
        <td class="catalogue-published"><span class="published_cost"><?php echo $this->common->number_format($service->Cost) ?></span></td> 
        <td class="catalogue-published"><span class="published_staff"><?php echo $service->Staff ?></span></td> 
        <td class="catalogue-published label_status"><span class="published_status"><?php echo $service->Status === "A"?$this->lang->appointmentLine('enabled'):$this->lang->appointmentLine('disabled') ?></span></td> 
		<td class="catalogue-opt" style="text-align:center;float:none;">
			<div class="menu-link" style="display:inline-block;padding:5px;">
				<a class="dbShop-ui-sorMoveUpBtn btnMoveUp" href="javascript:void(0);"></a>
				<a class="dbShop-ui-sorMoveDnBtn btnMoveDown" href="javascript:void(0);"></a>                           
				<div style="clear:both"></div>
			</div>
		</td>
        <td class="catalogue-opt" style="text-align:center;float:none;"> 
         <div class="menu-link" style="display:inline-block;padding:5px;"> 
          <a title="<?php echo $this->lang->appointmentLine('edit') ?>" class="dbShop-ui-appBtn dbShop-ui-addeditproduct_action" href="<?php echo site_url("admin/service/createservice?serviceid=".$this->guid->createFromBinary($service->Guid)->toString()) ?>"></a> 
          <a title="<?php echo $service->Status === "A"?$this->lang->appointmentLine('enabled'):$this->lang->appointmentLine('disabled') ?>" class="btnSwitchStatus dbShop-ui-appBtn dbShop-ui-<?php echo $service->Status === "A"?"show":"hide" ?>_action" href="#"></a> 
          <a title="<?php echo $this->lang->appointmentLine('delete') ?>" href="#" class="btnDelete dbShop-ui-appBtn dbShop-ui-adddelete_action"></a> 
          <?php if (FALSE) { ?>
          <a title="<?php echo $this->lang->appointmentLine('copylink') ?>" href="#" class="btnCopyLink dbShop-ui-appBtn dbShop-ui-addcopylink_action dbShop-ui-Btn-right"></a>
          <?php } ?> 
          <div style="clear:both"></div> 
         </div> </td> 
       </tr>   
	   <?php 
		$row++;
		if($row > 2)
		{
			$row = 1;
		}
	   endforeach; ?>
      </tbody> 
      <tfoot> 
       <tr> 
        <td class="category-pagination" colspan="7">
         <?php echo $this->common->genPaging($services["total"], $pageNumber)?></td> 
       </tr> 
      </tfoot> 
     </table> 
    </div> 
    <div class="Action"> 
     <div class="pageAdd fl"> 
      <a class="dbShop-ui-appBtn dbShop-ui-addproduct" href="<?php echo site_url("admin/service/createservice") ?>"><?php echo $this->lang->appointmentLine('add') ?></a> 
     </div> 
     <div class="pageBlank"> 
     </div> 
    </div> 
   </div>    
   <div id="delete-confirm"> 
    <p></p> 
   </div> 
   <div id="alert-box"> 
    <p></p> 
   </div> 
  </div> 
   <?php $this->load->view("section/footer.php"); ?>
 </body>
</html>
<?php 
	echo '<!-- '.$this->common->showBenchmark().' -->';
?>