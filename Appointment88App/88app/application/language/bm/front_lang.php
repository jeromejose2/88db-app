<?php

include_once('country_lang.php');

$lang['err_session_expired'] = 'Sesi telah tamat, sila muat semula kedai';
$lang['label'] = 'teks';

$lang['member_area'] = 'Lihat {AppointmentTitle} Anda';
$lang['member_login'] = 'Lihat {AppointmentTitle} Anda';
$lang['title_make_appointment'] = 'Buat {AppointmentTitle}';
$lang['tab_select_service'] = 'Pilih {ServiceTitle}';
$lang['tab_select_resource'] = 'Pilih {ResourceTitle}';
$lang['tab_select_schedule'] = 'Pilih {ScheduleTitle}';
$lang['tab_contace_info'] = 'Maklumat Kenalan';
$lang['tab_confirmation'] = 'Pengesahan';
$lang['service_duration'] = 'Tempoh';
$lang['service_cost'] = 'Harga';
$lang['next'] = 'Berikut';
$lang['back'] = 'Kembali';
$lang['ok'] = 'OK';
$lang['confirm'] = 'Sahkan';
$lang['yes'] = 'YA';
$lang['no'] = 'TIDAK';
$lang['cancel'] = 'Batal';
$lang['am'] = 'AM';
$lang['pm'] = 'PM';
$lang['name'] = 'Nama';
$lang['first_name'] = 'Nama Pertama';
$lang['last_name'] = 'Nama Keluarga';
$lang['contact'] = 'Hubungi';
$lang['phone_number'] = 'Nombor Telefon';
$lang['email'] = 'Emel';
$lang['email_verify'] = 'Emel (Sah)';
$lang['please_select_schedule'] = 'Sila pilih {ScheduleTitle}';
$lang['not_available'] = 'Tidak Didapati';
$lang['start'] = 'Mula';
$lang['end'] = 'Tamat';
$lang['timeslot_is_full'] = '{ResourceTitle} ini telah ditempah sepenuhnya. Sila pilih {ResourceTitle} lain.';

$lang['verify_firstname'] = 'sila masukkan nama pertama';
$lang['verify_lastname'] = 'sila masukkan nama keluarga';
$lang['verify_phone'] = 'sila masukkan nombor telefon';
$lang['verify_email'] = 'sila masukkan emel';
$lang['verify_verifyemail'] = 'sila masukkan emel sah';
$lang['invalid_phone'] = 'nombor telefon tidak sah';
$lang['invalid_email'] = 'emel tidak sah';
$lang['invalid_emailcompare'] = 'Emel dan Emel (Sah) tidak sepadan';
$lang['your_contact_information'] = 'Maklumat Perhubungan Anda.';
$lang['note_optional'] = 'Nota Khas';
$lang['please_leave_message_here'] = 'Anda boleh meninggalkan sebarang nota khas atau arahan untuk kedai';
$lang['verifycode'] = 'Masukkan Kod Pengesahan';
$lang['verifyCodeError_dialog_title'] = 'Ralat Kod Pengesahan';
$lang['verifyCodeError_dialog_content'] = 'Kod Pengesahan tidak betul. Sila cuba lagi.';
$lang['service'] = '{ServiceTitle}';
$lang['resource'] = '{ResourceTitle}';
$lang['schedule'] = '{ScheduleTitle}';
$lang['contact_info'] = 'Maklumat Perhubungan';
$lang['reminder'] = 'Peringatan';
$lang['send_me_email_reminder'] = 'Hantarkan saya emel peringatan ';
$lang['make_appointment'] = 'Buat {AppointmentTitle} ';
$lang['confirm_appointment'] = 'Sahkan {AppointmentTitle} ';

$lang['made_successfully'] = '{AppointmentTitle} telah Berjaya Dibuat ';
$lang['btn_view_booking_detail'] = 'Lihat Butiran ';
$lang['btn_make_new_appointment'] = 'Buat {AppointmentTitle} Baru';

$lang['scheduled_appointment'] = '{AppointmentTitle} yang Akan Datang';
$lang['cancel_appointment'] = 'Batal {AppointmentTitle}';
$lang['cancel_appointment_to_sure'] = 'Adakah anda pasti mahu membatalkan {AppointmentTitle} di bawah?';

$lang['almost_done_with_your_booking'] = 'Satu langkah lagi ...';
$lang['almost_done_with_your_booking_description'] = '<p>Untuk melengkapkan tempahan {AppointmentTitle} anda, anda perlu mengesahkan akaun ahli anda.</p> Satu emel dengan URL pengesahan daripada 88DB telah dihantar ke emel anda ({email}). Setelah pengesahan akaun selesai, {AppointmentTitle} anda akan disahkan secara automatik.';
$lang['open_your_email_inbox'] = 'Buka Peti Emel Anda';


$lang['time_format'] ='hh:mm(AM|PM)';
$lang['day_collection'] = 'Ahd,Isn,Sel,Rab,Kha,Jum,Sab';
$lang['to'] = 'kepada';
$lang['price_unit'] = 'RM';
$lang['err_service_expired'] = '{ServiceTitle} tidak tersedia.';
$lang['err_resource_expired'] = '{ResourceTitle} tidak tersedia.';
$lang['error_invalid_schedule_date'] = '{ScheduleTitle} tarikh tidak sah.';

$lang['timelostfull_dialog_title'] = 'Tiada slot masa';
$lang['timelostfull_dialog_content'] = 'Slot masa yang anda pilih tidak lagi tersedia. Sila pilih slot masa yang lain';
$lang['close'] = 'Tutup';

$lang['error_timeslot_is_full'] = 'tiada slot masa';
$lang['error_any_fileds_is_empty'] = 'terdapat ruangan kosong.';
$lang['error_invalid_isreminder_value'] = 'Nilai Peringatan tidak sah';
$lang['error_no_timeslot_status_record'] = 'tiada rekod status slot masa.';
$lang['error_deal_date_expired'] = 'tarikh urusan telah tamat.';
$lang['error_selected_range_error'] = 'yang dipilih bukan antara tarikh min dan tarikh max';
$lang['error_not_timeslot'] = 'tiada dalam slot masa.';
$lang['error_special_time_error'] = 'adalah hari istimewa, tetapi bukan masa yang istimewa;';
$lang['error_is_not_week_day'] = 'bukan hari minggu.';
$lang['error_in_dayoff_day'] = 'adalah hari cuti.';


$lang['please_enter_valid_value'] = 'Sila masukkan nilai sah. ';
$lang['resource_no_provided_service'] = '{ResourceTitle} tidak menyediakan {ServiceTitle} yang dipilih.';
$lang['login_with_facebook'] = 'Masuk dengan Facebook';
$lang["tc"] = 'Dengan mengesahkan {AppointmentTitle} ini, anda bersetuju dengan 88DB.com<a href="%s" target="_blank"> Pernyataan Privasi</a> dan <a href="%s" target="_blank">Terma & Syarat.</a>';
$lang['err_database_err'] = '<P>Maaf, permintaan anda tidak dapat kami proses buat sementara waktu. Anda boleh kembali ke <a href="javascript:void(0);" onclick="history.go(-1);">halaman sebelumnya</a> atau ke <a href="http://hk.88db.com/en/" target="_top">88DB.com</a>.<br /><br />Untuk bantuan, sila <a href="mailto:cs@88db.com">hubungi kami</a>.</P>';
$lang['service_is_unavailable'] = '{ServiceTitle} Penempahan {AppointmentTitle} adalah tidak tersedia.';

$lang['datepicker_language'] = '';
$lang['duration_format'] = '%dh %dm';
$lang['view_detail'] = 'Lihat Butiran';
$lang['please_select_resouce'] = 'Sila pilih {ResourceTitle}';
$lang['tab_select_resource_schedule'] = 'Pilih {ResourceTitle} & {ScheduleTitle}';
$lang['current_day_no_timeslot'] = 'Hari yang dipilih anda tiada {ScheduleTitle} kosong.';
$lang['available'] = 'Ada';
$lang['no_preference'] = 'Tiada Keutamaan';
$lang['no_available_resource'] = 'Tiada {ResourceTitle}';
$lang['select'] = 'Pilih';
$lang['clear_selection'] = 'Kosongkan Pemilihan';
