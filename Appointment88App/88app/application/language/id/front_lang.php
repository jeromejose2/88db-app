<?php

include_once('country_lang.php');

$lang['err_session_expired'] = 'Sesi telah berakhir, harap memuat ulang toko';
$lang['label'] = 'teks';

$lang['member_area'] = ' Anda Lihat {AppointmentTitle}';
$lang['member_login'] = ' Anda Lihat {AppointmentTitle}';
$lang['title_make_appointment'] = 'Buat {AppointmentTitle}';
$lang['tab_select_service'] = 'Pilih {ServiceTitle}';
$lang['tab_select_resource'] = 'Pilih {ResourceTitle}';
$lang['tab_select_schedule'] = 'Pilih {ScheduleTitle}';
$lang['tab_contace_info'] = 'Info Kontak';
$lang['tab_confirmation'] = 'Konfirmasi';
$lang['service_duration'] = 'Durasi';
$lang['service_cost'] = 'Harga';
$lang['next'] = 'Selanjutnya';
$lang['back'] = 'Kembali';
$lang['ok'] = 'OK';
$lang['confirm'] = 'Konfirmasi';
$lang['yes'] = 'YA';
$lang['no'] = 'TIDAK';
$lang['cancel'] = 'Batal';
$lang['am'] = 'AM';
$lang['pm'] = 'PM';
$lang['name'] = 'Nama';
$lang['first_name'] = 'Nama Depan';
$lang['last_name'] = 'Nama Belakang';
$lang['contact'] = 'Kontak';
$lang['phone_number'] = 'Nomor Telepon';
$lang['email'] = 'Email';
$lang['email_verify'] = 'Email (Pastikan)';
$lang['please_select_schedule'] = 'Silahkan Pilih {ScheduleTitle}';
$lang['not_available'] = 'Tidak tersedia';
$lang['start'] = 'Mulai';
$lang['end'] = 'Akhir';
$lang['timeslot_is_full'] = 'ini {ResourceTitle} sepenuhnya sudah dipesan. Silahkan pilih lainnya {ResourceTitle}.';

$lang['verify_firstname'] = 'Silahkan masukan nama depan';
$lang['verify_lastname'] = 'Silahkan masukan nama belakang';
$lang['verify_phone'] = ' Silahkan masukan nomor telepone';
$lang['verify_email'] = 'Silahkan masukan email';
$lang['verify_verifyemail'] = 'Silahkan masukan kepastian email';
$lang['invalid_phone'] = 'Nomor Telepon salah';
$lang['invalid_email'] = 'Email salah';
$lang['invalid_emailcompare'] = 'Email dan Email (pastikan) tidak cocok';
$lang['your_contact_information'] = 'Informasi Kontak Anda';
$lang['note_optional'] = 'Catatan khusus';
$lang['please_leave_message_here'] = 'Anda bisa meninggalkan catatan khusus atau instruksi ke toko';
$lang['verifycode'] = 'Masukan kode verifikasi';
$lang['verifyCodeError_dialog_title'] = 'Kode Verifikasi Salah';
$lang['verifyCodeError_dialog_content'] = 'Kode Verifikasi tidak tepat. Silahkan coba lagi.';
$lang['service'] = '{ServiceTitle}';
$lang['resource'] = '{ResourceTitle}';
$lang['schedule'] = '{ScheduleTitle}';
$lang['contact_info'] = 'Info kontak';
$lang['reminder'] = 'Pengingat';
$lang['send_me_email_reminder'] = 'Kirimkan saya pengingat email';
$lang['make_appointment'] = 'Buat {AppointmentTitle} ';
$lang['confirm_appointment'] = 'Konfirmasi {AppointmentTitle} ';

$lang['made_successfully'] = '{AppointmentTitle} Berhasil dibuat';
$lang['btn_view_booking_detail'] = 'Lihat Rincian ';
$lang['btn_make_new_appointment'] = 'Buat baru {AppointmentTitle}';

$lang['scheduled_appointment'] = 'Mendatang {AppointmentTitle}';
$lang['cancel_appointment'] = 'Batal {AppointmentTitle}';
$lang['cancel_appointment_to_sure'] = 'Apakah Anda yakin ingin membatalkan berikut {AppointmentTitle}?';

$lang['almost_done_with_your_booking'] = 'Selangkah lagi...';
$lang['almost_done_with_your_booking_description'] = '<p> Untuk melengkapi pesanan Anda {AppointmentTitle}, Anda harus pastikan akun keanggotaan Anda.</p> Sebuah email dengan URL verifikasi dari 88DB telah dikirim ke email Anda ({email}). 
Setelah selesainya verifikasi akun, Anda {AppointmentTitle} akan dikonfirmasikan secara otomatis';
$lang['open_your_email_inbox'] = 'Buka kotak masuk Email Anda';


$lang['time_format'] ='hh:mm(AM|PM)';
$lang['day_collection'] = 'Ming, Sen, Sel, Rab, Kam, Jum, sab,';
$lang['to'] = 'ke';
$lang['price_unit'] = 'HK$';
$lang['err_service_expired'] = '{ServiceTitle} Tidak tersedia';
$lang['err_resource_expired'] = '{ResourceTitle} Tidak tersedia .';
$lang['error_invalid_schedule_date'] = ' Tidak sah {ScheduleTitle} tanggal ';

$lang['timelostfull_dialog_title'] = 'Tempat waktu tidak tersedia';
$lang['timelostfull_dialog_content'] = 'Pilihan tempat waktu Anda sudah tidak tersedia lagi. Silahkan pilih tempat waktu yang lainnya.';
$lang['close'] = 'Tutup';

$lang['error_timeslot_is_full'] = 'Tempat waktu tidak tersedia';
$lang['error_any_fileds_is_empty'] = 'setiap bidang kosong.';
$lang['error_invalid_isreminder_value'] = 'Nilai Pengingat tidak benar';
$lang['error_no_timeslot_status_record'] = 'Tidak ada status tempat waktu yang terekam.';
$lang['error_deal_date_expired'] = 'Tanggal kesepakatan telah berakhir.';
$lang['error_selected_range_error'] = 'Tidak terpilih antara tanggal min dan tanggal max';
$lang['error_not_timeslot'] = 'Tidak pada tempat waktu';
$lang['error_special_time_error'] = 'Hari yang khusus, tapi bukan waktu yang tepat;';
$lang['error_is_not_week_day'] = 'bukan sehari-hari';
$lang['error_in_dayoff_day'] = 'ini di hari libur';


$lang['please_enter_valid_value'] = 'Harap masukan nilai yang benar ';
$lang['resource_no_provided_service'] = '{ResourceTitle} Pilihan tidak tersedia{ServiceTitle}.';
$lang['login_with_facebook'] = 'Masuk menggunakan Facebook';
$lang["tc"] = 'Dengan mengkonfirmasikan {AppointmentTitle}, Anda setuju untuk 88DB.com\'s <a href="%s" target="_blank"> Pernyataan pribadi </a> dan  <a href="%s">syarat  & ketentuan.</a>';
$lang['err_database_err'] = '<P>Maaf, server sementara tidak dapat {ServiceTitle} permintaan Anda. Anda dapat kembali ke <a href="javascript:void(0);" onclick="history.go(-1);">halaman sebelumnya</a> atau pergi ke  <a href="http://hk.88db.com/en/" target="_top">88DB.com</a>.<br /><br />Untuk bantuan lebih lanjut, silakan klik di sini <a href="mailto:cs@88db.com">Kontak kami </a>.</P>';
$lang['service_is_unavailable'] = '{AppointmentTitle} pesanan {ServiceTitle} tidak tersedia ';

$lang['datepicker_language'] = '';
$lang['duration_format'] = '%d Jam %d Menit';
$lang['view_detail'] = 'Lihat rincian ';
$lang['please_select_resouce'] =  'Silahkan pilih {ResourceTitle}';
$lang['tab_select_resource_schedule'] = 'Pilih {ResourceTitle} & {ScheduleTitle}';
$lang['current_day_no_timeslot'] = 'Hari yang Anda pilih tidak {ScheduleTitle} tersedia.';
$lang['available'] = 'Tersedia';
$lang['no_preference'] = 'Tidak ada pilihan';
$lang['no_available_resource'] = 'Tidak {ResourceTitle} tersedia';
$lang['select'] = 'Pilihan';
$lang['clear_selection'] = 'Pilihan Jelas';
