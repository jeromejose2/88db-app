<?php
$lang['new_appointment_to_customer_email_template'] = <<< EOD
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body style="margin:0; background:#d6d4d1">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#d6d4d1"  style="color: #494846; font-family: Arial, PMingLiu, Helvetica, sans-serif; font-size: 12px; -webkit-text-size-adjust: none;">
<tr>
<td>
<!--Email Content --><!--Header-->
<table width="773" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td align="center" height="15">&nbsp;</td></tr><!--Header--><tr><td>&nbsp;</td></tr><!--Body-->
<tr>
<td><table width="774" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="4">&nbsp;</td>
<td width="765" bgcolor="#FFFFFF">
<!--Body Content-->
<table width="710" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:arial,PMingLiu,verdana,sans-serif; font-size:12px; _font-size:13px; color: #494846; -webkit-text-size-adjust: none;">
<tbody><tr>
<td colspan="2" style="padding:15px 0">
<p>Kepada <strong><span>{FirstName} {LastName}</span></strong>:</p>
<p style="color:#494846; font-size:12px; margin:20px 0 0"><strong><span>Anda {Service} Janji dengan  {ShopName} konfirmasi.</span></strong></p>
</td>
</tr>
{SeperateLine}
<tr>
<td colspan="2" style="padding:10px; background:#FCF8E8" bgcolor="#FCF8E8">
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Layanan</strong> ： </td>
<td><p style="margin:5px 0">{Service}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Sumber</strong> ： </td>
<td><p style="margin:5px 0">{Resource}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Waktu</strong> ： </td>
<td><p style="margin:5px 0">{Schedule}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Harga</strong> ： </td>
<td><p style="margin:5px 0">{Price}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Catatan</strong> ： </td>
<td><p style="margin:5px 0">{Note}</p></td>
</tr>
</tbody></table>
</td>
</tr>
{SeperateLine}
<tr>
<td colspan="2" style="padding:0">
<p>Dengan Hormat,</p>
<p>Dari Pemilik Shop {ShopName}</p>
</td>
</tr>
<tr>
<td style="padding:0 ">&nbsp;</td>
</tr>
</tbody></table>
&nbsp;
</td>
<td width="4">&nbsp;</td>
</tr>
</table>
</td>
</tr>
<!--Footer-->
<tr>
<td >&nbsp;</td>
</tr>
<!--copyRight-->
<tr>
<td>&nbsp;</td>
</tr>
</table></td>
</tr>
</table>
</body>
</html>
EOD;

$lang['new_appointment_to_owner_email_template'] = <<< EOD
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body style="margin:0; background:#d6d4d1">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#d6d4d1"  style="color: #494846; font-family: Arial, PMingLiu, Helvetica, sans-serif; font-size: 12px; -webkit-text-size-adjust: none;">
<tr>
<td>
<!--Email Content --><!--Header-->
<table width="773" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td align="center" height="15">&nbsp;</td></tr><!--Header--><tr><td>&nbsp;</td></tr><!--Body-->
<tr>
<td><table width="774" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="4">&nbsp;</td>
<td width="765" bgcolor="#FFFFFF">
<!--Body Content-->
<table width="710" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:arial,PMingLiu,verdana,sans-serif; font-size:12px; _font-size:13px; color: #494846; -webkit-text-size-adjust: none;">
<tbody><tr>
<td colspan="2" style="padding:15px 0">
<p>Kepada Pemilik Shop <strong><span>{ShopName}</span></strong>:</p>
<p style="color:#494846; font-size:12px; margin:20px 0 0"><strong><span>Buat sebuah janji baru.</span></strong></p>
</td>
</tr>
{SeperateLine}
<tr>
<td colspan="2" style="padding:10px; background:#FCF8E8" bgcolor="#FCF8E8">
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Pelanggan</strong> ： </td>
<td><p style="margin:5px 0">{FirstName} {LastName}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Email</strong> ： </td>
<td><p style="margin:5px 0">{Email}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Telepon</strong> ： </td>
<td><p style="margin:5px 0">{Phone}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Layanan</strong> ： </td>
<td><p style="margin:5px 0">{Service}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Sumber</strong> ： </td>
<td><p style="margin:5px 0">{Resource}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Waktu</strong> ： </td>
<td><p style="margin:5px 0">{Schedule}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Harga</strong> ： </td>
<td><p style="margin:5px 0">{Price}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Catatan</strong> ： </td>
<td><p style="margin:5px 0">{Note}</p></td>
</tr>
</tbody></table>
</td>
</tr>
{SeperateLine}
<tr>
<td colspan="2" style="padding:0">
<p>Yours Sincerely,</p>
<p>88DB Customer Services</p>
<a href="http://www.88db.com/">http://www.88db.com/</a>
</td>
</tr>
<tr>
<td style="padding:0 ">&nbsp;</td>
</tr>
</tbody></table>
&nbsp;
</td>
<td width="4">&nbsp;</td>
</tr>
</table>
</td>
</tr>
<!--Footer-->
<tr>
<td >&nbsp;</td>
</tr>
<!--copyRight-->
<tr>
<td>&nbsp;</td>
</tr>
</table></td>
</tr>
</table>
</body>
</html>
EOD;

$lang['cancel_appointment_to_customer_email_template'] = <<< EOD
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body style="margin:0; background:#d6d4d1">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#d6d4d1"  style="color: #494846; font-family: Arial, PMingLiu, Helvetica, sans-serif; font-size: 12px; -webkit-text-size-adjust: none;">
<tr>
<td>
<!--Email Content --><!--Header-->
<table width="773" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td align="center" height="15">&nbsp;</td></tr><!--Header--><tr><td>&nbsp;</td></tr><!--Body-->
<tr>
<td><table width="774" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="4">&nbsp;</td>
<td width="765" bgcolor="#FFFFFF">
<!--Body Content-->
<table width="710" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:arial,PMingLiu,verdana,sans-serif; font-size:12px; _font-size:13px; color: #494846; -webkit-text-size-adjust: none;">
<tbody><tr>
<td colspan="2" style="padding:15px 0">
<p>Kepada <strong><span>{FirstName} {LastName}</span></strong>:</p>
<p style="color:#494846; font-size:12px; margin:20px 0 0"><strong><span>
Anda {Service} janji dengan {ShopName} telah dibatalkan.
</span></strong></p>
</td>
</tr>
{SeperateLine}
<tr>
<td colspan="2" style="padding:10px; background:#FCF8E8" bgcolor="#FCF8E8">
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Layanan</strong> ： </td>
<td><p style="margin:5px 0">{Service}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Sumber</strong> ： </td>
<td><p style="margin:5px 0">{Resource}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Waktu</strong> ： </td>
<td><p style="margin:5px 0">{Schedule}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Harga</strong> ： </td>
<td><p style="margin:5px 0">{Price}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Catatan</strong> ： </td>
<td><p style="margin:5px 0">{Note}</p></td>
</tr>
</tbody></table>
{Reason}
</td>
</tr>
{SeperateLine}
<tr>
<td colspan="2" style="padding:0">
<p>Dengan Hormat,</p>
<p>Dari Pemilik Shop {ShopName}</p>
</td>
</tr>
<tr>
<td style="padding:0 ">&nbsp;</td>
</tr>
</tbody></table>
&nbsp;
</td>
<td width="4">&nbsp;</td>
</tr>
</table>
</td>
</tr>
<!--Footer-->
<tr>
<td >&nbsp;</td>
</tr>
<!--copyRight-->
<tr>
<td>&nbsp;</td>
</tr>
</table></td>
</tr>
</table>
</body>
</html>
EOD;

$lang['cancel_appointment_reason'] = <<< EOD
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Alasan Pembatalan</strong> ： </td>
<td><p style="margin:5px 0">{Reason}</p></td>
</tr>
</tbody></table>
EOD;

$lang['cancel_appointment_to_owner_email_template'] = <<< EOD
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body style="margin:0; background:#d6d4d1">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#d6d4d1"  style="color: #494846; font-family: Arial, PMingLiu, Helvetica, sans-serif; font-size: 12px; -webkit-text-size-adjust: none;">
<tr>
<td>
<!--Email Content --><!--Header-->
<table width="773" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td align="center" height="15">&nbsp;</td></tr><!--Header--><tr><td>&nbsp;</td></tr><!--Body-->
<tr>
<td><table width="774" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="4">&nbsp;</td>
<td width="765" bgcolor="#FFFFFF">
<!--Body Content-->
<table width="710" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:arial,PMingLiu,verdana,sans-serif; font-size:12px; _font-size:13px; color: #494846; -webkit-text-size-adjust: none;">
<tbody><tr>
<td colspan="2" style="padding:15px 0">
<p>Kepada Pemilik Shop <strong><span>{ShopName}</span></strong>:</p>
<p style="color:#494846; font-size:12px; margin:20px 0 0"><strong><span>Di bawah ini  perjanjian telah dibatalkan.</span></strong></p>
</td>
</tr>
{SeperateLine}
<tr>
<td colspan="2" style="padding:10px; background:#FCF8E8" bgcolor="#FCF8E8">
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Pelanggan</strong> ： </td>
<td><p style="margin:5px 0">{FirstName} {LastName}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Email</strong> ： </td>
<td><p style="margin:5px 0">{Email}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Telepon</strong> ： </td>
<td><p style="margin:5px 0">{Phone}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Layanan</strong> ： </td>
<td><p style="margin:5px 0">{Service}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Sumber</strong> ： </td>
<td><p style="margin:5px 0">{Resource}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Waktu</strong> ： </td>
<td><p style="margin:5px 0">{Schedule}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Harga</strong> ： </td>
<td><p style="margin:5px 0">{Price}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Catatan</strong> ： </td>
<td><p style="margin:5px 0">{Note}</p></td>
</tr>
</tbody></table>
</td>
</tr>
{SeperateLine}
<tr>
<td colspan="2" style="padding:0">
<p>Yours Sincerely,</p>
<p>88DB Customer Services</p>
<a href="http://www.88db.com/">http://www.88db.com/</a>
</td>
</tr>
<tr>
<td style="padding:0 ">&nbsp;</td>
</tr>    
</tbody></table>
&nbsp;
</td>
<td width="4">&nbsp;</td>
</tr>
</table>
</td>
</tr>
<!--Footer-->
<tr>
<td >&nbsp;</td>
</tr>
<!--copyRight-->
<tr>
<td>&nbsp;</td>
</tr>
</table></td>
</tr>
</table>
</body>
</html>
EOD;

$lang['reminder_to_customer_email_template'] = <<< EOD
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body style="margin:0; background:#d6d4d1">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#d6d4d1"  style="color: #494846; font-family: Arial, PMingLiu, Helvetica, sans-serif; font-size: 12px; -webkit-text-size-adjust: none;">
<tr>
<td>
<!--Email Content --><!--Header-->
<table width="773" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td align="center" height="15">&nbsp;</td></tr><!--Header--><tr><td>&nbsp;</td></tr><!--Body-->
<tr>
<td><table width="774" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="4">&nbsp;</td>
<td width="765" bgcolor="#FFFFFF">
<!--Body Content-->
<table width="710" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:arial,PMingLiu,verdana,sans-serif; font-size:12px; _font-size:13px; color: #494846; -webkit-text-size-adjust: none;">
<tr>
<td colspan="2" style="padding:15px 0">
<p>Kepada <strong><span>{FirstName} {LastName}</span></strong>:</p>
<p style="color:#494846; font-size:12px; margin:20px 0 0"><strong><span>
Mohon diingat bahwa Anda memiliki janji berikut dengan kami.
</span></strong></p>
</td>
</tr>
{SeperateLine}
<tr>
<td colspan="2" style="padding:10px; background:#FCF8E8" bgcolor="#FCF8E8" >
<table width="680" border="0" cellspacing="0" cellpadding="0"  style="line-height:18px">
<tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Layanan</strong> ： </td>
<td><p style="margin:5px 0">{Service}</p></td>
</tr>
</table>
<table width="680" border="0" cellspacing="0" cellpadding="0"  style="line-height:18px">
<tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Sumber</strong> ： </td>
<td><p style="margin:5px 0">{Resource}</p></td>
</tr>
</table>
<table width="680" border="0" cellspacing="0" cellpadding="0"  style="line-height:18px">
<tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Waktu</strong> ： </td>
<td><p style="margin:5px 0">{Schedule}</p></td>
</tr>
</table>
<table width="680" border="0" cellspacing="0" cellpadding="0"  style="line-height:18px">
<tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Harga</strong> ： </td>
<td><p style="margin:5px 0">{Price}</p></td>
</tr>
</table>
<table width="680" border="0" cellspacing="0" cellpadding="0"  style="line-height:18px">
<tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Catatan</strong> ： </td>
<td><p style="margin:5px 0">{Note}</p></td>
</tr>
</table></td>
</tr>
{SeperateLine}
<tr>
<td colspan="2" style="padding:0">
<p>Dengan Hormat,</p>
<p>Dari Pemilik Shop {ShopName}</p>
</td>
</tr>
<tr>
<td  style="padding:0 ">&nbsp;</td>
</tr>
</table>
&nbsp;
</td>
<td width="4">&nbsp;</td>
</tr>
</table>
</td>
</tr>
<!--Footer-->
<tr>
<td >&nbsp;</td>
</tr>
<!--copyRight-->
<tr>
<td>&nbsp;</td>
</tr>
</table></td>
</tr>
</table>
</body>
</html>
EOD;

$lang['reminder_to_owner_email_template'] = <<< EOD
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body style="margin:0; background:#d6d4d1">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#d6d4d1"  style="color: #494846; font-family: Arial, PMingLiu, Helvetica, sans-serif; font-size: 12px; -webkit-text-size-adjust: none;">
<tr>
<td>
<!--Email Content --><!--Header-->
<table width="773" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td align="center" height="15">&nbsp;</td></tr><!--Header--><tr><td>&nbsp;</td></tr><!--Body-->
<tr>
<td><table width="774" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="4">&nbsp;</td>
<td width="765" bgcolor="#FFFFFF">
<!--Body Content-->
<table width="710" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:arial,PMingLiu,verdana,sans-serif; font-size:12px; _font-size:13px; color: #494846; -webkit-text-size-adjust: none;">
<tbody><tr>
<td colspan="2" style="padding:15px 0">
<p>Kepada Pemilik Shop <strong><span>{ShopName}</span></strong>:</p>
<p style="color:#494846; font-size:12px; margin:20px 0 0"><strong><span>
Mohon diingat janji berikut.
</span></strong></p>
</td>
</tr>
{SeperateLine}
<tr>
<td colspan="2" style="padding:10px; background:#FCF8E8" bgcolor="#FCF8E8">
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Pelanggan</strong> ： </td>
<td><p style="margin:5px 0">{FirstName} {LastName}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Email</strong> ： </td>
<td><p style="margin:5px 0">{Email}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Telepon</strong> ： </td>
<td><p style="margin:5px 0">{Phone}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Layanan</strong> ： </td>
<td><p style="margin:5px 0">{Service}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Sumber</strong> ： </td>
<td><p style="margin:5px 0">{Resource}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Waktu</strong> ： </td>
<td><p style="margin:5px 0">{Schedule}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Harga</strong> ： </td>
<td><p style="margin:5px 0">{Price}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Catatan</strong> ： </td>
<td><p style="margin:5px 0">{Note}</p></td>
</tr>
</tbody></table>
</td>
</tr>
{SeperateLine}
<tr>
<td colspan="2" style="padding:0">
<p>Yours Sincerely,</p>
<p>88DB Customer Services</p>
<a href="http://www.88db.com/">http://www.88db.com/</a>
</td>
</tr>
<tr>
<td style="padding:0 ">&nbsp;</td>
</tr>
</tbody></table>
&nbsp;
</td>
<td width="4">&nbsp;</td>
</tr>
</table>
</td>
</tr>
<!--Footer-->
<tr>
<td >&nbsp;</td>
</tr>
<!--copyRight-->
<tr>
<td>&nbsp;</td>
</tr>
</table></td>
</tr>
</table>
</body>
</html>
EOD;

$lang['update_appointment_to_customer_email_template'] = <<< EOD
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body style="margin:0; background:#d6d4d1">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#d6d4d1"  style="color: #494846; font-family: Arial, PMingLiu, Helvetica, sans-serif; font-size: 12px; -webkit-text-size-adjust: none;">
<tr>
<td>
<!--Email Content --><!--Header-->
<table width="773" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td align="center" height="15">&nbsp;</td></tr><!--Header--><tr><td>&nbsp;</td></tr><!--Body-->
<tr>
<td><table width="774" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="4">&nbsp;</td>
<td width="765" bgcolor="#FFFFFF">
<!--Body Content-->
<table width="710" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:arial,PMingLiu,verdana,sans-serif; font-size:12px; _font-size:13px; color: #494846; -webkit-text-size-adjust: none;">
<tbody><tr>
<td colspan="2" style="padding:15px 0">
<p>Kepada <strong><span>{FirstName} {LastName}</span></strong>:</p>
<p style="color:#494846; font-size:12px; margin:20px 0 0"><strong><span>Anda {Service} Janji dengan {ShopName} telah diperbarui.</span></strong></p>
</td>
</tr>
{SeperateLine}
<tr>
<td colspan="2" style="padding:10px; background:#FCF8E8" bgcolor="#FCF8E8">
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Layanan</strong> ： </td>
<td><p style="margin:5px 0">{Service}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Sumber</strong> ： </td>
<td><p style="margin:5px 0">{Resource}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Waktu</strong> ： </td>
<td><p style="margin:5px 0">{Schedule}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Harga</strong> ： </td>
<td><p style="margin:5px 0">{Price}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Catatan</strong> ： </td>
<td><p style="margin:5px 0">{Note}</p></td>
</tr>
</tbody></table>
</td>
</tr>
{SeperateLine}
<tr>
<td colspan="2" style="padding:0">
<p>Dengan Hormat,</p>
<p>Dari Pemilik Shop {ShopName}</p>
</td>
</tr>
<tr>
<td style="padding:0 ">&nbsp;</td>
</tr>
</tbody></table>
&nbsp;
</td>
<td width="4">&nbsp;</td>
</tr>
</table>
</td>
</tr>
<!--Footer-->
<tr>
<td >&nbsp;</td>
</tr>
<!--copyRight-->
<tr>
<td>&nbsp;</td>
</tr>
</table></td>
</tr>
</table>
</body>
</html>
EOD;

$lang['update_appointment_to_owner_email_template'] = <<< EOD
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body style="margin:0; background:#d6d4d1">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#d6d4d1"  style="color: #494846; font-family: Arial, PMingLiu, Helvetica, sans-serif; font-size: 12px; -webkit-text-size-adjust: none;">
<tr>
<td>
<!--Email Content --><!--Header-->
<table width="773" border="0" align="center" cellpadding="0" cellspacing="0"><tr><td align="center" height="15">&nbsp;</td></tr><!--Header--><tr><td>&nbsp;</td></tr><!--Body-->
<tr>
<td><table width="774" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="4">&nbsp;</td>
<td width="765" bgcolor="#FFFFFF">
<!--Body Content-->
<table width="710" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:arial,PMingLiu,verdana,sans-serif; font-size:12px; _font-size:13px; color: #494846; -webkit-text-size-adjust: none;">
<tbody><tr>
<td colspan="2" style="padding:15px 0">
<p>Kepada Pemilik Shop <strong><span>{ShopName}</span></strong>:</p>
<p style="color:#494846; font-size:12px; margin:20px 0 0"><strong><span>Di bawah ini perjanjian telah diperbarui.</span></strong></p>
</td>
</tr>
{SeperateLine}
<tr>
<td colspan="2" style="padding:10px; background:#FCF8E8" bgcolor="#FCF8E8">
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Pelanggan</strong> ： </td>
<td><p style="margin:5px 0">{FirstName} {LastName}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Email</strong> ： </td>
<td><p style="margin:5px 0">{Email}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Telepon</strong> ： </td>
<td><p style="margin:5px 0">{Phone}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Layanan</strong> ： </td>
<td><p style="margin:5px 0">{Service}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Sumber</strong> ： </td>
<td><p style="margin:5px 0">{Resource}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Waktu</strong> ： </td>
<td><p style="margin:5px 0">{Schedule}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Harga</strong> ： </td>
<td><p style="margin:5px 0">{Price}</p></td>
</tr>
</tbody></table>
<table width="680" border="0" cellspacing="0" cellpadding="0" style="line-height:18px">
<tbody><tr valign="top">
<td width="11%" style="padding-top:5px"><strong>Catatan</strong> ： </td>
<td><p style="margin:5px 0">{Note}</p></td>
</tr>
</tbody></table>
</td>
</tr>
{SeperateLine}
<tr>
<td colspan="2" style="padding:0">
<p>Yours Sincerely,</p>
<p>88DB Customer Services</p>
<a href="http://www.88db.com/">http://www.88db.com/</a>
</td>
</tr>
<tr>
<td style="padding:0 ">&nbsp;</td>
</tr>
</tbody></table>
&nbsp;
</td>
<td width="4">&nbsp;</td>
</tr>
</table>
</td>
</tr>
<!--Footer-->
<tr>
<td >&nbsp;</td>
</tr>
<!--copyRight-->
<tr>
<td>&nbsp;</td>
</tr>
</table></td>
</tr>
</table>
</body>
</html>
EOD;

$lang['new_appointment_to_customer_email_subject'] = 'Konfirmasi  %s janji dengan %s';
$lang['new_appointment_to_owner_email_subject'] = 'Baru %s janji ';
$lang['cancel_appointment_to_customer_by_customer_email_subject'] = 'Pembatalan % janji dengan %s';
$lang['cancel_appointment_to_customer_by_owner_email_subject'] = 'Pembatalan %s janji dengan %s dari pemilik';
$lang['cancel_appointment_to_owner_by_customer_email_subject'] = 'Pembatalan %s janji dari pelanggan';
$lang['cancel_appointment_to_owner_by_owner_email_subject'] = 'Pembatalan %s janji';
$lang['reminder_to_customer_email_subject'] = 'Pengingatan untuk %s janji dengan %s';
$lang['reminder_to_owner_email_subject'] = 'Pengingatan untuk %s janji';
$lang['update_appointment_to_customer_email_subject'] = '%s Janji dengan %s telah diperbarui';
$lang['update_appointment_to_owner_email_subject'] = '%s Janji telah diperbarui';
