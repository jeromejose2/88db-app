<?php

include_once('country_lang.php');

$lang['err_session_expired'] = 'Session has been expired, please reload the shop';
$lang['label'] = 'text';

$lang['member_area'] = 'View Your {AppointmentTitle}';
$lang['member_login'] = 'View Your {AppointmentTitle}';
$lang['title_make_appointment'] = 'Make {AppointmentTitle}';
$lang['tab_select_service'] = 'Select {ServiceTitle}';
$lang['tab_select_resource'] = 'Select {ResourceTitle}';
$lang['tab_select_schedule'] = 'Select {ScheduleTitle}';
$lang['tab_contace_info'] = 'Contact Info';
$lang['tab_confirmation'] = 'Confirmation';
$lang['service_duration'] = 'Duration';
$lang['service_cost'] = 'Price';
$lang['next'] = 'Next';
$lang['back'] = 'Back';
$lang['ok'] = 'OK';
$lang['confirm'] = 'Confirm';
$lang['yes'] = 'YES';
$lang['no'] = 'NO';
$lang['cancel'] = 'Cancel';
$lang['am'] = 'AM';
$lang['pm'] = 'PM';
$lang['name'] = 'Name';
$lang['first_name'] = 'First Name';
$lang['last_name'] = 'Last Name';
$lang['contact'] = 'Contact';
$lang['phone_number'] = 'Phone Number';
$lang['email'] = 'Email';
$lang['email_verify'] = 'Email (Verify)';
$lang['please_select_schedule'] = 'Please select a {ScheduleTitle}';
$lang['not_available'] = 'Not Available';
$lang['start'] = 'Start';
$lang['end'] = 'End';
$lang['timeslot_is_full'] = 'The {ResourceTitle} is fully booked. Plesae select another {ResourceTitle}.';

$lang['verify_firstname'] = 'please input first name';
$lang['verify_lastname'] = 'please input last name';
$lang['verify_phone'] = 'please input phone number';
$lang['verify_email'] = 'please input email';
$lang['verify_verifyemail'] = 'please input verify email';
$lang['invalid_phone'] = 'invalid phone number';
$lang['invalid_email'] = 'invalid email';
$lang['invalid_emailcompare'] = 'Email and Email (Verify) do not match';
$lang['your_contact_information'] = 'Your Contact Information.';
$lang['note_optional'] = 'Special Note';
$lang['please_leave_message_here'] = 'You may leave any special note or instruction to the shop';
$lang['verifycode'] = 'Enter Verify Code';
$lang['verifyCodeError_dialog_title'] = 'Verify Code Error';
$lang['verifyCodeError_dialog_content'] = 'Verify Code is incorrect. Please try again.';
$lang['service'] = '{ServiceTitle}';
$lang['resource'] = '{ResourceTitle}';
$lang['schedule'] = '{ScheduleTitle}';
$lang['contact_info'] = 'Contact Info';
$lang['reminder'] = 'Reminder';
$lang['send_me_email_reminder'] = 'Send me email reminder ';
$lang['make_appointment'] = 'Make {AppointmentTitle} ';
$lang['confirm_appointment'] = 'Confirm {AppointmentTitle} ';

$lang['made_successfully'] = '{AppointmentTitle} Made Successfully ';
$lang['btn_view_booking_detail'] = 'View Detail ';
$lang['btn_make_new_appointment'] = 'Make New {AppointmentTitle}';

$lang['scheduled_appointment'] = 'Upcoming {AppointmentTitle}';
$lang['cancel_appointment'] = 'Cancel {AppointmentTitle}';
$lang['cancel_appointment_to_sure'] = 'Are you sure you want to cancel below {AppointmentTitle}?';

$lang['almost_done_with_your_booking'] = 'One more step ...';
$lang['almost_done_with_your_booking_description'] = '<p>To complete the booking of your {AppointmentTitle}, you must verify your member account.</p> An email with the verification URL from 88DB has been sent to your email ({email}). Upon the completion of the account verification, your {AppointmentTitle} will be confirmed automatically.';
$lang['open_your_email_inbox'] = 'Open Your Email Inbox';


$lang['time_format'] ='hh:mm(AM|PM)';
$lang['day_collection'] = 'Sun,Mon,Tue,Wed,Thu,Fri,Sat';
$lang['to'] = 'to';
$lang['price_unit'] = 'HK$';
$lang['err_service_expired'] = '{ServiceTitle} is not available.';
$lang['err_resource_expired'] = '{ResourceTitle} is not available.';
$lang['error_invalid_schedule_date'] = 'invalid {ScheduleTitle} date.';

$lang['timelostfull_dialog_title'] = 'Time slot is not available';
$lang['timelostfull_dialog_content'] = 'Your selected time slot is no longer available. Please select another time slot.';
$lang['close'] = 'Close';

$lang['error_timeslot_is_full'] = 'time slot is not available';
$lang['error_any_fileds_is_empty'] = 'any fields is empty.';
$lang['error_invalid_isreminder_value'] = 'invalid isReminder value';
$lang['error_no_timeslot_status_record'] = 'no time slot status record.';
$lang['error_deal_date_expired'] = 'deal date has expired.';
$lang['error_selected_range_error'] = 'selected don\'t between min date and max date';
$lang['error_not_timeslot'] = 'is not in time slot.';
$lang['error_special_time_error'] = 'is special day, but not in special time;';
$lang['error_is_not_week_day'] = 'is not week day.';
$lang['error_in_dayoff_day'] = 'is in day off day.';


$lang['please_enter_valid_value'] = 'Please enter the valid value. ';
$lang['resource_no_provided_service'] = '{ResourceTitle} do not provide selected {ServiceTitle}.';
$lang['login_with_facebook'] = 'Login with Facebook';
$lang["tc"] = 'By confirming the {AppointmentTitle}, you agree to 88DB.com\'s <a href="%s" target="_blank"> Privacy Statement</a> and <a href="%s" target="_blank">Terms &amp; Conditions.</a>';
$lang['err_database_err'] = '<P>Sorry, the server is temporarily unable to {ServiceTitle} your request. You can return to the <a href="javascript:void(0);" onclick="history.go(-1);">previous page</a> or go to <a href="http://hk.88db.com/en/" target="_top">88DB.com</a>.<br /><br />For more help, please click here to <a href="mailto:cs@88db.com">contact us</a>.</P>';
$lang['service_is_unavailable'] = '{AppointmentTitle} Booking {ServiceTitle} is not available.';

$lang['datepicker_language'] = '';
$lang['duration_format'] = '%dh %dm';
$lang['view_detail'] = 'View Detail';
$lang['please_select_resouce'] = 'Please select a {ResourceTitle}';
$lang['tab_select_resource_schedule'] = 'Select {ResourceTitle} & {ScheduleTitle}';
$lang['current_day_no_timeslot'] = 'Your selected day has no any {ScheduleTitle}.';
$lang['available'] = 'Available';
$lang['no_preference'] = 'No Preference';
$lang['no_available_resource'] = 'No any available {ResourceTitle}';
$lang['select'] = 'Select';
$lang['clear_selection'] = 'Clear Selection';
