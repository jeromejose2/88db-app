<?php 

include_once('country_lang.php');

$lang['err_session_expired'] = 'เซสชั่นนี้ได้หมดอายุแล้ว กรุณาโหลดข้อมูลใหม่อีกครั้ง';
$lang['label'] = 'ข้อความ';

$lang['member_area'] = 'ดู{AppointmentTitle}';
$lang['member_login'] = 'ดู{AppointmentTitle}';
$lang['title_make_appointment'] = 'ทำ{AppointmentTitle}';
$lang['tab_select_service'] = 'เลือก{ServiceTitle}';
$lang['tab_select_resource'] = 'เลือก{ResourceTitle}';
$lang['tab_select_schedule'] = 'เลือก{ScheduleTitle}';
$lang['tab_contace_info'] = 'ข้อมูลติดต่อ';
$lang['tab_confirmation'] = 'การยืนยัน';
$lang['service_duration'] = 'ระยะเวลาที่ให้บริการ';
$lang['service_cost'] = 'ราคา';
$lang['next'] = 'ต่อไป';
$lang['back'] = 'กลับไป';
$lang['ok'] = 'ตกลง';
$lang['confirm'] = 'ยืนยัน';
$lang['yes'] = 'ใช่';
$lang['no'] = 'ไม่ใช่';
$lang['cancel'] = 'ยกเลิก';
$lang['am'] = 'AM';
$lang['pm'] = 'PM';
$lang['name'] = 'ชื่อ';
$lang['first_name'] = 'ชื่อ';
$lang['last_name'] = 'นามสกุล';
$lang['contact'] = 'ติดต่อ';
$lang['phone_number'] = 'เบอร์โทรศัพท์';
$lang['email'] = 'อีเมล';
$lang['email_verify'] = 'อีเมล (ยืนยัน)';
$lang['please_select_schedule'] = 'กรุณาระบุ{ScheduleTitle}';
$lang['not_available'] = 'ไม่ว่าง';
$lang['start'] = 'เริ่ม';
$lang['end'] = 'สิ้นสุด';
$lang['timeslot_is_full'] = '{ResourceTitle}ถูกจองเต็มแล้ว กรุณาระบุ{ResourceTitle}อื่น';

$lang['verify_firstname'] = 'กรุณาระบุชื่อ';
$lang['verify_lastname'] = 'กรุณาระบุนามสกุล';
$lang['verify_phone'] = 'กรุณาระบุเบอร์โทรศัพท์';
$lang['verify_email'] = 'กรุณาระบุอีเมล';
$lang['verify_verifyemail'] = 'กรุณาระบุอีเมลอีกครั้ง';
$lang['invalid_phone'] = 'เบอร์โทรศัพท์ไม่ถูกต้อง';
$lang['invalid_email'] = 'อีเมลไม่ถูกต้อง';
$lang['invalid_emailcompare'] = 'อีเมลและอีเมลยืนยันไม่ตรงกัน';
$lang['your_contact_information'] = 'ข้อมูลการติดต่อของคุณ';
$lang['note_optional'] = 'หมายเหตุพิเศษ';
$lang['please_leave_message_here'] = 'กรุณาระบุคำร้องขอพิเศษถึงร้าน';
$lang['verifycode'] = 'ระบุรหัสยืนยัน ';
$lang['verifyCodeError_dialog_title'] = 'รหัสยืนยันผิดพลาด';
$lang['verifyCodeError_dialog_content'] = 'รหัสยืนยันไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง';
$lang['service'] = '{ServiceTitle}';
$lang['resource'] = '{ResourceTitle}';
$lang['schedule'] = '{ScheduleTitle}';
$lang['contact_info'] = 'ข้อมูลติดต่อ';
$lang['reminder'] = 'แจ้งเตือน';
$lang['send_me_email_reminder'] = 'ส่งอีเมลแจ้งเตือน ';
$lang['make_appointment'] = 'ทำ{AppointmentTitle}';
$lang['confirm_appointment'] = 'ยืนยัน{AppointmentTitle}';

$lang['made_successfully'] = '{AppointmentTitle}เรียบร้อยแล้ว ';
$lang['btn_view_booking_detail'] = 'ดูรายละเอียด ';
$lang['btn_make_new_appointment'] = 'เพิ่ม{AppointmentTitle}ใหม่';

$lang['scheduled_appointment'] = '{AppointmentTitle}ต่อไป';
$lang['cancel_appointment'] = 'ยกเลิก{AppointmentTitle}';
$lang['cancel_appointment_to_sure'] = 'ยืนยันการยกเลิก{AppointmentTitle}ด้านล่างนี้? ';

$lang['almost_done_with_your_booking'] = 'อีกหนึ่งขั้นตอน ...';
$lang['almost_done_with_your_booking_description'] = '<p>เพื่อให้{AppointmentTitle}สมบูรณ์ กรุณายืนยันสมาชิกผ่านอีเมลของคุณ</p> อีเมลพร้อมลิงค์ยืนยันได้ส่งไปยังอีเมลของคุณเรียบร้อยแล้ว ({email}). เมื่อคลิกยืนยันผ่านอีเมลเรียบร้อยแล้ว {AppointmentTitle}ของคุณจะสมบูรณ์โดยอัตโนมัติ';
$lang['open_your_email_inbox'] = 'เปิดกล่องอีเมลข้อความเข้าของคุณ';


$lang['time_format'] ='hh:mm(AM|PM)';
$lang['day_collection'] = 'อา.,จ.,อ.,พ.,พฤ.,ศ.,ส.';
$lang['to'] = 'ถึง';
$lang['price_unit'] = 'บาท';
$lang['err_service_expired'] = '{ServiceTitle}ไม่สามารถใช้ได้';
$lang['err_resource_expired'] = '{ResourceTitle}ไม่ว่าง';
$lang['error_invalid_schedule_date'] = 'วันที่{ScheduleTitle}ไม่ถูกต้อง';

$lang['timelostfull_dialog_title'] = 'เวลาไม่ว่าง';
$lang['timelostfull_dialog_content'] = 'ช่วงเวลาที่คุณเลือกไม่สามารถใช้ได้ กรุณาเลือกช่วงเวลาอื่น';
$lang['close'] = 'ปิด';

$lang['error_timeslot_is_full'] = 'ช่วงเวลาไม่ว่าง';
$lang['error_any_fileds_is_empty'] = 'ข้อมูลไม่ครบถ้วน';
$lang['error_invalid_isreminder_value'] = 'ค่าการแจ้งเตือนไม่ถูกต้อง';
$lang['error_no_timeslot_status_record'] = 'ไม่มีประวัติการบันทึกเวลา';
$lang['error_deal_date_expired'] = 'วันที่ดีลได้หมดอายุแล้ว ';
$lang['error_selected_range_error'] = 'เลือกวันที่ระหว่างวันเริ่มและวันสิ้นสุด';
$lang['error_not_timeslot'] = 'ไม่ได้อยู่ในช่วงเวลา';
$lang['error_special_time_error'] = 'เป็นวันพิเศษ แต่ไม่ได้อยู่ในเวลาพิเศษ;';
$lang['error_is_not_week_day'] = 'ไม่ได้เป็นวันธรรมดา';
$lang['error_in_dayoff_day'] = 'เป็นวันหยุด';


$lang['please_enter_valid_value'] = 'กรุณาระบุค่าที่ถูกต้อง';
$lang['resource_no_provided_service'] = '{ResourceTitle}ไม่สามารถให้{ServiceTitle}ที่ถูกเลือก';
$lang['login_with_facebook'] = 'เข้าสู่ระบบด้วย Facebook';
$lang["tc"] = 'การคลิก{AppointmentTitle} หมายถึงคุณยอมรับใน<a href="%s" target="_blank"> ข้อกำหนด</a> และ <a href="%s" target="_blank">เงื่อนไข</a> ของ 88DB.com';
$lang['err_database_err'] = '<P>ขออภัย เซิร์ฟเวอร์ไม่สามารถใช้งานได้ชั่วคราว คุณสามารถกลับไปหน้า <a href="javascript:void(0);" onclick="history.go(-1);">ก่อนหน้า</a> หรือไปยัง <a href="http://th.88db.com" target="_top">88DB.com</a>.<br /><br />หากท่านต้องการความช่วยเหลือเพิ่มเติม กรุณาคลิกที่นี่เพื่อ <a href="mailto:cs@88db.co.th">ติดต่อเรา</a>.</P>';
$lang['service_is_unavailable'] = 'การจอง{AppointmentTitle}{ServiceTitle}ไม่สามารถใช้ได้';

$lang['datepicker_language'] = '';
$lang['duration_format'] = '%dh %dm';
$lang['view_detail'] = 'ดูรายละเอียด';
$lang['please_select_resouce'] = 'กรุณาระบุ{ResourceTitle}';
$lang['tab_select_resource_schedule'] = 'ระบุ{ResourceTitle} & {ScheduleTitle}';
$lang['current_day_no_timeslot'] = '{ScheduleTitle}วันที่เลือกไม่ว่าง';
$lang['available'] = 'ว่าง';
$lang['no_preference'] = 'ไม่เจาะจง';
$lang['no_available_resource'] = '{ResourceTitle} ไม่ว่าง';
$lang['select'] = 'เลือก';
$lang['clear_selection'] = 'ล้างข้อมูลที่เลือก';
