<?php

include_once('country_lang.php');

$lang['err_session_expired'] = '此部分已過期，請重新載入商店';
$lang['label'] = '文字';

$lang['member_area'] = '查看{AppointmentTitle}';
$lang['member_login'] = '查看{AppointmentTitle}';
$lang['title_make_appointment'] = '{AppointmentTitle}{ServiceTitle}';
$lang['tab_select_service'] = '選擇{ServiceTitle}';
$lang['tab_select_resource'] = '選擇{ResourceTitle}';
$lang['tab_select_schedule'] = '選擇{ScheduleTitle}';
$lang['tab_contace_info'] = '聯絡方法';
$lang['tab_confirmation'] = '確認{AppointmentTitle}';
$lang['service_duration'] = '{ServiceTitle}時段';
$lang['service_cost'] = '費用';
$lang['next'] = '下一步';
$lang['back'] = '返回';
$lang['ok'] = '確定';
$lang['confirm'] = '確定';
$lang['yes'] = '是';
$lang['no'] = '否';
$lang['cancel'] = '取消';
$lang['am'] = '上午';
$lang['pm'] = '下午';
$lang['name'] = '姓名';
$lang['first_name'] = '名字';
$lang['last_name'] = '姓氏';
$lang['contact'] = '聯絡電話';
$lang['phone_number'] = '聯絡電話';
$lang['email'] = '電郵';
$lang['email_verify'] = '確認電郵';
$lang['please_select_schedule'] = '請選擇{ScheduleTitle}';
$lang['not_available'] = '不適用';
$lang['start'] = '開始';
$lang['end'] = '結束';
$lang['timeslot_is_full'] = '你選擇的{ResourceTitle}巳經客滿，請選擇另一位{ResourceTitle}。';

$lang['verify_firstname'] = '請輸入名字';
$lang['verify_lastname'] = '請輸入姓氏';
$lang['verify_phone'] = '請輸入電話號碼';
$lang['verify_email'] = '請輸入電郵';
$lang['verify_verifyemail'] = '請輸入確認電郵';
$lang['invalid_phone'] = '不正確電話號碼';
$lang['invalid_email'] = '不正確電郵';
$lang['invalid_emailcompare'] = '電郵與確認電郵不相同';
$lang['your_contact_information'] = '你的聯繫資料';
$lang['note_optional'] = '特別備註';
$lang['please_leave_message_here'] = '你可以為商店留下任何特別備註或指引';
$lang['verifycode'] = '請輸入驗證碼';
$lang['verifyCodeError_dialog_title'] = '驗證碼不正確';
$lang['verifyCodeError_dialog_content'] = '驗證碼不正確，請重新輸入。';
$lang['service'] = '{ServiceTitle}';
$lang['resource'] = '{ResourceTitle}';
$lang['schedule'] = '選擇{ScheduleTitle}';
$lang['contact_info'] = '聯絡方法';
$lang['reminder'] = '提示';
$lang['send_me_email_reminder'] = '給我發送提示電郵 ';
$lang['make_appointment'] = '確認{AppointmentTitle} ';
$lang['confirm_appointment'] = '確認{AppointmentTitle} ';

$lang['made_successfully'] = '{AppointmentTitle}成功';
$lang['btn_view_booking_detail'] = '查看{AppointmentTitle}詳情';
$lang['btn_make_new_appointment'] = '創建新的{AppointmentTitle}';

$lang['scheduled_appointment'] = '{AppointmentTitle}安排';
$lang['cancel_appointment'] = '取消{AppointmentTitle}';
$lang['cancel_appointment_to_sure'] = '你確定要取消{AppointmentTitle}嗎?';

$lang['almost_done_with_your_booking'] = '還有一個步驟...';
$lang['almost_done_with_your_booking_description'] = '<p>你必須先確認你的會員帳戶才能完成{AppointmentTitle}程序。</p>一個附有驗證超鏈結的電郵巳發送到你的電郵信箱({email})。 當你完成帳戶確認後，你的{AppointmentTitle}將會自動確認。';
$lang['open_your_email_inbox'] = '開啟電郵收件匣';


$lang['time_format'] ='hh:mm(上午|下午)';
$lang['day_collection'] = '星期日,星期一,星期二,星期三,星期四,星期五,星期六';
$lang['to'] = '至';
$lang['price_unit'] = 'HK$';
$lang['err_service_expired'] = '暫時沒有提供{ServiceTitle}';
$lang['err_resource_expired'] = '暫時沒有合適{ResourceTitle}';
$lang['error_invalid_schedule_date'] = '不正確{AppointmentTitle}日期';

$lang['timelostfull_dialog_title'] = '暫時沒有適用時段';
$lang['timelostfull_dialog_content'] = '你選擇的時段已不再適用，請選擇另一個時段。';
$lang['close'] = '關閉';

$lang['error_timeslot_is_full'] = '時段不適用';
$lang['error_any_fileds_is_empty'] = '有空白欄位';
$lang['error_invalid_isreminder_value'] = 'isReminder值不正確';
$lang['error_no_timeslot_status_record'] = '沒有時段狀況記錄';
$lang['error_deal_date_expired'] = '交易日期已過';
$lang['error_selected_range_error'] = '請在最細和最大日期之間選擇';
$lang['error_not_timeslot'] = '不是在時段內';
$lang['error_special_time_error'] = '是特別日子但不在特別時間裡;';
$lang['error_is_not_week_day'] = '不是平日';
$lang['error_in_dayoff_day'] = '不是休假日';


$lang['please_enter_valid_value'] = '請輸入正確的內容';
$lang['resource_no_provided_service'] = '{ResourceTitle}不提供所選擇的{ServiceTitle}';
$lang['login_with_facebook'] = 'Facebook登入';
$lang["tc"] = '確認{AppointmentTitle}時，你將一併同意88DB.com 的 <a href="%s" target="_blank"> 私隱權聲明</a> 和 <a href="%s" target="_blank">使用條款及細則</a>';
$lang['err_database_err'] = '<P>對不起，伺服器暫時未能提供{ServiceTitle}。你可以返回 <a href="javascript:void(0);" onclick="history.go(-1);">前一頁</a> 或前往<a href="http://hk.88db.com/en/" target="_top">88DB.com</a>。<br /><br />如需要更多幫助，請按<a href="mailto:cs@88db.com">聯絡我們</a>。</P>';
$lang['service_is_unavailable'] = '{AppointmentTitle}服務不適用';

$lang['datepicker_language'] = '';
$lang['duration_format'] = '%dh %dm';
$lang['view_detail'] = '查看詳情';
$lang['please_select_resouce'] = '請選擇{ResourceTitle}';
$lang['tab_select_resource_schedule'] = '選擇{ResourceTitle}和{ScheduleTitle}';
$lang['current_day_no_timeslot'] = '您選擇的日子沒有{ScheduleTitle}提供服務';
$lang['available'] = '可用';
$lang['no_preference'] = '沒有指定';
$lang['no_available_resource'] = '沒有可用{ResourceTitle}';
$lang['select'] = '選擇';
$lang['clear_selection'] = '清除選項';
