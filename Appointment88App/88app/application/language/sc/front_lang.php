<?php 

include_once('country_lang.php');

$lang['err_session_expired'] = '会话已过期，请重新加载旺铺';
$lang['label'] = '文本';

$lang['member_area'] = '查看您的{AppointmentTitle}';
$lang['member_login'] = '查看您的{AppointmentTitle}';
$lang['title_make_appointment'] = '{AppointmentTitle}';
$lang['tab_select_service'] = '选择{ServiceTitle}';
$lang['tab_select_resource'] = '选择{ResourceTitle}';
$lang['tab_select_schedule'] = '选择{ScheduleTitle}';
$lang['tab_contace_info'] = '联系方式';
$lang['tab_confirmation'] = '确认';
$lang['service_duration'] = '{ServiceTitle}时长';
$lang['service_cost'] = '价格';
$lang['next'] = '下一页';
$lang['back'] = '返回';
$lang['ok'] = '确定';
$lang['confirm'] = '确定';
$lang['yes'] = '是';
$lang['no'] = '否';
$lang['cancel'] = '取消';
$lang['am'] = '上午';
$lang['pm'] = '下午';
$lang['name'] = '姓名';
$lang['first_name'] = '名';
$lang['last_name'] = '姓';
$lang['contact'] = '联系电话';
$lang['phone_number'] = '联系电话';
$lang['email'] = '邮箱';
$lang['email_verify'] = '邮箱 (验证)';
$lang['please_select_schedule'] = '请选择一个{ScheduleTitle}';
$lang['not_available'] = '不可用';
$lang['start'] = '开始';
$lang['end'] = '结束';
$lang['timeslot_is_full'] = '{ResourceTitle}已爆满，请选择另一个{ResourceTitle}。';

$lang['verify_firstname'] = '请输入名';
$lang['verify_lastname'] = '请输入姓';
$lang['verify_phone'] = '请输入电话号码';
$lang['verify_email'] = '请输入邮箱';
$lang['verify_verifyemail'] = '请输入验证邮箱';
$lang['invalid_phone'] = '电话号码无效';
$lang['invalid_email'] = '邮箱无效';
$lang['invalid_emailcompare'] = '邮箱和验证邮箱不一致';
$lang['your_contact_information'] = '您的联系信息';
$lang['note_optional'] = '备注';
$lang['please_leave_message_here'] = '您可以填写备注信息';
$lang['verifycode'] = '输入验证码';
$lang['verifyCodeError_dialog_title'] = '验证码错误';
$lang['verifyCodeError_dialog_content'] = '验证码错误，请重试。';
$lang['service'] = '{ServiceTitle}';
$lang['resource'] = '{ResourceTitle}';
$lang['schedule'] = '{ScheduleTitle}';
$lang['contact_info'] = '联系方式';
$lang['reminder'] = '提醒';
$lang['send_me_email_reminder'] = '发送邮件提醒 ';
$lang['make_appointment'] = '确认{AppointmentTitle} ';
$lang['confirm_appointment'] = '确定{AppointmentTitle} ';

$lang['made_successfully'] = '{AppointmentTitle}成功';
$lang['btn_view_booking_detail'] = '查看详情 ';
$lang['btn_make_new_appointment'] = '制定新{AppointmentTitle}';

$lang['scheduled_appointment'] = '{AppointmentTitle}安排';
$lang['cancel_appointment'] = '取消{AppointmentTitle}';
$lang['cancel_appointment_to_sure'] = '您确定想要取消以下{AppointmentTitle}？';

$lang['almost_done_with_your_booking'] = '一步 ...';
$lang['almost_done_with_your_booking_description'] = '<p>为了完成{AppointmentTitle}预订，您必须验证您的会员账户。</p>同时，88DB的邮件验证链接将会发送到您的邮箱({email})。对该账户验证完成后，您就会自动确认{AppointmentTitle}。';
$lang['open_your_email_inbox'] = '打开您的收件箱';


$lang['time_format'] ='hh:mm(上午|下午)';
$lang['day_collection'] = '星期日,星期一,星期二,星期三,星期四,星期五,星期六';
$lang['to'] = '到';
$lang['price_unit'] = '港币';
$lang['err_service_expired'] = '{ServiceTitle}不可用';
$lang['err_resource_expired'] = '{ResourceTitle}不可用';
$lang['error_invalid_schedule_date'] = '{AppointmentTitle}日期无效';

$lang['timelostfull_dialog_title'] = '时间段不可用';
$lang['timelostfull_dialog_content'] = '您选择的时间段不可用，请另选时间段';
$lang['close'] = '关闭';

$lang['error_timeslot_is_full'] = '时间段不可用';
$lang['error_any_fileds_is_empty'] = '所有字段是空的';
$lang['error_invalid_isreminder_value'] = '无效提醒值';
$lang['error_no_timeslot_status_record'] = '没有时间段状态记录';
$lang['error_deal_date_expired'] = '处理日期已过期';
$lang['error_selected_range_error'] = '选择不在最小和最大日期之间';
$lang['error_not_timeslot'] = '不在时间段';
$lang['error_special_time_error'] = '是特殊的日期，而不是在特殊的时间;';
$lang['error_is_not_week_day'] = '不是工作日';
$lang['error_in_dayoff_day'] = '在休息日';


$lang['please_enter_valid_value'] = '请输入有效值';
$lang['resource_no_provided_service'] = '{ResourceTitle}不提供所选{ServiceTitle}';
$lang['login_with_facebook'] = '登录脸谱';
$lang["tc"] = '通过确认{AppointmentTitle}，您同意的 <a href="%s" target="_blank"> 隐私声明</a> and <a href="%s" target="_blank">条款 &amp; 条件</a>';
$lang['err_database_err'] = '<P>对不起，{ServiceTitle}器暂时无法响应您的请求。您可以返回到 <a href="javascript:void(0);" onclick="history.go(-1);">上一页</a> 或前往 <a href="http://hk.88db.com/en/" target="_top">88DB.com</a>。<br /><br />如需更多帮助, 请点击这儿来<a href="mailto:cs@88db.com">联系我们</a>。</P>';
$lang['service_is_unavailable'] = '{AppointmentTitle}{ServiceTitle}不可用';

$lang['datepicker_language'] = '日期选择器';
$lang['duration_format'] = '%d 小时 %d 分';
$lang['view_detail'] = '查看详细内容';
$lang['please_select_resouce'] = '请选择{ResourceTitle}';
$lang['tab_select_resource_schedule'] = '选择{ResourceTitle}和{ScheduleTitle}';
$lang['current_day_no_timeslot'] = '您选择的日期没有{ScheduleTitle}可用';
$lang['available'] = '可用';
$lang['no_preference'] = '没有指定';
$lang['no_available_resource'] = '没有{ResourceTitle}可用';
$lang['select'] = '选择';
$lang['clear_selection'] = '清除选择';
