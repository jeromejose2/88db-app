<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Appointment extends CI_Controller {
	private $memberId;
	private $memberEmail;
	public function __construct($config = array())
	{
		parent::__construct();
		$this->lang->load('front', $this->app88param->getLang()?$this->app88param->getLang():APP88_FRONT_LANGUAGE);
		$this->load->helper('cookie');
		$this->load->library('email');
		$this->load->model("service_model");
		$this->load->model("resource_model");
		$this->load->model("timeslotstatus_model");
		$this->load->model("customer_model");
		$this->load->model("booking_model");
		$this->load->model("setting_model");
		$this->load->model("sendemail_model");
		$this->load->model("app_model");
		
		$this->load->library("app88authen");

		$this->memberId = $this->app88userinfo->getMemberId();
		$this->memberEmail = $this->app88userinfo->getMemberEmail();

		if (!$this->app88param->get88AppGuid())
		{
			show_error($this->lang->line('err_session_expired'));
		}	
		if($this->input->cookie("AppointmentSessionId") === FALSE){
			$this->input->set_cookie('AppointmentSessionId', $this->guid->newGuid(), 0);	
		}				
	}
	
	public function welcome()
	{	
	
		
		$this->timeslotstatus_model->releaseTimeSlot($this->input->cookie("AppointmentSessionId"));
		$data['isLogin'] = $this->memberId !== NULL;
		$data['services'] = $this->service_model->getAvailableServicesNameList();			
		$data['setting'] = $this->setting_model->getSetting();
		if( $data['services'] === FALSE)
		{
			$this->load->view('appointment/not_available', $data);
		}
		else
		{
			function getResourceGuid($v) 
			{						
				return $v->ResourceGuid;
			}
			foreach($data['services'] as $service)
			{
				$service->resources = array();
				$resouces = $this->resource_model->getAvailableResourceNameListByServiceGuid($this->guid->createFromBinary($service->ServiceGuid)->toString(), TRUE);
				if($resouces)
				{					
					$resouceIds = array_map("getResourceGuid",$resouces);
					foreach($resouceIds as $id){
						array_push($service->resources, $this->guid->createFromBinary($id)->toString());
					}
				}else
				{
					$service->Status = "I";
				}
			}
			$this->load->view('appointment/select_service', $data);
		}
	}
	
	public function member()
	{	
		if($this->memberId === NULL)
		{
			redirect('appointment/welcome');
		}
		else
		{
			$action = $this->input->post('action');
			if(!empty($action)) 
			{
				$guid = $this->input->post('guid');
				switch ($action)
				{
					case "delete":											
						$customer = $this->customer_model->getCustomerInfoByMemberId($this->memberId);
						$booking = $this->booking_model->getBookingDetailByGuid($guid, $customer[0]->Id);	
						$service = $this->service_model->getServiceDetailByGuid("", FALSE, $booking->ServiceId);
						$resource = $this->resource_model->getResourceDetailByGuid("", FALSE, $booking->ResourceId);
						$appGuid = $this->guid->createFromBinary($booking->AppInstanceGuid)->toString();
						$app = $this->app_model->getAppByAppGuid($appGuid);
						
						$this->booking_model->deleteBooking($guid, $customer[0]->Id, $this->memberId, $booking->TimeSlotStatusId);
						//send email to customer
						$this->email->sendCancelAppointmentEmail(array(
							'From' => 'customer',
							'Id' => $customer[0]->Id,
							'Email' => $customer[0]->Email,
							'MemberId' => $customer[0]->MemberId,
							'ShopName' => $app['ShopName'],
							'FirstName' => $customer[0]->FirstName,
							'LastName' => $customer[0]->LastName,
							'Service' => $service->Name,
							'Resource' => $resource->Name,
							'Schedule' => $booking->StartDateTime,
							'Price' => $service->Cost,
							'Note' => $booking->Note,
							'Reason' => '',
							'Phone' => $customer[0]->Contact,
						));
						break;
					default:
						break;
				}			
			}
			$data['isLogin'] = $this->memberId !== NULL;
			$bookings = $this->booking_model->getBookingList($this->memberId, 0, "upcoming", 1, "", "", "", "", "ASC");
			foreach($bookings["rows"] as $booking)
			{
				$booking->FormatCost = $this->common->number_format($booking->Cost);
				$booking->ServiceName = form_prep($booking->ServiceName);
			}
			$data['bookings'] = $bookings['rows'];
			$this->load->view('appointment/member', $data);
		}
	}
	
	public function logout()
	{
		$this->app88userinfo->setUserInfo(NULL);		
	}
	
	public function selectResource()
	{			
		$this->timeslotstatus_model->releaseTimeSlot($this->input->cookie("AppointmentSessionId"));
		$serviceId = $this->input->get("serviceid");
		$service = $this->getServiceDetail($serviceId);
		$resources = $this->resource_model->getAvailableResourceNameListByServiceGuid($serviceId, FALSE);
		
		
		$data['service'] = $service;
		$data['resources'] = $resources;
		$data['isLogin'] = $this->memberId !== NULL;
		$data['setting'] = $this->setting_model->getSetting();
		$this->load->view('appointment/select_resource_schedule', $data);
	}
	
	public function selectSchedule()
	{
		$serviceId = $this->input->get("serviceid");        
	    
		$this->timeslotstatus_model->releaseTimeSlot($this->input->cookie("AppointmentSessionId"));
		$data['isLogin'] = $this->memberId !== NULL;
		$data['service'] = $this->getServiceDetail($serviceId);
		$data['setting'] = $this->setting_model->getSetting();
		$this->load->view('appointment/select_schedule', $data);
	}
	
	public function contactInfo()
	{
		//create timeslot status
		$serviceGuid = $this->input->get("serviceid");
		$resourceGuid = $this->input->get("resourceid");
		$serviceId = $this->service_model->getServiceIdByGuid($serviceGuid);
		$resourceId = $this->resource_model->getResourceIdByGuid($resourceGuid);
		$dealdate = str_replace("-", " ", $this->input->get("datetime"));		
		$service = $this->getServiceDetail($serviceGuid);
		//check is invalid schedule
		if ( ! $this->checkDealDate($dealdate, $serviceGuid, $resourceGuid) )
		{
			show_error($this->lang->line('error_invalid_schedule_date'));
		}
		//check is full		
		$timeSlotStatusCount = count($this->timeslotstatus_model->getAvailableTimeSlotByDateTime($serviceId, $resourceId, $dealdate, '', "'Confirmed'"));
		if ( $timeSlotStatusCount >= $service->AvailablePerTimeSlot )
		{
			redirect('/appointment/selectResource?error=timeslotfull&serviceid='.$serviceGuid.'&resourceid='.$resourceGuid); //back to prev page.
			show_error($this->lang->line('error_timeslot_is_full'));
		}    
		$params = array(
			'sessionId' =>$this->input->cookie("AppointmentSessionId"),
			'serviceId' =>$serviceId,
			'resourceId' =>$resourceId,
			'bookingTime' =>$dealdate,
			'status' =>'Expired'
		);	
		$this->timeslotstatus_model->releaseTimeSlot($params["sessionId"]);
		
		$count = count($this->timeslotstatus_model->getTimeSlotStatus($params));        		
		$params['status'] = "Reserved";
		if($count > 0)
		{
			$this->timeslotstatus_model->updateTimeSlotStatus($params);
		}
		else
		{
			$this->timeslotstatus_model->createTimeSlotStatus($params);			
		}		
		
		$data['displayName'] = $this->app88userinfo->getShopName();
		$data['memberEmail'] = $this->memberEmail;
		$data['customer'] = $this->customer_model->getCustomerInfoByMemberId($this->memberId);
		$data['isLogin'] = $this->memberId !== NULL;
		$data['setting'] = $this->setting_model->getSetting();
		$this->load->view('appointment/contact_info', $data);
	}
	
	public function confirm()
	{
		$data['isLogin'] = $this->memberId !== NULL;
		if( $this->memberId === NULL )
		{
			$email = $this->input->post("email");
		}
		else
		{
			$email	= $this->memberEmail;
		}
		$contact = array(
			'email' =>$email,
			'firstName' =>$this->input->post("firstName"),
			'lastName' =>$this->input->post("lastName"),
			'phoneNumber' =>$this->input->post("phoneNumber"),
			'note' =>$this->input->post("note")
		);
		$data['contact'] = $contact;		
		$data['setting'] = $this->setting_model->getSetting();
		$this->load->view('appointment/confirm', $data);
	}
	
	public function complete()
	{		
		//contact info
		$firstName = $this->input->post("firstName");
		$lastName = $this->input->post("lastName");
		$phoneNumber = $this->input->post("phoneNumber");
		$isReminder = $this->input->post("isReminder");
		$email = $this->input->post("email");
		$note = $this->input->post("note");
		$verifyCode = $this->input->post("verifycode");
		if ($verifyCode !== $this->session->userdata['confirmVerifyCode']) 
		{
			return FALSE;
		}
		$this->clearVerifyCode();
		$isVerify = TRUE;
		if( $this->memberId === NULL )
		{
			$isLogin = FALSE;
			$memberInfo = $this->getMemberStatus($email, $firstName, $lastName, $phoneNumber);
			if($memberInfo['error'] !== 0 )
			{
				show_error($memberInfo['errorMessage']);
			}
			$isVerify = $memberInfo['isVerify'];
			$this->memberId = $memberInfo['memberId'];
		}
		else
		{
			$isLogin = TRUE;
			$email	= $this->memberEmail;
		}
		$serviceGuid = $this->input->post("serviceid");
		$resourceGuid = $this->input->post("resourceid");
		$dealDate = str_replace("-", " ", $this->input->post("datetime"));
		$serviceId = $this->service_model->getServiceIdByGuid($serviceGuid);
		$resourceId = $this->resource_model->getResourceIdByGuid($resourceGuid);
		$setting = $this->setting_model->getSetting();
		$service = $this->getServiceDetail($serviceGuid);
		//check resourceid is link to servierid
		if ( !$this->resource_model->isReourceProvidedService($serviceId, $resourceId) )
		{
			show_error($this->lang->line('resource_no_provided_service'));
		}
		
		//check contact info
		if(empty($firstName) || empty($phoneNumber) || empty($isReminder))
		{
			show_error($this->lang->line('error_any_fileds_is_empty'));
		}
		else if(APP88_SINGLE_NAME_FIELD === 'disable' && empty($lastName))
		{
			show_error($this->lang->line('error_any_fileds_is_empty'));
		}
		else if($isReminder !== 'T' && $isReminder !== 'F')
		{
			show_error($this->lang->line('error_invalid_isreminder_value'));
		}
		else if( !$this->checkDealDate($dealDate, $serviceGuid, $resourceGuid))
		{
			show_error($this->lang->line('error_invalid_schedule_date'));
		}
        
		//check TimeSlotStatus have recored
		$params = array(
			'sessionId' =>$this->input->cookie("AppointmentSessionId"),
			'serviceId' =>$serviceId,
			'resourceId' =>$resourceId,
			'bookingTime' =>$dealDate,
			'status' =>'Reserved'
		);		
		$timeSlotStatus = $this->timeslotstatus_model->getTimeSlotStatus($params);
		if(count($timeSlotStatus) === 0)
		{
			show_error($this->lang->line('error_no_timeslot_status_record'));
            return FALSE;
		}       
		//check has expired
		$now = $this->app88common->local_datetime();
		$checkExpiredDate = $this->app88common->local_datetime(empty($timeSlotStatus[0]->ModifiedTime)?$timeSlotStatus[0]->CreatedTime:$timeSlotStatus[0]->ModifiedTime);
		if ( strtotime($this->common->addTime($checkExpiredDate, 0, APP88_RESERVEDAUTOEXPIREDDURATION)) > strtotime($now) )
		{
			//"pass";
		}
		else
		{
			show_error($this->lang->line('error_deal_date_expired'));
			return FALSE;
		}
		
		//check is full
		$timeSlotStatusCount = count($this->timeslotstatus_model->getAvailableTimeSlotByDateTime($serviceId, $resourceId, $dealDate, $this->input->cookie("AppointmentSessionId"), "'Confirmed'"));
		if ( $timeSlotStatusCount >= $service->AvailablePerTimeSlot )
		{
			show_error($this->lang->line('error_timeslot_is_full'));
		}     
		
		
		//get service snapshot
		$serviceSnapShot = $this->service_model->getServiceSnapShot($serviceGuid);		
		if($serviceSnapShot === FALSE)
		{
			$this->service_model->createSnapShot($serviceGuid);		
			$serviceSnapShot = $this->service_model->getServiceSnapShot($serviceGuid);
		}
		
		//cal endDate
		$cd = strtotime($dealDate);
		$endDate = date('Y-m-d H:i:s', 
			mktime(
				date('H',$cd),
				date('i',$cd) + $serviceSnapShot[0]->Duration, 
				date('s',$cd), 
				date('m',$cd),
				date('d',$cd), 
				date('Y',$cd)
			)
		);
		
		//get resource snapshot
		$resourceSnapShot = $this->resource_model->getResourceSnapShot($resourceGuid);		
		if($resourceSnapShot === FALSE)
		{
			$this->resource_model->createResourceSnapShot($resourceGuid);		
			$resourceSnapShot = $this->resource_model->getResourceSnapShot($resourceGuid);
		}
		
		//update customer info		 
		$customer = $this->customer_model->getCustomerInfoByMemberId($this->memberId);
		$customerParams = array(
			'firstName' => $firstName, 
			'lastName' => $lastName,
			'contact' => $phoneNumber,
			'email' => $email,
			'note' => $note,
			'isOffline' => 'F',
			'memberId' => $this->memberId
		);
		if($customer === FALSE)
		{
			$customerId = $this->customer_model->createCustomer($customerParams);
		}
		else
		{		
			$this->customer_model->updateCustomerInfo($customerParams);
			$customerId = $customer[0]->Id;
		}
		
		$data['service'] = $serviceSnapShot;
		$data['resource'] = $resourceSnapShot;
		$data['dealDate'] = $dealDate;
		$data['isLogin'] = $isLogin;
		$data['email'] = $email;
		$data['setting'] = $this->setting_model->getSetting();
		$data['memberId'] = $this->memberId;
		
		//Not verified member
		if($isVerify)
		{			
			//create booking
			$this->booking_model->createBooking(array(
				'customerId' => $customerId,
				'serviceId' => $serviceSnapShot[0]->Id,
				'resourceId' => $resourceSnapShot[0]->Id,
				'startDateTime' => $dealDate,
				'endDateTime' => $endDate,
				'isReminder' => $isReminder,
				'timeSlotStatusId' => $timeSlotStatus[0]->Id,
				'status' => 'A',
				'note' => $note
			));
			
			//update timeslot status to confirmed
			$params = array(
				'sessionId' =>$this->input->cookie("AppointmentSessionId"),
				'serviceId' =>$serviceId,
				'memberId' =>$this->memberId,
				'resourceId' =>$resourceId,
				'bookingTime' =>$dealDate,
				'status' =>"Confirmed"
			);	
			$this->timeslotstatus_model->updateTimeSlotStatus($params);

			//send reminder email
			if($setting["enableNotificationEmail"] == 'T')
			{
				$app = $this->app_model->getAppByAppGuid($this->app88param->get88AppGuid());
				
				$this->email->sendNotificationEmail(array(
					'Id' => $customerId,
					'Email' => $customerParams['email'],
					'MemberId' => $data['memberId'],
					'ShopName' => $app['ShopName'],
					'FirstName' => $customerParams['firstName'],
					'LastName' => $customerParams['lastName'],
					'Service' => $data['service'][0]->Name,
					'Resource' => $data['resource'][0]->Name,
					'Schedule' => $data['dealDate'],
					'Price' => $data['service'][0]->Cost,
					'Note' => $customerParams['note'],
					'Phone' => $customerParams['contact'],
				));
			}
			$this->load->view('appointment/complete', $data);
		}
		else
		{
			//create booking
			$this->booking_model->createBooking(array(
				'customerId' => $customerId,
				'serviceId' => $serviceSnapShot[0]->Id,
				'resourceId' => $resourceSnapShot[0]->Id,
				'startDateTime' => $dealDate,
				'endDateTime' => $endDate,
				'isReminder' => $isReminder,
				'timeSlotStatusId' => $timeSlotStatus[0]->Id,
				'status' => 'P',
				'note' => $note
			));
			
			//update timeslot status member id
			$params = array(
				'sessionId' =>$this->input->cookie("AppointmentSessionId"),
				'serviceId' =>$serviceId,
				'memberId' =>$this->memberId,
				'resourceId' =>$resourceId,
				'bookingTime' =>$dealDate,
				'status' =>"Reserved"
			);	
			$this->timeslotstatus_model->updateTimeSlotStatus($params);
			$data['emailClientMapping'] = json_decode(APP88_APPOINTMENT_EMAIL_CLIENT_MAPPING);
			$this->load->view('appointment/request', $data);			
		}
		$this->input->set_cookie('AppointmentSessionId', NULL, 0); //clear SessionID
		
	}
	
	public function request()
	{
		$data;
		$this->load->view('appointment/request', $data);
	}
	
	public function authen()
	{
		if( $this->session->userdata('user_info') === NULL ){
			return;
		}		
		$state = json_encode(array('cid'=>$_GET['cid'], 'mode'=>''));
		$authen_handler = 'admin/bo/checkOwner_handler';
		$this->app88authen->authen($authen_handler, $state);
	}
	
	public function getServiceDetail($serviceGuid)
	{
		$service = $this->service_model->getServiceDetailByGuid($serviceGuid);
		$service->Name = form_prep($service->Name);
		if( ! $service )
		{
			show_error($this->lang->line('err_service_expired'));
		}
		return $service;
	}
	
	public function getResourceDetail($resourceGuid)
	{
		$resource = $this->resource_model->getResourceDetailByGuid($resourceGuid);
		$resource->Name = form_prep($resource->Name);
		if( ! $resource )
		{
			show_error($this->lang->line('err_resource_expired'));
		}
		return $resource;
	}
	
	public function checkDealDate($date, $serviceGuid, $resourceGuid, $ignoreServiceStatus = FALSE)
	{
	
		$dealDate = date('Y-m-d H:i:s', strtotime($date)); 
		$now = $this->app88common->local_datetime();
		$service = $this->service_model->getServiceDetailByGuid($serviceGuid, $ignoreServiceStatus);
		
		//check is after today
		$minDate = $this->common->addTime($now, -1 * $service->MinTime);
		$maxDate = $this->common->addTime($now, (empty($service->AdvanceTime)?20*365*24:$service->AdvanceTime));
		if( (strtotime($dealDate) < strtotime($minDate)) || (strtotime($dealDate) >strtotime($maxDate))){
			show_error($this->lang->line('error_selected_range_error'));
			return FALSE;
		}
        
		$resourceServiceDate = $this->resource_model->getResourceServiceDateTime($resourceGuid);
		
		//check specialHours
		$isInSpecialHours = FALSE;
		if(ISSET($resourceServiceDate["SpecialHours"]) && count($resourceServiceDate["SpecialHours"]) !== 0){
			foreach ($resourceServiceDate["SpecialHours"] as $specialhour) 
			{
				$specialDate = date('Y-m-d', strtotime($dealDate));
				$startDate = date('Y-m-d H:i:s', strtotime($specialhour->Date." ".$specialhour->StartTime));
				$endDate = date('Y-m-d H:i:s', strtotime($specialhour->Date." ".$specialhour->EndTime));
				if( strcmp($specialhour->Date, $specialDate) === 0 ) 
				{
					if( ($dealDate >= $startDate) && ($dealDate <= $endDate)){
						//check is start time            
						$rangStartTime  = $this->common->addTime($startDate, 0);
						$isInTimeSlot = FALSE;
						do
						{
							if( $rangStartTime === $dealDate)
							{
								$isInTimeSlot = TRUE;
								break;
							}
							$rangStartTime  = $this->common->addTime($rangStartTime, 0, $service->TimeSlotDivision);
							$i--;
						}while(($rangStartTime <= $endDate));
						if( !$isInTimeSlot ){
							show_error($this->lang->line('error_not_timeslot'));
							return FALSE;
						}
                    
						$isInSpecialHours = TRUE;
						break;
					}
					if( ! $isInSpecialHours )
					{
						show_error($this->lang->line('error_special_time_error'));
						return FALSE;
					}
				}            
			}
		}
        
		//check is normal day
		$dealDay = date("w",strtotime($dealDate)); 
        
		$inWeekDay = FALSE;
        
		if( $isInSpecialHours )
		{
			$inWeekDay = TRUE;
		}
		else
		{       		
			foreach ($resourceServiceDate["NormalHours"] as $normalHours) {
				if( $normalHours->Day === $dealDay )
				{
					
					$inWeekDay = TRUE;
					$startDate = date('Y-m-d H:i:s', strtotime(date('Y-m-d', strtotime($dealDate))." ".$normalHours->StartTime));
					$endTime = $normalHours->EndTime;
					if($endTime == "24:00:00"){
						$endTime = "23:00:00";
					}
					$endDate = date('Y-m-d H:i:s', strtotime(date('Y-m-d', strtotime($dealDate))." ".$endTime));
					if( ($dealDate < $startDate) || ($dealDate >$endDate)){						
						$inWeekDay = FALSE;
						continue;
					}
               
					//check is start date time       
					$rangStartTime  = $this->common->addTime($startDate, 0);
					$isInTimeSlot = FALSE;
					do
					{
						if( $rangStartTime === $dealDate)
						{
							$isInTimeSlot = TRUE;
							break;
						}
						$rangStartTime  = $this->common->addTime($rangStartTime, 0, $service->TimeSlotDivision);
						$i--;
					}while(($rangStartTime <= $endDate));
					if( !$isInTimeSlot ){
						show_error($this->lang->line('error_not_timeslot'));
						return FALSE;
					}
					break; //is normal day , break
				}
			}
		}
		
		if( !$inWeekDay){
			show_error($this->lang->line('error_is_not_week_day'));
			return FALSE;
		}
		
		
		//check is day off
		$isDayoff = FALSE;
		
		if(ISSET($resourceServiceDate["DayOff"])){
			foreach ($resourceServiceDate["DayOff"] as $dayoff) 
			{
			
				$startDate = date('Y-m-d H:i:s', strtotime($dayoff->StartDate." 00:00"));
				$endDate = date('Y-m-d H:i:s', strtotime($dayoff->EndDate." 23:59:59"));
				if( ($dealDate >= $startDate) && ($dealDate <= $endDate)){
					$isDayoff = TRUE;
					break;
				}
			}
		}
		
		if( $isDayoff )
		{
			show_error($this->lang->line('error_in_dayoff_day'));
			return FALSE;
		}        
		return TRUE;
	}

	public function getMemberStatus($email, $firstName, $lastName, $phoneNumber)
	{
		$info = $this->app88common->register88DBMember($email, $firstName, $lastName, $phoneNumber);
		$memberId = $info['uid'];
		$returnCode = $info['returnCode'];
		$isVerify = TRUE;
		if($returnCode == 1 || $returnCode == 3)
		{
			$isVerify = FALSE;
		}
		return array(
			'error' => 0,
			'errorMessage' => '',
			'memberId' => $memberId,
			'isVerify' => $isVerify
		);
	}    
	
	public function checkVerfiyCode()
	{
		$verifycodeName = 'confirmVerifyCode';
		if ($this->input->post("verifycode") === $this->session->userdata[$verifycodeName]) 
		{
			echo '{"success": true}';
		}
		else
		{
			echo '{"success": false}';
		}
	}
	
	public function getConfirmVerifyCode()
	{
		$this->verifycode('confirmVerifyCode');
	}
	
	public function clearVerifyCode() 
	{
		$this->session->set_userdata('confirmVerifyCode', NULL);
	}
	
	public function verifycode($sessionname) 
	{		
		session_start();
		$verifycode = '';
		for ($i = 0; $i < 4; $i++) 
		{
			$verifycode.= dechex(mt_rand(0, 15));
		}		
		$this->session->set_userdata($sessionname, $verifycode);
		$_width = 75;
		$_height = 25;
		$_img = imagecreatetruecolor($_width, $_height);
		$_white = imagecolorallocate($_img, 255, 255, 255);
		imagefill($_img, 0, 0, $_white);
		for ($i = 0; $i < 6; $i++) 
		{
			$_rnd_color = imagecolorallocate($_img, mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
			imageline($_img, mt_rand(0, $_width), mt_rand(0, $_height), mt_rand(0, $_width), mt_rand(0, $_height), $_rnd_color);
		}
		for ($i = 0; $i < 100; $i++) 
		{
			$_rnd_color = imagecolorallocate($_img, mt_rand(200, 255), mt_rand(200, 255), mt_rand(200, 255));
			imagestring($_img, 1, mt_rand(1, $_width), mt_rand(1, $_height), '*', $_rnd_color);
		}
		for ($i = 0; $i < strlen($this->session->userdata[$sessionname]); $i++) 
		{
			$_rnd_color = imagecolorallocate($_img, mt_rand(0, 100), mt_rand(0, 125), mt_rand(0, 150));
			imagestring($_img, 5, $i * $_width / 4 + mt_rand(1, 10), mt_rand(1, $_height / 2), $this->session->userdata[$sessionname][$i], $_rnd_color);
		}
		ob_clean();
		header('Content-Type: image/png');
		imagepng($_img);
		imagedestroy($_img);
	}
	
	public function updateAppointment()
	{
		$serviceGuid = $this->input->post('serviceGuid'); 
		$resouceGuid = $this->input->post('resouceGuid'); 
		$schedule = $this->input->post('schedule'); 
		$customerGuid = $this->input->post('customerGuid'); 
		$bookingGuid = $this->input->post('bookingGuid'); 
		$source = $this->input->post('source'); 
		//$bookingTime = $this->input->post('bookingTime'); 
		$serviceId = $this->service_model->getServiceIdByGuid($serviceGuid, TRUE);		
		$resourceId = $this->resource_model->getResourceIdByGuid($resouceGuid);
		
		
		//get service
		$service = $this->service_model->getServiceDetailByGuid($serviceGuid, TRUE);
				
		if( 
			$bookingGuid !== '' &&
			$this->checkDealDate($schedule, $serviceGuid, $resouceGuid, TRUE) === TRUE && 
			$this->timeslotstatus_model->getConfirmTimeSlotCount($serviceId, $resourceId, $schedule) < $service->AvailablePerTimeSlot			
		)
		{
			
			$customer =  $this->customer_model->getCustomerInfoByGuid($customerGuid);
			$booking = $this->booking_model->getBookingDetailByGuid($bookingGuid, $customer->Id);
			
			
			//get resource snapshot
			$resourceSnapShot = $this->resource_model->getResourceSnapShot($resouceGuid);		
			if($resourceSnapShot === FALSE)
			{
				$this->resource_model->createResourceSnapShot($resouceGuid);		
				$resourceSnapShot = $this->resource_model->getResourceSnapShot($resouceGuid);
			}
			
			//get service snapshot
			$serviceSnapShot = $this->service_model->getServiceSnapShot($serviceGuid, TRUE);		
			if($serviceSnapShot === FALSE)
			{			
				$this->service_model->createSnapShot($serviceGuid);		
				$serviceSnapShot = $this->service_model->getServiceSnapShot($serviceGuid, TRUE);
			}			
			//update timeslot			
			$this->timeslotstatus_model->updateTimeSlot(array(
				'serviceId'=>$serviceId,
				'resourceId'=>$resourceId,
				'bookingTime'=>$schedule,
				'tileSlotId'=>$booking->TimeSlotStatusId
			));
			
			
			//update booking
			$this->booking_model->updateBooking($booking->Id, $serviceSnapShot[0]->Id, $resourceSnapShot[0]->Id, $schedule, $this->common->addTime($schedule, 0, $service->TimeSlotDivision), $source);
			
			//send nofitication email to online customer
			$data['service'] = $serviceSnapShot;
			$data['resource'] = $resourceSnapShot;
			$data['dealDate'] = $schedule;
			$data['memberId'] = $customer->MemberId;
			$customerParams = array(
				'firstName' => $customer->FirstName, 
				'lastName' => $customer->LastName,
				'email' => $customer->Email,
				'note' => $booking->Note,
				'contact' => $customer->Contact
			);
			$appGuid = $this->app88param->get88AppGuid();
			
			if ($customer->MemberId > 0)
			{
				$app = $this->app_model->getAppByAppGuid($appGuid);
					
				$sent = $this->email->sendUpdateNotificationEmail(array(
						'Id' => $customer->Id,
						'AppGuid' => $appGuid,
						'Email' => $customerParams['email'],
						'MemberId' => $data['memberId'],
						'ShopName' => $app['ShopName'],
						'FirstName' => $customerParams['firstName'],
						'LastName' => $customerParams['lastName'],
						'Service' => $data['service'][0]->Name,
						'Resource' => $data['resource'][0]->Name,
						'Schedule' => $data['dealDate'],
						'Price' => $data['service'][0]->Cost,
						'Note' => $customerParams['note'],
						'Phone' => $customerParams['contact'],
						'OnlyToCustomer' => TRUE,
				));
				if($sent === FALSE)
				{
					echo '{"success":false}';
					return;
				}
			}
			
			echo '{"success":true}';
		}
		else
		{
			echo '{"success":false}';
		}
	}
	
	/*
	return value:
	0: success
	1: schedule is invalidate
	2: schedule full
	3: create timeslot error
	4: create booking error
	5: sent email error
	*/
	public function createAppointment()
	{
		$source = $this->input->post("source");
		$customerGuid = $this->input->post("customerGuid");
		$customerName = $this->input->post("customerName");
		$serviceGuid = $this->input->post("serviceGuid");
		$resourceGuid = $this->input->post("resourceGuid");
		$dealDate = $this->input->post("schedule");
		$type = $this->input->post("type");
		
		$firstName = $this->input->post("firstName");
		$lastName = $this->input->post("lastName");
		$phone = $this->input->post("phone");
		$email = $this->input->post("email");
		
		$serviceId = $this->service_model->getServiceIdByGuid($serviceGuid);
		$resourceId = $this->resource_model->getResourceIdByGuid($resourceGuid);
		$setting = $this->setting_model->getSetting();
		$service = $this->getServiceDetail($serviceGuid);
		
		if($type === 'existing')
		{
			$customer =  $this->customer_model->getCustomerInfoByGuid($customerGuid);
			$customerId = $customer->Id;
			$memberId = $customer->MemberId;
			$firstName = $customer->FirstName;
			$lastName = $customer->LastName;
			$phone = $customer->Contact;
			$email = $customer->Email;
		}
		else //new
		{
			//create customer $customerName	
			$memberId = 0;
			$customerParams = array(
				'firstName' => $firstName, 
				'lastName' => $lastName,
				'contact' => $phone,
				'email' => $email,
				'note' => '',				
				'isOffline' => 'T',
				'memberId' =>$memberId
			);
			$customerId = $this->customer_model->createCustomer($customerParams);	
		}
		
		//check date
		if( !$this->checkDealDate($dealDate, $serviceGuid, $resourceGuid))
		{
			echo '{"success":false, "errorCode": 1}';		
			return;
		}
		
		//check is full
		$timeSlotStatusCount = count($this->timeslotstatus_model->getAvailableTimeSlotByDateTime($serviceId, $resourceId, $dealDate, "", "'Confirmed'"));
		if ( $timeSlotStatusCount >= $service->AvailablePerTimeSlot )
		{
			echo '{"success":false, "errorCode": 2}';		
			return;
		} 
		
		//get service snapshot
		$serviceSnapShot = $this->service_model->getServiceSnapShot($serviceGuid);		
		if($serviceSnapShot === FALSE)
		{
			$this->service_model->createSnapShot($serviceGuid);		
			$serviceSnapShot = $this->service_model->getServiceSnapShot($serviceGuid);
		}
		
		//cal endDate
		$cd = strtotime($dealDate);
		$endDate = date('Y-m-d H:i:s', 
			mktime(
				date('H',$cd),
				date('i',$cd) + $serviceSnapShot[0]->Duration, 
				date('s',$cd), 
				date('m',$cd),
				date('d',$cd), 
				date('Y',$cd)
			)
		);
		
		//get resource snapshot
		$resourceSnapShot = $this->resource_model->getResourceSnapShot($resourceGuid);		
		if($resourceSnapShot === FALSE)
		{
			$this->resource_model->createResourceSnapShot($resourceGuid);		
			$resourceSnapShot = $this->resource_model->getResourceSnapShot($resourceGuid);
		}
		
		//create timeslot
		$params = array(
			'sessionId' =>'',
			'serviceId' =>$serviceId,
			'resourceId' =>$resourceId,
			'bookingTime' =>$dealDate,
			'memberId' => $memberId,
			'status' =>'Confirmed'
		);	
		$timeSlotId = $this->timeslotstatus_model->createTimeSlotStatus($params);	
		if($timeSlotId === FALSE)
		{
			echo '{"success":false, "errorCode": 3}';		
			return;
		}
		
		//create booking
		$result = $this->booking_model->createBooking(array(
				'customerId' => $customerId,
				'serviceId' => $serviceSnapShot[0]->Id,
				'resourceId' => $resourceSnapShot[0]->Id,
				'startDateTime' => $dealDate,
				'endDateTime' => $endDate,
				'isReminder' => 'T',
				'timeSlotStatusId' => $timeSlotId,
				'status' => 'A',
				'note' => '',
				'source' => $source
			));
		if($result === FALSE)
		{
			echo '{"success":false, "errorCode": 4}';	
			return;
		}
		
		//send nofitication email to online customer
		$data['service'] = $serviceSnapShot;
		$data['resource'] = $resourceSnapShot;
		$data['dealDate'] = $dealDate;
		$data['memberId'] = $memberId;
		$customerParams = array(
				'firstName' => $firstName,
				'lastName' => $lastName,
				'email' => $email,
				'note' => '',
				'contact' => $phone
		);
		$appGuid = $this->app88param->get88AppGuid();
		
		if ($memberId > 0)
		{
			$app = $this->app_model->getAppByAppGuid($appGuid);
			
			$sent = $this->email->sendNotificationEmail(array(
							'Id' => $customerId,
							'AppGuid' => $appGuid,
							'Email' => $customerParams['email'],
							'MemberId' => $data['memberId'],
							'ShopName' => $app['ShopName'],
							'FirstName' => $customerParams['firstName'],
							'LastName' => $customerParams['lastName'],
							'Service' => $data['service'][0]->Name,
							'Resource' => $data['resource'][0]->Name,
							'Schedule' => $data['dealDate'],
							'Price' => $data['service'][0]->Cost,
							'Note' => $customerParams['note'],
							'Phone' => $customerParams['contact'],
							'OnlyToCustomer' => TRUE,
					));
			if($sent === FALSE)
			{
				echo '{"success":false, "errorCode": 5}';
				return;
			}
		}
		echo '{"success":true, "errorCode": 0}';		
	}
}