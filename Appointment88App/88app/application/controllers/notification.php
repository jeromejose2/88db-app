<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notification extends CI_Controller {
	private $memberId;
	public function __construct($config = array())
	{
		parent::__construct();
		$this->load->library('email');
		$this->load->model("service_model");
		$this->load->model("resource_model");
		$this->load->model("timeslotstatus_model");
		$this->load->model("customer_model");
		$this->load->model("booking_model");
		$this->load->model("setting_model");
		$this->load->model("sendemail_model");
		$this->load->model("app_model");
		
		$this->load->library("app88authen");
	}

	//send reminder email to member and owner
	//ignore shopid
	public function index()
	{
		$this->SendReminderEmail();
	}
	
	public function SendReminderEmail()
	{		
		$date = $this->common->addTime($this->common->local_datetime(''), 24);
		$cd = strtotime($date);
		$resultDate=date('Y-m-d H:i:s',  mktime(
			date('H',$cd),
			date('i',$cd), 
			0, 
			date('m',$cd),
			date('d',$cd), 
			date('Y',$cd)
		));					
		$settings = $this->setting_model->getAllShopSettings();
		if( $settings === FALSE)
		{
			echo '{"status":"FAIL", "errorNo":1, "errorDesc": "Database get record fail"}';
			return ;
		}
		
		$result = '';
		foreach($settings as $setting)
		{
			$result .= 'AppInstanceGuid:'. $this->guid->createFromBinary($setting->AppInstanceGuid)->toString();
			if($setting->EnableReminderEmail === 'T')
			{
				$reminderMinute = $setting->ReminderMinute;
				$ownerReminderDate = $this->common->addTime($this->common->local_datetime(''), 0, $reminderMinute);				
				$ownercd = strtotime($ownerReminderDate);
				$ownerResultDate=date('Y-m-d H:i:s',  mktime(
					date('H',$ownercd),
					date('i',$ownercd), 
					0, 
					date('m',$ownercd),
					date('d',$ownercd), 
					date('Y',$ownercd)
				));		
				$ownerBookings = $this->booking_model->getNeedReminderBookings($ownerResultDate, 'OwnerReminder');
				foreach($ownerBookings as $booking)
				{
					$appGuid = $this->guid->createFromBinary($setting->AppInstanceGuid)->toString();
					$app = $this->app_model->getAppByAppGuid($appGuid);
					
					if ($this->email->sendReminderEmail(array(
						'Id' => $booking->BookingId,
						'AppGuid'=> $appGuid,
						'Email' => $booking->CustomerEmail,
						'ShopName' => $app['ShopName'],
						'FirstName' => $booking->FirstName,
						'LastName' => $booking->LastName,
						'Phone' => $booking->Contact,
						'Service' => $booking->ServiceName,
						'Resource' => $booking->ResourceName,
						'Schedule' => $booking->StartDateTime,
						'Price' => $booking->Cost,
						'Note' => $booking->Note,
						'EmailType' => 'OwnerReminder',
					)) === FALSE){
						echo '{"status":"FAIL", "errorNo":4 "errorDesc": "Email sending fail"}';
						return;
					}
				}
				$result .= ", Send to Owner:".count($ownerBookings);
			}
			$bookings = $this->booking_model->getNeedReminderBookings($resultDate);
			foreach($bookings as $booking)
			{
				$appGuid = $this->guid->createFromBinary($setting->AppInstanceGuid)->toString();
				$app = $this->app_model->getAppByAppGuid($appGuid);
				
				$this->email->sendReminderEmail(array(
					'Id' => $booking->BookingId,
					'AppGuid'=> $appGuid,
					'Email' => $booking->CustomerEmail,
					'MemberId' => $booking->MemberId,
					'ShopName' => $app['ShopName'],
					'FirstName' => $booking->FirstName,
					'LastName' => $booking->LastName,
					'Service' => $booking->ServiceName,
					'Resource' => $booking->ResourceName,
					'Schedule' => $booking->StartDateTime,
					'Price' => $booking->Cost,
					'Note' => $booking->Note,
					'EmailType' => 'CustomerReminder',
				));	
			}
			$result .= ", Send to Member:".count($bookings).'.';			
		}
		echo '{"status":"SUCCESS", "errorNo":0, "result": "'.$result.'"}';
	}
	
	//clear all expired timeslot (for agent to use)
	//ignore shopid
	public function releaseTimeSlot()
	{
		if($this->timeslotstatus_model->releaseExpritedTimeSlot(APP88_RESERVEDAUTOEXPIREDDURATION, TRUE) === FALSE)
		{
			echo '{"status":"FAIL", "errorNo":1, "errorDesc": "Database update fail"}';
		}
		else
		{
			echo '{"status":"SUCCESS", "errorNo":0, "errorDesc": ""}';
		}
	}
	
	//if member goto verify email, then to call, in order to activate pending appointment
	//ignore shop id
	public function activateAppointment()
	{
		$memberId = $this->input->get('memberId');
		$bookings = $this->booking_model->getPendingBookingByMemberBy($memberId, TRUE);
		if($memberId === FALSE){
			echo '{"status":"FAIL", "errorNo":3, "errorDesc": "Missing parameter"}';
			return;
		}
		if($bookings === FALSE)
		{
			echo '{"status":"FAIL", "errorNo":1, "errorDesc": "Database get record fail"}';
			return;
		}
		else if(count($bookings) == 0)
		{
			echo '{"status":"FAIL", "errorNo":5, "errorDesc": "No any booking need to active"}';
			return;
		}else
		{
			foreach($bookings as $booking){		
				$email = $booking->Email;
				$firstName = $booking->FirstName;
				$lastName = $booking->LastName;
				$phoneNumber = $booking->Contact;
				$customerId = $booking->CustomerId;
				$serviceGuid = $this->guid->createFromBinary($booking->ServiceGuid)->toString();
				$resourceGuid = $this->guid->createFromBinary($booking->ResourceGuid)->toString();
				$dealDate = $booking->DealDate;
				$serviceId = $booking->ServiceId;
				$resourceId = $booking->ResourceId;
				$timeSlotId = $booking->TimeSlotStatusId;
				$bookingId = $booking->BookingId;
				$data['service'] = $this->service_model->getServiceSnapShot($serviceGuid);
				$data['resource'] = $this->resource_model->getResourceSnapShot($resourceGuid);
				$data['dealDate'] = $dealDate;
				$data['memberId'] = $memberId;
				$appGuid = $this->guid->createFromBinary($booking->AppInstanceGuid)->toString();
				$setting = $this->setting_model->getSetting($appGuid);		

				//commit booking
				
				if($this->booking_model->activateBooking($bookingId, TRUE) === FALSE)
				{
					echo '{"status":"FAIL", "errorNo":2, "errorDesc": "Database update fail_1"}';
					return;
				}
			
				//update timeslot status to confirmed
				$params = array(
					'timeSlotId' =>$timeSlotId,
					'status' =>"Confirmed"
				);			
				
				if($this->timeslotstatus_model->updateTimeSlotStatusById($params, TRUE) === FALSE)
				{
					echo '{"status":"FAIL", "errorNo":2, "errorDesc": "Database update fail_2"}';
					return;
				}

				//send reminder email
				if($setting["enableNotificationEmail"] == 'T')
				{
					$customerParams = array(
							'firstName' => $firstName,
							'lastName' => $lastName,
							'contact' => $phoneNumber,
							'email' => $email,
							'memberId' => $memberId
					);
					
					$app = $this->app_model->getAppByAppGuid($appGuid);
					
					if ($this->email->sendNotificationEmail(array(
							'Id' => $customerId,
							'AppGuid' => $appGuid,
							'Email' => $customerParams['email'],
							'MemberId' => $data['memberId'],
							'ShopName' => $app['ShopName'],
							'FirstName' => $customerParams['firstName'],
							'LastName' => $customerParams['lastName'],
							'Phone' => $customerParams['contact'],
							'Service' => $data['service'][0]->Name,
							'Resource' => $data['resource'][0]->Name,
							'Schedule' => $data['dealDate'],
							'Price' => $data['service'][0]->Cost,
							'Note' => $customerParams['note'],
							
					)) === FALSE)
					{
						echo '{"status":"FAIL", "errorNo":4 "errorDesc": "Email sending fail"}';
						return;
					}
				}
				
				echo '{"status":"SUCCESS", "errorNo":0, "errorDesc": "Actived Bookings"}';//'Successfully Active Bookings: BookingId:'.$bookingId.', AppInstanceGuid:'.$this->guid->createFromBinary($booking->AppInstanceGuid)->toString();
			}
		}
	}
}