<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service extends CI_Controller {
	
	public function __construct($config = array())
	{
		parent::__construct();
		$this->lang->load('front', $this->app88param->getLang()?$this->app88param->getLang():APP88_FRONT_LANGUAGE);
		$this->load->model("service_model");
		$this->load->model("resource_model");
        $this->load->model("timeslotstatus_model");
		if (!$this->app88param->get88AppGuid())
		{
			show_error($this->lang->line('err_session_expired'));
		}	
	}
	
	public function getServerDetail(){	
		$serviceId = $this->input->post("serviceId");
		if( $serviceId === "" ){
			echo '{}';
			return;
		}
		$service = $this->service_model->getServiceDetailByGuid($serviceId, TRUE);
		$service->Name = form_prep($service->Name);
		if($service){
			echo json_encode($service);
		}else{
			echo '{}';
		}
	}
    
    public function getAvailableTimeSlotByDate()
    {
        $serviceId = $this->input->post("serviceid");
        $resourceId = $this->input->post("resourceid");
        $date = str_replace("_", " ", $this->input->post("date"));
        $timeSlotsStatus = $this->timeslotstatus_model->getAvailableTimeSlotByDate($this->service_model->getServiceIdByGuid($serviceId), $this->resource_model->getResourceIdByGuid($resourceId), $date);
		//print_r( $this->timeslotstatus_model->getAvailableTimeSlotByMonth($this->service_model->getServiceIdByGuid($serviceId), $this->resource_model->getResourceIdByGuid($resourceId), $date) );
        echo json_encode($timeSlotsStatus);
    }
	
	public function getAvailableTimeSlotByMonth()
    {
        $serviceId = $this->input->post("serviceid");
        $resourceId = $this->input->post("resourceid");
		$isMutilResouceGuid = is_array($resourceId);		
        $date = str_replace("_", " ", $this->input->post("date"));
		
		if($isMutilResouceGuid)
		{
			$result = array();
			foreach($resourceId as $id)
			{
				$result[] = $timeSlotsStatus = $this->timeslotstatus_model->getAvailableTimeSlotByMonth($this->service_model->getServiceIdByGuid($serviceId), $this->resource_model->getResourceIdByGuid($id), $date);				
			}
			echo json_encode($result);
			return;
		}
		else
		{
			$timeSlotsStatus = $this->timeslotstatus_model->getAvailableTimeSlotByMonth($this->service_model->getServiceIdByGuid($serviceId), $this->resource_model->getResourceIdByGuid($resourceId), $date);				
			echo json_encode($timeSlotsStatus);
		}
    }
	
	public function getAvailableServicesNameList()
	{
		$services = $this->service_model->getAvailableServicesNameList();
		foreach($services as $service)
		{
			$service->ServiceGuid = $this->guid->createFromBinary($service->ServiceGuid)->toString();
			$service->Name = form_prep($service->Name);
		}
		echo json_encode($services);
	}
}