<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Resource extends CI_Controller {
		  
	public function __construct($config = array())
	{
		parent::__construct();
		$this->lang->load('front', $this->app88param->getLang()?$this->app88param->getLang():APP88_FRONT_LANGUAGE);
		$this->load->model("resource_model");
		if (!$this->app88param->get88AppGuid())
		{
			show_error($this->lang->line('err_session_expired'));
		}	
	}
	
	private function getSingleResourceDetail($resourceGuid, $includedMoreInfo, $ignoreServiceStatus)
	{
		$resource = $this->resource_model->getResourceDetailByGuid($resourceGuid, FALSE, 0, $ignoreServiceStatus);
		$resource->ResourceGuid = $this->guid->createFromBinary($resource->ResourceGuid)->toString();
		if(ISSET($resource->Services) === FALSE)
		{
			$resource->Services = array();
		}
		else
		{
			foreach($resource->Services as $service)
			{
				$service->ServiceGuid =  $this->guid->createFromBinary($service->ServiceGuid)->toString();
			}
		}
		$resource->Name = form_prep($resource->Name);
		if($includedMoreInfo === TRUE)
		{
			$resource->MoreInfo = $this->resource_model->getResourceServiceDateTime($resourceGuid);   
		}
		return $resource;
	}
	public function getResourceDetail()
	{
		$resourceGuid = $this->input->post("resourceGuid");
		$includedMoreInfo = $this->input->post("includedMoreInfo") === 'true';
		$ignoreServiceStatus = $this->input->post("ignoreServiceStatus") === 'true';
		$isMutilResouceGuid = is_array($resourceGuid);
		if($isMutilResouceGuid)
		{
			$result = array();
			foreach($resourceGuid as $id)
			{
				$result[] = $this->getSingleResourceDetail($id, $includedMoreInfo, $ignoreServiceStatus);
			}
			echo json_encode($result);
			return;
		}
		else
		{
			if( $resourceGuid === "" )
			{
				echo '{}';
				return;
			}
			else
			{
				echo json_encode($this->getSingleResourceDetail($resourceGuid, $includedMoreInfo, $ignoreServiceStatus));
				return;
			}
		}		
	}
	
	public function getResources(){
		$resourceGuids = $this->input->post("resourceGuids");	
		$includedMoreInfo = $this->input->post("includedMoreInfo") === 'true';
		$resources = $this->resource_model->getResources($resourceGuids);	
		if($includedMoreInfo === TRUE)
		{
			foreach($resources as $resource)
			{				
				$resource->MoreInfo = $this->resource_model->getResourceServiceDateTime($this->guid->createFromBinary($resource->ResourceGuid)->toString());   
				$resource->ResourceGuid =  $this->guid->createFromBinary($resource->ResourceGuid)->toString();
			}
		}
		echo json_encode($resources);
	}
	
	public function getResourceServiceDateTime(){
		$resourceGuid = $this->input->post("resourceGuid");
		if( $resourceGuid === "" ){
			echo '[]';
			return;
		}
		$resource = $this->resource_model->getResourceServiceDateTime($resourceGuid);   
		if($resource){
			echo json_encode($resource);
		}else{
			echo '[]';
		}
	}
	
	public function getAvailableResourceNameListByServiceGuid()
	{
		$serviceGuid = $this->input->post("serviceGuid");
		$resources = $this->resource_model->getAvailableResourceNameListByServiceGuid($serviceGuid);	
		if($resources === FALSE)
		{
			echo '{}';
			return;
		}
		foreach($resources as $resource)
		{
			$resource->ResourceGuid = $this->guid->createFromBinary($resource->ResourceGuid)->toString();
			$resource->Name = form_prep($resource->Name);
		}
		echo json_encode($resources);
	}
}