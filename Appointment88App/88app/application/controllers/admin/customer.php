<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer extends CI_Controller {
	
	private $shopId;
	public function __construct($config = array())
	{
		parent::__construct();        
		$this->lang->load('admin', $this->app88common->get_shop_lang());
		$this->load->library('app88Authen');
		$this->load->library('email');
		$this->load->model("setting_model");
		$this->load->model("customer_model");
		$this->load->model("service_model");
		$this->load->model("booking_model");
		$this->load->model("deletebookinglog_model");
		$this->load->model("timeslotstatus_model");
		$this->load->model("sendemail_model");
		$this->load->model("customernote_model");
		$this->shopId = $this->app88param->get88AppGuid();
		
		if (!$this->app88userinfo->getMemberId() || !$this->app88param->get88AppGuid())
		{
			show_error($this->lang->line('err_session_expired'));
		}
		if (!$this->app88userinfo->isShopOwner())
		{
			redirect('fo/index');
		}
		
	}
	public function index()
    {	
		$data["customersName"] = $this->customer_model->getCustomersNameList();
        $this->load->view('admin/customer/index', $data);
    }		
	
	public function getCustomerNameList()
	{
		$customers = $this->customer_model->getCustomersNameList();
		foreach($customers as $customer)
		{
			$customer->CustomerGuid =  $this->guid->createFromBinary($customer->CustomerGuid)->toString();
		}
		echo json_encode($customers);
	}
	
	public function getCustomerMoreDetail()
	{	
		$customerGuid = $this->input->post("customerGuid");
		
		$customer = $this->customer_model->getCustomerInfoByGuid($customerGuid);

		$upcomingAppointments = $this->booking_model->getBookingList($customer->MemberId, 0, "upcoming", 1, '', '', '', '', 'ASC', $customer->Id);
		$prevousAppointments = $this->booking_model->getBookingList($customer->MemberId, 3, "previous", 1, '', '', '', '', 'DESC', $customer->Id);
		$latestCustomerNotes = $this->customernote_model->getCustomerNotes($customer->Id, 1, 3);
		foreach($upcomingAppointments["rows"] as $appointment)
		{
			$appointment->BookingGuid = $this->guid->createFromBinary($appointment->BookingGuid)->toString();
			$appointment->ResourceGuid = $this->guid->createFromBinary($appointment->ResourceGuid)->toString();
			$appointment->ServiceGuid = $this->guid->createFromBinary($appointment->ServiceGuid)->toString();
			$appointment->FormatCost = $this->common->number_format($appointment->Cost);
			$appointment->Note = form_prep($appointment->Note);
			$appointment->ServiceName = form_prep($appointment->ServiceName);
		}
		foreach($prevousAppointments["rows"] as $appointment)
		{
			$appointment->BookingGuid = $this->guid->createFromBinary($appointment->BookingGuid)->toString();
			$appointment->FormatCost = $this->common->number_format($appointment->Cost);
			$appointment->ServiceName = form_prep($appointment->ServiceName);
		}
		foreach($latestCustomerNotes["rows"] as $note)
		{
			$note->CreatedTime = $this->common->local_datetime($note->CreatedTime);
			$note->Guid = $this->guid->createFromBinary($note->Guid)->toString();
			$note->Message = form_prep($note->Message);
		}
		$result = array(
			'info' => array(
				'contact'=>form_prep($customer->Contact),
				'lastName'=>form_prep($customer->LastName),
				'firstName'=>form_prep($customer->FirstName),
				'email'=>form_prep($customer->Email),
				'isOffline'=>$customer->IsOffline
			),
			'upcomingAppointments' => $upcomingAppointments["rows"],
			'prevouisAppointments' => $prevousAppointments["rows"],
			'prevouisAppointmentsCount' => $prevousAppointments["total"],
			'notes' => $latestCustomerNotes["rows"],
			'customerNotesCount' => $latestCustomerNotes["total"]
		);
		echo json_encode($result);
	}
	
	public function cancelAppointment()
	{
		$bookingGuid = $this->input->post("bookingGuid");
		$customerGuid = $this->input->post("customerGuid");
		$reason = $this->input->post("reason");
		if($reason === FALSE)
		{
			return FALSE;
		}
		$customer = $this->customer_model->getCustomerInfoByGuid($customerGuid);
		$booking = $this->booking_model->getBookingDetailByGuid($bookingGuid, $customer->Id);
		$service = $this->service_model->getServiceDetailByGuid("", FALSE, $booking->ServiceId);
		$resource = $this->resource_model->getResourceDetailByGuid("", FALSE, $booking->ResourceId);
		$appGuid = $this->guid->createFromBinary($booking->AppInstanceGuid)->toString();
		$app = $this->app_model->getAppByAppGuid($appGuid);
		
		//delete booking
		$this->booking_model->deleteBooking($bookingGuid, $customer->Id, $customer->MemberId, $booking->TimeSlotStatusId);
		//add log
		$this->deletebookinglog_model->createLog(array(
			'bookingId' =>$booking->Id,
			'reason'=>$reason
		));
		if($customer->MemberId != '0')
		{
			//sent email
			$this->email->sendCancelAppointmentEmail(array(
				'From' => 'owner',
				'Id' => $customer->Id,
				'Email' => $customer->Email,
				'MemberId' => $customer->MemberId,
				'ShopName' => $app['ShopName'],
				'FirstName' => $customer->FirstName,
				'LastName' => $customer->LastName,
				'Service' => $service->Name,
				'Resource' => $resource->Name,
				'Schedule' => $booking->StartDateTime,
				'Price' => $service->Cost,
				'Note' => $booking->Note,
				'Reason' => $reason,
				'Phone' => $customer->Contact,
			));
		}
		echo "{\"success\":true}";
	}
	
	public function getPrevAppointments()
	{
		$customerGuid = $this->input->post("customerGuid");
		$customer = $this->customer_model->getCustomerInfoByGuid($customerGuid);
		$memberId = $customer->MemberId;
		$pageSize = $this->input->post("pageSize");
		$pageNumber= $this->input->post("pageNumber");
		$type = "previous";		
		$appointments = $this->booking_model->getBookingList($memberId, $pageSize, $type, $pageNumber);
		foreach($appointments["rows"] as $m)
		{
			$m->BookingGuid = $this->guid->createFromBinary($m->BookingGuid)->toString();
			$m->FormatCost = $this->common->number_format($m->Cost);
			$m->ServiceName = form_prep($m->ServiceName);
		}
		$result = array(
			'total'=>$appointments["total"],
			'rows'=>$appointments["rows"],
		);
		echo json_encode($result);
	}
	
	public function sendCustomerMessage()
	{
		$customerGuid = $this->input->post('customerGuid');
		$message = $this->input->post('message');
		$customerName = $this->input->post('customerName');
		if(empty($message))
		{
			return FALSE;
		}
		$customer = $this->customer_model->getCustomerInfoByGuid($customerGuid);
		if($customer->MemberId != '0')
		{
			$this->email->sendCustomerMessage(array(
				'customerId' => $customer->Id,
				'message' => $message,
				'customerName' =>$customerName,
				'email' => $customer->Email,
				'memberId' => $customer->MemberId
			));
		}
	}
	
	public function addCustomerNote()
	{		
		$message = $this->input->post("message");
		$customerGuid = $this->input->post("customerGuid");		
		if($message === FALSE)
		{
			return FALSE;
		}
		$customer = $this->customer_model->getCustomerInfoByGuid($customerGuid);		
		$result = $this->customernote_model->createCustomerNote(array(
						'customerId' =>$customer->Id,
						'message'=>$message
					));
		if($result === FALSE)
		{
			echo "{\"success\":false}";
		}
		else
		{
			echo "{\"success\":true, \"createdTime\":\"".$this->common->local_datetime($result["createdTime"])."\", \"Id\": \"".$result["guid"]."\"}";			
		}
	}
	
	public function deleteCustomerNote()
	{
		$noteGuid = $this->input->post("noteGuid");
		$this->customernote_model->deleteCustomerNote($noteGuid);
		echo "{\"success\":true}";
	}
	
	public function getCustomerNotes()
	{
		$customerGuid = $this->input->post("customerGuid");
		$customer = $this->customer_model->getCustomerInfoByGuid($customerGuid);
		$pageSize = $this->input->post("pageSize");
		$pageNumber= $this->input->post("pageNumber");
		$customer = $this->customernote_model->getCustomerNotes($customer->Id, $pageNumber, $pageSize);
		foreach($customer["rows"] as $note)
		{
			$note->CreatedTime = $this->common->local_datetime($note->CreatedTime);
			$note->Guid = $this->guid->createFromBinary($note->Guid)->toString();
			$note->Message = form_prep($note->Message);
		}
		$result = array(
			'total'=>$customer["total"],
			'rows'=>$customer["rows"],
		);
		echo json_encode($result);
	}
}