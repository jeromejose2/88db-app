<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service extends CI_Controller {
	private $shopId;
	public function __construct($config = array())
	{
		parent::__construct();        
		$this->lang->load('admin', $this->app88common->get_shop_lang());
		$this->load->library('app88Authen');
		$this->load->model("service_model");
		$this->shopId = $this->app88param->get88AppGuid();

		
		if (!$this->app88userinfo->getMemberId() || !$this->app88param->get88AppGuid())
		{
			show_error($this->lang->line('err_session_expired'));
		}
		if (!$this->app88userinfo->isShopOwner())
		{
			redirect('fo/index');
		}
		
	}
	public function servicelist()
	{
		$action = $this->input->post('action');
		if(!empty($action)) 
		{
			$guid = $this->input->post('guid');
			switch ($action)
			{
				case "delete":					
					$this->service_model->updateServiceStatus($guid, 'D');
					break;
				default:
					break;
			}			
		}
		$condition = array();
		$pageSize = APP88_PAGESIZE;
		$pageNumber = $this->input->get("page");
		if($pageNumber === FALSE)
		{
			$pageNumber = 1;
		}
		$sortName = $this->input->get("sn");
		$sortName = $sortName === FALSE?"Weight":$sortName;
		$sortOrder = $this->input->get("so");		
		$sortOrder = $sortOrder === FALSE?"asc": ($sortOrder == "d"?"desc":"asc");
		$q = $this->input->get("q");
		if($q !== FALSE && $q !== "")
		{		
			$condition["q"] = $q;
		}		
		$supportSortName = array("Id", "Name", "Duration", "Cost", "Status", "Staff", "Weight");
		if( !in_array($sortName, $supportSortName))
		{
			//show_error("no support current sort name.");
			show_error($this->lang->line('err_database_err'));
		}
		$list =  $this->service_model->getServicesList($this->shopId, $condition, $pageSize, $pageNumber, $sortName, $sortOrder);
		$data["services"] = $list;
		$data["pageNumber"] = $pageNumber;
        $this->load->view('admin/service/service_list', $data);
	}
	public function createservice()
	{
		if(isset($_POST['name'])) 
		{ 
			$model = array(
				'guid' => $this->input->post("id"),
				'name' => $this->input->post("name"),
				'assignColor' => $this->input->post("assignColor"),
				'cost' => $this->input->post("cost"),
				'duration' => $this->input->post("duration"),
				'description' => html_entity_decode($this->input->post("description")),
				'advanceTime' => $this->input->post("advanceTime") === ""? NULL :$this->input->post("advanceTime"),
				'minTime' => $this->input->post("minTime"),
				'availablePerTimeSlot' => $this->input->post("availablePerTimeSlot"),
				'timeSlotDivision' => $this->input->post("timeSlotDivision")
			);
			
			if(empty($model['guid']) === TRUE) 
			{ 
				if( $this->service_model->createService($model))
				{
					redirect('admin/service/servicelist');
				}
				else
				{
					show_error($this->lang->line('dialog_system_error_content'));
				}		
			}
			else
			{
				if( $this->service_model->updateService($model))
				{
					redirect('admin/service/servicelist');
				}
				else
				{				
					show_error($this->lang->line('dialog_system_error_content'));
				}
			}
			
		}
		else
		{
			$serviceGuid = $this->input->get("serviceid");
			$data = array();
			if( $serviceGuid !== FALSE )
			{
				$service = $this->service_model->getServiceDetailByGuid($serviceGuid, TRUE);				
				$data["service"] = $service;
			}
			$this->load->view('admin/service/create_service', $data);
		}
	}
	
	public function switchServiceStatus()
	{
		$serviceGuid = $this->input->post("serviceId");
		$status = $this->input->post("status");
		if($status === "I" || $status === "A")
		{
			echo "{\"success\":".($this->service_model->updateServiceStatus($serviceGuid, $status) ? "true":"false")."}";
		}
		else
		{
			echo "{\"success\":false}";
		}
	}
	
	public function changeServicesWeight()
	{
		$services = $this->input->post("services");	
		if(count($services) == 1)
		{
			$action = $this->input->post("action");
			$service = $services[0];
			$curServiceGuid = $service["serviceGuid"];
			$curWeight = $service["weight"];
			
			$neighbourService = $this->service_model->getNeighbourService($curServiceGuid, $action);
			$neighbourServiceGuid = $this->guid->createFromBinary($neighbourService->Guid)->toString();
			$neighbourWeight = $neighbourService->Weight;
			
			
			$services = array(
				array('serviceGuid'=>$curServiceGuid, 'weight'=>$neighbourWeight),
				array('serviceGuid'=>$neighbourServiceGuid, 'weight'=>$curWeight)
			);
		}		
		
		foreach($services as $service)
		{
			$serviceGuid = $service["serviceGuid"];
			$weight = $service["weight"];
			$result = $this->service_model->updateServiceWeight($serviceGuid, $weight);
		}
		
		echo "{\"success\":".($result===TRUE?"true":"false")."}";
	}
}