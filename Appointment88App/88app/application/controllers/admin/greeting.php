<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Greeting extends CI_Controller {
	private $shopId;
	public function __construct($config = array())
	{
		parent::__construct();        
		$this->lang->load('admin', $this->app88common->get_shop_lang());
		$this->load->library('app88Authen');
		$this->load->model("setting_model");
		$this->shopId = $this->app88param->get88AppGuid();
	
		if (!$this->app88userinfo->getMemberId() || !$this->app88param->get88AppGuid())
		{
			show_error($this->lang->line('err_session_expired'));
		}
		if (!$this->app88userinfo->isShopOwner())
		{
			redirect('fo/index');
		}
		
	}
	public function index()
	{	
		$setting = $this->setting_model->getSetting();
		$data['setting'] = $setting;
		$this->load->view('admin/greeting/index', $data);
	}
	
	public function update()
	{
		$greeting = $this->input->post("greeting");
		$greeting = html_entity_decode($greeting);
		
		if($this->setting_model->updateGreeting($greeting))
		{
			echo "{\"success\":true}";
		}
		else
		{
			echo "{\"success\":false}";
		}
	}
	
}