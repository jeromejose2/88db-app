<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends CI_Controller {
	private $shopId;
	public function __construct($config = array())
	{
		parent::__construct();        
		$this->lang->load('admin', $this->app88common->get_shop_lang());
		$this->load->library('app88Authen');
		$this->load->model("setting_model");
		$this->shopId = $this->app88param->get88AppGuid();
		
		if (!$this->app88userinfo->getMemberId() || !$this->app88param->get88AppGuid())
		{
			show_error($this->lang->line('err_session_expired'));
		}
		if (!$this->app88userinfo->isShopOwner())
		{
			redirect('fo/index');
		}
		
	}
	public function index()
	{	
		$setting = $this->setting_model->getSetting();
		$data['setting'] = $setting;
		$this->load->view('admin/setting/index', $data);
	}
	
	function HtmlEncode($fString)
	{
		if($fString!="")
		{
			$fString = str_replace( '>', '&gt;',$fString);
			$fString = str_replace( '<', '&lt;',$fString);
			$fString = str_replace( chr(32), '&nbsp;',$fString);
			$fString = str_replace( chr(13), ' ',$fString);
			$fString = str_replace( chr(10) & chr(10), '<br>',$fString);
			$fString = str_replace( chr(10), '<BR>',$fString);
		}
		return $fString;
	}
	
	public function update()
	{
		$enableNotificationEmail = $this->input->post("EnableNotificationEmail") === "true" ?"T":"F";
		$enableReminderEmail = $this->input->post("EnableReminderEmail") === "true" ?"T":"F";
		$reminderMinute = $this->input->post("ReminderMinute");
		$customizeTitles = $this->HtmlEncode($this->input->post("CustomizeTitles"));
		
		$setting = array(
			'enableNotificationEmail' =>$enableNotificationEmail,
			'enableReminderEmail' =>$enableReminderEmail,
			'reminderMinute' =>$reminderMinute,
			'customizeTitles' =>$customizeTitles
		);
		
		if($this->setting_model->updateSetting($setting))
		{
			echo "{\"success\":true}";
		}
		else
		{
			echo "{\"success\":false}";
		}
	}
	
}