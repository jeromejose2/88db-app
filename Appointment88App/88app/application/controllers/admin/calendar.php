<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calendar extends CI_Controller {
	
	public function __construct($config = array())
	{
		parent::__construct();
		$this->lang->load('admin', $this->app88common->get_shop_lang());
		$this->load->library('app88Authen');
		$this->load->model("service_model");
		$this->load->model("resource_model");
		$this->load->model("booking_model");
		$this->load->model("timeslotstatus_model");
		$this->load->model("customer_model");
		
		if (!$this->app88userinfo->getMemberId() || !$this->app88param->get88AppGuid())
		{
			show_error($this->lang->line('err_session_expired'));
		}
		if (!$this->app88userinfo->isShopOwner())
		{
			redirect('fo/index');
		}
		
	}

	public function index()
	{
		redirect('admin/calendar/month');
	}	
	
	public function month()
	{		
		$data["servicesNameList"] = $this->service_model->getServicesNameList(TRUE);
		$data["resourceNameList"] =$this->resource_model->getResourceNameList(TRUE);
		$this->load->view('admin/calendar/month', $data);
	}
	
	public function getConditionalAppointments()
	{
		$resourceGuid = $this->input->post("resourceGuid");
		$serviceGuid = $this->input->post("serviceGuid");
		$startDate = $this->input->post("startDate").' 00:00:00';
		$endDate = $this->input->post("endDate").' 23:59:59';
		$appointments = $this->booking_model->getBookingList(0, 0, "all", 1, $serviceGuid, $resourceGuid, $startDate, $endDate);
		foreach($appointments["rows"] as $appointment)
		{
			$appointment->BookingGuid = $this->guid->createFromBinary($appointment->BookingGuid)->toString();
			$appointment->CustomerGuid = $this->guid->createFromBinary($appointment->CustomerGuid)->toString();
			$appointment->ServiceGuid = $this->guid->createFromBinary($appointment->ServiceGuid)->toString();
			$appointment->ResourceGuid = $this->guid->createFromBinary($appointment->ResourceGuid)->toString();
			$appointment->FormatCost = $this->common->number_format($appointment->Cost);
			$appointment->ServiceName = form_prep($appointment->ServiceName);
		}
		echo json_encode($appointments);
	}	
	
	public function checkScheduleDateTime($date, $serviceGuid, $resourceGuid)
	{
		$dealDate = date('Y-m-d H:i:s', strtotime($date)); 
		$now = $this->app88common->local_datetime();
		$service = $this->service_model->getServiceDetailByGuid($serviceGuid);
		
		//check is after today
		$minDate = $this->common->addTime($now, -1 * $service->MinTime);
		$maxDate = $this->common->addTime($now, (empty($service->AdvanceTime)?20*365*24:$service->AdvanceTime));
		if( (strtotime($dealDate) < strtotime($minDate)) || (strtotime($dealDate) >strtotime($maxDate))){
			return FALSE;
		}
        
		$resourceServiceDate = $this->resource_model->getResourceServiceDateTime($resourceGuid);
		//check specialHours
		$isInSpecialHours = FALSE;
		if(ISSET($resourceServiceDate["SpecialHours"]))
		{
			foreach ($resourceServiceDate["SpecialHours"] as $specialhour) 
			{
				$specialDate = date('Y-m-d', strtotime($dealDate));
				$startDate = date('Y-m-d H:i:s', strtotime($specialhour->Date." ".$specialhour->StartTime));
				$endDate = date('Y-m-d H:i:s', strtotime($specialhour->Date." ".$specialhour->EndTime));
				if( strcmp($specialhour->Date, $specialDate) === 0 ) 
				{
					if( ($dealDate >= $startDate) && ($dealDate <= $endDate)){
						//check is start time            
						$rangStartTime  = $this->common->addTime($startDate, 0);
						$isInTimeSlot = FALSE;
						do
						{
							if( $rangStartTime === $dealDate)
							{
								$isInTimeSlot = TRUE;
								break;
							}
							$rangStartTime  = $this->common->addTime($rangStartTime, 0, $service->TimeSlotDivision);
							$i--;
						}while(($rangStartTime <= $endDate));
						if( !$isInTimeSlot ){
							return FALSE;
						}
						
						$isInSpecialHours = TRUE;
						break;
					}
					if( ! $isInSpecialHours )
					{
						return FALSE;
					}
				}            
			}
		}
        
        
		//check is normal day
		$dealDay = date("w",strtotime($dealDate)); 
        
		$inWeekDay = FALSE;
        
		if( $isInSpecialHours )
		{
			$inWeekDay = TRUE;
		}
		else
		{       		
			foreach ($resourceServiceDate["NormalHours"] as $normalHours) 
			{				
				if( $normalHours->Day === $dealDay )
				{
					$inWeekDay = TRUE;
					$startDate = date('Y-m-d H:i:s', strtotime(date('Y-m-d', strtotime($dealDate))." ".$normalHours->StartTime));
					$endDate = date('Y-m-d H:i:s', strtotime(date('Y-m-d', strtotime($dealDate))." ".$normalHours->EndTime));
					if( ($dealDate < $startDate) || ($dealDate >$endDate)){
						$inWeekDay = FALSE;
						continue;
					}
					
					//check is start date time       
					$rangStartTime  = $this->common->addTime($startDate, 0);
					$isInTimeSlot = FALSE;
					do
					{
						if( $rangStartTime === $dealDate)
						{
							$isInTimeSlot = TRUE;
							break;
						}
						$rangStartTime  = $this->common->addTime($rangStartTime, 0, $service->TimeSlotDivision);
						$i--;
					}while(($rangStartTime <= $endDate));
					if( !$isInTimeSlot ){
						return FALSE;
					}
					break; //is normal day , break
				}
			}
		}		
		if( !$inWeekDay){			
			return FALSE;
		}		
		
		//check is day off
		$isDayoff = FALSE;
		
		if(ISSET($resourceServiceDate["DayOff"]))
		{
			foreach ($resourceServiceDate["DayOff"] as $dayoff) 
			{
				
				$startDate = date('Y-m-d H:i:s', strtotime($dayoff->StartDate." 00:00"));
				$endDate = date('Y-m-d H:i:s', strtotime($dayoff->EndDate." 23:59:59"));
				if( ($dealDate >= $startDate) && ($dealDate <= $endDate)){
					$isDayoff = TRUE;
					break;
				}
			}
		}
		if( $isDayoff )
		{
			return FALSE;
		}
		return TRUE;
	}
	
	public function getSameScheduleResources()
	{
		$serviceGuid = $this->input->post('serviceGuid'); 
		$resouceGuid = $this->input->post('resouceGuid'); 
		$schedule = $this->input->post('schedule'); 
		$resourceList = $this->resource_model->getAvailableResourceNameListByServiceGuid($serviceGuid);
		$serviceId = $this->service_model->getServiceIdByGuid($serviceGuid);
		
		$availableResource = array();
		if( $resourceList !== FALSE )
		{
			foreach($resourceList as $resource)
			{
				$curResourceGuid = $this->guid->createFromBinary($resource->ResourceGuid)->toString();
				$curResourceId = $this->resource_model->getResourceIdByGuid($curResourceGuid);
				if(
					$curResourceGuid != $resouceGuid && 
					$this->checkScheduleDateTime($schedule, $serviceGuid, $curResourceGuid) === TRUE &&					
					$this->timeslotstatus_model->getConfirmTimeSlotCount($serviceId, $curResourceId, $schedule) < $resource->AvailablePerTimeSlot
				)
				{
					array_push($availableResource, array(
						'resourceGuid' => $curResourceGuid,
						'name' => $resource->Name
					));				
				}
			}
		}
		echo json_encode($availableResource);
	}
	

	public function updateAppointmentResouce()
	{
		$serviceGuid = $this->input->post('serviceGuid'); 
		$resouceGuid = $this->input->post('resouceGuid'); 
		$schedule = $this->input->post('schedule'); 
		$customerGuid = $this->input->post('customerGuid'); 
		$bookingGuid = $this->input->post('bookingGuid'); 
		$serviceId = $this->service_model->getServiceIdByGuid($serviceGuid);		
		$resourceId = $this->resource_model->getResourceIdByGuid($resouceGuid);
		
		//get service
		$service = $this->service_model->getServiceDetailByGuid($serviceGuid);		
		if( 
			$this->checkScheduleDateTime($schedule, $serviceGuid, $resouceGuid) === TRUE && 
			$this->timeslotstatus_model->getConfirmTimeSlotCount($serviceId, $resourceId, $schedule) < $service->AvailablePerTimeSlot			
		)
		{
			$customer =  $this->customer_model->getCustomerInfoByGuid($customerGuid);
			$booking = $this->booking_model->getBookingDetailByGuid($bookingGuid, $customer->Id);
					
			//get resource snapshot
			$resourceSnapShot = $this->resource_model->getResourceSnapShot($resouceGuid);		
			if($resourceSnapShot === FALSE)
			{
				$this->resource_model->createResourceSnapShot($resouceGuid);		
				$resourceSnapShot = $this->resource_model->getResourceSnapShot($resouceGuid);
			}
			
			//update timeslot			
			$this->timeslotstatus_model->updateTimeSlotRerouceId(array(
				'resourceId'=>$resourceId,
				'tileSlotId'=>$booking->TimeSlotStatusId
			));
			//update booking
			$this->booking_model->updateBookingResource($booking->Id, $resourceSnapShot[0]->Id);
			
			echo '{"success":true}';
		}
		else
		{
			echo '{"success":false}';
		}
	}
}