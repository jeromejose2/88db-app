<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Resources extends CI_Controller {
	private $shopId;
	public function __construct($config = array())
	{
		parent::__construct();        
		$this->lang->load('admin', $this->app88common->get_shop_lang());
		$this->load->library('app88Authen');
		$this->load->model("resource_model");
		$this->load->model("service_model");
		$this->shopId = $this->app88param->get88AppGuid();

		
		if (!$this->app88userinfo->getMemberId() || !$this->app88param->get88AppGuid())
		{
			show_error($this->lang->line('err_session_expired'));
		}
		if (!$this->app88userinfo->isShopOwner())
		{
			redirect('fo/index');
		}
		
	}
	public function resourceList()
    {		
		$action = $this->input->post('action');
		if(!empty($action)) 
		{
			$guid = $this->input->post('guid');
			switch ($action)
			{
				case "delete":					
					$this->resource_model->deleteResource($guid);
					break;
				default:
					break;
			}			
		}
		
		$resourceNames = $this->resource_model->getResourceNameList();
		$data["resourceNames"] = $resourceNames;
		$data["serviceNames"] = $this->service_model->getServicesNameList(TRUE);
		$this->load->view('admin/resource/resource_list', $data);
    }
	
	public function saveResource()
	{		
		$detail = $this->input->post("detail");
		if($detail === FALSE)
		{
			show_error("no any data.");
		}

		$detail['Description'] = html_entity_decode($detail['Description']);
		
		if(empty($detail["ResourceGuid"]) === TRUE)
		{
			if(($this->resource_model->getActiveResourceCount() >= APP88_RESOURCE_CREATION_LIMIT) && (APP88_RESOURCE_CREATION_LIMIT != -1))
			{
				show_error($this->lang->line('reached_resource_creation_limit'));
			}
			$result = $this->resource_model->createResource($detail);
		}
		else
		{
			$result = $this->resource_model->updateResource($detail);
		}
		
		if($result === TRUE)
		{
			echo "{\"success\":true}";
		}
		else
		{
			echo "{\"success\":false}";
		}
	}
}