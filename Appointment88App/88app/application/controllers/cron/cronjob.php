<?php 
class Cronjob {
	
	private $notify_url = '';
	private $time_file = '';
	private $pid_file = '';
	private $nusoap_path = '';
	private $log_file_name = 'cronjob';
	private $xml_encode = 'utf-8';
	private $log_path = '';
	protected $_date_fmt = 'Y-m-d H:i:s';
	
	public function __construct($config = array())
	{
		$app_path = str_replace("\\", "/", dirname(dirname(__DIR__)));
		
		$this->notify_url = APP88_ACTIVE_APPOINTMENT_API;
		
		$this->time_file = $app_path.'/logs/update_booking_time.log';
		
		$this->pid_file = $app_path.'/logs/update_booking_pid.log';
		
		$this->nusoap_path = $app_path.'/libraries/nusoap/lib/nusoap.php';
		
		$this->log_path = $app_path.'/logs/';
	}
	

	public function updateBooking()
	{
		list($verified_since_time, $last_member_id) = $this->_get_verified_since_time($this->time_file);
		
		$logged_pid = $this->_get_pid_log($this->pid_file);
		
		if ($logged_pid)
		{
			if ($this->_process_exists($logged_pid))
			{
				$output = $this->_kill_previous_process($logged_pid);
				
				$this->_log_message(
						$this->log_file_name, 
						'ERROR', 
						__FILE__.'('.__METHOD__.'@'. __LINE__ .') '.
						'Kill the previous process pid: '.$logged_pid.
						', with result: '.$output);
				
				unset($logged_pid);
			}
		}
		
		$this->_update_pid($this->pid_file);
		
		$result = $this->_get_88db_verified_member_list($verified_since_time);
		
		if ($result !== FALSE)
		{
			$member_list = $result['GetVerifiedMemberListResult']['Members']['APIMemberProxy'];
			
			if (is_array($member_list) && !empty($member_list))
			{
				if (isset($member_list['PassportID']))
				{
					if ($member_list['PassportID'] != $last_member_id)
					{
						$has_error = $this->_activate_booking($member_list['PassportID']);
						
						$this->_update_verified_since_time(
								$this->time_file, 
								$member_list['LastVerifiedTimeUTC'],
								$member_list['PassportID']);
						
						if ($has_error)
						{
							@unlink($this->pid_file);
							exit;
						}
					}
				}
				else
				{
					foreach ($member_list as $member)
					{
						if ($member['PassportID'] != $last_member_id)
						{
							$has_error = $this->_activate_booking($member['PassportID']);
							
							$this->_update_verified_since_time(
									$this->time_file, 
									$member['LastVerifiedTimeUTC'],
									$member['PassportID']);
							
							if ($has_error)
							{
								@unlink($this->pid_file);
								exit;
							}
						}
					}
				}
			}
		}
			
		@unlink($this->pid_file);
	}
	
	
	private function _get_verified_since_time($file_path)
	{
		$file = @fopen($file_path, 'rb');
		
		if ($file === FALSE)
		{
			$file_content = gmdate("Y-m-d\TH:i:s\Z", time() - APP88_RESERVEDAUTOEXPIREDDURATION * 60 - 5 * 60);
		}
		else
		{
			$file_content = fread($file, filesize($file_path));
		}
		
		@fclose($file);
		
		return explode(' ', $file_content);
	}
	
	
	private function _update_verified_since_time($file_path, $time, $member_id)
	{
		$file = fopen($file_path, 'w');
		
		if ($file === FALSE) 
		{
			exit;
		}
		else
		{
			fwrite($file, $time.' '.$member_id);
		}
		
		@fclose($file);
	}
	
	
	private function _update_pid($file_path)
	{
		$file = fopen($file_path, 'w');
		
		if ($file === FALSE)
		{
			exit;
		}
		else
		{
			$pid = getmypid();
			if ($pid === FALSE)
			{
				exit;
			}
			else 
			{
				fwrite($file, $pid);
			}
		}
		
		@fclose($file);
	}
	
	
	private function _get_pid_log($file_path)
	{
		$file = @fopen($file_path, 'rb');
		
		if ($file === FALSE)
		{
			$pid = '';
		}
		else
		{
			$pid = fread($file, filesize($file_path));
		}
		
		@fclose($file);
		
		return $pid;
	}
	
	
	private function _activate_booking($member_id)
	{
		$has_error = FALSE;
		
		$ch = curl_init();
			
		curl_setopt($ch, CURLOPT_URL, $this->notify_url."?memberId=".urlencode($member_id));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			
		$output = curl_exec($ch);
		
		$this->_log_message(
				$this->log_file_name,
				'ERROR',
				'Log start for member id: '.$member_id);
		
		$this->_log_message(
			$this->log_file_name, 
			'ERROR', 
			__FILE__.'('.__METHOD__.'@'. __LINE__ .') Curl info: '.json_encode(curl_getinfo($ch)));
		
		if (curl_errno($ch))
		{
			$this->_log_message(
					$this->log_file_name, 
					'ERROR', 
					__FILE__.'('.__METHOD__.'@'. __LINE__ .') Curl Error num: '.curl_errno($ch).' Curl Error Desc: '.curl_error($ch));
			
			$has_error = TRUE;
		}
		
		$this->_log_message(
				$this->log_file_name,
				'ERROR',
				__FILE__.'('.__METHOD__.'@'. __LINE__ .') Curl output: '.$output);
		
		$this->_log_message(
				$this->log_file_name,
				'ERROR',
				'Log end for member id: '.$member_id);
		
		curl_close($ch);
		
		$output = json_decode($output, TRUE);
		
		if  ($output['status'] == 'FAIL' && 
			($output['errorNo'] == '1' || $output['errorNo'] == '2' || $output['errorNo'] == '3'))
		{
			$has_error = TRUE;
		}
		
		return $has_error;
	}
	
	
	private function _get_88db_verified_member_list($verified_since_time)
	{
		require_once($this->nusoap_path);
		
		$url = APP88_SHOPPASSPORTSERVICE_ENDPOINT;
		
		$client = new nusoap_client($url, TRUE);
		
		if ($client->getError())
		{
			$this->_log_message(
					$this->log_file_name, 
					'ERROR', 
					'Fail to connect '.$url.' with error: '.$client->getError());
			
			return FALSE;
		}
		
		$xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sso="http://sso.88db.com/">';
		$xml .= 	'<soapenv:Header/>';
		$xml .= 	'<soapenv:Body>';
		$xml .= 		'<sso:GetVerifiedMemberList>';
		$xml .= 			'<sso:verifiedSince>'.$verified_since_time.'</sso:verifiedSince>';
		$xml .=			'</sso:GetVerifiedMemberList>';
		$xml .=		'</soapenv:Body>';
		$xml .=	'</soapenv:Envelope>';
		
		$client->soap_defencoding = $this->xml_encode;
		$client->decode_utf8 = FALSE;
		$client->xml_encoding = $this->xml_encode;
		
		$result = $client->send($xml, 'http://sso.88db.com/GetVerifiedMemberList');
		
		if ($client->fault)
		{
			$this->_log_message(
					$this->log_file_name, 
					'ERROR', 
					__FILE__.'('.__METHOD__.'@'. __LINE__ .') '.$client->fault->faultcode);
			
			$this->_log_message(
					$this->log_file_name, 
					'ERROR', 
					__FILE__.'('.__METHOD__.'@'. __LINE__ .') '.$client->fault->faultstring);
			
			return FALSE;
		}
		elseif ($client->getError())
		{
			$this->_log_message(
					$this->log_file_name, 
					'ERROR', 
					__FILE__.'('.__METHOD__.'@'. __LINE__ .') '.$xml);
			
			$this->_log_message(
					$this->log_file_name, 
					'ERROR', 
					__FILE__.'('.__METHOD__.'@'. __LINE__ .') '.htmlspecialchars($client->response, ENT_QUOTES));
			
			$this->_log_message(
					$this->log_file_name, 
					'ERROR', 
					'Error in web service '.$url.' with error: '.$client->getError());
			
			return FALSE;
		}
		elseif ($result['GetVerifiedMemberListResult']['Success'] == 'false')
		{
			$this->_log_message(
					$this->log_file_name, 
					'ERROR', 
					__FILE__.'('.__METHOD__.'@'. __LINE__ .') '.$xml);
			
			$this->_log_message(
					$this->log_file_name, 
					'ERROR', 
					__FILE__.'('.__METHOD__.'@'. __LINE__ .') '.htmlspecialchars($client->response, ENT_QUOTES));
			
			$this->_log_message(
					$this->log_file_name, 
					'ERROR', 
					'Error in web service '.$url.' with error ('. 
					$result['GetOptionListResult']['ErrorCode'] .'): '.
					$result['GetVerifiedMemberListResult']['ErrorMessage']);
			
			return FALSE;
		}
		
		return $result;
	}
	
	
	private function _process_exists($pid)
	{
		if (PHP_OS == 'WINNT')
		{
			$output = shell_exec('tasklist');
			return (strpos($output, " $pid ") !== false);
		}
		elseif (PHP_OS == 'Linux')
		{
			exec('ps '.$pid, $output, $result);
	        return (count($output) == 2); 
		}
		else 
		{
			@unlink($this->pid_file);
			exit;
		}
	}
	
	
	private function _kill_previous_process($pid)
	{
		$output = '';
		
		if (PHP_OS == 'Linux')
		{
			$output = shell_exec('kill -9 '.$pid);
		}
		elseif (PHP_OS == 'WINNT')
		{
			$output = exec('taskkill /T /F /PID >nul 2>&1'.$pid);
		}
		else 
		{
			@unlink($this->pid_file);
			exit;
		}
		
		return $output;
	}
	
	
	private function _log_message($log_name = 'log', $level = 'error', $msg, $php_error = FALSE)
	{
		$level = strtoupper($level);
		
		$filepath = $this->log_path.$log_name.'-'.date('Y-m-d').'.php';
		$message  = '';
		
		if ( ! file_exists($filepath))
		{
			$message .= "<"."?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?".">\n\n";
		}
		
		if ( ! $fp = @fopen($filepath, 'a'))
		{
			return FALSE;
		}
		
		$message .= $level.' '.(($level == 'INFO') ? ' -' : '-').' '.@date($this->_date_fmt). ' --> '.$msg."\n";
		
		flock($fp, LOCK_EX);
		fwrite($fp, $message);
		flock($fp, LOCK_UN);
		fclose($fp);
		
		@chmod($filepath, FILE_WRITE_MODE);
		
		return TRUE;
	}
}

error_reporting(0);
$app_path = str_replace("\\", "/", dirname(dirname(__DIR__)));
include dirname($app_path).'/88appVars.php';
include dirname($app_path).'/config/'.ENV.'/'.COUNTRY.'.php';
$cronjob = new Cronjob();
$cronjob->updateBooking();