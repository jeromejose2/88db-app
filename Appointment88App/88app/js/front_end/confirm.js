﻿$(function () {
	appointment.common.setDefaultValue();
	appointment.common.setValidation();
	refreshVerifyCode();
	function refreshVerifyCode() {
		$("#imgConfirmVerifyCode").attr("src", "/appointment/getConfirmVerifyCode?cid=" + global.cid + "&t" + appointment.common.getRandom());
	}
	//get service detail
	window.appointment.getServiceDetail({ serviceId: $.query.get("serviceid") }, function (r) {
		if (!r.Name) {
			return;
		}
		$(".appServiceName").html(r.Name);
		$("#serviceName").html(r.Name);
	});

	//get resource detail
	window.appointment.getResourceDetail({ resourceGuid: $.query.get("resourceid") }, function (r) {
		if (!r.Name) {
			return;
		}
		var dealDateString = window.appointment.dateTimeFormat($.query.get("datetime").replace("-", " "));
		$(".appDealDate").html(r.Name + "<br/>" + dealDateString);
		$("#resourceName").html(r.Name);
	});

	var dealDateString = window.appointment.dateTimeFormat($.query.get("datetime").replace("-", " "));
	//$(".appDealDate").html(dealDateString);
	$("#scheduleTime").html(dealDateString);
	/*
	$(".appBtnGotoStep2").click(function () {
		location.href = global["SELECTERESOURCE_URL"] + "&serviceid=" + $.query.get("serviceid");
	});
	*/
	$(".appBtnGotoStep3").click(function () {
		location.href = global["SELECTERESOURCE_URL"] + "&serviceid=" + $.query.get("serviceid") + "&resourceid=" + $.query.get("resourceid");
	});
	$(".btnRereshVerifyCode").click(function () {
		refreshVerifyCode();
	});
	$(".btnNext").click(function () {
		if (appointment.common.getValidation()) {
			var verifycode = $.trim($(".txtVerifyCode").val());
			appointment.checkVerifyCode({ verifycode: verifycode }, function (r) {
				if (r.success) {
					var contact = [];
					contact.push({ name: "serviceid", value: $.query.get("serviceid") });
					contact.push({ name: "resourceid", value: $.query.get("resourceid") });
					contact.push({ name: "datetime", value: $.query.get("datetime") });
					contact.push({ name: "firstName", value: $.trim($("#firstName").html()) });
					contact.push({ name: "lastName", value: $.trim($("#lastName").html()) });
					contact.push({ name: "phoneNumber", value: $.trim($("#phoneNumber").html()) });
					contact.push({ name: "email", value: $.trim($("#email").html()) });
					contact.push({ name: "note", value: $.trim($("#note").html()) });
					contact.push({ name: "verifycode", value: verifycode });
					contact.push({ name: "isReminder", value: $("#IsReminder").is(":checked") ? "T" : "F" });
					var postUrl = "{0}".format(global["COMPLETE_URL"]);
					appointment.common.postUrl({
						inputs: contact,
						postUrl: postUrl
					});
				} else {
					new window.appointment.dialog({
						title: global["verifyCodeError_dialog_title"],
						content: global["verifyCodeError_dialog_content"],
						buttons: [{
							text: global.close,
							onclick: function () {
								this.destory();
							}
						}]
					});
				}
			});
		}
	});
});