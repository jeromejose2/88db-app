﻿$(function () {
	window.serviceSchedule = {};
	$(".customScrollBox ul.SelectorList li a").click(function () {
		//if (!$(this).parents(".SelectorList_container").find(":radio").attr("disabled")) {
		loadServiceDetail(this);
		$(this).parents(".SelectorList_container").find(":radio").attr("checked", "checked");

		//}
	});
	$(".customScrollBox :radio").click(function () {
		if (!$(this).attr("disabled")) {
			$(this).parents(".SelectorList_container").find("a").click();
		}
	});


	//load first server
	loadServiceDetail($(".customScrollBox ul.SelectorList li a:first")[0]);
	$(".customScrollBox :radio:first").parents(".SelectorList_container").find(":radio").attr("checked", "checked");
	function loadServiceDetail(obj) {
		var disabled = $(obj).attr("disabled") == "disabled";
		var serviceid = $(obj).attr("serviceid");

		$(".detail-title>label").hide();
		appointment.getServiceDetail({ serviceId: serviceid }, function (r) {
			if (!r.Name) {
				return;
			}
			$(".detail-title h2").html(r.Name + $(".detail-title").find(".service_notavailable")[0].outerHTML);

			if (appointment.common.toNumber(r.Cost) == 0) {
				$(".cost").css('visibility', 'hidden');
			} else {
				$(".cost").css('visibility', 'visible');
				$(".cost span").html(global["APP88_MONEY_FORMAT"].replace("%s", r.FormatCost));
			}
			if (appointment.common.toNumber(r.Duration) == 0) {
				$(".duration").hide();
			} else {
				$(".duration").show();
				var duration = r.Duration;
				var hour = Math.floor(duration / 60);
				var min = duration % 60;
				$(".duration span").html(global["DURATION_FORMAT"].replace("%d", hour).replace("%d", min));
			}
			$description = $(r.Description);
			$description.find("img").load(function () {
				appointment.resizeFrame();
			})
			$(".itemContent-des").empty().append($description);
			appointment.resizeFrame();

		});

		//set css class
		$(".customScrollBox ul.SelectorList li a").removeClass("current-category").removeClass("On");
		$(obj).addClass("current-category").addClass("On");

		var serviceid = $(obj).attr("serviceid");
		var resouceIds = $(obj).attr("resouceids");


		if (disabled || typeof (serviceSchedule[serviceid]) != 'undefined' || $(obj).attr("status") == "I") {
			displayNextButtion(serviceSchedule[serviceid], serviceid);
			if (!serviceSchedule[serviceid]) {
				$(obj).attr("status", "I");
			}
		} else {
			//$(".loadingNextButton").show();
			new appointment.schedule({
				serviceId: serviceid,
				resourceId: resouceIds.split(","),
				serverDate: Date.parse(global["nowDateTime"]),  //當前系統時間	
				calendar: $("#datepicker"), //日曆jq對象
				autoBegin: true,
				calendarShow: function (calendar) { },
				onLoadComplete: function () {

				},
				onHasSchedule: function (has) {
					serviceSchedule[serviceid] = has;
					displayNextButtion(has, serviceid);
				}
			});
		}
		var status = $(obj).attr("status");
		if (status == 'A') {
			$(".btnGotoSelectResource").removeClass("itemBtn-red2_disable").unbind("click").click(function () {
				var selectedServiceId = $(".SelectorList li a.current-category").attr("serviceid");
				location.href = global["SELECTERESOURCE_URL"] + "&serviceid=" + selectedServiceId;
			});
		} else {
			$(".btnGotoSelectResource").addClass("itemBtn-red2_disable").unbind("click");
		}

	}

	function displayNextButtion(display, serviceid) {
		if (display) {
			$(".btnGotoSelectResource").show();
			$(".service_notavailable").hide();
		} else {
			$(".btnGotoSelectResource").hide();
			$(".service_notavailable").show();
		}
		//$(".loadingNextButton").hide();
	}
});