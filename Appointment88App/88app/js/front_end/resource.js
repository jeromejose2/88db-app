﻿$(function () {
	//var serverDate = Date.parse(global["nowDateTime"]);

	$(".customScrollBox ul.SelectorList li a").click(function () {
		loadResourceDetail(this);
		$(this).parents(".SelectorList_container").find(":radio").attr("checked", "checked");
	});
	$(".customScrollBox :radio").click(function () {
		$(this).parents(".SelectorList_container").find("a").click();
	});

	var serviceInfo = global["serviceInfo"];
	//load first server
	loadResourceDetail($(".customScrollBox ul.SelectorList li a:first")[0]);
	$(".customScrollBox :radio:first").parents(".SelectorList_container").find(":radio").attr("checked", "checked");

	var reousces = []
	$(".SelectorList li a[hasschema='true']").each(function () {
		reousces.push($(this).attr("resourceid"));
	});

	function loadResourceDetail(obj) {
		var resourceGuid = $(obj).attr("resourceid");
		var hasSchema = $(obj).attr("hasschema") == "true";
		window.appointment.getResourceDetail({ resourceGuid: resourceGuid, includedMoreInfo: true }, function (r) {
			if (!r.Name) {
				return;
			}
			$(".detail-title h2").html(r.Name);
			$(".itemContent-des").html(r.Description);
			appointment.resizeFrame();

			var moreInfo = r.MoreInfo;


			//hasSchema = hasSchema && checkHasSchema(moreInfo);
			if (hasSchema) {
				$(".btnGotoSelectSchedule").show().removeClass("itemBtn-red2_disable").unbind("click").click(function () {
					var selectedResourceId = $(".SelectorList li a.current-category").attr("resourceid");
					//判断schema是否可用
					checkResourceHasSchema(selectedResourceId, function (r) {
						if (r) {
							location.href = global["SELECTESCHEDULE_URL"] + "&serviceid=" + $.query.get("serviceid") + "&resourceid=" + selectedResourceId;
						} else {
							new window.appointment.dialog({
								title: global["text_not_available"],
								content: global["text_timeslot_is_full"],
								buttons: [{
									text: global["close"],
									onclick: function () {
										this.destory();
									}
								}]
							});
						}
					});
				});
			} else {
				$(".btnGotoSelectSchedule").hide().addClass("itemBtn-red2_disable").unbind("click");
			}
		});

		//set css class
		$(".customScrollBox ul.SelectorList li a").removeClass("current-category").removeClass("On");
		$(obj).addClass("current-category").addClass("On");
	}
	function checkResourceHasSchema(resourceGuid, callback) {
		new appointment.schedule({
			serviceId: $.query.get("serviceid"),
			resourceId: resourceGuid,
			serverDate: Date.parse(global["nowDateTime"]),  //當前系統時間	
			onCheckedHasSchema: function(hasSchema){
				callback(hasSchema);
			}
		});
	}
});

