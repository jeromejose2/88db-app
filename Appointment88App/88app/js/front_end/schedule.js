﻿$(function () {
	if ($.query.get("error") == 'timeslotfull') {
		new window.appointment.dialog({
			title: global["timelostfull_dialog_title"],
			content: global["timelostfull_dialog_content"],
			buttons: [{
				text: global.close,
				onclick: function () {
					this.destory();
				}
			}]
		});
	}

	new appointment.schedule({
		serviceId: $.query.get("serviceid"),
		resourceId: $.query.get("resourceid"),
		serverDate: Date.parse(global["nowDateTime"]),  //當前系統時間	
		calendar: $("#datepicker"), //日曆jq對象
		onLoadedService: function (r) { $(".appServiceName").html(r.Name); },
		onLoadedResource: function (r) { $(".appResourceName").html(r.Name); },
		onBeforeSelectDate: function(date){
			$(".detail-title h2").html(date.toString(global["dateFormat"]));		
		},
		onAfterSelectDate: function(date){
			$(".itemBtn-date").first().addClass("on");;
		},
		onBindTimeSlots: function(timeSlots){
			$(".AMTimeSlots, .PMTimeSlots").empty();
			if (timeSlots.AM.length == 0) {
				$(".AMTimeSlots").prev().hide();
			} else {
				$(".AMTimeSlots").prev().show();
				$.each(timeSlots.AM, function (i, t) {
					$(".AMTimeSlots").append('<a href="javascript:;" class="itemBtn-date" time="{0}">{0} {1}</a>'.format(t, $(".AMTimeSlots").prev().html()));
				});
			}
			if (timeSlots.PM.length == 0) {
				$(".PMTimeSlots").prev().hide();
			} else {
				$(".PMTimeSlots").prev().show();
				$.each(timeSlots.PM, function (i, t) {
					var ts = t.split(":");
					$(".PMTimeSlots").append('<a href="javascript:;" class="itemBtn-date" time="{2}">{0} {1}</a>'.format(t, $(".PMTimeSlots").prev().html(), (appointment.common.toNumber(ts[0]) >= 12 ? appointment.common.toNumber(ts[0]) : appointment.common.toNumber(ts[0]) + 12) + ":" + ts[1]));
				});
			}
			appointment.resizeFrame(false);
		},
		onLoadComplete: function(){
			this.begin();
		}
	});
	
	appointment.common.setDefaultValue();
	
	$(".gotoStep2").click(function () {
		location.href = global["SELECTERESOURCE_URL"] + "&serviceid=" + global.ServiceId;
	});

	$(".timeSlotContainer").on("click", ".itemBtn-date", function () {
		$(".itemBtn-date").removeClass("on");
		$(this).addClass("on");
	});

	$(".btnGotoContactInfo").click(function () {
		var selectedDateTime = $("#datepicker").datepicker("getDate");
		var selectedTimeSlot = $(".item_datetable .on").attr("time");
		if (selectedTimeSlot) {
			var times = selectedTimeSlot.split(":");
			selectedDateTime.set({ hour: appointment.common.toNumber(times[0]), minute: appointment.common.toNumber(times[1]) });
		} else {
			appointment.dialog.error(global["please_select_schedule"]);
			return;
		}
		var result = selectedDateTime.toString("MM/dd/yyyy-HH:mm");
		location.href = "{0}&serviceid={1}&resourceid={2}&datetime={3}".format(global["CONTACTINFO_URL"], $.query.get("serviceid"), $.query.get("resourceid"), result);
	});
});