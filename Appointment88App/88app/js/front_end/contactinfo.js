﻿$(function () {
	appointment.common.setDefaultValue();
	appointment.common.setValidation();
	//get service detail
	window.appointment.getServiceDetail({ serviceId: $.query.get("serviceid") }, function (r) {
		if (!r.Name) {
			return;
		}

		$(".appServiceName").html(r.Name);
	});

	//get resource detail
	window.appointment.getResourceDetail({ resourceGuid: $.query.get("resourceid") }, function (r) {
		if (!r.Name) {
			return;
		}
		var dealDateString = window.appointment.dateTimeFormat($.query.get("datetime").replace("-", " "));
		$(".appDealDate").html(r.Name + "<br/>"+ dealDateString);
	});

	$(".appBtnGotoSelectResource").click(function () {
		location.href = global["SELECTERESOURCE_URL"] + "&serviceid=" + $.query.get("serviceid");
	});


	

	$(".btnNext").click(function () {
		if (appointment.common.getValidation()) {
			if ($.trim($("#email").val()) != $.trim($("#emailVerify").val())) {
				appointment.dialog.error(global["resx_invalid_emailcompare"]);
			} else {
				//all valid pass
				var contact = [];
				contact.push({ name: "firstName", value: $.trim($("#firstName").val()) });
				contact.push({ name: "lastName", value: $.trim($("#lastName").val()) });
				contact.push({ name: "phoneNumber", value: $.trim($("#phoneNumber").val()) });
				contact.push({ name: "email", value: $.trim($("#email").val()) });
				contact.push({ name: "note", value: $.trim($("#note").getValue()) });
				var postUrl = "{0}&serviceid={1}&resourceid={2}&datetime={3}".format(global["CONFIRM_URL"], $.query.get("serviceid"), $.query.get("resourceid"), $.query.get("datetime"));
				appointment.common.postUrl({
					inputs: contact,
					postUrl: postUrl
				});
			}
		}
	});

	//disable copy cut paste event for email input
	$("#email, #emailVerify").bind("copy cut paste", function (e) {
		e.preventDefault();
	});
});