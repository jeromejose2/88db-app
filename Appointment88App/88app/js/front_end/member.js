﻿$(function () {
	$(".btnCancel").click(function () {
		openCancelConfrimDialog($(this), $(this).parents(".listcontent"), function (guid) {
			var inputs = [];
			inputs.push({ name: "action", value: "delete" });
			inputs.push({ name: "guid", value: guid });
			appointment.common.postUrl({
				inputs: inputs
			});
		});
	});

	$(".listcontent").each(function () {
		$(this).find(".cc02, .cc03").each(function () {
			var col = $(this);
			var startDateTime = col.html();
			var dealDateString = window.appointment.dateTimeFormat(startDateTime);
			col.html(dealDateString);
		});		
	});


	function openCancelConfrimDialog(btn, row, onOk) {
		var schedule = row.find(".cc02").html();
		var service = row.find(".cc01").html();
		var resource = row.find(".cc04").html();
		var guid = row.attr("guid");
		var content = '<p>' + global["cancel_appointment_to_sure"] + '</p> <div class="form_container_a_pop"><div class="form_title_pop">' + global["schedule"] + ':</div><div class="form_info_pop">' + schedule + '</div></div><div class="dbShop-ui-clearboth"></div><div class="form_container_b_pop"><div class="form_title_pop">' + global["service"] + ':</div><div class="form_info_pop">' + service + '</div></div>	<div class="dbShop-ui-clearboth"></div><div class="form_container_b_pop">		<div class="form_title_pop">' + global["resource"] + ':</div><div class="form_info_pop">' + resource + '</div></div>	<div class="dbShop-ui-clearboth"></div>';
		new window.appointment.dialog({
			title: global["cancel_appointment"],
			content: content,
			clickObject: btn,
			buttons: [{
				text: global["yes"],
				cls: "dbShop-ui-sendBtn",
				onclick: function () {
					if (typeof (onOk) != "undefined") {
						onOk.call(this, guid);
					}
				}
			},
				{
					text: global["no"],
					cls: "dbShop-ui-cancelBtn",
					onclick: function () {
						this.destory();
					}
				}]
		});
	}
});