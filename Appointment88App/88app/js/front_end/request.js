﻿$(function () {
	var dealDate = new Date(global["dealdate"]);
	var endDate = appointment.common.cloneDate(dealDate).addMinutes(global["serverduration"]);
	$(".schedule").html($(".schedule").html().format(
		window.appointment.dateTimeFormat(dealDate),
		window.appointment.dateTimeFormat(endDate)
	));
});