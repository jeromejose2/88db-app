﻿$(function () {
	if ($.query.get("error") == 'timeslotfull') {
		new window.appointment.dialog({
			title: global["timelostfull_dialog_title"],
			content: global["timelostfull_dialog_content"],
			buttons: [{
				text: global.close,
				onclick: function () {
					this.destory();
				}
			}]
		});
	}

	var resourceIds = []; //当前service 全部可用的resource id 集合
	var serviceId = $.query.get("serviceid");
	var allResourceTimeSlots; //全部resource 可用time slot 并集
	var currentSelectedTime = ""; //当前选择的时间

	$(".SelectorList li[hasschema='true']").each(function () {
		var rid = $(this).attr("resourceid");
		if (rid.length > 0) {
			resourceIds.push(rid);
		}
	});

	schedule = new appointment.schedule({
		serviceId: serviceId,
		resourceId: resourceIds,
		serverDate: Date.parse(global["nowDateTime"]),  //當前系統時間	
		calendar: $("#datepicker"), //日曆jq對象
		autoBegin: true,
		disableTodayHighlight: true,
		onLoadedService: function (r) { },
		onLoadedResource: function (r) { },
		onBeforeSelectDate: function (date) {

		},
		calendarShow: function (calendar) {
			calendar.fadeIn("fast", function(){
				$(".datetimeSum").html(calendar.datepicker('getDate').toString(global["dateFormat"]));
				appointment.resizeFrame(false);
			});
		}, 
		onAfterSelectDate: function (date) {

		},
		onBindTimeSlots: function (timeSlots) {
			datePickerOnSelect.call(this, timeSlots);
		},
		onLoadComplete: function () {	
				
		},
		onNoAnySchema: function(){
			alert("no any schma");
			//history.back(-1);
		}
	});

	$(".SelectorList li[hasschema='true'] .btnSelectReource").click(function () {
		var li = $(this).parents("li");
		if (li.find(":radio").attr("disabled") != "disabled") {
			if (!li.find("a.btnSelectReource").hasClass("On")) {
				select(null, li.attr("resourceid"));
			}
		}
	});

	$(".timeSlotContainer").on("click", ".itemBtn-date", function () {
		var selectedTime = $(this);
		if(selectedTime.hasClass("on")){
			currentSelectedTime = "";
			$(".resettime").css({"visibility":"hidden"});
		}else{
			currentSelectedTime = selectedTime.attr("time");
			$(".resettime").css({"visibility":""});
		}
		select(currentSelectedTime, null);		
	});

	$(".btnViewRerouceDetail").click(function () {
		var li = $(this).parents("li");
		var resourceGuid = li.attr("resourceid");
		var $btn = $(this);
		appointment.getResourceDetail({
			resourceGuid: resourceGuid,
			includedMoreInfo: true,
			cache: true
		}, function (resource) {
			if (typeof resource[0] == 'object')
			{
				new appointment.dialog.info(resource[0].Name, resource[0].Description, function () { }, $btn);
			}
			else
			{
				new appointment.dialog.info(resource.Name, resource.Description, function () { }, $btn);
			}
		});
	});

	$(".gotoStep2").click(function () {
		location.href = global["SELECTERESOURCE_URL"] + "&serviceid=" + global.ServiceId;
	});

	$(".resettime").click(function () {
		currentSelectedTime = "";
		$(this).css({"visibility":"hidden"});
		select(currentSelectedTime, null);
	});

	$(".btnGotoContactInfo").click(function () {
		var selectedDateTime = $("#datepicker").datepicker("getDate");
		var selectedTimeSlot = $(".item_datetable .on").attr("time");
		var resourceGuid = $(".SelectorList .On").parents("li").attr("resourceid");
		if (!resourceGuid) {
			//random resource
			var avaliableResout = $(".SelectorList li.Available");
			if (avaliableResout.length == 0) {
				appointment.dialog.error(global["no_available_resource"]);
				return;
			} else {
				var idx = Math.ceil(Math.random() * avaliableResout.length) - 1;
				resourceGuid = avaliableResout.eq(idx).attr("resourceid");
			}
		}
		if (selectedTimeSlot) {
			var times = selectedTimeSlot.split(":");
			selectedDateTime.set({ hour: appointment.common.toNumber(times[0]), minute: appointment.common.toNumber(times[1]) });
		} else {
			appointment.dialog.error(global["please_select_schedule"]);
			return;
		}
		var result = selectedDateTime.toString("MM/dd/yyyy-HH:mm");
		location.href = "{0}&serviceid={1}&resourceid={2}&datetime={3}".format(global["CONTACTINFO_URL"], serviceId, resourceGuid, result);
	});

	//设置选择timeslot 和 resource

	function select(time, resourceId) {
		if (time == null && resourceId == null) {
			resetSelector(); //重置全部
		} else if (time == null) {			
			var selectedResourceId = $(".SelectorList .On").parents("li").attr("resourceid");
			if (resourceId.length == 0) {
				selectResource();
				bindTimeslotByResourceId();
			} else if (resourceId == selectedResourceId) {
				//重置当前的resource timeslot
				//bindTimeSlot(schedule.ResoucesTimeSlot[resourceId]);
			} else {
				//切换resource timeslot
				selectResource($(".SelectorList li[resourceid='" + resourceId + "']"));
				bindTimeslotByResourceId(resourceId);
			}
			selectTimeslot(currentSelectedTime);
		} else if (resourceId == null) {
			selectTimeslot(time);
			if(time.length > 0){
				var hour = parseInt(time.substr(0, 2));
				var isAM = hour < 12;
				//转换为12小时制
				if(!isAM){
					if(hour > 12){
						hour = hour - 12;
						time = hour + time.substr(2);
						if(time.length < 5){
							time = "0"+time;
						}
					}				
				}
				var hasTimeSlotsResourceGuids = [];
				$.each(schedule.ResoucesTimeSlot, function (resurceGuid, ts) {
					var timeSlots = ts[isAM ? 'AM' : 'PM'];
					if ($.inArray(time, timeSlots) >= 0) {
						hasTimeSlotsResourceGuids.push(resurceGuid);
					}
				});
				$(".SelectorList li").each(function () {
					var rGuid = $(this).attr("resourceid");
					if (rGuid.length > 0) {
						if ($.inArray(rGuid, hasTimeSlotsResourceGuids) == -1) {
							enableResourceSelecter(rGuid, false);
						} else {
							enableResourceSelecter(rGuid, true);
						}
					}
				});
			}else{
				enableResourceSelecter("all");
			}
		}
	}
	//window.s = select;

	//通过resourceid 绑定中间的timeslot
	function bindTimeslotByResourceId(resourceId) {
		if (resourceId && resourceId.length > 0) {
			var timeslots = schedule.ResoucesTimeSlot[resourceId];
			bindTimeSlot(timeslots);
		} else {
			bindTimeSlot(allResourceTimeSlots);
		}
	}

	//设置选择中间的time slot
	function selectTimeslot(time) {
		$(".itemBtn-date").removeClass("on").find(".btnTick").hide();
		if (time && time.length > 0) {
			$(".itemBtn-date[time='" + time + "']").addClass("on").find(".btnTick").show();
		}
	}

	//设置选择右边li resource
	//li 为空，则选择第一个
	function selectResource(li) {
		$(".SelectorList :radio").removeAttr("checked");
		$(".SelectorList a.btnSelectReource").removeClass("On");
		if (li) {
			li.find("a.btnSelectReource").addClass("On");
			li.find(":radio").attr("checked", "checked");
		} else {
			$("a.btnSelectReource:first").addClass("On");
			$(":radio:first").attr("checked", "checked");
		}
	}

	//设置右边resoruce 是否可用
	function enableResourceSelecter(resurceGuid, enable) {
		if (resurceGuid.length == 0) {
			return;
		}
		var li;
		if(resurceGuid == "all"){
			$(".SelectorList li[hasschema='true']")
				.filter(function(){
					return $(this).attr('resourceid');
				}).each(function(){
					var rid = $(this).attr("resourceid");
					var ts = schedule.ResoucesTimeSlot[rid];
					if(ts.AM.length == 0 && ts.PM.length == 0){
						$(this).removeClass("Available");
						$(this).find(":radio").attr("disabled", "disabled");
						$(this).find("a.btnSelectReource").addClass("Off");
					}else{
						$(this).addClass("Available");
						$(this).find(":radio").removeAttr("disabled");
						$(this).find("a.btnSelectReource").removeClass("Off");
					}
				});	

			return;
		}
		li = $(".SelectorList li[resourceid='" + resurceGuid + "']");
		if (enable) {
			li.addClass("Available");
			li.find(":radio").removeAttr("disabled");
			li.find("a.btnSelectReource").removeClass("Off");
		} else {
			li.removeClass("Available");
			li.find(":radio").attr("disabled", "disabled");
			li.find("a.btnSelectReource").addClass("Off");
		}
	}

	//重置全部选择
	function resetSelector() {
		selectResource();
		selectTimeslot();
		bindTimeslotByResourceId();
		enableResourceSelecter("all", true);
	}

	//日历单击事件
	function datePickerOnSelect(timeSlots) {
		var date = $("#datepicker").datepicker("getDate");
		currentSelectedTime = "";
		$(".datetimeSum").html(date.toString(global["dateFormat"]));
		$.each(schedule.ResoucesTimeSlot, function (rGuid, ts) {
			if (rGuid.length > 0) {
				if (ts.AM.length == 0 && ts.PM.length == 0) {
					enableResourceSelecter(rGuid, false);
				} else {
					enableResourceSelecter(rGuid, true);
				}
			}
		});
		allResourceTimeSlots = timeSlots;
		resetSelector();
	}

	//输出timeslot
	function bindTimeSlot(timeSlots) {
		if(!timeSlots){
			return;
		}
		$(".AMTimeSlots, .PMTimeSlots").empty();
		if (timeSlots.AM.length == 0) {
			$(".AMTimeSlots").prev().hide();
		} else {
			$(".AMTimeSlots").prev().show();
			$.each(timeSlots.AM, function (i, t) {
				$(".AMTimeSlots").append('<a href="javascript:;" class="itemBtn-date" text="{0}" time="{0}">{0} {1}<span class="itemBtn-dateselect ico_white_select btnTick" style="display:none"></span></a>'.format(t, ''));
			});
		}
		if (timeSlots.PM.length == 0) {
			$(".PMTimeSlots").prev().hide();
		} else {
			$(".PMTimeSlots").prev().show();
			$.each(timeSlots.PM, function (i, t) {
				var ts = t.split(":");
				$(".PMTimeSlots").append('<a href="javascript:;" class="itemBtn-date" text="{0}" time="{2}">{0} {1}<span class="itemBtn-dateselect ico_white_select btnTick" style="display:none"></span></a>'.format(t, '', (appointment.common.toNumber(ts[0]) >= 12 ? appointment.common.toNumber(ts[0]) : appointment.common.toNumber(ts[0]) + 12) + ":" + ts[1]));
			});
		}
		

		$(".timeSlotLoading").hide();
		if (timeSlots.PM.length == 0 && timeSlots.AM.length == 0) {
			$(".noTimeSlot").show();
		} else {
			$(".noTimeSlot").hide();
			$(".timeSlotContainer").show();
		}
		appointment.resizeFrame(false);
	}



});