/*
 * jQuery miniColors: A small color selector
 *
 * Copyright 2011 Cory LaViska for A Beautiful Site, LLC. (http://abeautifulsite.net/)
 *
 * Dual licensed under the MIT or GPL Version 2 licenses
 *
 *
 * Usage:
 *
 *	1. Link to jQuery: <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>
 *
 *  2. Link to miniColors: <script type="text/javascript" src="jquery.miniColors.js"></script>
 *
 *  3. Include miniColors stylesheet: <link type="text/css" rel="stylesheet" href="jquery.miniColors.css" />
 *
 *	4. Apply $([selector]).miniColors() to one or more INPUT elements
 *
 *
 * Options:
 *
 *	disabled		[true|false]
 *	readonly		[true|false]
 *
 *
 *  Specify options on creation:
 *
 *		$([selector]).miniColors({
 *
 *			optionName: value,
 *			optionName: value,
 *			...
 *
 *		});
 *
 *
 * Methods:
 *
 *	Call a method using: $([selector]).miniColors('methodName', [value]);
 *
 *	disabled		[true|false]
 *	readonly		[true|false]
 *	value			[hex value]
 *	destroy
 *
 *
 * Events:
 *
 *	Attach events on creation:
 *
 *		$([selector]).miniColors({
 *
 *			change: function(hex, rgb) { ... }
 *
 *		});
 *
 *	change(hex, rgb)	called when the color value changes; 'this' will refer to the original input element;
 *                      hex is the string hex value of the selected color; rgb is an object with the RGB values
 *
 *
 * Change log:
 *
 *	- v0.1 (2011-02-24) - Initial release
 *
 *
 * Attribution:
 *
 *	- The color picker icon is based on an icon from the amazing Fugue icon set: 
 *    http://p.yusukekamiyamane.com/
 *
 *	- The gradient image, the hue image, and the math functions are courtesy of 
 *    the eyecon.co jQuery color picker: http://www.eyecon.ro/colorpicker/
 *
 *
*/
if(jQuery) (function($) {
	var themeColorsSet = [
		['#4a4a4a','#eeece1','#0094dd','#dd2d7a','#dd6b00','#532098','#c0504d','#9bbb59']
	];
	var showThemeColor = false;
	if(window.jsConfig && window.jsConfig.themeColors){
		if (jsConfig.showThemeColorBar && jsConfig.themeColors.length > 0) {
			themeColorsSet = jsConfig.themeColors;
			showThemeColor = true;
		}
	}
	
	var colorsSet = [
		['#000000','#993300','#333300','#003300','#003366','#000080','#333399','#333333'],
		['#800000','#FF6600','#808000','#008000','#008080','#0000FF','#666699','#808080'],
		['#FF0000','#FF9900','#99CC00','#339966','#33CCCC','#3366FF','#800080','#999999'],
		['#FF00FF','#FFCC00','#FFFF00','#00FF00','#00FFFF','#00CCFF','#993366','#C0C0C0'],
		['#FF99CC','#FFCC99','#FFFF99','#CCFFCC','#CCFFFF','#99CCFF','#CC99FF','#FFFFFF']
	];
	
	
	var getColorPositionFromHSB = function(hsb) {
		
		var x = Math.ceil(hsb.s / .67);
		if( x < 0 ) x = 0;
		if( x > 150 ) x = 150;
		
		var y = 150 - Math.ceil(hsb.b / .67);
		if( y < 0 ) y = 0;
		if( y > 150 ) y = 150;
		
		return { x: x - 5, y: y - 5 };
		
	};
	
	var getHuePositionFromHSB = function(hsb) {
		
		var y = 150 - (hsb.h / 2.4);
		if( y < 0 ) h = 0;
		if( y > 150 ) h = 150;				
		
		return { y: y - 1 };
		
	};
	
	var cleanHex = function(hex) {
		
		//
		// Turns a dirty hex string into clean, 6-character hex color
		//
		
		hex = hex.replace(/[^A-Fa-f0-9]/, '');
		
		if( hex.length == 3 ) {
			hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
		}
		
		return hex.length === 6 ? hex : null;
		
	};			
	
	var hsb2rgb = function(hsb) {
		var rgb = {};
		var h = Math.round(hsb.h);
		var s = Math.round(hsb.s*255/100);
		var v = Math.round(hsb.b*255/100);
		if(s == 0) {
			rgb.r = rgb.g = rgb.b = v;
		} else {
			var t1 = v;
			var t2 = (255 - s) * v / 255;
			var t3 = (t1 - t2) * (h % 60) / 60;
			if( h == 360 ) h = 0;
			if( h < 60 ) { rgb.r = t1; rgb.b = t2; rgb.g = t2 + t3; }
			else if( h<120 ) {rgb.g = t1; rgb.b = t2; rgb.r = t1 - t3; }
			else if( h<180 ) {rgb.g = t1; rgb.r = t2; rgb.b = t2 + t3; }
			else if( h<240 ) {rgb.b = t1; rgb.r = t2; rgb.g = t1 - t3; }
			else if( h<300 ) {rgb.b = t1; rgb.g = t2; rgb.r = t2 + t3; }
			else if( h<360 ) {rgb.r = t1; rgb.g = t2; rgb.b = t1 - t3; }
			else { rgb.r = 0; rgb.g = 0; rgb.b = 0; }
		}
		return {
			r: Math.round(rgb.r),
			g: Math.round(rgb.g),
			b: Math.round(rgb.b)
		};
	};
	
	var rgb2hex = function(rgb) {
		
		var hex = [
			rgb.r.toString(16),
			rgb.g.toString(16),
			rgb.b.toString(16)
		];
		$.each(hex, function(nr, val) {
			if (val.length == 1) hex[nr] = '0' + val;
		});
		
		return hex.join('');
	};
	
	var hex2rgb = function(hex) {
		var hex = parseInt(((hex.indexOf('#') > -1) ? hex.substring(1) : hex), 16);
		
		return {
			r: hex >> 16,
			g: (hex & 0x00FF00) >> 8,
			b: (hex & 0x0000FF)
		};
	};
	
	var rgb2hsb = function(rgb) {
		var hsb = { h: 0, s: 0, b: 0 };
		var min = Math.min(rgb.r, rgb.g, rgb.b);
		var max = Math.max(rgb.r, rgb.g, rgb.b);
		var delta = max - min;
		hsb.b = max;
		hsb.s = max != 0 ? 255 * delta / max : 0;
		if( hsb.s != 0 ) {
			if( rgb.r == max ) {
				hsb.h = (rgb.g - rgb.b) / delta;
			} else if( rgb.g == max ) {
				hsb.h = 2 + (rgb.b - rgb.r) / delta;
			} else {
				hsb.h = 4 + (rgb.r - rgb.g) / delta;
			}
		} else {
			hsb.h = -1;
		}
		hsb.h *= 60;
		if( hsb.h < 0 ) {
			hsb.h += 360;
		}
		hsb.s *= 100/255;
		hsb.b *= 100/255;
		return hsb;
	};
	
	var hex2hsb = function(hex) {
		var hsb = rgb2hsb(hex2rgb(hex));
		// Zero out hue marker for black, white, and grays (saturation === 0)
		if( hsb.s === 0 ) hsb.h = 360;
		return hsb;
	};
	
	var hsb2hex = function(hsb) {
		return rgb2hex(hsb2rgb(hsb));
	};
	
	
	
	function miniPicker(options){
		this.hsb = {
			h: 0,
			s: 0,
			b: 100
		};
		this.hasConfirmPanel = false;
		this.parent = $("body");
		this.isShowTransparent = false;
		this.isTransparent = false;
		//TODO load config form jsConfig
		this.showThemeColors = showThemeColor;
		
		if (options) {
			var mpOption = ['hasConfirmPanel', 'hex', 'parent','showThemeColors','isShowTransparent'];
			for (var i in mpOption) {
				if (typeof options[mpOption[i]] !== 'undefined') {
					switch (mpOption[i]) {
						case 'hex':
							this.hsb = hex2hsb(options[mpOption[i]]);
							break;
						default:
							this[mpOption[i]] = options[mpOption[i]];
					}
				}
			}
		}
		
		//this.init();
	}
	
	miniPicker.prototype.init = function (){
		var that = this;
		if(this.selector){
			this.destroy();
		}
		var selector = this.selector = $('<div class="miniColors-selector"></div>');
		this.themeSelector = $('<div class="miniColors-theme-selector"></div>').appendTo(this.selector);
		
		var maxLenThemeColor = 0;
		for(var i in themeColorsSet){
			var speedSelectorLine = $('<div class="miniColors-speed-theme-line"></div>').appendTo(this.themeSelector);
			for(var j in themeColorsSet[i]){
				if(themeColorsSet[i][j] === '' || themeColorsSet[i][j] === null){
					continue;
				}
				var colorBttn = $('<div class="miniColors-theme-colorBttn"></div>').appendTo(speedSelectorLine);
				var colorCell = $('<div class="miniColors-speed-cell" style="background-color:'+themeColorsSet[i][j]+'">&nbsp;</div>').appendTo(colorBttn);
				colorCell.data('color',themeColorsSet[i][j]);
			}
			if(this.isShowTransparent){
				var colorBttn = $('<div class="miniColors-theme-colorBttn"></div>').appendTo(speedSelectorLine);
				var colorCell = $('<div class="miniColors-speed-cell miniColors-speed-trans" style="background-color:#fff;border-color:#cca">&nbsp;</div>').appendTo(colorBttn);
				colorCell.data('color',"transparent");
				maxLenThemeColor = Math.max(maxLenThemeColor, themeColorsSet[i].length+1);
			}else{
				maxLenThemeColor = Math.max(maxLenThemeColor, themeColorsSet[i].length);
			}
		}
		
		
		var advancedBttn = $('<div class="miniColors-selector-moreBttn"></div>').appendTo(this.selector);
		var customerColorCell = $('<div class="miniColors-selector-more-colorBox" style="background-color:#' + hsb2hex(this.hsb) + '"></div>').appendTo(advancedBttn);
		$('<div class="miniColors-selector-moreBttn-arrow"></div>').appendTo(advancedBttn);
		
		
		
		var advancedPanel = $('<div class="miniColors-selector-advancedPanel"></div>').appendTo(this.selector);
		var speedSelector = $('<div class="miniColors-speed"></div>').appendTo(advancedPanel);
		advancedPanel.append('<div class="miniColors-colors" style="background-color: #FFF;"><div class="miniColors-colorPicker"></div></div>');
		advancedPanel.append('<div class="miniColors-hues"><div class="miniColors-huePicker"></div></div>');
		
		for(var i in colorsSet){
			var speedSelectorLine = $('<div class="miniColors-speed-line"></div>').appendTo(speedSelector);
			for(var j in colorsSet[i]){
				var colorCell = $('<div class="miniColors-speed-cell" style="background-color:'+colorsSet[i][j]+'">&nbsp;</div>').appendTo(speedSelectorLine);
				colorCell.data('color',colorsSet[i][j]);
			}
		}
		
		if (this.hasConfirmPanel) {
			var footerBar = $('<div class="miniColors-footer"></div>').appendTo(advancedPanel);
			var valBoxWrapper = $('<div class="miniColors-footer-valBoxWrapper">#</div>').appendTo(footerBar)
			var valBox = $('<input class="miniColors-footer-valBox" maxlength="7">').appendTo(valBoxWrapper);
			var buttonSet = $('<div class="miniColors-footer-buttons"></div>').appendTo(footerBar);
			
			$('<a class="msBtn dbShop-ui-tickBlk" href="#"></a>').click(function(event){
				event.preventDefault();
				that.selector.trigger("mpChange",['#' + hsb2hex(that.hsb), hsb2rgb(that.hsb)]);
			}).appendTo(buttonSet);
			
			$('<a class="msBtn dbShop-ui-crossBlk" href="#"></a>').click(function(event){
				event.preventDefault();
				that.selector.trigger("mpCancel");
			}).appendTo(buttonSet);
			
			this.valBox = valBox;
			
			this.valBox.val(hsb2hex(that.hsb));
			
			this.valBox.mousedown(function(event){
				event.stopPropagation();
			})
			
			this.selector.bind("mpRealTimeChange",function(){
				that.valBox.val(hsb2hex(that.hsb));
			});
			
			// Update when color is typed in
			this.valBox.bind('keyup', function(event) {
				// Remove non-hex characters
				var filteredHex = that.valBox.val().replace(/[^A-F0-9]/ig, '');
				if(filteredHex.length > 6){
					filteredHex = filteredHex.substring(0,6);
				}
				that.valBox.val(filteredHex);
				//var hexVal = filteredHex.replace(/[#]/g, '');
				that.setColor(hex2hsb(filteredHex), false);
			});
			
			// Handle pasting
			this.valBox.bind('paste.miniColors', function(event) {
				// Short pause to wait for paste to complete
				setTimeout( function() {
					that.valBox.trigger('keyup');
				}, 5);
			});
			
			
		}
		
		this.huePicker = this.selector.find('.miniColors-huePicker');
		this.colorPicker = this.selector.find('.miniColors-colorPicker');
		this.mousebutton = 0;
		this.advancedPanel = advancedPanel;
		this.customerColorCell = customerColorCell;
		this.advancedBttn = advancedBttn;
		
		this.parent.append(this.selector);
		
		// 18: color cell width, 33 width of advancedBttn, 8 : total horizontal padding of the Theme Color Panel
		this.selector.css("width", (8+(maxLenThemeColor)*18+33)+"px");
		
		var stopEvt = function(event){
			event.stopPropagation();
		};
		advancedBttn.click(function(event){
			stopEvt(event);
			that.showAdvanced();
		});
		
		this.selector.bind('selectstart', function() { return false; });
		
		this.selector.bind('mousedown.miniColors', function(event) {
			that.mousebutton = 1;
			event.preventDefault();
			event.stopPropagation();
			if( $(event.target).parents().andSelf().hasClass('miniColors-colors') ) {
				event.preventDefault();
				that.moving = 'colors';
				that.moveColor(event);
			}
			
			if( $(event.target).parents().andSelf().hasClass('miniColors-hues') ) {
				event.preventDefault();
				that.moving ='hues';
				that.moveHue(event);
			}
			
			if( $(event.target).parents().andSelf().hasClass('miniColors-selector') ) {
				event.preventDefault();
				return;
			}
			
			if( $(event.target).parents().andSelf().hasClass('miniColors') ) return;
			
		});
		
		$(document).bind('mouseup.miniColors', function(event) {
			that.mousebutton = 0;
			that.moving = null;
		});
		
		this.selector.bind('mousemove.miniColors', function(event) {
			if( that.mousebutton === 1 ) {
				if( that.moving === 'colors' ) that.moveColor(event);
				if( that.moving === 'hues' ) that.moveHue(event);
			}
		});
		
		this.selector.click(function(event){
			var target = $(event.target);
			
			if(target.hasClass("miniColors-speed-cell")){
				var rClr = target.data("color");
				var color;
				if(rClr=="transparent"){
					color = "transparent";
				} else {
					color = hex2hsb(rClr);
				}
				if (that.advancedPanel.is(":visible")) {
					that.setColor(color);
				}else{
					that.setColor(color, true, true);
				}
				
			}
		});
		
		this.selector.bind('mpRealTimeChange',function(){
			customerColorCell.css("background-color","#"+hsb2hex(that.hsb));
		});
		
		if(!this.showThemeColors){
			this.themeSelector.css("display", "none");
			this.advancedBttn.css("display", "none");
			this.selector.addClass("miniColors-selector-notheme");
			this.showAdvanced();
		}
	}
	
	miniPicker.prototype.moveColor = function(event) {
		
		this.colorPicker.hide();
		
		var position = {
			x: event.clientX - this.selector.find('.miniColors-colors').offset().left + $(document).scrollLeft() - 5,
			y: event.clientY - this.selector.find('.miniColors-colors').offset().top + $(document).scrollTop() - 5
		};
		
		if( position.x <= -5 ) position.x = -5;
		if( position.x >= 144 ) position.x = 144;
		if( position.y <= -5 ) position.y = -5;
		if( position.y >= 144 ) position.y = 144;
		
		this.colorPosition = position;
		
		this.colorPicker.css('left', position.x).css('top', position.y).show();
		
		// Calculate saturation
		var s = Math.round((position.x + 5) * .67);
		if( s < 0 ) s = 0;
		if( s > 100 ) s = 100;
		
		// Calculate brightness
		var b = 100 - Math.round((position.y + 5) * .67);
		if( b < 0 ) b = 0;
		if( b > 100 ) b = 100;
		
		// Update HSB values
		this.hsb.s = s;
		this.hsb.b = b;
		
		// Set color
		this.setColor(this.hsb);
		
	};
	
	miniPicker.prototype.moveHue = function(event) {
		
		this.huePicker.hide();
		
		var position = {
			y: event.clientY - this.selector.find('.miniColors-colors').offset().top + $(document).scrollTop() - 1
		};
		
		if( position.y <= -1 ) position.y = -1;
		if( position.y >= 149 ) position.y = 149;
		this.huePosition = position;
		this.huePicker.css('top', position.y).show();
		
		// Calculate hue
		var h = Math.round((150 - position.y - 1) * 2.4);
		if( h < 0 ) h = 0;
		if( h > 360 ) h = 360;
		
		// Update HSB values
		this.hsb.h = h;
		
		// Set color
		this.setColor(this.hsb);
		
	};
	
	miniPicker.prototype.setColor = function(hsb, isTrigger, isChange) {
		if (hsb == "transparent") {
			this.isTransparent = true;
		} else {
			this.isTransparent = false;
			this.hsb = hsb;
			var hex = hsb2hex(hsb);
			this.selector.find('.miniColors-colors').css('backgroundColor', '#' + hsb2hex({
				h: hsb.h,
				s: 100,
				b: 100
			}));
			
			// Set colorPicker position
			var colorPosition = getColorPositionFromHSB(hsb);
			this.colorPicker.css('top', colorPosition.y + 'px').css('left', colorPosition.x + 'px');
			
			// Set huePosition position
			var huePosition = getHuePositionFromHSB(hsb);
			this.huePicker.css('top', huePosition.y + 'px');
		}
		var hexColor = (this.isTransparent) ? "" : '#' + hsb2hex(hsb);
		var rgbColor = hsb2rgb(this.hsb);
		
		if (isTrigger !== false) {
			this.selector.trigger("mpRealTimeChange", [hexColor, rgbColor]);
			if (!this.hasConfirmPanel || isChange) 
				this.selector.trigger("mpChange", [hexColor, rgbColor]);
		}
	};
	
	miniPicker.prototype.change = function(handle){
		this.selector.bind("mpChange",handle);
	};
	
	miniPicker.prototype.realTimeChange = function(handle){
		this.selector.bind("mpRealTimeChange",handle);
	};
	
	miniPicker.prototype.cancel = function(handle){
		this.selector.bind("mpCancel",handle);
	}
	
	miniPicker.prototype.destroy = function(){
		this.selector.remove();
		$(document).unbind('mouseup.miniColors');
	};
	
	miniPicker.prototype.hideAdvanced = function(){
		if (this.showThemeColors) {
			this.advancedPanel.fadeOut(500);
			this.advancedBttn.removeClass("miniColors-selector-advancedOpen");
		}
	};
	miniPicker.prototype.showAdvanced = function(){
		this.advancedPanel.fadeIn(500);
		this.advancedBttn.addClass("miniColors-selector-advancedOpen");
		// Set background for colors
		var hsb = this.hsb;
		this.selector.find('.miniColors-colors').css('backgroundColor', '#' +
		hsb2hex({
			h: hsb.h,
			s: 100,
			b: 100
		}));
		
		// Set colorPicker position
		var colorPosition = getColorPositionFromHSB(hsb);
		this.selector.find('.miniColors-colorPicker').css('top', colorPosition.y + 'px').css('left', colorPosition.x + 'px');
		
		// Set huePicker position
		this.huePosition = getHuePositionFromHSB(hsb);
		this.selector.find('.miniColors-huePicker').css('top', this.huePosition.y + 'px');
		
		this.selector.trigger("mpShowAdvanced");
		
		
		var panelOffset = this.advancedPanel.offset();
		var maxWidth = this.parent.width() || $(window).width();
		var panelWidth = this.advancedPanel.outerWidth(false);
		if(panelOffset.left+ panelWidth > maxWidth){
			this.advancedPanel.css('left', maxWidth - (panelOffset.left+panelWidth));
		};
	};
	
	miniPicker.prototype.onShowAdvanced = function(handler){
		this.selector.bind("mpShowAdvanced", handler);
	}
	
	window.miniPicker = miniPicker;
	
	window.miniPicker.updateThemeColor = function(themeColors, cIndex){
		if (isNaN(cIndex)) {
			if (jsConfig.showThemeColorBar && themeColors.length > 0) {
				themeColorsSet = themeColors;
				showThemeColor = true;
			}
		}else{
			if(jQuery.isArray(themeColorsSet[0])){
				themeColorsSet[0][cIndex] = themeColors;
			}
		}
	};
	window.miniPicker.getThemeColor = function(){
		return themeColorsSet;
	};
	
	var showingPicker;
	$.extend($.fn, {
		
		miniColors: function(o, data) {
			
			var create = function(input, o, data) {
				
				//
				// Creates a new instance of the miniColors selector
				//
				
				// Determine initial color (defaults to white)
				var color = cleanHex(input.val());
				// Create trigger
				var trigger;
				if(o.triggerEle){
					trigger = o.triggerEle;
				}else{					
					trigger = $('<a class="miniColors-trigger" style="background-color: #' + color + '" href="javascript:void(0);"></a>');
					trigger.insertAfter(input);
				}
				// Add necessary attributes
				input.addClass('miniColors').attr('maxlength', 7).attr('autocomplete', 'off');
				
				// Set input data
				input.data('trigger', trigger);
				if( o.change ) input.data('change', o.change);
				
				var picker = new miniPicker({
					"showThemeColors": o.showThemeColors,
					"hasConfirmPanel": (typeof o.hasConfirmPanel !== 'undefined' && o.hasConfirmPanel == true),
					"isShowTransparent": (typeof o.isShowTransparent !== 'undefined' && o.isShowTransparent == true)
				});
				input.data('picker', picker);
				
				hide(input);
				
				// Handle options
				if( o.readonly ) input.attr('readonly', true);
				if( o.disabled ) disable(input);
				
				// Show selector when trigger is clicked
				trigger.bind('click.miniColors', function(event) {
					event.preventDefault();
					if (input.data('disabled') !== "true" && input.data('disabled') !== true) {
						input.trigger('focus');
					}
					
				});
				
				// Show selector when input receives focus
				input.bind('focus.miniColors', function(event) {
					_show(input);
				});
				
				// Hide on blur
				input.bind('blur.miniColors', function(event) {
					var hex = cleanHex(input.val());
					input.val( hex ? '#' + hex : '' );
					if (input.data('picker').mousebutton !== 1) {
						_hide(input);
					}
				});
				
				// Hide when tabbing out of the input
				input.bind('keydown.miniColors', function(event) {
					if( event.keyCode === 9 )_hide(input);
				});
				
				// Update when color is typed in
				input.bind('keyup.miniColors', function(event) {
					// Remove non-hex characters
					var filteredHex = input.val().replace(/[^A-F0-9#]/ig, '');
					input.val(filteredHex);
					if( !setColorFromInput(input) ) {
						if( input.data('change') ) {						
							input.data('change').call(input, '', '');
						}
						
						// Reset trigger color when color is invalid
						input.data('trigger').css('backgroundColor', '#FFF');
					}
				});
				
				// Handle pasting
				input.bind('paste.miniColors', function(event) {
					// Short pause to wait for paste to complete
					setTimeout( function() {
						input.trigger('keyup');
					}, 5);
				});
				
				
			};
			
			var _show = function(input){
				input.data('isReadyToShow',true);
				show(input);
			};
			var show = function(input){
				if (!input.data("isShown")) {
					if(showingPicker){
						hide(showingPicker);
					}
					showingPicker = input;
					
					var picker = input.data('picker');
					var trigger = input.data('trigger');
					var color = cleanHex(input.val());
					if (!color) 
						color = 'FFFFFF';
					
					picker.init();
					picker.setColor(hex2hsb(color), true, false);
					if (o.realTimeChangeTriggerEleColor) {
						picker.realTimeChange(function(event, hex){
							trigger.css('backgroundColor', hex);
						});
						picker.cancel(function(event){
							trigger.css('backgroundColor', input.val());
						});
					}
					picker.change(function(event, hex, rgb){
						input.val(hex);
						trigger.css("background-color", hex);
						if (o.change) 
							o.change.call(input, hex, rgb);
						if (o.hasConfirmPanel) {
							hide(input);
						}
					});
					
					if (o.hasConfirmPanel) {
						picker.cancel(function(event){
							hide(input);
						});
					}
					
					setColorFromInput(input);
					
					picker.selector.css({
						top: input.is(':visible') ? input.offset().top + input.outerHeight() : trigger.offset().top + trigger.outerHeight(),
						left: input.is(':visible') ? input.offset().left : trigger.offset().left,
						display: 'none'
					}).addClass(input.attr('class'));
					
					picker.selector.fadeIn(300);
					
					var panelOffset = picker.selector.offset();
					var maxWidth = picker.parent.width() || $(window).width();
					var panelWidth = picker.selector.outerWidth(false);
					if(panelOffset.left+ panelWidth > maxWidth){
						picker.selector.css('left', maxWidth - panelWidth);
					};
					
					$(document).bind('mousedown.miniColors', function(event){
						if (o.realTimeChangeTriggerEleColor) {
							trigger.css('backgroundColor', input.val());
						}
						_hide(input);
					});
					
					input.data("isShown", true);
				}
			};
			
			var _hide = function(input){
				input.data('isReadyToShow',false);
				setTimeout(function(){
					if(input.data('isReadyToShow')===false){
						hide(input);
					}
				}, 200);
			};
			var hide = function(input){
				if (input.data("isShown")) {
					var picker = input.data('picker');
					
					if (picker.selector && picker.selector.is &&
					picker.selector.is(":visible")) 
						picker.selector.fadeOut(300, function(){
							picker.destroy();
						});
					$(document).unbind('mousedown.miniColors');
					
					input.data("isShown", false);
				}
			};
			
			var setColorFromInput = function(input){
				// Don't update if the hex color is invalid
				var hex = cleanHex(input.val());
				if( !hex ) return false;
				
				// Get HSB equivalent
				var hsb = hex2hsb(hex);
				
				var picker = input.data('picker');
				if (picker.selector) {
					input.data('picker').setColor(hsb, false);
				}
				
				input.data('trigger').css("background-color", "#"+hex);
				
				if (o.change) 
					o.change.call(input, "#"+hex, hex2rgb(hex));
				
				return true;
			};
			
			var destroy = function(input){
				input.data('trigger').remove();
				input.removeAttr('autocomplete');
				input.removeData('trigger');
				input.unbind('focus.miniColors');
				input.unbind('blur.miniColors');
				input.unbind('keyup.miniColors');
				input.unbind('keydown.miniColors');
				input.unbind('paste.miniColors');
				
				input.data('picker').destroy();
			};
			
			var enable = function(input){
				//
				// Disables the input control and the selector
				//
				
				input.attr('disabled', false);
				input.data('trigger').css('opacity', 1);
				input.data('disabled', false);
			};
			
			var disable = function(input){
			
				//
				// Disables the input control and the selector
				//
				
				hide(input);
				input.attr('disabled', true);
				input.data('trigger').css('opacity', .5);
				input.data('disabled', true);
			};
			
			switch(o) {
			
				case 'readonly':
					
					$(this).each( function() {
						$(this).attr('readonly', data);
					});
					
					return $(this);
					
					break;
				
				case 'disabled':
					
					$(this).each( function() {
						if( data ) {
							disable($(this));
						} else {
							enable($(this));
						}
					});
										
					return this;
			
				case 'value':
					
					$(this).each( function() {
						$(this).val(data).trigger('keyup');
					});
					
					return $(this);
					
					break;
				
				case 'show':
					
					$(this).each( function() {
						show($(this));
					});
					
					break;
				
				case 'hide':
					
					$(this).each( function() {
						hide($(this));
					});
					
					break;
				
				case 'destroy':
					
					$(this).each( function() {
						destroy($(this));
					});
										
					return $(this);
				
				default:
					
					if( !o ) o = {};
					
					$(this).each( function() {
						
						// Must be called on an input element
						if( $(this)[0].tagName.toLowerCase() !== 'input' ) return;
						
						// If a trigger is present, the control was already created
						if( $(this).data('trigger') ) return;
						
						// Create the control
						create($(this), o, data);
						
					});
										
					return $(this);
					
			}
		}
	});
	
})(jQuery);



