﻿String.prototype.format = function() {
	var args = arguments;
	return this.replace(/\{(\d+)\}/g,
	function(m, i) {
		return args[i];
	});
}
Date.prototype.addMilliseconds = function (value) {
	var millisecond = this.getMilliseconds();
	this.setMilliseconds(millisecond + value);
	return this;
};
String.prototype.htmlEncode = function () {
	return $('<div/>').text(this).html();
}

//數組去除重複
Array.prototype.distinct = function () {
	var newArr = [], obj = {};
	for (var i = 0, len = this.length; i < len; i++) {
		if (!obj[this[i]]) {
			newArr.push(this[i]);
			obj[this[i]] = 'new';
		}
	}
	return newArr;
};

window.appointment = {
	common: {
		getDateFormateForDatepicker: function () {
			if(global["dateFormat"].indexOf("MMM") == -1){
				return global["dateFormat"].replace("yyyy", "yy").replace("MM", "M").toLowerCase();
			}else{
				return global["dateFormat"].replace("yyyy", "yy").replace("MMM", "M");
			}
		},
		htmlDecode: function (v) {
			return $('<div/>').html(v).text();
		},
		getRandom: function () {
			var d = new Date();
			return (d.getFullYear() + "" + d.getMonth() + d.getDate() + d.getHours() + d.getMinutes() + d.getSeconds() + d.getMilliseconds());
		},
		getCookie: function (name) {
			var arr = document.cookie.match(new RegExp("(^| )" + name + "=([^;]*)(;|$)"));
			if (arr != null) return unescape(arr[2]);
			return null;
		},
		dayDiff: function (date1, date2) {
			var idays = parseInt(Math.abs(date1 - date2) / 1000 / 60 / 60 / 24);
			if ((date1 - date2) < 0) {
				return -idays;
			}
			return idays;
		},
		parseDay: function (day) {
			switch (day) {
				case 1:
					return "monday";
					break;
				case 2:
					return "tuesday";
					break;
				case 3:
					return "wednesday";
					break;
				case 4:
					return "thursday";
					break;
				case 5:
					return "friday";
					break;
				case 6:
					return "saturday";
					break;
				case 0:
					return "sunday";
					break;
				default:
					return "unknow";
					break;
			}
		},
		cloneDate: function (date) {
			return new Date(date.toString());
		},
		toNumber: function (num) {
			if ((num + "").length == 0) {
				return 0;
			}
			return appointment.common.isNumber(num) ? parseInt(num, 10) : 0;
		},
		isNumber: function (num) {
			return !isNaN(num);
		},
		isPrice: function (price) {
			if (!price.match(/^[\d]{0,}(\.{0,1})?([\d]{0,})?$/)) {
				return false;
			}
			return true;
		},
		isPhoneNumber: function (phone) {
			if (phone.match(/^[\d\+\-\(\) ]{0,}$/)) {
				return true;
			}
			return false;
		},
		isInt: function (number, min, max) {
			if (!number.match(/^[\d]{0,}$/)) {
				return false;
			}
			var num = appointment.common.toNumber(number);
			if (typeof (min) != undefined) {
				if (num < min) {
					return false;
				}
			}
			if (typeof (max) != undefined) {
				if (num > max) {
					return false;
				}
			}
			return true;
		},
		isEmail: function (email) {
			if (email.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1) {
				return true;
			} else {
				return false;
			}
		},
		fe_validation: function () {
			var result = {
				required: true,
				valid: true,
				pass: true
			};
			$(".common-validatebox").each(function () {
				var dataOptions = $(this).attr("data-options");
				var value = $.trim($(this).val());
				if (value.length > 0) {
					var placeholder = $.trim($(this).attr("placeholder"));
					if (value == placeholder) {
						value = "";
					}
				}
				if (dataOptions) {
					var options = eval("({" + dataOptions + "})");
					options.target = this;
					//console.log(options);
					$.extend(result, options);
					//驗證必填
					if (options.required && value.length == 0) {
						result.required = false;
						result.pass = false;
						return false;
					}

					//驗證正則
					if (result.required && options.validType) {
						result.valid = true;
						switch (options.validType) {
							case "phone":
								if (!appointment.common.isPhoneNumber(value)) {
									result.valid = false;
									result.pass = false;
								}
								break;
							case "email":
								if (!appointment.common.isEmail(value)) {
									result.valid = false;
									result.pass = false;
								}
								break;
								break;
						}
						if (!result.valid) {
							return false;
						}
					}
				}
			});
			return result;
		},
		//verifyList : 要驗證的jq對象集合，空則驗證所有包含common-validatebox class的對象
		getValidation: function (verifyList) {
			var invalid = [];
			if (typeof (verifyList) == "undefined") {
				verifyList = $(".common-validatebox");
			}
			verifyList.each(function () {
				var result = {
					required: true,
					valid: true,
					pass: true
				};
				var dataOptions = $(this).data("common-validatebox-options");
				var isText = $(this).is(":text") || this.tagName.toLowerCase() == "textarea";
				var value;
				if (isText) {
					value = $(this).getValue();
				} else {
					value = $(this).val();
				}
				if (dataOptions) {
					var options = dataOptions;
					options.target = this;
					//console.log(options);
					$.extend(result, options);
					//驗證必填
					if (options.required && (value == null || value.length == 0)) {
						result.required = false;
						result.pass = false;
						invalid.push(result);
						return true;
					}

					//驗證正則
					if (isText && value.length > 0 && options.validType) {
						result.valid = true;
						switch (options.validType) {
							case "number":
								if (!appointment.common.isInt(value, options.min, options.max)) {
									result.valid = false;
									result.pass = false;
								}
								break;
							case "price":
								if (!appointment.common.isPrice(value)) {
									result.valid = false;
									result.pass = false;
								}
								break;
							case "phone":
								if (!appointment.common.isPhoneNumber(value)) {
									result.valid = false;
									result.pass = false;
								}
								break;
							case "email":
								if (!appointment.common.isEmail(value)) {
									result.valid = false;
									result.pass = false;
								}
								break;
						}
						if (!result.valid) {
							invalid.push(result);
							return true;
						}
					}
				}
			});
			//console.log(invalid);
			$.each(invalid, function (i, m) {
				var id = $(m.target).attr("id");
				var tipsDiv = $(".commonTipsMsg_" + id);
				if (tipsDiv.length == 0) {
					tipsDiv = $('<div style="left:0; top0; display:block;z-index:999;" class="dbShop-ui-eMsg dbShop-ui-eMsgTRt"><div class="dbShop-ui-eMsgPtr"></div><div class="dbShop-ui-eMsgCnt common-msg"></div></div>');
					$("body").append(tipsDiv.addClass("commonTipsMsg_" + id));
				}
				if (!m.required) {
					tipsDiv.find(".common-msg").html(m.missingMessage);
				} else if (!m.valid) {
					tipsDiv.find(".common-msg").html(m.invalidMessage ? m.invalidMessage : m.missingMessage);
				}
				tipsDiv.css({
					top: $(m.target).offset().top + $(m.target).height() + 5,
					left: $(m.target).offset().left
				}).show();
			});
			return invalid.length == 0;
		},
		displayErrorMessage: function (obj, msg) {
			var id = $(obj).attr("id");
			var tipsDiv = $(".commonTipsMsg_" + id);
			if (tipsDiv.length == 0) {
				tipsDiv = $('<div style="left:0; top0; z-index:999; display:block;" class="dbShop-ui-eMsg dbShop-ui-eMsgTRt"><div class="dbShop-ui-eMsgPtr"></div><div class="dbShop-ui-eMsgCnt common-msg"></div></div>');
				$("body").append(tipsDiv.addClass("commonTipsMsg_" + id));
			}
			tipsDiv.find(".common-msg").html(msg);
			tipsDiv.css({
				top: $(obj).offset().top + $(obj).height() +5,
				left: $(obj).offset().left
			}).show();
		},
		hideErrorMessage: function (obj) {
			$(obj).hideErrorMessage();
		},
		setValidation: function () {
			$(".common-validatebox").each(function () {
				var dataOptions = $(this).attr("data-options");
				if (dataOptions) {
					var options = eval("({" + dataOptions + "})");
					$(this).setValidation(options);
				}
			});
		},
		postUrl: function (options) {
			var settings = {
				inputs: [],
				postUrl: location.href,
				formName: "form",
				target: ""
			};
			$.extend(settings, options);
			//var form1 = document.createElement(settings.formName);
			var form1 = $("<form>");
			form1.appendTo("body");
			//document.body.appendChild(form1);
			settings.inputs.push({
				name: window.appointment.ajax.csrf.name,
				value: window.appointment.ajax.csrf.value()
			});
			$.each(settings.inputs, function (i, m) {
				var input = document.createElement("input");
				input.type = "hidden";
				input.value = m.value;
				input.name = m.name;
				//form1.appendChild(input);
				form1.append($(input));
			});
			/*
			form1.method = "post";
			form1.target = settings.target;
			form1.action = $.trim(settings.postUrl) == "" ? location.href : settings.postUrl;
			form1.submit();
			*/
			form1.attr({
				method: "post",
				target: settings.target,
				action: $.trim(settings.postUrl) == "" ? location.href : settings.postUrl
			}).submit();
		},
		setDefaultValue: function () {
			$("input[type=text],textarea").setPlaceHolder();
		},
		newGuid: function () {
			var guid = "";
			for (var i = 1; i <= 32; i++) {
				var n = Math.floor(Math.random() * 16.0).toString(16);
				guid += n;
				if ((i == 8) || (i == 12) || (i == 16) || (i == 20))
					guid += "-";
			}
			return guid;
		},
		//以value1為基準，判斷是否和value2相等
		isEqual: function (value1, value2) {
			var equal = true;

			if ($.isArray(value1)) { //如果是數組
				//數組長度不對
				if (!$.isArray(value2) || value1.length != value2.length) {
					return false;
				}
				$.each(value1, function (i, ary) {
					if (!appointment.common.isEqual(ary, value2[i])) {
						equal = false;
						return false; //跳出each
					}
				});
			} else if ($.isPlainObject(value1)) {//如果是對象
				if (!$.isPlainObject(value2)) {
					return false;
				}
				$.each(value1, function (key, value) {
					if (typeof (value2[key]) == "undefined") {
						equal = false;
						return false; //跳出each
					}
					if (!appointment.common.isEqual(value, value2[key])) {
						equal = false;
						return false; //跳出each
					}
				});
			} else {
				if (value1 !== value2) {//否則默認為字符串或數字				
					return false;
				}
			}
			return equal;
		},
		//判斷timeRange的順序是否對（小的在前面)
		checkTimeRang: function (timeRange) {
			if (timeRange.length != 2) {
				return false;
			} else if (this.compareTime(timeRange[0], timeRange[1]) == 1) {
				return false;
			}
			return true;
		},
		//比較2個日期時間，時間格式 yyyy-MM-dd HH:mm:ss
		compareDateTime: function (dateTime1, dateTime2) {
			var dateTimes1 = dateTime1.split(" ");
			var dateTimes2 = dateTime2.split(" ");
			var compareDateResult = this.compareDate(dateTimes1[0], dateTimes2[0]);
			if (compareDateResult != 0) {
				return compareDateResult;
			} else {
				return this.compareTime(dateTimes1[1], dateTimes2[1]);
			}
		},
		//比較2個時間
		//比较时间, 1 大于， -1 小于, 0 等于
		compareTime: function (time1, time2) {
			/*
			if (time1 == time2) {
			return 0;
			}
			return Date.parse(time1) > Date.parse(time2) ? 1 : -1;
			*/
			var _time1 = time1.split(":");
			var _time2 = time2.split(":");
			if (_time1[0] == _time2[0] && _time1[1] == _time2[1]) {
				return 0;
			} else if (_time1[0] > _time2[0]) {
				return 1;
			} else if (_time1[0] == _time2[0] && _time1[1] > _time2[1]) {
				return 1;
			}
			return -1;
		},
		//比较日期, 1 大于， -1 小于, 0 等于
		compareDate: function (date1, date2) {
			var _date1 = date1.split("-");
			var _date2 = date2.split("-");
			if (_date1[0] == _date2[0] && _date1[1] == _date2[1] && _date1[2] == _date2[2]) {
				return 0;
			}

			if (_date1[0] > _date2[0]) {
				return 1;
			} else if (_date1[0] < _date2[0]) {
				return -1;
			}

			if (_date1[1] > _date2[1]) {
				return 1;
			} else if (_date1[1] < _date2[1]) {
				return -1;
			}

			if (_date1[2] > _date2[2]) {
				return 1;
			} else if (_date1[2] < _date2[2]) {
				return -1;
			}

			return -1;
		},
		//排序時間段
		sortTimeRange: function (timeRanges) {
			var temp;
			for (var i = 0; i < timeRanges.length - 1; i++) {
				for (var j = 0; j < timeRanges.length - i - 1; j++) {
					if (this.compareTime(timeRanges[j][0], timeRanges[j + 1][0]) == 1) {
						temp = timeRanges[j];
						timeRanges[j] = timeRanges[j + 1];
						timeRanges[j + 1] = temp;
					}
				}
			}
			return timeRanges;
		},
		//判斷2個時間段是否有重合
		isCrossTimeRange: function (timeRange1, timeRange2) {
			if (!this.checkTimeRang(timeRange1) || !this.checkTimeRang(timeRange2)) {
				return null;
			}
			if (timeRange1[0] == timeRange2[0] || timeRange1[1] == timeRange2[1]) {
				return true;
			}
			var sortTimes = [timeRange1, timeRange2];
			var early = sortTimes[0];
			var late = sortTimes[1];
			if (this.compareTime(early[1], late[0]) != -1 || this.compareTime(early[1], late[1]) != -1) {
				return true;
			}
			return false;
		},
		//合併2個時間段
		merge2TimeRange: function (timeRange1, timeRange2) {
			var sortTimes = [timeRange1, timeRange2];
			if (this.isCrossTimeRange(timeRange1, timeRange2)) {
				return [[
					this.compareTime(sortTimes[0][0], sortTimes[1][0]) == 1 ? sortTimes[1][0] : sortTimes[0][0],
					this.compareTime(sortTimes[0][1], sortTimes[1][1]) == 1 ? sortTimes[0][1] : sortTimes[1][1]
				]];
			} else {
				return sortTimes;
			}
		},
		//合併多個時間段
		mergeTimeRanges: function (timeRanges) {
			timeRanges = this.sortTimeRange(timeRanges);
			var result = [];
			for (var i = 0; i < timeRanges.length; i++) {
				if (i == 0) {
					result.push(timeRanges[i]);
					continue;
				}
				var hasMerge = false;
				$.each(result, function (j, n) {
					var r = appointment.common.merge2TimeRange(n, timeRanges[i]);
					if (r.length == 1) { //merge
						result[j] = r[0];
						hasMerge = true;
						return false;
					}
				});
				if (hasMerge == false) {
					result.push(timeRanges[i])
				}
			}
			return result;
		},
		//判斷某個時間在不在指定的時間段裏面
		isInTimeRange: function (timeRange, time) {
			var isIn = false;
			$.each(timeRange, function (i, tr) {
				if (appointment.common.compareTime(tr[0], time) != 1 && appointment.common.compareTime(tr[1], time) != -1) {
					isIn = true;
					return false;
				}
			});
			return isIn;
		},
		//獲取時間段的最小時間
		getMinTimeByRange: function (timeRanges) {
			if (timeRanges.length == 1) {
				return timeRanges[0][0];
			} else if (timeRanges.length == 0) {
				return null;
			}
			timeRanges = sortTimeRange(timeRanges);
			return timeRanges[0][0];
		}

	},
	localStorage: {
		data: {},
		get: function (key) {
			return this.data[key];
		},
		set: function (key, value) {
			this.data[key] = value;
		},
		clear: function (key) {
			if (typeof (key) == "undefined") {
				this.data = {};
			} else {
				this.data[key] = null;
			}
		}
	},
	ajax: {
		ajaxOptions: {
			ajaxUrl: {
				getServiceDetail: "/service/getServerDetail?cid=" + global.cid,
				getResourceDetail: "/resource/getResourceDetail?cid=" + global.cid,
				getResources: "/resource/getResources?cid=" + global.cid,
				getResourceServiceDateTime: "/resource/getResourceServiceDateTime?cid=" + global.cid,
				getAvailableTimeSlotByDate: "/service/getAvailableTimeSlotByDate?cid=" + global.cid,
				getAvailableTimeSlotByMonth: "/service/getAvailableTimeSlotByMonth?cid=" + global.cid,
				checkVerifyCode: "/appointment/checkVerfiyCode?cid=" + global.cid,
				logout: "/appointment/logout?cid=" + global.cid
			},
			type: "post",
			dataType: "json"
		},
		genPostData: function (params) {
			var p = {};
			p[this.csrf.name] = this.csrf.value();
			return $.extend(p, params);
		},
		post: function (url, params, fn) {
			var async = true;
			params = $.extend({ async: true }, params);
			if (typeof (fn) == "undefined") {
				params.async = false;
			}
			var result;
			$.ajax({
				url: url,
				type: this.ajaxOptions.type,
				async: params.async,
				dataType: this.ajaxOptions.dataType,
				data: this.genPostData(params),
				success: function (response) {
					if (params.async) {
						fn(response);
					} else {
						result = response;
					}
				},
				error: function (respnse) {

				}
			});
			return result;
		},
		csrf: {
			name: "csrf_test_name",
			value: function () { return appointment.common.getCookie('csrf_cookie_name'); }
		}
	},
	getServiceDetail: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			async = false;
		}
		var options = $.extend({
			serviceId: 0,
			cache: true
		},
		options);
		if (options.cache) {
			var cacheData = this.localStorage.get("getServiceDetail_" + options.serviceId);
			if (cacheData != null) {
				if (async) {
					fn(cacheData);
				}
				return cacheData;
			}
		}
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.getServiceDetail, {
			serviceId: options.serviceId
		},
		function (r) {
			me.localStorage.set("getServiceDetail_" + options.serviceId, r);
			fn(r);
		});
		this.localStorage.set("getServiceDetail_" + options.serviceId, result);
		return result;
	},
	getResourceDetail: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			async = false;
		}
		var options = $.extend({
			resourceGuid: 0,
			includedMoreInfo: false,
			ignoreServiceStatus: false,
			cache: true
		},
		options);
		var cacheKey = "getResourceDetail_" + options.resourceGuid + "_" + options.ignoreServiceStatus;
		if (options.cache) {
			var cacheData = this.localStorage.get(cacheKey);
			if (cacheData != null) {
				if (async) {
					fn(cacheData);
					return;
				}
				return cacheData;
			}
		}
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.getResourceDetail, {
			resourceGuid: options.resourceGuid,
			ignoreServiceStatus: options.ignoreServiceStatus,
			includedMoreInfo: options.includedMoreInfo
		},
		function (r) {
			me.localStorage.set(cacheKey, r);
			fn(r);
		});
		if (async == false) {
			this.localStorage.set(cacheKey, result);
		}
		return result;
	},
	getResources: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			fn = $.noop;
			async = false;
		}
		var options = $.extend({
			async: async,
			resourceGuids: [],
			includedMoreInfo: false
		},
		options);
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.getResources, options, function (r) {
			result = r;
			fn(r);
		});
		return result;
	},
	getResourceServiceDateTime: function (options, fn) {
		var options = $.extend({
			resourceGuid: 0
		},
		options);
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.getResourceServiceDateTime, {
			resourceGuid: options.resourceGuid
		},
		fn);
		return result;
	},
	getAvailableTimeSlotByDate: function (options, fn) {
		var options = $.extend({
			serviceid: 0,
			resourceid: 0,
			date: ""
		},
		options);
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.getAvailableTimeSlotByDate, {
			serviceid: options.serviceid,
			resourceid: options.resourceid,
			date: options.date
		},
		fn);
		return result;
	},
	getAvailableTimeSlotByMonth: function (options, fn) {
		var options = $.extend({
			serviceid: 0,
			resourceid: 0,
			date: ""
		},
		options);
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.getAvailableTimeSlotByMonth, {
			serviceid: options.serviceid,
			resourceid: options.resourceid,
			date: options.date
		},
		fn);
		return result; // eval("(" + result + ")");
	},
	checkVerifyCode: function (options, fn) {
		var options = $.extend({
			verifycode: ""
		},
		options);
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.checkVerifyCode, {
			verifycode: options.verifycode
		},
		fn);
		return result; // eval("(" + result + ")");
	},
	timeFormat: function (date, fullTimeFormat) {
		if (typeof (fullTimeFormat) == "undefined" && global && global.timeFormat) {
			fullTimeFormat = global.timeFormat;
		}
		var ampm = fullTimeFormat.match(/\((.*)\)/);
		var fullampm = ampm[0];
		var ampmValue = ampm[1].split("|");
		var timeFormat = fullTimeFormat.replace(fullampm, "");
		var hour = date.getHours();
		var result = fullTimeFormat.replace(timeFormat, date.toString(timeFormat)).replace(fullampm, hour < 12 ? ampmValue[0] : ampmValue[1]);
		/*
		//如果是00:** 點， 則轉換為12:**, 防止出現00:00AM，而是出現12:00AM
		if (result.substr(0, 2) == "00") {
		result = "12" + result.substr(2)
		}
		*/
		return result;
	},
	dateTimeFormat: function (time) {
		var dealDate;
		if (typeof (time) == "string") {
			dealDate = Date.parse(time);
		} else {
			dealDate = time;
		}
		var dealDateString = "{1} ({2}), {0}".format(
			window.appointment.timeFormat(dealDate),
			dealDate.toString(global["dateFormat"]),
			window.appointment.translateDay(dealDate.getDay())
		);
		return dealDateString;
	},
	translateTime: function (time) {
		if (time.length < 5) {
			return "";
		}
		time = time.substr(0, 5);
		times = time.split(":");
		if (times.length != 2) {
			return "";
		}
		var hour = appointment.common.toNumber(times[0]);
		var min = appointment.common.toNumber(times[1]);
		var txtAm = (global.text_am) ? global.text_am : "AM";
		var txtPm = (global.text_pm) ? global.text_pm : "PM";
		var unit = txtAm;
		if (hour > 12) {
			if (hour != 12) {
				hour = hour - 12;
			}
			unit = txtPm;
		}
		return "{0}:{1} {2}".format((hour + "").length == 1 ? "0" + hour : hour, (min + "").length == 1 ? "0" + min : min, unit);
	},
	translateDay: function (idx) {
		var days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
		if (global && global["day_collection"]) {
			days = global["day_collection"].split(",");
		}
		return days[idx];
	},
	popupLoginDialog: function () {
		var state = {
			cid: global.cid
		};
		var data = {
			appid: global.appid,
			redirecturi: "/admin/bo/checkOwner_handler",
			scope: "user_info,email_user",
			state: JSON.stringify(state)
		};
		shop88api.authen(data);
	},
	logout: function (callback) {
		this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.logout, {}, callback);
	},
	resizeFrame: function (s, h) {
		try {
			//app88resize(false);
			//var data = {height: (Math.max($(document).height(), $(window).height()))+'px'};		
			//shop88api.resizeIframeHeight(data);
			var data;

			if (!s) {
				s = true;
			}
			else {
				s = false;
			}
			if (h) {
				data = {
					height: Math.max(70, h) + 'px',
					noFocusTop: s
				};
			}
			else {
				var dialogHeight = $(".app_dialog").length > 0 ? $(".app_dialog").offset().top + $(".app_dialog").outerHeight(true) + window.appointment.dialog.prototype.minTop : 0;
				data = {
					height: (Math.max(70, $("#wrapper").height(), dialogHeight)) + 'px',
					noFocusTop: s
				};
			}
			shop88api.resizeIframeHeight(data);
		} catch (e) { }
	}
}
window.appointment.dialog = function(options) {
	this.init(options);
};
window.appointment.dialog.prototype = {
	minTop: 30,
	init: function (options) {
		this.options = $.extend({
			cls: "dbShop-ui-appLayer", // fe : dbShop-ui-appLayer, bo:dbShop-ui-nlayer
			titleTag: "h1",
			title: "",
			clickObject: null,
			contentObj: null,
			content: "content",
			buttons: [] //text, onclick, cls
		},
		options);
		this.setup();
	},
	setup: function () {
		var me = this;
		this.bkLayer = $(".common_bkLayer");
		if (this.bkLayer.length == 0) {
			this.bkLayer = $("<div class='common_bkLayer' style='background-color: black; position: absolute; left: 0px; top: 0px; width: 500px; height: 600px; opacity: 0.5;z-index:900'>");
		}
		this.container = $("<div class=\"app_dialog " + this.options.cls + "\" style=\"position:absolute;z-index:910;top:0;\">");
		if (this.options.title.length > 0) {
			this.container.append("<" + this.options.titleTag + ">" + this.options.title + "</" + this.options.titleTag + "> ");
		}
		this.container.append("<div class=\"dbShop-ui-wrapper20\"><span class='dialog_content'>" + this.options.content + "</span><div class=\"dbShop-ui-clearboth\"></div></div>");
		if (this.options.contentObj) {
			this.container.find(".dialog_content").append(this.options.contentObj);
		}
		if (this.options.buttons.length > 0) {
			var buttonAreas = $("<div class=\"dbShop-ui-btnArea\">");
			$.each(this.options.buttons,
			function (i, buttonOptions) {
				buttonOptions = $.extend({
					text: "text",
					onclick: function () { },
					cls: "dbShop-ui-sendBtn"
				},
				buttonOptions);
				var button = $("<a onclick=\"return false;\"  class=\"" + buttonOptions.cls + "\" href=\"javascript:;\">" + buttonOptions.text + "</a>");
				button.click(function () {
					buttonOptions.onclick.apply(me);
				});
				buttonAreas.append(button);
			});
			this.container.append(buttonAreas);
		}
		$("body").append(this.container).append(this.bkLayer);
		this.show();
	},
	setPosition: function () {
		var top;
		if (this.options.clickObject) {
			top = this.options.clickObject.offset().top - (this.container.outerHeight(true) / 2);
		}
		else
		{
			top = ($(window).height() - this.container.outerHeight(true)) / 2;
		}
		if ((top + this.container.outerHeight(true) + this.minTop) > $("#wrapper").height() &&
			(this.minTop + this.container.outerHeight(true) + this.minTop) < $("#wrapper").height())
		{
			top = $("#wrapper").height() - this.container.outerHeight(true) - this.minTop;
		}
		top = Math.max(this.minTop, top);
		this.container.css({
			left: ($(window).width() - this.container.width()) / 2,
			top: top
		});
		this.bkLayer.css({
			width: $(document).width(),
			height: Math.max($(document).height(), $(window).height(), top + this.container.outerHeight(true) + this.minTop)
		});
		window.appointment.resizeFrame(false);
	},
	show: function (msg) {
		if (typeof (msg) != "undefined") {
			this.container.find(".dialog_content").html(msg);
		}
		$("body").css({
			"overflow-y": "hidden"
		});
		this.container.show();
		this.bkLayer.show();
		this.setZIndex(true);
		this.setPosition();
		
		var thisDialog = this;
		var dialogContentImgNum = this.container.find(".dialog_content img").length;
		this.container.find(".dialog_content img").load(function(){
			dialogContentImgNum--;
			if(!dialogContentImgNum)
			{
				thisDialog.setPosition();
			}
		});
	},
	hideBkLayer: function () {
		var existDialogLength = $(".app_dialog:visible").length;
		if (existDialogLength == 1) {
			$("body").css({
				"overflow-y": "auto"
			});
			this.bkLayer.hide();
		}
	},
	setZIndex: function (setContainer) {
		var maxIndex = 0;
		$(".app_dialog:visible").each(function () {
			maxIndex = Math.max(maxIndex, $(this).css("z-index"));
		});
		if (setContainer) {
			this.container.css("z-index", maxIndex + 2);
			this.bkLayer.css("z-index", maxIndex + 1);
		} else {
			this.bkLayer.css("z-index", maxIndex - 1);
		}

	},
	hide: function () {
		this.hideBkLayer();
		this.container.hide();
		this.setZIndex();
	},
	destory: function () {
		this.hideBkLayer();
		this.container.remove();
		this.setZIndex();
		window.appointment.resizeFrame(false);
	}
};
$.extend(window.appointment.dialog, {
	color: {
		grey: "msBtn dbShop-ui-grey",
		blue: "msBtn dbShop-ui-red",
		red: "dbShop-ui-sendBtn"
	},
	error: function (msg, fn) {
		new window.appointment.dialog({
			content: msg,
			buttons: [{
				text: global.close,
				onclick: function () {
					this.destory();
					if (typeof (fn) != "undefined") {
						fn();
					}
				}
			}]
		});
	},
	info: function (title, msg, onOk, clickObject) {
		new window.appointment.dialog({
			title: title,
			content: msg,
			clickObject: clickObject,
			buttons: [{
				text: global.close,
				cls: appointment.dialog.color.blue,
				onclick: function () {
					this.destory();
					if (typeof (onOk) != "undefined") {
						onOk.apply(this);
					}
				}
			}]
		});
	}
});

window.appointment.schedule = function (options) {
	this.options = $.extend({
		serviceId: '',
		resourceId: '', //可以接受 resourceid的数组
		serverDate: new Date(),  //當前系統時間	
		calendar: null, //日曆jq對象
		autoBegin: false, //是否自動綁定日曆
		disableTodayHighlight: false,
		calendarShow: function (calendar) {
			calendar.fadeIn("fast");
		}, //calendar 顯示動作
		calendarHide: function (calendar) {
			calendar.hide();
		}, //calendar 隱藏動作
		onLoadedService: function (service) { }, //ajax getServiceDetail 完成囘調函數
		onLoadedResource: function (resource) { }, //ajax getResourceDetail 完成囘調函數
		onBeforeSelectDate: function (date) { }, //單擊日曆一個日期前的囘調函數
		onAfterSelectDate: function (date) { }, //單擊日曆一個日期后的囘調函數
		onBindTimeSlots: function (timeSlots) { }, //綁定時間的囘調函數
		onLoadComplete: function () { }, // 全部ajax加載完囘調函數
		onHasSchedule: function () { }, //没有任何schema的回调函数
		onCheckedHasSchema: null //檢查是否含有可用的schema 囘調函數
	}, options);
	var t = this;
	if (t.options.resourceId.length == "") {
		return false;
	}
	var opts = this.options;
	var calendar = opts.calendar;

	var serverDate = opts.serverDate //當前系統時間	
	var resourceServiceDateTime = {}; //resource 基本設置
	var monthAvailableTimeSlot = []; //保存每個月被預定的信息
	var datePickerDayStatus = {}; //保存每一天的預定狀態
	var getAvailableTimeSlotByDateCache = {}; //getAvailableTimeSlotByDate 方法緩存
	this.ResoucesTimeSlot = {}; //保存当前的resources的各自timeslot值

	this.getAvailableTimeSlotByDate = getAvailableTimeSlotByDate;
	this.getMaxDateTime = getMaxDateTime;
	var isMutilResource = $.type(t.options.resourceId) == "array"; //是否是多resource
	var ignoreDates = {}; //不可選擇的日期
	var weekday = {}; //保存工作日
	var curMaxDate = null; //最大可选日期
	var curMinDate = null; //最小可选日期
	//this.resourceServiceDateTime = resourceServiceDateTime;
	//this.weekday = weekday;
	//this.ignoreDates = ignoreDates;


	//獲取所有的time slot
	this.getTimeSlot = function (date, resourceId) {
		var serviceTimes = getAvailableTimeSlotByDate(date, resourceId);
		return genTimeSlots(serviceTimes);
	}
	//修改options
	this.setOptions = function (opt) {
		return $.extend(this.options, opt);
	}

	this.isBinded = false; //是否已經綁定日曆，防止多次調用this.begin，多次綁定

	if (calendar) {
		opts.calendarHide.call(t, calendar);
		calendar.datepicker({
			dateFormat: appointment.common.getDateFormateForDatepicker(),
			minDate: serverDate,
			onSelect: datePickerOnSelect
		}).datepicker("setDate", serverDate.toString(global["dateFormat"]));
	}


	t.setDate = function (date) {
		onSelectDate(date, resourceServiceDateTime.resource);
		if (calendar) {
			if (date < calendar.datepicker("option", "minDate")) {
				calendar.val(date.toString(global["dateFormat"]));
			} else {
				calendar.datepicker("setDate", date);
			}
		}
	}

	t.destroy = function () {
		if (calendar) {
			calendar.datepicker("destroy");
		}
	}
	//檢查當前resource是否有可用的schema
	this.checkResourceHasSchema = function () {
		var hasSchema = false;
		$.each(resourceServiceDateTime.resource, function (resourceGuid, moreInfo) {
			if ((!moreInfo.NormalHours || moreInfo.NormalHours.length == 0) && (!moreInfo.SpecialHours || moreInfo.SpecialHours.length == 0)) {
				return true; //continue each
			}
			//var hasSchema = false;

			//判断schema是否可用
			var maxDate = getMaxDateTime(moreInfo);
			if (maxDate == null) {
				maxDate = Date.now().addDays(3 * 31); //如果沒有enddate，則判斷3個月內是否有可用時間段
			}
			var minDate = appointment.common.cloneDate(serverDate).addHours(resourceServiceDateTime.MinTime); //最小日期
			var startDate = appointment.common.cloneDate(serverDate).clearTime();
			var _ignoreDates = []; //不可選擇的日期
			if (moreInfo.DayOff) {
				$.each(moreInfo.DayOff, function (i, dayOff) {
					if (dayOff.EndDate != null) {
						_ignoreDates.push(Date.parse(dayOff.StartDate));
						var dayDiff = appointment.common.dayDiff(Date.parse(dayOff.EndDate), Date.parse(dayOff.StartDate));
						if (dayDiff > 0) {
							for (var i = 1; i <= dayDiff; i++) {
								_ignoreDates.push(Date.parse(dayOff.StartDate).addDays(i));
							}
						}
					}
				});
			}

			var _weekday = []; //保存工作日
			if (moreInfo.NormalHours) {
				$.each(moreInfo.NormalHours, function (i, normalHour) {
					if ($.inArray(normalHour.Day, _weekday) == -1) {
						_weekday.push(normalHour.Day);
					}
				});
			}

			while (startDate < maxDate) {
				console.log("while");
				var _hasAvailableDate = haveAvailableDate(startDate, _ignoreDates, _weekday, minDate, moreInfo, opts.serviceId, resourceGuid);
				if (_hasAvailableDate) {
					console.log("_hasAvailableDate");
					hasSchema = true;
					break;
				} else {
				}
				startDate.addDays(1);
			}
			if (hasSchema) {
				return false; //break each.
			}

		});
		return hasSchema;

	}
	//get service detail
	appointment.getServiceDetail({ serviceId: opts.serviceId }, function (r) {
		if (!r.Name) {
			return;
		}
		//resourceServiceDateTime[""] = {};
		resourceServiceDateTime["TimeSlotDivision"] = appointment.common.toNumber(r.TimeSlotDivision);
		resourceServiceDateTime["MinTime"] = appointment.common.toNumber(r.MinTime);
		resourceServiceDateTime["AvailablePerTimeSlot"] = appointment.common.toNumber(r.AvailablePerTimeSlot);
		resourceServiceDateTime["AdvanceTime"] = (r.AdvanceTime == null ? 10 * 365 * 24 : r.AdvanceTime);
		resourceServiceDateTime["Duration"] = appointment.common.toNumber(r.Duration);
		resourceServiceDateTime.resource = {};
		firstDate = null;
		secondDate = null;
		opts.onLoadedService.call(t, r);
		//get resource detail
		appointment.getResourceDetail({ resourceGuid: opts.resourceId, includedMoreInfo: true }, function (rs) {
			if (isMutilResource) {
				$.each(rs, function (i, r) {
					process(r);
				});
			} else {
				process(rs);
			}

			if (opts.autoBegin) {
				t.isBinded = true;
				setCalendar(curMaxDate, curMinDate, ignoreDates, weekday, rs);
			}
			t.begin = function () {
				if (!t.isBinded) {
					t.isBinded = true;
					setCalendar(curMaxDate, curMinDate, ignoreDates, weekday, rs);
				}
			}
			if (opts.onCheckedHasSchema) {
				//checkResourceHasSchema(r, function (hasSchema) {
				//	opts.onCheckedHasSchema.call(t, hasSchema);
				//});
			}
			opts.onLoadComplete.call(t);
		});
	});

	//获取完server，resource data之后开始执行
	function process(resource) {
		var r = resource;
		if (!r.Name) {
			return;
		}
		opts.onLoadedResource.call(t, r);

		//get resource service time
		var moreInfo = r.MoreInfo;
		weekday[r.ResourceGuid] = [];
		ignoreDates[r.ResourceGuid] = [];
		resourceServiceDateTime.resource[r.ResourceGuid] = {};
		$.extend(resourceServiceDateTime.resource[r.ResourceGuid], moreInfo);
		//set disable date
		var maxDate = appointment.common.cloneDate(serverDate).addHours(resourceServiceDateTime.AdvanceTime); //最大日期
		var minDate = appointment.common.cloneDate(serverDate).addHours(resourceServiceDateTime.MinTime); //最小日期

		if (moreInfo.DayOff) {
			$.each(moreInfo.DayOff, function (i, dayOff) {
				//如果无限结束日期，则重设maxdate
				if (dayOff.EndDate == null && Date.parse(dayOff.StartDate) < maxDate) {
					maxDate = Date.parse(dayOff.StartDate).addDays(-1);
				} else if (dayOff.EndDate != null) {
					ignoreDates[r.ResourceGuid].push(Date.parse(dayOff.StartDate));
					var dayDiff = appointment.common.dayDiff(Date.parse(dayOff.EndDate), Date.parse(dayOff.StartDate));
					if (dayDiff > 0) {
						for (var i = 1; i <= dayDiff; i++) {
							ignoreDates[r.ResourceGuid].push(Date.parse(dayOff.StartDate).addDays(i));
						}
					}
				}
			});
		}
		if (moreInfo.NormalHours) {
			$.each(moreInfo.NormalHours, function (i, normalHour) {
				if ($.inArray(normalHour.Day, weekday[r.ResourceGuid]) == -1) {
					weekday[r.ResourceGuid].push(normalHour.Day);
				}
			});
		}

		if (curMaxDate == null || curMaxDate.compareTo(maxDate) == -1) {
			curMaxDate = appointment.common.cloneDate(maxDate);
		}
		if (curMinDate == null || minDate.compareTo(curMinDate) == -1) {
			curMinDate = appointment.common.cloneDate(minDate);
		}
	}



	function setCalendar(maxDate, minDate, _ignoreDates, _weekday, resources) {
		if (maxDate != null) {
			calendar.datepicker("option", "maxDate", maxDate);
			calendar.datepicker("option", "minDate", minDate);
		}

		//遍歷當前月全部日期			
		calendar.datepicker("option", "beforeShowDay", function (date) {
			if (isMutilResource) {
				//预先读取月份的使用时间
				var month = date.getMonth() + 1;
				if (date.getDate() == 1 &&
					(
						monthAvailableTimeSlot.length == 0 ||
					$.grep(monthAvailableTimeSlot, function (m) { return m.month == month && m.resourceGuid == resources[0].ResourceGuid; }).length == 0)
				) {
					var rid = [];
					$.each(resources, function (i, r) {
						rid.push(r.ResourceGuid);
					});
					var countResult = appointment.getAvailableTimeSlotByMonth({
						serviceid: opts.serviceId,
						resourceid: rid,
						date: date.toString('1-MMM-yyyy')
					});
					$.each(countResult, function (i, c) {
						monthAvailableTimeSlot.push({
							month: month,
							resourceGuid: resources[i].ResourceGuid,
							count: c
						});
					});

				}

				var isAvailableDate = false;
				$.each(resources, function (i, resource) {
					var moreInfo = resource.MoreInfo;
					var resourceId = resource.ResourceGuid
					var _avalableDate = haveAvailableDate(date, _ignoreDates[resourceId], _weekday[resourceId], minDate, moreInfo, opts.serviceId, resourceId);
					if (_avalableDate == true) {
						isAvailableDate = true;
						return false;
					}
				});
				return [isAvailableDate];
			} else {
				var moreInfo = resources.MoreInfo;
				var resourceId = resources.ResourceGuid
				return [haveAvailableDate(date, _ignoreDates[resourceId], _weekday[resourceId], minDate, moreInfo, opts.serviceId, resourceId)];
			}
		});

		//判斷當前月是否有可用日期
		var loopCount = 5; //最多判斷10個月
		while (firstDate == null && loopCount > 0) {
			loopCount--;
			//加載下一個月
			var nextMonthDate = calendar.datepicker("getDate").addMonths(1).set({
				day: 1, hour: 0, minute: 0
			});
			calendar.datepicker("setDate", nextMonthDate.toString(global["dateFormat"]));
		}

		if (firstDate && firstDate <= maxDate) {
			onSelectDate(firstDate, resources);
			calendar.datepicker("setDate", firstDate);
			calendar.datepicker("option", "minDate", firstDate);
			opts.onHasSchedule.call(t, true);
		} else {
			opts.onHasSchedule.call(t, false);
		}
		
		if(opts.disableTodayHighlight)
		{
			calendar.find("a.ui-state-highlight").removeClass('ui-state-highlight');
			calendar.find("a.ui-state-hover").removeClass('ui-state-hover');
		}
		
		opts.calendarShow.call(t, calendar);
	}

	//判斷日期是否可用
	function haveAvailableDate(date, _ignoreDates, _weekday, minDate, moreInfo, serviceid, resourceId) {
		var status = datePickerDayStatus[resourceId]
		if (status != null && status[date.toString()] != null) {
			return status[date.toString()];
		}
		var month = date.getMonth() + 1;
		if (date.getDate() == 1 &&
			(
				monthAvailableTimeSlot.length == 0 ||
			$.grep(monthAvailableTimeSlot, function (m) { return m.month == month && m.resourceGuid == resourceId; }).length == 0)
		) {
			monthAvailableTimeSlot.push({
				month: month,
				resourceGuid: resourceId,
				count: appointment.getAvailableTimeSlotByMonth({
					serviceid: serviceid,
					resourceid: resourceId,
					date: date.toString('1-MMM-yyyy')
				})
			});
		}

		var isIgnore = false;

		$.each(_ignoreDates, function (i, ignoreDate) {
			if (ignoreDate.getYear() == date.getYear() && ignoreDate.getMonth() == date.getMonth() && ignoreDate.getDate() == date.getDate()) {
				isIgnore = true;
				return true;
			}
		});

		if (!isIgnore) {
			//判斷是否是工作日
			isIgnore = true;
			$.each(_weekday, function (i, d) {
				if (date.getDay() == d) {
					isIgnore = false;
					return true;
				}
			});
			//如果沒在工作日，則判斷在不在special date
			if (isIgnore) {
				if ($.grep(moreInfo.SpecialHours, function (m) { return m.Date == date.toString('yyyy-MM-dd'); }).length > 0) {
					isIgnore = false;
				}
			}
		}

		if (!isIgnore) {
			//判断当前日期是否小于mindate
			if (appointment.common.cloneDate(date).set({ hour: 23, minute: 59 }) < minDate) {
				isIgnore = true;
			}
			else if (getAvailableTimeSlotByDate(date, true, resourceId).length == 0) {
				//判斷當前日期是否訂滿了
				isIgnore = true;
			}
		}

		if (!isIgnore && date >= appointment.common.cloneDate(serverDate).at({ hour: 0, minute: 0, second: 0 }) && (firstDate == null || secondDate == null)) {
			if (firstDate == null) {
				firstDate = appointment.common.cloneDate(date);
			} else {
				secondDate = appointment.common.cloneDate(date);
			}
		}
		//把日期狀態保存，以便下一次不用判斷
		if (!datePickerDayStatus[resourceId]) {
			datePickerDayStatus[resourceId] = {};
		}
		datePickerDayStatus[resourceId][date.toString()] = !isIgnore;
		return !isIgnore;
	}

	function datePickerOnSelect(dateText, inst) {
		//var date = Date.parse(dateText);
		onSelectDate(t.options.calendar.datepicker("getDate"), resourceServiceDateTime.resource);
		if(opts.disableTodayHighlight)
		{
			setTimeout(function(){
				calendar.find("a.ui-state-highlight").removeClass('ui-state-highlight');
				calendar.find("a.ui-state-hover").removeClass('ui-state-hover');
			}, 0);
		}
	}

	function getMaxDateTime(r) {
		var minDate = appointment.common.cloneDate(serverDate).addHours(resourceServiceDateTime.MinTime);
		var maxDate = null;
		if (resourceServiceDateTime.AdvanceTime > 0) {
			maxDate = appointment.common.cloneDate(serverDate).addHours(resourceServiceDateTime.AdvanceTime);
		}
		if (!r.DayOff && maxDate == null) {
			return null;
		}
		if (!r.DayOff) {
			r.DayOff = [];
		}

		//获取最小DayOff的StartDate
		var noEndDate = $.grep(r.DayOff, function (m) {
			return m.EndDate == null;
		});
		if (noEndDate.length > 0 || maxDate != null) {
			var minEndDate = null;
			$.each(noEndDate, function (i, m) {
				if (minEndDate == null || minEndDate > Date.parse(m.StartDate)) {
					minEndDate = Date.parse(m.StartDate);
				}
			});
			if (minEndDate) {
				minEndDate = minEndDate.addDays(-1).set({ hour: 23, minute: 59 });
			}
			if (minEndDate == null || maxDate != null && minEndDate > maxDate) {
				minEndDate = maxDate;
			}
		}
		return minEndDate;
	}


	function onSelectDate(date, resources) {
		opts.onBeforeSelectDate.call(t, date);
		var serviceTimes = [];
		if (isMutilResource) {
			$.each(resources, function (i, resource) {
				var resourceId = $.type(resources) == "array" ? resource.ResourceGuid : i; //resources参数允许数组或者对象
				var timeSlot = getAvailableTimeSlotByDate(date, false, resourceId);
				t.ResoucesTimeSlot[resourceId] = genTimeSlots(timeSlot);
				$.merge(serviceTimes, timeSlot);
			});
		} else {
			var resourceId = resources.ResourceGuid;
			if (!resourceId) {
				$.each(resources, function (rid, data) {
					resourceId = rid;
				});
			}
			var timeSlot = getAvailableTimeSlotByDate(date, false, resourceId);
			t.ResoucesTimeSlot[resourceId] = genTimeSlots(timeSlot);
			serviceTimes = timeSlot;
		}
		serviceTimes = serviceTimes.distinct().sort();
		//var serviceTimes = getAvailableTimeSlotByDate(date, false, resourceGuid);
		if (serviceTimes.length == 0) {
			//今天沒有時間段可選，則設置下一天
			if (calendar && date.toString("yyyyMMdd") == serverDate.toString("yyyyMMdd")) {
				calendar.datepicker("option", "minDate", secondDate);
				onSelectDate(secondDate, resources);
				return false;
			}
		}
		bindTimeSlots(genTimeSlots(serviceTimes));
		opts.onAfterSelectDate.call(t, date);
	}

	//返回某個時間的timeslot 列表
	//onlyCheck true: 只返回有或者沒有的結果，不返回列表		
	function getAvailableTimeSlotByDate(date, onlyCheck, resourceGuid) {
		var cacheKey = "{0}_{1}_{2}_{3}_{4}".format(date.toString("yyyy-MM-dd HH:mm:ss"), t.options.serviceId, t.options.resourceId, (onlyCheck ? "1" : "0"), resourceGuid);
		//console.log("cacheKey=", cacheKey);
		if (getAvailableTimeSlotByDateCache[cacheKey]) {
			return getAvailableTimeSlotByDateCache[cacheKey];
		}

		if (typeof (onlyCheck) == "undefined") {
			onlyCheck = false;
		}
		var bookingTimes = getBookingTimesByDate(date, resourceGuid);
		var bookingTimesObject = {};
		$.each(bookingTimes, function (i, bt) {
			if (bookingTimesObject[bt.BookingTime.substr(0, 10)]) { //獲取時間， 排除毫秒
				bookingTimesObject[bt.BookingTime.substr(0, 10)].push(bt);
			} else {
				bookingTimesObject[bt.BookingTime.substr(0, 10)] = [bt];
			}
		});


		var dayName = date.getDay();
		var serviceDays = $.grep(resourceServiceDateTime.resource[resourceGuid].NormalHours, function (m) { return m.Day == dayName; });
		var specialDate = [];
		if (resourceServiceDateTime.resource[resourceGuid].SpecialHours) {
			specialDate = $.grep(resourceServiceDateTime.resource[resourceGuid].SpecialHours, function (m) { return m.Date == date.toString('yyyy-MM-dd'); });
		}
		if (specialDate.length > 0) {
			serviceDays = specialDate;
		}
		//處理24點，轉為23:59
		for (var i = 0; i < serviceDays.length; i++) {
			if (serviceDays[i].EndTime.substr(0, 5) == "24:00") {
				serviceDays[i].EndTime = "23:59";
			}
		}
		if (!onlyCheck) {
			//合并时间
			if (serviceDays.length > 1) {
				var _mergeDates = [];
				$.each(serviceDays, function (i, n) {
					_mergeDates.push([n.StartTime, n.EndTime]);
				});
				_mergeDates = appointment.common.mergeTimeRanges(_mergeDates);
				var day = serviceDays[0].Day;
				serviceDays = [];
				$.each(_mergeDates, function (i, n) {
					serviceDays.push({
						Day: day,
						StartTime: n[0],
						EndTime: n[1]
					});
				});
			}
		}
		var serviceTimes = []; //可用時間段
		$.each(serviceDays, function (i, d) {
			var sdata = d.StartTime.split(":");
			var edata = d.EndTime.split(":");
			var startTime = appointment.common.cloneDate(date).set({ hour: appointment.common.toNumber(sdata[0], 10), minute: appointment.common.toNumber(sdata[1], 10) });
			var endTime = appointment.common.cloneDate(date).set({ hour: appointment.common.toNumber(edata[0], 10), minute: appointment.common.toNumber(edata[1], 10) });
			var i = 0;
			while (i < 200) { //最多200個TimeSlots,防止死循環
				i++;
				if (appointment.common.compareDateTime(startTime.toString("yyyy-MM-dd HH:mm:ss"), endTime.toString("yyyy-MM-dd HH:mm:ss")) == 1) {
					//開始時間要大於結束時間
					break;
				} else if (
					appointment.common.compareDateTime(appointment.common.cloneDate(startTime).addMinutes(endTime.toString("mm") == "59" ? resourceServiceDateTime.Duration - 1 : resourceServiceDateTime.Duration).toString("yyyy-MM-dd HH:mm:ss"), endTime.toString("yyyy-MM-dd HH:mm:ss")) == 1
				) { //不能OT
					startTime.addMinutes(resourceServiceDateTime.TimeSlotDivision);
					continue;
				} else if (
					appointment.common.compareDateTime(appointment.common.cloneDate(serverDate).addHours(resourceServiceDateTime.MinTime).toString("yyyy-MM-dd HH:mm:ss"), startTime.toString("yyyy-MM-dd HH:mm:ss")) == 1
				) { //只能提前多久能够预订（單位小時）
					startTime.addMinutes(resourceServiceDateTime.TimeSlotDivision);
					continue;
				} else if (
					appointment.common.compareDateTime(appointment.common.cloneDate(serverDate).addHours(resourceServiceDateTime.AdvanceTime).toString("yyyy-MM-dd HH:mm:ss"), startTime.toString("yyyy-MM-dd HH:mm:ss")) == -1
				) {   //能預訂到多久（單位小時）
					break;
				}
				//判斷的當前時間段是否訂滿了

				var alreadyBooking = [];
				if (bookingTimesObject[startTime.toString("yyyy-MM-dd")]) {
					alreadyBooking = $.grep(bookingTimesObject[startTime.toString("yyyy-MM-dd")], function (m) {
						//1.判斷時間開始是否已經被預定過
						//2.判斷是否在結束時間之前
						//比如 服務的時間長度是120分鐘，曾經有9:00的預定記錄,那麼在9:00到11:00間的時間段不可用

						//3.檢查其他service是否有該時間的預定記錄, 同理要判斷#1  #2的邏輯		
						var strStartTime = startTime.toString("yyyy-MM-dd HH:mm:ss");
						var strDurationBookingTime = Date.parse(m.BookingTime).addMilliseconds(1000 * 60 * m.Duration).toString("yyyy-MM-dd HH:mm:ss");
						if (
						appointment.common.compareDateTime(strStartTime, m.BookingTime) != -1
						&& appointment.common.compareDateTime(strStartTime, (m.EndDateTime == null ? strDurationBookingTime : m.EndDateTime)) == -1
					) {
							return true;
						}
						return false;
					}
                );
				}
				if (alreadyBooking.length < resourceServiceDateTime["AvailablePerTimeSlot"]) {
					serviceTimes.push(appointment.common.cloneDate(startTime));
					if (onlyCheck) {
						break;
					}
				}
				startTime.addMinutes(resourceServiceDateTime.TimeSlotDivision);
			}
		});
		getAvailableTimeSlotByDateCache[cacheKey] = serviceTimes;
		return serviceTimes;
	}

	//獲取指定日期被預約的日期
	function getBookingTimesByDate(date, resourceGuid) {
		var month = date.getMonth() + 1;
		var datas = $.grep(monthAvailableTimeSlot, function (m) { return m.month == month && m.resourceGuid == resourceGuid; });
		if (datas.length == 0) {
			var data = {
				month: month,
				resourceGuid: resourceGuid,
				count: appointment.getAvailableTimeSlotByMonth({
					serviceid: opts.serviceId,
					resourceid: resourceGuid,
					date: date.toString('1-MMM-yyyy')
				})
			};
			monthAvailableTimeSlot.push(data);
			return data.count;
		} else {
			if (datas.length == 0) {
				return [];
			}
			var data = datas[0];
			return data.count;
		}
	}

	function genTimeSlots(serviceTimes) {
		var timeSlots = { AM: [], PM: [] };
		$.each(serviceTimes, function (i, time) {
			if (time.getHours() < 12) {
				//上午
				timeSlots.AM.push(time.toString("hh:mm"));
			} else {
				//下午
				timeSlots.PM.push(time.toString("hh:mm"));
			}
		});
		return timeSlots;
	}

	function bindTimeSlots(timeSlots) {
		opts.onBindTimeSlots.call(t, timeSlots);
	}
}

jQuery.fn.extend({
	getValue: function () {
		var value = $.trim($(this).val());
		if (value.length > 0) {
			var placeholder = $(this).attr("placeholder");
			if (typeof (placeholder) == "undefined") {
				placeholder = "";
			}
			if (value == placeholder) {
				value = "";
			}
		}
		return value;
	},
	setValidation: function (options) {
		return this.each(function () {
			var t = this;
			if (!$(this).hasClass("common-validatebox")) {
				$(this).addClass('common-validatebox');
			}
			if (typeof ($(this).attr("id")) == "undefined") {
				$(this).attr("id", appointment.common.newGuid());
			}
			$(this).data("common-validatebox-options", options);
			var isText = $(this).is(":text") || this.tagName.toLowerCase() == "textarea"; //是否是文本框<input/>, 否則為下拉菜單<select>
			if (options.required || options.validType) {
				var validFunction = function () { };
				if(isText){
					if (options.validType) {
					switch (options.validType) {
						case "number":
							$(this).keypress(function (e) {
								if (e.which == 45) {
									return false;
								}
								if (e.which == 46) {
									return false;
								} else {
									if ((e.which >= 48 && e.which <= 57 && e.ctrlKey == false && e.shiftKey == false) || e.which == 0 || e.which == 8) {
										return true;
									} else {
										if (e.ctrlKey == true && (e.which == 99 || e.which == 118)) {
											return true;
										} else {
											return false;
										}
									}
								}
							});
							$(this).bind("paste", function () {
								if (window.clipboardData) {
									var s = clipboardData.getData("text");
									if (!/\D/.test(s)) {
										return true;
									} else {
										return false;
									}
								} else {
									return false;
								}
							});
							validFunction = appointment.common.isInt;
							break;
						case "price":
							$(this).keypress(function (e) {
								if (e.which == 45) {
									//if ($(this).val().indexOf("-") == -1) {
									//	return true;
									//} else {
									return false;
									//}
								}
								if (e.which == 46) {
									if ($(this).val().indexOf(".") == -1) {
										return true;
									} else {
										return false;
									}
								} else {
									if ((e.which >= 48 && e.which <= 57 && e.ctrlKey == false && e.shiftKey == false) || e.which == 0 || e.which == 8) {
										return true;
									} else {
										if (e.ctrlKey == true && (e.which == 99 || e.which == 118)) {
											return true;
										} else {
											return false;
										}
									}
								}
							});
							$(this).bind("paste", function () {
								if (window.clipboardData) {
									var s = clipboardData.getData("text");
									if (!/\D/.test(s)) {
										return true;
									} else {
										return false;
									}
								} else {
									return false;
								}
							});
							validFunction = appointment.common.isPrice;
							break;
						case "phone":
							$(this).keypress(function (e) {
								var supportKey = [0, 8, 32, 45, 43, 41, 40];
								if ((e.which >= 48 && e.which <= 57 && e.ctrlKey == false && e.shiftKey == false)
										|| $.inArray(e.which, supportKey) > -1
									) {
									return true;
								} else {
									if (e.ctrlKey == true && (e.which == 99 || e.which == 118)) {
										return true;
									} else {
										return false;
									}
								}

							});
							validFunction = appointment.common.isPhoneNumber;
							break;
						case "email":
							validFunction = appointment.common.isEmail;
							break;
					}
				}
					$(this).keyup(function () {
						var value = $(this).getValue();
						//驗證必填
						if (options.required && value.length == 0) {
							//console.log("empty");
							appointment.common.displayErrorMessage(this, options.missingMessage);
							return true;
						} else {
							appointment.common.hideErrorMessage(this);
						}
						//驗證正則
						if (options.validType && value.length > 0) {
							if (!validFunction(value)) {
								appointment.common.displayErrorMessage(this, options.invalidMessage);
							} else {
								appointment.common.hideErrorMessage(this);
							}
						}
					});
				} else {
					$(this).change(function () {
						var value = $(this).val();
						//驗證必填
						if (options.required && value.length == 0) {
							appointment.common.displayErrorMessage(this, options.missingMessage);
							return true;
						} else {
							appointment.common.hideErrorMessage(this);
						}
					});
				}
			}
		});

	},
	hideErrorMessage: function () {
		return this.each(function () {
			var id = $(this).attr("id");
			$(".commonTipsMsg_" + id).hide();
		});
	},
	setPlaceHolder: function () {
		var doc = document,
		inputs = $(this),
		supportPlaceholder = 'placeholder' in doc.createElement('input'),
		placeholder = function (input) {
			var t = $(input);
			var text = t.attr('placeholder'),
			value = t.getValue(),
			defaultValue = ""; // input.defaultValue;
			if (defaultValue == '' && value.length == 0) {
				t.val(text);
			}
			input.onfocus = function () {
				if (t.val() === text) {
					t.val('');
				}
			};
			input.onblur = function () {
				if (t.val() === '') {
					t.val(text);
				}
			}
		};
		if (!supportPlaceholder) {
			inputs.each(function () {
				var text = $(this).attr('placeholder');
				if (text && text.length > 0) {
					placeholder(this)
				}
			});
		}
		return this;
	},
	helper: function () {
		return this.each(function () {
			var t = $(this);
			var id = "helperIcon_" + appointment.common.getRandom();
			t.addClass(id).append('<a class="help_img"><img width="16" height="16" alt="Help" src="/images/back_end/info-icon.png"></a>');
			var $helper = $(".helper_" + id);
			if ($(".helper_" + id).length == 0) {
				$helper = $('<div class="dbShop-ui-mHelp"><div class="dbShop-ui-helpContainer">' + t.attr("tips") + '</div></div>');
				$helper.addClass($(".helper_" + id));
				$("body").append($helper);
			}
			$helper.css({
				left: t.offset().left + t.find(".help_img").outerWidth(true),
				top: t.offset().top + t.find(".help_img").outerHeight(true)
			}).hide();
			$(this).click(function () {
				$helper.show().outerClick(function () {
					$(this).hide();
					$.removeOuterClick(id);
				}, [t], function () { }, id); ;
			});
		});
	}
});

(function ($) {
	$.fn.emptyHtml =  function(){
		return this.each(function(){
			this.innerHTML = "";
		});
	},
	$.fn.outerClick = function (callback, excludedObjs, excludedsFromFun, eventName) {
		this.each(function () {
			var t = this;
			eventName = eventName || "";
			$(document).bind("mousedown." + eventName, function (event) {
				var clickObj = event.target;
				if (!$(clickObj).isChild($(t))) {
					var match = true;
					if (excludedObjs == undefined) {
						excludedObjs = [];
					}
					if (excludedsFromFun == undefined) {
						excludedsFromFun = function () { return []; };
					}
					var funArray = excludedsFromFun.call(t);
					$.merge(excludedObjs, funArray || []);
					$.each(excludedObjs, function (i, excluded) {
						excluded.each(function () {
							if ($(clickObj).isChild($(this))) {
								match = false;
								return false; 
							}
						});
					});
					if (match) {
						callback.call(t);
					}
				}
			});
		});
	}
	$.fn.isChild = function (parents) {
		var isChild = false;
		this.each(function () {
			var child = this;
			parents.each(function () {
				if (this == child) {
					isChild = true;
					return true;
				}
				$(this).find("*").each(function () {
					if (this == child) {
						isChild = true;
						return false; //break;
					}
				});
			});
			if (isChild) {
				return false; //break;
			}
		});
		return isChild;
	}
	$.removeOuterClick = function (eventName) {
		eventName = eventName || "";
		if (eventName.length > 0) {
			$(document).unbind("mousedown." + eventName);
		}
	}
})(jQuery);