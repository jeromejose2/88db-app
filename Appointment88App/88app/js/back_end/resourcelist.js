﻿$(function () {
	appointment.common.setDefaultValue();
	appointment.common.setValidation();
	ddlMinutesDuration = 15;
	global.resourceId = "";
	normalHourIgnoreTime = 0;

	//load first resource
	var firstResourceGuid = $(".listScrollBox_B li a").first().attr("resourceid");
	$(".listScrollBox_B :radio:first").attr("checked", "checked");
	if (firstResourceGuid) {
		loadResource(firstResourceGuid);
	}
	else
	{
		$(".btnDeleteResource").hide();
	}

	$('.minicolors').each(function () {
		$(this).miniColors({
			hasConfirmPanel: false,
			showThemeColors: true,
			realTimeChangeTriggerEleColor: true,
			change: function (hex, rgb) {
				appointment.common.hideErrorMessage($('.minicolors'));
			}
		});

	});

	$(".btnViewDetail").click(function () {
		var radio = $(this).prev(":radio");
		$(".listScrollBox_B :radio").removeAttr("checked");
		radio.attr("checked", "checked");
		var resourceid = $(this).attr("resourceid");
		if (hasChanged()) {
			openDiscardDialog(function () {
				this.destory();
				loadResource(resourceid);
			}, $(this));
		} else {
			loadResource(resourceid);
		}
	});

	$(".listScrollBox_B :radio").click(function () {
		$(this).next(".btnViewDetail").click();
	});

	$(".btnDeleteResource").click(function () {
		var resourceId = global.resourceId; // $(this).prev().attr("resourceid");
		var name = $("#name").val(); // $(this).prev().text();
		appointment.dialog.bo.deleteConfirm(global["delete_resource_dialog_title"], global["delete_resource_dialog_content"].replace("{name}", name), function () {
			this.destory();
			var inputs = [];
			inputs.push({ name: "action", value: "delete" });
			inputs.push({ name: "guid", value: resourceId });
			appointment.common.postUrl({
				inputs: inputs
			});
		}, $(this));
	});

	$(document).on("click", ".btnDeleteRow", function () {
		var container = $(this).parents(".scheduled-container");
		var row = $(this).parents(".row-data");
		var title, content;
		if (container.hasClass("normalHoursContainer")) {
			title = global["delete_normal_hour_dialog_title"];
			content = global["delete_normal_hour_dialog_content"];
			normalHourIgnoreTime = 0;
		} else if (container.hasClass("specialHoursContainer")) {
			title = global["delete_special_hour_dialog_title"];
			content = global["delete_special_hour_dialog_content"];
		} else if (container.hasClass("dayoffContainer")) {
			title = global["delete_day_off_dialog_title"];
			content = global["delete_day_off_dialog_content"];
		}
		appointment.dialog.bo.deleteConfirm(title, content, function () {
			this.destory();
			row.remove();
			appointment.resizeFrame(false);
		}, $(this));
	});

	$(".btnAddDayoffDay").click(function () {
		//保存已添加过的
		var newRow = $(".dayoffContainer .add");
		if (newRow.length > 0) {

			appendDayoffDay({
				startDate: newRow.find(".datePicker:first").datepicker("getDate").toString("yyyy-MM-dd"),
				endDate: newRow.find(":checkbox").is(":checked") ? null : newRow.find(".datePicker:last").datepicker("getDate").toString("yyyy-MM-dd")
			});
			newRow.remove();
		}
		appendNewDayoffDay();
		appointment.resizeFrame(false);
	});

	$(".btnAddSpecialHour").click(function () {
		//保存已添加过的
		var newRow = $(".specialHoursContainer .add");
		if (newRow.length > 0) {
			appendSpecialHour({
				date: newRow.find(".datePicker").datepicker("getDate").toString("yyyy-MM-dd"),
				startTime: newRow.find(".timePicker:first").val(),
				endTime: newRow.find(".timePicker:last").val()
			});
			newRow.remove();
		}
		appendNewSpecialHour();
		appointment.resizeFrame(false);
	});

	$(".btnNormalHour").click(function () {
		//保存已添加过的
		var newRow = $(".normalHoursContainer .add");
		if (newRow.length > 0) {
			appendNormalHour({
				day: newRow.find(".dayPicker").val(),
				startTime: newRow.find(".timePicker:first").val(),
				endTime: newRow.find(".timePicker:last").val()
			});
			newRow.remove();
		}
		appendNewNormalHour();
		appointment.resizeFrame(false);
	});

	$(".btnAddResource").click(function () {
		if (hasChanged()) {
			openDiscardDialog(function () {
				emptyFields();
				this.destory();
			}, $(this));
		} else {
			emptyFields();
		}
	});

	$(".btnCancel").click(function () {
		openDiscardDialog(function () {
			loadCompleted(global.initValue);
			this.destory();
		}, $(this));
	});

	$(".HelpIco").click(function () {
		new appointment.dialog.bo.createResouceTips({ clickObject: $(this) });
	});

	/*
	$(".btnPrividedSericeTitle").click(function () {
	$(".providedService .infoCheckbox").toggle("fast");
	});
	*/

	$(".btnSave").click(function () {
		var btn = $(this);
		if (appointment.common.getValidation()) {
			new appointment.dialog({
				title: global["save_changes_dialog_title"],
				content: global["save_changes_dialog_content"],
				clickObject: btn,
				buttons: [{
					text: global.save,
					cls: appointment.dialog.bo.color.blue,
					onclick: function () {
						var detail = getResourceDetail();
						var description = detail.Description;
						detail.Description = detail.Description.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
						var t = this;
						appointment.dialog.bo.loading();
						saveResource(detail, function () {
							appointment.dialog.bo.closeLoading();
							detail.Description = description;
							global.initValue = detail;
							loadCompleted(detail);
							t.destory();
							appointment.dialog.bo.info(global["save_resource_dialog_title"], global["save_resource_dialog_content"], function () {
								if (global.resourceId.length == 0) {
									location.href = location.href;
								} else {
									//update resource name
									$(".btnViewDetail[resourceid=" + global.resourceId + "]").html(detail.Name)
								}
							}, btn);
						});
					}
				},
				{
					text: global.cancel,
					cls: "msBtn dbShop-ui-grey",
					onclick: function () {
						this.destory();
					}
				}]
			});
		}
	});

	$("#btnOrder").click(function () {
		var lis = $(".listScrollBox_B li");
		for (var i = lis.length - 1; i >= 0; i--) {
			$(".listScrollBox_B").append(lis[i])
		}
		if ($(this).hasClass("arrowSort_des")) {
			$(this).removeClass("arrowSort_des").addClass("arrowSort_asc")
		} else {
			$(this).addClass("arrowSort_des").removeClass("arrowSort_asc")
		}
	});

	$(window).bind('beforeunload', function () {
		if (hasChanged()) {
			return global["save_changes_dialog_content"];
		}
	});

	function emptyFields() {
		//關閉所有錯誤提示
		$("div[class*=commonTipsMsg_]").hide();
		global.resourceId = "";
		loadCompleted({
			Name: "",
			PhoneNumber: "",
			Email: "",
			Description: "",
			Services: [],
			NormalHours: [],
			SpecialHours: [],
			DayOff: []
		});
		$(".btnDeleteResource").hide();
	}

	function saveResource(detail, callback) {
		window.appointment.saveResource({ detail: detail }, function (r) {
			if (r.success) {
				callback();
			} else {
				appointment.dialog.bo.systemError();
			}
		});
	}

	function openDiscardDialog(onOk, clickObject) {
		//關閉所有錯誤提示
		$("div[class*=commonTipsMsg_]").hide();
		new appointment.dialog({
			title: global["discard_changes_dialog_title"],
			content: global["discard_changes_dialog_content"],
			clickObject: clickObject,
			buttons: [{
				text: global.text_discard,
				cls: appointment.dialog.bo.color.blue,
				onclick: function () {
					if (typeof (onOk) != "undefined") {
						onOk.apply(this);
					}
				}
			},
				{
					text: global.cancel,
					cls: "msBtn dbShop-ui-grey",
					onclick: function () {
						this.destory();
					}
				}]
		});
	}

	function hasChanged() {
		var initValue = global.initValue;
		if (initValue) {
			var curValue = getResourceDetail();
			return !appointment.common.isEqual(initValue, curValue)
		} else {
			return false;
		}
	}

	function loadResource(resourceId) {
		global.resourceId = resourceId;
		//close all error tips
		$("div[class*=commonTipsMsg_]").hide();
		$(".btnDeleteResource").show();
		appointment.getResourceDetail({
			resourceGuid: resourceId,
			ignoreServiceStatus: true,
			cache: false
		}, function (detail) {
			appointment.getResourceServiceDateTime({
				resourceGuid: resourceId,
				cache: false
			}, function (dateTimes) {
				loadCompleted($.extend(detail, {
					NormalHours: [],
					SpecialHours: [],
					DayOff: []
				}, dateTimes));
			});
		});
	}

	function loadCompleted(resource) {
		$("#name").val(appointment.common.htmlDecode(resource.Name));
		$("#phoneNumber").val(resource.PhoneNumber);
		$("#email").val(resource.Email);
		$("#assignColor").miniColors('value', resource.AssignColor);

		$(".providedService input").removeAttr("checked");
		$.each(resource.Services, function (i, service) {
			$(".providedService #" + service.ServiceGuid).attr("checked", "checked");
		});

		$(".normalHoursContainer .row-data").remove();
		if (resource.NormalHours) {
			$.each(resource.NormalHours, function (i, normalHour) {
				appendNormalHour({
					day: normalHour.Day,
					startTime: normalHour.StartTime,
					endTime: normalHour.EndTime
				});
			});
		}

		$(".specialHoursContainer .row-data").remove();
		if (resource.SpecialHours) {
			$.each(resource.SpecialHours, function (i, specialHour) {
				appendSpecialHour({
					date: specialHour.Date,
					startTime: specialHour.StartTime,
					endTime: specialHour.EndTime
				});
			});
		}

		$(".dayoffContainer .row-data").remove();
		if (resource.DayOff) {
			$.each(resource.DayOff, function (i, dayoff) {
				appendDayoffDay({
					startDate: dayoff.StartDate,
					endDate: dayoff.EndDate
				});
			});
		}

		if (tinyMCE.get()[0]) {
			tinyMCE.get()[0].setContent(resource.Description);
			global.initValue = getResourceDetail();
		} else {
			setTimeout(function () {
				tinyMCE.get()[0].setContent(resource.Description);
				global.initValue = getResourceDetail();
			}, 300);
		}

		appointment.resizeFrame();
	}

	function appendNormalHour(options) {
		options = $.extend({
			day: 0,
			startTime: "00:00",
			endTime: "00:00"
		}, options);

		var html = '<div class="listcontent row1 row-data" day="' + options.day + '" starttime="' + options.startTime + '" endtime="' + options.endTime + '"><span class="cc_day">{day}</span><span class="cc_start">{startTime}</span><span class="cc_end">{endTime}</span><span class="cc05"><a onclick="return false;" title="Delete" href="javascript:;" style="margin-left:5px" class="btnDeleteRow dbShop-ui-appBtn dbShop-ui-adddelete_action"></a></span></div> ';
		html = html.replace(/\{day\}/g, appointment.translateDay(options.day)).replace(/\{startTime\}/g, appointment.translateTime(options.startTime)).replace(/\{endTime\}/g, appointment.translateTime(options.endTime));
		$(".normalHoursContainer").append(html);
	}

	function appendSpecialHour(options) {
		options = $.extend({
			date: "1900-01-01",
			startTime: "00:00",
			endTime: "00:00"
		}, options);

		var html = '<div class="listcontent row1 row-data" date="' + options.date + '" starttime="' + options.startTime + '" endtime="' + options.endTime + '"><span class="cc_day">{date}</span><span class="cc_start">{startTime}</span><span class="cc_end">{endTime}</span><span class="cc05"><a onclick="return false;" title="Delete" href="javascript:;" style="margin-left:5px" class="btnDeleteRow dbShop-ui-appBtn dbShop-ui-adddelete_action"></a></span></div> ';
		html = html.replace("{date}", Date.parse(options.date).toString(global["dateFormat"])).replace("{startTime}", appointment.translateTime(options.startTime)).replace("{endTime}", appointment.translateTime(options.endTime));
		$(".specialHoursContainer").append(html);
	}

	function appendDayoffDay(options) {
		options = $.extend({
			startDate: "1900-01-01",
			endDate: "1900-01-01"
		}, options);
		var html = '<div class="listcontent row1 row-data" startdate="' + options.startDate + '" enddate="' + options.endDate + '" ><span class="cc_day">{startDate}</span><span class="cc_start" style="width:390px;_width:380px">{endDate}</span><span class="cc05"><a onclick="return false;" title="Delete" href="javascript:;" style="margin-left:5px" class="btnDeleteRow dbShop-ui-appBtn dbShop-ui-adddelete_action"></a></span></div>';
		html = html.replace("{startDate}", Date.parse(options.startDate).toString(global["dateFormat"])).replace("{endDate}", options.endDate ? Date.parse(options.endDate).toString(global["dateFormat"]) : global["text_no_end_date"]);
		$(".dayoffContainer").append(html);
	}

	//添加新的day off
	function appendNewDayoffDay() {
		var html = $('<div class="listcontent add row-data"><span class="cc_day"> <input type="text" readonly="" value="" size="12" class="datePicker" name="' + appointment.common.newGuid() + '"><img class="ui-datepicker-trigger" src="../../images/calendar.gif" alt="" title=""> </span> <span style="width:203px;_width:194px" class="cc_start"> <input type="text" readonly="" value="" size="12" id="' + appointment.common.newGuid() + '" class="datePicker"><img class="ui-datepicker-trigger" src="../../images/calendar.gif" alt="" title=""> </span> <div style="width:146px;" class="infoCheckbox"><div class="checkbox"><input type="checkbox" name="nolimit" value="Yes"></div>' + global["text_no_end_date"] + '</div><span class="cc05"><a onclick="return false;"  class="btnDeleteRow dbShop-ui-appBtn dbShop-ui-adddelete_action" style="margin-left:5px" href="javascript:;" title="Delete"></a></span> </div><div style="clear:both"></div>');
		$(".dayoffContainer").append(html);
		html.find(".datePicker").val(new Date().toString(global["dateFormat"]));
		var maxDate = Date.parse("9999-01-01");
		var minDate = new Date();
		var ignoreDate = getDayoff(true);
		$.each(ignoreDate, function (i, d) {
			if (d.EndDate == null && Date.parse(d.StartDate) < maxDate) {
				maxDate = Date.parse(d.StartDate);
			}
		});
		//设置最小时间
		var _minDate = minDate.toString("yyyy-MM-dd");
		$.each($.grep(ignoreDate, function (d) { return d.EndDate != null; }), function (i, m) {
			if (compareDate(_minDate, m.StartDate) != -1 && compareDate(_minDate, m.EndDate) != 1) {
				_minDate = Date.parse(m.EndDate).addDays(1).toString("yyyy-MM-dd");
			}
			minDate = Date.parse(_minDate);
		});

		//没有可用的日期
		if (minDate >= maxDate) {
			html.remove();
			return false;
		}

		//第一个时间日期选择器
		html.find(".datePicker:eq(0)").datepicker({
			minDate: minDate,
			dateFormat: appointment.common.getDateFormateForDatepicker(),
			maxDate: maxDate,
			onClose: function (selectedDate) {
				var _maxDate = maxDate.toString("yyyy-MM-dd");
				selectedDate = $(this).datepicker("getDate").toString("yyyy-MM-dd");
				$.each($.grep(ignoreDate, function (d) { return d.EndDate != null; }), function (i, m) {
					if (compareDate(selectedDate, m.StartDate) != 1 && compareDate(m.StartDate, _maxDate) != 1) {
						_maxDate = m.StartDate;
					}
				});
				html.find(".datePicker:eq(1)").datepicker("option", { minDate: Date.parse(selectedDate), maxDate: Date.parse(_maxDate) });
			}
		}).datepicker("option", "beforeShowDay", function (date) {
			return [isDispalyDate(date)];
		});
		//第二个日期选择器
		html.find(".datePicker:eq(1)").datepicker({
			dateFormat: appointment.common.getDateFormateForDatepicker(),
			minDate: minDate,
			maxDate: maxDate.addDays(-1)
		}).datepicker("option", "beforeShowDay", function (date) {
			return [isDispalyDate(date)];
		});

		function datePickerChangeEvent() {

		}
		//是否显示当前日期 
		function isDispalyDate(date) {
			var display = true;
			$.each($.grep(ignoreDate, function (d) { return d.EndDate != null; }), function (i, m) {
				var sDate = date.toString("yyyy-MM-dd");
				if (compareDate(sDate, m.StartDate) != -1 && compareDate(sDate, m.EndDate) != 1) {
					display = false;
					return false;
				}
			});
			return display;
		}
	}

	//添加新的Special hour
	function appendNewSpecialHour() {
		var html = $('<div class="listcontent add row-data"><span class="cc_day"> <input type="text" readonly="" value="" size="12" name="specialHoursDate" name="' + appointment.common.newGuid() + '" class="datePicker"><img class="ui-datepicker-trigger" src="../../images/calendar.gif" alt="" title=""> </span><span class="cc_start"><select class="timePicker" date-options="{type:\'am\'}"></select></span><span class="cc_end"><select class="timePicker" date-options="{type:\'pm\'}"> </select></span><span class="cc05"><a onclick="return false;" class="btnDeleteRow dbShop-ui-appBtn dbShop-ui-adddelete_action" style="margin-left:5px" href="javascript:;" title="Delete"></a></span></div><div style="clear:both"></div>');
		$(".specialHoursContainer").append(html);
		html.find(".datePicker").val(new Date().toString(global["dateFormat"]));
		html.find(".datePicker").datepicker({
			dateFormat: appointment.common.getDateFormateForDatepicker(),
			minDate: 0,
			onClose: function (selectedDate) {
				datePickerChangeEvent(selectedDate);
			}
		});
		bindAMDropdown(html.find(".timePicker"));
		html.find(".timePicker:eq(0)").change(function () {
			timePickerChangeEvent($(this));
		});
		datePickerChangeEvent(new Date().toString("yyyy-MM-dd"));
		function datePickerChangeEvent(selectedDate) {
			var ignoreTime = $.grep(getSpecialHours(true), (function (m) { return m.Date == Date.parse(selectedDate).toString("yyyy-MM-dd"); }));

			//忽略的时间提前一个时间段（15分钟)
			for (var i = 0; i < ignoreTime.length; i++) {
				if (Date.parse(ignoreTime[i].StartTime).toString("HH:mm") != "00:00") {
					ignoreTime[i].StartTime = Date.parse(ignoreTime[i].StartTime).toString("HH:mm");
				}
			}
			for (var i = 0; i < ignoreTime.length; i++) {
				ignoreTime[i].EndTime = Date.parse(ignoreTime[i].EndTime).addMinutes(-15).toString("HH:mm");
			}
			ignoreTime.push({ StartTime: "24:00", EndTime: "24:00" });
			setTimeSelecterOptions(html.find(".timePicker:eq(0)"), null, null, ignoreTime);
			timePickerChangeEvent(html.find(".timePicker:eq(0)"));
		}
		function timePickerChangeEvent(t) {
			var val = t.val();
			var nextSelect = html.find(".timePicker:eq(1)");
			var selectedDate = html.find(".datePicker").datepicker("getDate");
			var row = t.parents(".listcontent");
			var ignoreTime = $.grep(getSpecialHours(true), (function (m) { return m.Date == selectedDate.toString("yyyy-MM-dd"); }));
			ignoreTime.push({ StartTime: "00:00", EndTime: "00:00" });
			for (var i = 0; i < ignoreTime.length; i++) {
				ignoreTime[i].StartTime = Date.parse(ignoreTime[i].StartTime).addMinutes(15).toString("HH:mm");
			}
			setTimeSelecterOptions(nextSelect, null, null, ignoreTime);

			var idx = t.find("option").index(t.find("option[value='" + val + "']")); //当前点击option的index
			var mustDisabled = false;
			nextSelect.find("option").each(function (i) {
				if (mustDisabled) {
					$(this).attr("disabled", "disabled");
				} else if (i <= idx) {
					//设置不能点击小于index的option
					$(this).attr("disabled", "disabled");
				} else if ($(this).attr("disabled") == "disabled") {
					mustDisabled = true;
					$(this).attr("disabled", "disabled");
				}
			});
			if (nextSelect.find("option:not([disabled=disabled]):first").length == 0) {
				//没有可选时间段，自动选择下一天
				html.find(".datePicker").datepicker("setDate", selectedDate.addDays(1));
				datePickerChangeEvent(selectedDate.toString("yyyy-MM-dd"));
				return false;
			}
			nextSelect.val(nextSelect.find("option:not([disabled=disabled]):first").val());
		}
	}

	//添加新的Normal hour
	function appendNewNormalHour() {
		var html = $('<div class="listcontent add row-data"><span class="cc_day"><select class="dayPicker"></select></span><span class="cc_start"><select class="timePicker" date-options="{type:\'am\'}"></select></span><span class="cc_end"><select class="timePicker" date-options="{type:\'pm\'}"></select></span><span class="cc05"><a onclick="return false;" class="btnDeleteRow dbShop-ui-appBtn dbShop-ui-adddelete_action" style="margin-left:5px" href="javascript:;" title="Delete"></a></span></div><div style="clear:both"></div>');
		$(".normalHoursContainer").append(html);
		bindAMDropdown(html.find(".timePicker"));
		//html.find(".timePicker:eq(0) option:last").attr("disabled", "disabled");
		html.find(".dayPicker").change(function () {
			dayPickerChangeEvent($(this));
		});
		html.find(".timePicker:eq(0)").change(function () {
			timePickerChangeEvent($(this));
		});
		bindDayDropdown();
		dayPickerChangeEvent(html.find(".dayPicker"));

		function dayPickerChangeEvent(t) {
			var day = t.val();
			var row = t.parents(".listcontent");
			var ignoreTime = $.grep(getNormalHours(true), (function (m) { return m.Day == day; }));

			for (var i = 0; i < ignoreTime.length; i++) {
				if (Date.parse(ignoreTime[i].StartTime).toString("HH:mm") != "00:00") {
					ignoreTime[i].StartTime = Date.parse(ignoreTime[i].StartTime).toString("HH:mm");
				}
			}
			for (var i = 0; i < ignoreTime.length; i++) {
				if (ignoreTime[i].EndTime == "24:00") {
					ignoreTime[i].EndTime = "23:45";
				} else {
					ignoreTime[i].EndTime = Date.parse(ignoreTime[i].EndTime).addMinutes(-15).toString("HH:mm");
				}
			}
			ignoreTime.push({ StartTime: "24:00", EndTime: "24:00" });
			setTimeSelecterOptions(row.find(".timePicker:eq(0)"), null, null, ignoreTime);
			timePickerChangeEvent(html.find(".timePicker:eq(0)"));
		}
		function timePickerChangeEvent(t) {
			var val = t.val();
			var nextSelect = html.find(".timePicker:eq(1)");

			var row = t.parents(".listcontent");
			var ignoreTime = $.grep(getNormalHours(true), (function (m) { return m.Day == row.find(".dayPicker").val(); }));
			//var maxTime = getMinStartTime(ignoreTime);
			for (var i = 0; i < ignoreTime.length; i++) {
				ignoreTime[i].StartTime = Date.parse(ignoreTime[i].StartTime).addMinutes(15).toString("HH:mm");
			}
			ignoreTime.push({ StartTime: "00:00", EndTime: "00:00" });
			setTimeSelecterOptions(nextSelect, null, null, ignoreTime);

			var idx = t.find("option").index(t.find("option[value='" + val + "']")); //当前点击option的index
			var mustDisabled = false;
			nextSelect.find("option").each(function (i) {
				if (mustDisabled) {
					$(this).attr("disabled", "disabled");
				} else if (i <= idx) {
					//设置不能点击小于index的option
					$(this).attr("disabled", "disabled");
				} else if ($(this).attr("disabled") == "disabled") {
					mustDisabled = true;
					$(this).attr("disabled", "disabled");
				}
			});
			if (nextSelect.find("option:not([disabled=disabled]):first").length == 0) {
				//自动选择下一个
				normalHourIgnoreTime++;
				if (normalHourIgnoreTime < 7) { //最多遍历7个
					html.find(".dayPicker").val(appointment.common.toNumber(html.find(".dayPicker").val()) + 1);
					dayPickerChangeEvent(html.find(".dayPicker"));
				} else {
					html.remove();
				}

				return false;
			}
			normalHourIgnoreTime = 0;
			nextSelect.val(nextSelect.find("option:not([disabled=disabled]):first").val());
		}
	}

	//比较时间, 1 大于， -1 小于, 0 等于
	function compareTime(time1, time2) {
		var _time1 = time1.split(":");
		var _time2 = time2.split(":");
		if (_time1[0] == _time2[0] && _time1[1] == _time2[1]) {
			return 0;
		} else if (_time1[0] > _time2[0]) {
			return 1;
		} else if (_time1[0] == _time2[0] && _time1[1] > _time2[1]) {
			return 1;
		}
		return -1;
	}

	//比较日期, 1 大于， -1 小于, 0 等于
	function compareDate(date1, date2) {
		var _date1 = date1.split("-");
		var _date2 = date2.split("-");
		if (_date1[0] == _date2[0] && _date1[1] == _date2[1] && _date1[2] == _date2[2]) {
			return 0;
		}

		if (_date1[0] > _date2[0]) {
			return 1;
		} else if (_date1[0] < _date2[0]) {
			return -1;
		}

		if (_date1[1] > _date2[1]) {
			return 1;
		} else if (_date1[1] < _date2[1]) {
			return -1;
		}

		if (_date1[2] > _date2[2]) {
			return 1;
		} else if (_date1[2] < _date2[2]) {
			return -1;
		}

		return -1;
	}

	//获取最小的开始事件
	function getMinStartTime(timeRangs) {
		var min = "23:59";
		$.each(timeRangs, function (i, n) {
			if (compareTime(n.StartTime, min) == -1) {
				min = n.StartTime;
			}
		});
		return min;
	}

	//设置下拉时间的不可用状态
	function setTimeSelecterOptions(jqObj, minTime, maxTime, ignoreRangs) {
		var hasMinTime = true, hasMaxTime = true, hasIgnoreArray = true;
		if (typeof (maxTime) == "undefined" || maxTime == null) {
			hasMaxTime = false;
			maxTime = "00:00"
		}
		if (typeof (minTime) == "undefined" || minTime == null) {
			hasMinTime = false;
			minTime = "23:00" + (60 - ddlMinutesDuration);
		}
		if (typeof (ignoreRangs) == "undefined" || ignoreRangs == null || ignoreRangs.length == 0) {
			hasIgnoreArray = false;
		}
		//激活所有option
		jqObj.find("option[disabled=disabled]").removeAttr("disabled");
		if (hasMaxTime || hasMinTime) {
			jqObj.find("option[disabled!=disabled]").filter(function (i, m) {
				var curTime = $(m).val();
				if (compareTime(curTime, minTime) != 1 || compareTime(curTime, maxTime) != -1) {
					return true;
				}
			}).attr("disabled", "disabled");
		}
		if (hasIgnoreArray) {
			jqObj.find("option[disabled!=disabled]").filter(function () {
				var curTime = $(this).val();
				var disabled = false;
				$.each(ignoreRangs, function (i, r) {
					if (compareTime(curTime, r.StartTime) != -1 && compareTime(curTime, r.EndTime) != 1) {
						disabled = true;
						return false;
					}
				});
				return disabled;
			}).attr("disabled", "disabled");
		}
		//选择第一个
		jqObj.find("option[disabled!=disabled]:first").attr("selected", "selected");
	}

	function bindDayDropdown() {
		var htm = '<option value="{0}" style="padding-left:0px;">{1}</option> ';
		$("select.dayPicker").filter(function () { return !$(this).hasClass("hasDayPicker"); }).each(function () {
			for (var i = 0; i < 7; i++) {
				$(this).append(htm.format(i, appointment.translateDay(i)))
			}
			$(this).addClass("hasDayPicker");
		});
	}

	function bindAMDropdown(obj) {
		var htm = '<option value="{0}" style="padding-left:0px;">{1}</option> ';
		obj.each(function () {
			var startDate = new Date().clearTime();
			var endDate = new Date().clearTime().at({ hour: 23, minute: 59 });
			do {
				var val = startDate.toString("HH:mm");
				$(this).append(htm.format(val, appointment.translateTime(val)));
				startDate.addMinutes(ddlMinutesDuration);
			} while (startDate <= endDate)
			$(this).append(htm.format("24:00", appointment.translateTime("24:00"))); //允許24點
			$(this).addClass("hasTimePicker");
		});
	}

	function getResourceDetail() {
		var detail = {
			ResourceGuid: global.resourceId,
			Name: $.trim($("#name").val()),
			PhoneNumber: $.trim($("#phoneNumber").val()),
			Email: $.trim($("#email").val()),
			AssignColor: $("#assignColor").val(),
			Description: tinyMCE.get()[0].getContent(),
			/*
			(function () {
			var content = tinyMCE.get()[0].getContent();
			content = content.substr(3);
			content = content.substr(0, content.length - 4);
			return content
			})(),*/
			Services: [],
			NormalHours: [],
			SpecialHours: [],
			DayOff: []
		}

		//get services
		$(".providedService :checkbox").filter(function () { return $(this).is(":checked") }).each(function () {
			detail.Services.push({ ServiceGuid: $(this).attr("id") });
		});

		//get normal hours
		detail.NormalHours = getNormalHours();

		//get special hours
		detail.SpecialHours = getSpecialHours();

		//get day off
		detail.DayOff = getDayoff();
		return detail;
	}

	function getNormalHours(onlyAdded) {
		var normalHours = [];
		$(".normalHoursContainer .row-data").filter(function () { return !$(this).hasClass("add"); }).each(function () {
			normalHours.push({ Day: $(this).attr("day"), StartTime: $(this).attr("starttime"), EndTime: $(this).attr("endtime") });
		});
		if (typeof (onlyAdded) == "undefined") {
			onlyAdded = false;
		}

		if (onlyAdded == false) {
			$(".normalHoursContainer .row-data").filter(function () { return $(this).hasClass("add"); }).each(function () {
				normalHours.push({ Day: $(this).find(".cc_day select").val(), StartTime: $(this).find(".cc_start select").val(), EndTime: $(this).find(".cc_end select").val() });
			});
		}
		return normalHours;
	}

	function getSpecialHours(onlyAdded) {
		var specialHours = [];
		$(".specialHoursContainer .row-data").filter(function () { return !$(this).hasClass("add"); }).each(function () {
			specialHours.push({ Date: $(this).attr("date"), StartTime: $(this).attr("starttime"), EndTime: $(this).attr("endtime") });
		});
		if (typeof (onlyAdded) == "undefined") {
			onlyAdded = false;
		}
		if (onlyAdded == false) {
			$(".specialHoursContainer .row-data").filter(function () { return $(this).hasClass("add"); }).each(function () {
				specialHours.push({ Date: $(this).find(".cc_day .datePicker").datepicker("getDate").toString("yyyy-MM-dd"), StartTime: $(this).find(".cc_start select").val(), EndTime: $(this).find(".cc_end select").val() });
			});
		}
		return specialHours;
	}

	function getDayoff(onlyAdded) {
		var dayOff = [];
		$(".dayoffContainer .row-data").filter(function () { return !$(this).hasClass("add"); }).each(function () {
			dayOff.push({ StartDate: $(this).attr("startdate"), EndDate: $(this).attr("enddate") == "null" ? null : $(this).attr("enddate") });
		});
		if (typeof (onlyAdded) == "undefined") {
			onlyAdded = false;
		}
		if (onlyAdded == false) {
			$(".dayoffContainer .row-data").filter(function () { return $(this).hasClass("add"); }).each(function () {
				isNoLimit = $(this).find(":checkbox").eq(0).is(":checked");
				dayOff.push({ StartDate: $(this).find(".cc_day .datePicker").datepicker("getDate").toString("yyyy-MM-dd"), EndDate: isNoLimit ? null : $(this).find(".cc_start .datePicker").datepicker("getDate").toString("yyyy-MM-dd") });
			});
		}
		//sort it
		var temp;
		for (var i = 0; i < dayOff.length - 1; i++) {
			for (var j = 0; j < dayOff.length - i - 1; j++) {
				if (compareDate(dayOff[j].StartDate, dayOff[j + 1].StartDate) == 1) {
					temp = dayOff[j];
					dayOff[j] = dayOff[j + 1];
					dayOff[j + 1] = temp;
				}
			}
		}
		return dayOff;
	}
	window.a = getDayoff;
});