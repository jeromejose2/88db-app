﻿$.extend(window.appointment.ajax.ajaxOptions.ajaxUrl, {
	switchServiceStatus: "/admin/service/switchServiceStatus?cid=" + global.cid,
	saveResource: "/admin/resources/saveResource?cid=" + global.cid,
	saveSetting: "/admin/setting/update?cid=" + global.cid,
	saveGreeting: "/admin/greeting/update?cid=" + global.cid,
	getCustomerMoreDetail: "/admin/customer/getCustomerMoreDetail?cid=" + global.cid,
	cancelAppointment: "/admin/customer/cancelAppointment?cid=" + global.cid,
	getPrevAppointments: "/admin/customer/getPrevAppointments?cid=" + global.cid,
	sendCustomerMessage: "/admin/customer/sendCustomerMessage?cid=" + global.cid,
	addCustomerNote: "/admin/customer/addCustomerNote?cid=" + global.cid,
	deleteCustomerNote: "/admin/customer/deleteCustomerNote?cid=" + global.cid,
	getCustomerNotes: "/admin/customer/getCustomerNotes?cid=" + global.cid,
	getConditionalAppointments: "/admin/calendar/getConditionalAppointments?cid=" + global.cid,
	getSameScheduleResources: "/admin/calendar/getSameScheduleResources?cid=" + global.cid,
	updateAppointmentResouce: "/admin/calendar/updateAppointmentResouce?cid=" + global.cid,
	changeServicesWeight: "/admin/service/changeServicesWeight?cid=" + global.cid,
	getAvailableServicesNameList: "/service/getAvailableServicesNameList?cid=" + global.cid,
	getAvailableResourceNameListByServiceGuid: "/resource/getAvailableResourceNameListByServiceGuid?cid=" + global.cid,
	updateAppointment: "/appointment/updateAppointment?cid=" + global.cid,
	createAppointment: "/appointment/createAppointment?cid=" + global.cid,
	getCustomerNameList: "/admin/customer/getCustomerNameList?cid=" + global.cid
});
$.extend(window.appointment, {
	switchServiceStatus: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			fn = $.noop;
			async = false;
		}
		var options = $.extend({
			async: async,
			serviceId: "",
			status: "A" //A or I
		},
		options);
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.switchServiceStatus, options, function (r) {
			fn(r);
		});
		return result;
	},
	saveResource: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			fn = $.noop;
			async = false;
		}
		var options = $.extend({
			async: async,
			detail: {}
		},
		options);
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.saveResource, options, function (r) {
			fn(r);
		});
		return result;
	},
	saveSetting: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			fn = $.noop;
			async = false;
		}
		var options = $.extend({
			async: async,
			setting: {}
		},
		options);
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.saveSetting, options, function (r) {
			fn(r);
		});
		return result;
	},
	saveGreeting: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			fn = $.noop;
			async = false;
		}
		var options = $.extend({
			async: async,
			greeting: {}
		},
		options);
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.saveGreeting, options, function (r) {
			fn(r);
		});
		return result;
	},
	getCustomerMoreDetail: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			fn = $.noop;
			async = false;
		}
		var options = $.extend({
			async: async,
			customerGuid: ""
		},
		options);
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.getCustomerMoreDetail, options, function (r) {
			fn(r);
		});
		return result;
	},
	cancelAppointment: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			fn = $.noop;
			async = false;
		}
		var options = $.extend({
			async: async,
			customerGuid: "",
			bookingGuid: "",
			reason: ""
		},
		options);
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.cancelAppointment, options, function (r) {
			fn(r);
		});
		return result;
	},
	getPrevAppointments: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			fn = $.noop;
			async = false;
		}
		var options = $.extend({
			async: async,
			pageSize: 12,
			pageNumber: 1,
			customerGuid: ""
		},
		options);
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.getPrevAppointments, options, function (r) {
			fn(r);
		});
		return result;
	},
	sendCustomerMessage: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			fn = $.noop;
			async = false;
		}
		var options = $.extend({
			async: async,
			message: "",
			customerName: "",
			customerGuid: ""
		},
		options);
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.sendCustomerMessage, options, function (r) {
			fn(r);
		});
		return result;
	},
	addCustomerNote: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			fn = $.noop;
			async = false;
		}
		var options = $.extend({
			async: async,
			message: "",
			customerGuid: ""
		},
		options);
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.addCustomerNote, options, function (r) {
			fn(r);
		});
		return result;
	},
	getCustomerNameList: function(options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			fn = $.noop;
			async = false;
		}
		var options = $.extend({
			async: async
		},
		options);
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.getCustomerNameList, options, function (r) {
			fn(r);
		});
		return result;
	},
	deleteCustomerNote: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			fn = $.noop;
			async = false;
		}
		var options = $.extend({
			async: async,
			noteGuid: ""
		},
		options);
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.deleteCustomerNote, options, function (r) {
			fn(r);
		});
		return result;
	},
	getCustomerNotes: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			fn = $.noop;
			async = false;
		}
		var options = $.extend({
			async: async,
			customerGuid: "",
			pageSize: 12,
			pageNumber: 1
		},
		options);
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.getCustomerNotes, options, function (r) {
			fn(r);
		});
		return result;
	},
	getConditionalAppointments: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			fn = $.noop;
			async = false;
		}
		var options = $.extend({
			async: async,
			serviceGuid: "",
			resourceGuid: "",
			startDate: "",
			endDate: ""
		},
		options);
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.getConditionalAppointments, options, function (r) {
			fn(r);
		});
		return result;
	},
	getSameScheduleResources: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			fn = $.noop;
			async = false;
		}
		var options = $.extend({
			async: async,
			serviceGuid: "",
			resouceGuid: "",
			schedule: ""
		},
		options);
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.getSameScheduleResources, options, function (r) {
			fn(r);
		});
		return result;
	},
	updateAppointmentResouce: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			fn = $.noop;
			async = false;
		}
		var options = $.extend({
			async: async,
			serviceGuid: "",
			resouceGuid: "",
			customerGuid: "",
			schedule: "",
			bookingGuid: ""
		},
		options);
		var result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.updateAppointmentResouce, options, function (r) {
			fn(r);
		});
		return result;
	},
	changeServicesWeight: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			fn = $.noop;
			async = false;
		}
		var options = $.extend({
			async: async,
			services: [],
			action: ""
		},
		options);
		var result
		result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.changeServicesWeight, options, fn);
		return result;
	},
	getAvailableServicesNameList: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			fn = $.noop;
			async = false;
		}
		var options = $.extend({
			async: async,
			action: ""
		},
		options);
		var result
		result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.getAvailableServicesNameList, options, fn);
		return result;
	},
	getAvailableResourceNameListByServiceGuid: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			fn = $.noop;
			async = false;
		}
		var options = $.extend({
			async: async,
			serviceGuid: "",
			action: ""
		},
		options);
		var result
		result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.getAvailableResourceNameListByServiceGuid, options, fn);
		return result;
	},
	updateAppointment: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			fn = $.noop;
			async = false;
		}
		var options = $.extend({
			async: async,
			serviceGuid: "",
			resouceGuid: "",
			schedule: "",
			customerGuid: "",
			bookingGuid: ""
		},
		options);
		var result
		result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.updateAppointment, options, fn);
		return result;
	},
	createAppointment: function (options, fn) {
		var me = this;
		var async = true;
		if (typeof (fn) == "undefined") {
			fn = $.noop;
			async = false;
		}
		var options = $.extend({
			async: async,
			type: "existing",
			customerGuid: "",
			serviceGuid : "",
			resourceGuid: "",
			schedule: "",
			customerName: ""
		},
		options);
		var result
		result = this.ajax.post(this.ajax.ajaxOptions.ajaxUrl.createAppointment, options, fn);
		return result;
	}
});
window.appointment.dialog.prototype.init = function (options) {
	this.options = $.extend({
		cls: "dbShop-ui-nlayer", // fe : dbShop-ui-appLayer, bo:dbShop-ui-nlayer
		titleTag: "h2",
		title: "",
		content: "content",
		buttons: [] //text, onclick, cls
	},
		options);
	this.setup();
}
window.appointment.paging = function (options) {
	this.init(options);
}
window.appointment.paging.prototype = {
	init: function (options) {
		this.options = $.extend({
			pageSize: 12,
			pageNumber: 1,
			appendTo: null,
			total: 100,
			onclick: function (page) { }
		}, options);
		this.begin();
	},
	begin: function () {
		var t = this;
		if (this.options.appendTo == null) {
			return;
		}
		this.currentPage = appointment.common.toNumber(this.options.pageNumber);
		this.totalPages = Math.ceil(this.options.total / this.options.pageSize);
		this.preBtn = $('<a class="previous" href="javascript:;">&lt;&lt;</a>');
		this.nextBtn = $('<a class="next" href="javascript:;">&gt;&gt;</a>');
		this.resetPageDisplay();
	},
	goPage: function (page) {
		this.currentPage = page;
		this.options.onclick.call(this, page);
		this.resetPageDisplay();
	},
	resetPageDisplay: function () {
		var t = this;
		this.options.appendTo.empty();

		this.options.appendTo.append(this.preBtn);
		for (var i = 1; i <= this.totalPages; i++) {
			var cls = (i == this.totalPages ? "last" : "");
			if (i == this.currentPage) {
				this.options.appendTo.append('<strong page="' + i + '" class="' + cls + '">' + i + '</strong>');
			} else {
				var page = $('<a page="' + i + '" href="javascript:;" class="' + cls + '">' + i + '</a>');
				(function (p) {
					p.click(function () {
						t.goPage(appointment.common.toNumber(p.attr("page")));
					});
				})(page);
				this.options.appendTo.append(page);
			}
		}
		this.options.appendTo.append(this.nextBtn);

		if (this.currentPage == 1) {
			this.preBtn.hide();
		} else {
			this.preBtn.show();
		}
		if (this.currentPage == this.totalPages) {
			this.nextBtn.hide();
		} else {
			this.nextBtn.show();
		}

		this.preBtn.click(function () {
			t.goPage(t.currentPage - 1)
		});
		this.nextBtn.click(function () {
			t.goPage(t.currentPage + 1)
		});
	},
	reset: function (options) {
		$.extend(this.options, options);
		this.totalPages = Math.ceil(this.options.total / this.options.pageSize);
		this.currentPage = appointment.common.toNumber(this.options.pageNumber);
		this.resetPageDisplay();
		return this;
	}
};
$.extend(window.appointment.dialog, {
	bo: {
		color: {
			grey: "msBtn dbShop-ui-grey",
			blue: "msBtn dbShop-ui-red",
			red: "dbShop-ui-sendBtn"
		},
		systemError: function () {
			new window.appointment.dialog({
				title: global["dialog_system_error_title"],
				content: global["dialog_system_error_content"],
				buttons: [{
					text: global.cancel,
					cls: appointment.dialog.bo.color.grey,
					onclick: function () {
						this.destory();
					}
				}]
			});
		},
		deleteConfirm: function (title, msg, onOk, clickObject) {
			new window.appointment.dialog({
				title: title,
				content: msg,
				clickObject: clickObject,
				buttons: [{
					text: global["delete"],
					cls: appointment.dialog.bo.color.blue,
					onclick: function () {
						if (typeof (onOk) != "undefined") {
							onOk.apply(this);
						}
					}
				},
				{
					text: global.cancel,
					cls: "msBtn dbShop-ui-grey",
					onclick: function () {
						this.destory();
					}
				}]
			});
		},
		deleteAppointmentConfirm: function (options) {
			this.options = $.extend({
				clickObject: null,
				title: "Delete Appointment",
				tips: "Are you sure you want to cancel the following appointment?",
				customerLabel: "Customer",
				scheduleLabel: "Schedule",
				serviceLabel: "Service",
				resourceLabel: "Resource",
				costLabel: "Cost",
				reasonTitleLabel: "Reason for delete",
				reasonMessageLabel: "Message will appear in customer's notification email",
				textTips: " Please leave message here",
				successDialogTitle: "Appointment cancelled",
				successDialogContent: "The appointment has been canceled successfully.",
				customerName: "",
				serviceName: "",
				schedule: "",
				resourceName: "",
				cost: "0",
				onOK: function () { },
				onCancel: function () { }
			}, options);
			var contentHtml = '' +
					'<p>' + this.options.tips + '</p>' +
					'<div style="margin-top:0px" class="form_container_a_pop">		' +
					'<div class="form_title_pop">' + this.options.customerLabel + ':</div>' +
					'<div class="form_info_pop">' + this.options.customerName + '</div>' +
					'</div>	' +
					'<div class="dbShop-ui-clearboth"></div>' +
					'<div class="form_container_b_pop">		' +
					'<div class="form_title_pop">' + this.options.scheduleLabel + ':</div>' +
					'<div class="form_info_pop">' + this.options.schedule + '</div>' +
					'</div>	' +
					'<div class="dbShop-ui-clearboth"></div>' +
					'<div class="form_container_b_pop">	' +
					'<div style="*padding-top:10px" class="form_title_pop">' + this.options.serviceLabel + ':</div>' +
					'<div class="form_info_pop">' + this.options.serviceName + '</div>' +
					'</div>	' +
					'<div class="dbShop-ui-clearboth"></div>' +
					'<div style="padding-bottom:40px" class="form_container_b_pop">	' +
					'<div style="*padding-top:10px" class="form_title_pop">' + this.options.resourceLabel + ':</div>' +
					'<div class="form_info_pop">' + this.options.resourceName + '</div>' +
					'</div>	' +
			//'<div class="dbShop-ui-clearboth"></div>' +
			//'<div style="padding-bottom:40px" class="form_container_b_pop">	' +
			//'<div style="*padding-top:10px" class="form_title_pop">' + this.options.costLabel + ':</div>' +
			//'<div class="form_info_pop">' + this.options.cost + '</div>' +
			//'</div>	' +
					'<div class="dbShop-ui-clearboth"></div>' +
					'<div class="form_container_b_pop">	' +
				 	'<p style="font-weight:bold; padding-bottom:5px;">' + this.options.reasonTitleLabel + '</p>' +
					'<p>(' + this.options.reasonMessageLabel + ')</p>' +
					'<textarea style="color:#777777;" maxlength="1000" class="txtArea deleteAppointmentConfirmReason" placeholder="' + this.options.textTips + '"></textarea></div>';
			var t = this;
			new window.appointment.dialog({
				title: this.options.title,
				content: contentHtml,
				clickObject: this.options.clickObject,
				buttons: [{
					text: global["confirm"],
					cls: appointment.dialog.bo.color.blue,
					onclick: function () {
						if (appointment.common.getValidation($(".deleteAppointmentConfirmReason:visible"))) {
							var value = $(".deleteAppointmentConfirmReason:visible").val();
							$(".deleteAppointmentConfirmReason:visible").hideErrorMessage();
							this.destory();
							new appointment.dialog.bo.info(t.options.successDialogTitle, t.options.successDialogContent, function () { }, t.options.clickObject);
							t.options.onOK.apply(this, [value]);
						}
					}
				},
				{
					text: global["cancel"],
					cls: "msBtn dbShop-ui-grey",
					onclick: function () {
						$(".deleteAppointmentConfirmReason:visible").hideErrorMessage();
						this.destory();
						t.options.onCancel.apply(this);
					}
				}]
			});

			$(".deleteAppointmentConfirmReason:visible").setValidation({
				required: true,
				missingMessage: global["text_please_enter_valid_value"]
			}).setPlaceHolder();
		},
		confirm: function (msg, title, onOk, clickObject) {
			new window.appointment.dialog({
				title: title,
				content: msg,
				clickObject: clickObject,
				buttons: [{
					text: global.confirm,
					cls: appointment.dialog.bo.color.blue,
					onclick: function () {
						if (typeof (onOk) != "undefined") {
							onOk.apply(this);
						}
					}
				},
				{
					text: global.cancel,
					cls: "msBtn dbShop-ui-grey",
					onclick: function () {
						this.destory();
					}
				}]
			});
		},
		info: function (title, msg, onOk, clickObject) {
			new window.appointment.dialog({
				title: title,
				content: msg,
				clickObject: clickObject,
				buttons: [{
					text: global.close,
					cls: appointment.dialog.bo.color.blue,
					onclick: function () {
						this.destory();
						if (typeof (onOk) != "undefined") {
							onOk.apply(this);
						}
					}
				}]
			});
		},
		loading: function (clickObject, msg) {
			window.loadingDialog = new window.appointment.dialog({
				title: '',
				content: msg || global["text_loading"],
				clickObject: clickObject
			});
		},
		closeLoading: function () {
			if (window.loadingDialog) {
				window.loadingDialog.destory();
				window.loadingDialog = null;
			}
		},
		sendMessage: function (options) {
			this.options = $.extend({
				clickObject: null,
				title: "Send Message To Customer",
				customerLabel: "Customer",
				messageLabel: "Message",
				textTips: " Please leave message here",
				customerName: "",
				confirmDialogTitle: "Send message confimation",
				confirmDialogSubTitle: "Are you sure you want to send this message?",
				onOK: function () { }
			}, options);
			var contentHtml = '' +
					'<div style="margin-top:0px" class="form_container_a_pop">		' +
					'<div class="form_title_pop">' + this.options.customerLabel + ':</div>' +
					'<div class="form_info_pop">' + this.options.customerName + '</div>' +
					'</div>	' +
					'<div class="dbShop-ui-clearboth"></div>' +
					'<div class="form_container_b_pop">		' +
					'<div class="form_title_pop">' + this.options.messageLabel + ':</div>' +
					'</div>	' +
					'<div class="dbShop-ui-clearboth"></div>' +
					'<div class="form_container_b_pop">	' +
					'<textarea style="color:#777777;" class="txtArea sendMessageDialogContent" maxlength="2500" placeholder="' + this.options.textTips + '"></textarea></div>';
			var t = this;
			new window.appointment.dialog({
				title: this.options.title,
				content: contentHtml,
				clickObject: this.options.clickObject,
				buttons: [{
					text: global["send"],
					cls: appointment.dialog.bo.color.blue,
					onclick: function () {
						if (appointment.common.getValidation($(".sendMessageDialogContent"))) {
							var message = $(".sendMessageDialogContent:visible").val();
							$(".sendMessageDialogContent").hideErrorMessage();
							var sendMessageDialog = this;
							sendMessageDialog.hide();
							new appointment.dialog.bo.confirmSendMessage({
								title: t.options.confirmDialogTitle,
								subTitle: t.options.confirmDialogSubTitle,
								message: message,
								customerLabel: t.options.customerLabel,
								messageLabel: t.options.messageLabel,
								clickObject: this.options.clickObject,
								customerName: t.options.customerName,
								onCancel: function () {
									sendMessageDialog.show();
								},
								onOK: function (msg) {
									sendMessageDialog.destory();
									t.options.onOK.call(this, msg);
								}
							});
						}
					}
				},
				{
					text: global["cancel"],
					cls: "msBtn dbShop-ui-grey",
					onclick: function () {
						$(".sendMessageDialogContent").hideErrorMessage();
						this.destory();
					}
				}]
			});
			$(".sendMessageDialogContent").setValidation({
				required: true,
				missingMessage: global["text_please_enter_valid_value"]
			}).setPlaceHolder();
		},
		confirmSendMessage: function (options) {
			this.options = $.extend({
				clickObject: null,
				title: "Send message confimation",
				subTitle: "Are you sure you want to send this message?",
				customerLabel: "Customer",
				messageLabel: "Message",
				message: "",
				customerName: "",
				sendButtonText: global["send"],
				cancelButtonText: global["cancel"],
				onOK: function () { },
				onCancel: function () { }
			}, options);
			var contentHtml = '' +
					'<p>' + this.options.subTitle + '</p>' +
					'<div class="form_title_pop">' + this.options.customerLabel + ':</div>' +
					'<div class="form_info_pop">' + this.options.customerName + '</div>' +
					'<div class="form_title_pop">' + this.options.messageLabel + ':</div>' +
					'<div class="form_info_pop">' + this.options.message + '</div>' +
					'<div class="dbShop-ui-clearboth"></div>';
			var t = this;
			new window.appointment.dialog({
				title: this.options.title,
				content: contentHtml,
				clickObject: this.options.clickObject,
				buttons: [{
					text: this.options.sendButtonText,
					cls: appointment.dialog.bo.color.blue,
					onclick: function () {
						this.destory();
						t.options.onOK.call(this, t.options.message);
					}
				},
				{
					text: this.options.cancelButtonText,
					cls: "msBtn dbShop-ui-grey",
					onclick: function () {
						this.destory();
						t.options.onCancel.call(this);
					}
				}]
			});
		},
		addCustomerNote: function (options) {
			this.options = $.extend({
				clickObject: null,
				title: "Add Notes",
				textTips: " Please leave message here",
				onOK: function () { }
			}, options);
			var contentHtml = '' +
					'<div style="margin-top:-1px;" class="form_container_b_pop">	' +
					'<textarea style="color:#777777;" class="txtArea addCustomerNoteDialogMessage" maxlength="1000" placeholder="' + this.options.textTips + '"></textarea></div>';
			var t = this;
			new window.appointment.dialog({
				title: this.options.title,
				content: contentHtml,
				clickObject: this.options.clickObject,
				buttons: [{
					text: global["text_add"],
					cls: appointment.dialog.bo.color.blue,
					onclick: function () {
						if (appointment.common.getValidation($(".addCustomerNoteDialogMessage"))) {
							t.options.onOK.apply(this, [$(".addCustomerNoteDialogMessage:visible").val()]);
							$(".addCustomerNoteDialogMessage").hideErrorMessage();
							this.destory();
						}
					}
				},
				{
					text: global["cancel"],
					cls: "msBtn dbShop-ui-grey",
					onclick: function () {
						$(".addCustomerNoteDialogMessage").hideErrorMessage();
						this.destory();
					}
				}]
			});
			$(".addCustomerNoteDialogMessage").setValidation({
				required: true,
				missingMessage: global["text_please_enter_valid_value"]
			}).setPlaceHolder();
		},
		updateAppointment: function (options) {
			this.options = $.extend({
				isSingleName: global["singleName"] == "enable",
				clickObject: null,
				serviceLabel: "Service",
				scheduleLabel: "Schedule",
				resourceLabel: "Resource",
				customerLabel: "Customer",
				phoneLabel: "Phone",
				emailLabel: "Email",
				timeLabel: "Time",
				firstNameLabel: "First",
				lastNameLabel: "Last",
				sourceOfAppointmentLable: "Source of appointment",
				cancelAppointmentLable: "Cancel This Appointment",
				successDialogTitle: "Appointment cancelled",
				successDialogContent: "The appointment has been canceled successfully.",
				confirmUpdateDialogMessage: "Are you sure you want to update the following appointment?",
				confirmUpdateDialogTitle: "Confirm update appointment",
				confirmCreateDialogMessage: "Are you sure you want to create the following appointment?",
				confirmCreateDialogTitle: "Confirm create appointment",
				phone: "",
				email: "",
				source: "",
				source_phone: "Phone",
				source_email: "Email",
				source_walkin: "Walk-in",
				source_others: "Others",
				existingCustomerLabel: "Existing customer",
				newCustomerLabel: "New customer",
				typeLabel: "Type",
				serviceName: "",
				schedule: "",
				resourceName: "",
				serviceGuid: "",
				customerName: "",
				customerGuid: "",
				resouceGuid: "",
				startDateTime: "",
				bookingGuid: "",
				cost: 0,
				serverDate: new Date(),
				title: "Create appointment",
				createText: "Create Appointment",
				selectResourceText: "Select Resource",
				noSchemaLabel: "No Schema",
				resourceData: [],
				dateLable: 'Date',
				avaliableTimeLabel: 'Avaliable Time',
				newScheduleLabel: 'New Schedule',
				selectServiceText: 'Select Service',
				selectResourceText: 'Select Resource',
				onDelete: function () { },
				onCancel: function () { },
				onUpdate: function (result) { },
				onCreate: function (result) { }
			}, options);
			var t = this;
			var buttons = [];
			var resoucesCache = {};
			var isUpdate = t.options.bookingGuid.length > 0;
			var isCreate = !isUpdate;
			var isCreateByCustomer = t.options.customerGuid.length > 0;
			var isOnlineAppointment = t.options.source.length == 0;
			var isOfflineAppointment = !isOnlineAppointment;
			var customersInfo = [];
			t.schema = null;
			var contentHtml = '<div>';

			//Type
			if (isCreate) {
				if (!isCreateByCustomer) {
					contentHtml += ("   <div style=\"margin-top:0px\" class=\"form_container_a_pop\"> ")
					+ ("    <div class=\"form_title_pop\">")
					+ ("     " + this.options.typeLabel + ":")
					+ ("    </div> ")
					+ ("  <div style=\"padding-bottom:5px\" class=\"form_info_pop container_type\"> ")
					+ ("   <span style=\"position:relative; padding-left:25px;\"> <input type=\"radio\" name=\"appointmentType\" value=\"existing\" style=\"position:absolute;margin-top:1px;left:0px;\" />" + this.options.existingCustomerLabel + "</span> ")
					+ ("   <span style=\"position:relative;padding-left:25px;\"> <input type=\"radio\" checked=\"checked\" name=\"appointmentType\" value=\"new\" style=\"position:absolute;margin-top:1px;left:0px;\" />" + this.options.newCustomerLabel + "</span> ")
					+ ("  </div>")
					+ ("   </div> ");
				}

				//name phone email inputs
				contentHtml += ("  <div style=\"margin-top:0px\" class=\"form_container_a_pop container_type_new\"> ")
				+ ("   <div class=\"form_title_pop\">")
				+ ("    <span class=\"compulsory-s\">*</span>" + this.options.firstNameLabel + ":")
				+ ("   </div> ")
				+ ("   <div style=\"padding-bottom:5px;\" class=\"form_info_pop\">")
				+ ("    <input type=\"text\" class=\"firstName\" size=\"35\" value=\"\" />")
				+ ("   </div> ")
				+ ("  </div>")
				+ ("	<div class=\"dbShop-ui-clearboth\"></div>");

				if (!this.options.isSingleName) {
					contentHtml += ("  <div style=\"margin-top:0px\" class=\"form_container_a_pop container_type_new\"> ")
					+ ("   <div class=\"form_title_pop\">")
					+ ("    <span class=\"compulsory-s\">*</span>" + this.options.lastNameLabel + ":")
					+ ("   </div> ")
					+ ("   <div style=\"padding-bottom:5px;\" class=\"form_info_pop\">")
					+ ("    <input type=\"text\" class=\"lastName\" size=\"35\" value=\"\" />")
					+ ("   </div> ")
					+ ("  </div>")
					+ ("  <div class=\"dbShop-ui-clearboth\"></div>");
				};
				contentHtml += ("  <div style=\"margin-top:0px\" class=\"form_container_a_pop  container_type_new\"> ")
				+ ("   <div class=\"form_title_pop\">")
				+ ("    <span class=\"compulsory-s\">*</span> " + this.options.phoneLabel + ":")
				+ ("   </div> ")
				+ ("   <div style=\"padding-bottom:5px\" class=\"form_info_pop\">")
				+ ("    <input type=\"text\" class=\"phone\" size=\"35\" value=\"\" />")
				+ ("   </div> ")
				+ ("  </div>")

				+ ("  <div class=\"dbShop-ui-clearboth\"></div>")
				+ ("  <div style=\"margin-top:0px\" class=\"form_container_a_pop  container_type_new\"> ")
				+ ("   <div class=\"form_title_pop\">")
				+ ("     " + this.options.emailLabel + ":")
				+ ("   </div> ")
				+ ("   <div style=\"padding-bottom:5px;\" class=\"form_info_pop\">")
				+ ("    <input type=\"text\" class=\"email\" size=\"35\" value=\"\" />")
				+ ("   </div> ")
				+ ("  </div>");
			}
			contentHtml += ("   <div style=\"margin-top:0px\" class=\"form_container_a_pop  container_type_existing\"> ")
				+ ("    <div class=\"form_title_pop\">")
				+ ("     " + this.options.customerLabel + ":")
				+ ("    </div> ")
				+ ("    <div style=\"padding-bottom:5px\" class=\"form_info_pop container_customer_name\">")
			//+ ("     <input type=\"text\" size=\"29\" value=\"\" />")
				+ ("    </div> ")
				+ ("   </div> ");

			if (this.options.phone.length > 0 || isCreate) {
				//phone number
				contentHtml += ("   <div style=\"margin-top:0px;display:" + ((isCreate && !isCreateByCustomer) ? "none" : "block") + ";\" class=\"form_container_a_pop  container_type_existing\"> ")
				+ ("    <div class=\"form_title_pop\">")
				+ ("     " + this.options.phoneLabel + ":")
				+ ("    </div> ")
				+ ("    <div style=\"padding-bottom:5px\" class=\"form_info_pop container_phone\">" + (this.options.phone.length > 0 ? this.options.phone : "--"))
				+ ("    </div> ")
				+ ("   </div> ");
			}
			//email
			if (this.options.email.length > 0 || isCreate) {
				contentHtml += ("   <div style=\"margin-top:0px;display:" + ((isCreate && !isCreateByCustomer) ? "none" : "block") + ";\" class=\"form_container_a_pop  container_type_existing\"> ")
				+ ("    <div class=\"form_title_pop\">")
				+ ("     " + this.options.emailLabel + ":")
				+ ("    </div> ")
				+ ("    <div style=\"padding-bottom:5px\" class=\"form_info_pop container_email\">" + (this.options.email.length > 0 ? this.options.email : "--"))
				+ ("    </div> ")
				+ ("   </div> ");
			}

			//service
			contentHtml += ("   <div style=\"margin-top:0px\" class=\"form_container_a_pop\"> ")
				+ ("    <div class=\"form_title_pop\">")
				+ ("     " + this.options.serviceLabel + ":")
				+ ("    </div> ")
				+ ("    <div style=\"padding-bottom:5px\" class=\"form_info_pop container_service_selecter\">")
			//+ ("     <select style=\"margin:0px 0px;width:200px;\" name=\"category_id\"> <option style=\"font-weight:bold;\" selected=\"selected\" value=\"94\"> Spa-Pamper</option> </select>")
				+ ("    </div> ")
				+ ("   </div> ")

			//resouce
				+ ("   <div style=\"margin-top:0px\" class=\"form_container_a_pop\"> ")
				+ ("    <div class=\"form_title_pop\">")
				+ ("     " + this.options.resourceLabel + ":")
				+ ("    </div> ")
				+ ("    <div style=\"padding-bottom:5px\" class=\"form_info_pop container_resource_selecter\">")
			//+ ("     <select style=\"margin:0px 0px;width:200px;\" name=\"category_id\"> <option style=\"font-weight:bold;\" selected=\"selected\" value=\"94\"> Johnny</option> </select>")
				+ ("    </div> ")
				+ ("   </div> ")
				+ ("   <div class=\"dbShop-ui-clearboth\"></div> ")

			//date
				+ ("   <div class=\"form_container_b_pop\"> ")
				+ ("    <div class=\"form_title_pop\">")
				+ ("     " + this.options.dateLable + ":")
				+ ("    </div> ")
				+ ("    <div style=\"padding-bottom:5px; width:200px;\" class=\"form_info_pop container_calendar\"> ")
			//+ ("     <input type=\"text\" value=\"11 / 11 / 2013\" size=\"10\" /> ")
			//+ ("     <a class=\"ico_calendarpop\"><img width=\"20\" height=\"19\" alt=\"Calandar\" src=\"../../images/back_end/ico_calendarpop.gif\" /></a> ")
				+ ("    </div> ")
				+ ("   </div> ")
				+ ("   <div class=\"dbShop-ui-clearboth\"></div> ")

			//Time
				+ ("   <div style=\"margin-top:0px\" class=\"form_container_a_pop\"> ")
				+ ("    <div class=\"form_title_pop\">")
				+ ("     " + this.options.timeLabel + ":")
				+ ("    </div> ")
				+ ("    <div style=\"padding-bottom:5px\" class=\"form_info_pop \"><select class=\"container_timeSelecter\"></select>")
				+ ("    </div> ")
				+ ("   </div> ")

				+ ("<div class=\"form_container_b_pop container_noSchema\" style=\"display:none;\"><div class=\"form_title_pop\"></div><div class=\"form_info_pop\" style=\"padding: 0px; color: red;\">" + t.options.noSchemaLabel + "</div></div>")
				+ ("   <div class=\"dbShop-ui-clearboth\"></div> ");

			//Source of appointment
			if (isOfflineAppointment || isCreate) {
				contentHtml += ("   <div style=\"margin-top:0px;\" class=\"form_container_a_pop container_source\"> ")
				+ ("    <div class=\"form_title_pop\">")
				+ ("     " + this.options.sourceOfAppointmentLable + ":")
				+ ("    </div> ")
				+ ("    <div style=\"padding-bottom:5px;width:115px;\" class=\"form_info_pop\">")
				+ ("		<select class=\"container_sourceSelecter\" style=\"margin:0px 0px;width:115px;\">")
				+ ("			<option value=\"phone\">" + this.options.source_phone + "</option>")
				+ ("			<option value=\"email\">" + this.options.source_email + "</option>")
				+ ("			<option value=\"walkin\">" + this.options.source_walkin + "</option>")
				+ ("			<option value=\"others\">" + this.options.source_others + "</option>")
				+ ("		</select>")
				+ ("    </div> ")
				+ ("   </div> ");
			}

			contentHtml += ("   <div style=\"padding-bottom:10px;\" class=\"dbShop-ui-clearboth\"></div> ");

			// cancel appointment button
			if (isUpdate) {
				contentHtml += ("<div style=\"padding:20px 0px 20px 0px;\"> ")
				+ ("<a href=\"javascript:void(0);\" class=\"msBtn dbShop-ui-popDelete btnCancelAppointment\">" + this.options.cancelAppointmentLable + "</a> ")
				+ ("</div> ");
			}
			contentHtml + ("   <div class=\"dbShop-ui-clearboth\"></div> ")
				+ ("</div>");
			var container = $(contentHtml);
			var timeSelecter = container.find(".container_timeSelecter");
			var sourceSelecter = container.find(".container_sourceSelecter");
			if (sourceSelecter.length > 0) {
				sourceSelecter.val(t.options.source);
			}

			//add controls
			var customerNameSelecter = $('<select></select>');

			if (isUpdate || isCreateByCustomer) {
				container.find(".container_customer_name").html(t.options.customerName);
			} else {
				container.find(".container_customer_name").append(customerNameSelecter);
				customerNameSelecter.change(function () {
					displayCustomerInfo($(this).val());
				});
			}

			t.serviceSelecter = $('<select></select>');
			container.find(".container_service_selecter").append(t.serviceSelecter);
			t.serviceSelecter.after('<span class="loading" style="display:none"></span>');

			t.resourceSelecter = $('<select></select>');
			container.find(".container_resource_selecter").append(t.resourceSelecter);
			t.resourceSelecter.after('<span class="loading" style="display:none"></span>');

			var calendarInput = $('<input disabled=\"disabled\" type=\"text\" value=\"\" readonly=\"readonly\" size=\"10\" />').val(new Date().toString(global["dateFormat"]));
			var calendarIcon = $('<a class=\"ico_calendarpop\"><img width=\"20\" height=\"19\" alt=\"Calandar\" src=\"../../images/back_end/ico_calendarpop.gif\" /></a>');
			container.find(".container_calendar").append(calendarInput).append(calendarIcon);
			calendarIcon.after('<span class="loading" style="margin-left:31px;display:none"></span>');


			swithTimeslotContainer(false);

			//set verify
			if (isCreate) {
				container.find(".container_type_new").find(".firstName, .lastName").setValidation({
					required: true,
					missingMessage: global["text_please_enter_valid_value"]
				}).setPlaceHolder();
				container.find(".container_type_new").find(".phone").setValidation({
					required: true,
					missingMessage: global["text_please_enter_valid_value"],
					validType: 'phone',
					invalidMessage: global["text_invalid_phone"]
				}).setPlaceHolder();
				container.find(".container_type_new").find(".email").setValidation({
					validType: 'email',
					invalidMessage: global["text_invalid_email"]
				}).setPlaceHolder();
			}

			t.serviceSelecter.setValidation({
				required: true,
				missingMessage: global["text_please_enter_valid_value"]
			});
			timeSelecter.setValidation({
				required: true,
				missingMessage: global["text_please_enter_valid_value"]
			});

			t.resourceSelecter.setValidation({
				required: true,
				missingMessage: global["text_please_enter_valid_value"]
			});


			function checkVerifyofSelec() {
				var servicePass = appointment.common.getValidation(t.serviceSelecter);
				var resourcePass = appointment.common.getValidation(t.resourceSelecter);
				var schedulePass = appointment.common.getValidation(timeSelecter);
				return servicePass && resourcePass && schedulePass;
			}
			function showConfirmDialog(title, message, onOk) {
				var confirmDialogContent = '';
				confirmDialogContent += ("   <p>" + message + "</p> ");
				confirmDialogContent += ("   <div style=\"margin-top:0px\" class=\"form_container_a_pop\"> ");
				confirmDialogContent += ("    <div class=\"form_title_pop\">");
				confirmDialogContent += ("     " + t.options.customerLabel + ":");
				confirmDialogContent += ("    </div> ");
				confirmDialogContent += ("    <div class=\"form_info_pop\">");
				var type = container.find(".container_type :radio:checked").val();
				if (isCreateByCustomer) {
					confirmDialogContent += ("     " + t.options.customerName + "");
				} else if (type == "new") {
					confirmDialogContent += $.trim(container.find(".container_type_new .firstName").val()) + " " + $.trim(container.find(".container_type_new .lastName").val());
				} else if (type == "existing") {
					confirmDialogContent += customerNameSelecter.find("option:selected").text();
				}


				confirmDialogContent += ("    </div> ");
				confirmDialogContent += ("   </div> ");
				confirmDialogContent += ("   <div class=\"dbShop-ui-clearboth\"></div> ");
				confirmDialogContent += ("   <div class=\"form_container_b_pop\"> ");
				confirmDialogContent += ("    <div style=\"*padding-top:10px\" class=\"form_title_pop\">");
				confirmDialogContent += ("     " + t.options.serviceLabel + ":");
				confirmDialogContent += ("    </div> ");
				confirmDialogContent += ("    <div class=\"form_info_pop\">");
				confirmDialogContent += ("     " + t.serviceSelecter.find("option:selected").text().htmlEncode() + "");
				confirmDialogContent += ("    </div> ");
				confirmDialogContent += ("   </div> ");
				confirmDialogContent += ("   <div class=\"dbShop-ui-clearboth\"></div> ");
				confirmDialogContent += ("   <div class=\"form_container_b_pop\"> ");
				confirmDialogContent += ("    <div style=\"*padding-top:10px\" class=\"form_title_pop\">");
				confirmDialogContent += ("     " + t.options.resourceLabel + ":");
				confirmDialogContent += ("    </div> ");
				confirmDialogContent += ("    <div class=\"form_info_pop\">");
				confirmDialogContent += ("     " + t.resourceSelecter.find("option:selected").text().htmlEncode() + "");
				confirmDialogContent += ("    </div> ");
				confirmDialogContent += ("   </div> ");
				confirmDialogContent += ("   <div class=\"dbShop-ui-clearboth\"></div> ");
				confirmDialogContent += ("   <div class=\"form_container_b_pop\"> ");
				confirmDialogContent += ("    <div class=\"form_title_pop\">");
				confirmDialogContent += ("     " + t.options.scheduleLabel + ":");
				confirmDialogContent += ("    </div> ");
				confirmDialogContent += ("    <div class=\"form_info_pop\">");
				confirmDialogContent += ("     " + appointment.dateTimeFormat(calendarInput.datepicker("getDate").toString("yyyy-MM-dd") + " " + timeSelecter.val()) + "");
				confirmDialogContent += ("    </div> ");
				confirmDialogContent += ("   </div> ");
				if (sourceSelecter.length > 0) {
					confirmDialogContent += ("   <div class=\"dbShop-ui-clearboth\"></div> ");
					confirmDialogContent += ("   <div class=\"form_container_b_pop\"> ");
					confirmDialogContent += ("    <div class=\"form_title_pop\">");
					confirmDialogContent += ("     " + t.options.sourceOfAppointmentLable + ":");
					confirmDialogContent += ("    </div> ");
					confirmDialogContent += ("    <div style=\"width:90px\" class=\"form_info_pop\">");
					confirmDialogContent += ("     " + sourceSelecter.find("option:selected").text().htmlEncode() + "");
					confirmDialogContent += ("    </div> ");
					confirmDialogContent += ("   </div> ");
				}
				confirmDialogContent += ("   <div class=\"dbShop-ui-clearboth\"></div> ");
				new appointment.dialog.bo.confirm(confirmDialogContent, title, function () {
					this.destory();
					onOk();
				}, t.options.clickObject);
			}
			//create button
			buttons.push({
				text: t.options.createText,
				cls: "msBtn btnUpdateAppointmentDone dbShop-ui-red",
				onclick: onDone
			});

			function onDone() {
				var schedule = timeSelecter.val();
				if (isUpdate) {
					if (checkVerifyofSelec()) {
						if (schedule && t.serviceSelecter.val().length > 0 && t.resourceSelecter.val().length > 0) {
							//call bal onUpdate
							showConfirmDialog(t.options.confirmUpdateDialogTitle, t.options.confirmUpdateDialogMessage, function () {
								t.options.onUpdate.call(dialog, {
									source: sourceSelecter.val(),
									customerGuid: t.options.customerGuid,
									//customerName: $.trim(customerNameInput.val()),
									serviceGuid: t.serviceSelecter.val(),
									resourceGuid: t.resourceSelecter.val(),
									schedule: calendarInput.datepicker("getDate").toString("yyyy-MM-dd") + " " + schedule
								});
							});
						}
					} else {
						return false;
					}
				} else {
					var type = container.find(".container_type :radio:checked").val();
					var result = {
						source: sourceSelecter.val(),
						serviceGuid: t.serviceSelecter.val(),
						resourceGuid: t.resourceSelecter.val(),
						schedule: calendarInput.datepicker("getDate").toString("yyyy-MM-dd") + " " + schedule
					};
					if (isCreateByCustomer) {
						if (checkVerifyofSelec()) {
							$.extend(result, {
								type: "existing",
								customerGuid: t.options.customerGuid,
								customerName: t.options.customerName
							});
						} else {
							return false;
						}
					} else if (type == "new") {
						var inputPass = appointment.common.getValidation(container.find(".container_type_new").find(".firstName, .lastName, .phone, .email"));
						if (checkVerifyofSelec() && inputPass) {
							$.extend(result, {
								type: "new",
								firstName: $.trim(container.find(".container_type_new .firstName").val()),
								lastName: $.trim(container.find(".container_type_new .lastName").val()),
								phone: $.trim(container.find(".container_type_new .phone").val()),
								email: $.trim(container.find(".container_type_new .email").val())
							});
						} else {
							return false;
						}
					} else if (type == "existing") {
						if (checkVerifyofSelec()) {
							$.extend(result, {
								type: "existing",
								customerGuid: customerNameSelecter.val(),
								customerName: customerNameSelecter.find("option:selected").text()
							});
						} else {
							return false;
						}
					}
					//call back onCreate
					if (schedule && result.serviceGuid.length > 0 && result.resourceGuid.length > 0) {
						showConfirmDialog(t.options.confirmCreateDialogTitle, t.options.confirmCreateDialogMessage, function () {
							t.options.onCreate.call(dialog, result);
						});
					}
				}
			}

			buttons.push({
				text: global["cancel"],
				cls: "msBtn dbShop-ui-grey",
				onclick: function () {
					hideAllErrorMessage();
					this.destory();
					//t.options.onCancel.call(this);
				}
			});

			function hideAllErrorMessage() {
				container.find(".container_type_new").find(".firstName, .lastName, .phone, .email").hideErrorMessage();
				t.serviceSelecter.hideErrorMessage();
				t.resourceSelecter.hideErrorMessage();
				timeSelecter.hideErrorMessage();
			}
			//click type event
			if (isCreate) {
				if (isCreateByCustomer) {
					container.find(".container_type_new").hide();
				} else {
					container.find(".container_type_existing").hide();
				}
			}
			container.find(".container_type :radio").click(function () {
				hideAllErrorMessage();
				var type = $(this).val();
				if (type == "existing") {
					container.find(".container_type_existing").show();
					container.find(".container_type_new").hide();
					genCustomerList();
				} else if (type == "new") {
					container.find(".container_type_existing").hide();
					container.find(".container_type_new").show();
				}
			});

			function genCustomerList() {
				if (customersInfo.length == 0) {
					appointment.getCustomerNameList({}, function (customers) {
						customersInfo = customers;
						$.each(customers, function (i, customer) {
							customerNameSelecter.append('<option value="{0}">{1} {2}</option>'.format(customer.CustomerGuid, customer.FirstName, customer.LastName));
						});
						displayCustomerInfo(customerNameSelecter.val());
					});
				}
			}

			function displayCustomerInfo(customerGuid) {
				if (customersInfo.length > 0 && customerGuid && customerGuid.length > 0) {
					var customer = $.grep(customersInfo, function (t) { return t.CustomerGuid == customerGuid });
					if (customer.length > 0) {
						customer = customer[0];
						container.find(".container_phone").html(customer.Contact.length > 0 ? customer.Contact : "--");
						container.find(".container_email").html(customer.Email.length > 0 ? customer.Email : "--");
					}
				}
			}

			//cancel appointment event
			container.find(".btnCancelAppointment").click(function () {
				hideAllErrorMessage();
				var btn = $(this);
				var deleteDialog = new appointment.dialog.bo.deleteAppointmentConfirm({
					clickObject: btn,
					title: global["text_delete_appointment"],
					tips: global["text_are_you_to_cancel_appointment"],
					reasonTitleLabel: global["text_reason_for_cancel_title"],
					reasonMessageLabel: global["text_reason_for_cancel_message"],
					textTips: global["text_leave_message_here"],
					customerLabel: global["text_customer"],
					resourceLabel: global["text_resource"],
					scheduleLabel: global["text_schedule"],
					serviceLabel: global["text_service"],
					successDialogTitle: t.options.successDialogTitle,
					successDialogContent: t.options.successDialogContent,
					serviceName: t.options.serviceName,
					costLabel: global["text_cost"],
					customerName: t.options.customerName,
					schedule: t.options.startDateTime,
					resourceName: t.options.resourceName,
					cost: t.options.cost,
					onOK: function (r) {
						var reason = r;
						appointment.cancelAppointment({
							customerGuid: t.options.customerGuid,
							bookingGuid: t.options.bookingGuid,
							reason: reason
						}, function (r) {
							if (r.success) {
								//open success dialog								
								t.options.onCancel.call(this);
								dialog.destory();
							} else {
								alert("Cancle error");
							}
						});
					}
				})
			});
			var dialog = new window.appointment.dialog({
				title: this.options.title,
				content: '',
				contentObj: container,
				clickObject: this.options.clickObject,
				buttons: buttons
			});

			function bindServices(callback) {
				t.serviceSelecter.next(".loading").show();
				appointment.getAvailableServicesNameList({}, function (services) {
					t.serviceSelecter.emptyHtml();
					t.serviceSelecter.append('<option value=\"{0}\">{1}</option>'.format('', t.options.selectServiceText));
					t.resourceSelecter.append('<option value=\"{0}\">{1}</option>'.format('', t.options.selectResourceText));
					$.each(services, function (i, service) {
						//if (service.Status == "A") {
						t.serviceSelecter.append('<option value=\"{0}\" status = \"{3}\">{1}{2}</option>'.format(service.ServiceGuid, service.Name, service.Status == "A" ? "" : "(" + global["text_not_available"] + ")", service.Status));
						//}
					});
					t.serviceSelecter.next(".loading").hide();
					var firstServiceGuid = '';
					//no need load on first time
					if (t.options.serviceGuid.length > 0 && t.serviceSelecter.find("option[value='" + t.options.serviceGuid + "']").length == 1) {
						firstServiceGuid = t.options.serviceGuid;
						t.serviceSelecter.val(firstServiceGuid);
					}
					if (firstServiceGuid.length > 0) {
						if (t.serviceSelecter.find("option:selected").attr("status") == "A") {
							bindResource(firstServiceGuid, function (serviceGuid, resourceGuid) {
								callback(serviceGuid, resourceGuid);
							});
						} else {
							swithTimeslotContainer(false);
							t.resourceSelecter.attr("disabled", "disabled");
						}
					} else {
						callback('', '');
					}

				});
				t.serviceSelecter.change(function () {
					var serviceGuid = $(this).val();
					var status = $(this).find("option:selected").attr("status");
					if (status == 'A') {
						t.resourceSelecter.removeAttr("disabled");
						if (serviceGuid.length > 0) {
							bindResource(serviceGuid, function (serviceGuid, resourceGuid) {
								callback(serviceGuid, resourceGuid);
							});
						} else {
							swithTimeslotContainer(false);
							calendarInput.attr("disabled", "disabled");
							t.resourceSelecter.find("option:first").siblings("option").remove()
						}
					} else {
						swithTimeslotContainer(false);
						t.resourceSelecter.attr("disabled", "disabled");
					}
				});
			}

			function bindResource(serviceGuid, callback) {
				if (serviceGuid && serviceGuid.length > 0) {
					t.resourceSelecter.emptyHtml();
					//t.resourceSelecter[0].innerHTML = "";
					t.resourceSelecter.next(".loading").show();
					if (resoucesCache[serviceGuid]) {
						var resourceGuid = bind(resoucesCache[serviceGuid]);
						callback(serviceGuid, resourceGuid);
					} else {
						appointment.getAvailableResourceNameListByServiceGuid({ serviceGuid: serviceGuid }, function (resources) {
							resoucesCache[serviceGuid] = resources;
							var resourceGuid = bind(resources);
							callback(serviceGuid, resourceGuid);
						});
					}
					t.resourceSelecter.unbind("change.dialog").bind("change.dialog", function () {
						var resourceGuid = $(this).val();
						if (resourceGuid.length == 0) {
							swithTimeslotContainer(false, true);
							calendarInput.attr("disabled", "disabled");
						} else {
							callback(serviceGuid, resourceGuid);
						}
					});
				}
				function bind(resources) {
					t.resourceSelecter.append('<option value=\"{0}\">{1}</option>'.format('', t.options.selectResourceText));
					$.each(resources, function (i, resource) {
						if (resource.HasSchema) {
							t.resourceSelecter.append('<option value=\"{0}\">{1}</option>'.format(resource.ResourceGuid, resource.Name));
						}
					});
					t.resourceSelecter.next(".loading").hide();
					if (t.options.resouceGuid.length > 0 && t.resourceSelecter.find("option[value='" + t.options.resouceGuid + "']").length == 1) {
						t.resourceSelecter.val(t.options.resouceGuid);
						return t.options.resouceGuid;
					} else {
						var firstResource = t.resourceSelecter.find("option:first");
						if (firstResource.length > 0) {
							return firstResource.val();
						}
					}
				}
			}
			function swithTimeslotContainer(toShow, toShowCalendarInput) {
				if (toShow) {
					timeSelecter.hideErrorMessage();
					container.find(".container_selectedSchema,.newScheduleTitle,.avaliableTimeTitle").show();
					timeSelecter.removeAttr("disabled");
					calendarInput.removeAttr("disabled");
					calendarIcon.unbind("click").bind("click", function () {
						calendarInput.datepicker("show");
					});
					$(".btnUpdateAppointmentDone").addClass("dbShop-ui-red").removeClass("dbShop-ui-grey").unbind("click").bind("click", onDone);
					//timeslotContainer.show();
				} else {
					hideAllErrorMessage();
					container.find(".container_selectedSchema,.newScheduleTitle,.avaliableTimeTitle").hide();
					timeSelecter.attr("disabled", "disabled");
					if (!toShowCalendarInput) {
						calendarInput.attr("disabled", "disabled");
						calendarIcon.unbind("click");

					}
					$(".btnUpdateAppointmentDone").removeClass("dbShop-ui-red").addClass("dbShop-ui-grey").unbind("click");

					//timeslotContainer.hide();
				}
			}
			function bindTimeslot(timeSlots) {
				//timeslotContainer.emptyHtml();
				timeSelecter.empty();
				if (timeSlots.AM.length == 0 && timeSlots.PM.length == 0) {
					swithTimeslotContainer(false, true);
					return;
				} else {
					swithTimeslotContainer(true);
				}
				//timeslotContainer[0].innerHTML = ""; 
				var timeSlotHtmlFormat = ("    <div class=\"time_container\"> ")
								+ ("     <input type=\"radio\" name=\"rad_timeslot\" value=\"{0}\">")
								+ ("     <span>{1}</span>")
								+ ("    </div><div class=\"dbShop-ui-clearboth\"></div>");
				var timeSlotOptionHtmlFormat = "<option value=\"{0}\">{1}</option>";
				//$.merge(timeSlots.AM, timeSlots.PM);
				$.each(timeSlots.AM, function (i, ts) {
					//timeslotContainer.append(timeSlotHtmlFormat.format(ts, appointment.timeFormat(Date.parse(ts))));
					timeSelecter.append(timeSlotOptionHtmlFormat.format(ts, appointment.timeFormat(Date.parse(ts))));
				});
				$.each(timeSlots.PM, function (i, ts) {
					var curTs = ts.substr(0, 2) == "12" ? Date.parse(ts) : Date.parse(ts).addHours(12);
					//timeslotContainer.append(timeSlotHtmlFormat.format(curTs.toString("HH:mm"), appointment.timeFormat(curTs)));
					timeSelecter.append(timeSlotOptionHtmlFormat.format(curTs.toString("HH:mm"), appointment.timeFormat(curTs)));
				});
				/*
				timeslotContainer.find(".time_container").click(function () {
				timeslotContainer.find(":radio").removeAttr("checked");
				timeslotContainer.find(".time_container").removeClass("time_container_on");
				//showSelectedSchema($(this));
				});
				*/
				//checked first timeslot
				//showSelectedSchema(timeslotContainer.find(".time_container:first"));
			}

			/*
			function showSelectedSchema(time_container) {
			if (time_container) {
			time_container.addClass("time_container_on");
			var curRadio = time_container.find(":radio");
			if (curRadio.length > 0) {
			var timeValue = curRadio.val().split(":")
			curRadio.attr("checked", "checked");
			container.find(".container_selectedSchema").html(appointment.dateTimeFormat(calendarInput.datepicker("getDate").at({ hour: appointment.common.toNumber(timeValue[0]), minute: appointment.common.toNumber(timeValue[1]) })));
			}
			}
			}
			*/
			bindServices(function (serviceGuid, resourceGuid) {
				if (serviceGuid.length == 0 || resourceGuid.length == 0) {
					return;
				}
				container.find(".container_noSchema").hide();
				if (t.schema) {
					t.schema.destroy();
				}
				t.schema = new appointment.schedule({
					serviceId: serviceGuid,
					resourceId: resourceGuid,
					serverDate: new Date(),  //當前系統時間	
					calendar: calendarInput, //日曆jq對象
					autoBegin: true,
					serverDate: t.options.serverDate,
					onLoadedService: function (r) { },
					onLoadedResource: function (r) {
						//timeslotContainer.emptyHtml();
						timeSelecter.empty();
					},
					onBeforeSelectDate: function (date) {

					},
					onLoadComplete: function () {
						if (t.options.startDateTime.length > 0) {
							var schemaDate = Date.parse(t.options.startDateTime);
							this.setDate(schemaDate);
						}
					},
					onAfterSelectDate: function (date) {
					},
					onBindTimeSlots: function (timeSlots) {
						bindTimeslot(timeSlots);
					},
					calendarShow: function (calendar) {
						calendarInput.removeAttr("disabled");
						calendarIcon.next(".loading").hide();
					},
					calendarHide: function (calendar) {
						calendarInput.attr("disabled", "disabled");
						calendarIcon.next(".loading").show();
					},
					onCheckedHasSchema: function (hasSchema) {
						if (!hasSchema) {
							container.find(".container_noSchema").show();
							swithTimeslotContainer(false);
							calendarInput.attr("disabled", "disabled");
							calendarIcon.unbind("click");
						} else {
							swithTimeslotContainer(true, true);
							calendarIcon.bind("click", function () {
								calendarInput.datepicker("show");
							});
						}
					}
				});
			});
			return dialog;
		},
		previewAppointment: function (options) {
			this.options = $.extend({
				clickObject: null,
				serviceLabel: "Service",
				scheduleLabel: "Schedule",
				resourceLabel: "Resource",
				serviceName: "",
				schedule: "",
				resourceName: "",
				serviceGuid: "",
				customerGuid: "",
				resouceGuid: "",
				startDateTime: "",
				bookingGuid: "",
				title: "Preview Appointment",
				updateText: "Update",
				deleteText: "Delete Appointment",
				selectResourceText: "Select Resource",
				resourceData: [],
				onDelete: function () { },
				onCancel: function () { },
				onUpdate: function () { }
			}, options);
			var contentHtml = '' +
				'<div style="margin-top:0px" class="form_container_a_pop">		' +
				'<div class="form_title_pop">' + this.options.serviceLabel + ':</div>' +
				'<div class="form_info_pop">' + this.options.serviceName + '</div>' +
				'</div>	' +
				'<div class="dbShop-ui-clearboth"></div>' +
				'<div class="form_container_b_pop">		' +
				'<div class="form_title_pop">' + this.options.scheduleLabel + ':</div>' +
				'<div class="form_info_pop">' + this.options.schedule + '</div>' +
				'</div>	' +
				'<div class="dbShop-ui-clearboth"></div>' +
				'<div class="form_container_b_pop">	' +
				'<div style="*padding-top:10px" class="form_title_pop">' + this.options.resourceLabel + ':</div>' +
				'<div class="form_info_pop">' + this.options.resourceName + '</div>' +
				'</div>' +
				'<div class="dbShop-ui-clearboth"></div>';
			var t = this;
			/*
			var resoucesList = appointment.getSameScheduleResources({
			serviceGuid: this.options.serviceGuid,
			resouceGuid: this.options.resouceGuid,
			schedule: this.options.startDateTime,
			bookingGuid: this.options.bookingGuid
			});
			*/
			/*
			contentHtml += '<select style="margin:0px 10px;width:150px;" id="ddlResourceList">' +
			'<option style="" selected="selected" value="">' + this.options.selectResourceText + '</option>';
			$.each(resoucesList, function (i, resouce) {
			contentHtml += '<option style="" value="' + resouce.resourceGuid + '">' + resouce.name + '</option>';
			});

			contentHtml += '</select>' +
			'<a href="javascript:;" class="btnDialogUpdateAppointment msBtn dbShop-ui-red">' + this.options.updateText + '</a>' +
			'</div>' +
			'</div>	' +
			'<div class="dbShop-ui-clearboth"></div>';
			*/
			var buttons = []; // 

			if (false && Date.parse(this.options.startDateTime) > new Date()) { //強制不顯示delete 按鈕，只提供預覽
				//display delete button				
				buttons.push({
					text: t.options.deleteText,
					cls: "msBtn dbShop-ui-grey",
					onclick: function () {
						t.options.onDelete.call(this);
					}
				});
			}
			buttons.push({
				text: global["close"],
				cls: "msBtn dbShop-ui-grey",
				onclick: function () {
					this.destory();
					t.options.onCancel.call(this);
				}
			});
			var dialog = new window.appointment.dialog({
				title: this.options.title,
				content: contentHtml,
				clickObject: this.options.clickObject,
				buttons: buttons
			});
			$(".btnDialogUpdateAppointment:visible").click(function () {
				var resourceGuid = $(this).closest(".dialog_content").find("#ddlResourceList").val();
				if (resourceGuid.length > 0) {
					var result = appointment.updateAppointmentResouce({
						schedule: t.options.startDateTime,
						resouceGuid: resourceGuid,
						customerGuid: t.options.customerGuid,
						bookingGuid: t.options.bookingGuid,
						serviceGuid: t.options.serviceGuid
					});
					t.options.onUpdate.call(dialog, result.success);
				}
			});
			return dialog;
		},
		createResouceTips: function (options) {
			this.options = $.extend({
				title: global["text_resource_working_hours_settings_title"],
				clickObject: null
			}, options);
			var contentHtml = global["text_resource_working_hours_settings_tips"];
			var buttons = [];
			buttons.push({
				text: global["close"],
				cls: "msBtn dbShop-ui-red",
				onclick: function () {
					this.destory();
				}
			});
			var dialog = new window.appointment.dialog({
				title: this.options.title,
				content: contentHtml,
				cls: 'dbShop-ui-nlayer_settings',
				clickObject: this.options.clickObject,
				buttons: buttons
			});
		},
		createServiceTips: function (options) {
			this.options = $.extend({
				title: global["text_service_advance_booking_settings_title"],
				clickObject: null
			}, options);
			var contentHtml = global["text_service_advance_booking_settings_tips"];
			var buttons = [];
			buttons.push({
				text: global["close"],
				cls: "msBtn dbShop-ui-red",
				onclick: function () {
					this.destory();
				}
			});
			var dialog = new window.appointment.dialog({
				title: this.options.title,
				content: contentHtml,
				cls: 'dbShop-ui-nlayer_settings',
				clickObject: this.options.clickObject,
				buttons: buttons
			});
		}
	}
});