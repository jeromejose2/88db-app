﻿$(function () {
	var calendar = {
		target: $("#calendar"),
		serviceGuid: "0",
		resourceGuid: "0",
		startDate: "",
		endDate: ""
	};
	window.previewAppointmentDialog = null;
	$(".btnChangeView").click(function () {
		$(".btnChangeView").removeClass("dbShop-ui-applyOn").addClass("dbShop-ui-apply");
		$(this).addClass("dbShop-ui-applyOn").removeClass("dbShop-ui-apply");
		calendar.target.fullCalendar('changeView', $(this).attr("view"));
	});

	$(".btnViewNext").click(function () { calendar.target.fullCalendar('next'); });

	$(".btnViewPrev").click(function () { calendar.target.fullCalendar('prev'); });

	$(".btnViewToday").click(function () { calendar.target.fullCalendar('today'); });

	$(".serviceFilter").change(function () {
		var serviceGuid = $(this).val();
		if (serviceGuid == "0") {
			serviceGuid = "";
			$(this).val(serviceGuid);
		}
		calendar.serviceGuid = serviceGuid;
		bindEventSource();
	});

	$(".resourceFilter").change(function () {
		var resourceGuid = $(this).val();
		if (resourceGuid == "0") {
			resourceGuid = "";
			$(this).val(resourceGuid);
		}
		calendar.resourceGuid = resourceGuid;
		bindEventSource();
	});

	$(".btnCreateAppointment").click(function () {
		var model = {
			/*
			ServiceGuid: 
			ResourceGuid: 
			BookingGuid: 
			StartDateTime: 
			BookingGuid:
			ServiceName:
			ResourceName:
			CustomerGuid: currentCustomerGuid,
			FirstName: $(".app_name").html(),
			LastName: ''
			*/
		};
		window.updateAppointment = new appointment.dialog.bo.updateAppointment({
			serviceLabel: global["text_service"],
			scheduleLabel: global["text_schedule"],
			resourceLabel: global["text_resource"],
			customerLabel: global["text_customer"],

			phoneLabel: global["text_phone"],
			emailLabel: global["text_email"],
			timeLabel: global["text_time"],
			sourceOfAppointmentLable: global["text_source_of_appointment"],
			cancelAppointmentLable: global["text_cancel_this_appointment"],
			successDialogTitle: global["text_appointment_cancelled"],
			successDialogContent: global["text_the_appointment_has_been_canceled_successfully"],
			firstNameLabel: global["text_first_name"],
			lastNameLabel: global["text_last_name"],
			source_phone: global["text_source_phone"],
			source_email: global["text_source_email"],
			source_walkin: global["text_source_walkin"],
			source_others: global["text_source_others"],
			typeLabel: global["text_typeLabel"],
			existingCustomerLabel: global["text_existingCustomerLabel"],
			newCustomerLabel: global["text_newCustomerLabel"],
			confirmCreateDialogMessage: global["text_want_to_create_appointment"],
			confirmCreateDialogTitle: global["text_confirm_create_appointment"],

			selectServiceText: global["text_select_service"],
			selectResourceText: global["text_select_resource"],
			serverDate: Date.parse(global["nowDateTime"]),
			title: global["text_create_appointment"],
			dateLable: global["text_date"],
			avaliableTimeLabel: global["text_avaliable_time"],
			newScheduleLabel: global["text_new_schedule"],
			createText: global["text_create_appointment"],
			noSchemaLabel: global["text_no_schema"],
			onCreate: function (result) {
				var dialog = this;
				appointment.dialog.bo.loading();
				appointment.createAppointment({
					type: result.type,
					source: result.source,
					customerGuid: result.customerGuid,
					customerName: result.customerName,
					serviceGuid: result.serviceGuid,
					resourceGuid: result.resourceGuid,
					schedule: Date.parse(result.schedule).toString("yyyy-MM-dd HH:mm"),
					firstName: result.firstName,
					lastName: result.lastName,
					phone: result.phone,
					email: result.email
				}, function (r) {
					appointment.dialog.bo.closeLoading();
					if (r.success) {
						dialog.destory();
						new appointment.dialog.bo.info(global["text_appointment_created_title"], global["text_appointment_created_message"]);
						bindEventSource();
					} else {
						switch (r.errorCode) {
							case 1:
								alert('schedule is invalidate');
								break;
							case 2:
								alert('schedule full');
								break;
							case 3:
								alert('create timeslot error');
								break;
							case 4:
								alert('create booking error');
								break;
							case 5:
								alert('sent email error');
								break;
						}
					}
				});
			}
		});
	});

	var dayNames = global["text_day_collection"].split(",");
	var monthNames = global["text_month_collection"].split(",");
	calendar.target.fullCalendar({
		monthNames: monthNames,
		dayNames: dayNames,
		dayNamesShort: dayNames,
		header: {
			left: '', //'prev,next today',
			center: 'title',
			right: '' //'month,agendaWeek,agendaDay'
		},
		titleFormat: {
			month: global["monthDateFormat"],                             // September 2009
			week: global["dateFormat"] + "{ '&#8212;'" + global["dateFormat"] + "}", // Sep 7 - 13 2009
			day: global["dateFormat"] + '(dddd)'                  // Tuesday, Sep 8, 2009
		},
		columnFormat: {
			month: 'dddd',    // Mon
			week: 'dddd ' + global["dayDateFormat"], // Mon 9/7
			day: 'dddd ' + global["dayDateFormat"]  // Monday 9/7
		},
		timeFormat: "hh:mmTT",
		disableDragging: true,
		disableResizing: true,
		eventClick: function (calEvent, jsEvent, view) {
			timeSlotClick(calEvent);
		},
		viewDisplay: function (view) {
			$(".dbShop-ui-applyOn").removeClass("dbShop-ui-applyOn").addClass("dbShop-ui-apply");
			$('a[view="' + view.name + '"]').removeClass("dbShop-ui-apply").addClass("dbShop-ui-applyOn");
			calendar.startDate = view.visStart.toString("yyyy-MM-dd");
			calendar.endDate = appointment.common.cloneDate(view.visEnd).addDays(-1).toString("yyyy-MM-dd");
			bindEventSource();
		},
		slotMinutes: 15,
		editable: true
	});

	function addEventSource(appointments) {
		calendar.target.fullCalendar('removeEvents');
		var events = [];
		$.each(appointments, function (i, appointment) {
			events.push({
				title: "{0} {1}".format(appointment.FirstName, appointment.LastName),
				start: Date.parse(appointment.StartDateTime),
				end: Date.parse(appointment.EndDateTime),
				appointment: appointment,
				allDay: false,
				color: appointment.AssignColor
			});
		});
		calendar.target.fullCalendar('addEventSource', events)
	}

	function bindEventSource() {
		if (calendar.serviceGuid == "0") { calendar.serviceGuid = ""; }
		if (calendar.resourceGuid == "0") { calendar.resourceGuid = ""; }
		appointment.getConditionalAppointments({
			serviceGuid: calendar.serviceGuid,
			resourceGuid: calendar.resourceGuid,
			startDate: calendar.startDate,
			endDate: calendar.endDate
		}, function (result) {
			var appointments = result.rows;
			addEventSource(appointments);
			appointment.resizeFrame();
		});
	}


	function timeSlotClick(calEvent) {
		var model = calEvent.appointment;
		if (Date.parse(model.StartDateTime) > Date.parse(global["nowDateTime"])) {
			window.updateAppointment = new appointment.dialog.bo.updateAppointment({
				serviceLabel: global["text_service"],
				scheduleLabel: global["text_schedule"],
				resourceLabel: global["text_resource"],
				customerLabel: global["text_customer"],

				phoneLabel: global["text_phone"],
				emailLabel: global["text_email"],
				timeLabel: global["text_time"],
				sourceOfAppointmentLable: global["text_source_of_appointment"],
				cancelAppointmentLable: global["text_cancel_this_appointment"],
				phone: model.Contact,
				email: model.Email,
				successDialogTitle: global["text_appointment_cancelled"],
				successDialogContent: global["text_the_appointment_has_been_canceled_successfully"],
				source: model.Source,
				source_phone: global["text_source_phone"],
				source_email: global["text_source_email"],
				source_walkin: global["text_source_walkin"],
				source_others: global["text_source_others"],
				typeLabel: global["text_typeLabel"],
				existingCustomerLabel: global["text_existingCustomerLabel"],
				newCustomerLabel: global["text_newCustomerLabel"],
				confirmUpdateDialogMessage: global["text_want_to_update_appointment"],
				confirmUpdateDialogTitle: global["text_confirm_update_appointment"],

				selectServiceText: global["text_select_service"],
				serviceName: model.ServiceName,
				selectResourceText: global["text_select_resource"],
				resourceName: model.ResourceName,
				customerGuid: model.CustomerGuid,
				customerName: "{0} {1}".format(model.FirstName, model.LastName).htmlEncode(),
				serviceGuid: model.ServiceGuid,
				resouceGuid: model.ResourceGuid,
				bookingGuid: model.BookingGuid,
				startDateTime: model.StartDateTime,
				serverDate: Date.parse(global["nowDateTime"]),
				title: global["text_update_appointment"],
				dateLable: global["text_date"],
				avaliableTimeLabel: global["text_avaliable_time"],
				newScheduleLabel: global["text_new_schedule"],
				createText: global["text_update_appointment"],
				noSchemaLabel: global["text_no_schema"],
				onUpdate: function (result) {
					var dialog = this;
					appointment.dialog.bo.loading();
					appointment.updateAppointment({
						serviceGuid: result.serviceGuid,
						resouceGuid: result.resourceGuid,
						schedule: Date.parse(result.schedule).toString("yyyy-MM-dd HH:mm"),
						customerGuid: result.customerGuid,
						source: result.source,
						bookingGuid: model.BookingGuid
					}, function (r) {
						appointment.dialog.bo.closeLoading();
						if (r.success) {
							dialog.destory();
							bindEventSource();
						} else {
							alert("error");
						}
					});
				},
				onCancel: function () {
					bindEventSource();
				}
			});
		} else {
			if (previewAppointmentDialog == null) {
				var dealDateString = window.appointment.dateTimeFormat(model.StartDateTime);
				previewAppointmentDialog = new appointment.dialog.bo.previewAppointment({
					serviceLabel: global["text_service"],
					scheduleLabel: global["text_schedule"],
					resourceLabel: global["text_resource"],
					serviceName: model.ServiceName,
					schedule: dealDateString,
					selectResourceText: global["text_select_resource"],
					resourceName: model.ResourceName,
					customerGuid: model.CustomerGuid,
					serviceGuid: model.ServiceGuid,
					resouceGuid: model.ResourceGuid,
					bookingGuid: model.BookingGuid,
					startDateTime: model.StartDateTime,
					title: global["text_preview_appointment"],
					updateText: global["text_update"],
					deleteText: global["text_delete_appointment"],
					onDelete: function () {
						//						this.hide();
						//						new appointment.dialog.bo.deleteAppointmentConfirm({
						//							title: global["text_delete_appointment"],
						//							tips: global["text_are_you_to_cancel_appointment"],
						//							reasonLabel: global["text_reason_for_delete"],
						//							textTips: global["text_leave_message_here"],
						//							customerLabel: global["text_customer"],
						//							resourceLabel: global["text_resource"],
						//							scheduleLabel: global["text_schedule"],
						//							serviceLabel: global["text_service"],
						//							serviceName: model.ServiceName,
						//							costLabel: global["text_cost"],
						//							customerName: model.FirstName,
						//							schedule: dealDateString,
						//							resourceName: model.ResourceName,
						//							cost: model.FormatCost,
						//							onOK: function (reason) {
						//								appointment.cancelAppointment({
						//									customerGuid: model.CustomerGuid,
						//									bookingGuid: model.BookingGuid,
						//									reason: reason
						//								}, function () {
						//									previewAppointmentDialog = null;
						//									calendar.target.fullCalendar('removeEvents', calEvent._id)
						//								});
						//							},
						//							onCancel: function () { previewAppointmentDialog.show(); }
						//						});
					},
					onCancel: function () {
						previewAppointmentDialog = null;
					},
					onUpdate: function (success) {
						if (success) {
							previewAppointmentDialog = null;
							this.destory();
							bindEventSource();
						}
					}
				});
			} else {
				previewAppointmentDialog.show();
			}
		}
	}
});