﻿$(function () {
	appendDropdown($("#ReminderMinute_day"), "day");
	appendDropdown($("#ReminderMinute_hour"), "hour");
	appendDropdown($("#ReminderMinute_min"), "min");
	appointment.common.setValidation();
	//load detail
	if (defaultValue.EnableNotificationEmail) {
		$("#EnableNotificationEmail").attr("checked", "checked");
	}
	if (defaultValue.EnableReminderEmail) {
		$("#EnableReminderEmail").attr("checked", "checked");
		$("#ReminderMinute_day, #ReminderMinute_min, #ReminderMinute_hour").removeAttr("disabled");
	}
	var mins = defaultValue.ReminderMinute;
	var day = Math.floor(mins / (60 * 24));
	var hour = Math.floor((mins - day * 60 * 24) / 60);
	var min = mins - day * 60 * 24 - hour * 60;
	$("#ReminderMinute_day").val(day);
	$("#ReminderMinute_hour").val(hour);
	$("#ReminderMinute_min").val(min);

	setCusomizeTitlesToInput(defaultValue.CustomizeTitles);

	var labelCusomizeTitles = global["defaultCustomizeTitles"][global["lang"]];
	if (!labelCusomizeTitles) {
		labelCusomizeTitles = global["defaultCustomizeTitles"]['en'];
		global["defaultCustomizeTitles"][global["lang"]] = labelCusomizeTitles;
	}
	$(".labelAppointmentTitle").html(labelCusomizeTitles.AppointmentTitle);
	$(".labelServiceTitle").html(labelCusomizeTitles.ServiceTitle);
	$(".labelResourceTitle").html(labelCusomizeTitles.ResourceTitle);
	$(".labelScheduleTitle").html(labelCusomizeTitles.ScheduleTitle);


	$("#EnableReminderEmail").click(function () {
		var isChecked = $(this).is(":checked");
		if (isChecked) {
			$("#ReminderMinute_day, #ReminderMinute_min, #ReminderMinute_hour").removeAttr("disabled");
		} else {
			$("#ReminderMinute_day, #ReminderMinute_min, #ReminderMinute_hour").attr("disabled", "disabled");
		}
	});

	$(".btnSave").click(function () {
		if (appointment.common.getValidation()) {
			var btnSave = $(this);
			new appointment.dialog({
				title: global["save_changes_dialog_title"],
				content: global["save_changes_dialog_content"],
				clickObject: btnSave,
				buttons: [
				{
					text: global.save,
					cls: appointment.dialog.bo.color.blue,
					onclick: function () {
						var value = getValues();
						var t = this;
						window.appointment.saveSetting(value, function (r) {
							if (r.success) {
								defaultValue = value;
								t.destory();
								appointment.dialog.bo.info(global["save_setting_dialog_title"], global["save_setting_dialog_content"], function () {
									this.destory();
									window.location.reload();
								}, btnSave);
							}
						});
					}
				},
				{
					text: global.cancel,
					cls: "msBtn dbShop-ui-grey",
					onclick: function () {
						this.destory();
					}
				}
			]
			});
		}
	});

	$(".btnRetoreCustomizeTitle").click(function () {
		setCusomizeTitlesToInput(global["defaultCustomizeTitles"][global["lang"]]);
	});
	$(window).bind('beforeunload', function () {
		if (hasChanged()) {
			return global["save_changes_dialog_content"];
		}
	});

	function setCusomizeTitlesToInput(titles) {
		$("#title_appointment").val(appointment.common.htmlDecode(titles.AppointmentTitle));
		$("#title_service").val(appointment.common.htmlDecode(titles.ServiceTitle));
		$("#title_resource").val(appointment.common.htmlDecode(titles.ResourceTitle));
		$("#title_schedule").val(appointment.common.htmlDecode(titles.ScheduleTitle));
	}

	function appendDropdown(selecter, type) {
		var options = [];
		var optionHtml = '<option value="{0}" style="padding-left:0px;">{1}</option>';
		switch (type) {
			case "day":
				for (var i = 0; i <= 31; i++) {
					//options.push(optionHtml.format(i, i + global["text_day"]));
					options.push(optionHtml.format(i, i));
				}
				break;
			case "hour":
				for (var i = 0; i < 24; i++) {
					options.push(optionHtml.format(i, i + global["text_hour"]));
				}
				break;
			case "min":
				for (var i = 0; i < 60; i++) {
					options.push(optionHtml.format(i, i + global["text_mins"]));
				}
				break;
		}
		$.each(options, function (i, option) {
			selecter.append(option);
		});
	}

	function getValues() {
		var setting = {};
		setting.EnableNotificationEmail = $("#EnableNotificationEmail").is(":checked");
		setting.EnableReminderEmail = $("#EnableReminderEmail").is(":checked");
		setting.CustomizeTitles = {
			AppointmentTitle: $.trim($("#title_appointment").val()),
			ServiceTitle: $.trim($("#title_service").val()),
			ResourceTitle: $.trim($("#title_resource").val()),
			ScheduleTitle: $.trim($("#title_schedule").val())
		};
		var ReminderMinute = 0;
		if (setting.EnableReminderEmail) {
			/*
			$("#ReminderMinute_day, #ReminderMinute_hour, #ReminderMinute_min").each(function (i) {
			var val = appointment.common.toNumber($(this).val());
			if (i == 0) {
			ReminderMinute += 60 * 24 * val;
			} else if (i == 1) {
			ReminderMinute += 60 * val;
			} else if (i == 2) {
			ReminderMinute += val;
			}
			});
			*/
			$("#ReminderMinute_day").each(function (i) {
				var val = appointment.common.toNumber($(this).val());
				ReminderMinute += 60 * 24 * val;
			});
		}
		setting.ReminderMinute = ReminderMinute;
		if (ReminderMinute == 0) {
			setting.EnableReminderEmail = false;
		}
		return setting;
	}

	function hasChanged() {
		return !appointment.common.isEqual(defaultValue, getValues())
	}
});