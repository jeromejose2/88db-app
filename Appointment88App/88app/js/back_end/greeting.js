﻿$(function () {
	$(".btnSave").click(function () {
		var btnSave = $(this);
		new appointment.dialog({
			title: global["save_changes_dialog_title"],
			content: global["save_changes_dialog_content"],
			clickObject: btnSave,
			buttons: [
				{
					text: global.save,
					cls: appointment.dialog.bo.color.blue,
					onclick: function () {
						var value = tinyMCE.get()[0].getContent().replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
						var t = this;
						window.appointment.saveGreeting({ greeting: value }, function (r) {
							if (r.success) {
								//defaultValue = value;
								t.destory();
								appointment.dialog.bo.info(global["save_setting_dialog_title"], global["save_setting_dialog_content"], function () {
									this.destory();
								}, btnSave);
							}
						});
					}
				},
				{
					text: global.cancel,
					cls: "msBtn dbShop-ui-grey",
					onclick: function () {
						this.destory();
					}
				}
			]
		});
	});
});