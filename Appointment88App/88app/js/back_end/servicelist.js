﻿$(function () {
	//$("div[sortname=" + $.query.get("sn") + "]").addClass("arrowSort_" + ($.query.get("so") == "a" ? "asc" : "des")).show()
	resetRowsUI();
	$("#txtKeyword").val($.query.get("q")).keypress(function (e) {
		if (e.keyCode == 13) {
			goSearch();
			return false;
		}
		return true;
	});
	/*
	$("*[sortname]").click(function () {
	var sortName = $(this).attr("sortname");
	order(sortName);
	});
	*/
	$(".btnDelete").click(function () {
		var btnDelete = $(this);
		var guid = $(this).parents("tr").attr("guid");
		window.appointment.dialog.bo.confirm(
			global["dialog_delete_service_content"].replace("{Name}", $(this).parents("tr").find("td:eq(0)").text()),
			global["dialog_delete_service_title"],
			function () {
				del(guid);
			},
			btnDelete
		);
	});
	$(".btnSwitchStatus").click(function () {
		var t = $(this);
		var toShow = $(this).hasClass("dbShop-ui-show_action") ? false : true;
		var tr = $(this).parents("tr");
		var guid = tr.attr("guid");
		var activeCount = appointment.common.toNumber($(".activeCount").html());
		window.appointment.switchServiceStatus({
			serviceId: guid,
			status: toShow ? "A" : "I"
		}, function (r) {
			if (r.success) {
				if (toShow) {
					t.removeClass("dbShop-ui-hide_action").addClass("dbShop-ui-show_action");
					t.attr("title", global["text_enabled"]);
					tr.find(".label_status").html(global["text_enabled"]);
					$(".activeCount").html(++activeCount);
				} else {
					t.removeClass("dbShop-ui-show_action").addClass("dbShop-ui-hide_action");
					t.attr("title", global["text_disabled"]);
					tr.find(".label_status").html(global["text_disabled"]);
					$(".activeCount").html(--activeCount);
				}
			} else {
				appointment.dialog.bo.systemError();
			}
		});
	});
	$(".btnCopyLink").click(function () {
		var btnCopyLink = $(this);
		var tr = $(this).parents("tr");
		var guid = tr.attr("guid");
		var link = "{0}&serviceid={1}".format(global["select_service_url"], guid);
		new window.appointment.dialog({
			title: global["dialog_copylink_title"],
			clickObject: btnCopyLink,
			content: '<div class="dbShop-ui-container_text">' + link + '</div>',
			buttons: [
				{
					text: global.close,
					cls: "msBtn dbShop-ui-grey",
					onclick: function () {
						this.destory();
					}
				}]
		});
	});

	$(".btnSearch").click(function () {
		goSearch();
	});
	$(".btnClear").click(function () {
		var query = $.query.remove("q");
		location.href = location.pathname + query.toString();
	});

	$(".btnMoveUp").click(function () {
		if (!$(this).hasClass("dbShop-ui-sorMoveUpBtnDis")) {
			var row = $(this).parents("tr");
			if (saveSort(row, row.prev(), "prev") == 1) {
				location.href = location.href;
			}
			row.prev().before(row);
			resetRowsUI();
		}
	});
	$(".btnMoveDown").click(function () {
		if (!$(this).hasClass("dbShop-ui-sorMoveDnBtnDis")) {
			var row = $(this).parents("tr");
			if (saveSort(row, row.next(), "next") == 1) {
				location.href = location.href;
			}
			row.next().after(row);
			resetRowsUI();
		}
	});
	function saveSort(row1, row2, action) {
		var services = [{ serviceGuid: row1.attr("guid"), weight: row2.attr("weight")}];
		if (row2.length == 1) {
			services.push({ serviceGuid: row2.attr("guid"), weight: row1.attr("weight") });
		} else {
			services = [{ serviceGuid: row1.attr("guid"), weight: row1.attr("weight")}];
		}
		var result = appointment.changeServicesWeight({ services: services, action: action });
		if (result.success) {
			var weight = row2.attr("weight");
			row2.attr("weight", row1.attr("weight"));
			row1.attr("weight", weight);
		}
		return services.length;
	}
	function resetRowsUI() {
		$("#product-table-grid tbody tr").each(function (i) {
			//$(this).removeClass("row1").removeClass("row2").addClass("row" + (i % 2 == 0 ? 1 : 2));
		}).find(".btnMoveDown, .btnMoveUp").removeClass("dbShop-ui-sorMoveDnBtnDis").removeClass("dbShop-ui-sorMoveUpBtnDis");
		if ($("#paging .next").length == 0) {
			$("#product-table-grid tbody tr:last").find(".btnMoveDown").addClass("dbShop-ui-sorMoveDnBtnDis");
		}
		if ($("#paging .previous").length == 0) {
			$("#product-table-grid tbody tr:first").find(".btnMoveUp").addClass("dbShop-ui-sorMoveUpBtnDis");
		}
	}
	function goSearch() {
		var keyword = $.trim($("#txtKeyword").val());
		if (keyword.length == 0) {
			$("#txtKeyword").focus();
			return false;
		}
		var query = $.query.set("q", keyword);
		location.href = location.pathname + query.toString();
	}
	function order(sortName) {
		var so = $.query.get("so"); //a asc , d desc
		var sn = $.query.get("sn");
		if (so.length == 0 || sortName != sn) {
			so = "d";
		} else {
			so = (so == "a" ? "d" : "a");
		}
		var query = $.query.set("so", so);
		query = query.set("sn", sortName);
		location.href = location.pathname + query.toString();
	}

	function del(guid) {
		var inputs = [];
		inputs.push({ name: "action", value: "delete" });
		inputs.push({ name: "guid", value: guid });
		appointment.common.postUrl({
			inputs: inputs
		});
	}


});