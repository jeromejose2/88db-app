﻿$(function () {
	var prevAppointmentPaging = null;
	var allCustomerNotesPaging = null;
	var currentCustomerGuid = "";
	var getPrevAppointmentsPageSize = global["pageSize"];
	var getAllCustomerNotesPageSize = global["pageSize"];
	var needRefreshLatestNotes = false;

	//load first customer
	var firstCustomerGuid = $(".listScrollBox_B li a").first().attr("guid");
	if (firstCustomerGuid) {
		loadCustomerMoreDetail(firstCustomerGuid);
		$(".listScrollBox_B :radio:first").attr("checked", "checked");
	}

	$(".listScrollBox_B li a").click(function () {
		var guid = $(this).attr("guid");
		var radio = $(this).prev(":radio");
		$(".listScrollBox_B :radio").removeAttr("checked");
		radio.attr("checked", "checked");
		loadCustomerMoreDetail(guid);
	});
	$(".listScrollBox_B :radio").click(function () {
		$(this).next(".scrollList").click();
	});

	$(document).on("click", ".btnDeleteUpcomingAppointment", function() {
		var row = $(this).parents(".listcontent");
		var guid = row.attr("guid");
		var upcoming_schedule = row.find(".upcoming_schedule").html();
		var upcoming_service = row.find(".upcoming_service").html();
		var upcoming_resource = row.find(".upcoming_resource").html();
		var upcoming_cost = row.find(".upcoming_cost").html();
		var customerName = $(".app_name").html();
		var t = $(this);
		new appointment.dialog.bo.deleteAppointmentConfirm({
			clickObject: t,
			title: global["text_delete_appointment"],
			tips: global["text_are_you_to_cancel_appointment"],
			reasonLabel: global["text_reason_for_delete"],
			textTips: global["text_leave_message_here"],
			customerLabel: global["text_customer"],
			resourceLabel: global["text_resource"],
			scheduleLabel: global["text_schedule"],
			reasonTitleLabel: global["text_reason_for_cancel_title"],
			reasonMessageLabel: global["text_reason_for_cancel_message"],
			serviceLabel: global["text_service"],
			serviceName: upcoming_service,
			costLabel: global["text_cost"],
			successDialogTitle: global["text_appointment_cancelled"],
			successDialogContent: global["text_the_appointment_has_been_canceled_successfully"],
			customerName: customerName,
			schedule: upcoming_schedule,
			resourceName: upcoming_resource,
			cost: upcoming_cost,
			onOK: function (r) {
				var reason = r;
				appointment.cancelAppointment({
					customerGuid: currentCustomerGuid,
					bookingGuid: guid,
					reason: reason
				}, function (r) {
					row.fadeOut("fast");
				});
			}
		})
	});

	$(document).on("click", ".btnEditAppointment", function() {
		var booking = $(this).eq(0).parents(".listcontent").data("appointment");
		var $btn = $(this);
		var model = {
			ServiceGuid: booking.ServiceGuid,
			ResourceGuid: booking.ResourceGuid,
			BookingGuid: booking.BookingGuid,
			StartDateTime: booking.StartDateTime,
			ServiceName: booking.ServiceName,
			ResourceName: booking.ResourceName,
			CustomerGuid: currentCustomerGuid,
			FirstName: $(".app_name").html(),
			LastName: '',
			Email: $(".app_email").html(),
			Phone: $(".app_phone").html(),
			Source: booking.Source
		};
		window.updateAppointment = new appointment.dialog.bo.updateAppointment({
			clickObject: $btn,

			firstNameLabel: global["text_first_name"],
			lastNameLabel: global["text_last_name"],
			phoneLabel: global["text_phone"],
			emailLabel: global["text_email"],
			timeLabel: global["text_time"],
			sourceOfAppointmentLable: global["text_source_of_appointment"],
			cancelAppointmentLable: global["text_cancel_this_appointment"],
			phone: model.Contact,
			email: model.Email,
			successDialogTitle: global["text_appointment_cancelled"],
			successDialogContent: global["text_the_appointment_has_been_canceled_successfully"],
			source: model.Source,
			source_phone: global["text_source_phone"],
			source_email: global["text_source_email"],
			source_walkin: global["text_source_walkin"],
			source_others: global["text_source_others"],
			typeLabel: global["text_typeLabel"],
			existingCustomerLabel: global["text_existingCustomerLabel"],
			newCustomerLabel: global["text_newCustomerLabel"],
			confirmUpdateDialogMessage: global["text_want_to_update_appointment"],
			confirmUpdateDialogTitle: global["text_confirm_update_appointment"],

			phone: model.Phone,
			email: model.Email,
			serviceLabel: global["text_service"],
			scheduleLabel: global["text_schedule"],
			resourceLabel: global["text_resource"],
			customerLabel: global["text_customer"],
			selectServiceText: global["text_select_service"],
			serviceName: model.ServiceName,
			selectResourceText: global["text_select_resource"],
			resourceName: model.ResourceName,
			customerGuid: model.CustomerGuid,
			customerName: "{0} {1}".format(model.FirstName, model.LastName),
			serviceGuid: model.ServiceGuid,
			resouceGuid: model.ResourceGuid,
			bookingGuid: model.BookingGuid,
			startDateTime: model.StartDateTime,
			serverDate: Date.parse(global["nowDateTime"]),
			title: global["text_edit_appointment"],
			dateLable: global["text_date"],
			avaliableTimeLabel: global["text_avaliable_time"],
			newScheduleLabel: global["text_new_schedule"],
			createText: global["text_update"],
			noSchemaLabel: global["text_no_schema"],
			onUpdate: function (result) {
				var dialog = this;
				appointment.dialog.bo.loading($btn);
				appointment.updateAppointment({
					serviceGuid: result.serviceGuid,
					resouceGuid: result.resourceGuid,
					source: result.source,
					schedule: Date.parse(result.schedule).toString("yyyy-MM-dd HH:mm"),
					customerGuid: result.customerGuid,
					bookingGuid: model.BookingGuid
				}, function (r) {
					appointment.dialog.bo.closeLoading();
					if (r.success) {
						dialog.destory();
						loadCustomerMoreDetail(currentCustomerGuid);
					} else {
						alert("error");
					}
				});
			}
		});
	});

	$(".search_Keyword").keyup(function (e) {
		var searchCustomerName = $.trim($(".search_Keyword").val());
		if (searchCustomerName.length == 0) {
			$(".listScrollBox_B li").show();
		} else {
			$(".listScrollBox_B li").show().filter(function (m) {
				return $(this).text().toLowerCase().indexOf(searchCustomerName.toLowerCase()) < 0
			}).hide();
		}
	});

	$(".btnViewAllPrevAppointments").click(function () {
		//get first page data
		appointment.getPrevAppointments({
			pageSize: getPrevAppointmentsPageSize,
			customerGuid: currentCustomerGuid
		}, function (r) {
			if (prevAppointmentPaging == null) {
				prevAppointmentPaging = new window.appointment.paging({
					pageSize: getPrevAppointmentsPageSize,
					total: r.total,
					appendTo: $(".prevAppointmentsPaging"),
					onclick: function (page) {
						//get next page data
						appointment.getPrevAppointments({
							pageSize: getPrevAppointmentsPageSize,
							pageNumber: page,
							customerGuid: currentCustomerGuid
						}, function (r) {
							bindPrevouisAppointments($(".allPrevAppointmentsList"), r.rows, true);
							appointment.resizeFrame(true);
						});
					}
				})
			} else {
				prevAppointmentPaging.reset({
					total: r.total
				});
			}
			$(".customerListContainer").hide();
			$(".allPrevAppointmentsContainer").show();
			bindPrevouisAppointments($(".allPrevAppointmentsList"), r.rows, true);
			appointment.resizeFrame(true);
		});
	});

	$(".btnSendMessage").click(function () {
		var t = $(this);
		var customerName = $(".app_name").html();
		new appointment.dialog.bo.sendMessage({
			textTips: global["text_leave_message_here"],
			customerLabel: global["text_customer"],
			title: global["text_send_message_to_customer"],
			messageLabel: global["text_message"],
			customerName: customerName,
			confirmDialogTitle: global["text_send_message_confimation"],
			confirmDialogSubTitle: global["text_are_you_sure_send_this_message"],
			clickObject: t,
			onOK: function (message) {
				appointment.sendCustomerMessage({
					customerName: $(".app_name").html(),
					message: message,
					customerGuid: currentCustomerGuid
				}, function (r) {
					console.log(r);
				});
			}
		});
	});

	$(".btnBackToCustomerList").click(function () {
		parent.scrollTo(0, 0);
		refreshLatestNotes();
		$(".customerListContainer").show();
		$(".allPrevAppointmentsContainer").hide();
		$(".allCustomerNotesContainer").hide();
		appointment.resizeFrame(false);
	});

	$(".btnAddCustomerNote").click(function () {
		new appointment.dialog.bo.addCustomerNote({
			title: global["text_add_notes"],
			textTips: global["text_leave_message_here"],
			clickObject: $(this),
			onOK: function (message) {
				appointment.addCustomerNote({
					message: message,
					customerGuid: currentCustomerGuid
				}, function (r) {
					if (r.success) {
						if ($(".latestCustomerNotes .listcontent").eq(2).length > 0) {
							$(".latestCustomerNotes .listcontent").eq(2).remove();
							$(".btnViewAllNotes").show();
						}
						appendNewCustomerNote(r.Id, message, r.createdTime, "first");
						$(".latestCustomerNotes").show();
						$(".latestCustomerNotesNoData").hide();
						resetNotesRowBgColor();
					}
				});
			}
		});
	});

	$(".btnViewAllNotes").click(function () {
		appointment.getCustomerNotes({ customerGuid: currentCustomerGuid, pageSize: getAllCustomerNotesPageSize }, function (result) {

			var notes = result.rows;
			var total = result.total;
			$(".allCustomerNotesContainer .scheduled-container .listcontent").remove();
			$.each(notes, function (i, note) {
				appendToAllNotesContainer(note.Guid, note.Message, note.CreatedTime);
			});
			if (allCustomerNotesPaging == null) {
				allCustomerNotesPaging = new window.appointment.paging({
					pageSize: getAllCustomerNotesPageSize,
					total: total,
					appendTo: $(".allCustomerNotesPaging"),
					onclick: function (page) {
						//get next page data
						appointment.getCustomerNotes({
							pageSize: getAllCustomerNotesPageSize,
							pageNumber: page,
							customerGuid: currentCustomerGuid
						}, function (r) {
							$(".allCustomerNotesContainer .scheduled-container .listcontent").remove();
							$.each(r.rows, function (i, note) {
								appendToAllNotesContainer(note.Guid, note.Message, note.CreatedTime);
							});
							appointment.resizeFrame(true);
						});
					}
				})
			} else {
				allCustomerNotesPaging.reset({
					total: total
				});
			}

			$(".customerListContainer").hide();
			$(".allCustomerNotesContainer").show();
			appointment.resizeFrame(true);

		});
	});

	$(document).on("click", ".ViewNote_Upcomming", function() {
		var row = $(this).parents(".listcontent");
		var dealDateString = row.find(".upcoming_schedule").html();
		var message = row.find(".upcoming_note").html();
		var content = '<p>' + dealDateString + '</p><br><p>' + message + '</p>';
		appointment.dialog.bo.info(global["text_view_note"], content, function () { }, row);
	});

	$('.btnCreateAppointment').click(function () {
		var $btn = $(this);
		var model = {
			/*
			ServiceGuid: 
			ResourceGuid: 
			BookingGuid: 
			StartDateTime: 
			BookingGuid:
			ServiceName:
			ResourceName:*/
			CustomerGuid: currentCustomerGuid,
			Email: $(".app_email").html(),
			Phone: $(".app_phone").html(),
			FirstName: $(".app_name").html()
		};
		window.updateAppointment = new appointment.dialog.bo.updateAppointment({
			clickObject: $btn,
			serviceLabel: global["text_service"],
			scheduleLabel: global["text_schedule"],
			resourceLabel: global["text_resource"],
			customerLabel: global["text_customer"],
			//serviceName: model.ServiceName,
			selectServiceText: global["text_select_service"],
			selectResourceText: global["text_select_resource"],
			//resourceName: model.ResourceName,
			customerGuid: model.CustomerGuid,
			phone: model.Phone,
			email: model.Email,
			customerName: "{0}".format(model.FirstName),
			//serviceGuid: model.ServiceGuid,
			//resouceGuid: model.ResourceGuid,

			firstNameLabel: global["text_first_name"],
			lastNameLabel: global["text_last_name"],
			phoneLabel: global["text_phone"],
			emailLabel: global["text_email"],
			timeLabel: global["text_time"],
			sourceOfAppointmentLable: global["text_source_of_appointment"],
			cancelAppointmentLable: global["text_cancel_this_appointment"],
			//phone: model.Contact,
			//email: model.Email,
			successDialogTitle: global["text_appointment_cancelled"],
			successDialogContent: global["text_the_appointment_has_been_canceled_successfully"],
			//source: model.Source,
			source_phone: global["text_source_phone"],
			source_email: global["text_source_email"],
			source_walkin: global["text_source_walkin"],
			source_others: global["text_source_others"],
			typeLabel: global["text_typeLabel"],
			existingCustomerLabel: global["text_existingCustomerLabel"],
			newCustomerLabel: global["text_newCustomerLabel"],
			confirmCreateDialogMessage: global["text_want_to_create_appointment"],
			confirmCreateDialogTitle: global["text_confirm_create_appointment"],

			//startDateTime: model.StartDateTime,
			serverDate: Date.parse(global["nowDateTime"]),
			title: global["text_create_appointment"],
			dateLable: global["text_date"],
			avaliableTimeLabel: global["text_avaliable_time"],
			newScheduleLabel: global["text_new_schedule"],
			createText: global["text_create_appointment"],
			noSchemaLabel: global["text_no_schema"],
			onCreate: function (result) {
				var dialog = this;
				appointment.dialog.bo.loading($btn);
				appointment.createAppointment({
					customerGuid: result.customerGuid,
					serviceGuid: result.serviceGuid,
					resourceGuid: result.resourceGuid,
					source: result.source,
					schedule: Date.parse(result.schedule).toString("yyyy-MM-dd HH:mm")
				}, function (r) {
					appointment.dialog.bo.closeLoading();
					if (r.success) {
						dialog.destory();
						new appointment.dialog.bo.info(global["text_appointment_created_title"], global["text_appointment_created_message"], function () { }, $btn);
						loadCustomerMoreDetail(result.customerGuid);
					} else {
						switch (r.errorCode) {
							case 1:
								alert('schedule is invalidate');
								break;
							case 2:
								alert('schedule full');
								break;
							case 3:
								alert('create timeslot error');
								break;
							case 4:
								alert('create booking error');
								break;
							case 5:
								alert('sent email error');
								break;
						}
					}
				});
			}
		});
	});

	function loadCustomerMoreDetail(cusomterGuid) {
		currentCustomerGuid = cusomterGuid;
		//add class selectedList
		$(".listScrollBox_B li a").removeClass("selectedList");
		$(".listScrollBox_B li a[guid=" + cusomterGuid + "]").addClass("selectedList");
		appointment.getCustomerMoreDetail({ customerGuid: cusomterGuid }, function (r) {
			bindData(r);
		});
	}

	function bindData(detail) {
		var info = detail.info;
		if (info.email.length == 0 || info.isOffline == "T") {
			$(".btnSendMessage").hide();
		} else {
			$(".btnSendMessage").show();
		}
		var upcomingAppointments = detail.upcomingAppointments;
		var prevouisAppointments = detail.prevouisAppointments;
		var prevouisAppointmentsCount = appointment.common.toNumber(detail.prevouisAppointmentsCount);
		var latestCustomerNotes = detail.notes;
		var customerNotesCount = appointment.common.toNumber(detail.customerNotesCount);
		//bind info
		$(".app_name").html("{0} {1}".format(info.firstName, info.lastName));
		$(".app_phone").html(info.contact);
		$(".app_email").html(info.email);

		//bind upcoming appointments
		if (upcomingAppointments.length == 0) {
			$(".upcomingAppointmentsNoDataContainer").show();
			$(".upcomingAppointmentsContainer").hide();
		} else {
			$(".upcomingAppointmentsNoDataContainer").hide();
			$(".upcomingAppointmentsContainer").show();
			$(".upcomingAppointmentsContainer .listcontent").remove();
			rowHtml = '<div class="listcontent row{index}" guid="{guid}"> ' +
		       '<span class="cc01_Upcoming_Appointment upcoming_schedule">{schedule}</span> ' +
			'<span class="cc02_Upcoming_Appointment upcoming_service">{service}</span> ' +
			'<span class="cc03_Upcoming_Appointment upcoming_resource">{resource}</span> ' +
			'<span class="cc04_Upcoming_Appointment upcoming_cost">{cost}</span> ' +
			'<span class="cc05_Upcoming_Appointment upcoming_view"><a href="javascript:;" class="ViewNote_Upcomming">{display}</a></span> ' +
			'<span class="cc06_Upcoming_Appointment"><a title="' + global["text_cancel"] + '" href="javascript:;" style="margin-left:5px" class="btnDeleteUpcomingAppointment dbShop-ui-appBtn dbShop-ui-adddelete_action"></a><a style="margin-left:10px" class="btnEditAppointment dbShop-ui-appBtn dbShop-ui-edit_action dbShop-ui-Btn-right" href="javascript:void(0);" title="' + global["text_edit"] + '"></a></span> ' +
			'<b class="upcoming_note" style="display:none">{note}</b> ' +
			'</div> ';
			$.each(upcomingAppointments, function (i, appointment) {
				var dealDateString = window.appointment.dateTimeFormat(appointment.StartDateTime);
				var $row = $(rowHtml.replace("{index}", (i % 2) == 0 ? 1 : 2).replace("{display}", appointment.Note.length == 0 ? "" : global["text_view"]).replace("{note}", appointment.Note).replace("{guid}", appointment.BookingGuid).replace("{schedule}", dealDateString).replace("{service}", appointment.ServiceName).replace("{resource}", appointment.ResourceName).replace("{cost}", appointment.FormatCost));
				$row.data("appointment", appointment);
				$(".upcomingAppointmentsContainer").append($row).append('<div class="dbShop-ui-clearboth"></div>');
			});
		}

		//bind prevouis appointments
		if (prevouisAppointments.length == 0) {
			$(".previousAppointmentsContainer").hide();
			$(".previousAppointmentsNoDataContainer").show();
			$(".btnViewAllPrevAppointments").hide();
		} else {
			$(".previousAppointmentsContainer").show();
			$(".previousAppointmentsNoDataContainer").hide();
			bindPrevouisAppointments($(".previousAppointmentsContainer"), prevouisAppointments, false);
			if (prevouisAppointmentsCount > 3) {
				$(".btnViewAllPrevAppointments").show();
			} else {
				$(".btnViewAllPrevAppointments").hide();
			}
		}

		//bind customer notes
		bindLatestCustomerNotes(latestCustomerNotes, customerNotesCount);
		/*
		if (latestCustomerNotes.length == 0) {
		$(".latestCustomerNotes").hide();
		$(".latestCustomerNotesNoData").show();
		$(".btnViewAllNotes").hide();
		} else {
		$(".latestCustomerNotes").show();
		$(".latestCustomerNotesNoData").hide();
		bindLatestCustomerNotes(latestCustomerNotes, customerNotesCount);
		}
		*/
		appointment.resizeFrame();
	}

	function bindPrevouisAppointments(container, data, appendRowLetter) {
		container.find(".listcontent").remove();
		if (appendRowLetter) {
			rowHtml = '<div class="listcontent row{index}"> ' +
        	'<span class="cc01">{service}</span> ' +
        	'<span class="cc02 upcoming_schedule">{schedule}</span> ' +
        	'<span class="cc03">{endDate}</span> ' +
        	'<span class="cc04">{resource}</span> ' +
			'<span class="cc05">{cost}</span>' +
			'<span class="cc05_Previous_Appointment upcoming_view"><a href="javascript:;" class="ViewNote_Upcomming">{display}</a></span> ' +
			'<b class="upcoming_note" style="display:none">{note}</b> ' +
       		'</div><div class="dbShop-ui-clearboth"></div>';
		} else {
			rowHtml = '<div class="listcontent row{index}"> ' +
        	'<span class="cc01_Previous_Appointment upcoming_schedule">{schedule}</span> ' +
        	'<span class="cc02_Previous_Appointment">{service}</span> ' +
        	'<span class="cc03_Previous_Appointment">{resource}</span> ' +
        	'<span class="cc04_Previous_Appointment">{cost}</span> ' +
			'<span class="cc05_Previous_Appointment upcoming_view"><a href="javascript:;" class="ViewNote_Upcomming">{display}</a></span> ' +
			'<b class="upcoming_note" style="display:none">{note}</b> ' +
       		'</div><div class="dbShop-ui-clearboth"></div>';
		}

		$.each(data, function (i, appointment) {
			var dealDateString = window.appointment.dateTimeFormat(appointment.StartDateTime);
			var endDateString = window.appointment.dateTimeFormat(appointment.EndDateTime);
			var $row = $(rowHtml.replace("{index}", (i % 2) == 0 ? 1 : 2).replace("{display}", appointment.Note.length == 0 ? "" : global["text_view"]).replace("{note}", appointment.Note).replace("{endDate}", endDateString).replace("{schedule}", dealDateString).replace("{service}", appointment.ServiceName).replace("{resource}", appointment.ResourceName).replace("{cost}", appointment.FormatCost));
			$row.data("appointment", appointment);
			container.append($row);
		});
	}

	//appendTo: first or last
	function appendNewCustomerNote(id, message, time, appendTo) {
		if (typeof (appendTo) == "undefined") {
			appendTo = "last";
		}
		var dealDateString = appointment.dateTimeFormat(time);
		var row = $('<div class="listcontent row1" id="' + id + '"> ');
		row.append('<span class="cc01_Upcoming_Appointment_Note">' + dealDateString + '</span> ');
		row.append('<span class="cc02_Upcoming_Appointment_Note">' + message + '</span> ');
		row.append('<span class="cc03_Upcoming_Appointment_Note"><a class="dbShop-ui-appBtn dbShop-ui-adddelete_action" style="margin-left:5px" href="javascript:;" title="' + global["delete"] + '"></a></span> ');

		row.find("a").click(function () {
			new appointment.dialog.bo.deleteConfirm(global["text_delete_note"], global["text_are_you_to_del_note"], function () {
				var t = this;
				appointment.deleteCustomerNote({ noteGuid: id }, function () {
					needRefreshLatestNotes = true;
					t.destory();
					row.remove();
					refreshLatestNotes();
					appointment.resizeFrame();
				});
			}, $(this));
			return false;
		});
		if (appendTo == "first") {
			$(".latestCustomerNotes .scheduled-header").after(row).after('<div class="dbShop-ui-clearboth"></div>');
		} else {
			$(".latestCustomerNotes").append(row).append('<div class="dbShop-ui-clearboth"></div>');
		}

		row.click(function () {
			var content = '<p>' + dealDateString + '</p><br><p>' + message + '</p>';
			appointment.dialog.bo.info(global["text_view_note"], content, function () { }, row);
		});
		appointment.resizeFrame();
	}

	function appendToAllNotesContainer(id, message, time) {
		var dealDateString = window.appointment.dateTimeFormat(time);
		var row = $('<div class="listcontent row1" id="' + id + '"> ');
		row.append('<span class="cc01_an">' + dealDateString + '</span> ');
		row.append('<span class="cc02_bn">' + message + '</span> ');
		row.append('<span class="cc04_dn"><a class="dbShop-ui-appBtn dbShop-ui-adddelete_action" style="margin-left:5px" href="javascript:;" title="' + global["delete"] + '"></a></span> ');
		row.find("a").click(function () {
			new appointment.dialog.bo.deleteConfirm(global["text_delete_note"], global["text_are_you_to_del_note"], function () {
				var t = this;
				appointment.deleteCustomerNote({ noteGuid: id }, function () {
					needRefreshLatestNotes = true;
					t.destory();
					row.remove();
					resetNotesRowBgColor();
					appointment.resizeFrame();
				});
			}, $(this));
			return false;
		});
		$(".allCustomerNotesContainer .scheduled-container").append(row);
		row.click(function () {
			var content = '<p>' + dealDateString + '</p><br><p>' + message + '</p>';
			appointment.dialog.bo.info(global["text_view_note"], content, function () { }, row);
		});
		resetNotesRowBgColor();
		appointment.resizeFrame();
	}

	function bindLatestCustomerNotes(notes, count) {
		$(".latestCustomerNotes .listcontent").remove();
		$.each(notes, function (i, note) {
			appendNewCustomerNote(note.Guid, note.Message, note.CreatedTime);
		});
		resetNotesRowBgColor();
		if (count > 3) {
			$(".btnViewAllNotes").show();
		} else {
			$(".btnViewAllNotes").hide();
		}
		if (count == 0) {
			$(".latestCustomerNotes").hide();
			$(".latestCustomerNotesNoData").show();
			$(".btnViewAllNotes").hide();
		} else {
			$(".latestCustomerNotes").show();
			$(".latestCustomerNotesNoData").hide();
		}
	}

	function refreshLatestNotes() {
		if (needRefreshLatestNotes) {
			appointment.getCustomerNotes({ customerGuid: currentCustomerGuid, pageSize: 3, pageNumber: 1 }, function (result) {
				var notes = result.rows;
				var total = result.total;
				bindLatestCustomerNotes(notes, total);
				needRefreshLatestNotes = false;
			});
		}
	}

	function resetNotesRowBgColor() {
		$(".latestCustomerNotes .listcontent").each(function (i, n) {
			$(this).removeClass("row1").removeClass("row2").addClass("row" + (i % 2 == 0 ? 1 : 2));
		});
		$(".allCustomerNotesContainer .listcontent").each(function (i, n) {
			$(this).removeClass("row1").removeClass("row2").addClass("row" + (i % 2 == 0 ? 1 : 2));
		});
	}
});