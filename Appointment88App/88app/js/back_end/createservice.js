﻿$(function () {
	appointment.common.setDefaultValue();
	appointment.common.setValidation();

	for (var i = 0; i < 24; i++) {
		$(".ddl_hours").append("<option value='{0}'>{0} {1}</option>".format(i, ''));
	}
	for (var i = 0; i < 60; i++) {
		$(".ddl_mins").append("<option value='{0}'>{0} {1}</option>".format(i, ''));
	}
	for (var i = 1; i < 30; i++) {
		$(".ddl_days").append("<option value='{0}'>{0} {1}</option>".format(i, ''));
	}
	for (var i = 0; i < 30; i++) {
		$(".ddl_days_0").append("<option value='{0}'>{0} {1}</option>".format(i, ''));
	}
	for (var i = 0; i < 4; i++) {
		$(".ddl_hafMins").append("<option value='{0}'>{0} {1}</option>".format(i * 15, ''));
	}
	for (var i = 0; i < 13; i++) {
		$(".ddl_hafHours").append("<option value='{0}'>{0} {1}</option>".format(i, ''));
	}

	if (global.isEdit) {
		loadDetail();
	}

	$('.minicolors').each(function () {
		$(this).miniColors({
			hasConfirmPanel: false,
			showThemeColors: true,
			realTimeChangeTriggerEleColor: true,
			change: function (hex, rgb) {
				appointment.common.hideErrorMessage($('.minicolors'));
			}
		});

	});

	$("#timeSlotDivision_hour, #timeSlotDivision_min").change(function () {
		getTimeSlotDivision();
	});
	$("#minTime_day, #minTime_hour").change(function () {
		getMinTime();
	});
	$("#duration_hour, #duration_min").change(function () {
		getDuration();
	});
	$("#advanceTime_day, #advanceTime_hour").change(function () {
		$(":radio[name='RadadvanceTime']").eq(1).attr("checked", "checked");
		var type = $(":radio[name='RadadvanceTime']:checked").val();
		getAdvanceTime();
	});
	$(":radio[name='RadadvanceTime']").click(function () {
		var type = $(":radio[name='RadadvanceTime']:checked").val();
		if (type == "limit") {
			getAdvanceTime();
		} else {
			appointment.common.hideErrorMessage($("#advanceTime")[0]);
		}
	});

	$("#duration_allday").click(function () {
		var isChecked = $(this).is(":checked");
		if (isChecked) {
			$("#duration_hour").attr("disabled", "disabled");
			$("#duration_min").attr("disabled", "disabled");
		} else {
			$("#duration_hour").removeAttr("disabled");
			$("#duration_min").removeAttr("disabled");
		}
		getDuration();
	});
	$(".btnSave").click(function () {
		getTimeSlotDivision();
		getDuration();
		if (appointment.common.getValidation()) {
			var model = {
				name: $.trim($("#name").val()),
				//duration: $.trim($("#duration").val()),
				availablePerTimeSlot: $.trim($("#availablePerTimeSlot").val()),
				cost: $.trim($("#cost").val()),
				assignColor: $.trim($("#assignColor").val()),
				description: tinyMCE.get()[0].getContent().replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;')
			};
			
			/*
			if (model.description.length == 0) {
			appointment.common.displayErrorMessage($("#description_tbl")[0], global["please_select_valid_value"]);
			return false;
			} else {
			appointment.common.hideErrorMessage($("#description_tbl")[0]);
			}
			*/
			appointment.dialog.bo.loading($(this))
			model.timeSlotDivision = getTimeSlotDivision();
			model.duration = getDuration();
			model.minTime = getMinTime();
			model.advanceTime = getAdvanceTime();
			model.id = $.query.get("serviceid");

			if (model.duration == 0 || model.timeSlotDivision == 0 || (model.advanceTime === 0)) {
				return false;
			}

			var inputs = [];
			$.each(model, function (k, v) {
				inputs.push({ name: k, value: v });
			});
			appointment.common.postUrl({
				inputs: inputs
			});
		}
	});

	$(".HelpIco").click(function () {
		new appointment.dialog.bo.createServiceTips({ clickObject: $(this) });
	});

	function getDuration() {
		var isAllDay = $("#duration_allday").is(":checked");
		if (!isAllDay) {
			var duration_hour = appointment.common.toNumber($("#duration_hour").val());
			var duration_min = appointment.common.toNumber($("#duration_min").val());
			if (duration_hour + duration_min == 0) {
				appointment.common.displayErrorMessage($("#duration")[0], global["please_select_valid_value"]);
				return 0;
			} else {
				appointment.common.hideErrorMessage($("#duration")[0]);
			}
			return duration_hour * 60 + duration_min;
		} else {
			appointment.common.hideErrorMessage($("#duration")[0]);
			return 24 * 60;
		}
	}
	function getMinTime() {
		var minTime_day = appointment.common.toNumber($("#minTime_day").val());
		var minTime_hour = appointment.common.toNumber($("#minTime_hour").val());
		if (minTime_day + minTime_hour == 0) {
			//appointment.common.displayErrorMessage($("#minTime")[0], global["please_select_valid_value"]);
			return 0;
		} else {
			appointment.common.hideErrorMessage($("#minTime")[0]);
		}
		return minTime_day * 24 + minTime_hour;
	}
	function getTimeSlotDivision() {
		var timeSlotDivision_hour = appointment.common.toNumber($("#timeSlotDivision_hour").val());
		var timeSlotDivision_min = appointment.common.toNumber($("#timeSlotDivision_min").val());
		if (timeSlotDivision_hour + timeSlotDivision_min == 0) {
			appointment.common.displayErrorMessage($("#timeSlotDivision")[0], global["please_select_valid_value"]);
			return 0;
		} else {
			appointment.common.hideErrorMessage($("#timeSlotDivision")[0]);
		}
		return timeSlotDivision_hour * 60 + timeSlotDivision_min;
	}
	function getAdvanceTime() {
		var type = $(":radio[name='RadadvanceTime']:checked").val();
		if (type == "limit") {
			var advanceTime_day = appointment.common.toNumber($("#advanceTime_day").val());
			var advanceTime_hour = appointment.common.toNumber($("#advanceTime_hour").val());
			if (advanceTime_day + advanceTime_hour == 0) {
				appointment.common.displayErrorMessage($("#advanceTime")[0], global["please_select_valid_value"]);
				return 0;
			} else {
				appointment.common.hideErrorMessage($("#advanceTime")[0]);
			}
			return advanceTime_day * 24 + advanceTime_hour;
		} else {
			return "";
		}
	}

	function loadDetail() {
		var timeSlotDivision = appointment.common.toNumber(global.timeSlotDivision);
		var minTime = appointment.common.toNumber(global.minTime);
		var advanceTime = appointment.common.toNumber(global.advanceTime);
		var duration = appointment.common.toNumber(global.duration);

		timeSlotDivision_hour = Math.floor(timeSlotDivision / 60);
		timeSlotDivision_min = timeSlotDivision % 60;
		$("#timeSlotDivision_hour").val(timeSlotDivision_hour);
		$("#timeSlotDivision_min").val(timeSlotDivision_min);

		var minTime_day = Math.floor(minTime / 24);
		var minTime_hour = minTime % 24;
		$("#minTime_day").val(minTime_day);
		$("#minTime_hour").val(minTime_hour);

		duration_hour = Math.floor(duration / 60);
		duration_min = duration % 60;
		if (duration_hour == 24) {
			$("#duration_hour").attr("disabled", "disabled");
			$("#duration_min").attr("disabled", "disabled");
			$("#duration_allday").attr("checked", "checked");
		} else {
			$("#duration_hour").removeAttr("disabled");
			$("#duration_min").removeAttr("disabled");
			$("#duration_hour").val(duration_hour);
			$("#duration_min").val(duration_min);
			$("#duration_allday").removeAttr("checked");
		}

		if (advanceTime > 0) {
			advanceTime_day = Math.floor(advanceTime / 24);
			advanceTime_hour = advanceTime % 24;
			$("#advanceTime_day").val(advanceTime_day);
			$("#advanceTime_hour").val(advanceTime_hour);
			$(":radio[name='RadadvanceTime']").eq(1).attr("checked", "checked");
		} else {
			$(":radio[name='RadadvanceTime']").eq(0).attr("checked", "checked");
		}
	}
});
