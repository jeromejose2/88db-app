﻿/**
 * Version: 1.0 Alpha-1 
 * Build Date: 13-Nov-2007
 * Copyright (c) 2006-2007, Coolite Inc. (http://www.coolite.com/). All rights reserved.
 * License: Licensed under The MIT License. See license.txt and http://www.datejs.com/license/. 
 * Website: http://www.datejs.com/ or http://www.coolite.com/datejs/
 */
Date.CultureInfo={name:"en-US",englishName:"English (United States)",nativeName:"English (United States)",dayNames:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],abbreviatedDayNames:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],shortestDayNames:["Su","Mo","Tu","We","Th","Fr","Sa"],firstLetterDayNames:["S","M","T","W","T","F","S"],monthNames:["January","February","March","April","May","June","July","August","September","October","November","December"],abbreviatedMonthNames:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],amDesignator:"AM",pmDesignator:"PM",firstDayOfWeek:0,twoDigitYearMax:2029,dateElementOrder:"mdy",formatPatterns:{shortDate:"M/d/yyyy",longDate:"dddd, MMMM dd, yyyy",shortTime:"h:mm tt",longTime:"h:mm:ss tt",fullDateTime:"dddd, MMMM dd, yyyy h:mm:ss tt",sortableDateTime:"yyyy-MM-ddTHH:mm:ss",universalSortableDateTime:"yyyy-MM-dd HH:mm:ssZ",rfc1123:"ddd, dd MMM yyyy HH:mm:ss GMT",monthDay:"MMMM dd",yearMonth:"MMMM, yyyy"},regexPatterns:{jan:/^jan(uary)?/i,feb:/^feb(ruary)?/i,mar:/^mar(ch)?/i,apr:/^apr(il)?/i,may:/^may/i,jun:/^jun(e)?/i,jul:/^jul(y)?/i,aug:/^aug(ust)?/i,sep:/^sep(t(ember)?)?/i,oct:/^oct(ober)?/i,nov:/^nov(ember)?/i,dec:/^dec(ember)?/i,sun:/^su(n(day)?)?/i,mon:/^mo(n(day)?)?/i,tue:/^tu(e(s(day)?)?)?/i,wed:/^we(d(nesday)?)?/i,thu:/^th(u(r(s(day)?)?)?)?/i,fri:/^fr(i(day)?)?/i,sat:/^sa(t(urday)?)?/i,future:/^next/i,past:/^last|past|prev(ious)?/i,add:/^(\+|after|from)/i,subtract:/^(\-|before|ago)/i,yesterday:/^yesterday/i,today:/^t(oday)?/i,tomorrow:/^tomorrow/i,now:/^n(ow)?/i,millisecond:/^ms|milli(second)?s?/i,second:/^sec(ond)?s?/i,minute:/^min(ute)?s?/i,hour:/^h(ou)?rs?/i,week:/^w(ee)?k/i,month:/^m(o(nth)?s?)?/i,day:/^d(ays?)?/i,year:/^y((ea)?rs?)?/i,shortMeridian:/^(a|p)/i,longMeridian:/^(a\.?m?\.?|p\.?m?\.?)/i,timezone:/^((e(s|d)t|c(s|d)t|m(s|d)t|p(s|d)t)|((gmt)?\s*(\+|\-)\s*\d\d\d\d?)|gmt)/i,ordinalSuffix:/^\s*(st|nd|rd|th)/i,timeContext:/^\s*(\:|a|p)/i},abbreviatedTimeZoneStandard:{GMT:"-000",EST:"-0400",CST:"-0500",MST:"-0600",PST:"-0700"},abbreviatedTimeZoneDST:{GMT:"-000",EDT:"-0500",CDT:"-0600",MDT:"-0700",PDT:"-0800"}};
Date.getMonthNumberFromName=function(name){var n=Date.CultureInfo.monthNames,m=Date.CultureInfo.abbreviatedMonthNames,s=name.toLowerCase();for(var i=0;i<n.length;i++){if(n[i].toLowerCase()==s||m[i].toLowerCase()==s){return i;}}
return-1;};Date.getDayNumberFromName=function(name){var n=Date.CultureInfo.dayNames,m=Date.CultureInfo.abbreviatedDayNames,o=Date.CultureInfo.shortestDayNames,s=name.toLowerCase();for(var i=0;i<n.length;i++){if(n[i].toLowerCase()==s||m[i].toLowerCase()==s){return i;}}
return-1;};Date.isLeapYear=function(year){return(((year%4===0)&&(year%100!==0))||(year%400===0));};Date.getDaysInMonth=function(year,month){return[31,(Date.isLeapYear(year)?29:28),31,30,31,30,31,31,30,31,30,31][month];};Date.getTimezoneOffset=function(s,dst){return(dst||false)?Date.CultureInfo.abbreviatedTimeZoneDST[s.toUpperCase()]:Date.CultureInfo.abbreviatedTimeZoneStandard[s.toUpperCase()];};Date.getTimezoneAbbreviation=function(offset,dst){var n=(dst||false)?Date.CultureInfo.abbreviatedTimeZoneDST:Date.CultureInfo.abbreviatedTimeZoneStandard,p;for(p in n){if(n[p]===offset){return p;}}
return null;};Date.prototype.clone=function(){return new Date(this.getTime());};Date.prototype.compareTo=function(date){if(isNaN(this)){throw new Error(this);}
if(date instanceof Date&&!isNaN(date)){return(this>date)?1:(this<date)?-1:0;}else{throw new TypeError(date);}};Date.prototype.equals=function(date){return(this.compareTo(date)===0);};Date.prototype.between=function(start,end){var t=this.getTime();return t>=start.getTime()&&t<=end.getTime();};Date.prototype.addMilliseconds=function(value){this.setMilliseconds(this.getMilliseconds()+value);return this;};Date.prototype.addSeconds=function(value){return this.addMilliseconds(value*1000);};Date.prototype.addMinutes=function(value){return this.addMilliseconds(value*60000);};Date.prototype.addHours=function(value){return this.addMilliseconds(value*3600000);};Date.prototype.addDays=function(value){return this.addMilliseconds(value*86400000);};Date.prototype.addWeeks=function(value){return this.addMilliseconds(value*604800000);};Date.prototype.addMonths=function(value){var n=this.getDate();this.setDate(1);this.setMonth(this.getMonth()+value);this.setDate(Math.min(n,this.getDaysInMonth()));return this;};Date.prototype.addYears=function(value){return this.addMonths(value*12);};Date.prototype.add=function(config){if(typeof config=="number"){this._orient=config;return this;}
var x=config;if(x.millisecond||x.milliseconds){this.addMilliseconds(x.millisecond||x.milliseconds);}
if(x.second||x.seconds){this.addSeconds(x.second||x.seconds);}
if(x.minute||x.minutes){this.addMinutes(x.minute||x.minutes);}
if(x.hour||x.hours){this.addHours(x.hour||x.hours);}
if(x.month||x.months){this.addMonths(x.month||x.months);}
if(x.year||x.years){this.addYears(x.year||x.years);}
if(x.day||x.days){this.addDays(x.day||x.days);}
return this;};Date._validate=function(value,min,max,name){if(typeof value!="number"){throw new TypeError(value+" is not a Number.");}else if(value<min||value>max){throw new RangeError(value+" is not a valid value for "+name+".");}
return true;};Date.validateMillisecond=function(n){return Date._validate(n,0,999,"milliseconds");};Date.validateSecond=function(n){return Date._validate(n,0,59,"seconds");};Date.validateMinute=function(n){return Date._validate(n,0,59,"minutes");};Date.validateHour=function(n){return Date._validate(n,0,23,"hours");};Date.validateDay=function(n,year,month){return Date._validate(n,1,Date.getDaysInMonth(year,month),"days");};Date.validateMonth=function(n){return Date._validate(n,0,11,"months");};Date.validateYear=function(n){return Date._validate(n,1,9999,"seconds");};Date.prototype.set=function(config){var x=config;if(!x.millisecond&&x.millisecond!==0){x.millisecond=-1;}
if(!x.second&&x.second!==0){x.second=-1;}
if(!x.minute&&x.minute!==0){x.minute=-1;}
if(!x.hour&&x.hour!==0){x.hour=-1;}
if(!x.day&&x.day!==0){x.day=-1;}
if(!x.month&&x.month!==0){x.month=-1;}
if(!x.year&&x.year!==0){x.year=-1;}
if(x.millisecond!=-1&&Date.validateMillisecond(x.millisecond)){this.addMilliseconds(x.millisecond-this.getMilliseconds());}
if(x.second!=-1&&Date.validateSecond(x.second)){this.addSeconds(x.second-this.getSeconds());}
if(x.minute!=-1&&Date.validateMinute(x.minute)){this.addMinutes(x.minute-this.getMinutes());}
if(x.hour!=-1&&Date.validateHour(x.hour)){this.addHours(x.hour-this.getHours());}
if(x.month!==-1&&Date.validateMonth(x.month)){this.addMonths(x.month-this.getMonth());}
if(x.year!=-1&&Date.validateYear(x.year)){this.addYears(x.year-this.getFullYear());}
if(x.day!=-1&&Date.validateDay(x.day,this.getFullYear(),this.getMonth())){this.addDays(x.day-this.getDate());}
if(x.timezone){this.setTimezone(x.timezone);}
if(x.timezoneOffset){this.setTimezoneOffset(x.timezoneOffset);}
return this;};Date.prototype.clearTime=function(){this.setHours(0);this.setMinutes(0);this.setSeconds(0);this.setMilliseconds(0);return this;};Date.prototype.isLeapYear=function(){var y=this.getFullYear();return(((y%4===0)&&(y%100!==0))||(y%400===0));};Date.prototype.isWeekday=function(){return!(this.is().sat()||this.is().sun());};Date.prototype.getDaysInMonth=function(){return Date.getDaysInMonth(this.getFullYear(),this.getMonth());};Date.prototype.moveToFirstDayOfMonth=function(){return this.set({day:1});};Date.prototype.moveToLastDayOfMonth=function(){return this.set({day:this.getDaysInMonth()});};Date.prototype.moveToDayOfWeek=function(day,orient){var diff=(day-this.getDay()+7*(orient||+1))%7;return this.addDays((diff===0)?diff+=7*(orient||+1):diff);};Date.prototype.moveToMonth=function(month,orient){var diff=(month-this.getMonth()+12*(orient||+1))%12;return this.addMonths((diff===0)?diff+=12*(orient||+1):diff);};Date.prototype.getDayOfYear=function(){return Math.floor((this-new Date(this.getFullYear(),0,1))/86400000);};Date.prototype.getWeekOfYear=function(firstDayOfWeek){var y=this.getFullYear(),m=this.getMonth(),d=this.getDate();var dow=firstDayOfWeek||Date.CultureInfo.firstDayOfWeek;var offset=7+1-new Date(y,0,1).getDay();if(offset==8){offset=1;}
var daynum=((Date.UTC(y,m,d,0,0,0)-Date.UTC(y,0,1,0,0,0))/86400000)+1;var w=Math.floor((daynum-offset+7)/7);if(w===dow){y--;var prevOffset=7+1-new Date(y,0,1).getDay();if(prevOffset==2||prevOffset==8){w=53;}else{w=52;}}
return w;};Date.prototype.isDST=function(){console.log('isDST');return this.toString().match(/(E|C|M|P)(S|D)T/)[2]=="D";};Date.prototype.getTimezone=function(){return Date.getTimezoneAbbreviation(this.getUTCOffset,this.isDST());};Date.prototype.setTimezoneOffset=function(s){var here=this.getTimezoneOffset(),there=Number(s)*-6/10;this.addMinutes(there-here);return this;};Date.prototype.setTimezone=function(s){return this.setTimezoneOffset(Date.getTimezoneOffset(s));};Date.prototype.getUTCOffset=function(){var n=this.getTimezoneOffset()*-10/6,r;if(n<0){r=(n-10000).toString();return r[0]+r.substr(2);}else{r=(n+10000).toString();return"+"+r.substr(1);}};Date.prototype.getDayName=function(abbrev){return abbrev?Date.CultureInfo.abbreviatedDayNames[this.getDay()]:Date.CultureInfo.dayNames[this.getDay()];};Date.prototype.getMonthName=function(abbrev){return abbrev?Date.CultureInfo.abbreviatedMonthNames[this.getMonth()]:Date.CultureInfo.monthNames[this.getMonth()];};Date.prototype._toString=Date.prototype.toString;Date.prototype.toString=function(format){var self=this;var p=function p(s){return(s.toString().length==1)?"0"+s:s;};return format?format.replace(/dd?d?d?|MM?M?M?|yy?y?y?|hh?|HH?|mm?|ss?|tt?|zz?z?/g,function(format){switch(format){case"hh":return p(self.getHours()<13?self.getHours():(self.getHours()-12));case"h":return self.getHours()<13?self.getHours():(self.getHours()-12);case"HH":return p(self.getHours());case"H":return self.getHours();case"mm":return p(self.getMinutes());case"m":return self.getMinutes();case"ss":return p(self.getSeconds());case"s":return self.getSeconds();case"yyyy":return self.getFullYear();case"yy":return self.getFullYear().toString().substring(2,4);case"dddd":return self.getDayName();case"ddd":return self.getDayName(true);case"dd":return p(self.getDate());case"d":return self.getDate().toString();case"MMMM":return self.getMonthName();case"MMM":return self.getMonthName(true);case"MM":return p((self.getMonth()+1));case"M":return self.getMonth()+1;case"t":return self.getHours()<12?Date.CultureInfo.amDesignator.substring(0,1):Date.CultureInfo.pmDesignator.substring(0,1);case"tt":return self.getHours()<12?Date.CultureInfo.amDesignator:Date.CultureInfo.pmDesignator;case"zzz":case"zz":case"z":return"";}}):this._toString();};
Date.now=function(){return new Date();};Date.today=function(){return Date.now().clearTime();};Date.prototype._orient=+1;Date.prototype.next=function(){this._orient=+1;return this;};Date.prototype.last=Date.prototype.prev=Date.prototype.previous=function(){this._orient=-1;return this;};Date.prototype._is=false;Date.prototype.is=function(){this._is=true;return this;};Number.prototype._dateElement="day";Number.prototype.fromNow=function(){var c={};c[this._dateElement]=this;return Date.now().add(c);};Number.prototype.ago=function(){var c={};c[this._dateElement]=this*-1;return Date.now().add(c);};(function(){var $D=Date.prototype,$N=Number.prototype;var dx=("sunday monday tuesday wednesday thursday friday saturday").split(/\s/),mx=("january february march april may june july august september october november december").split(/\s/),px=("Millisecond Second Minute Hour Day Week Month Year").split(/\s/),de;var df=function(n){return function(){if(this._is){this._is=false;return this.getDay()==n;}
return this.moveToDayOfWeek(n,this._orient);};};for(var i=0;i<dx.length;i++){$D[dx[i]]=$D[dx[i].substring(0,3)]=df(i);}
var mf=function(n){return function(){if(this._is){this._is=false;return this.getMonth()===n;}
return this.moveToMonth(n,this._orient);};};for(var j=0;j<mx.length;j++){$D[mx[j]]=$D[mx[j].substring(0,3)]=mf(j);}
var ef=function(j){return function(){if(j.substring(j.length-1)!="s"){j+="s";}
return this["add"+j](this._orient);};};var nf=function(n){return function(){this._dateElement=n;return this;};};for(var k=0;k<px.length;k++){de=px[k].toLowerCase();$D[de]=$D[de+"s"]=ef(px[k]);$N[de]=$N[de+"s"]=nf(de);}}());Date.prototype.toJSONString=function(){return this.toString("yyyy-MM-ddThh:mm:ssZ");};Date.prototype.toShortDateString=function(){return this.toString(Date.CultureInfo.formatPatterns.shortDatePattern);};Date.prototype.toLongDateString=function(){return this.toString(Date.CultureInfo.formatPatterns.longDatePattern);};Date.prototype.toShortTimeString=function(){return this.toString(Date.CultureInfo.formatPatterns.shortTimePattern);};Date.prototype.toLongTimeString=function(){return this.toString(Date.CultureInfo.formatPatterns.longTimePattern);};Date.prototype.getOrdinal=function(){switch(this.getDate()){case 1:case 21:case 31:return"st";case 2:case 22:return"nd";case 3:case 23:return"rd";default:return"th";}};
(function(){Date.Parsing={Exception:function(s){this.message="Parse error at '"+s.substring(0,10)+" ...'";}};var $P=Date.Parsing;var _=$P.Operators={rtoken:function(r){return function(s){var mx=s.match(r);if(mx){return([mx[0],s.substring(mx[0].length)]);}else{throw new $P.Exception(s);}};},token:function(s){return function(s){return _.rtoken(new RegExp("^\s*"+s+"\s*"))(s);};},stoken:function(s){return _.rtoken(new RegExp("^"+s));},until:function(p){return function(s){var qx=[],rx=null;while(s.length){try{rx=p.call(this,s);}catch(e){qx.push(rx[0]);s=rx[1];continue;}
break;}
return[qx,s];};},many:function(p){return function(s){var rx=[],r=null;while(s.length){try{r=p.call(this,s);}catch(e){return[rx,s];}
rx.push(r[0]);s=r[1];}
return[rx,s];};},optional:function(p){return function(s){var r=null;try{r=p.call(this,s);}catch(e){return[null,s];}
return[r[0],r[1]];};},not:function(p){return function(s){try{p.call(this,s);}catch(e){return[null,s];}
throw new $P.Exception(s);};},ignore:function(p){return p?function(s){var r=null;r=p.call(this,s);return[null,r[1]];}:null;},product:function(){var px=arguments[0],qx=Array.prototype.slice.call(arguments,1),rx=[];for(var i=0;i<px.length;i++){rx.push(_.each(px[i],qx));}
return rx;},cache:function(rule){var cache={},r=null;return function(s){try{r=cache[s]=(cache[s]||rule.call(this,s));}catch(e){r=cache[s]=e;}
if(r instanceof $P.Exception){throw r;}else{return r;}};},any:function(){var px=arguments;return function(s){var r=null;for(var i=0;i<px.length;i++){if(px[i]==null){continue;}
try{r=(px[i].call(this,s));}catch(e){r=null;}
if(r){return r;}}
throw new $P.Exception(s);};},each:function(){var px=arguments;return function(s){var rx=[],r=null;for(var i=0;i<px.length;i++){if(px[i]==null){continue;}
try{r=(px[i].call(this,s));}catch(e){throw new $P.Exception(s);}
rx.push(r[0]);s=r[1];}
return[rx,s];};},all:function(){var px=arguments,_=_;return _.each(_.optional(px));},sequence:function(px,d,c){d=d||_.rtoken(/^\s*/);c=c||null;if(px.length==1){return px[0];}
return function(s){var r=null,q=null;var rx=[];for(var i=0;i<px.length;i++){try{r=px[i].call(this,s);}catch(e){break;}
rx.push(r[0]);try{q=d.call(this,r[1]);}catch(ex){q=null;break;}
s=q[1];}
if(!r){throw new $P.Exception(s);}
if(q){throw new $P.Exception(q[1]);}
if(c){try{r=c.call(this,r[1]);}catch(ey){throw new $P.Exception(r[1]);}}
return[rx,(r?r[1]:s)];};},between:function(d1,p,d2){d2=d2||d1;var _fn=_.each(_.ignore(d1),p,_.ignore(d2));return function(s){var rx=_fn.call(this,s);return[[rx[0][0],r[0][2]],rx[1]];};},list:function(p,d,c){d=d||_.rtoken(/^\s*/);c=c||null;return(p instanceof Array?_.each(_.product(p.slice(0,-1),_.ignore(d)),p.slice(-1),_.ignore(c)):_.each(_.many(_.each(p,_.ignore(d))),px,_.ignore(c)));},set:function(px,d,c){d=d||_.rtoken(/^\s*/);c=c||null;return function(s){var r=null,p=null,q=null,rx=null,best=[[],s],last=false;for(var i=0;i<px.length;i++){q=null;p=null;r=null;last=(px.length==1);try{r=px[i].call(this,s);}catch(e){continue;}
rx=[[r[0]],r[1]];if(r[1].length>0&&!last){try{q=d.call(this,r[1]);}catch(ex){last=true;}}else{last=true;}
if(!last&&q[1].length===0){last=true;}
if(!last){var qx=[];for(var j=0;j<px.length;j++){if(i!=j){qx.push(px[j]);}}
p=_.set(qx,d).call(this,q[1]);if(p[0].length>0){rx[0]=rx[0].concat(p[0]);rx[1]=p[1];}}
if(rx[1].length<best[1].length){best=rx;}
if(best[1].length===0){break;}}
if(best[0].length===0){return best;}
if(c){try{q=c.call(this,best[1]);}catch(ey){throw new $P.Exception(best[1]);}
best[1]=q[1];}
return best;};},forward:function(gr,fname){return function(s){return gr[fname].call(this,s);};},replace:function(rule,repl){return function(s){var r=rule.call(this,s);return[repl,r[1]];};},process:function(rule,fn){return function(s){var r=rule.call(this,s);return[fn.call(this,r[0]),r[1]];};},min:function(min,rule){return function(s){var rx=rule.call(this,s);if(rx[0].length<min){throw new $P.Exception(s);}
return rx;};}};var _generator=function(op){return function(){var args=null,rx=[];if(arguments.length>1){args=Array.prototype.slice.call(arguments);}else if(arguments[0]instanceof Array){args=arguments[0];}
if(args){for(var i=0,px=args.shift();i<px.length;i++){args.unshift(px[i]);rx.push(op.apply(null,args));args.shift();return rx;}}else{return op.apply(null,arguments);}};};var gx="optional not ignore cache".split(/\s/);for(var i=0;i<gx.length;i++){_[gx[i]]=_generator(_[gx[i]]);}
var _vector=function(op){return function(){if(arguments[0]instanceof Array){return op.apply(null,arguments[0]);}else{return op.apply(null,arguments);}};};var vx="each any all".split(/\s/);for(var j=0;j<vx.length;j++){_[vx[j]]=_vector(_[vx[j]]);}}());(function(){var flattenAndCompact=function(ax){var rx=[];for(var i=0;i<ax.length;i++){if(ax[i]instanceof Array){rx=rx.concat(flattenAndCompact(ax[i]));}else{if(ax[i]){rx.push(ax[i]);}}}
return rx;};Date.Grammar={};Date.Translator={hour:function(s){return function(){this.hour=Number(s);};},minute:function(s){return function(){this.minute=Number(s);};},second:function(s){return function(){this.second=Number(s);};},meridian:function(s){return function(){this.meridian=s.slice(0,1).toLowerCase();};},timezone:function(s){return function(){var n=s.replace(/[^\d\+\-]/g,"");if(n.length){this.timezoneOffset=Number(n);}else{this.timezone=s.toLowerCase();}};},day:function(x){var s=x[0];return function(){this.day=Number(s.match(/\d+/)[0]);};},month:function(s){return function(){this.month=((s.length==3)?Date.getMonthNumberFromName(s):(Number(s)-1));};},year:function(s){return function(){var n=Number(s);this.year=((s.length>2)?n:(n+(((n+2000)<Date.CultureInfo.twoDigitYearMax)?2000:1900)));};},rday:function(s){return function(){switch(s){case"yesterday":this.days=-1;break;case"tomorrow":this.days=1;break;case"today":this.days=0;break;case"now":this.days=0;this.now=true;break;}};},finishExact:function(x){x=(x instanceof Array)?x:[x];var now=new Date();this.year=now.getFullYear();this.month=now.getMonth();this.day=1;this.hour=0;this.minute=0;this.second=0;for(var i=0;i<x.length;i++){if(x[i]){x[i].call(this);}}
this.hour=(this.meridian=="p"&&this.hour<13)?this.hour+12:this.hour;if(this.day>Date.getDaysInMonth(this.year,this.month)){throw new RangeError(this.day+" is not a valid value for days.");}
var r=new Date(this.year,this.month,this.day,this.hour,this.minute,this.second);if(this.timezone){r.set({timezone:this.timezone});}else if(this.timezoneOffset){r.set({timezoneOffset:this.timezoneOffset});}
return r;},finish:function(x){x=(x instanceof Array)?flattenAndCompact(x):[x];if(x.length===0){return null;}
for(var i=0;i<x.length;i++){if(typeof x[i]=="function"){x[i].call(this);}}
if(this.now){return new Date();}
var today=Date.today();var method=null;var expression=!!(this.days!=null||this.orient||this.operator);if(expression){var gap,mod,orient;orient=((this.orient=="past"||this.operator=="subtract")?-1:1);if(this.weekday){this.unit="day";gap=(Date.getDayNumberFromName(this.weekday)-today.getDay());mod=7;this.days=gap?((gap+(orient*mod))%mod):(orient*mod);}
if(this.month){this.unit="month";gap=(this.month-today.getMonth());mod=12;this.months=gap?((gap+(orient*mod))%mod):(orient*mod);this.month=null;}
if(!this.unit){this.unit="day";}
if(this[this.unit+"s"]==null||this.operator!=null){if(!this.value){this.value=1;}
if(this.unit=="week"){this.unit="day";this.value=this.value*7;}
this[this.unit+"s"]=this.value*orient;}
return today.add(this);}else{if(this.meridian&&this.hour){this.hour=(this.hour<13&&this.meridian=="p")?this.hour+12:this.hour;}
if(this.weekday&&!this.day){this.day=(today.addDays((Date.getDayNumberFromName(this.weekday)-today.getDay()))).getDate();}
if(this.month&&!this.day){this.day=1;}
return today.set(this);}}};var _=Date.Parsing.Operators,g=Date.Grammar,t=Date.Translator,_fn;g.datePartDelimiter=_.rtoken(/^([\s\-\.\,\/\x27]+)/);g.timePartDelimiter=_.stoken(":");g.whiteSpace=_.rtoken(/^\s*/);g.generalDelimiter=_.rtoken(/^(([\s\,]|at|on)+)/);var _C={};g.ctoken=function(keys){var fn=_C[keys];if(!fn){var c=Date.CultureInfo.regexPatterns;var kx=keys.split(/\s+/),px=[];for(var i=0;i<kx.length;i++){px.push(_.replace(_.rtoken(c[kx[i]]),kx[i]));}
fn=_C[keys]=_.any.apply(null,px);}
return fn;};g.ctoken2=function(key){return _.rtoken(Date.CultureInfo.regexPatterns[key]);};g.h=_.cache(_.process(_.rtoken(/^(0[0-9]|1[0-2]|[1-9])/),t.hour));g.hh=_.cache(_.process(_.rtoken(/^(0[0-9]|1[0-2])/),t.hour));g.H=_.cache(_.process(_.rtoken(/^([0-1][0-9]|2[0-3]|[0-9])/),t.hour));g.HH=_.cache(_.process(_.rtoken(/^([0-1][0-9]|2[0-3])/),t.hour));g.m=_.cache(_.process(_.rtoken(/^([0-5][0-9]|[0-9])/),t.minute));g.mm=_.cache(_.process(_.rtoken(/^[0-5][0-9]/),t.minute));g.s=_.cache(_.process(_.rtoken(/^([0-5][0-9]|[0-9])/),t.second));g.ss=_.cache(_.process(_.rtoken(/^[0-5][0-9]/),t.second));g.hms=_.cache(_.sequence([g.H,g.mm,g.ss],g.timePartDelimiter));g.t=_.cache(_.process(g.ctoken2("shortMeridian"),t.meridian));g.tt=_.cache(_.process(g.ctoken2("longMeridian"),t.meridian));g.z=_.cache(_.process(_.rtoken(/^(\+|\-)?\s*\d\d\d\d?/),t.timezone));g.zz=_.cache(_.process(_.rtoken(/^(\+|\-)\s*\d\d\d\d/),t.timezone));g.zzz=_.cache(_.process(g.ctoken2("timezone"),t.timezone));g.timeSuffix=_.each(_.ignore(g.whiteSpace),_.set([g.tt,g.zzz]));g.time=_.each(_.optional(_.ignore(_.stoken("T"))),g.hms,g.timeSuffix);g.d=_.cache(_.process(_.each(_.rtoken(/^([0-2]\d|3[0-1]|\d)/),_.optional(g.ctoken2("ordinalSuffix"))),t.day));g.dd=_.cache(_.process(_.each(_.rtoken(/^([0-2]\d|3[0-1])/),_.optional(g.ctoken2("ordinalSuffix"))),t.day));g.ddd=g.dddd=_.cache(_.process(g.ctoken("sun mon tue wed thu fri sat"),function(s){return function(){this.weekday=s;};}));g.M=_.cache(_.process(_.rtoken(/^(1[0-2]|0\d|\d)/),t.month));g.MM=_.cache(_.process(_.rtoken(/^(1[0-2]|0\d)/),t.month));g.MMM=g.MMMM=_.cache(_.process(g.ctoken("jan feb mar apr may jun jul aug sep oct nov dec"),t.month));g.y=_.cache(_.process(_.rtoken(/^(\d\d?)/),t.year));g.yy=_.cache(_.process(_.rtoken(/^(\d\d)/),t.year));g.yyy=_.cache(_.process(_.rtoken(/^(\d\d?\d?\d?)/),t.year));g.yyyy=_.cache(_.process(_.rtoken(/^(\d\d\d\d)/),t.year));_fn=function(){return _.each(_.any.apply(null,arguments),_.not(g.ctoken2("timeContext")));};g.day=_fn(g.d,g.dd);g.month=_fn(g.M,g.MMM);g.year=_fn(g.yyyy,g.yy);g.orientation=_.process(g.ctoken("past future"),function(s){return function(){this.orient=s;};});g.operator=_.process(g.ctoken("add subtract"),function(s){return function(){this.operator=s;};});g.rday=_.process(g.ctoken("yesterday tomorrow today now"),t.rday);g.unit=_.process(g.ctoken("minute hour day week month year"),function(s){return function(){this.unit=s;};});g.value=_.process(_.rtoken(/^\d\d?(st|nd|rd|th)?/),function(s){return function(){this.value=s.replace(/\D/g,"");};});g.expression=_.set([g.rday,g.operator,g.value,g.unit,g.orientation,g.ddd,g.MMM]);_fn=function(){return _.set(arguments,g.datePartDelimiter);};g.mdy=_fn(g.ddd,g.month,g.day,g.year);g.ymd=_fn(g.ddd,g.year,g.month,g.day);g.dmy=_fn(g.ddd,g.day,g.month,g.year);g.date=function(s){return((g[Date.CultureInfo.dateElementOrder]||g.mdy).call(this,s));};g.format=_.process(_.many(_.any(_.process(_.rtoken(/^(dd?d?d?|MM?M?M?|yy?y?y?|hh?|HH?|mm?|ss?|tt?|zz?z?)/),function(fmt){if(g[fmt]){return g[fmt];}else{throw Date.Parsing.Exception(fmt);}}),_.process(_.rtoken(/^[^dMyhHmstz]+/),function(s){return _.ignore(_.stoken(s));}))),function(rules){return _.process(_.each.apply(null,rules),t.finishExact);});var _F={};var _get=function(f){return _F[f]=(_F[f]||g.format(f)[0]);};g.formats=function(fx){if(fx instanceof Array){var rx=[];for(var i=0;i<fx.length;i++){rx.push(_get(fx[i]));}
return _.any.apply(null,rx);}else{return _get(fx);}};g._formats=g.formats(["yyyy-MM-ddTHH:mm:ss","ddd, MMM dd, yyyy H:mm:ss tt","ddd MMM d yyyy HH:mm:ss zzz","d"]);g._start=_.process(_.set([g.date,g.time,g.expression],g.generalDelimiter,g.whiteSpace),t.finish);g.start=function(s){try{var r=g._formats.call({},s);if(r[1].length===0){return r;}}catch(e){}
return g._start.call({},s);};}());Date._parse=Date.parse;Date.parse=function(s){var r=null;if(!s){return null;}
try{r=Date.Grammar.start.call({},s);}catch(e){return null;}
return((r[1].length===0)?r[0]:null);};Date.getParseFunction=function(fx){var fn=Date.Grammar.formats(fx);return function(s){var r=null;try{r=fn.call({},s);}catch(e){return null;}
	return ((r[1].length === 0) ? r[0] : null);
};
}; Date.parseExact = function (s, fx) { return Date.getParseFunction(fx)(s); };
/**
* @version: 1.0 Alpha-1
* @author: Coolite Inc. http://www.coolite.com/
* @date: 2008-04-13
* @copyright: Copyright (c) 2006-2008, Coolite Inc. (http://www.coolite.com/). All rights reserved.
* @license: Licensed under The MIT License. See license.txt and http://www.datejs.com/license/. 
* @website: http://www.datejs.com/
*/

/* 
* TimeSpan(milliseconds);
* TimeSpan(days, hours, minutes, seconds);
* TimeSpan(days, hours, minutes, seconds, milliseconds);
*/
var TimeSpan = function (days, hours, minutes, seconds, milliseconds) {
	var attrs = "days hours minutes seconds milliseconds".split(/\s+/);

	var gFn = function (attr) {
		return function () {
			return this[attr];
		};
	};

	var sFn = function (attr) {
		return function (val) {
			this[attr] = val;
			return this;
		};
	};

	for (var i = 0; i < attrs.length; i++) {
		var $a = attrs[i], $b = $a.slice(0, 1).toUpperCase() + $a.slice(1);
		TimeSpan.prototype[$a] = 0;
		TimeSpan.prototype["get" + $b] = gFn($a);
		TimeSpan.prototype["set" + $b] = sFn($a);
	}

	if (arguments.length == 4) {
		this.setDays(days);
		this.setHours(hours);
		this.setMinutes(minutes);
		this.setSeconds(seconds);
	} else if (arguments.length == 5) {
		this.setDays(days);
		this.setHours(hours);
		this.setMinutes(minutes);
		this.setSeconds(seconds);
		this.setMilliseconds(milliseconds);
	} else if (arguments.length == 1 && typeof days == "number") {
		var orient = (days < 0) ? -1 : +1;
		this.setMilliseconds(Math.abs(days));

		this.setDays(Math.floor(this.getMilliseconds() / 86400000) * orient);
		this.setMilliseconds(this.getMilliseconds() % 86400000);

		this.setHours(Math.floor(this.getMilliseconds() / 3600000) * orient);
		this.setMilliseconds(this.getMilliseconds() % 3600000);

		this.setMinutes(Math.floor(this.getMilliseconds() / 60000) * orient);
		this.setMilliseconds(this.getMilliseconds() % 60000);

		this.setSeconds(Math.floor(this.getMilliseconds() / 1000) * orient);
		this.setMilliseconds(this.getMilliseconds() % 1000);

		this.setMilliseconds(this.getMilliseconds() * orient);
	}

	this.getTotalMilliseconds = function () {
		return (this.getDays() * 86400000) + (this.getHours() * 3600000) + (this.getMinutes() * 60000) + (this.getSeconds() * 1000);
	};

	this.compareTo = function (time) {
		var t1 = new Date(1970, 1, 1, this.getHours(), this.getMinutes(), this.getSeconds()), t2;
		if (time === null) {
			t2 = new Date(1970, 1, 1, 0, 0, 0);
		}
		else {
			t2 = new Date(1970, 1, 1, time.getHours(), time.getMinutes(), time.getSeconds());
		}
		return (t1 < t2) ? -1 : (t1 > t2) ? 1 : 0;
	};

	this.equals = function (time) {
		return (this.compareTo(time) === 0);
	};

	this.add = function (time) {
		return (time === null) ? this : this.addSeconds(time.getTotalMilliseconds() / 1000);
	};

	this.subtract = function (time) {
		return (time === null) ? this : this.addSeconds(-time.getTotalMilliseconds() / 1000);
	};

	this.addDays = function (n) {
		return new TimeSpan(this.getTotalMilliseconds() + (n * 86400000));
	};

	this.addHours = function (n) {
		return new TimeSpan(this.getTotalMilliseconds() + (n * 3600000));
	};

	this.addMinutes = function (n) {
		return new TimeSpan(this.getTotalMilliseconds() + (n * 60000));
	};

	this.addSeconds = function (n) {
		return new TimeSpan(this.getTotalMilliseconds() + (n * 1000));
	};

	this.addMilliseconds = function (n) {
		return new TimeSpan(this.getTotalMilliseconds() + n);
	};

	this.get12HourHour = function () {
		return (this.getHours() > 12) ? this.getHours() - 12 : (this.getHours() === 0) ? 12 : this.getHours();
	};

	this.getDesignator = function () {
		return (this.getHours() < 12) ? Date.CultureInfo.amDesignator : Date.CultureInfo.pmDesignator;
	};

	this.toString = function (format) {
		this._toString = function () {
			if (this.getDays() !== null && this.getDays() > 0) {
				return this.getDays() + "." + this.getHours() + ":" + this.p(this.getMinutes()) + ":" + this.p(this.getSeconds());
			}
			else {
				return this.getHours() + ":" + this.p(this.getMinutes()) + ":" + this.p(this.getSeconds());
			}
		};

		this.p = function (s) {
			return (s.toString().length < 2) ? "0" + s : s;
		};

		var me = this;

		return format ? format.replace(/dd?|HH?|hh?|mm?|ss?|tt?/g,
        function (format) {
        	switch (format) {
        		case "d":
        			return me.getDays();
        		case "dd":
        			return me.p(me.getDays());
        		case "H":
        			return me.getHours();
        		case "HH":
        			return me.p(me.getHours());
        		case "h":
        			return me.get12HourHour();
        		case "hh":
        			return me.p(me.get12HourHour());
        		case "m":
        			return me.getMinutes();
        		case "mm":
        			return me.p(me.getMinutes());
        		case "s":
        			return me.getSeconds();
        		case "ss":
        			return me.p(me.getSeconds());
        		case "t":
        			return ((me.getHours() < 12) ? Date.CultureInfo.amDesignator : Date.CultureInfo.pmDesignator).substring(0, 1);
        		case "tt":
        			return (me.getHours() < 12) ? Date.CultureInfo.amDesignator : Date.CultureInfo.pmDesignator;
        	}
        }
        ) : this._toString();
	};
	return this;
};

/**
* Gets the time of day for this date instances. 
* @return {TimeSpan} TimeSpan
*/
Date.prototype.getTimeOfDay = function () {
	return new TimeSpan(0, this.getHours(), this.getMinutes(), this.getSeconds(), this.getMilliseconds());
};

/* 
* TimePeriod(startDate, endDate);
* TimePeriod(years, months, days, hours, minutes, seconds, milliseconds);
*/
var TimePeriod = function (years, months, days, hours, minutes, seconds, milliseconds) {
	var attrs = "years months days hours minutes seconds milliseconds".split(/\s+/);

	var gFn = function (attr) {
		return function () {
			return this[attr];
		};
	};

	var sFn = function (attr) {
		return function (val) {
			this[attr] = val;
			return this;
		};
	};

	for (var i = 0; i < attrs.length; i++) {
		var $a = attrs[i], $b = $a.slice(0, 1).toUpperCase() + $a.slice(1);
		TimePeriod.prototype[$a] = 0;
		TimePeriod.prototype["get" + $b] = gFn($a);
		TimePeriod.prototype["set" + $b] = sFn($a);
	}

	if (arguments.length == 7) {
		this.years = years;
		this.months = months;
		this.setDays(days);
		this.setHours(hours);
		this.setMinutes(minutes);
		this.setSeconds(seconds);
		this.setMilliseconds(milliseconds);
	} else if (arguments.length == 2 && arguments[0] instanceof Date && arguments[1] instanceof Date) {
		// startDate and endDate as arguments

		var d1 = years.clone();
		var d2 = months.clone();

		var temp = d1.clone();
		var orient = (d1 > d2) ? -1 : +1;

		this.years = d2.getFullYear() - d1.getFullYear();
		temp.addYears(this.years);

		if (orient == +1) {
			if (temp > d2) {
				if (this.years !== 0) {
					this.years--;
				}
			}
		} else {
			if (temp < d2) {
				if (this.years !== 0) {
					this.years++;
				}
			}
		}

		d1.addYears(this.years);

		if (orient == +1) {
			while (d1 < d2 && d1.clone().addDays(Date.getDaysInMonth(d1.getYear(), d1.getMonth())) < d2) {
				d1.addMonths(1);
				this.months++;
			}
		}
		else {
			while (d1 > d2 && d1.clone().addDays(-d1.getDaysInMonth()) > d2) {
				d1.addMonths(-1);
				this.months--;
			}
		}

		var diff = d2 - d1;

		if (diff !== 0) {
			var ts = new TimeSpan(diff);
			this.setDays(ts.getDays());
			this.setHours(ts.getHours());
			this.setMinutes(ts.getMinutes());
			this.setSeconds(ts.getSeconds());
			this.setMilliseconds(ts.getMilliseconds());
		}
	}
	return this;
};

/**
* @version: 1.0 Alpha-1
* @author: Coolite Inc. http://www.coolite.com/
* @date: 2008-04-13
* @copyright: Copyright (c) 2006-2008, Coolite Inc. (http://www.coolite.com/). All rights reserved.
* @license: Licensed under The MIT License. See license.txt and http://www.datejs.com/license/. 
* @website: http://www.datejs.com/
*/

(function () {
	var $D = Date,
        $P = $D.prototype,
        $C = $D.CultureInfo,
        $f = [],
        p = function (s, l) {
        	if (!l) {
        		l = 2;
        	}
        	return ("000" + s).slice(l * -1);
        };

	/**
	* Converts a PHP format string to Java/.NET format string. 
	* A PHP format string can be used with .$format or .format.
	* A Java/.NET format string can be used with .toString().
	* The .parseExact function will only accept a Java/.NET format string
	*
	* Example
	<pre>
	var f1 = "%m/%d/%y"
	var f2 = Date.normalizeFormat(f1); // "MM/dd/yy"
     
	new Date().format(f1);    // "04/13/08"
	new Date().$format(f1);   // "04/13/08"
	new Date().toString(f2);  // "04/13/08"
     
	var date = Date.parseExact("04/13/08", f2); // Sun Apr 13 2008
	</pre>
	* @param {String}   A PHP format string consisting of one or more format spcifiers.
	* @return {String}  The PHP format converted to a Java/.NET format string.
	*/
	$D.normalizeFormat = function (format) {
		$f = [];
		var t = new Date().$format(format);
		return $f.join("");
	};

	/**
	* Format a local Unix timestamp according to locale settings
	* 
	* Example
	<pre>
	Date.strftime("%m/%d/%y", new Date());       // "04/13/08"
	Date.strftime("c", "2008-04-13T17:52:03Z");  // "04/13/08"
	</pre>
	* @param {String}   A format string consisting of one or more format spcifiers [Optional].
	* @param {Number}   The number representing the number of seconds that have elapsed since January 1, 1970 (local time). 
	* @return {String}  A string representation of the current Date object.
	*/
	$D.strftime = function (format, time) {
		return new Date(time * 1000).$format(format);
	};

	/**
	* Parse any textual datetime description into a Unix timestamp. 
	* A Unix timestamp is the number of seconds that have elapsed since January 1, 1970 (midnight UTC/GMT).
	* 
	* Example
	<pre>
	Date.strtotime("04/13/08");              // 1208044800
	Date.strtotime("1970-01-01T00:00:00Z");  // 0
	</pre>
	* @param {String}   A format string consisting of one or more format spcifiers [Optional].
	* @param {Object}   A string or date object.
	* @return {String}  A string representation of the current Date object.
	*/
	$D.strtotime = function (time) {
		var d = $D.parse(time);
		d.addMinutes(d.getTimezoneOffset() * -1);
		return Math.round($D.UTC(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate(), d.getUTCHours(), d.getUTCMinutes(), d.getUTCSeconds(), d.getUTCMilliseconds()) / 1000);
	};

	/**
	* Converts the value of the current Date object to its equivalent string representation using a PHP/Unix style of date format specifiers.
	*
	* The following descriptions are from http://www.php.net/strftime and http://www.php.net/manual/en/function.date.php. 
	* Copyright © 2001-2008 The PHP Group
	* 
	* Format Specifiers
	<pre>
	Format  Description                                                                  Example
	------  ---------------------------------------------------------------------------  -----------------------
	%a     abbreviated weekday name according to the current localed                    "Mon" through "Sun"
	%A     full weekday name according to the current locale                            "Sunday" through "Saturday"
	%b     abbreviated month name according to the current locale                       "Jan" through "Dec"
	%B     full month name according to the current locale                              "January" through "December"
	%c     preferred date and time representation for the current locale                "4/13/2008 12:33 PM"
	%C     century number (the year divided by 100 and truncated to an integer)         "00" to "99"
	%d     day of the month as a decimal number                                         "01" to "31"
	%D     same as %m/%d/%y                                                             "04/13/08"
	%e     day of the month as a decimal number, a single digit is preceded by a space  "1" to "31"
	%g     like %G, but without the century                                             "08"
	%G     The 4-digit year corresponding to the ISO week number (see %V).              "2008"
	This has the same format and value as %Y, except that if the ISO week number 
	belongs to the previous or next year, that year is used instead.
	%h     same as %b                                                                   "Jan" through "Dec"
	%H     hour as a decimal number using a 24-hour clock                               "00" to "23"
	%I     hour as a decimal number using a 12-hour clock                               "01" to "12"
	%j     day of the year as a decimal number                                          "001" to "366"
	%m     month as a decimal number                                                    "01" to "12"
	%M     minute as a decimal number                                                   "00" to "59"
	%n     newline character                                                            "\n"
	%p     either "am" or "pm" according to the given time value, or the                "am" or "pm"
	corresponding strings for the current locale
	%r     time in a.m. and p.m. notation                                               "8:44 PM"
	%R     time in 24 hour notation                                                     "20:44"
	%S     second as a decimal number                                                   "00" to "59"
	%t     tab character                                                                "\t"
	%T     current time, equal to %H:%M:%S                                              "12:49:11"
	%u     weekday as a decimal number ["1", "7"], with "1" representing Monday         "1" to "7"
	%U     week number of the current year as a decimal number, starting with the       "0" to ("52" or "53")
	first Sunday as the first day of the first week
	%V     The ISO 8601:1988 week number of the current year as a decimal number,       "00" to ("52" or "53")
	range 01 to 53, where week 1 is the first week that has at least 4 days 
	in the current year, and with Monday as the first day of the week. 
	(Use %G or %g for the year component that corresponds to the week number 
	for the specified timestamp.)
	%W     week number of the current year as a decimal number, starting with the       "00" to ("52" or "53")
	first Monday as the first day of the first week
	%w     day of the week as a decimal, Sunday being "0"                               "0" to "6"
	%x     preferred date representation for the current locale without the time        "4/13/2008"
	%X     preferred time representation for the current locale without the date        "12:53:05"
	%y     year as a decimal number without a century                                   "00" "99"
	%Y     year as a decimal number including the century                               "2008"
	%Z     time zone or name or abbreviation                                            "UTC", "EST", "PST"
	%z     same as %Z 
	%%     a literal "%" character                                                      "%"
      
	d      Day of the month, 2 digits with leading zeros                                "01" to "31"
	D      A textual representation of a day, three letters                             "Mon" through "Sun"
	j      Day of the month without leading zeros                                       "1" to "31"
	l      A full textual representation of the day of the week (lowercase "L")         "Sunday" through "Saturday"
	N      ISO-8601 numeric representation of the day of the week (added in PHP 5.1.0)  "1" (for Monday) through "7" (for Sunday)
	S      English ordinal suffix for the day of the month, 2 characters                "st", "nd", "rd" or "th". Works well with j
	w      Numeric representation of the day of the week                                "0" (for Sunday) through "6" (for Saturday)
	z      The day of the year (starting from "0")                                      "0" through "365"      
	W      ISO-8601 week number of year, weeks starting on Monday                       "00" to ("52" or "53")
	F      A full textual representation of a month, such as January or March           "January" through "December"
	m      Numeric representation of a month, with leading zeros                        "01" through "12"
	M      A short textual representation of a month, three letters                     "Jan" through "Dec"
	n      Numeric representation of a month, without leading zeros                     "1" through "12"
	t      Number of days in the given month                                            "28" through "31"
	L      Whether it's a leap year                                                     "1" if it is a leap year, "0" otherwise
	o      ISO-8601 year number. This has the same value as Y, except that if the       "2008"
	ISO week number (W) belongs to the previous or next year, that year 
	is used instead.
	Y      A full numeric representation of a year, 4 digits                            "2008"
	y      A two digit representation of a year                                         "08"
	a      Lowercase Ante meridiem and Post meridiem                                    "am" or "pm"
	A      Uppercase Ante meridiem and Post meridiem                                    "AM" or "PM"
	B      Swatch Internet time                                                         "000" through "999"
	g      12-hour format of an hour without leading zeros                              "1" through "12"
	G      24-hour format of an hour without leading zeros                              "0" through "23"
	h      12-hour format of an hour with leading zeros                                 "01" through "12"
	H      24-hour format of an hour with leading zeros                                 "00" through "23"
	i      Minutes with leading zeros                                                   "00" to "59"
	s      Seconds, with leading zeros                                                  "00" through "59"
	u      Milliseconds                                                                 "54321"
	e      Timezone identifier                                                          "UTC", "EST", "PST"
	I      Whether or not the date is in daylight saving time (uppercase i)             "1" if Daylight Saving Time, "0" otherwise
	O      Difference to Greenwich time (GMT) in hours                                  "+0200", "-0600"
	P      Difference to Greenwich time (GMT) with colon between hours and minutes      "+02:00", "-06:00"
	T      Timezone abbreviation                                                        "UTC", "EST", "PST"
	Z      Timezone offset in seconds. The offset for timezones west of UTC is          "-43200" through "50400"
	always negative, and for those east of UTC is always positive.
	c      ISO 8601 date                                                                "2004-02-12T15:19:21+00:00"
	r      RFC 2822 formatted date                                                      "Thu, 21 Dec 2000 16:01:07 +0200"
	U      Seconds since the Unix Epoch (January 1 1970 00:00:00 GMT)                   "0"     
	</pre>
	* @param {String}   A format string consisting of one or more format spcifiers [Optional].
	* @return {String}  A string representation of the current Date object.
	*/
	$P.$format = function (format) {
		var x = this,
            y,
            t = function (v) {
            	$f.push(v);
            	return x.toString(v);
            };

		return format ? format.replace(/(%|\\)?.|%%/g,
        function (m) {
        	if (m.charAt(0) === "\\" || m.substring(0, 2) === "%%") {
        		return m.replace("\\", "").replace("%%", "%");
        	}
        	switch (m) {
        		case "d":
        		case "%d":
        			return t("dd");
        		case "D":
        		case "%a":
        			return t("ddd");
        		case "j":
        		case "%e":
        			return t("d");
        		case "l":
        		case "%A":
        			return t("dddd");
        		case "N":
        		case "%u":
        			return x.getDay() + 1;
        		case "S":
        			return t("S");
        		case "w":
        		case "%w":
        			return x.getDay();
        		case "z":
        			return x.getOrdinalNumber();
        		case "%j":
        			return p(x.getOrdinalNumber(), 3);
        		case "%U":
        			var d1 = x.clone().set({ month: 0, day: 1 }).addDays(-1).moveToDayOfWeek(0),
                    d2 = x.clone().addDays(1).moveToDayOfWeek(0, -1);
        			return (d2 < d1) ? "00" : p((d2.getOrdinalNumber() - d1.getOrdinalNumber()) / 7 + 1);
        		case "W":
        		case "%V":
        			return x.getISOWeek();
        		case "%W":
        			return p(x.getWeek());
        		case "F":
        		case "%B":
        			return t("MMMM");
        		case "m":
        		case "%m":
        			return t("MM");
        		case "M":
        		case "%b":
        		case "%h":
        			return t("MMM");
        		case "n":
        			return t("M");
        		case "t":
        			return $D.getDaysInMonth(x.getFullYear(), x.getMonth());
        		case "L":
        			return ($D.isLeapYear(x.getFullYear())) ? 1 : 0;
        		case "o":
        		case "%G":
        			return x.setWeek(x.getISOWeek()).toString("yyyy");
        		case "%g":
        			return x.$format("%G").slice(-2);
        		case "Y":
        		case "%Y":
        			return t("yyyy");
        		case "y":
        		case "%y":
        			return t("yy");
        		case "a":
        		case "%p":
        			return t("tt").toLowerCase();
        		case "A":
        			return t("tt").toUpperCase();
        		case "g":
        		case "%I":
        			return t("h");
        		case "G":
        			return t("H");
        		case "h":
        			return t("hh");
        		case "H":
        		case "%H":
        			return t("HH");
        		case "i":
        		case "%M":
        			return t("mm");
        		case "s":
        		case "%S":
        			return t("ss");
        		case "u":
        			return p(x.getMilliseconds(), 3);
        		case "I":
        			return (x.isDaylightSavingTime()) ? 1 : 0;
        		case "O":
        			return x.getUTCOffset();
        		case "P":
        			y = x.getUTCOffset();
        			return y.substring(0, y.length - 2) + ":" + y.substring(y.length - 2);
        		case "e":
        		case "T":
        		case "%z":
        		case "%Z":
        			return x.getTimezone();
        		case "Z":
        			return x.getTimezoneOffset() * -60;
        		case "B":
        			var now = new Date();
        			return Math.floor(((now.getHours() * 3600) + (now.getMinutes() * 60) + now.getSeconds() + (now.getTimezoneOffset() + 60) * 60) / 86.4);
        		case "c":
        			return x.toISOString().replace(/\"/g, "");
        		case "U":
        			return $D.strtotime("now");
        		case "%c":
        			return t("d") + " " + t("t");
        		case "%C":
        			return Math.floor(x.getFullYear() / 100 + 1);
        		case "%D":
        			return t("MM/dd/yy");
        		case "%n":
        			return "\\n";
        		case "%t":
        			return "\\t";
        		case "%r":
        			return t("hh:mm tt");
        		case "%R":
        			return t("H:mm");
        		case "%T":
        			return t("H:mm:ss");
        		case "%x":
        			return t("d");
        		case "%X":
        			return t("t");
        		default:
        			$f.push(m);
        			return m;
        	}
        }
        ) : this._toString();
	};

	if (!$P.format) {
		$P.format = $P.$format;
	}
} ());

/**
* @version: 1.0 Alpha-1
* @author: Coolite Inc. http://www.coolite.com/
* @date: 2008-04-13
* @copyright: Copyright (c) 2006-2008, Coolite Inc. (http://www.coolite.com/). All rights reserved.
* @license: Licensed under The MIT License. See license.txt and http://www.datejs.com/license/. 
* @website: http://www.datejs.com/
*/

/**
**************************************************************
** SugarPak - Domain Specific Language -  Syntactical Sugar **
**************************************************************
*/

(function () {
	var $D = Date, $P = $D.prototype, $C = $D.CultureInfo, $N = Number.prototype;

	// private
	$P._orient = +1;

	// private
	$P._nth = null;

	// private
	$P._is = false;

	// private
	$P._same = false;

	// private
	$P._isSecond = false;

	// private
	$N._dateElement = "day";

	/** 
	* Moves the date to the next instance of a date as specified by the subsequent date element function (eg. .day(), .month()), month name function (eg. .january(), .jan()) or day name function (eg. .friday(), fri()).
	* Example
	<pre><code>
	Date.today().next().friday();
	Date.today().next().fri();
	Date.today().next().march();
	Date.today().next().mar();
	Date.today().next().week();
	</code></pre>
	* 
	* @return {Date}    date
	*/
	$P.next = function () {
		this._orient = +1;
		return this;
	};

	/** 
	* Creates a new Date (Date.today()) and moves the date to the next instance of the date as specified by the subsequent date element function (eg. .day(), .month()), month name function (eg. .january(), .jan()) or day name function (eg. .friday(), fri()).
	* Example
	<pre><code>
	Date.next().friday();
	Date.next().fri();
	Date.next().march();
	Date.next().mar();
	Date.next().week();
	</code></pre>
	* 
	* @return {Date}    date
	*/
	$D.next = function () {
		return $D.today().next();
	};

	/** 
	* Moves the date to the previous instance of a date as specified by the subsequent date element function (eg. .day(), .month()), month name function (eg. .january(), .jan()) or day name function (eg. .friday(), fri()).
	* Example
	<pre><code>
	Date.today().last().friday();
	Date.today().last().fri();
	Date.today().last().march();
	Date.today().last().mar();
	Date.today().last().week();
	</code></pre>
	*  
	* @return {Date}    date
	*/
	$P.last = $P.prev = $P.previous = function () {
		this._orient = -1;
		return this;
	};

	/** 
	* Creates a new Date (Date.today()) and moves the date to the previous instance of the date as specified by the subsequent date element function (eg. .day(), .month()), month name function (eg. .january(), .jan()) or day name function (eg. .friday(), fri()).
	* Example
	<pre><code>
	Date.last().friday();
	Date.last().fri();
	Date.previous().march();
	Date.prev().mar();
	Date.last().week();
	</code></pre>
	*  
	* @return {Date}    date
	*/
	$D.last = $D.prev = $D.previous = function () {
		return $D.today().last();
	};

	/** 
	* Performs a equality check when followed by either a month name, day name or .weekday() function.
	* Example
	<pre><code>
	Date.today().is().friday(); // true|false
	Date.today().is().fri();
	Date.today().is().march();
	Date.today().is().mar();
	</code></pre>
	*  
	* @return {Boolean}    true|false
	*/
	$P.is = function () {
		this._is = true;
		return this;
	};

	/** 
	* Determines if two date objects occur on/in exactly the same instance of the subsequent date part function.
	* The function .same() must be followed by a date part function (example: .day(), .month(), .year(), etc).
	*
	* An optional Date can be passed in the date part function. If now date is passed as a parameter, 'Now' is used. 
	*
	* The following example demonstrates how to determine if two dates fall on the exact same day.
	*
	* Example
	<pre><code>
	var d1 = Date.today(); // today at 00:00
	var d2 = new Date();   // exactly now.

	// Do they occur on the same day?
	d1.same().day(d2); // true
    
	// Do they occur on the same hour?
	d1.same().hour(d2); // false, unless d2 hour is '00' (midnight).
    
	// What if it's the same day, but one year apart?
	var nextYear = Date.today().add(1).year();

	d1.same().day(nextYear); // false, because the dates must occur on the exact same day. 
	</code></pre>
	*
	* Scenario: Determine if a given date occurs during some week period 2 months from now. 
	*
	* Example
	<pre><code>
	var future = Date.today().add(2).months();
	return someDate.same().week(future); // true|false;
	</code></pre>
	*  
	* @return {Boolean}    true|false
	*/
	$P.same = function () {
		this._same = true;
		this._isSecond = false;
		return this;
	};

	/** 
	* Determines if the current date/time occurs during Today. Must be preceded by the .is() function.
	* Example
	<pre><code>
	someDate.is().today();    // true|false
	new Date().is().today();  // true
	Date.today().is().today();// true
	Date.today().add(-1).day().is().today(); // false
	</code></pre>
	*  
	* @return {Boolean}    true|false
	*/
	$P.today = function () {
		return this.same().day();
	};

	/** 
	* Determines if the current date is a weekday. This function must be preceded by the .is() function.
	* Example
	<pre><code>
	Date.today().is().weekday(); // true|false
	</code></pre>
	*  
	* @return {Boolean}    true|false
	*/
	$P.weekday = function () {
		if (this._is) {
			this._is = false;
			return (!this.is().sat() && !this.is().sun());
		}
		return false;
	};

	/** 
	* Sets the Time of the current Date instance. A string "6:15 pm" or config object {hour:18, minute:15} are accepted.
	* Example
	<pre><code>
	// Set time to 6:15pm with a String
	Date.today().at("6:15pm");

	// Set time to 6:15pm with a config object
	Date.today().at({hour:18, minute:15});
	</code></pre>
	*  
	* @return {Date}    date
	*/
	$P.at = function (time) {
		return (typeof time === "string") ? $D.parse(this.toString("d") + " " + time) : this.set(time);
	};

	/** 
	* Creates a new Date() and adds this (Number) to the date based on the preceding date element function (eg. second|minute|hour|day|month|year).
	* Example
	<pre><code>
	// Undeclared Numbers must be wrapped with parentheses. Requirment of JavaScript.
	(3).days().fromNow();
	(6).months().fromNow();

	// Declared Number variables do not require parentheses. 
	var n = 6;
	n.months().fromNow();
	</code></pre>
	*  
	* @return {Date}    A new Date instance
	*/
	$N.fromNow = $N.after = function (date) {
		var c = {};
		c[this._dateElement] = this;
		return ((!date) ? new Date() : date.clone()).add(c);
	};

	/** 
	* Creates a new Date() and subtract this (Number) from the date based on the preceding date element function (eg. second|minute|hour|day|month|year).
	* Example
	<pre><code>
	// Undeclared Numbers must be wrapped with parentheses. Requirment of JavaScript.
	(3).days().ago();
	(6).months().ago();

	// Declared Number variables do not require parentheses. 
	var n = 6;
	n.months().ago();
	</code></pre>
	*  
	* @return {Date}    A new Date instance
	*/
	$N.ago = $N.before = function (date) {
		var c = {};
		c[this._dateElement] = this * -1;
		return ((!date) ? new Date() : date.clone()).add(c);
	};

	// Do NOT modify the following string tokens. These tokens are used to build dynamic functions.
	// All culture-specific strings can be found in the CultureInfo files. See /trunk/src/globalization/.
	var dx = ("sunday monday tuesday wednesday thursday friday saturday").split(/\s/),
        mx = ("january february march april may june july august september october november december").split(/\s/),
        px = ("Millisecond Second Minute Hour Day Week Month Year").split(/\s/),
        pxf = ("Milliseconds Seconds Minutes Hours Date Week Month FullYear").split(/\s/),
                nth = ("final first second third fourth fifth").split(/\s/),
        de;

	/** 
	* Returns an object literal of all the date parts.
	* Example
	<pre><code>
	var o = new Date().toObject();
        
	// { year: 2008, month: 4, week: 20, day: 13, hour: 18, minute: 9, second: 32, millisecond: 812 }
        
	// The object properties can be referenced directly from the object.
        
	alert(o.day);  // alerts "13"
	alert(o.year); // alerts "2008"
	</code></pre>
	*  
	* @return {Date}    An object literal representing the original date object.
	*/
	$P.toObject = function () {
		var o = {};
		for (var i = 0; i < px.length; i++) {
			o[px[i].toLowerCase()] = this["get" + pxf[i]]();
		}
		return o;
	};

	/** 
	* Returns a date created from an object literal. Ignores the .week property if set in the config. 
	* Example
	<pre><code>
	var o = new Date().toObject();
        
	return Date.fromObject(o); // will return the same date. 

	var o2 = {month: 1, day: 20, hour: 18}; // birthday party!
	Date.fromObject(o2);
	</code></pre>
	*  
	* @return {Date}    An object literal representing the original date object.
	*/
	$D.fromObject = function (config) {
		config.week = null;
		return Date.today().set(config);
	};

	// Create day name functions and abbreviated day name functions (eg. monday(), friday(), fri()).
	var df = function (n) {
		return function () {
			if (this._is) {
				this._is = false;
				return this.getDay() == n;
			}
			if (this._nth !== null) {
				// If the .second() function was called earlier, remove the _orient 
				// from the date, and then continue.
				// This is required because 'second' can be used in two different context.
				// 
				// Example
				//
				//   Date.today().add(1).second();
				//   Date.march().second().monday();
				// 
				// Things get crazy with the following...
				//   Date.march().add(1).second().second().monday(); // but it works!!
				//  
				if (this._isSecond) {
					this.addSeconds(this._orient * -1);
				}
				// make sure we reset _isSecond
				this._isSecond = false;

				var ntemp = this._nth;
				this._nth = null;
				var temp = this.clone().moveToLastDayOfMonth();
				this.moveToNthOccurrence(n, ntemp);
				if (this > temp) {
					throw new RangeError($D.getDayName(n) + " does not occur " + ntemp + " times in the month of " + $D.getMonthName(temp.getMonth()) + " " + temp.getFullYear() + ".");
				}
				return this;
			}
			return this.moveToDayOfWeek(n, this._orient);
		};
	};

	var sdf = function (n) {
		return function () {
			var t = $D.today(), shift = n - t.getDay();
			if (n === 0 && $C.firstDayOfWeek === 1 && t.getDay() !== 0) {
				shift = shift + 7;
			}
			return t.addDays(shift);
		};
	};

	for (var i = 0; i < dx.length; i++) {
		// Create constant static Day Name variables. Example: Date.MONDAY or Date.MON
		$D[dx[i].toUpperCase()] = $D[dx[i].toUpperCase().substring(0, 3)] = i;

		// Create Day Name functions. Example: Date.monday() or Date.mon()
		$D[dx[i]] = $D[dx[i].substring(0, 3)] = sdf(i);

		// Create Day Name instance functions. Example: Date.today().next().monday()
		$P[dx[i]] = $P[dx[i].substring(0, 3)] = df(i);
	}

	// Create month name functions and abbreviated month name functions (eg. january(), march(), mar()).
	var mf = function (n) {
		return function () {
			if (this._is) {
				this._is = false;
				return this.getMonth() === n;
			}
			return this.moveToMonth(n, this._orient);
		};
	};

	var smf = function (n) {
		return function () {
			return $D.today().set({ month: n, day: 1 });
		};
	};

	for (var j = 0; j < mx.length; j++) {
		// Create constant static Month Name variables. Example: Date.MARCH or Date.MAR
		$D[mx[j].toUpperCase()] = $D[mx[j].toUpperCase().substring(0, 3)] = j;

		// Create Month Name functions. Example: Date.march() or Date.mar()
		$D[mx[j]] = $D[mx[j].substring(0, 3)] = smf(j);

		// Create Month Name instance functions. Example: Date.today().next().march()
		$P[mx[j]] = $P[mx[j].substring(0, 3)] = mf(j);
	}

	// Create date element functions and plural date element functions used with Date (eg. day(), days(), months()).
	var ef = function (j) {
		return function () {
			// if the .second() function was called earlier, the _orient 
			// has alread been added. Just return this and reset _isSecond.
			if (this._isSecond) {
				this._isSecond = false;
				return this;
			}

			if (this._same) {
				this._same = this._is = false;
				var o1 = this.toObject(),
                    o2 = (arguments[0] || new Date()).toObject(),
                    v = "",
                    k = j.toLowerCase();

				for (var m = (px.length - 1); m > -1; m--) {
					v = px[m].toLowerCase();
					if (o1[v] != o2[v]) {
						return false;
					}
					if (k == v) {
						break;
					}
				}
				return true;
			}

			if (j.substring(j.length - 1) != "s") {
				j += "s";
			}
			return this["add" + j](this._orient);
		};
	};


	var nf = function (n) {
		return function () {
			this._dateElement = n;
			return this;
		};
	};

	for (var k = 0; k < px.length; k++) {
		de = px[k].toLowerCase();

		// Create date element functions and plural date element functions used with Date (eg. day(), days(), months()).
		$P[de] = $P[de + "s"] = ef(px[k]);

		// Create date element functions and plural date element functions used with Number (eg. day(), days(), months()).
		$N[de] = $N[de + "s"] = nf(de);
	}

	$P._ss = ef("Second");

	var nthfn = function (n) {
		return function (dayOfWeek) {
			if (this._same) {
				return this._ss(arguments[0]);
			}
			if (dayOfWeek || dayOfWeek === 0) {
				return this.moveToNthOccurrence(dayOfWeek, n);
			}
			this._nth = n;

			// if the operator is 'second' add the _orient, then deal with it later...
			if (n === 2 && (dayOfWeek === undefined || dayOfWeek === null)) {
				this._isSecond = true;
				return this.addSeconds(this._orient);
			}
			return this;
		};
	};

	for (var l = 0; l < nth.length; l++) {
		$P[nth[l]] = (l === 0) ? nthfn(-1) : nthfn(l);
	}
} ());